var assert = require('chai').assert;
var moment = require('moment');
var LeaderboardDateProviderService = require('../../src/services/leaderboard-date-provider');

describe('leaderboard date', function() {
  var dateProviderService = new LeaderboardDateProviderService(false);

  describe('#getDate()', function() {
    it('given leaderboard updates every Monday and today is Monday', function() {
      var now = moment('2015-01-05 09:32:21.141');
      var result = dateProviderService.getDates([ 1 ], now);

      assert.isTrue(result.current.isSame(new moment('2015-01-05 00:00:00')));
      assert.isTrue(result.previous.isSame(new moment('2014-12-29 00:00:00')));
      assert.isTrue(result.next.isSame(new moment('2015-01-12 00:00:00')));
    });

    it('given leaderboard updates every Monday and today is Tuesday', function() {
      var now = moment('2015-01-06 09:32:21.141');
      var result = dateProviderService.getDates([ 1 ], now);

      assert.isTrue(result.current.isSame(new moment('2015-01-05 00:00:00')));
      assert.isTrue(result.previous.isSame(new moment('2014-12-29 00:00:00')));
      assert.isTrue(result.next.isSame(new moment('2015-01-12 00:00:00')));
    });

    it('given leaderboard updates every Tuesday and Friday and today is Saturday', function() {
      var now = moment('2015-01-10 09:32:21.141');
      var result = dateProviderService.getDates([ 2, 5 ], now);

      assert.isTrue(result.current.isSame(new moment('2015-01-09 00:00:00')));
      assert.isTrue(result.previous.isSame(new moment('2015-01-06 00:00:00')));
      assert.isTrue(result.next.isSame(new moment('2015-01-13 00:00:00')));
    });
  });
});
