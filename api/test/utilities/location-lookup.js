var assert = require('chai').assert;
var LocationLookup = require('../../src/utilities/location-lookup');

describe('location lookup', function() {
  var locationLookup = new LocationLookup();

  describe('#lookup()', function() {
    it('given country found should return countryId', function() {
      var result = locationLookup.lookup(locationData, mapData);

      var expected = {
        district: null,
        county: null,
        region: null,
        country: {
          id: 1,
          name: 'Great Britain',
          countryCode: 'gb'
        }
      };

      assert.deepEqual(result, expected);
    });
  });

  var locationData = [
    { name: 'Kyle and Carrick',
      districtId: null,
      countyId: 172,
      regionId: null,
      countryId: null,
      countryCode: null
    },
    {
      name: 'Blaenau Gwent',
      districtId: 437,
      countyId: null,
      regionId: null,
      countryId: null,
      countryCode: null
    },
    {
      name: 'Belfast',
      districtId: null,
      countyId: null,
      regionId: 18,
      countryId: null,
      countryCode: null
    },
    {
      name: 'Great Britain',
      districtId: null,
      countyId: null,
      regionId: null,
      countryId: 1,
      countryCode: 'gb'
    }
  ];

  var mapData = [
    {
      "address_components": [
        {
          "long_name": "Taylorson Street South",
          "short_name": "Taylorson St S",
          "types": [
            "route"
          ]
        },
        {
          "long_name": "Salford",
          "short_name": "Salford",
          "types": [
            "locality",
            "political"
          ]
        },
        {
          "long_name": "Salford",
          "short_name": "Salford",
          "types": [
            "postal_town"
          ]
        },
        {
          "long_name": "Greater Manchester",
          "short_name": "Gt Man",
          "types": [
            "administrative_area_level_2",
            "political"
          ]
        },
        {
          "long_name": "United Kingdom",
          "short_name": "GB",
          "types": [
            "country",
            "political"
          ]
        },
        {
          "long_name": "M5",
          "short_name": "M5",
          "types": [
            "postal_code_prefix",
            "postal_code"
          ]
        }
      ]
    }
  ];
});
