var assert = require('chai').assert;
var formatter = require('../../src/utilities/markdown-text-formatter');

describe('challenge-body-text-formatter', function() {
  describe('#format()', function() {
    it('Should format the text as markdown', function() {
      var text = '## Testing';
      var result = formatter.format(text);

      assert.equal(result, '<h2>Testing</h2>');
    });

    it('Should replace youtu.be video links with player controls', function() {
      var text = 'Testing https://youtu.be/JZQA08SlJnM Testing\n\nhttp://youtu.be/JZQA08SlJnN\n\nTesting';
      var result = formatter.format(text);

      assert.equal(result, '<p>Testing <div class="embed-responsive embed-responsive-16by9"><iframe src="https://www.youtube.com/embed/JZQA08SlJnM?rel=0" allowfullscreen></iframe></div> Testing</p>\n\n<p><div class="embed-responsive embed-responsive-16by9"><iframe src="https://www.youtube.com/embed/JZQA08SlJnN?rel=0" allowfullscreen></iframe></div></p>\n\n<p>Testing</p>');
    });

    it('Should replace YouTube video links with player controls', function() {
      var text = 'Testing\n\nhttps://www.youtube.com/watch?v=gRVjAtPip0Y&feature=youtu.be';
      var result = formatter.format(text);

      assert.equal(result, '<p>Testing</p>\n\n<p><div class="embed-responsive embed-responsive-16by9"><iframe src="https://www.youtube.com/embed/gRVjAtPip0Y?rel=0" allowfullscreen></iframe></div></p>');
    });
  });
});
