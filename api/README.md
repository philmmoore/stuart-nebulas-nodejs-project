# GymFit API
This is the API for GymFit.

# General Notes
* `POST` requests should be sent as `x-www-form-urlencoded` type.
* All successful requests will return a status of `200`.
* Bad/invalid parameters will result in a status of `400` with the errors in JSON.
* API availability can be checked by navigating to the root of the app, if the service is available status `200` should be returned.

## Data Conventions
For consistency please observe the following:
* Durations to be stored in milliseconds.
* Times to be UTC.
* Date format to be stored as UK format `DD/MM/YYYY`.
* Timestamps to be in ISO 8601 format as UTC `2015-11-02T00:00:00.000Z`.
* Weights to be stored in grams.
* Heights to be stored in millimeters.
* Distance to to be stored in millimeters.
* Phone numbers to validate against: must start with '07' and be 11 digits long.

## Errors
Errors will take the following format where `password` is a field in error and `Error_Field_Blank` is the error message. A field can have multiple error messages:

    {
      "errors": [
        [
          "password",
          "Error_Field_Blank"
        ]
      ]
    }

## Authentication
All requests to the API must be sent with the following header:

    x-auth-key: *secret key*

# V1

## Auth

### Login

    POST:             /v1/auth/login/

    Body fields:      emailOrUsername
                      password
                      deviceId                                        // some form of hardware identifier for auditing

    Success response: {
                        "id": 1,
                        "username": "philmmoore",
                        "firstName": "Philip",
                        "lastName": "Moore",
                        "isMale": true,                               // null if not set
                        "dateOfBirth": "09/01/1986",
                        "avatarUploaded": true                        // true if user uploaded an avatar
                        "location": {
                          "districtId": 6,                            // null if not set
                          "districtName": "Salford",                  // null if not set
                          "countyId": 53,                             // null if not set
                          "countyName": "Greater Manchester",         // null if not set
                          "regionId": 3,                              // null if not set
                          "regionName": "North West",                 // null if not set
                          "countryId": 1,                             // null if not set
                          "countryName": "Great Britain",             // null if not set
                          "countryCode": "gb"                         // null if not set
                        },
                        "height": 10500,                              // in mm, null if not set
                        "weight": 65500,                              // in grams, null if not set
                        "routineType": "Beginner"                     // null if not set,
                        "gym": "Virgin A",
                        "followers": [],
                        "following": [],
                        "clients": [],
                        "trainers": [],
                        "blocked": null,
                        "bio": "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
                        "avatarImage": "https://s3-eu-west-1.amazonaws.com/gymfit-development-avatars/26-170x170.png"
                      }

    Error responses:  Error_Field_Blank,
                      Error_Invalid_Username_Or_Password
                      
### Change Password
Passwords must be between 5 and 256 characters and also contain a number.

    POST:             /v1/auth/*userId*/change-password/

    Body fields:      oldPassword
                      newPassword
                      deviceId                                        // some form of hardware identifier for auditing

    Success response: {
                        "success": true
                      }

    Error responses:  Error_Field_Not_Int
                      Error_Field_Blank
                      Error_Invalid_Username_Or_Password
                      User_Invalid_Or_Current_Password_Incorrect
                      Error_Field_Must_Be_At_Least_5_Characters
                      Error_Field_Too_Long
                      Error_Password_Must_Contain_Numeric

### Reset Password

    POST:             /v1/auth/*userId*/reset-password/

    Success response: {
                        "email": "phil@wearehinge.com",
                        "key": "42028683-5c88-4982-9ccd-7abff54fd99e"
                      }

    Error responses:  Error_Field_Not_Int
                      Error_Field_Blank
                      User_Invalid

### Recover Username

    POST:             /v1/auth/*userId*/recover-username/

    Success response: {
                        "email": "phil@wearehinge.com",
                        "username": "philmmoore"
                      }

    Error responses:  Error_Field_Not_Int
                      Error_Field_Blank
                      User_Invalid

## Location

### Get Countries

    GET:              /v1/location/countries/

    Body fields:      

    Success response: [
                        {
                          "id": 1,
                          "name": "Great Britain",
                          "code": "gb",                               // country code
                          "isAvailable": true                         // true if GymFit is available in this country
                        },
                        ...
                      ]

    Error responses:

### Get Regions

    GET:              /v1/location/regions/*countryId*/

    Body fields:      

    Success response: [
                        {
                          "id": 3,
                          "name": "North West"
                        },
                        ...
                      ]

    Error responses:  Error_Field_Not_Int

### Get Counties

    GET:              /v1/location/counties/*regionId*/

    Body fields:      

    Success response: [
                        {
                          "id": 53,
                          "name": "Greater Manchester"
                        },
                        ...
                      ]

    Error responses:  Error_Field_Not_Int

### Get Districts

    GET:              /v1/location/districts/*countyId*/

    Body fields:      

    Success response: [
                        {
                          "id": 1,
                          "name": "Bolton"
                        },
                        ...
                      ]

    Error responses:  Error_Field_Not_Int

### Lookup Location
Returns the location based off latitude and longitude.

    GET:              /v1/location/lookup/:latlon/                    // (lat,lon) eg: /v1/location/lookup/53.4676260,-2.2813490/

    Body fields:      

    Success response: {                                               // The closest found location will be populated or all will be null
                        "district": {
                          "name": "Salford",
                          "id": 6
                        },
                        "county": null,
                        "region": null,
                        "country": null
                      }

    Error responses:  Error_Invalid_Lat_Lon

## Register

### Is Email Registered

    GET:              /v1/register/is-email-registered/*email*/

    Body fields:      

    Success response: {                                               
                        "isEmailRegistered": true,                  // true if registered
                        "userId" : 10                               // will only return userId if isUsernameRegistered = true
                      }

    Error responses:  Error_Field_Invalid_Email
                      Error_Field_Email_Is_Disposable

### Is Username Registered

    GET:              /v1/register/is-username-registered/*username*/

    Body fields:      

    Success response: {                                               
                        "isUsernameRegistered": true,               // true if registered
                        "userId" : 10                               // will only return userId if isUsernameRegistered = true
                      }

    Error responses:  Error_Field_Invalid_Username
                      Error_Field_Must_Be_At_Least_3_Characters

### Get Terms and Conditions

Returns Markdown formatted terms and conditions.

    GET:              /v1/register/terms-and-conditions/*countryId*/

    Body fields:      

    Success response: {
                        "firstBlock": "We would like to contact you by email, text message or with a personalised online message with information and offers of products and services (including those of our <a href=\"https://gymfit.com/gb/partners/\">GymFit Partners</a>) that we believe may interest you. <b>If you <u>do not want to receive information and offers</u> please tick the relevant boxes</b> (if you do not tick the boxes you will be consenting to receive information and offers as described. You can change your mind at any time):",
                        "option1": "By email",
                        "option2": "By text",
                        "option3": "Personalised online message",
                        "question1": "I DO NOT WANT information or offers available from GymFit",
                        "question2": "I DO NOT WANT GymFit to send me information or offers available from GymFit Partners",
                        "secondBlock": "<ul><li>I am over 16 years old</li><li>I have read and agree to the <a href=\"https://gymfit.com/gb/terms-and-conditions/\">GymFit Terms and Conditions</a></li><li>I have read and agree to my personal information being used as described in the <a href=\"https://gymfit.com/gb/privacy-policy/\">GymFit Privacy Policy</a> and have selected my preferences</li></ul>"
                      }

    Error responses:  Error_Field_Not_Int

### Register User

This must only be called after the user has accepted the Terms and Conditions.

    POST:             /v1/register/

    Body fields:      username                                        // must be at least 3 characters and meet [a-zA-Z0-9-]+
                      password                                        // must be at least 5 characters
                      firstName
                      lastName
                      dateOfBirth                                     // GymFit members must be at least 16 years old
                      email

    Success response: {
                        "id": 23
                      }

    Error responses:  Error_Field_Blank
                      Error_Field_Invalid_Username
                      Error_Field_Must_Be_At_Least_3_Characters       // for username
                      Error_Field_Invalid_Email
                      Error_Field_Email_Is_Disposable
                      Error_Field_Must_Be_At_Least_5_Characters       // for password
                      Error_Field_Invalid_Date
                      Error_Field_Date_cannot_Be_In_Future
                      Error_Field_Age_Cannot_Be_Greater_Than_130
                      Error_Field_Age_Too_Young                       // GymFit members have to be at least 16 years old
                      Email_Already_Registered
                      Username_Already_Registered

## User
### Set District

    POST:             /v1/user/*userId*/set-district/

    Body fields:      districtId

    Success response: {
                        "success": true
                      }

    Error responses:  User_Invalid
                      District_Invalid

### Set Weight

    POST:             /v1/user/*userId*/set-weight/

    Body fields:      weight

    Success response: {
                        "success": true
                      }

    Error responses:  User_Invalid
                      Error_Field_Not_Int
                      Error_Field_Weight_Less_Than_5_kg
                      Error_Field_Weight_Greater_Than_700_kg

### Set Height

    POST:             /v1/user/*userId*/set-height/

    Body fields:      height

    Success response: {
                        "success": true
                      }

    Error responses:  User_Invalid
                      Error_Field_Not_Int
                      Error_Field_Height_Less_Than_50_cm
                      Error_Field_Height_Greater_Than_300_cm

### Set Is Male/Female

    POST:             /v1/user/*userId*/set-is-male/

    Body fields:      isMale                                          // true/false

    Success response: {
                        "success": true
                      }

    Error responses:  User_Invalid
                      Error_Field_Blank

### Set Is Gym

    POST:             /v1/user/*userId*/set-is-gym/

    Body fields:      isGym                                          // true/false

    Success response: {
                        "success": true
                      }

    Error responses:  User_Invalid
                      Error_Field_Blank
                      
### Set Is Personal Trainer

    POST:             /v1/user/*userId*/set-is-personal-trainer/

    Body fields:      isPersonalTrainer                                          // true/false

    Success response: {
                        "success": true
                      }

    Error responses:  User_Invalid
                      Error_Field_Blank

### Set Mobile Number

    POST:             /v1/user/*userId*/set-mobile-number/

    Body fields:      mobileNumber                                    // must be 11 digits, starting with '07'

    Success response: {
                        "success": true
                      }

    Error responses:  User_Invalid
                      Error_Field_Blank
                      Error_Field_Invalid_Mobile_Number

### Set Gym

    POST:             /v1/user/*userId*/set-gym/

    Body fields:      gym                                             // name of gym

    Success response: {
                        "success": true
                      }

    Error responses:  User_Invalid

### Set Routine Type

    POST:             /v1/user/*userId*/set-routine-type/

    Body fields:      height

    Success response: {
                        "success": true
                      }

    Error responses:  User_Invalid
                      Error_Field_Not_Int
                      Invalid_Routine_Type_Id

### Set Privacy Preferences

    POST:             /v1/user/*userId*/set-privacy-preferences/

    Body fields:      gymFitEmailOptIn                                // true/false
                      partnersEmailOptIn                              // true/false
                      gymFitSMSOptIn                                  // true/false
                      partnersSMSOptIn                                // true/false
                      gymFitOnlineOptIn                               // true/false
                      partnersOnlineOptIn                             // true/false

    Success response: {
                        "success": true
                      }

    Error responses:  User_Invalid
                      Error_Field_Blank

### Get Activities

    GET:              /v1/user/*userId*/activities/

    Body fields:      

    Success response: [
                        {
                          "challengeId": 6,
                          "challengeName": "Bench Press",
                          "challengeGroupId": 12,
                          "challengeGroupName": "40kg",
                          "category": "Strength",
                          "categoryId": 1,
                          "measurement": "count",
                          "result": 461,
                          "worldCurrent": 2,
                          "worldPrevious": 20,
                          "worldTotal": 29,
                          "countryCurrent": 2,
                          "countryPrevious": 20,
                          "countryTotal": 29,
                          "regionCurrent": 1,
                          "regionPrevious": 19,
                          "regionTotal": 26,
                          "countyCurrent": 1,
                          "countyPrevious": 19,
                          "countyTotal": 26,
                          "districtCurrent": 1,
                          "districtPrevious": 19,
                          "districtTotal": 26,
                          "submitted": "2016-02-17T16:36:58.028325",
                          "evidenceImageUrl": null,
                          "evidenceVideoUrl": "https://gymfit-development-videos.s3.amazonaws.com/122.mp4"
                        },
                        ...
                      ]

    Error responses:  


### Send Push Notification

    POST:             /v1/user/*userId*/send-push-notification/

    Body fields:      title           // string | **optional** 
                      content         // string | **required** 
                      badge           // int | **optional** 
                      sound           // bool | **optional** 

    Success response: {
                        "success": true
                      }

    Error responses:  Error_Field_Not_Int
                      Error_Field_Blank

### Get Notifications

This is almost depreciated. it's original purpose was for site-generated notifications but it isn't really effective these days.

    GET:              /v1/user/*userId*/notifications/

    Body fields:      

    Success response: [
                        {
                          "id": 2,
                          "title": "Email Not Verified",
                          "description": "Check your email account for...",
                          "isUserDismissable": true,                  // if true, send request to acknowledge to remove this using the id of the notificaiton.
                          "type": "warning"
                        },
                        ...
                      ]

    Error responses:  User_Invalid

### Acknowledge Notification
If the notification was flagged as being user dismissible then it can be acknowledged using it's ID.

    POST:             /v1/user/*userId*/notifications/*notificationId*/acknowledge/

    Body fields:      

    Success response: {
                        "success": true
                      }

    Error responses:  Error_Field_Not_Int
                      Notification_Or_User_Id_Invalid

### Get Profile

    GET:             /v1/user/profile/*username*/

    Body fields:      

    Success response: {} if no user found or
                      {
                        "id": 26,
                        "username": "philmmoore",
                        "firstName": "Philip",
                        "lastName": "Moore",
                        "isMale": true,
                        "dateOfBirth": "1986-01-09",
                        "isPersonalTrainer": true,
                        "location": {
                          "districtId": 6,
                          "districtName": "Salford",
                          "countyId": 53,
                          "countyName": "Greater Manchester",
                          "regionId": 3,
                          "regionName": "North West England",
                          "countryId": 1,
                          "countryName": "Great Britain",
                          "countryCode": "gb"
                        },
                        "height": 10500,
                        "weight": 56600,
                        "routineType": "Beginner",
                        "gym": "Virgin A",
                        "followers": [
                          {
                            "id": 8,
                            "username": "arthur.brad",
                            "firstName": "Arthur",
                            "lastName": "Brad",
                            "avatarImage": "https://s3-eu-west-1.amazonaws.com/gymfit-development-avatars/0.png"
                          }
                        ],
                        "following": [
                          {
                            "id": 1,
                            "username": "roscoe.babar",
                            "firstName": "Roscoe",
                            "lastName": "Babar",
                            "avatarImage": "https://s3-eu-west-1.amazonaws.com/gymfit-development-avatars/1.png"
                          }
                        ],
                        "clients": [
                          {
                            "id": 1,
                            "username": "roscoe.babar",
                            "firstName": "Roscoe",
                            "lastName": "Babar",
                            "avatarImage": "https://s3-eu-west-1.amazonaws.com/gymfit-development-avatars/0.png"
                          }
                        ],
                        "trainers": [],
                        "blocked": null,
                        "bio": "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
                        "avatarImage": "https://s3-eu-west-1.amazonaws.com/gymfit-development-avatars/26-170x170.png"
                      }

    Error responses:  Error_Username_Invalid
                      Error_Field_Must_Be_At_Least_3_Characters

### Get Profile
Given a query this will search users' first names, last names and usernames.

    GET:             /v1/user/search/*query*/

    Body fields:      

    Success response: [
                        {
                          "id": 30,
                          "username": "philmmooressdd",
                          "firstName": "sjdklf",
                          "lastName": "sldkfjsdlfjk",
                          "isPersonalTrainer": false,
                          "isGym": false,
                          "bio": "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
                          "avatarImage": null
                        },
                        {
                          "id": 26,
                          "username": "philmmoore",
                          "firstName": "Philip",
                          "lastName": "Moore",
                          "isPersonalTrainer": false,
                          "isGym": false,
                          "bio": "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
                          "avatarImage": "https://s3-eu-west-1.amazonaws.com/gymfit-development-avatars/undefined-170x170.png"
                        }
                      ]

    Error responses:  
      
### Follow user

    GET:             /v1/user/*userId*/follow/*userIdToFollow*/

    Body fields:      

    Success response: {
                        "success": true
                      }

    Error responses:  Error_Field_Not_Int
                      User_Invalid
### Unfollow user

    GET:             /v1/user/*userId*/unfollow/*userIdToUnfollow*/

    Body fields:      

    Success response: {
                        "success": true
                      }

    Error responses:  Error_Field_Not_Int
    
### Set client (for personal trainers)

    GET:             /v1/user/*userId*/set-client/*clientUserId*/

    Body fields:      

    Success response: {
                        "success": true
                      }

    Error responses:  Error_Field_Not_Int
                      User_Invalid

### Unset client (for personal trainers)

    GET:             /v1/user/*userId*/unset-client/*clientUserId*/

    Body fields:      

    Success response: {
                        "success": true
                      }

    Error responses:  Error_Field_Not_Int
    
### Set first name

    POST:             /v1/user/*userId*/first-name/

    Body fields:      first-name

    Success response: {
                        "success": true
                      }

    Error responses:  Error_Field_Not_Int
                      User_Invalid
                      Error_Field_Blank
                      
### Set last name

    POST:             /v1/user/*userId*/last-name/

    Body fields:      last-name

    Success response: {
                        "success": true
                      }

    Error responses:  Error_Field_Not_Int
                      User_Invalid
                      
### Set bio

    POST:             /v1/user/*userId*/bio/

    Body fields:      bio                                             // text for bio

    Success response: {
                        "success": true
                      }

    Error responses:  Error_Field_Not_Int
                      User_Invalid
                      
### Get challenge progress chart
Returns all results submitted by a user for a challenge and challenge group.

    GET:              /user/*userId*/challenge/*challengeId*/progress/*challengeGroupId*/

    Body fields:      

    Success response: {
                        "challengeName": "Bench Press",
                        "challengeGroupName": "20kg",
                        "measurement": "count",
                        "results": [
                          {
                            "submitted": "2016-02-22T10:24:01.913242",
                            "result": 5
                          },
                          {
                            "submitted": "2016-02-24T12:14:06.941943",
                            "result": 6
                          },
                          ...
                        ]
                      }

    Error responses:  Error_Field_Not_Int
                      Not_Found

### Get user reviews
Returns all reviews for a particular user

    GET:              /user/*userId*/reviews/

    Query params:     limit           // int | **optional** 

    Success response: [
                        {
                          "id": 7,
                          "reviewerUserId": 239,
                          "title": "Testing",
                          "body": "Testing 123",
                          "rating": 3,
                          "created": "2016-08-19T12:10:11.677951",
                          "lastUpdated": "2016-08-19T12:10:11.677951"
                        },
                        {
                          "id": 3,
                          "reviewerUserId": 236,
                          "title": "Test 2",
                          "body": "Test 2",
                          "rating": 4,
                          "created": "2016-08-19T09:55:39.497903",
                          "lastUpdated": "2016-08-19T09:55:39.497903"
                        }
                      ]

    Error responses:  

### Get user review
Returns a specified review only

    GET:              /user/*userId*/reviews/*reviewId*/

    Body fields:      

    Success response: {
                        "id": 7,
                        "reviewerUserId": 239,
                        "title": "Testing",
                        "body": "Testing 123",
                        "rating": 3,
                        "created": "2016-08-19T12:10:11.677951",
                        "lastUpdated": "2016-08-19T12:10:11.677951"
                      }

    Error responses:  

### Submit a review
Submits or updates an existing review

    POST:              /user/*userId*/reviews/

    Body fields:      reviewerUserId           // int | **requird** | The id of the user submitting the review
                      title                    // string | **required** 
                      body                     // string | **required** 
                      rating                   // int | **required** | Rating 1 - 5 


    Success response: {
                        "id": 7,
                        "reviewerUserId": 239,
                        "title": "Testing",
                        "body": "Testing 123",
                        "rating": 3,
                        "created": "2016-08-19T12:10:11.677951",
                        "lastUpdated": "2016-08-19T12:10:11.677951"
                      }

    Error responses:  

### Delete a review
Deletes the given review

    DELETE:              /user/*userId*/reviews/*reviewId*/

    Body fields:

    Success response: 1 or 0

    Error responses:  

                      
### Get featured users
Returns all currently featured users

    GET:              /user/featured/

    Body fields:      

    Success response: [
                        {
                          "id": 9,
                          "username": "philmmoore",
                          "firstName": "Philip",
                          "lastName": "Moore",
                          "isPersonalTrainer": true,
                          "isGym": false,
                          "bio": null,
                          "avatarImage": "https://s3-eu-west-1.amazonaws.com/gymfit-staging-avatars/9-170x170.png"
                        },
                        {
                          "id": 200,
                          "username": "bubbles",
                          "firstName": "Sarah-Jane",
                          "lastName": "Taylor",
                          "isPersonalTrainer": true,
                          "isGym": false,
                          "bio": null,
                          "avatarImage": "https://s3-eu-west-1.amazonaws.com/gymfit-staging-avatars/200-170x170.png"
                        },  ...
                      ]

    Error responses:  
                    


## Challenge
### Get Challenge Groups
This returns an array of challenge group ids and names, along with (optionally) the last group the user entered.

    POST:             /v1/challenge/groups/

    Body fields:      challengeId
                      userId                                          // optional

    Success response: {
                        "groups": [
                          {
                            "id": 7,
                            "name": "10kg"
                          },
                          {
                            "id": 9,
                            "name": "20kg"
                          },
                          ...
                        ],
                        "selectedGroup": 12                          // if userId specified and user previously submitted a result for this group
                      }

    Error responses:  Error_Field_Not_Int

### Get Challenge Categories

    GET:             /v1/challenge/categories/

    Body fields:      

    Success response: [
                        {
                          "name": "Strength",
                          "id": 1
                        },
                        {
                          "name": "Cardio",
                          "id": 2
                        },
                        {
                          "name": "Ultimate",
                          "id": 3
                        }
                      ]

    Error responses:  

### Get Challenges
This returns all challenges

    GET:              /v1/challenges/

    Body fields:      

    Success response: [
                        {
                          "groups": [
                            {
                              "id": 5,                            // This is the challenge group Id
                              "name": "3m"
                            }
                          ],
                          "id": 1,
                          "name": "Press Up",
                          "category": "Strength",
                          "categoryId": 1,
                          "type": "How many within a set time",
                          "measurement": "count",
                          "bodyCopy": "##Lorem Ipsum\ndolor sit...",
                          "submissionMessage": "Duis aute irure dolor in reprehenderit...",
                          "comingSoon": "false"
                        },
                        ...
                      ]

    Error responses:  

### Get Challenges By Category Id
This returns all challenges for the category id

    GET:              /v1/challenge/categories/*categoryId*/

    Body fields:      

    Success response: [] if no challenges found/category id invalid, or
                      [
                        {
                          "groups": [
                            {
                              "id": 5,                            // This is the challenge group Id
                              "name": "3m"
                            }
                          ],
                          "id": 1,
                          "name": "Press Up",
                          "category": "Strength",
                          "categoryId": 1,
                          "type": "How many within a set time",
                          "measurement": "count",
                          "bodyCopy": "##Lorem Ipsum\ndolor sit...",
                          "submissionMessage": "Duis aute irure dolor in reprehenderit...",
                          "comingSoon": "false"
                        },
                        ...
                      ]

    Error responses:  

## Result
### Save
Returns the result id for the submitted result. This ID should be used when uploading videos/images.

    POST:             /v1/result/save/

    Body fields:      challengeId
                      challengeGroupId
                      userId
                      result

    Success response: {
                        "resultId": 63
                      }

    Error responses:  Error_Field_Not_Int
                      Challenge_Group_Invalid
                      Challenge_Invalid

### Get Result
Given a result id return the result details including evidence.

    POST:             /v1/result/*resultId*/

    Body fields:      

    Success response: {
                        "result": 109,
                        "submitted": "2016-01-20T12:20:10.473313",
                        "challengeId": 6,
                        "challengeGroupId": 12,
                        "userId": 28,
                        "username": "philmmoore",
                        "firstName": "Philip",
                        "lastName": "Moore",
                        "bio": "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
                        "worldCurrent": 15,
                        "worldPrevious": null,
                        "worldTotal": 21,
                        "countryCurrent": 15,
                        "countryPrevious": null,
                        "countryTotal": 21,
                        "regionCurrent": 1,
                        "regionPrevious": null,
                        "regionTotal": 1,
                        "countyCurrent": 1,
                        "countyPrevious": null,
                        "countyTotal": 1,
                        "districtCurrent": 1,
                        "districtPrevious": null,
                        "districtTotal": 1,
                        "avatarImage": "https://s3-eu-west-1.amazonaws.com/gymfit-development-avatars/28-170x170.png",
                        "evidenceImageUrl": null,
                        "evidenceImageThumbUrl": null,
                        "evidenceVideoUrl": "https://gymfit-development-videos.s3.amazonaws.com/122.mp4"
                        "evidenceVideoThumbUrl": "https://gymfit-development-videos.s3.amazonaws.com/122.jpg"
                      }

    Error responses:  Error_Field_Not_Int

## Leaderboard
### Get Leaderboard

    POST:             /v1/leaderboard/

    Body fields:      challengeId
                      challengeGroupId
                      districtId                                      // optional
                      countyId                                        // optional
                      regionId                                        // optional
                      countryId                                       // optional
                      resultsPerPage
                      page
                      isMale                                          // optional filter (true/false)
                      ageMin                                          // optional filter
                      ageMax                                          // optional filter
                      weightMin                                       // optional filter
                      weightMax                                       // optional filter
                      onlyShowValidated                               // optional filter
                      userId                                          // optional for highlighting user likes/flags

    Success response: {
                        "results": [
                          {
                            "rank": 1,
                            "userId": 9,
                            "username": "jochim.epiktetos",
                            "avatarImage": "https://s3-eu-west-1.amazonaws.com/gymfit-development-avatars/26-170x170.png",
                            "firstName": "Jochim",
                            "lastName": "Epiktetos",
                            "bio": "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
                            "result": 457,
                            "evidenceImageUrl": "https://s3-eu-west-1.amazonaws.com/gymfit-development-evidence-photos/34.png",
                            "evidenceImageThumbUrl": "https://s3-eu-west-1.amazonaws.com/gymfit-development-evidence-photos/34-sm.png",
                            "evidenceVideoUrl": null,
                            "evidenceVideoThumbUrl": null,
                            "updateId": "c7589346-5d0a-432a-85ec-be4d38cdf499",
                            "likes": 1,
                            "flags": 0,
                            "userLikes": true,                        // true if userId already likes this update,
                            "userFlagged": false,                     // true if userId already flagged this update
                            "lastRank": 1
                          },
                          {
                            "rank": 2,
                            "userId": 2,
                            "username": "emeka.milan",
                            "avatarImage": null,
                            "firstName": "Emeka",
                            "lastName": "Milan",
                            "bio": "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
                            "result": 435,
                            "evidenceImageUrl": null,
                            "evidenceImageThumbUrl": null,
                            "evidenceVideoUrl": "https://gymfit-development-videos.s3.amazonaws.com/34.mp4",
                            "evidenceVideoThumbUrl": "https://gymfit-development-videos.s3.amazonaws.com/34-sm.mp4",
                            "updateId": "c7589346-5d0a-432a-85ec-be4d38cdf499",
                            "likes": 1,
                            "flags": 0,
                            "userLikes": false,                       // true if userId already likes this update,
                            "userFlagged": true,                      // true if userId already flagged this update
                            "lastRank": 2
                          },
                          ...
                        ],
                        "numberOfPages": 3,
                        "measurement": "count",                       // this is the measurement for the challenge type: count, distance, time are the available options
                        "lastUpdated": "2015-11-04T00:00:00.000Z",
                        "nextUpdate": "2015-11-11T00:00:00.000Z"
                      }

                      or if no leaderboard data:                      
                      {
                        "results": null,
                        "numberOfPages": 0,
                        "lastUpdated": "2015-11-04T00:00:00.000Z",
                        "nextUpdate": "2015-11-11T00:00:00.000Z"
                      }

    Error responses:  Error_Field_Not_Int
                      Error_Field_Age_Less_Than_16
                      Error_Field_Age_Greater_Than_130
                      Error_Field_Age_Max_Greater_Than_Min
                      Error_Field_Weight_Less_Than_5_kg
                      Error_Field_Weight_Greater_Than_700_kg
                      Error_Field_Weight_Max_Greater_Than_Min

### Get Leaderboard Rankings for user Ids
This returns an array of user IDs. The index of the array is the position within the leaderboard. This is useful to work out which page a user's result is on before requesting the leaderboard.

    POST:             /v1/leaderboard/user-position/

    Body fields:      challengeId
                      challengeGroupId
                      districtId                                      // optional
                      countyId                                        // optional
                      regionId                                        // optional
                      countryId                                       // optional
                      isMale                                          // optional filter
                      ageMin                                          // optional filter
                      ageMax                                          // optional filter
                      weightMin                                       // optional filter
                      weightMax                                       // optional filter
                      onlyShowValidated                               // optional filter

    Success response: [
                        9,                                            // this user ID is rank 1
                        2,
                        25,
                        10,
                        ...,
                        18                                            // this user ID is the last position
                      ]
                      or [] if no leaderboard data

    Error responses:  Error_Field_Not_Int
                      Error_Field_Age_Less_Than_16
                      Error_Field_Age_Greater_Than_130
                      Error_Field_Age_Max_Greater_Than_Min
                      Error_Field_Weight_Less_Than_5_kg
                      Error_Field_Weight_Greater_Than_700_kg
                      Error_Field_Weight_Max_Greater_Than_Min
## Fitness

### Get Fitness Types

    GET:              /v1/fitness-types/

    Body fields:      

    Success response: [
                        {
                          "id": 1,
                          "name": "Beginner"
                        },
                        {
                          "id": 2,
                          "name": "Intermediate"
                        },
                        {
                          "id": 3,
                          "name": "Serious"
                        },
                        {
                          "id": 4,
                          "name": "Fitness Freak"
                        }
                      ]

    Error responses: 
    
## Blog

### Get all Posts

    POST:              /v1/blog/

    Body fields:      countryId                                       // 1 for UK
                      postsPerPage
                      pageNumber

    Success response: {
                        "numberOfPages": 1,
                        "numberOfPosts": 3,
                        "posts": [
                          {
                            "id": 4,
                            "name": "Test",
                            "summary": "Testing",
                            "published": "2016-01-04T17:46:17"
                          },
                          {
                            "id": 1,
                            "name": "Test Post 1",
                            "summary": "TEST",
                            "published": "2016-01-04T12:23:27.46748"
                          },
                          {
                            "id": 2,
                            "name": "Test Post 2",
                            "summary": "Lorem ipsum dolor sit amet.",
                            "published": "2016-01-04T12:23:27.46748"
                          }
                        ]
                      }

    Error responses:  Error_Field_Not_Int
    
### Get Post

    GET:              /v1/blog/*postId*/

    Body fields:      

    Success response: {
                        "name": "Test Post",
                        "bodyCopy": "<p>Lorem ipsum dolor sit amet.</p>",
                        "published": "2016-01-04T12:23:27.46748"
                      }

    Error responses:  Error_Field_Not_Int

## Page

### Get Page

    GET:              /v1/page/*name*/

    Body fields:      

    Success response: {} if no page found or
                      {
                        "name": "About",
                        "bodyCopy": "<p>Lorem ipsum dolor sit amet</p>"
                      }

    Error responses:  Error_Field_Blank
    
## Social

Note that the notifications mechanism has been depreciated in favor of updates.

### Get Notifications **(DEPRECATED)**
Returns different JSON responses based on the notification `type`:

Available options are:

* result
* result-evidence


    POST:              /v1/social/*userId*/notifications/

    Body fields:      from                                            // the timestamp to show notifications up to e.g. 2016-01-20T11:49:07.051857
                      numberToReturn                                  // the number of notifications to return

    Success response: [] if no notifications found or
                      [
                        {
                          "type": "result",
                          "resultId": 5,
                          "result": 21,
                          "userId": 5,
                          "username": "avitus.olujimi",
                          "firstName": "Avitus",
                          "lastName": "Olujimi",
                          "avatarUploaded": null,
                          "challengeId": 6,
                          "challengeName": "Bench Press",
                          "challengeGroupId": 12,
                          "challengeGroupName": "40kg",
                          "challengeCategory": "Strength",
                          "challengeCategoryId": 1,
                          "measurement": "count",
                          "notificationId": 257,
                          "notificationTimestamp": "2016-01-19T12:45:19.887987",
                          "avatar": "https://s3-eu-west-1.amazonaws.com/gymfit-development-avatars/0.png"
                        },
                        {
                          "type": "result-evidence",
                          "resultId": 116,
                          "result": 105,
                          "userId": 28,
                          "username": "philmmoore",
                          "firstName": "Philip",
                          "lastName": "Moore",
                          "avatarUploaded": "1",
                          "challengeId": 6,
                          "challengeName": "Bench Press",
                          "challengeGroupId": 12,
                          "challengeGroupName": "40kg",
                          "challengeCategory": "Strength",
                          "challengeCategoryId": 1,
                          "measurement": "count",
                          "videoValidated": null,
                          "evidenceVideoUrl": null,
                          "evidenceImageUrl": "https://s3-eu-west-1.amazonaws.com/gymfit-development-evidence-photos/116.png",
                          "notificationId": 172,
                          "notificationTimestamp": "2016-01-19T12:23:58.120755",
                          "avatar": "https://s3-eu-west-1.amazonaws.com/gymfit-development-avatars/28.png"
                        }
                        ,...
                      ]

    Error responses:  Error_Field_Invalid_Date

### Get Global Updates

Retrieve all updates globally.

    POST:              /v1/social/updates/

    Body fields:      from                                            // the timestamp to show notifications up to e.g. 2016-01-20T11:49:07.051857
                      numberToReturn                                  // the number of notifications to return
                      tag                                             // text that searches the body field for the given tag or text (optional)
                      userId                                          // (optional)


    Success response: [] if no updates found or
                      [
                        {
                          "id": "e81fa250-194c-4a62-8ed7-085999bff558",
                          "created": "2016-02-17T15:24:02.213754",
                          "userId": 28,
                          "body": "Test #Hashtag  @Tag sdfj ksdlfds sf",
                          "username": "philmmoore",
                          "firstName": "Philip",
                          "lastName": "Moore",
                          "isPersonalTrainer": false,
                          "isGym": false,
                          "bio": "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
                          "likes": 0,
                          "flags": 0,
                          "result": {},
                          "videos": [
                            {
                              "videoUrl": "https://gymfit-development-misc-videos-to-process.s3.amazonaws.com/55e9fc4e-3e10-4432-ac69-05a319ef3ac5.mp4",
                              "placeholderImageUrl": "https://gymfit-development-misc-videos-to-process.s3.amazonaws.com/55e9fc4e-3e10-4432-ac69-05a319ef3ac5.jpg"
                            }
                          ],
                          "images": [
                            {
                              "imageUrl": "https://gymfit-development-social-photos.s3.amazonaws.com/55e9fc4e-3e10-4432-ac69-05a319ef3ac5.png",
                              "imageThumbUrl": "https://gymfit-development-social-photos.s3.amazonaws.com/55e9fc4e-3e10-4432-ac69-05a319ef3ac5-sm.png"
                            }
                          ],
                          "tags": [
                            "@Tag",
                            "#Hashtag"
                          ],
                          "userLikes": true,                          // true if userId already likes this update
                          "userFlagged": false,                       // true if userId already flagged this update
                          "avatar": "https://s3-eu-west-1.amazonaws.com/gymfit-development-avatars/28-170x170.png"
                        },
                        ,...
                        {
                          "id": "58139b51-d258-413e-88b4-3505cd507f1d",
                          "created": "2016-02-17T16:36:58.028325",
                          "userId": 28,
                          "body": null,
                          "username": "philmmoore",
                          "firstName": "Philip",
                          "lastName": "Moore",
                          "isPersonalTrainer": false,
                          "isGym": false,
                          "bio": "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
                          "likes": 0,
                          "flags": 0,
                          "result": {
                            "id": 195,
                            "result": 102,
                            "submitted": "2016-02-17T16:36:58.028325",
                            "challengeId": 6,
                            "challengeName": "Bench Press",
                            "challengeGroupId": 12,
                            "challengeGroupName": "40kg",
                            "category": "Strength",
                            "categoryId": 1,
                            "measurement": "count",
                            "evidenceVideoUrl": null,
                            "evidenceVideoThumbUrl": null,
                            "evidenceImageUrl": "https://s3-eu-west-1.amazonaws.com/gymfit-development-evidence-photos/62.png",
                            "evidenceImageThumbUrl": "https://s3-eu-west-1.amazonaws.com/gymfit-development-evidence-photos/62-sm.png"
                          },
                          "videos": [],
                          "images": [],
                          "tags": [],
                          "userLikes": true,                          // true if userId already likes this update
                          "userFlagged": false,                       // true if userId already flagged this update
                          "avatar": "https://s3-eu-west-1.amazonaws.com/gymfit-development-avatars/28-170x170.png"
                        },
                      ]

    Error responses:  Error_Field_Invalid_Date
                      Error_Field_Blank
                      Error_Field_Not_Int

### Get User's Updates

Retrieve updates for a user. This will return the user's updates as well as any from those the user is following.

    POST:             /v1/social/*userId*/updates/

    Body fields:      from                                            // the timestamp to show notifications up to e.g. 2016-01-20T11:49:07.051857
                      numberToReturn                                  // the number of notifications to return
                      showOnlyMine                                    // true/false, optional. If true then updates are only those from the user
                      ignoreMine                                    // true/false, optional. If true then updates will not include those from the user, just following + clients

    Success response: [] if no updates found or
                      [
                        {
                          "id": "e81fa250-194c-4a62-8ed7-085999bff558", // update Id
                          "created": "2016-02-17T15:24:02.213754",
                          "userId": 28,
                          "body": "Test #Hashtag  @Tag sdfj ksdlfds sf",
                          "username": "philmmoore",
                          "firstName": "Philip",
                          "lastName": "Moore",
                          "isPersonalTrainer": false,
                          "isGym": false,
                          "bio": "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
                          "likes": 0,
                          "flags": 0,
                          "result": {},                               // if a result is present this will have fields
                          "videos": [
                            {
                              "videoUrl": "https://gymfit-development-misc-videos-to-process.s3.amazonaws.com/55e9fc4e-3e10-4432-ac69-05a319ef3ac5.mp4",
                              "placeholderImageUrl": "https://gymfit-development-misc-videos-to-process.s3.amazonaws.com/55e9fc4e-3e10-4432-ac69-05a319ef3ac5.jpg"
                            }
                          ],
                          "images": [
                            {
                              "imageUrl": "https://gymfit-development-social-photos.s3.amazonaws.com/55e9fc4e-3e10-4432-ac69-05a319ef3ac5.png",
                              "imageThumbUrl": "https://gymfit-development-social-photos.s3.amazonaws.com/55e9fc4e-3e10-4432-ac69-05a319ef3ac5-sm.png"
                            }
                          ],
                          "tags": [
                            "@Tag",
                            "#Hashtag"
                          ],
                          "userLikes": true,                          // true if userId already likes this update
                          "userFlagged": false,                       // true if userId already flagged this update
                          "avatar": "https://s3-eu-west-1.amazonaws.com/gymfit-development-avatars/28-170x170.png"
                        },
                        ,...
                        {
                          "id": "58139b51-d258-413e-88b4-3505cd507f1d",
                          "created": "2016-02-17T16:36:58.028325",
                          "userId": 28,
                          "body": null,
                          "username": "philmmoore",
                          "firstName": "Philip",
                          "lastName": "Moore",
                          "isPersonalTrainer": false,
                          "isGym": false,
                          "bio": "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
                          "likes": 0,
                          "flags": 0,
                          "result": {
                            "id": 195,
                            "result": 102,
                            "submitted": "2016-02-17T16:36:58.028325",
                            "challengeId": 6,
                            "challengeName": "Bench Press",
                            "challengeGroupId": 12,
                            "challengeGroupName": "40kg",
                            "category": "Strength",
                            "categoryId": 1,
                            "measurement": "count",
                            "evidenceVideoUrl": null,
                            "evidenceVideoThumbUrl": null,
                            "evidenceImageUrl": "https://s3-eu-west-1.amazonaws.com/gymfit-development-evidence-photos/195.png",
                            "evidenceImageThumbUrl": "https://s3-eu-west-1.amazonaws.com/gymfit-development-evidence-photos/195-sm.png"
                          },
                          "videos": [],
                          "images": [],
                          "tags": [],
                          "userLikes": true,                          // true if userId already likes this update
                          "userFlagged": false,                       // true if userId already flagged this update
                          "avatar": "https://s3-eu-west-1.amazonaws.com/gymfit-development-avatars/28-170x170.png"
                        },
                      ]

    Error responses:  Error_Field_Invalid_Date
                      Error_Field_Blank
                      Error_Field_Not_Int
                      
### Post a new Update

    POST:             /v1/social/*userId*/update/new/

    Body fields:      body                                            // optional
                      tags                                            // optional, comma-separated list of tags

    Success response: {
                        "id": "6ef69b4f-0699-4b31-bd99-f1bd1271ed65"    // update Id
                      }

    Error responses:  Error_Field_Blank
                      Error_Field_Not_Int
                      Error_Field_Not_Valid_UUID

### Like an Update

    POST:             /v1/social/update/*updateId*/like/

    Body fields:      userId                                            // user Id

    Success response: {
                        "likes": 19,
                        "flags": 4
                      }

    Error responses:  Error_Field_Blank
                      Error_Field_Not_Int
                      Error_Field_Not_Valid_UUID
                      Not_Found
                      
### Unlike an Update

    POST:             /v1/social/update/*updateId*/unlike/

    Body fields:      userId                                            // user Id

    Success response: {
                        "likes": 18,
                        "flags": 4
                      }

    Error responses:  Error_Field_Blank
                      Error_Field_Not_Int
                      Error_Field_Not_Valid_UUID
                      
### Flag an Update

    POST:             /v1/social/update/*updateId*/flag/

    Body fields:      userId                                            // user Id

    Success response: {
                        "likes": 18,
                        "flags": 5
                      }

    Error responses:  Error_Field_Blank
                      Error_Field_Not_Int
                      Error_Field_Not_Valid_UUID
                      Not_Found
                      
### Unflag an Update

    POST:             /v1/social/update/*updateId*/unflag/

    Body fields:      userId                                            // user Id

    Success response: {
                        "likes": 18,
                        "flags": 4
                      }

    Error responses:  Error_Field_Blank
                      Error_Field_Not_Int
                      Error_Field_Not_Valid_UUID
                      
### Attach an Image to an Update

    POST:             /v1/social/update/*updateId*/attach/image/

    Body fields:      userId                                            // user Id

    Success response: {
                        "filename": "53091148-695e-4419-9a29-cdd85604b527"
                      }

    Error responses:  Error_Field_Blank
                      Error_Field_Not_Int
                      Error_Field_Not_Valid_UUID
                      Not_Authorised                                  // if this is not the user's update
                      
### Attach a Video to an Update

    POST:             /v1/social/update/*updateId*/attach/video/

    Body fields:      userId                                            // user Id

    Success response: {
                        "filename": "843afb7a-2356-4f50-a1fa-5831058b2da3"
                      }

    Error responses:  Error_Field_Blank
                      Error_Field_Not_Int
                      Error_Field_Not_Valid_UUID
                      Not_Authorised                                  // if this is not the user's update
                      
                      
### Search (DEPRECATED)

use: /v1/social/updates/

Search will search the body of the update for example it could be used for searching for hashtags

    POST:             /v1/social/update/search/

    Body fields:      query                                          
                      userId                                          // optional

    Success response: [] if no updates found or
                      [
                        {
                          "id": "e81fa250-194c-4a62-8ed7-085999bff558", // update Id
                          "created": "2016-02-17T15:24:02.213754",
                          "userId": 28,
                          "body": "Test #Hashtag  @Tag sdfj ksdlfds sf",
                          "username": "philmmoore",
                          "firstName": "Philip",
                          "lastName": "Moore",
                          "bio": "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
                          "likes": 0,
                          "flags": 0,
                          "result": {},                               // if a result is present this will have fields
                          "videos": [
                            {
                              "videoUrl": "https://gymfit-development-misc-videos-to-process.s3.amazonaws.com/55e9fc4e-3e10-4432-ac69-05a319ef3ac5.mp4",
                              "placeholderImageUrl": "https://gymfit-development-misc-videos-to-process.s3.amazonaws.com/55e9fc4e-3e10-4432-ac69-05a319ef3ac5.jpg"
                            }
                          ],
                          "images": [
                            {
                              "imageUrl": "https://gymfit-development-social-photos.s3.amazonaws.com/55e9fc4e-3e10-4432-ac69-05a319ef3ac5.png",
                              "imageThumbUrl": "https://gymfit-development-social-photos.s3.amazonaws.com/55e9fc4e-3e10-4432-ac69-05a319ef3ac5-sm.png"
                            }
                          ],
                          "tags": [
                            "@Tag",
                            "#Hashtag"
                          ],
                          "userLikes": true,                          // true if userId already likes this update
                          "userFlagged": false,                       // true if userId already flagged this update
                          "avatar": "https://s3-eu-west-1.amazonaws.com/gymfit-development-avatars/28-170x170.png"
                        },
                        ,...
                        {
                          "id": "58139b51-d258-413e-88b4-3505cd507f1d",
                          "created": "2016-02-17T16:36:58.028325",
                          "userId": 28,
                          "body": null,
                          "username": "philmmoore",
                          "firstName": "Philip",
                          "lastName": "Moore",
                          "bio": "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
                          "likes": 0,
                          "flags": 0,
                          "result": {
                            "id": 195,
                            "result": 102,
                            "submitted": "2016-02-17T16:36:58.028325",
                            "challengeId": 6,
                            "challengeName": "Bench Press",
                            "challengeGroupId": 12,
                            "challengeGroupName": "40kg",
                            "category": "Strength",
                            "categoryId": 1,
                            "measurement": "count",
                            "evidenceVideoUrl": null,
                            "evidenceVideoThumbUrl": null,
                            "evidenceImageUrl": "https://s3-eu-west-1.amazonaws.com/gymfit-development-evidence-photos/195.png",
                            "evidenceImageThumbUrl": "https://s3-eu-west-1.amazonaws.com/gymfit-development-evidence-photos/195-sm.png"
                          },
                          "videos": [],
                          "images": [],
                          "tags": [],
                          "userLikes": true,                          // true if userId already likes this update
                          "userFlagged": false,                       // true if userId already flagged this update
                          "avatar": "https://s3-eu-west-1.amazonaws.com/gymfit-development-avatars/28-170x170.png"
                        },
                      ]

    Error responses:  Error_Field_Invalid_Date
                      Error_Field_Blank
                      Error_Field_Not_Int


### Near Me

Near me is an end point for returning users near a given latitude and longitude

    POST:             /v1/near-me/

    Body fields:      latitude                                            // numeric | *required*
                      longitude                                           // numeric | *required*
                      radius                                              // numeric | **optional** | radius in miles (defaults to 10)
                      isPersonalTrainer                                   // bool    | **optional** 
                      isGym                                               // bool    | **optional**  
                      userId                                              // numeric | **optional** | will exclude the user with Id i.e. for getting near but excluding self
                      showFollowing                                       // bool    | **optional** | if combined with userId you can choose to show followed users or not

    Success response: [] if no results
                      [
                        {
                          "id": 7,
                          "username": "yoyoyo",
                          "firstName": "James",
                          "lastName": "Wolstenholme",
                          "isMale": "1",
                          "isPersonalTrainer": false,
                          "isGym": true,
                          "bio": null,
                          "district": "Manchester",
                          "distance": 181.152134740113,
                          "avatarImage": null
                        },
                        {
                          "id": 120,
                          "username": "qwerty",
                          "firstName": "Phili",
                          "lastName": "Fff",
                          "isMale": "0",
                          "isPersonalTrainer": false,
                          "isGym": true,
                          "bio": null,
                          "district": "Belfast",
                          "distance": 317.249833873011,
                          "avatarImage": null
                        },
                        {
                          "id": 263,
                          "username": "search7",
                          "firstName": "Search7",
                          "lastName": "",
                          "isMale": "0",
                          "isPersonalTrainer": false,
                          "isGym": true,
                          "bio": null,
                          "district": "Belfast",
                          "distance": 317.249833873011,
                          "avatarImage": null
                        }
                      ]

    Error responses:  Error_Field_Blank
                      Error_Field_Not_Int