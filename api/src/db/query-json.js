var Promise = require('bluebird');

module.exports = function QueryJson(pg, postgresURL, isProduction, logger) {
  var QUERY_PREFIX = 'SELECT * FROM sp_gymfit_api_';
  var queryCache = {};
  
  var _registerParameters = function _registerParameters(storedProcedureName, parameters) {
    var query = QUERY_PREFIX + storedProcedureName + '(' + _getParameterTypes(parameters) + ') AS result';
    
    queryCache[storedProcedureName] = query;
    return query;
  };
  
  var _getParameterTypes = function _getParameterTypes(parameters) {
    var formattedParameters = [];
    for (var index = 0; index < parameters.length; index++) {
      formattedParameters.push('$' + (index + 1) + '::' + parameters[index]);
    }
    
    return formattedParameters.join(',');
  };
  
  var _queryJson = function _queryJson(storedProcedureName, parameters, errorHandler) {
    if (!isProduction) {
      logger.log('DB: ' + storedProcedureName + (parameters ? ': ' + JSON.stringify(_getParameterValues(parameters)) : ''));
    }
    
    return _getConnection(postgresURL).spread(function(client, done) {
      if (parameters) {
        return _queryParameterisedQuery(storedProcedureName, parameters, client, done, errorHandler);
      }
      
      return _queryParameterlessQuery(storedProcedureName, client, done, errorHandler);
    });
  };
  
  var _getConnection = function _getConnection() {
    return new Promise(function(resolve, reject) {
      pg.connect(postgresURL, function(err, client, done) {
        if (err) {
          logger.log(err);
          
          if (client) {
            done(client);
          }
          
          return reject(err);
        }
        
        return resolve([client, done]);
      });
    });
  };
  
  var _queryParameterlessQuery = function _queryParameterlessQuery(storedProcedureName, client, done, errorHandler) {
    return new Promise(function(resolve, reject) {
      var query = QUERY_PREFIX + storedProcedureName + '() AS result';
      
      client.query(query, function(err, result) {
        _handleQueryResponse(err, result, storedProcedureName, errorHandler, resolve, reject, client, done);
      });
    });
  };
  
  var _queryParameterisedQuery = function _queryParameterisedQuery(storedProcedureName, parameters, client, done, errorHandler) {
    return new Promise(function(resolve, reject) {
      if (!queryCache[storedProcedureName]) {
        return reject('Parameters not registered for query: ' + storedProcedureName);
      }
      
      client.query(queryCache[storedProcedureName], _getParameterValues(parameters), function(err, result) {
        _handleQueryResponse(err, result, storedProcedureName, errorHandler, resolve, reject, client, done);
      });
    });
  };
  
  var _getParameterValues = function _getParameterValues(parameters) {
    var parameterValues = [];
    for (var index = 0; index < parameters.length; index++) {
      parameterValues.push(parameters[index]);
    }
    
    return parameterValues;
  };
  
  var _handleQueryResponse = function _handleQueryResponse(err, result, storedProcedureName, errorHandler, resolve, reject, client, done) {
    if (err) {
      if (errorHandler) {
        var errorHandlerResult = errorHandler(err);

        if (errorHandlerResult) {
          if (client) {
            done(client);
          }
          
          return reject({
            errors: errorHandlerResult
          });
        }
      }
      
      logger.error({
        storedProcedure: storedProcedureName,
        err: err
      });
      
      if (client) {
        done(client);
      }
      
      return reject(err);
    }

    done();

    return resolve(result.rows[0].result);
  };
  
  return {
    registerParameters: _registerParameters,
    queryJson: _queryJson
  };
};