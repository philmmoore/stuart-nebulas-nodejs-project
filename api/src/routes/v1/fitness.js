module.exports = function FitnessRoute(router, controller, logger) {
  router.get('/routine-types/', function(req, res) {
    controller.getRoutineTypes(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /routine-types/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
};
