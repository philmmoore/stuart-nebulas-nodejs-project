module.exports = function ResultRoute(router, controller, logger) {
  router.post('/result/save/', function(req, res) {
    controller.saveResult(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /result/save/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.get('/result/:resultId/', function(req, res) {
    controller.getResult(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /result/:resultId/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
};
