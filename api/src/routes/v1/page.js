module.exports = function ChallengeRoute(router, controller, logger) {
  router.get('/page/:name/', function(req, res) {
    controller.getPage(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /page/:name/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
};