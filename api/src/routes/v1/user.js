module.exports = function UserRoute(router, controller, logger) {
  router.post('/user/:userId/set-district/', function(req, res) {
    controller.setDistrict(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/set-district/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.post('/user/:userId/set-weight/', function(req, res) {
    controller.setWeight(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/set-weight/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.post('/user/:userId/set-height/', function(req, res) {
    controller.setHeight(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/set-height/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.post('/user/:userId/set-is-male/', function(req, res) {
    controller.setIsMale(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/set-is-male/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.post('/user/:userId/set-mobile-number/', function(req, res) {
    controller.setMobileNumber(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/set-mobile-number/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.post('/user/:userId/set-gym/', function(req, res) {
    controller.setGym(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/set-gym/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.post('/user/:userId/set-routine-type/', function(req, res) {
    controller.setRoutineType(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/set-routine-type/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.post('/user/:userId/set-privacy-preferences/', function(req, res) {
    controller.setPrivacyPreferences(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/set-privacy-preferences/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.get('/user/:userId/activities/', function(req, res) {
    controller.getActivities(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/activities/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.get('/user/:userId/notifications/', function(req, res) {
    controller.getNotifications(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/notifications/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.post('/user/:userId/notifications/:notificationId/acknowledge/', function(req, res) {
    controller.acknowledgeNotification(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/notifications/:notificationId/acknowledge/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.get('/user/profile/:username/', function(req, res) {
    controller.getUserProfile(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/profile/:username/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.get('/user/search/:query/', function(req, res) {
    controller.search(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/search/:query/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
  
  router.get('/user/:userId/follow/:userIdToFollow/', function(req, res) {
    controller.follow(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/follow/:userIdToFollow/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
  
  router.get('/user/:userId/unfollow/:userIdToUnfollow/', function(req, res) {
    controller.unfollow(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/unfollow/:userIdToUnfollow/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
  
  router.get('/user/:userId/set-client/:clientUserId/', function(req, res) {
    controller.setClient(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/set-client/:clientUserId/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
  
  router.get('/user/:userId/unset-client/:clientUserId/', function(req, res) {
    controller.unsetClient(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/unset-client/:clientUserId/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
  
  router.post('/user/:userId/first-name/', function(req, res) {
    controller.setFirstName(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/first-name/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
  
  router.post('/user/:userId/last-name/', function(req, res) {
    controller.setLastName(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/last-name/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
  
  router.post('/user/:userId/bio/', function(req, res) {
    controller.setBio(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/bio/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
  
  router.get('/user/:userId/challenge/:challengeId/progress/:challengeGroupId/', function(req, res) {
    controller.getProgressChartData(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/challenge/:challengeId/progress/:challengeGroupId/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.post('/user/:userId/set-is-gym/', function(req, res) {
    controller.setIsGym(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/set-is-gym/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.post('/user/:userId/set-is-personal-trainer/', function(req, res) {
    controller.setIsPersonalTrainer(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/set-is-personal-trainer/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.post('/user/:userId/send-push-notification/', function(req, res) {
    controller.sendPushNotification(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/send-push-notification/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.post('/user/:userId/register-device-for-push-notification/', function(req, res) {
    controller.registerDeviceForPushNotification(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/register-device-for-push-notification/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.get('/user/featured/', function(req, res) {
    controller.getFeatured(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/featured/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.get('/user/:userId/reviews/', function(req, res) {
    controller.getReviews(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: user/:userId/reviews/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.get('/user/:userId/reviews/:reviewId/', function(req, res) {
    controller.getReview(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/reviews/:reviewId/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

 router.post('/user/:userId/reviews/', function(req, res) {
    controller.submitReview(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/reviews/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

 router.delete('/user/:userId/reviews/:reviewId/', function(req, res) {
    controller.deleteReview(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /user/:userId/reviews/:reviewId/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

};