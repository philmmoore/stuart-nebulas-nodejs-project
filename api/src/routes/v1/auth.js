module.exports = function AuthRoute(router, controller, logger) {
  router.post('/auth/login/', function(req, res) {
    controller.login(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /auth/login/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
  
  router.post('/auth/:userId/change-password/', function(req, res) {
    controller.changePassword(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /auth/:userId/change-password/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.post('/auth/:userId/reset-password/', function(req, res) {
    controller.resetPassword(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /auth/:userId/reset-password/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.post('/auth/recover-username/', function(req, res) {
    controller.recoverUsername(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /auth/recover-username/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  // router.get('/auth/:userId/exists/', function(req, res) {
  //   controller.userExists(req).then(function(data) {
  //     res.json(data);
  //   }).error(function(err) {
  //     logger.log('gymfit-error: /auth/:userId/exists/', JSON.stringify(err));
  //     res.status(400).json(err);
  //   });
  // });

};