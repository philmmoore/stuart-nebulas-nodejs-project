module.exports = function LeaderboardRoute(router, controller, logger) {
  router.post('/leaderboard/', function(req, res) {
    controller.getLeaderboard(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /leaderboard/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.post('/leaderboard/user-position/', function(req, res) {
    controller.getUserPosition(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /leaderboard/user-position/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
};
