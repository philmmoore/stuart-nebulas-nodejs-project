module.exports = function SocialRoute(router, controller, logger, newController) {
  'use strict';
  
  router.post('/social/:userId/notifications/', function(req, res) {
    controller.getNotifications(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /social/:userId/notifications/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
  
  router.post('/social/updates/', function(req, res) {
    newController.getGlobalUpdates(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /social/updates/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
  
  router.post('/social/:userId/updates/', function(req, res) {
    newController.getUsersUpdates(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /social/:userId/updates/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
  
  router.post('/social/:userId/update/new/', function(req, res) {
    newController.newUpdate(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /social/:userId/update/new/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
  
  router.post('/social/update/:updateId/like/', function(req, res) {
    newController.like(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /social/update/:updateId/like/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
  
  router.post('/social/update/:updateId/unlike/', function(req, res) {
    newController.unlike(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /social/update/:updateId/unlike/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
  
  router.post('/social/update/:updateId/flag/', function(req, res) {
    newController.flag(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /social/update/:updateId/flag/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
  
  router.post('/social/update/:updateId/unflag/', function(req, res) {
    newController.unflag(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /social/update/:updateId/unflag/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
  
  router.post('/social/update/:updateId/attach/image/', function(req, res) {
    newController.attachImage(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /social/update/:updateId/attach/image/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
  
  router.post('/social/update/:updateId/attach/video/', function(req, res) {
    newController.attachVideo(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /social/update/:updateId/attach/video/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
  
  router.post('/social/update/search/', function(req, res) {
    newController.search(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /social/update/search/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
};