module.exports = function ChallengeRoute(router, controller, logger) {
  router.post('/blog/', function(req, res) {
    controller.getPosts(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /blog/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.get('/blog/:postId/', function(req, res) {
    controller.getPost(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /blog/:postId/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
};