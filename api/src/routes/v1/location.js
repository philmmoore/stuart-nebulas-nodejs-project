module.exports = function LocationRoute(router, controller, logger) {
  router.get('/location/countries/', function(req, res) {
    controller.getCountries(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /location/countries/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.get('/location/regions/:countryId/', function(req, res) {
    controller.getRegions(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /location/regions/:countryId/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.get('/location/counties/:regionId/', function(req, res) {
    controller.getCounties(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /location/counties/:regionId/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.get('/location/districts/:countyId/', function(req, res) {
    controller.getDistricts(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /location/districts/:countyId/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.get('/location/lookup/:latlon/', function(req, res) {
    controller.locationLookup(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /location/lookup/:latlon/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
};
