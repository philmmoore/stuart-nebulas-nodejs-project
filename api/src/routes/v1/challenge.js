module.exports = function ChallengeRoute(router, controller, logger) {
  router.post('/challenge/groups/', function(req, res) {
    controller.getGroups(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /challenge/groups/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.get('/challenge/categories/', function(req, res) {
    controller.getCategories(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /challenge/categories/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.get('/challenge/categories/:categoryId/', function(req, res) {
    controller.getChallengesForCategory(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /challenge/categories/:categoryId/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.get('/challenges/', function(req, res) {
    controller.getChallenges(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /challenges/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
};
