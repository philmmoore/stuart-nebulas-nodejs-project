module.exports = function NearMeRoute(router, controller, logger) {
  router.post('/near-me/', function(req, res) {
    controller.getNearMe(req).then(function(data) {
      res.json(data);
    }).error(function(err){
        logger.log('gymfit-error: /near-me/', JSON.stringify(err));
        res.status(400).json(err);
    });
  });
};