module.exports = function RegisterRoute(router, controller, logger) {
  router.get('/register/is-email-registered/:email/', function(req, res) {
    controller.getIsEmailRegistered(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /register/is-email-registered/:email/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.get('/register/is-username-registered/:username/', function(req, res) {
    controller.getIsUsernameRegistered(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /register/is-username-registered/:username/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.post('/register/', function(req, res) {
    controller.registerUser(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /register/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });

  router.get('/register/terms-and-conditions/:countryId/', function(req, res) {
    controller.getTermsAndConditions(req).then(function(data) {
      res.json(data);
    }).error(function(err) {
      logger.log('gymfit-error: /register/terms-and-conditions/:countryId/', JSON.stringify(err));
      res.status(400).json(err);
    });
  });
};
