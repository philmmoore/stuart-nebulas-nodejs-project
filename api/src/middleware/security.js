module.exports = function security(targetHost) {
  return function handle(req, res, next) {
    if (!req.secure || req.headers.host !== targetHost) {
      res.redirect('https://' + targetHost);
      return;
    }

    next();
  };
};
