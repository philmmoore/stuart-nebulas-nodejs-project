module.exports = function authentication(authKey) {
  return function handle(req, res, next) {
    if (req.url === '/') {
      return next();
    }

    if (req.headers['x-auth-key'] !== authKey) {
      return res.status(401).end();
    }

    next();
  };
};
