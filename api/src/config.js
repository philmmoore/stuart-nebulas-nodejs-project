var Config = function config() {
  var workers = process.env.WEB_CONCURRENCY || 1;
  var redisURL = process.env.REDIS_URL || 'redis://localhost:6379';
  var postgresURL = process.env.DATABASE_URL || 'postgres://localhost:5432';
  var postgresPoolSize = process.env.DATABASE_POOL_SIZE || 4;
  var port = process.env.PORT || 5000;
  var isProduction = process.env.NODE_ENV === 'production';
  var targetHost = process.env.TARGET_HOST;
  var authKey = process.env.AUTH_KEY || 'SJ2AjS4*r(0j@/lnSHe k3392/&&n -s£/sSKj2';
  var authSecretKey = process.env.AUTH_SECRET_KEY || 'Q[!3&K2h-Z*2 uKQ-8sK£28%9nT!';
  var avatarImageUrl = process.env.AVATAR_IMAGE_URL || 'https://s3-eu-west-1.amazonaws.com/gymfit-staging-avatars/';
  var evidenceImageUrl = process.env.EVIDENCE_IMAGE_URL || 'https://s3-eu-west-1.amazonaws.com/gymfit-staging-evidence-photos/';
  var evidenceVideoUrl = process.env.EVIDENCE_VIDEO_URL || 'https://gymfit-staging-videos.s3.amazonaws.com/';
  var socialImageUrl = process.env.SOCIAL_IMAGE_URL || 'https://gymfit-staging-social-photos.s3.amazonaws.com/';
  var socialVideoUrl = process.env.SOCIAL_VIDEO_URL || 'https://gymfit-staging-misc-videos.s3.amazonaws.com/';
  var googleMapsAPIServerKey = process.env.GOOGLE_MAPS_API_SERVER_KEY || 'AIzaSyBaka4ye2k8xHfS2dn9MIzQ15w0TabEWH8';
  var isLeaderboardRealtime = process.env.IS_LEADERBOARD_REALTIME ? true : (isProduction ? false : true);
  
  var imageUploadAWSAccessKeyID = process.env.IMAGE_UPLOAD_AWS_ACCESS_KEY_ID || 'AKIAJHDJZBQAREA5REBA';
  var imageUploadAWSSecretAccessKey = process.env.IMAGE_UPLOAD_AWS_SECRET_ACCESS_KEY || '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';
  var awsRegion = process.env.AWS_REGION_NAME || 'eu-west-1';
  var awsResourcePrefix = process.env.AWS_RESOURCE_PREFIX || 'gymfit-staging-';

  if (isProduction) {
    postgresURL += '?ssl=true';
  }

  return {
    workers: workers,
    redisURL: redisURL,
    postgresURL: postgresURL,
    postgresPoolSize: postgresPoolSize,
    port: port,
    isProduction: isProduction,
    targetHost: targetHost,
    authKey: authKey,
    authSecretKey: authSecretKey,
    avatarImageUrl: avatarImageUrl,
    googleMapsAPIServerKey: googleMapsAPIServerKey,
    evidenceImageUrl: evidenceImageUrl,
    evidenceVideoUrl: evidenceVideoUrl,
    socialImageUrl: socialImageUrl,
    socialVideoUrl: socialVideoUrl,
    isLeaderboardRealtime: isLeaderboardRealtime,
    snsConfig: {
      accessKeyId: imageUploadAWSAccessKeyID,
      secretAccessKey: imageUploadAWSSecretAccessKey,
      region: awsRegion
    },
    queueConfig: {
      accessKeyId: imageUploadAWSAccessKeyID,
      secretAccessKey: imageUploadAWSSecretAccessKey,
      region: awsRegion
    },
    queueUrlPrefix: 'https://sqs.eu-west-1.amazonaws.com/164535666447/' + awsResourcePrefix
  };
};

module.exports = Config;