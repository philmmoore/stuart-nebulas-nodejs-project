var Promise = require('bluebird');

module.exports = function AuthController(repository, passwordHashService, validator, avatarImageUrl, messageQueueService) {
  var _login = function _login(req) {
    return validator.validateLogin(req).then(function() {
      return passwordHashService.getHash(req.body.password);
    }).then(function(hash) {
      return repository.login(req.body.emailOrUsername.trim(), hash, req.body.deviceId);
    }).then(function(data) {
      if (!data) {
        return Promise.reject({
          errors: [['password', 'Error_Invalid_Username_Or_Password']]
        });
      }

      if (data.avatarUploaded) {
        data.avatarImage = avatarImageUrl + data.id + '-170x170.png';
      } else {
        data.avatarImage = null;
      }

      return data;
    });
  };
  
  var _changePassword = function _changePassword(req) {
    return validator.validateChangePassword(req).then(function() {
      return [
        passwordHashService.getHash(req.body.oldPassword),
        passwordHashService.getHash(req.body.newPassword)
      ];
    }).spread(function(oldHash, newHash) {
      return repository.changePassword(req.params.userId, oldHash, newHash, req.body.deviceId);
    }).then(function(email) {
      var message = JSON.stringify({
        email: email
      });

      return messageQueueService.sendMessage('change-password-email', message);
    }).then(function() {
      return {
        success: true
      };
    });
  };

  var _userExists = function _userExists(req) {

    return validator.validateUserExists(req).then(function() {
      return repository.userExists(req.params.userId);
    }).then(function(response){
      return response;
    });

  };

  var _userExistsByEmail = function _userExistsByEmail(req) {

    return validator.validateEmailAddress(req).then(function(response) {
      return repository.userExistsByEmail(req.body.email);
    }).then(function(response){
      return response;
    });

  };


  var _resetPassword = function _resetPassword(req) {

    return validator.validateResetPassword(req).then(function(){
      return _userExists(req);
    }).then(function(response){
      if (!response.exists){
        return Promise.reject({
          errors: [['userId', 'User_Invalid']]
        });
      }
      return repository.resetPassword(req.params.userId);
    }).then(function(response){

      var message = JSON.stringify({
        email: response.email,
        key: response.key
      });

      messageQueueService.sendMessage('password-reset', message);

      return response;

    });

  };

  var _recoverUsername = function _recoverUsername(req) {
    return _userExistsByEmail(req).then(function(response){
      console.log(response);
      if (!response.exists){
        return Promise.reject({
          errors: [['email', 'User_Invalid']]
        });
      }
      return repository.recoverUsername(req.body.email);
    }).then(function(response){

      var message = JSON.stringify({
        email: response.email,
        username: response.username
      });

      messageQueueService.sendMessage('recover-username', message);

      return response;

    });

  };

  return {
    login: _login,
    changePassword: _changePassword,
    resetPassword: _resetPassword,
    recoverUsername: _recoverUsername,
    userExists: _userExists,
    userExistsByEmail: _userExistsByEmail
  };
};
