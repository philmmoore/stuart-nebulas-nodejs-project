module.exports = function LeaderboardController(repository, validator, leaderboardDateService, avatarImageUrl, evidenceImageUrl, evidenceVideoUrl) {
  var _getLeaderboard = function getLeaderboard(req) {
    var model = {
      challengeId:        req.body.challengeId,
      challengeGroupId:   req.body.challengeGroupId,
      districtId:         req.body.districtId,
      countyId:           req.body.countyId,
      regionId:           req.body.regionId,
      countryId:          req.body.countryId,
      resultsPerPage:     req.body.resultsPerPage,
      page:               req.body.page,
      isMale:             req.body.isMale,
      ageMin:             req.body.ageMin,
      ageMax:             req.body.ageMax,
      weightMin:          req.body.weightMin,
      weightMax:          req.body.weightMax,
      onlyShowValidated:  req.body.onlyShowValidated ? (req.body.onlyShowValidated === '1' ? true : false) : false,
      userId:             req.body.userId
    };

    return validator.validateGetLeaderboard(model).then(function() {
      return leaderboardDateService.getDates();
    }).then(function(leaderboardDates) {
      model.currentPeriodStart = leaderboardDates.seasonStartDate;
      model.lastPeriodStart = leaderboardDates.seasonStartDate;
      model.lastPeriodEnd = leaderboardDates.previous;
      model.currentPeriodEnd = leaderboardDates.current;

      return repository.getLeaderboard(model).then(function(data) {
        if (!data) {
          data = {
            numberOfPages: 0
          };
        }
        
        data.lastUpdated = leaderboardDates.current;
        data.nextUpdate = leaderboardDates.next;

        if (!data.results) {
          return data;
        }

        for (var index = 0; index < data.results.length; index++) {
          var result = data.results[index];

          if (result.userHasAvatar) {
            result.avatarImage = avatarImageUrl + result.userId + '-170x170.png';
          } else {
            result.avatarImage = null;
          }

          if (result.imageValidated) {
            result.evidenceImageUrl = evidenceImageUrl + result.id + '.png';
            result.evidenceImageThumbUrl = evidenceImageUrl + result.id + '-sm.png';
          } else {
            result.evidenceImageUrl = null;
            result.evidenceImageThumbUrl = null;
          }

          if (result.videoValidated) {
            result.evidenceVideoUrl = evidenceVideoUrl + result.id + '.mp4';
            result.evidenceVideoThumbUrl = evidenceVideoUrl + result.id + '.jpg';
          } else {
            result.evidenceVideoUrl = null;
            result.evidenceVideoThumbUrl = null;
          }

          delete result.userHasAvatar;
          delete result.imageValidated;
          delete result.videoValidated;
        }

        return data;
      });
    });
  };

  var _getUserPosition = function _getUserPosition(req) {
    var model = {
      challengeId:        req.body.challengeId,
      challengeGroupId:   req.body.challengeGroupId,
      districtId:         req.body.districtId,
      countyId:           req.body.countyId,
      regionId:           req.body.regionId,
      countryId:          req.body.countryId,
      isMale:             req.body.isMale,
      ageMin:             req.body.ageMin,
      ageMax:             req.body.ageMax,
      weightMin:          req.body.weightMin,
      weightMax:          req.body.weightMax,
      onlyShowValidated:  req.body.onlyShowValidated ? (req.body.onlyShowValidated === '1' ? true : false) : false
    };

    return validator.validateGetUserPosition(model).then(function() {
      return leaderboardDateService.getDates();
    }).then(function(leaderboardDates) {
      model.currentPeriodStart = leaderboardDates.seasonStartDate;
      model.currentPeriodEnd = leaderboardDates.current;

      return repository.getUserPosition(model);
    }).then(function(data) {
      return !data ? [] : data;
    });
  };

  return {
    getLeaderboard: _getLeaderboard,
    getUserPosition: _getUserPosition
  };
};
