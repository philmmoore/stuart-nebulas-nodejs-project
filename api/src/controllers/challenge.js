module.exports = function ChallengeController(repository, validator, markdownTextFormatter) {
  var _getGroups = function _getGroups(req) {
    return validator.validateGetGroups(req).then(function() {
      return repository.getGroups(req.body.challengeId, req.body.userId);
    }).then(function(data) {
      return !data ? [] : data;
    });
  };
  
  var _formatChallenges = function _formatChallenges(challenges) {
    if (!challenges) {
      return;
    }
    
    for (var index = 0; index < challenges.length; index++) {
      var challenge = challenges[index];
      challenge.bodyCopy = markdownTextFormatter.format(challenge.bodyCopy);
    }
  };

  var _getChallenges = function _getChallenges(req) {
    return repository.getChallenges().then(function(challenges) {
      _formatChallenges(challenges);      
      return challenges;
    });
  };

  var _getCategories = function _getCategories(req) {
    return repository.getCategories();
  };

  var _getChallengesForCategory = function _getChallengesForCategory(req) {
    return validator.validateGetChallengesForCategory(req).then(function() {
      return repository.getChallengesForCategory(req.params.categoryId);
    }).then(function(challenges) {
      _formatChallenges(challenges);
      return !challenges ? [] : challenges;
    });
  };

  return {
    getGroups: _getGroups,
    getCategories: _getCategories,
    getChallengesForCategory: _getChallengesForCategory,
    getChallenges: _getChallenges
  };
};
