var Promise = require('bluebird');

module.exports = function NearMeController(repository,validator,avatarImageUrl) {
  
  var _getNearMe = function _getNearMe(req) {
    return validator.validateGetNearMe(req).then(function(){
      return repository.getNearMe(req.body.latitude, req.body.longitude, req.body.radius, req.body.isPersonalTrainer, req.body.isGym, req.body.userId, req.body.showFollowing).then(function(data){

        if (!data) {
          return [];
        }

        for (var index = 0; index < data.length; index++) {
          var result = data[index];
          if (result.avatarUploaded) {
            result.avatarImage = avatarImageUrl + result.id + '-170x170.png';
          } else {
            result.avatarImage = null;
          }
          delete result.avatarUploaded;
        }

        return data;

      });
    });
  };

  return {
    getNearMe: _getNearMe
  };

};