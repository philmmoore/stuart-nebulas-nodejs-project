module.exports = function UserController(repository, validator, leaderboardDateService, avatarImageUrl, evidenceImageUrl, evidenceVideoUrl,messageQueueService, snsService) {
  var _setDistrict = function _setDistrict(req) {
    return validator.validateSetDistrict(req).then(function() {
      return repository.setDistrict(req.params.userId, req.body.districtId);
    }).then(function() {
      return {
        success: true
      };
    });
  };

  var _setWeight = function _setWeight(req) {
    return validator.validateSetWeight(req).then(function() {
      return repository.setWeight(req.params.userId, req.body.weight);
    }).then(function() {
      return {
        success: true
      };
    });
  };

  var _setHeight = function _setHeight(req) {
    return validator.validateSetHeight(req).then(function() {
      return repository.setHeight(req.params.userId, req.body.height);
    }).then(function() {
      return {
        success: true
      };
    });
  };

  var _setIsMale = function _setIsMale(req) {
    return validator.validateSetIsMale(req).then(function() {
      return repository.setIsMale(req.params.userId, req.body.isMale === 'true');
    }).then(function() {
      return {
        success: true
      };
    });
  };

  var _setMobileNumber = function _setMobileNumber(req) {
    return validator.validateSetMobileNumber(req).then(function() {
      return repository.setMobileNumber(req.params.userId, req.body.mobileNumber);
    }).then(function() {
      return {
        success: true
      };
    });
  };

  var _setGym = function _setGym(req) {
    return validator.validateSetGym(req).then(function() {
      return repository.setGym(req.params.userId, req.body.gym.trim());
    }).then(function() {
      return {
        success: true
      };
    });
  };

  var _setRoutineType = function _setRoutineType(req) {
    return validator.validateSetRoutineType(req).then(function() {
      return repository.setRoutineType(req.params.userId, req.body.routineTypeId);
    }).then(function() {
      return {
        success: true
      };
    });
  };

  var _formatBool = function _formatBool(value) {
    return value.toLowerCase() === 'true' || value.toLowerCase() === '1';
  };

  var _setPrivacyPreferences = function _setPrivacyPreferences(req) {
    var privacyPreferences = {
      userId: req.params.userId,
      gymFitEmailOptIn: req.body.gymFitEmailOptIn,
      partnersEmailOptIn: req.body.partnersEmailOptIn,
      gymFitSMSOptIn: req.body.gymFitSMSOptIn,
      partnersSMSOptIn: req.body.partnersSMSOptIn,
      gymFitOnlineOptIn: req.body.gymFitOnlineOptIn,
      partnersOnlineOptIn: req.body.partnersOnlineOptIn
    };

    return validator.validateSetPrivacyPreferences(privacyPreferences).then(function() {
      privacyPreferences.gymFitEmailOptIn = _formatBool(privacyPreferences.gymFitEmailOptIn);
      privacyPreferences.partnersEmailOptIn = _formatBool(privacyPreferences.partnersEmailOptIn);
      privacyPreferences.gymFitSMSOptIn = _formatBool(privacyPreferences.gymFitSMSOptIn);
      privacyPreferences.partnersSMSOptIn = _formatBool(privacyPreferences.partnersSMSOptIn);
      privacyPreferences.gymFitOnlineOptIn = _formatBool(privacyPreferences.gymFitOnlineOptIn);
      privacyPreferences.partnersOnlineOptIn = _formatBool(privacyPreferences.partnersOnlineOptIn);

      return repository.setPrivacyPreferences(privacyPreferences);
    }).then(function() {
      return {
        success: true
      };
    });
  };

  var _getActivities = function _getActivities(req) {
    return validator.validateGetActivities(req).then(function() {
      return leaderboardDateService.getDates();
    }).then(function(leaderboardDates) {
      return repository.getActivities(req.params.userId, leaderboardDates.seasonStartDate, leaderboardDates.current, leaderboardDates.seasonStartDate, leaderboardDates.previous);
    }).then(function(data) {
      return _formatActivities(data || []);
    });
  };
  
  var _formatActivities = function _formatActivities(activities) {
    for (var index = 0; index < activities.length; index++) {
      var activity = activities[index];
      
      if (activity.evidenceImage) {
        activity.evidenceImageUrl = evidenceImageUrl + activity.evidenceImage + '.png';
      } else {
        activity.evidenceImageUrl = null;
      }
      
      if (activity.evidenceVideo) {
        activity.evidenceVideoUrl = evidenceVideoUrl + activity.evidenceVideo + '.mp4';
      } else {
        activity.evidenceVideoUrl = null;
      }
      
      delete activity.evidenceImage;
      delete activity.evidenceVideo;
    }
    
    return activities;
  };

  var _getNotifications = function _getNotifications(req) {
    return validator.validateGetNotifications(req).then(function() {
      return repository.getNotifications(req.params.userId);
    });
  };

  var _acknowledgeNotification = function _acknowledgeNotification(req) {
    return validator.validateAcknowledgeNotification(req).then(function() {
      return repository.acknowledgeNotification(req.params.userId, req.params.notificationId);
    }).then(function() {
      return {
        success: true
      };
    });
  };

  var _getUserProfile = function _getUserProfile(req) {
    return validator.validateGetUserProfile(req).then(function() {
      return repository.getUserProfile(req.params.username).then(function(data) {
        if (!data) {
          return {};
        }
        
        _appendAvatar(data);
        
        if (data.following.length > 0) {
          for (var followingIndex = 0; followingIndex < data.following.length; followingIndex++) {
            _appendAvatar(data.following[followingIndex]);
          }
        }
        
        if (data.followers.length > 0) {
          for (var followerIndex = 0; followerIndex < data.followers.length; followerIndex++) {
            _appendAvatar(data.followers[followerIndex]);
          }
        }
        
        if (data.clients.length > 0) {
          for (var clientIndex = 0; clientIndex < data.clients.length; clientIndex++) {
            _appendAvatar(data.clients[clientIndex]);
          }
        }
        
        if (data.trainers.length > 0) {
          for (var trainerIndex = 0; trainerIndex < data.trainers.length; trainerIndex++) {
            _appendAvatar(data.trainers[trainerIndex]);
          }
        }

        return data;
      });
    });
  };
  
  var _appendAvatar = function _appendAvatar(data) {
    if (data.avatarUploaded) {
      data.avatarImage = avatarImageUrl + data.id + '-170x170.png';
    } else {
      data.avatarImage = null;
    }

    delete data.avatarUploaded;
  };

  var _search = function _search(req) {
    return repository.search(req.params.query).then(function(data) {
      if (!data) {
        return [];
      }

      for (var index = 0; index < data.length; index++) {
        var result = data[index];

        if (result.avatarUploaded) {
          result.avatarImage = avatarImageUrl + result.id + '-170x170.png';
        } else {
          result.avatarImage = null;
        }

        delete result.avatarUploaded;
      }

      return data;
    });
  };
  
  var _follow = function _follow(req) {
    return validator.validateFollow(req).then(function() {
      return repository.follow(req.params.userIdToFollow, req.params.userId);
    }).then(function() {
      return {
        success: true
      };
    });
  };
  
  var _unfollow = function _unfollow(req) {
    return validator.validateUnfollow(req).then(function() {
      return repository.unfollow(req.params.userIdToUnfollow, req.params.userId);
    }).then(function() {
      return {
        success: true
      };
    });
  };
  
  var _setClient = function _setClient(req) {
    return validator.validateSetClient(req).then(function() {
      return repository.setClient(req.params.userId, req.params.clientUserId);
    }).then(function() {
      return {
        success: true
      };
    });
  };
  
  var _unsetClient = function _unsetClient(req) {
    return validator.validateUnsetClient(req).then(function() {
      return repository.unsetClient(req.params.userId, req.params.clientUserId);
    }).then(function() {
      return {
        success: true
      };
    });
  };
  
  var _setFirstName = function _setFirstName(req) {
    return validator.validateSetFirstName(req).then(function() {
      return repository.setFirstName(req.params.userId, req.body.firstName.trim());
    }).then(function() {
      return {
        success: true
      };
    });
  };
  
  var _setLastName = function _setLastName(req) {
    return validator.validateSetLastName(req).then(function() {
      return repository.setLastName(req.params.userId, req.body.lastName.trim());
    }).then(function() {
      return {
        success: true
      };
    });
  };
  
  var _setBio = function _setBio(req) {
    return validator.validateSetBio(req).then(function() {
      return repository.setBio(req.params.userId, (req.body.bio || '').trim());
    }).then(function() {
      return {
        success: true
      };
    });
  };
  
  var _getProgressChartData = function _getProgressChartData(req) {
    return validator.validateGetProgressChartData(req).then(function() {
      return repository.getProgressChartData(req.params.userId, req.params.challengeId, req.params.challengeGroupId);
    });
  };

  var _setIsGym = function(req) {
    return validator.validateSetIsGym(req).then(function() {
      return repository.setIsGym(req.params.userId, req.body.isGym);
    }).then(function() {
      return {
        success: true
      };
    });
  };

  var _setIsPersonalTrainer = function(req) {
    return validator.validateSetIsPersonalTrainer(req).then(function() {
      return repository.setIsPersonalTrainer(req.params.userId, req.body.isPersonalTrainer);
    }).then(function() {
      return {
        success: true
      };
    });
  };


  var _sendPushNotification = function(req) {
    return validator.validateSendPushNotification(req).then(function() {
      var message = JSON.stringify({
        userId: parseInt(req.params.userId),
        title: (req.body.title ? req.body.title : false),
        content: req.body.content,
        badge: (req.body.badge ? parseInt(req.body.badge) : 0),
        sound: (req.body.sound ? true : false)
      });
      return messageQueueService.sendMessage('send-push-notification', message);
    }).then(function(){
      return {
        success: true
      };
    });
  };

  var _registerDeviceForPushNotification = function(req) {
    return validator.validateRegisterDeviceForPushNotification(req).then(function() {
      return snsService.registerDeviceToken(req.body.deviceToken);
    }).then(function(endpoint){
      return repository.registerDeviceForPushNotification(req.params.userId, req.body.deviceId, req.body.deviceToken, endpoint);
    }).then(function() {
      return {
        success: true
      };
    });
  };

  var _getFeatured = function _getFeatured(req) {
    return repository.getFeatured().then(function(data){

        if (!data) {
          return {};
        }
        for (var userIndex in data) {
          var user = data[userIndex];
          _appendAvatar(user);
          data[userIndex] = user;
        }

        return data;

    });
  };

  var _getReviews = function _getReviews(req) {
    return validator.validateGetReviews(req).then(function() {
      if (!req.query.limit){
        req.query.limit = null;
      }
      return repository.getReviews(req.params.userId, req.query.limit);
    });
  };

  var _getReview = function _getReview(req) {
    return validator.validateGetReview(req).then(function() {
      return repository.getReview(req.params.userId, req.params.reviewId);
    });
  };

  var _submitReview = function _submitReview(req) {
    return validator.validateSubmitReview(req).then(function() {
      return repository.submitReview(req.params.userId, req.body.reviewerUserId, req.body.title, req.body.body, req.body.rating);
    });
  };

  var _deleteReview = function _deleteReview(req) {
    return validator.validateDeleteReview(req).then(function() {
      return repository.deleteReview(req.params.userId, req.params.reviewId);
    });
  };

  return {
    setDistrict: _setDistrict,
    setWeight: _setWeight,
    setHeight: _setHeight,
    setIsMale: _setIsMale,
    setIsGym: _setIsGym,
    setIsPersonalTrainer: _setIsPersonalTrainer,
    setMobileNumber: _setMobileNumber,
    setGym: _setGym,
    setRoutineType: _setRoutineType,
    setPrivacyPreferences: _setPrivacyPreferences,
    getActivities: _getActivities,
    getNotifications: _getNotifications,
    acknowledgeNotification: _acknowledgeNotification,
    getUserProfile: _getUserProfile,
    search: _search,
    follow: _follow,
    unfollow: _unfollow,
    setClient: _setClient,
    unsetClient: _unsetClient,
    setFirstName: _setFirstName,
    setLastName: _setLastName,
    setBio: _setBio,
    getProgressChartData: _getProgressChartData,
    getFeatured: _getFeatured,
    sendPushNotification: _sendPushNotification,
    registerDeviceForPushNotification: _registerDeviceForPushNotification,
    getReviews: _getReviews,
    getReview: _getReview,
    submitReview: _submitReview,
    deleteReview: _deleteReview
  };

};