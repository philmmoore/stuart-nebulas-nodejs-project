module.exports = function BlogController(repository, validator, markdownTextFormatter) {
  var COUNTRY_ID = 1;
  
  var _getPosts = function _getPosts(req) {
    return validator.validateGetPosts(req).then(function() {
      return repository.getPosts(COUNTRY_ID, req.body.postsPerPage, req.body.pageNumber);
    }).then(function(data) {
      return data;
    });
  };
  
  var _getPost = function _getPost(req) {
    return validator.validateGetPost(req).then(function() {
      return repository.getPost(req.params.postId);
    }).then(function(post) {
      post.bodyCopy = markdownTextFormatter.format(post.bodyCopy);
      return post;
    });
  };
  
  return {
    getPosts: _getPosts,
    getPost: _getPost
  };
};
