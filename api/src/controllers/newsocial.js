var Promise = require('bluebird');

module.exports = function NewSocialController(repository, validator, avatarImageUrl, evidenceImageUrl, evidenceVideoUrl, socialImageUrl, socialVideoUrl, messageQueueService) {
  'use strict';
  
  var _getGlobalUpdates = function _getGlobalUpdates(req) {
    return validator.validateGetGlobalUpdates(req).then(function() {
      return repository.getGlobalUpdates(req.body.userId, req.body.from, req.body.numberToReturn, (req.body.tag ? req.body.tag : ''));
    }).then(function(data) {
      return _formatUpdates(data);
    });
  };
  
  var _getUsersUpdates = function _getUsersUpdates(req) {
    return validator.validateGetUsersUpdates(req).then(function() {
      var showOnlyMine = (req.body.showOnlyMine || '').toLowerCase() === 'true';
      var ignoreMine = (req.body.ignoreMine || '').toLowerCase() === 'true';
      
      return repository.getUsersUpdates(req.params.userId, req.body.from, req.body.numberToReturn, showOnlyMine, ignoreMine);
    }).then(function(data) {
      return _formatUpdates(data);
    });
  };
  
  var _formatUpdates = function _formatUpdates(data) {
    if (!data) {
      return [];
    }
    
    for (var index = 0; index < data.length; index++) {
      var update = data[index];
      
      update.avatar = avatarImageUrl + (update.avatarUploaded ? update.userId : 0) + '-170x170.png';
      delete update.avatarUploaded;
      
      if (!update.result) {
        update.result = {};
      } else {
        _formatResult(update.result);
      }
      
      _formatImages(update.images);
      _formatVideos(update.videos);
    }
    
    return data;
  };
  
  var _formatResult = function _formatResult(result) {
    result.evidenceImageUrl = null;
    result.evidenceImageThumbUrl = null;
    result.evidenceVideoUrl = null;
    result.evidenceVideoThumbUrl = null;
    
    if (result.imageValidated) {
      result.evidenceImageUrl = evidenceImageUrl + result.id + '.png';
      result.evidenceImageThumbUrl = evidenceImageUrl + result.id + '-sm.png';
    }
    
    if (result.videoValidated) {
      result.evidenceVideoUrl = evidenceVideoUrl + result.id + '.mp4';
      result.evidenceVideoThumbUrl = evidenceVideoUrl + result.id + '.jpg';
    }
    
    delete result.imageValidated;
    delete result.videoValidated;
  };
  
  var _formatImages = function _formatImages(images) {
    for (var index = 0; index < images.length; index++) {
      images[index] = {
        imageUrl: socialImageUrl + images[index] + '.png',
        imageThumbUrl: socialImageUrl + images[index] + '-sm.png'
      };
    }
  };
  
  var _formatVideos = function _formatVideos(videos) {
    for (var index = 0; index < videos.length; index++) {
      videos[index] = {
        videoUrl: socialVideoUrl + videos[index] + '.mp4',
        placeholderImageUrl: socialVideoUrl + videos[index] + '.jpg'
      };
    }
  };
  
  var _newUpdate = function _newUpdate(req) {
    return validator.validateNewUpdate(req).then(function() {
      var formattedTags = _formatTags(req.body.body, req.body.tags);
      var formattedBody = (req.body.body || '').trim();
      return repository.postNewUpdate(req.params.userId, formattedBody, formattedTags);
    });
  };
  
  var _formatTags = function _formatTags(body, tags) {
    var formattedTags = tags ? tags.split(',') : [];
    
    if (body) {
      var matches = body.match(/(@\S+)|(#\S+)/g);
      formattedTags = formattedTags.concat(matches);
    }
    
    return formattedTags.join(',');
  };
  
  var _like = function _like(req) {
    return validator.validateLike(req).then(function() {
      return repository.like(req.body.userId, req.params.updateId);
    });
  };
  
  var _unlike = function _unlike(req) {
    return validator.validateUnlike(req).then(function() {
      return repository.unlike(req.body.userId, req.params.updateId);
    });
  };
  
  var _flag = function _flag(req) {
    return validator.validateFlag(req).then(function() {
      return repository.flag(req.body.userId, req.params.updateId);
    }).then(function(data) {
      var message = JSON.stringify({
        updateId: req.params.updateId
      });
    
      return messageQueueService.sendMessage('social-update-flagged-email', message).then(function() {
        return data;
      });
    });
  };
  
  var _unflag = function _unflag(req) {
    return validator.validateUnflag(req).then(function() {
      return repository.unflag(req.body.userId, req.params.updateId);
    });
  };
  
  var _attachImage = function _attachImage(req) {
    return validator.validateAttachImage(req).then(function() {
      return repository.attachImage(req.body.userId, req.params.updateId).then(function(data) {
        if (!data.filename) {
          return Promise.reject({
            errors: [['userId', 'Not_Authorised']]
          });
        }
        
        return data;
      });
    });
  };
  
  var _attachVideo = function _attachVideo(req) {
    return validator.validateAttachVideo(req).then(function() {
      return repository.attachVideo(req.body.userId, req.params.updateId).then(function(data) {
        if (!data.filename) {
          return Promise.reject({
            errors: [['userId', 'Not_Authorised']]
          });
        }
        
        return data;
      });
    });
  };
  
  var _search = function _search(req) {
    return validator.validateSearch(req).then(function() {
      return repository.search(req.body.userId, req.body.query).then(function(data) {
        return _formatUpdates(data);
      });
    });
  };
  
  return {
    getGlobalUpdates: _getGlobalUpdates,
    getUsersUpdates: _getUsersUpdates,
    newUpdate: _newUpdate,
    like: _like,
    unlike: _unlike,
    flag: _flag,
    unflag: _unflag,
    attachImage: _attachImage,
    attachVideo: _attachVideo,
    search: _search
  };
};