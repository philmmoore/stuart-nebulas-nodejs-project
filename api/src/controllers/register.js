var moment = require('moment');

module.exports = function RegisterController(repository, passwordHashService, validator, messageQueueService) {
  var _getIsEmailRegistered = function _getIsEmailRegistered(req) {
    return validator.validateEmail(req).then(function() {
      return repository.getIsEmailRegistered(req.params.email);
    }).then(function(result) {
      if (result){
        return {
          isEmailRegistered: true,
          userId: result[0].userId
        };
      } else {
        return {
          isEmailRegistered: false
        };      
      }
    });
  };

  var _getIsUsernameRegistered = function _getIsUsernameRegistered(req) {
    return validator.validateUsername(req).then(function() {
      return repository.getIsUsernameRegistered(req.params.username);
    }).then(function(result) {
      if (result){
        return {
          isUsernameRegistered: true,
          userId: result[0].userId
        };
      } else {
        return {
          isUsernameRegistered: false
        };      
      }
    });
  };

  var _registerUser = function _registerUser(req) {
    var registrationDetails = {
      username: req.body.username,
      password: req.body.password,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      dateOfBirth: req.body.dateOfBirth,
      email: req.body.email
    };

    return validator.validateRegistrationDetails(registrationDetails).then(function() {
      return passwordHashService.getHash(req.body.password);
    }).then(function(hash) {
      registrationDetails.hash = hash;
      registrationDetails.dateOfBirth = moment(registrationDetails.dateOfBirth);
      registrationDetails.firstName = registrationDetails.firstName.trim();
      registrationDetails.lastName = registrationDetails.lastName.trim();
      
      return repository.registerUser(registrationDetails);
    }).then(function(registrationResult) {
      var message = JSON.stringify({
        email: registrationResult.email,
        key: registrationResult.verificationKey
      });

      return messageQueueService.sendMessage('confirm-email', message).then(function() {
        return {
          id: registrationResult.id
        };
      });
    });
  };

  var _getTermsAndConditions = function _getTermsAndConditions(req) {
    return validator.validateTermsAndConditions(req).then(function() {
      return {
        firstBlock: 'We would like to contact you by email, text message or with a personalised online message with information and offers of products and services (including those of our <a href="https://gymfit.com/gb/partners/">GymFit Partners</a>) that we believe may interest you. <b>If you <u>do not want to receive information and offers</u> please tick the relevant boxes</b> (if you do not tick the boxes you will be consenting to receive information and offers as described. You can change your mind at any time):',
        option1: 'By email',
        option2: 'By text',
        option3: 'Personalised online message',
        question1: 'I DO NOT WANT information or offers available from GymFit',
        question2: 'I DO NOT WANT GymFit to send me information or offers available from GymFit Partners',
        secondBlock: '<ul><li>I am over 16 years old</li><li>I have read and agree to the <a href="https://gymfit.com/gb/terms-and-conditions/">GymFit Terms and Conditions</a></li><li>I have read and agree to my personal information being used as described in the <a href="https://gymfit.com/gb/privacy-policy/">GymFit Privacy Policy</a> and have selected my preferences</li></ul>'
      };
    });
  };

  return {
    getIsEmailRegistered: _getIsEmailRegistered,
    getIsUsernameRegistered: _getIsUsernameRegistered,
    registerUser: _registerUser,
    getTermsAndConditions: _getTermsAndConditions
  };
};
