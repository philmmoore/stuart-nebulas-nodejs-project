module.exports = function FitnessController(repository) {
  var _getRoutineTypes = function _getRoutineTypes(req) {
    return repository.getRoutineTypes();
  };

  return {
    getRoutineTypes: _getRoutineTypes
  };
};
