module.exports = function ResultController(repository, validator, avatarImageUrl, evidenceImageUrl, evidenceVideoUrl, leaderboardDateService) {
  var _saveResult = function _saveResult(req) {
    return validator.validateSaveResult(req).then(function() {
      return repository.saveResult(req.body.userId, req.body.challengeId, req.body.challengeGroupId, req.body.result);
    });
  };

  var _getResult = function _getResult(req) {
    return validator.validateGetResult(req).then(function() {
      return leaderboardDateService.getDates();
    }).then(function(leaderboardDates) {
      return repository.getResult(req.params.resultId, leaderboardDates.seasonStartDate, leaderboardDates.current, leaderboardDates.seasonStartDate, leaderboardDates.previous);
    }).then(function(data) {
      if (!data) {
        return {};
      }

      if (data.avatarUploaded) {
        data.avatarImage = avatarImageUrl + data.userId + '-170x170.png';
      } else {
        data.avatarImage = null;
      }

      if (data.imageValidated) {
        data.evidenceImageUrl = evidenceImageUrl + req.params.resultId + '.png';
        data.evidenceImageThumbUrl = evidenceImageUrl + req.params.resultId + '-sm.png';
      } else {
        data.evidenceImageUrl = null;
        data.evidenceImageThumbUrl = null;
      }

      if (data.videoValidated) {
        data.evidenceVideoUrl = evidenceVideoUrl + req.params.resultId + '.mp4';
        data.evidenceVideoThumbUrl = evidenceVideoUrl + req.params.resultId + '.jpg';
      } else {
        data.evidenceVideoUrl = null;
        data.evidenceVideoThumbUrl = null;
      }

      delete data.avatarUploaded;
      delete data.imageValidated;
      delete data.videoValidated;

      return data;
    });
  };

  return {
    saveResult : _saveResult,
    getResult: _getResult
  };
};
