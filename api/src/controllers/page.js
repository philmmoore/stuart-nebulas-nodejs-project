module.exports = function BlogController(repository, validator, markdownTextFormatter) {
  var COUNTRY_ID = 1;
  
  var _getPage = function _getPost(req) {
    return validator.validate(req).then(function() {
      return repository.getPage(COUNTRY_ID, req.params.name);
    }).then(function(page) {
      if (page) {
        page.bodyCopy = markdownTextFormatter.format(page.bodyCopy);
        return page;
      }
      
      return {};
    });
  };
  
  return {
    getPage: _getPage
  };
};