module.exports = function SocialController(repository, validator, avatarImageUrl, evidenceImageUrl, evidenceVideoUrl) {
  'use strict';
  
  var _formatResultEvidenceNotification = function _formatResultEvidenceNotification(notification) {
    notification.evidenceVideoUrl = null;
    notification.evidenceVideoUrl = null;
    
    if (notification.imageValidated) {
      notification.evidenceImageUrl = evidenceImageUrl + notification.resultId + '.png';
      delete notification.imageValidated;
    }
    
    if (notification.videoValidated) {
      notification.evidenceVideoUrl = evidenceVideoUrl + notification.resultId + '.mp4';
      delete notification.videoValidated;
    }
    
    return notification;
  };
  
  var _formatNotification = function _formatNotification(notification) {
    switch (notification.type) {
      case 'result-evidence':
        return _formatResultEvidenceNotification(notification);
    }
    
    return notification;
  };
  
  var _formatNotificationMessage = function _formatNotificationMessage(data) {
    var notification = _formatNotification(data.notification);
    
    notification.notificationId = data.id;
    notification.userId = data.userId;
    notification.notificationTimestamp = data.created;
    notification.avatar = avatarImageUrl + (data.avatarUploaded ? data.userId : 0) + '-170x170.png';
    
    return notification;
  };
  
  var _getNotifications = function(req) {
    return validator.validate(req).then(function() {
      return repository.getNotifications(req.params.userId, req.body.from, req.body.numberToReturn).then(function(data) {
        if (!data) {
          return [];
        }
        
        var notifications = [];
        
        for (var index = 0; index < data.length; index++) {
          notifications[index] = _formatNotificationMessage(data[index]);
        }
        
        return notifications;
      });
    });
  };
  
  return {
    getNotifications: _getNotifications
  };
};