var LocationLookup = require('../utilities/location-lookup');

module.exports = function LocationController(repository, validator, googleMapsService) {
  var locationLookup = new LocationLookup();

  var _getCountries = function _getCountries(req) {
    return repository.getCountries().then(function(countries) {
      if (!countries) {
        return [];
      }

      return countries.map(function(country) {
        country.isAvailable = country.isAvailable === '1' ? true : false;
        return country;
      });
    });
  };

  var _getRegions = function _getRegions(req) {
    return validator.validateGetRegions(req).then(function() {
      return repository.getRegions(req.params.countryId);
    }).then(function(data) {
      return data || [];
    });
  };

  var _getCounties = function _getCounties(req) {
    return validator.validateGetCounties(req).then(function() {
      return repository.getCounties(req.params.regionId);
    }).then(function(data) {
      return data || [];
    });
  };

  var _getDistricts = function _getDistricts(req) {
    return validator.validateGetDistricts(req).then(function() {
      return repository.getDistricts(req.params.countyId);
    }).then(function(data) {
      return data || [];
    });
  };

  var _locationLookup = function _locationLookup(req) {
    return validator.validateLocationLookup(req).then(function() {
      return [
        repository.getAllLocations(),
        googleMapsService.lookup(req.params.latlon),
      ];
    }).spread(function(locations, mapData) {
      return locationLookup.lookup(locations, mapData);
    });
  };

  return {
    getCountries: _getCountries,
    getRegions: _getRegions,
    getCounties: _getCounties,
    getDistricts: _getDistricts,
    locationLookup: _locationLookup
  };
};
