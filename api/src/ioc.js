var express = require('express');

// middleware
var securityMiddleware = require('./middleware/security');
var compressionMiddleware = require('compression');
var authenticationMiddleware = require('./middleware/authentication');
var formParserMiddleware = require('./middleware/form-parser');

module.exports = function IoC(config, db, logger) {
  // utilities
  var markdownTextFormatter = require('./utilities/markdown-text-formatter');

  // repositories
  var authRepository = new require('./repositories/auth')(db);
  var locationRepository = new require('./repositories/location')(db);
  var registerRepository = new require('./repositories/register')(db);
  var userRepository = new require('./repositories/user')(db);
  var resultRepository = new require('./repositories/result')(db);
  var leaderboardRepository = new require('./repositories/leaderboard')(db);
  var challengeRepository = new require('./repositories/challenge')(db);
  var leaderboardDateRepository = new require('./repositories/leaderboard-date')(db);
  var fitnessRepository = new require('./repositories/fitness')(db);
  var blogRepository = new require('./repositories/blog')(db);
  var pageRepository = new require('./repositories/page')(db);
  var socialRepository = new require('./repositories/social')(db);
  var nearMeRepository = new require('./repositories/near-me')(db);

  // validators
  var authValidator = new require('./validators/auth')();
  var locationValidator = new require('./validators/location')();
  var registerValidator = new require('./validators/register')();
  var userValidator = new require('./validators/user')();
  var resultValidator = new require('./validators/result')();
  var leaderboardValidator = new require('./validators/leaderboard')();
  var challengeValidator = new require('./validators/challenge')();
  var blogValidator = new require('./validators/blog')();
  var pageValidator = new require('./validators/page')();
  var socialValidator = new require('./validators/social')();
  var newsocialValidator = new require('./validators/newsocial')();
  var nearMeValidator = new require('./validators/near-me')();

  // services
  var passwordHashService = new require('./services/password-hash')(config.authSecretKey);
  var messageQueueService = require('./services/message-queue')(config);
  var leaderboardDateProviderService = new require('./services/leaderboard-date-provider')(config.isLeaderboardRealtime);
  var leaderboardDateService = new require('./services/leaderboard-date')(leaderboardDateRepository, leaderboardDateProviderService);
  var googleMapsService = new require('./services/google-maps')(config.googleMapsAPIServerKey);
  var snsService = new require('./services/sns')(config);

  // controllers
  var authController = new require('./controllers/auth')(authRepository, passwordHashService, authValidator, config.avatarImageUrl, messageQueueService);
  var locationController = new require('./controllers/location')(locationRepository, locationValidator, googleMapsService);
  var registerController = new require('./controllers/register')(registerRepository, passwordHashService, registerValidator, messageQueueService);
  var userController = new require('./controllers/user')(userRepository, userValidator, leaderboardDateService, config.avatarImageUrl, config.evidenceImageUrl, config.evidenceVideoUrl,messageQueueService, snsService);
  var resultController = new require('./controllers/result')(resultRepository, resultValidator, config.avatarImageUrl, config.evidenceImageUrl, config.evidenceVideoUrl, leaderboardDateService);
  var leaderboardController = new require('./controllers/leaderboard')(leaderboardRepository, leaderboardValidator, leaderboardDateService, config.avatarImageUrl, config.evidenceImageUrl, config.evidenceVideoUrl);
  var challengeController = new require('./controllers/challenge')(challengeRepository, challengeValidator, markdownTextFormatter);
  var fitnessController = new require('./controllers/fitness')(fitnessRepository);
  var blogController = new require('./controllers/blog')(blogRepository, blogValidator, markdownTextFormatter);
  var pageController = new require('./controllers/page')(pageRepository, pageValidator, markdownTextFormatter);
  var socialController = new require('./controllers/social')(socialRepository, socialValidator, config.avatarImageUrl, config.evidenceImageUrl, config.evidenceVideoUrl);
  var newSocialController = new require('./controllers/newsocial')(socialRepository, newsocialValidator, config.avatarImageUrl, config.evidenceImageUrl, config.evidenceVideoUrl, config.socialImageUrl, config.socialVideoUrl, messageQueueService);
  var nearMeController = new require('./controllers/near-me')(nearMeRepository, nearMeValidator,config.avatarImageUrl);

  var v1Router = express.Router();

  // attach routes
  new require('./routes/v1/auth')(v1Router, authController, logger);
  new require('./routes/v1/location')(v1Router, locationController, logger);
  new require('./routes/v1/register')(v1Router, registerController, logger);
  new require('./routes/v1/user')(v1Router, userController, logger);
  new require('./routes/v1/result')(v1Router, resultController, logger);
  new require('./routes/v1/leaderboard')(v1Router, leaderboardController, logger);
  new require('./routes/v1/challenge')(v1Router, challengeController, logger);
  new require('./routes/v1/fitness')(v1Router, fitnessController, logger);
  new require('./routes/v1/blog')(v1Router, blogController, logger);
  new require('./routes/v1/page')(v1Router, pageController, logger);
  new require('./routes/v1/social')(v1Router, socialController, logger, newSocialController);
  new require('./routes/v1/near-me')(v1Router, nearMeController, logger);

  return {
    logger: logger,
    config: config,
    middleware: {
      security: securityMiddleware(config.targetHost),
      compression: compressionMiddleware(),
      authentication: authenticationMiddleware(config.authKey),
      formParser: formParserMiddleware(),
      v1Router: v1Router
    }
  };
};