var express = require('express');

module.exports = function Server(ioc) {
  server = express();

  server.set('x-powered-by', false);

  if (ioc.config.isProduction) {
    server.enable('trust proxy');
    server.use(ioc.middleware.security);
  }

  server.use(ioc.middleware.compression);
  server.use(ioc.middleware.authentication);
  server.use(ioc.middleware.formParser);
  server.use('/v1/', ioc.middleware.v1Router);

  server.get('/', function(req, res) {
    res.status(200).end();
  });

  var _listen = function _listen(port) {
    server.listen(port, function listening() {
      ioc.logger.log('Listening on port %s.', port);
    });
  };

  return {
    listen: _listen
  };
};
