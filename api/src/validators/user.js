var util = require('./util');

module.exports = function UserValidator() {

  var _validateSetDistrict = function _validateSetDistrict(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (isNaN(req.body.districtId)) {
      validationErrors.push(['districtId', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);
  };

  var _validateSetWeight = function _validateSetWeight(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (isNaN(req.body.weight)) {
      validationErrors.push(['weight', 'Error_Field_Not_Int']);
    }

    if (req.body.weight < 5000) {
      validationErrors.push(['weight', 'Error_Field_Weight_Less_Than_5_kg']);
    }

    if (req.body.weight > 700000) {
      validationErrors.push(['weight', 'Error_Field_Weight_Greater_Than_700_kg']);
    }

    return util.validationPromise(validationErrors);
  };

  var _validateSetHeight = function _validateSetHeight(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (isNaN(req.body.height)) {
      validationErrors.push(['height', 'Error_Field_Not_Int']);
    }

    if (req.body.height < 5000) {
      validationErrors.push(['height', 'Error_Field_Height_Less_Than_50_cm']);
    }

    if (req.body.height > 30000) {
      validationErrors.push(['height', 'Error_Field_Height_Greater_Than_300_cm']);
    }

    return util.validationPromise(validationErrors);
  };

  var _validateSetIsMale = function _validateSetIsMale(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (!req.body.isMale) {
      validationErrors.push(['isMale', 'Error_Field_Blank']);
    }

    return util.validationPromise(validationErrors);
  };

  var _validateSetMobileNumber = function _validateSetMobileNumber(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (!req.body.mobileNumber) {
      validationErrors.push(['mobileNumber', 'Error_Field_Blank']);
    } 

    // Mobile number will soon change to become telephone number. These checks have been
    // disabled to allow all formats of number to be entered.
    // else {
    //   if (req.body.mobileNumber.length != 11) {
    //     validationErrors.push(['mobileNumber', 'Error_Field_Invalid_Mobile_Number']);
    //   } else if (isNaN(req.body.mobileNumber)) {
    //       validationErrors.push(['mobileNumber', 'Error_Field_Invalid_Mobile_Number']);
    //   } else if (req.body.mobileNumber.substring(0, 2) !== '07') {
    //     validationErrors.push(['mobileNumber', 'Error_Field_Invalid_Mobile_Number']);
    //   }
    // }

    return util.validationPromise(validationErrors);
  };

  var _validateSetGym = function _validateSetGym(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);
  };

  var _validateSetRoutineType = function _validateSetRoutineType(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (isNaN(req.body.routineTypeId)) {
      validationErrors.push(['routineTypeId', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);
  };

  var _validateSetPrivacyPreferences = function _validateSetPrivacyPreferences(privacyPreferences) {
    var validationErrors = [];

    if (isNaN(privacyPreferences.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (!privacyPreferences.gymFitEmailOptIn) {
      validationErrors.push(['gymFitEmailOptIn', 'Error_Field_Blank']);
    }

    if (!privacyPreferences.partnersEmailOptIn) {
      validationErrors.push(['partnersEmailOptIn', 'Error_Field_Blank']);
    }

    if (!privacyPreferences.gymFitSMSOptIn) {
      validationErrors.push(['gymFitSMSOptIn', 'Error_Field_Blank']);
    }

    if (!privacyPreferences.partnersSMSOptIn) {
      validationErrors.push(['partnersSMSOptIn', 'Error_Field_Blank']);
    }

    if (!privacyPreferences.gymFitOnlineOptIn) {
      validationErrors.push(['gymFitOnlineOptIn', 'Error_Field_Blank']);
    }

    if (!privacyPreferences.partnersOnlineOptIn) {
      validationErrors.push(['partnersOnlineOptIn', 'Error_Field_Blank']);
    }

    return util.validationPromise(validationErrors);
  };

  var _validateGetActivities = function _validateGetActivities(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);
  };

  var _validateGetNotifications = function _validateGetNotifications(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);
  };

  var _validateAcknowledgeNotification = function _validateAcknowledgeNotification(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (isNaN(req.params.notificationId)) {
      validationErrors.push(['notificationId', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);
  };

  var _validateGetUserProfile = function _validateGetUserProfile(req) {
    var validationErrors = [];

    if (!util.isValidUsernameFormat(req.params.username)) {
      validationErrors.push(['username', 'Error_Field_Invalid_Username']);
    } else if (req.params.username.length < 3) {
      validationErrors.push(['username', 'Error_Field_Must_Be_At_Least_3_Characters']);
    }

    return util.validationPromise(validationErrors);
  };
  
  var _validateFollow = function _validateFollow(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (isNaN(req.params.userIdToFollow)) {
      validationErrors.push(['userIdToFollow', 'Error_Field_Blank']);
    }

    return util.validationPromise(validationErrors);
  };
  
  var _validateUnfollow = function _validateUnfollow(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (isNaN(req.params.userIdToUnfollow)) {
      validationErrors.push(['userIdToUnfollow', 'Error_Field_Blank']);
    }

    return util.validationPromise(validationErrors);
  };
  
  var _validateSetClient = function _validateSetClient(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (isNaN(req.params.clientUserId)) {
      validationErrors.push(['clientUserId', 'Error_Field_Blank']);
    }

    return util.validationPromise(validationErrors);
  };
  
  var _validateUnsetClient = function _validateUnsetClient(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (isNaN(req.params.clientUserId)) {
      validationErrors.push(['clientUserId', 'Error_Field_Blank']);
    }

    return util.validationPromise(validationErrors);
  };
  
  var _validateSetFirstName = function _validateSetFirstName(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (util.isBlankOrWhitespace(req.body.firstName)) {
      validationErrors.push(['firstName', 'Error_Field_Blank']);
    }

    return util.validationPromise(validationErrors);
  };
  
  var _validateSetLastName = function _validateSetLastName(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    // We want to allow companys to user only the firstName field. This is a work around to the existing database
    // structure that didn't account for more than one type of user. In future this should be swapped out for a 'Company Name' field
    // if (util.isBlankOrWhitespace(req.body.lastName)) {
    //   validationErrors.push(['lastName', 'Error_Field_Blank']);
    // }

    return util.validationPromise(validationErrors);
  };
  
  var _validateSetBio = function _validateSetBio(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }
    
    return util.validationPromise(validationErrors);
  };
  
  var _validateGetProgressChartData = function _validateGetProgressChartData(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }
    
    if (isNaN(req.params.challengeId)) {
      validationErrors.push(['challengeId', 'Error_Field_Not_Int']);
    }
    
    if (isNaN(req.params.challengeGroupId)) {
      validationErrors.push(['challengeGroupId', 'Error_Field_Not_Int']);
    }
    
    return util.validationPromise(validationErrors);
  };

  var _validateSetIsGym = function(req){
    
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (util.isBlankOrWhitespace(req.body.isGym)) {
      validationErrors.push(['isGym', 'Error_Field_Blank']);
    }

    if (isNaN(req.body.isGym)) {
      validationErrors.push(['isGym', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);

  };

  var _validateSetIsPersonalTrainer = function(req){
    
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (util.isBlankOrWhitespace(req.body.isPersonalTrainer)) {
      validationErrors.push(['isPersonalTrainer', 'Error_Field_Blank']);
    }

    if (isNaN(req.body.isPersonalTrainer)) {
      validationErrors.push(['isPersonalTrainer', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);

  };

  var _validateSendPushNotification = function(req){
      
      var validationErrors = [];

      if (isNaN(req.params.userId)) {
        validationErrors.push(['userId', 'Error_Field_Not_Int']);
      }

      if (util.isBlankOrWhitespace(req.body.content)) {
        validationErrors.push(['content', 'Error_Field_Blank']);
      }

      if (req.body.badge){
        if (isNaN(req.body.badge)) {
          validationErrors.push(['badge', 'Error_Field_Not_Int']);
        }     
      }

      return util.validationPromise(validationErrors);

  };

  var _validateRegisterDeviceForPushNotification = function(req){
      
      var validationErrors = [];

      if (isNaN(req.params.userId)) {
        validationErrors.push(['userId', 'Error_Field_Not_Int']);
      }

      if (util.isBlankOrWhitespace(req.body.deviceId)) {
        validationErrors.push(['deviceId', 'Error_Field_Blank']);
      }
      
      if (util.isBlankOrWhitespace(req.body.deviceToken)) {
        validationErrors.push(['deviceToken', 'Error_Field_Blank']);
      }

      return util.validationPromise(validationErrors);

  };

  var _validateGetReviews = function _validateGetReviews(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (req.query.limit){
      if (isNaN(req.query.limit)) {
        validationErrors.push(['limit', 'Error_Field_Not_Int']);
      }
    }

    return util.validationPromise(validationErrors);
  };

  var _validateGetReview = function _validateGetReview(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (isNaN(req.params.reviewId)) {
      validationErrors.push(['reviewId', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);
  };

  var _validateSubmitReview = function _validateSubmitReview(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (util.isBlankOrWhitespace(req.body.reviewerUserId)) {
        validationErrors.push(['reviewerUserId', 'Error_Field_Blank']);
    }

    if (util.isBlankOrWhitespace(req.body.title)) {
        validationErrors.push(['title', 'Error_Field_Blank']);
    }

    if (util.isBlankOrWhitespace(req.body.body)) {
        validationErrors.push(['body', 'Error_Field_Blank']);
    }

    if (util.isBlankOrWhitespace(req.body.rating)) {
        validationErrors.push(['rating', 'Error_Field_Blank']);
    }

    if (isNaN(req.body.reviewerUserId)) {
      validationErrors.push(['reviewerUserId', 'Error_Field_Not_Int']);
    }

    if (isNaN(req.body.rating)) {
      validationErrors.push(['rating', 'Error_Field_Not_Int']);
    }

    if (req.body.rating > 5) {
      validationErrors.push(['rating', 'Error_Field_Invalid_Rating']);
    }

    return util.validationPromise(validationErrors);
  };

  var _validateDeleteReview = function _validateDeleteReview(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (isNaN(req.params.reviewId)) {
      validationErrors.push(['reviewId', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);
  };

  return {
    validateSetDistrict: _validateSetDistrict,
    validateSetWeight: _validateSetWeight,
    validateSetHeight: _validateSetHeight,
    validateSetIsMale: _validateSetIsMale,
    validateSetIsGym: _validateSetIsGym,
    validateSetIsPersonalTrainer: _validateSetIsPersonalTrainer,
    validateSetMobileNumber: _validateSetMobileNumber,
    validateSetGym: _validateSetGym,
    validateSetRoutineType: _validateSetRoutineType,
    validateSetPrivacyPreferences: _validateSetPrivacyPreferences,
    validateGetActivities: _validateGetActivities,
    validateGetNotifications: _validateGetNotifications,
    validateAcknowledgeNotification: _validateAcknowledgeNotification,
    validateGetUserProfile: _validateGetUserProfile,
    validateFollow: _validateFollow,
    validateUnfollow: _validateUnfollow,
    validateSetClient: _validateSetClient,
    validateUnsetClient: _validateUnsetClient,
    validateSetFirstName: _validateSetFirstName,
    validateSetLastName: _validateSetLastName,
    validateSetBio: _validateSetBio,
    validateGetProgressChartData: _validateGetProgressChartData,
    validateSendPushNotification: _validateSendPushNotification,
    validateRegisterDeviceForPushNotification: _validateRegisterDeviceForPushNotification,
    validateGetReviews: _validateGetReviews,
    validateGetReview: _validateGetReview,
    validateSubmitReview: _validateSubmitReview,
    validateDeleteReview: _validateDeleteReview

  };
};
