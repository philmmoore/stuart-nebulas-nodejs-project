var util = require('./util');

module.exports = function LeaderboardValidator() {
  var _validateCommonModel = function _validateCommonModel(validationErrors, model) {
    if (isNaN(model.challengeId)) {
      validationErrors.push(['challengeId', 'Error_Field_Not_Int']);
    }

    if (isNaN(model.challengeGroupId)) {
      validationErrors.push(['challengeGroupId', 'Error_Field_Not_Int']);
    }

    if (model.districtId && isNaN(model.districtId)) {
      validationErrors.push(['districtId', 'Error_Field_Not_Int']);
    }

    if (model.countyId && isNaN(model.countyId)) {
      validationErrors.push(['countyId', 'Error_Field_Not_Int']);
    }

    if (model.regionId && isNaN(model.regionId)) {
      validationErrors.push(['regionId', 'Error_Field_Not_Int']);
    }

    if (model.countryId && isNaN(model.countryId)) {
      validationErrors.push(['countryId', 'Error_Field_Not_Int']);
    }

    if (model.ageMin && isNaN(model.ageMin)) {
      validationErrors.push(['ageMin', 'Error_Field_Not_Int']);
    }

    if (model.ageMax && isNaN(model.ageMax)) {
      validationErrors.push(['ageMax', 'Error_Field_Not_Int']);
    }

    if (model.ageMin && model.ageMin < 16) {
      validationErrors.push(['ageMin', 'Error_Field_Age_Less_Than_16']);
    }

    if (model.ageMin && model.ageMin > 130) {
      validationErrors.push(['ageMin', 'Error_Field_Age_Greater_Than_130']);
    }

    if (model.ageMax && model.ageMax < 16) {
      validationErrors.push(['ageMax', 'Error_Field_Age_Less_Than_16']);
    }

    if (model.ageMax && model.ageMax > 130) {
      validationErrors.push(['ageMax', 'Error_Field_Age_Greater_Than_130']);
    }

    if (model.ageMin && model.ageMax && model.ageMin > model.ageMax) {
      validationErrors.push(['ageMax', 'Error_Field_Age_Max_Greater_Than_Min']);
    }

    if (model.weightMin && isNaN(model.weightMin)) {
      validationErrors.push(['weightMin', 'Error_Field_Not_Int']);
    }

    if (model.weightMax && isNaN(model.weightMax)) {
      validationErrors.push(['weightMax', 'Error_Field_Not_Int']);
    }

    if (model.weightMin && model.weightMin < 5000) {
      validationErrors.push(['weightMin', 'Error_Field_Weight_Less_Than_5_kg']);
    }

    if (model.weightMin && model.weightMin > 700000) {
      validationErrors.push(['weightMin', 'Error_Field_Weight_Greater_Than_700_kg']);
    }

    if (model.weightMax && model.weightMax < 5000) {
      validationErrors.push(['weightMax', 'Error_Field_Weight_Less_Than_5_kg']);
    }

    if (model.weightMax && model.weightMax > 700000) {
      validationErrors.push(['weightMax', 'Error_Field_Weight_Greater_Than_700_kg']);
    }

    if (model.weightMin && model.weightMax && model.weightMin > model.weightMax) {
      validationErrors.push(['weightMax', 'Error_Field_Weight_Max_Greater_Than_Min']);
    }
    
    if (model.userId && isNaN(model.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }
  };

  var _validateGetLeaderboard = function _validateGetLeaderboard(model) {
    var validationErrors = [];

    _validateCommonModel(validationErrors, model);

    if (isNaN(model.resultsPerPage)) {
      validationErrors.push(['resultsPerPage', 'Error_Field_Not_Int']);
    }

    if (isNaN(model.page)) {
      validationErrors.push(['page', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);
  };

  var _validateGetUserPosition = function _validateGetUserPosition(model) {
    var validationErrors = [];

    _validateCommonModel(validationErrors, model);

    return util.validationPromise(validationErrors);
  };

  return {
    validateGetLeaderboard: _validateGetLeaderboard,
    validateGetUserPosition: _validateGetUserPosition
  };
};
