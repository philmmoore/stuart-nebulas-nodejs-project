var util = require('./util');

module.exports = function AuthValidator() {
  var _validateLogin = function _validateLogin(req) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(req.body.emailOrUsername)) {
      validationErrors.push(['emailOrUsername', 'Error_Field_Blank']);
    }

    if (util.isBlankOrWhitespace(req.body.password)) {
      validationErrors.push(['password', 'Error_Field_Blank']);
    }

    return util.validationPromise(validationErrors);
  };
  
  var _validateChangePassword = function _validateChangePassword(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (util.isBlankOrWhitespace(req.body.oldPassword)) {
      validationErrors.push(['oldPassword', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(req.body.newPassword)) {
      validationErrors.push(['newPassword', 'Error_Field_Blank']);
    } else {
      if (req.body.newPassword.length < 5) {
        validationErrors.push(['newPassword', 'Error_Field_Must_Be_At_Least_5_Characters']);
      }
      
      if (req.body.newPassword.length > 256) {
        validationErrors.push(['newPassword', 'Error_Field_Too_Long']);
      }
      
      if (!req.body.newPassword.match(/\d/)) {
        validationErrors.push(['newPassword', 'Error_Password_Must_Contain_Numeric']);
      }
    }
    
    if (util.isBlankOrWhitespace(req.body.deviceId)) {
      validationErrors.push(['deviceId', 'Error_Field_Blank']);
    }
    
    return util.validationPromise(validationErrors);
  };

  var _validateResetPassword = function _validateResetPassword(req) {

    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (util.isBlankOrWhitespace(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Blank']);
    }

    return util.validationPromise(validationErrors);

  };



  var _validateUserExists = function _validateUserExists(req) {

    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (util.isBlankOrWhitespace(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Blank']);
    }

    return util.validationPromise(validationErrors);

  };

  var _validateEmailAddress = function _validateEmailAddress(req) {

    var validationErrors = [];

    if (util.isBlankOrWhitespace(req.body.email)) {
      validationErrors.push(['email', 'Error_Field_Blank']);
    }

    return util.validationPromise(validationErrors);

  };

  return {
    validateLogin: _validateLogin,
    validateChangePassword: _validateChangePassword,
    validateResetPassword: _validateResetPassword,
    validateUserExists: _validateUserExists,
    validateEmailAddress: _validateEmailAddress
  };
  
};
