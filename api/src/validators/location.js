var util = require('./util');

module.exports = function AuthValidator() {
  var _validateGetRegions = function _validateGetRegions(req) {
    var validationErrors = [];

    if (isNaN(req.params.countryId)) {
      validationErrors.push(['countryId', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);
  };

  var _validateGetCounties = function _validateGetCounties(req) {
    var validationErrors = [];

    if (isNaN(req.params.regionId)) {
      validationErrors.push(['regionId', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);
  };

  var _validateGetDistricts = function _validateGetDistricts(req) {
    var validationErrors = [];

    if (isNaN(req.params.countyId)) {
      validationErrors.push(['countyId', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);
  };

  var _validateLocationLookup = function _validateLocationLookup(req) {
    var validationErrors = [];

    if (req.params.latlon.indexOf(',') < 1) {
      validationErrors.push(['latlon', 'Error_Invalid_Lat_Lon']);
    } else {
      var coordinates = req.params.latlon.split(',');

      if (isNaN(coordinates[0]) || isNaN(coordinates[1])) {
        validationErrors.push(['latlon', 'Error_Invalid_Lat_Lon']);
      }
    }

    return util.validationPromise(validationErrors);
  };

  return {
    validateGetRegions: _validateGetRegions,
    validateGetCounties: _validateGetCounties,
    validateGetDistricts: _validateGetDistricts,
    validateLocationLookup: _validateLocationLookup
  };
};
