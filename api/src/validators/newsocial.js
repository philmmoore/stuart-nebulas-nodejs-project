var moment = require('moment');
var util = require('./util');

module.exports = function SocialValidator() {
  'use strict';
  
  var _validateGetGlobalUpdates = function _validateGetGlobalUpdates(req) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(req.body.from)) {
      validationErrors.push(['from', 'Error_Field_Blank']);
    } else if (!moment(req.body.from).isValid()) {
      validationErrors.push(['from', 'Error_Field_Invalid_Date']);
    }

    if (util.isBlankOrWhitespace(req.body.numberToReturn)) {
      validationErrors.push(['numberToReturn', 'Error_Field_Blank']);
    } else if (isNaN(req.body.numberToReturn)) {
      validationErrors.push(['numberToReturn', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);
  };
  
  var _validateGetUsersUpdates = function _validateGetUsersUpdates(req) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Blank']);
    } else if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }
    
    if (util.isBlankOrWhitespace(req.body.from)) {
      validationErrors.push(['from', 'Error_Field_Blank']);
    } else if (!moment(req.body.from).isValid()) {
      validationErrors.push(['from', 'Error_Field_Invalid_Date']);
    }

    if (util.isBlankOrWhitespace(req.body.numberToReturn)) {
      validationErrors.push(['numberToReturn', 'Error_Field_Blank']);
    } else if (isNaN(req.body.numberToReturn)) {
      validationErrors.push(['numberToReturn', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);
  };
  
  var _validateNewUpdate = function _validateNewUpdate(req) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Blank']);
    } else if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }
    
    return util.validationPromise(validationErrors);
  };
  
  var _validateLike = function _validateLike(req) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(req.body.userId)) {
      validationErrors.push(['userId', 'Error_Field_Blank']);
    } else if (isNaN(req.body.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }
    
    if (!util.isValidUUID(req.params.updateId)) {
      validationErrors.push(['updateId', 'Error_Field_Not_Valid_UUID']);
    }
    
    return util.validationPromise(validationErrors);
  };
  
  var _validateSearch = function _validateSearch(req) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(req.body.query)) {
      validationErrors.push(['query', 'Error_Field_Blank']);
    }
    
    return util.validationPromise(validationErrors);
  };
  
  return {
    validateGetGlobalUpdates: _validateGetGlobalUpdates,
    validateGetUsersUpdates: _validateGetUsersUpdates,
    validateNewUpdate: _validateNewUpdate,
    validateLike: _validateLike,
    validateUnlike: _validateLike,
    validateFlag: _validateLike,
    validateUnflag: _validateLike,
    validateAttachImage: _validateLike,
    validateAttachVideo: _validateLike,
    validateSearch: _validateSearch
  };
};