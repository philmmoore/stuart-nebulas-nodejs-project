var moment = require('moment');
var util = require('./util');

module.exports = function SocialValidator() {
  'use strict';
  
  var _validate = function _validate(req) {
    var validationErrors = [];

    if (isNaN(req.params.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }
    
    if (util.isBlankOrWhitespace(req.body.from)) {
      validationErrors.push(['from', 'Error_Field_Blank']);
    } else if (!moment(req.body.from).isValid()) {
      validationErrors.push(['from', 'Error_Field_Invalid_Date']);
    }

    if (isNaN(req.body.numberToReturn)) {
      validationErrors.push(['numberToReturn', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);
  };

  return {
    validate: _validate
  };
};