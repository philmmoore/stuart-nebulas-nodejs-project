var util = require('./util');
var moment = require('moment');

module.exports = function RegisterValidator() {
  var _validateEmail = function _validateEmail(req) {
    var validationErrors = [];

    if (!util.isEmailValid(req.params.email)) {
      validationErrors.push(['email', 'Error_Field_Invalid_Email']);
    } else if (util.isEmailDisposable(req.params.email)) {
      validationErrors.push(['email', 'Error_Field_Email_Is_Disposable']);
    }

    return util.validationPromise(validationErrors);
  };

  var _validateUsername = function _validateUsername(req) {
    var validationErrors = [];

    if (!util.isValidUsernameFormat(req.params.username)) {
      validationErrors.push(['username', 'Error_Field_Invalid_Username']);
    } else if (req.params.username.length < 3) {
      validationErrors.push(['username', 'Error_Field_Must_Be_At_Least_3_Characters']);
    }

    return util.validationPromise(validationErrors);
  };

  var _validateDateOfBirth = function _validateDateOfBirth(dateOfBirth, validationErrors) {
    if (util.isBlankOrWhitespace(dateOfBirth)) {
      validationErrors.push(['dateOfBirth', 'Error_Field_Blank']);
      return;
    }

    var parsedDateOfBirth = moment(dateOfBirth);

    if (!parsedDateOfBirth.isValid()) {
      validationErrors.push(['dateOfBirth', 'Error_Field_Invalid_Date']);
      return;
    }

    if (parsedDateOfBirth.isAfter()) {
      validationErrors.push(['dateOfBirth', 'Error_Field_Date_cannot_Be_In_Future']);
      return;
    }

    if (moment().year() - parsedDateOfBirth.year() > 130) {
      validationErrors.push(['dateOfBirth', 'Error_Field_Age_Cannot_Be_Greater_Than_130']);
      return;
    }

    if (moment().year() - parsedDateOfBirth.year() < 16) {
      validationErrors.push(['dateOfBirth', 'Error_Field_Age_Too_Young']);
      return;
    }
  };

  var _validateRegistrationDetails = function _validateRegistrationDetails(registrationDetails) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(registrationDetails.username)) {
      validationErrors.push(['username', 'Error_Field_Blank']);
    } else if (!util.isValidUsernameFormat(registrationDetails.username)) {
      validationErrors.push(['username', 'Error_Field_Invalid_Username']);
    } else if (registrationDetails.username.length < 3) {
      validationErrors.push(['username', 'Error_Field_Must_Be_At_Least_3_Characters']);
    }

    if (util.isBlankOrWhitespace(registrationDetails.email)) {
      validationErrors.push(['email', 'Error_Field_Blank']);
    } else if (!util.isEmailValid(registrationDetails.email)) {
      validationErrors.push(['email', 'Error_Field_Invalid_Email']);
    } else if (util.isEmailDisposable(registrationDetails.email)) {
      validationErrors.push(['email', 'Error_Field_Email_Is_Disposable']);
    }

    if (util.isBlankOrWhitespace(registrationDetails.password)) {
      validationErrors.push(['password', 'Error_Field_Blank']);
    } else if (registrationDetails.password.length < 5) {
      validationErrors.push(['password', 'Error_Field_Must_Be_At_Least_5_Characters']);
    }

    if (util.isBlankOrWhitespace(registrationDetails.firstName)) {
      validationErrors.push(['firstName', 'Error_Field_Blank']);
    }

    _validateDateOfBirth(registrationDetails.dateOfBirth, validationErrors);

    return util.validationPromise(validationErrors);
  };

  var _validateTermsAndConditions = function _validateTermsAndConditions(req) {
    var validationErrors = [];

    if (isNaN(req.params.countryId)) {
      validationErrors.push(['countryId', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);
  };

  return {
    validateEmail: _validateEmail,
    validateUsername: _validateUsername,
    validateRegistrationDetails: _validateRegistrationDetails,
    validateTermsAndConditions: _validateTermsAndConditions
  };
};
