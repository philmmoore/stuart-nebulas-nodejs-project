var util = require('./util');

module.exports = function BlogValidator() {
  var _validateGetPosts = function _validateGetPosts(req) {
    var validationErrors = [];

    if (isNaN(req.body.countryId)) {
      validationErrors.push(['countryId', 'Error_Field_Not_Int']);
    }
    
    if (isNaN(req.body.postsPerPage)) {
      validationErrors.push(['postsPerPage', 'Error_Field_Not_Int']);
    }
    
    if (isNaN(req.body.pageNumber)) {
      validationErrors.push(['pageNumber', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);
  };
  
  var _validateGetPost = function _validateGetPost(req) {
    var validationErrors = [];

    if (isNaN(req.params.postId)) {
      validationErrors.push(['postId', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);
  };

  return {
    validateGetPosts: _validateGetPosts,
    validateGetPost: _validateGetPost
  };
};