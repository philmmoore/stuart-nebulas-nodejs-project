var util = require('./util');

module.exports = function BlogValidator() {
  var _validate = function _validate(req) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(req.params.name)) {
      validationErrors.push(['name', 'Error_Field_Blank']);
    }

    return util.validationPromise(validationErrors);
  };

  return {
    validate: _validate
  };
};