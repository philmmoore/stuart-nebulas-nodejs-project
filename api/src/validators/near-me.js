var util = require('./util');

module.exports = function NearMeValidator() {

  var _validateGetNearMe = function _validateGetNearMe(req) {
    var validationErrors = [];

    if (isNaN(req.body.latitude)) {
      validationErrors.push(['latitude', 'Error_Field_Not_Int']);
    }

    if (isNaN(req.body.longitude)) {
      validationErrors.push(['longitude', 'Error_Field_Not_Int']);
    }

    if (req.body.radius){
      if (isNaN(req.body.radius)) {
        validationErrors.push(['radius', 'Error_Field_Not_Int']);
      }
      if (util.isBlankOrWhitespace(req.body.radius)) {
        validationErrors.push(['radius', 'Error_Field_Blank']);
      }
    }

    if (req.body.isPersonalTrainer){
      if (util.isBlankOrWhitespace(req.body.isPersonalTrainer)) {
        validationErrors.push(['isPersonalTrainer', 'Error_Field_Blank']);
      }
      if (!util.isBool(req.body.isPersonalTrainer)) {
        validationErrors.push(['isPersonalTrainer', 'Error_Field_Not_Bool']);
      }
    }

    if (req.body.isGym){
      if (util.isBlankOrWhitespace(req.body.isGym)) {
        validationErrors.push(['isGym', 'Error_Field_Blank']);
      }
      if (!util.isBool(req.body.isGym)) {
        validationErrors.push(['isGym', 'Error_Field_Not_Bool']);
      }
    }

    if (req.body.userId){
      if (isNaN(req.body.userId)) {
        validationErrors.push(['userId', 'Error_Field_Not_Int']);
      }
      if (util.isBlankOrWhitespace(req.body.userId)) {
        validationErrors.push(['userId', 'Error_Field_Blank']);
      }
    }

    if (req.body.showFollowing){
      if (util.isBlankOrWhitespace(req.body.showFollowing)) {
        validationErrors.push(['showFollowing', 'Error_Field_Blank']);
      }
      if (!util.isBool(req.body.showFollowing)) {
        validationErrors.push(['showFollowing', 'Error_Field_Not_Bool']);
      }
    }    

    return util.validationPromise(validationErrors);
  };

  return {
    validateGetNearMe: _validateGetNearMe
  };
};
