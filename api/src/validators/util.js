var Promise = require('bluebird');
var emailValidator = require('email-validator');
var isDisposableEmail = require('is-disposable-email');

module.exports.isBlankOrWhitespace = function isBlankOrWhitespace(string) {
  if (!string) {
    return true;
  }

  return string.length < 1 || /^\s+$/.test(string);
};

module.exports.isEmailValid = function isEmailValid(email) {
  return emailValidator.validate(email);
};

module.exports.isEmailDisposable = function isEmailDisposable(email) {
  return isDisposableEmail(email);
};

module.exports.isValidUsernameFormat = function isValidUsernameFormat(string) {
  return /^[a-zA-Z0-9-_]+$/.test(string);
};

module.exports.isValidUUID = function isValidUUID(uuid) {
  return /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i.test(uuid);
};

module.exports.isBool = function isBool(bool) {
  if (bool == "true" || bool == "false"){
    return true; 
  } else {
    return false;
  }
};

module.exports.validationPromise = function validationPromise(validationErrors) {
  return new Promise(function(resolve, reject) {
    if (Object.keys(validationErrors).length > 0) {
      return reject({
        errors: validationErrors
      });
    }

    resolve();
  });
};
