var util = require('./util');

module.exports = function LeaderboardValidator() {
  var _validateGetGroups = function _validateGetGroups(req) {
    var validationErrors = [];

    if (isNaN(req.body.challengeId)) {
      validationErrors.push(['challengeId', 'Error_Field_Not_Int']);
    }

    if (req.body.userId && isNaN(req.body.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);
  };

  var _validateGetChallengesForCategory = function _validateGetChallengesForCategory(req) {
    var validationErrors = [];

    if (isNaN(req.params.categoryId)) {
      validationErrors.push(['categoryId', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);
  };

  return {
    validateGetGroups: _validateGetGroups,
    validateGetChallengesForCategory: _validateGetChallengesForCategory
  };
};
