var util = require('./util');

module.exports = function ResultValidator() {
  var _validateSaveResult = function _validateSaveResult(req) {
    var validationErrors = [];

    if (isNaN(req.body.userId)) {
      validationErrors.push(['userId', 'Error_Field_Not_Int']);
    }

    if (isNaN(req.body.challengeId)) {
      validationErrors.push(['challengeId', 'Error_Field_Not_Int']);
    }

    if (isNaN(req.body.challengeGroupId)) {
      validationErrors.push(['challengeGroupId', 'Error_Field_Not_Int']);
    }

    if (isNaN(req.body.result)) {
      validationErrors.push(['result', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);
  };

  var _validateGetResult = function _validateGetResult(req) {
    var validationErrors = [];

    if (isNaN(req.params.resultId)) {
      validationErrors.push(['resultId', 'Error_Field_Not_Int']);
    }

    return util.validationPromise(validationErrors);
  };

  return {
    validateSaveResult: _validateSaveResult,
    validateGetResult: _validateGetResult
  };
};
