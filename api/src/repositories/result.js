module.exports = function ResultRepository(db) {
  db.registerParameters('result_submit', [ 'int', 'int', 'int', 'int' ]);
  db.registerParameters('result_get', [ 'int', 'timestamp', 'timestamp', 'timestamp', 'timestamp' ]);
  
  var _handleSaveResultError = function _handleSaveResultError(err) {
    if (err.constraint === 'result_challenge_group_id_fkey') {
      return [['challengeGroupId' , 'Challenge_Group_Invalid']];
    }

    if (err.constraint === 'result_challenge_id_fkey') {
      return [['challengeId' , 'Challenge_Invalid']];
    }
  };

  var _saveResult = function _saveResult(userId, challengeId, challengeGroupId, result) {
    var params = [
      userId,
      challengeId,
      challengeGroupId,
      result
    ];

    return db.queryJson('result_submit', params, _handleSaveResultError).then(function(data) {
      return {
        resultId: data
      };
    });
  };

  var _getResult = function _getResult(resultId, currentPeriodStart, currentPeriodEnd, lastPeriodStart, lastPeriodEnd) {
    var params = [
      resultId,
      currentPeriodStart,
      currentPeriodEnd,
      lastPeriodStart,
      lastPeriodEnd
    ];
    
    return db.queryJson('result_get', params).then(function(data) {
      if (!data) {
        return null;
      }

      data.avatarUploaded = data.avatarUploaded === '1';
      return data;
    });
  };

  return {
    saveResult: _saveResult,
    getResult: _getResult
  };
};
