module.exports = function LeaderboardDateRepository(db) {
  db.registerParameters('configuration_get', [ 'int' ]);
  
  var _getConfiguration = function _getConfiguration(countryId) {
    return db.queryJson('configuration_get', [ countryId ]);
  };

  return {
    getConfiguration: _getConfiguration
  };
};
