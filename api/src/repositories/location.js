module.exports = function LocationRepository(db) {
  db.registerParameters('location_get_regions', [ 'int' ]);
  db.registerParameters('location_get_counties', [ 'int' ]);
  db.registerParameters('location_get_districts', [ 'int' ]);
  
  var _getCountries = function _getCountries() {
    return db.queryJson('location_get_countries');
  };

  var _getRegions = function _getRegions(countryId) {
    return db.queryJson('location_get_regions', [ countryId ]);
  };

  var _getCounties = function _getCounties(regionId) {
    return db.queryJson('location_get_counties', [ regionId ]);
  };

  var _getDistricts = function _getDistricts(countyId) {
    return db.queryJson('location_get_districts', [ countyId ]);
  };

  var _getAllLocations = function _getAllLocations() {
    return db.queryJson('location_get_all_names');
  };

  return {
    getCountries: _getCountries,
    getRegions: _getRegions,
    getCounties: _getCounties,
    getDistricts: _getDistricts,
    getAllLocations: _getAllLocations
  };
};
