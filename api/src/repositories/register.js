module.exports = function RegisterRepository(db) {
  db.registerParameters('register_is_email_registered', [ 'text' ]);
  db.registerParameters('register_is_username_registered', [ 'text' ]);
  db.registerParameters('register_user', [ 'text', 'bytea', 'text', 'text', 'date', 'text' ]);
  db.registerParameters('register_get_terms_and_conditions', [ 'int' ]);
  
  var _getIsEmailRegistered = function _getIsEmailRegistered(email) {
    return db.queryJson('register_is_email_registered', [ email ]);
  };

  var _getIsUsernameRegistered = function _getIsUsernameRegistered(username) {
    return db.queryJson('register_is_username_registered', [ username ]);
  };

  var _registerUserErrorHandler = function _registerUserErrorHandler(err) {
    if (err.constraint === 'user_email_key') {
      return [['email', 'Email_Already_Registered']];
    }

    if (err.constraint === 'user_unique_username') {
      return [['username', 'Username_Already_Registered']];
    }
  };

  var _registerUser = function _registerUser(registrationDetails) {
    var params = [
      registrationDetails.username,
      registrationDetails.hash,
      registrationDetails.firstName,
      registrationDetails.lastName,
      registrationDetails.dateOfBirth,
      registrationDetails.email
    ];

    return db.queryJson('register_user', params, _registerUserErrorHandler);
  };

  var _getTermsAndConditions = function _getTermsAndConditions(countryId) {
    return db.queryJson('register_get_terms_and_conditions', [ countryId ]);
  };

  return {
    getIsEmailRegistered: _getIsEmailRegistered,
    getIsUsernameRegistered: _getIsUsernameRegistered,
    registerUser: _registerUser,
    getTermsAndConditions: _getTermsAndConditions
  };
};
