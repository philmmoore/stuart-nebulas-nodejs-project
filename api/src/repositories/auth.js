var Promise = require('bluebird');
var moment = require('moment');

module.exports = function AuthRepository(db) {
  db.registerParameters('auth_login', [ 'text', 'bytea', 'text' ]);
  db.registerParameters('auth_change_password', [ 'int', 'bytea', 'bytea', 'text' ]);
  db.registerParameters('auth_password_reset', [ 'int' ]);
  db.registerParameters('auth_recover_username', [ 'text' ]);
  db.registerParameters('auth_user_exists', [ 'int' ]);
  db.registerParameters('register_is_email_registered', [ 'text' ]);
  
  var _login = function _login(emailOrUsername, hash, deviceId) {
    var params = [
      emailOrUsername,
      hash,
      deviceId
    ];

    return db.queryJson('auth_login', params).then(function(data) {
      if (data) {
        data.isMale = data.isMale === '1' ? true : (data.isMale === '0' ? false : null);
        data.avatarUploaded = data.avatarUploaded === '1' ? true : (data.avatarUploaded === '0' ? false : null);
        data.dateOfBirth = new moment(data.dateOfBirth).format('DD/MM/YYYY');
      }
      
      return data;
    });
  };
  
  var _changePassword = function _changePassword(userId, oldHash, newHash, deviceId) {
    var params = [
      userId,
      oldHash,
      newHash,
      deviceId
    ];
    
    return db.queryJson('auth_change_password', params).then(function(result) {
      if (!result) {
        return Promise.reject({
          errors: [['userId', 'User_Invalid_Or_Current_Password_Incorrect']]
        });
      }

      return Promise.resolve(result.email);
    });
  };

  var _resetPassword = function _resetPassword(userId) {
    return db.queryJson('auth_password_reset', [ userId ]).then(function(result) {
      if (!result) {
        return Promise.reject({
          errors: [['userId', 'User_Invalid']]
        });
      }
      return Promise.resolve(result);
    });
  };

  var _recoverUsername = function _recoverUsername(email) {
     return db.queryJson('auth_recover_username', [ email ]).then(function(result){
      return Promise.resolve({
        email: result.email,
        username: result.username
      });
     });
  };

  var _userExistsByEmail = function _userExistsByEmail(email) {
     return db.queryJson('register_is_email_registered', [ email ]).then(function(result){
      return Promise.resolve({
        exists: (result && result[0].result ? true : false)
      });
     });
  };

  var _userExists = function _userExists(userId) {
     return db.queryJson('auth_user_exists', [ userId ]).then(function(result){
      return Promise.resolve({
        exists: (result.count ? true : false)
      });
     });
  };

  return {
    login: _login,
    changePassword: _changePassword,
    resetPassword: _resetPassword,
    recoverUsername: _recoverUsername,
    userExists: _userExists,
    userExistsByEmail: _userExistsByEmail
  };
};
