module.exports = function LeaderboardRepository(db) {
  db.registerParameters('leaderboard_search', [ 'int', 'int', 'int', 'int', 'int', 'int', 'timestamp', 'timestamp', 'timestamp', 'timestamp', 'int', 'int', 'bit', 'smallint', 'smallint', 'int', 'int', 'bit', 'int' ]);
  db.registerParameters('leaderboard_user_position', [ 'int', 'int', 'int', 'int', 'int', 'int', 'timestamp', 'timestamp', 'bit', 'smallint', 'smallint', 'int', 'int', 'bit' ]);
  
  var _getLeaderboard = function _getLeaderboard(model) {
    var params = [
      model.challengeId,
      model.challengeGroupId,
      model.districtId,
      model.countyId,
      model.regionId,
      model.countryId,
      model.currentPeriodStart,
      model.currentPeriodEnd,
      model.lastPeriodStart,
      model.lastPeriodEnd,
      model.resultsPerPage,
      model.page,
      model.isMale === null || model.isMale === undefined ? null : (model.isMale.toLowerCase() === 'true' ? 1 :0),
      model.ageMin || null,
      model.ageMax || null,
      model.weightMin || null,
      model.weightMax || null,
      model.onlyShowValidated === null || model.onlyShowValidated === undefined ? null : (model.onlyShowValidated ? 1 :0),
      model.userId
    ];
    
    return db.queryJson('leaderboard_search', params).then(function (data) {
      if (data && data.results) {
        for (var index = 0; index < data.results.length; index++) {
          var result = data.results[index];
          result.userHasAvatar = result.userHasAvatar ? true : false;
        }
      }

      return data;
    });
  };

  var _getUserPosition = function _getUserPosition(model) {
    var params = [
      model.challengeId,
      model.challengeGroupId,
      model.districtId,
      model.countyId,
      model.regionId,
      model.countryId,
      model.currentPeriodStart,
      model.currentPeriodEnd,
      model.isMale === null || model.isMale === undefined ? null : (model.isMale ? 1 :0),
      model.ageMin || null,
      model.ageMax || null,
      model.weightMin || null,
      model.weightMax || null,
      model.onlyShowValidated === null || model.onlyShowValidated === undefined ? null : (model.onlyShowValidated ? 1 :0)
    ];

    return db.queryJson('leaderboard_user_position', params).then(function (data) {
      if (data.rankings.length > 0) {
        return data.rankings;
      }

      return null;
    });
  };

  return {
    getLeaderboard: _getLeaderboard,
    getUserPosition: _getUserPosition
  };
};
