module.exports = function FitnessRepository(db) {
  var _getRoutineTypes = function _getRoutineTypes() {
    return db.queryJson('routine_type_get');
  };

  return {
    getRoutineTypes: _getRoutineTypes
  };
};
