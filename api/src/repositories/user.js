var Promise = require('bluebird');

module.exports = function UserRepository(db) {
  db.registerParameters('user_set_district', [ 'int', 'int' ]);
  db.registerParameters('user_set_weight', [ 'int', 'int' ]);
  db.registerParameters('user_set_height', [ 'int', 'smallint' ]);
  db.registerParameters('user_set_is_male', [ 'int', 'bit' ]);
  db.registerParameters('user_set_is_gym', [ 'int', 'bool' ]);
  db.registerParameters('user_set_is_personal_trainer', [ 'int', 'bool' ]);
  db.registerParameters('user_set_mobile', [ 'int', 'text' ]);
  db.registerParameters('user_set_gym', [ 'int', 'text' ]);
  db.registerParameters('user_set_routine', [ 'int', 'int' ]);
  db.registerParameters('user_set_privacy', [ 'int', 'bool', 'bool', 'bool', 'bool', 'bool', 'bool' ]);
  db.registerParameters('user_activity', [ 'int', 'timestamp', 'timestamp', 'timestamp', 'timestamp' ]);
  db.registerParameters('user_get_notifications', [ 'int' ]);
  db.registerParameters('user_acknowledge_notification', [ 'int', 'int' ]);
  db.registerParameters('user_get_details', [ 'text' ]);
  db.registerParameters('user_search', [ 'text' ]);
  db.registerParameters('user_follow', [ 'int', 'int' ]);
  db.registerParameters('user_unfollow', [ 'int', 'int' ]);
  db.registerParameters('user_set_client', [ 'int', 'int' ]);
  db.registerParameters('user_unset_client', [ 'int', 'int' ]);
  db.registerParameters('user_set_first_name', [ 'int', 'text' ]);
  db.registerParameters('user_set_last_name', [ 'int', 'text' ]);
  db.registerParameters('user_set_bio', [ 'int', 'text' ]);
  db.registerParameters('challenge_activity_chart', [ 'int', 'int', 'int' ]);
  db.registerParameters('featured_user_get', []);
  db.registerParameters('user_register_device_for_push_notification', ['int', 'text', 'text', 'text']);
  db.registerParameters('user_reviews', ['int', 'int']);
  db.registerParameters('user_review', ['int', 'int']);
  db.registerParameters('user_review_submit', [ 'int', 'int', 'text', 'text', 'int']);
  db.registerParameters('user_review_delete', [ 'int', 'int']);
  

  var _handleSetDistrictError = function _handleSetDistrictError(err) {
    if (err.constraint === 'user_district_user_id_fkey') {
      return [['userId', 'User_Invalid']];
    }

    if (err.constraint === 'user_district_district_id_fkey') {
      return [['districtId', 'District_Invalid']];
    }
  };

  var _setDistrict = function _setDistrict(userId, districtId) {
    return db.queryJson('user_set_district', [ userId, districtId ], _handleSetDistrictError);
  };

  var _handleSetWeightError = function _handleSetWeightError(err) {
    if (err.constraint === 'user_weight_user_id_fkey') {
      return [['userId', 'User_Invalid']];
    }
  };

  var _setWeight = function _setWeight(userId, weight) {
    return db.queryJson('user_set_weight', [ userId, weight ], _handleSetWeightError);
  };

  var _handleSetHeightError = function _handleSetHeightError(err) {
    if (err.constraint === 'user_height_user_id_fkey') {
      return [['userId', 'User_Invalid']];
    }
  };

  var _setHeight = function _setHeight(userId, height) {
    return db.queryJson('user_set_height', [ userId, height ], _handleSetHeightError);
  };

  var _setIsMale = function _setIsMale(userId, isMale) {
    return db.queryJson('user_set_is_male', [ userId, isMale ? '1' : '0' ]).then(function(result) {
      if (result !== 1) {
        return Promise.reject({
          errors: [['userId', 'User_Invalid']]
        });
      }

      return Promise.resolve();
    });
  };

  var _setMobileNumber = function _setMobileNumber(userId, mobileNumber) {
    return db.queryJson('user_set_mobile', [ userId, mobileNumber ]).then(function(result) {
      if (result !== 1) {
        return Promise.reject({
          errors: [['userId', 'User_Invalid']]
        });
      }

      return Promise.resolve();
    });
  };

  var _handleSetGymError = function _handleSetGymError(err) {
    if (err.constraint === 'user_gym_user_id_fkey') {
      return [['userId', 'User_Invalid']];
    }
  };

  var _setGym = function _setGym(userId, gym) {
    return db.queryJson('user_set_gym', [ userId, gym ], _handleSetGymError);
  };

  var _handleSetRoutineTypeError = function _handleSetRoutineTypeError(err) {
    if (err.constraint === 'user_routine_type_user_id_fkey') {
      return [['userId', 'User_Invalid']];
    }

    if (err.constraint === 'user_routine_type_routine_type_id_fkey') {
      return [['routineTypeId', 'Invalid_Routine_Type_Id']];
    }
  };

  var _setRoutineType = function _setRoutineType(userId, routineTypeId) {
    return db.queryJson('user_set_routine', [ userId, routineTypeId ], _handleSetRoutineTypeError);
  };

  var _setPrivacyPreferences = function _setPrivacyPreferences(privacyPreferences) {
    var params = [
      privacyPreferences.userId,
      privacyPreferences.gymFitEmailOptIn,
      privacyPreferences.partnersEmailOptIn,
      privacyPreferences.gymFitSMSOptIn,
      privacyPreferences.partnersSMSOptIn,
      privacyPreferences.gymFitOnlineOptIn,
      privacyPreferences.partnersOnlineOptIn
    ];

    return db.queryJson('user_set_privacy', params);
  };

  var _getActivities = function _getActivities(userId, currentPeriodStart, currentPeriodEnd, previousPeriodStart, previousPeriodEnd) {
    var params = [
      userId,
      currentPeriodStart,
      currentPeriodEnd,
      previousPeriodStart,
      previousPeriodEnd
    ];

    return db.queryJson('user_activity', params);
  };

  var _getNotifications = function _getNotifications(userId) {
    return db.queryJson('user_get_notifications', [ userId ]).then(function(data) {
      if (!data) {
        return [];
      }

      for (var index = 0; index < data.length; index++) {
        data[index].isUserDismissable = '1' ? true : false;
      }

      return data;
    });
  };

  var _acknowledgeNotification = function _acknowledgeNotification(userId, notificationId) {
    return db.queryJson('user_acknowledge_notification', [ userId, notificationId ]).then(function(result) {
      if (result !== 1) {
        return Promise.reject({
          errors: [['userId', 'Notification_Or_User_Id_Invalid']]
        });
      }
    });
  };

  var _getUserProfile = function _getUserProfile(username) {
    return db.queryJson('user_get_details', [ username ]).then(function (data) {
      if (!data) {
        return null;
      }

      data.isMale = data.isMale === '1';
      data.avatarUploaded = data.avatarUploaded === '1';
      return data;
    });
  };

  var _search = function _search(query) {
    return db.queryJson('user_search', [ query ]);
  };
  
  var _handleFollowError = function _handleFollowError(err) {
    if (err.constraint === 'user_follower_follower_user_id_fkey') {
      return [['userId', 'User_Invalid']];
    }

    if (err.constraint === 'user_follower_user_id_fkey') {
      return [['userIdToFollow', 'User_Invalid']];
    }
  };
  
  var _follow = function _follow(userId, userIdToFollow) {
    return db.queryJson('user_follow', [ userId, userIdToFollow ], _handleFollowError);
  };
  
  var _unfollow = function _unfollow(userId, userIdToUnfollow) {
    return db.queryJson('user_unfollow', [ userId, userIdToUnfollow ]);
  };
  
  var _handleSetClientError = function _handleSetClientError(err) {
    if (err.constraint === 'user_client_user_id_fkey') {
      return [['userId', 'User_Invalid']];
    }

    if (err.constraint === 'user_client_client_user_id_fkey') {
      return [['clientUserId', 'User_Invalid']];
    }
  };
  
  var _setClient = function _setClient(userId, clientUserId) {
    return db.queryJson('user_set_client', [ userId, clientUserId ], _handleSetClientError);
  };
  
  var _unsetClient = function _unsetClient(userId, clientUserId) {
    return db.queryJson('user_unset_client', [ userId, clientUserId ]);
  };
  
  var _setFirstName = function _setFirstName(userId, firstName) {
    return db.queryJson('user_set_first_name', [ userId, firstName ]).then(function(result) {
      if (result !== true) {
        return Promise.reject({
          errors: [['userId', 'User_Invalid']]
        });
      }

      return Promise.resolve();
    });
  };
  
  var _setLastName = function _setLastName(userId, lastName) {
    return db.queryJson('user_set_last_name', [ userId, lastName ]).then(function(result) {
      if (result !== true) {
        return Promise.reject({
          errors: [['userId', 'User_Invalid']]
        });
      }

      return Promise.resolve();
    });
  };
  
  var _setBio = function _setBio(userId, bio) {
    return db.queryJson('user_set_bio', [ userId, bio ]).then(function(result) {
      if (result !== true) {
        return Promise.reject({
          errors: [['userId', 'User_Invalid']]
        });
      }

      return Promise.resolve();
    });
  };
  
  var _getProgressChartData = function _getProgressChartData(userId, challengeId, challengeGroupId) {
    return db.queryJson('challenge_activity_chart', [ userId, challengeId, challengeGroupId ]).then(function (data) {
      if (!data) {
        return Promise.reject({
          errors: [['challengeId', 'Not_Found']]
        });
      }
      
      if (!data.results) {
        data.results = [];
      }
      
      return data;
    });
  };

  var _setIsGym = function _setIsGym (userId, isGym) {
    return db.queryJson('user_set_is_gym', [ userId, isGym ]).then(function(result) {
      if (!result) {
        return Promise.reject({
          errors: [['userId', 'User_Invalid']]
        });
      }
      return Promise.resolve();
    });
  };

  var _setIsPersonalTrainer = function _setIsPersonalTrainer (userId, isPersonalTrainer) {
    return db.queryJson('user_set_is_personal_trainer', [ userId, isPersonalTrainer ]).then(function(result) {
      if (!result) {
        return Promise.reject({
          errors: [['userId', 'User_Invalid']]
        });
      }
      return Promise.resolve();
    });
  };

  var _registerDeviceForPushNotification = function _registerDeviceForPushNotification (userId, deviceId, deviceToken, endpoint) {
    return db.queryJson('user_register_device_for_push_notification', [ userId, deviceId, deviceToken, endpoint]).then(function(result) {
      if (!result) {
        return Promise.reject({
          errors: [['userId', 'User_Invalid']]
        });
      }
      return Promise.resolve();
    });
  };
  
  var _getFeatured = function _getFeatured() {
    return db.queryJson('featured_user_get', []).then(function (data) {
      if (!data){
        data = [];
      }
      return data;
    });
  };

  var _getReviews = function _getReviews(userId, limit) {
    return db.queryJson('user_reviews', [ userId, limit]).then(function(data) {
      if (!data) {
        return [];
      }
      return data;
    });
  };

  var _getReview = function _getReview(userId, reviewId) {
    return db.queryJson('user_review', [ userId, reviewId ]).then(function(data) {
      if (!data) {
        return {};
      }
      return data;
    });
  };

  var _submitReview = function _submitReview(userId, reviewerUserId, title, body, rating) {
    return db.queryJson('user_review_submit', [ userId, reviewerUserId, title, body, rating ]).then(function(data) {
      if (!data){
        return {};
      }
      return data;
    });
  };

  var _deleteReview = function _deleteReview(userId, reviewId) {
    return db.queryJson('user_review_delete', [ userId, reviewId]).then(function(data) {
      return data;
    });
  };

  return {
    setDistrict: _setDistrict,
    setWeight: _setWeight,
    setHeight: _setHeight,
    setIsMale: _setIsMale,
    setIsGym: _setIsGym,
    setIsPersonalTrainer: _setIsPersonalTrainer,
    setMobileNumber: _setMobileNumber,
    setGym: _setGym,
    setRoutineType: _setRoutineType,
    setPrivacyPreferences: _setPrivacyPreferences,
    getActivities: _getActivities,
    getNotifications: _getNotifications,
    acknowledgeNotification: _acknowledgeNotification,
    getUserProfile: _getUserProfile,
    search: _search,
    follow: _follow,
    unfollow: _unfollow,
    setClient: _setClient,
    unsetClient: _unsetClient,
    setFirstName: _setFirstName,
    setLastName: _setLastName,
    setBio: _setBio,
    getProgressChartData: _getProgressChartData,
    getFeatured: _getFeatured,
    registerDeviceForPushNotification: _registerDeviceForPushNotification,
    getReviews: _getReviews,
    getReview: _getReview,
    submitReview: _submitReview,
    deleteReview: _deleteReview
  };
};
