module.exports = function BlogRepository(db) {
  db.registerParameters('blog_get_posts', [ 'int', 'int', 'int' ]);
  db.registerParameters('blog_get_post', [ 'int' ]);
  
  var _getPosts = function _getPosts(countryId, postsPerPage, pageNumber) {
    return db.queryJson('blog_get_posts', [ countryId, postsPerPage, pageNumber ]);
  };
  
  var _getPost = function _getPost(postId) {
    return db.queryJson('blog_get_post', [ postId ]);
  };

  return {
    getPosts: _getPosts,
    getPost: _getPost
  };
};