module.exports = function SocialRepository(db) {
  'use strict';
  
  db.registerParameters('social_get_notifications', [ 'int', 'timestamp', 'int' ]);
  db.registerParameters('social_update_new', [ 'int', 'text', 'text' ]);
  db.registerParameters('social_get_global_updates', [ 'int', 'timestamp', 'int' , 'text']);
  db.registerParameters('social_get_user_updates', [ 'int', 'timestamp', 'int', 'boolean', 'boolean' ]);
  db.registerParameters('social_update_like', [ 'int', 'uuid' ]);
  db.registerParameters('social_update_unlike', [ 'int', 'uuid' ]);
  db.registerParameters('social_update_flag', [ 'int', 'uuid' ]);
  db.registerParameters('social_update_unflag', [ 'int', 'uuid' ]);
  db.registerParameters('social_update_attach_image', [ 'int', 'uuid' ]);
  db.registerParameters('social_update_attach_video', [ 'int', 'uuid' ]);
  db.registerParameters('social_update_search', [ 'int', 'text' ]);
  
  var _getNotifications = function _getNotifications(userId, fromTimestamp, numberToReturn) {
    return db.queryJson('social_get_notifications', [ userId, fromTimestamp, numberToReturn ]);
  };
  
  var _postNewUpdate = function _postNewUpdate(userId, body, tags) {
    return db.queryJson('social_update_new', [ userId, body, tags ]);
  };
  
  var _getGlobalUpdates = function _getGlobalUpdates(userId, fromTimestamp, numberToReturn, tag) {
    return db.queryJson('social_get_global_updates', [ userId, fromTimestamp, numberToReturn, tag]);
  };
  
  var _getUsersUpdates = function _getUsersUpdates(userId, fromTimestamp, numberToReturn, showOnlyMine, ignoreMine) {
    return db.queryJson('social_get_user_updates', [ userId, fromTimestamp, numberToReturn, showOnlyMine, ignoreMine ]);
  };
  
  var _like = function _like(userId, updateId) {
    return db.queryJson('social_update_like', [ userId, updateId ], _handleLikeError);
  };
  
  var _handleLikeError = function _handleLikeError(err) {
    if (err.constraint === 'social_update_like_update_id_fkey') {
      return [['updateId' , 'Not_Found']];
    }
  };
  
  var _unlike = function _unlike(userId, updateId) {
    return db.queryJson('social_update_unlike', [ userId, updateId ]);
  };
  
  var _flag = function _flag(userId, updateId) {
    return db.queryJson('social_update_flag', [ userId, updateId ], _handleFlagError);
  };
  
  var _handleFlagError = function _handleFlagError(err) {
    if (err.constraint === 'social_update_flag_update_id_fkey') {
      return [['updateId' , 'Not_Found']];
    }
  };
  
  var _unflag = function _unflag(userId, updateId) {
    return db.queryJson('social_update_unflag', [ userId, updateId ]);
  };
  
  var _attachImage = function _attachImage(userId, updateId) {
    return db.queryJson('social_update_attach_image', [ userId, updateId ]);
  };
  
  var _attachVideo = function _attachVideo(userId, updateId) {
    return db.queryJson('social_update_attach_video', [ userId, updateId ]);
  };
  
  var _search = function _search(userId, query) {
    return db.queryJson('social_update_search', [ userId, query ]);
  };
  
  return {
    getNotifications: _getNotifications,
    postNewUpdate: _postNewUpdate,
    getGlobalUpdates: _getGlobalUpdates,
    getUsersUpdates: _getUsersUpdates,
    like: _like,
    unlike: _unlike,
    flag: _flag,
    unflag: _unflag,
    attachImage: _attachImage,
    attachVideo: _attachVideo,
    search: _search
  };
};