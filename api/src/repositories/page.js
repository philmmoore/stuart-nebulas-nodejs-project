module.exports = function PageRepository(db) {
  db.registerParameters('page_get', [ 'int', 'text' ]);
  
  var _getPage = function _getPage(countryId, name) {
    return db.queryJson('page_get', [ countryId, name ]);
  };

  return {
    getPage: _getPage
  };
};