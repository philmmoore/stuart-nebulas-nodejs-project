module.exports = function ChallengeRepository(db) {
  db.registerParameters('challenge_get_groups', [ 'int', 'int' ]);
  db.registerParameters('challenge_get_for_category', [ 'int' ]);
  
  var _getGroups = function _getGroups(challengeId, userId) {
    return db.queryJson('challenge_get_groups', [ challengeId, userId ]);
  };

  var _getChallenges = function _getChallenges() {
    return db.queryJson('challenge_get');
  };

  var _getCategories = function _getCategories() {
    return db.queryJson('challenge_get_categories');
  };

  var _getChallengesForCategory = function _getChallengesForCategory(categoryId) {
    return db.queryJson('challenge_get_for_category', [ categoryId ]);
  };

  return {
    getGroups: _getGroups,
    getChallenges: _getChallenges,
    getCategories: _getCategories,
    getChallengesForCategory: _getChallengesForCategory
  };
};