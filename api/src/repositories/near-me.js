var Promise = require('bluebird');

module.exports = function UserRepository(db) {

  db.registerParameters('users_near_me', [ 'numeric', 'numeric', 'numeric', 'bool', 'bool', 'numeric', 'bool']);

  var _getNearMe = function _getNearMe(lat, lon, radius, is_pt, is_gym, user_id, show_following) {

    var params;

    if (!radius){
      radius = 10; // Default radius
    }

    if (!user_id){
      user_id = 0; // No users exists with the id of 0 therefore should not exclude any results
    }

    if (!show_following){
      show_following = true;
    }

    params = [
      lat,
      lon,
      radius, 
      (!is_pt ? false : is_pt),
      (!is_gym ? false : is_gym),
      user_id,
      show_following
    ];

    return db.queryJson('users_near_me', params);

  };

  return {
    getNearMe: _getNearMe
  };
};
