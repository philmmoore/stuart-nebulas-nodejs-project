var requestPromise = require('request-promise');
var Promise = require('bluebird');

module.exports = function GoogleMapsService(googleMapsAPIKey) {
  var _lookup = function _lookup(latlon) {
    var options = {
      uri: 'https://maps.googleapis.com/maps/api/geocode/json',
      qs: {
        latlng: latlon,
        key: googleMapsAPIKey
      },
      json: true
    };

    return requestPromise(options).then(function(response) {
      if (!response.results || response.results.length < 1) {
        return Promise.reject('could not lookup latlon');
      }

      return response.results;
    });
  };

  return {
    lookup: _lookup
  };
};
