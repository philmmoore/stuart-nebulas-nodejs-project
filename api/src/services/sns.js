var Promise = require('bluebird');

module.exports = function SNS(config) {

  var AWS = require('aws-sdk');
  AWS.config.update(config.snsConfig);
  var sns = new AWS.SNS();

  var _registerDeviceToken = function registerDeviceToken(deviceToken) {
    return new Promise(function(resolve, reject) {

      var params = {
        "PlatformApplicationArn" : "arn:aws:sns:eu-west-1:164535666447:app/" + (config.isProduction ? "APNS" : "APNS_SANDBOX") + "/GymFit",
        "Token": deviceToken
      };

      sns.createPlatformEndpoint(params,function(err,EndPointResult){
        if (err) {
          return reject(err);
        }
        return resolve(EndPointResult.EndpointArn);
      });

    });
  };

  return {
    registerDeviceToken: _registerDeviceToken
  };
};
