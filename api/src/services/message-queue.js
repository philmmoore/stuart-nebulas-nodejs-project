var Promise = require('bluebird');

module.exports = function MessageQueueService(config) {
  var AWS = require('aws-sdk');
  AWS.config.update(config.queueConfig);
  var sqs = new AWS.SQS();

  var _sendMessage = function sendMessage(queueName, message) {
    var params = {
      MessageBody: message,
      QueueUrl: config.queueUrlPrefix + queueName
    };

    return new Promise(function(resolve, reject) {
      sqs.sendMessage(params, function(err, result) {
        if (err) {
          return reject(err);
        }

        return resolve();
      });
    });
  };

  return {
    sendMessage: _sendMessage
  };
};
