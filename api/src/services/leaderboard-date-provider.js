var moment = require('moment');

module.exports = function LeaderboardDateProviderService(isLeaderboardRealtime) {
  var SEASON_START_DATE = '01-OCT-2015';

  var _getDates = function _getDates(frequency, now) {
    if (!now) {
      now = moment();
    }

    var current = moment(now).hours(0).minutes(0).seconds(0).milliseconds(0);

    while (frequency.indexOf(current.isoWeekday()) === -1) {
      current.subtract(1, 'day');
    }

    var previous = moment(current).subtract(1, 'day');

    while (frequency.indexOf(previous.isoWeekday()) === -1) {
      previous.subtract(1, 'day');
    }

    var next = moment(current).add(1, 'day');

    while (frequency.indexOf(next.isoWeekday()) === -1) {
      next.add(1, 'day');
    }

    if (isLeaderboardRealtime) {
      current = now;
      next = moment(now).add(1, 'second');
    }

    return {
      current: current,
      previous: previous,
      next: next
    };
  };

  return {
    getDates: _getDates,
    SEASON_START_DATE: SEASON_START_DATE
  };
};
