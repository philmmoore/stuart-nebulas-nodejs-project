var Promise = require('bluebird');
var pbkdf2 = Promise.promisify(require('crypto').pbkdf2);

module.exports = function PasswordHashService(salt) {
  var _getHash = function _getHash(password) {
    return pbkdf2(password, salt, 4096, 256, 'sha256');
  };

  return {
    getHash: _getHash
  };
};
