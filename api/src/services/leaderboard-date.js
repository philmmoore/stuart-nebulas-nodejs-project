module.exports = function LeaderboardDateService(repository, leaderboardDateProviderService) {
  var COUNTRY_ID = 1;

  var _getDates = function _getDates() {
    return repository.getConfiguration(COUNTRY_ID).then(function(config) {
      var data = leaderboardDateProviderService.getDates(config.leaderboard.updateFrequency);
      data.seasonStartDate = leaderboardDateProviderService.SEASON_START_DATE;
      return data;
    });
  };

  return {
    getDates: _getDates
  };
};
