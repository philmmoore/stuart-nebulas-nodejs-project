module.exports = function LocationLookup() {
  var _formatName = function _formatName(name) {
    return name.toLowerCase().trim();
  };

  var _formatLocations = function _formatLocations(locations) {
    var districts = [];
    var counties = [];
    var regions = [];
    var countries = [];

    for (var locationId = 0; locationId < locations.length; locationId++) {
      var location = locations[locationId];

      if (location.districtId) {
        districts.push({
          name: location.name,
          id: location.districtId
        });
      }

      if (location.countyId) {
        counties.push({
          name: location.name,
          id: location.countyId
        });
      }

      if (location.regionId) {
        regions.push({
          name: location.name,
          id: location.regionId
        });
      }

      if (location.countryId) {
        countries.push({
          name: location.name,
          countryCode: location.countryCode,
          id: location.countryId
        });
      }
    }

    return {
      districts: districts,
      counties: counties,
      regions: regions,
      countries: countries
    };
  };

  var _formatMapData = function _formatMapData(mapData) {
    var data = [];

    for (var resultIndex = 0; resultIndex < mapData.length; resultIndex++) {
      var result = mapData[resultIndex];

      for (var addressComponentIndex = 0; addressComponentIndex < result.address_components.length; addressComponentIndex++) {
        var addressComponent = result.address_components[addressComponentIndex];

        _addMapDataToArrayIfNotExists(data, addressComponent.long_name);
        _addMapDataToArrayIfNotExists(data, addressComponent.short_name);
      }
    }

    return data;
  };

  var _addMapDataToArrayIfNotExists = function _addMapDataToArrayIfNotExists(array, value) {
    if (!value) {
      return;
    }

    value = _formatName(value);

    if (array.indexOf(value) === -1) {
      array.push(value);
    }
  };

  var _lookupLocation = function _lookupLocation(locations, mapData) {
    for (var locationIndex = 0; locationIndex < locations.length; locationIndex++) {
      var location = locations[locationIndex];

      for (var mapDataIndex = 0; mapDataIndex < mapData.length; mapDataIndex++) {
        var mapDataRow = mapData[mapDataIndex];

        if (_formatName(location.name) === mapDataRow || location.countryCode === mapDataRow) {
          return location;
        }
      }
    }

    return null;
  };

  var _lookup = function _lookup(locations, mapData) {
    locations = _formatLocations(locations);
    mapData = _formatMapData(mapData);

    var data = {
      district: null,
      county: null,
      region: null,
      country: null
    };

    var district = _lookupLocation(locations.districts, mapData);

    if (district) {
      data.district = district;
      return data;
    }

    var county = _lookupLocation(locations.counties, mapData);

    if (county) {
      data.county = county;
      return data;
    }

    var region = _lookupLocation(locations.regions, mapData);

    if (region) {
      data.region = region;
      return data;
    }

    var country = _lookupLocation(locations.countries, mapData);

    if (country) {
      data.country = country;
      return data;
    }

    return data;
  };

  return {
    lookup: _lookup
  };
};
