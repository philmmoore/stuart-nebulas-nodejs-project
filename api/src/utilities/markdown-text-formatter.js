var markdown = require('markdown').markdown;

var videoIFrame = '<div class="embed-responsive embed-responsive-16by9"><iframe src="https://www.youtube.com/embed/{video}?rel=0" allowfullscreen></iframe></div>';

var _processVideos = function _processVideos(text) {
  var regex = /((http(s)?:\/\/)?(www.)?youtu(?:\.be|be\.com)\/(?:.*v(?:\/|=)|(?:.*\/)?)([\w'-]+))/ig;
  var matches = text.match(regex);

  if (matches === null) {
    return text;
  }

  for(var index = 0; index < matches.length; index++) {
    var match = matches[index];

    var videoUrl = text.substr(text.indexOf(match));
    videoUrl = videoUrl.split(' ')[0];
    videoUrl = videoUrl.split('<')[0];

    var videoId = videoUrl.split('&')[0];
    videoId = videoId.replace('watch?v=', '');
    videoId = videoId.split('/')[videoId.split('/').length - 1];

    text = text.replace(videoUrl, videoIFrame.replace('{video}', videoId));
  }

  return text;
};

var _processFeatures = function _processFeatures(text) {
  var regex = /<a href="gymfit-feature:.*<\/a>/ig;
  var matches = text.match(regex);

  if (matches === null) {
    return text;
  }

  for(var index = 0; index < matches.length; index++) {
    var match = matches[index];
    text = text.replace(match, '');
  }

  return text;
};

var _processImages = function _processImages(text) {
  var regex = /<img /ig;
  var matches = text.match(regex);

  if (matches === null) {
    return text;
  }

  for(var index = 0; index < matches.length; index++) {
    var match = matches[index];
    text = text.replace(match, '<img class="img-responsive" ');
  }

  return text;
};

var _format = function _format(text) {
  var markdownFormattedText = markdown.toHTML(text);

  markdownFormattedText = _processVideos(markdownFormattedText);
  markdownFormattedText = _processFeatures(markdownFormattedText);
  markdownFormattedText = _processImages(markdownFormattedText);
  
  return markdownFormattedText;
};

module.exports.format = _format;
