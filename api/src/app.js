if (process.env.NEW_RELIC_LICENSE_KEY) {
  require('newrelic');
}

var PostgresDB = require('./db/postgres-db');
var Server = require('./server');
var Config = require('./config');
var IoC = require('./ioc');
var throng = require('throng');

var logger = console;
var config = new Config();
logger.log('Environment: %s.', config.isProduction ? 'production' : 'development');

function start() {
  var postgresDB = new PostgresDB(config, logger);

  postgresDB.connect().then(function(db) {
    var ioc = new IoC(config, db, logger);
    var server = new Server(ioc);
    server.listen(config.port);
  });
}

throng(start, {
  workers: config.workers,
  lifetime: Infinity
});