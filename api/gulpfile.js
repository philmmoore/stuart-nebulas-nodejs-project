var gulp = require('gulp');
var gulpif = require('gulp-if');
var path = require('path');
var mocha = require('gulp-mocha');
var jshint = require('gulp-jshint');
var istanbul = require('gulp-istanbul');

var production = process.env.NODE_ENV === 'production';
var allJavaScriptFiles = ['src/**/*.js', 'test/**/*.js', '*.js'];
var istanbulReporters = production ? ['text'] : ['text-summary', 'lcov'];
var mochaReporter = production ? 'spec' : 'nyan';
var watchMode = false;

gulp.task('test', function (callback) {
  gulp.src('src/**/*.js')
    .pipe(istanbul({ includeUntested: true }))
    .pipe(istanbul.hookRequire())
    .on('finish', function () {
      gulp.src('test/**/*.js')
        .pipe(mocha({ reporter: mochaReporter }))
        .on('error', handleTestError)
        .pipe(istanbul.writeReports({ reporters: istanbulReporters }))
        .pipe(istanbul.enforceThresholds({
          thresholds: {
            global: watchMode ? 0 : 0
          }
        }))
        .on('end', callback);
    });
});

function handleTestError(err) {
  if (watchMode) {
    this.emit('end');
    return;
  }

  throw err;
}

gulp.task('lint', function() {
  return gulp.src(allJavaScriptFiles)
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(jshint.reporter('fail'));
});

gulp.task('watch', function() {
  watchMode = true;

  gulp.watch(allJavaScriptFiles, ['test', 'lint']);
});

gulp.task('default', ['test', 'lint']);
