var LeaderboardResultDialog = function(jquery, videojs, resultData) {
  'use strict';

  var _videoPlayer = null;
  var _selectedResult = null;
  var _dialogBoxElement = null;

  var _resultClicked = function _resultClicked(data) {
    _dialogBoxElement = jquery(data.type === 'image' ? '#evidenceImagePopup' : '#evidenceVideoPopup');
    
    _dialogBoxElement.find('[data-update-id]').attr('data-update-id', data.updateId);
    
    var lastLikeElement = jquery('.btn-like[data-update-id=' + data.updateId + ']:last');
    
    if (lastLikeElement.hasClass('btn-liked')) {
      _dialogBoxElement.find('.btn-like[data-update-id]').addClass('btn-liked');
    } else {
      _dialogBoxElement.find('.btn-like[data-update-id]').removeClass('btn-liked');
    }
    
    _dialogBoxElement.find('.likes[data-update-id]').text(lastLikeElement.text());

    _dialogBoxElement.find('.evidence-src').attr('src', data.src);
    _dialogBoxElement.find('.share-evidence-button').attr('href', '/result/' + data.id + '/');
    _dialogBoxElement.modal('toggle');

    if (data.type === 'video') {
      if (!_videoPlayer) {
        var videoPlayerOptions = {
          autoplay: true,
          controls: false
        };

        _videoPlayer = videojs("evidenceVideo", videoPlayerOptions);
      }

      _videoPlayer.src(data.src);
    }
  };
  
  var _init = function _init() {
    var leaderboardTableElement = jquery('#leaderboard-table, #leaderboard-cards');

    jquery('#evidenceVideoPopup').on('hidden.bs.modal', function(e) {
      if (_videoPlayer) {
        _videoPlayer.pause();
      }
    });

    leaderboardTableElement.find('.result-link').click(function(e) {
      _selectedResult = resultData[parseInt($(this).attr('data-id'))];
      _resultClicked(_selectedResult);
    });
  };

  _init();
};