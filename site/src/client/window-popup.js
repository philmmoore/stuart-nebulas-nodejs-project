var WindowPopup = function(jquery) {
  'use strict';
  
  var button = null;
  
  var _init = function _init() {
    button = jquery('.window-popup');
    button.click(_handleShare);
  };
  
  var _handleShare = function _handleShare(e) {
    e.preventDefault();
    var url = jquery(this).attr('href');
    
    var width = 450;
    var height = 320;
    var leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
    var topPosition = (window.screen.height / 2) - ((height / 2) + 50);
    var windowFeatures = 'status=no,height=' + height + ',width=' + width + ',resizable=yes,left=' + leftPosition + ',top=' + topPosition + ',screenX=' + leftPosition + ',screenY=' + topPosition + ',toolbar=no,menubar=no,scrollbars=no,location=no,directories=no';
    
    window.open(url, '_blank', windowFeatures);
    
    return false;
  };
  
  _init();
};