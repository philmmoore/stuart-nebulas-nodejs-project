var AvatarUploader = function(jquery) {
  'use strict';

  var uploaderElement = jquery('#avatar-uploader');
  var errorAlertElement = jquery('#validationErrorAlert');
  var errorListElement = errorAlertElement.find('ul');
  var complete = false;
  var processingVisible = false;

  var _clearErrors = function _clearErros() {
    errorAlertElement.hide();
    errorListElement.empty();
    
    jquery('.form-group').removeClass('has-error');
  };

  var _showError = function _showError(message) {
    errorAlertElement.show();
    
    errorListElement.append('<li><strong>' + 'Profile Picture' + '</strong> - ' + message + '</li>');
    jquery('#Avatar').parent().addClass('has-error');
    jquery('#Avatar').focus();
  };

  var _renderUploading = function _renderUploading() {
    var evidenceElement = uploaderElement.find('#Avatar');
    var progressContainerElement = uploaderElement.find('#uploadProgressContainer');
    var progressElement = progressContainerElement.find('.progress-bar');

    uploaderElement.find('input').prop('disabled', 'disabled');
    jquery('.btn').prop('disabled', 'disabled');
    evidenceElement.hide();
    progressElement.css('width', '0%');
    progressElement.find('span').first().show();
    progressElement.find('span').last().hide();
    progressContainerElement.show();
    processingVisible = false;
  };

  var _getUploadModel = function _getUploadModel(file) {
    return {
      name: file.name,
      size: file.size,
      type: file.type
    };
  };

  var _uploadProgress = function _uploadProgress(progressEvent) {
    if (!progressEvent.lengthComputable) {
      return;
    }

    var percentComplete = Math.round((progressEvent.loaded / progressEvent.total) * 100);

    var progressContainerElement = uploaderElement.find('#uploadProgressContainer');
    var progressElement = progressContainerElement.find('.progress-bar');

    progressElement.css('width', percentComplete + '%');
    progressElement.find('span').first().text(percentComplete + '%');

    if (percentComplete === 100) {
      if (processingVisible) {
        return;
      }

      processingVisible = true;

      setTimeout(function()
      {
        progressElement.find('span').first().hide();
        progressElement.find('span').last().show();
      }, 500);
    }
  };

  var _uploadSuccessful = function _uploadSuccessful() {
    jquery('#avatar-uploader').hide();
    jquery('#successfulAvatarUploadAlert').show();
  };

  var _uploadFailed = function _uploadFailed(err) {
    if (err.status === 400 && err.responseJSON === 'unsupported file') {
      _showError('File is not supported. Please upload a supported image e.g. jpg, png.');
    }
    
    var progressContainerElement = uploaderElement.find('#uploadProgressContainer');
    var progressElement = progressContainerElement.find('.progress-bar');
    var evidenceElement = uploaderElement.find('#Avatar');

    uploaderElement.find('input').prop('disabled', '');
    uploaderElement.find('.btn').prop('disabled', '');
    progressContainerElement.hide();
    evidenceElement.show();
  };

  var _uploadFile = function _uploadFile(evidenceElement) {
    var fileData = evidenceElement[0].files[0];
    var model = _getUploadModel(fileData);

    jquery.post('sign-upload/', model).done(function(url) {
      jquery.ajax({
        url: url,
        type: 'put',
        crossDomain: true,
        data: fileData,
        contentType: false,
        processData: false,
        xhr: function () {
          var xhr = new window.XMLHttpRequest();
          xhr.upload.addEventListener('progress', _uploadProgress, false);
          return xhr;
        },
        success: _uploadSuccessful
      }).error(function(err) {
        _uploadFailed(err);
      });
    }).error(function(err) {
      _uploadFailed(err);
    });
  };

  var _init = function _init() {
    jquery(uploaderElement).submit(function(submitEvent) {
      submitEvent.preventDefault();
      var evidenceElement = uploaderElement.find('#Avatar');
      
      _clearErrors();

      if (!evidenceElement.val()) {
        return _showError('Please select an image to upload.');
      }
      
      _renderUploading();
      _uploadFile(evidenceElement);
    });
  };

  _init();
};
