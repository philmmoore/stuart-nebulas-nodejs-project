var RegistrationDistrictSearch = function(jquery, Bloodhound) {
  'use strict';

  var _init = function _init() {
    var locationTypeahead = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.whitespace,
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      prefetch: {
        url: 'get-all-districts/',
        cache: false
      }
    });

    jquery('#prefetch-registration-district .typeahead').typeahead(null, {
      source: locationTypeahead
    });
  };

  _init();
};
