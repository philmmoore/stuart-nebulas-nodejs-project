// function onYouTubeIframeAPIReady() {

//     player = new YT.Player('player', {
//         height: '484',
//         width: '100%',
//         videoId: 'B7hYunYw_Qw',
//         events: {
//             // 'onReady': onPlayerReady,
//                 // 'onStateChange': onPlayerStateChange
//         },
//         playerVars: {
//           autoplay: 0,
//           controls: 0,
//           modestbranding: 1,
//           rel: 0,
//           showInfo: 0
//         }
//     });

// }

var Home = function(jquery) {
  'use strict';
  
  var button = null;

  var player = null;
  
  var _scrollToAnchor = function _scrollToAnchor(anchor, speed){
    $('html,body').animate({
      scrollTop: jquery(anchor).offset().top
    }, speed);
  };

  var _formTypes = function _formTypes(){
    jquery('.tile.register .types li').on('click', function(){
      var $this = jquery(this);
      $this.parent().find('li').removeClass('selected');
      $this.addClass('selected');
    });
  };

  var _hideModal = function hideModal(){
      jquery('body').removeClass('modal-active');
      jquery('.tile.modal.video .video').removeClass('finished');
      _youtube.player.stopVideo();
  };

  var _heroVideo = function _heroVideo() {
    
    var $modal = jquery('.tile.modal.video');
    var $hero = jquery('#start');
    _youtube.playVideo('player', 'LN6TrBn3x0M');
    
    $hero.find('.watch-video a').on('click', function(e){
      e.preventDefault();
      jquery('body').addClass('modal-active');
      _youtube.player.playVideo();
    });
    
    $modal.find('.watch-again a').on('click', function(e){
      e.preventDefault();
      _youtube.player.playVideo();
      $modal.find('.video').removeClass('finished');
    });

    $modal.find('.close').on('click', function(e){
      e.preventDefault();
      _hideModal();
    });

    $modal.find('.object.btn-style-two').on('click', function(e){
      e.preventDefault();
      _hideModal();
      _scrollToAnchor('#register');
    });
    
  };

  var _youtube = {
    player: null,

    playVideo: function(container, videoId) {
      if (typeof(YT) == 'undefined' || typeof(YT.Player) == 'undefined') {
        window.onYouTubeIframeAPIReady = function() {
          _youtube.loadPlayer(container, videoId);
        };
        $.getScript('//www.youtube.com/iframe_api');
      } else {
        _youtube.loadPlayer(container, videoId);
      }
    },

    loadPlayer: function(container, videoId) {
      _youtube.player = new YT.Player(container, {
        videoId: videoId,
        width: '100%',
        height: 484,
        playerVars: {
          autoplay: 0,
          controls: 0,
          modestbranding: 0,
          rel: 0,
          showinfo: 0
        },
        events: {
            'onStateChange': _youtube.events.onPlayerStateChange
        }
      });
    }, 
    events: {
      onPlayerStateChange: function(state){
        if (state.data === 0){
          jquery('.tile.modal.video .video').addClass('finished');
        }
      }
    }
  };

  var _jumpToForm = function _formErrors(){

    var message = jquery('.object.form-msg');
    if (jquery('.object.form-msg').length > 0 || jquery('.tile.register .form.success').length > 0){
      _scrollToAnchor('#register', 0);
    }

  };

  var _init = function _init() {
    var $this = this;
    // Bind anchor class clicks to scroll to anchor method
    jquery('.anchor').on('click', function(e){
      e.preventDefault();
      _scrollToAnchor(jquery(this).attr('scroll-to'), 1000);
    });
    // Bind form controls to set active state
    _formTypes();
    // Bind hero video functions
    _heroVideo();
    // Watch for form msg so we can jump to the form when the page reloads if necessary
    _jumpToForm();

  };

  _init();
};