$(function() {
  var likeFlagButtons = null;
  
  if ($('.registerButton').length > 0) {
    $('.registerButton').addClass('animated tada');
  }

  if ($('#result-evidence-uploader').length > 0) {
    new ResultEvidenceUploader($, moment);
  }

  if ($('#leaderboard-table').length > 0) {
    new LeaderboardResultDialog($, videojs, resultData);
  }

  if ($('#avatar-uploader').length > 0) {
    new AvatarUploader($);
  }

  if ($('#search-all-locations').length > 0) {
    new SearchAllLocationsControl($, Bloodhound);
  }

  if ($('#prefetch-registration-district').length > 0) {
    new RegistrationDistrictSearch($, Bloodhound);
  }
  
  if ($('#prefetch-edit-district').length > 0) {
    new UserUpdateDistrictSearch($, Bloodhound);
  }
  
  if ($('.btn-like[data-update-id]').length > 0) {
    likeFlagButtons = new LikeFlagButtons($);
  }
  
  if ($('.social-feed').length > 0) {
    new SocialFeed($, likeFlagButtons);
  }
  
  if ($('.window-popup').length > 0) {
    new WindowPopup($);
  }
  if ($('.tile.hero-search').length > 0){
    new Home($);
  }

  new Global($);
  
});
