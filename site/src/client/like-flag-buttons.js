var LikeFlagButtons = function(jquery) {
  'use strict';

  var _likeButtonClicked = function _likeButtonClicked(e) {
    e.preventDefault();
    
    var buttonElement = jquery(this);
    buttonElement.blur();
    
    var updateId = buttonElement.data('updateId');
    var action = buttonElement.hasClass('btn-liked') ? 'unlike' : 'like';
    
    jquery.get('/api/social/update/' + updateId + '/' + action + '/').success(function(data) {
      if (action === 'like') {
        jquery('.btn-like[data-update-id=' + updateId + ']').addClass('btn-liked');
      } else {
        jquery('.btn-like[data-update-id=' + updateId + ']').removeClass('btn-liked');
      }
      
      _updateLikesLabel(updateId, data);
    });
    
    return false;
  };
  
  var _flagButtonClicked = function _flagButtonClicked(e) {
    e.preventDefault();
    
    var updateId = jquery(this).data('updateId');
    
    jquery.get('/api/social/update/' + updateId + '/flag/').success(function(data) {
      jquery('.btn-like[data-update-id=' + updateId + ']').removeClass('btn-liked');
      _updateLikesLabel(updateId, data);
    });
  };
  
  var _updateLikesLabel = function _updateLikesLabel(updateId, data) {
    var likesLabel = jquery('.likes[data-update-id=' + updateId + ']');
    
    if (likesLabel) {
      likesLabel.text(data.likes);
    }
  };
  
  var _deleteButtonClicked = function _deleteButtonClicked(e) {
    e.preventDefault();

    if (confirm('Are you sure you want to delete this post?')){
    
      var updateId = jquery(this).data('updateId');
      
      jquery.get('/api/social/update/' + updateId + '/delete/').success(function(data) {
        var elementToRemove = jquery('[data-update-id=' + updateId + ']');
        
        if (elementToRemove) {
          elementToRemove.remove();
        }
      });

    }
  };
  
  var _init = function _init() {
    jquery('.btn-like[data-update-id]').click(_likeButtonClicked);
    jquery('.btn-flag[data-update-id]').click(_flagButtonClicked);
    jquery('.btn-delete[data-update-id]').click(_deleteButtonClicked);
  };
  
  var _attachHandlers = function _attachHandlers(updateId) {
    jquery('.btn-like[data-update-id=' + updateId + ']').click(_likeButtonClicked);
    jquery('.btn-flag[data-update-id=' + updateId + ']').click(_flagButtonClicked);
    jquery('.btn-delete[data-update-id=' + updateId + ']').click(_deleteButtonClicked);
  };

  _init();
  
  return {
    attachHandlers: _attachHandlers
  };
};