var Global = function(jquery) {

  'use strict';
  
  var $jq = jquery;

  var _scrollToAnchor = function _scrollToAnchor(anchor, speed, offset){
    $('html,body').animate({
      scrollTop: jquery(anchor).offset().top - offset
    }, speed);
  };

  var _bindEvents = function(){


    jquery('.anchor').bind('click', function(e){
      e.preventDefault();
      var $this = jquery(this);
      jquery('.anchor').removeClass('selected');
      var offset = 0;
      if ($this.attr('anchor-offset')){
        offset = $this.attr('anchor-offset');
      }
      _scrollToAnchor($this.attr('href'), 'normal', offset);
      $this.addClass('selected');
    });

    if (jquery('.object.write-review').size() > 0 || jquery('.object.choose-rating').size() > 0){
      jquery('.object.write-review .rating label, .object.choose-rating .rating label').bind('click', function(){
        var $label = jquery(this);
        var index = $label.closest('li').index();
        var $ul = $label.closest('ul');
        $ul.find('li .star').removeClass('active');
        // Loop through each star and mark it as active when the user clicks      
        $ul.find('li').each(function(){
          var $this = jquery(this);
          if ($this.index() < index){
            $this.find('.star').addClass('active');
          }
        });
      });
    }

  };

  // Toggle the responsive menu
  var _toggleMenu = function(){
    
    // Bind the event
    $jq('.tile.header .responsive-nav .menu a').bind('click', function(e){
      e.preventDefault();
      var $site = $jq('.site');

      // Toggle the nav-open class
      if ($site.hasClass('nav-open')){
        $site.removeClass('nav-open');
      } else {
        $site.addClass('nav-open');
      }

    });

  };

  // Decide whether header should be shown as the user scrolls up the page
  var _showHeader = function(){

    var onThreshold = 600; // Only show header on scroll up if the user is more than value (pixels)
    var showThreshold = 50; // Only show the header if the onThreshold is met and the user scrolls up by value (pixels)
    var currentScrollPosition = 0;
    var scrollUpOffset = 0;
    var scrollDownOffset = 0;
    var $header = $jq('.tile.header');

    $jq(window).scroll(function(){
      // The new scroll position
      var scrollPosition = $jq(this).scrollTop();
      // Check if the current scroll position is positive or negative i.e. 
      // scrolling up or scrolling down
      if (scrollPosition < currentScrollPosition){
        scrollUpOffset--;

        // Hide the header if the user has scrolled above the show threshold
        if (scrollPosition <= onThreshold && $header.hasClass('scrolled') && !$header.hasClass('hiding')){
          hideHeader();
        }

        // Show the header if the user has begun scrolling up
        if (scrollUpOffset < 0 && scrollPosition >= onThreshold && !$header.hasClass('scrolled')){
          // Show the header if the scroll up offset and on threshold are met
          showHeader();
        }

      } else {
        // User scrolling down, reset the scroll up offset
        scrollUpOffset = showThreshold;
        scrollDownOffset++;
        // If user begins scrolling again, gracefull hide the header if it's visible
        if (scrollDownOffset > showThreshold && $header.hasClass('scrolled')){
          hideHeader();
        }
      }

      // Sets the current scroll position once scrolling has stopped
      currentScrollPosition = scrollPosition;

    });

    var showHeader = function(){
        $jq('.site').addClass('scrolled');
        $header.addClass('scrolled');
        $header.animate({
          top: 0
        }, 300);
        scrollUpOffset = showThreshold;
        scrollDownOffset = 0;
    };

    var hideHeader = function(){
        $header.addClass('hiding');
        $header.animate({
          top: '-70px'
        }, 300, function(){
          $jq('.site').removeClass('nav-open').removeClass('scrolled');

          $header.removeClass('scrolled');
          $header.removeClass('hiding');
          $header.removeAttr('style');
        });
        scrollDownOffset = 0;
    };

  };

  var _inputPlaceholderSupport = function(){
      $jq("input[placeholder]").placeholder();
  };

  var _registerWidgetClose = function(){
    $jq('.object.sign-up-widget .close').bind('click', function(e){
      e.preventDefault();
      $jq(this).closest('.object').fadeOut('fast');
    });
  };

  // Hide the responsive menu of the browser window is resized above the threshold
  var _hideResponsiveMenu = function(){
    $jq(window).resize(function(){
      var width = $jq(this).outerWidth();
      if (width > 920 && $jq('.site').hasClass('nav-open')){
        $jq('.site').removeClass('nav-open');
      }
    });
  };

  var _init = function _init() {
    var $this = this;
    _toggleMenu();
    _hideResponsiveMenu();
    // Watch for header position to decide whether to show it or not.
    _showHeader();
    // Widget close button
    _registerWidgetClose();
    // Placholder support on input
    _inputPlaceholderSupport();
    // Bind events
    _bindEvents();
  };

  _init();

};