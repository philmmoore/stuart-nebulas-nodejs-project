var ResultEvidenceUploader = function(jquery, moment) {
  'use strict';

  var uploaderElement = jquery('#result-evidence-uploader');
  var errorAlertElement = jquery('#validationErrorAlert');
  var errorListElement = errorAlertElement.find('ul');
  var complete = false;
  var processingVisible = false;
  var measurement = null;

  var _clearErrors = function _clearErros() {
    errorAlertElement.hide();
    errorListElement.empty();

    jquery('.form-group').removeClass('has-error');
  };

  var _showErrors = function _showErrors(errors) {
    errorAlertElement.show();

    for (var index = 0; index < errors.length; index++) {
      errorListElement.append('<li><strong>' + errors[index].label + '</strong> - ' + errors[index].message + '</li>');
      jquery('#' + errors[index].id).parent().addClass('has-error');
      jquery('#' + errors[index].id).focus();
    }
  };

  var _renderUploading = function _renderUploading() {
    var evidenceElement = uploaderElement.find('#Evidence');
    var progressContainerElement = uploaderElement.find('#uploadProgressContainer');
    var progressElement = progressContainerElement.find('.progress-bar');

    uploaderElement.find('input').prop('disabled', 'disabled');
    uploaderElement.find('button').prop('disabled', 'disabled');
    evidenceElement.hide();
    progressElement.css('width', '0%');
    progressElement.find('span').first().show();
    progressElement.find('span').last().hide();
    progressContainerElement.show();
    processingVisible = false;
    _clearErrors();
  };

  var _getEvidenceModel = function _getEvidenceModel(file, result) {
    return {
      name: file.name,
      size: file.size,
      type: file.type,
      Result: result,
      FilterGroup: jquery('#FilterGroup').val()
    };
  };

  var _uploadProgress = function _uploadProgress(progressEvent) {
    if (!progressEvent.lengthComputable) {
      return;
    }

    var percentComplete = Math.round((progressEvent.loaded / progressEvent.total) * 100);

    var progressContainerElement = uploaderElement.find('#uploadProgressContainer');
    var progressElement = progressContainerElement.find('.progress-bar');

    progressElement.css('width', percentComplete + '%');
    progressElement.find('span').first().text(percentComplete + '%');

    if (percentComplete === 100) {
      if (processingVisible) {
        return;
      }

      processingVisible = true;

      setTimeout(function()
      {
        progressElement.find('span').first().hide();
        progressElement.find('span').last().show();
      }, 500);
    }
  };

  var _uploadSuccessful = function _uploadSuccessful(leaderboardUrl) {
    window.location.href = leaderboardUrl;
  };

  var _uploadFailed = function _uploadFailed(err) {
    if (err.status === 400 && err.responseJSON === 'unsupported file') {
      _showErrors([
        {
          label: 'Evidence',
          message: 'File is not supported. Please select a video or image to upload.'
        }
      ]);
    } else {
      _showErrors([
        {
          label: 'Evidence',
          message: 'Could not upload. Please try again.'
        }
      ]);
    }
    
    var progressContainerElement = uploaderElement.find('#uploadProgressContainer');
    var progressElement = progressContainerElement.find('.progress-bar');
    var evidenceElement = uploaderElement.find('#Evidence');

    uploaderElement.find('button').prop('disabled', '');
    uploaderElement.find('input').prop('disabled', '');
    progressContainerElement.hide();
    evidenceElement.show();
  };

  var _uploadEvidence = function _uploadEvidence(evidenceElement) {
    var fileData = evidenceElement[0].files[0];
    var result = jquery('#Result').val();
    var evidence = _getEvidenceModel(fileData, result);

    jquery.post('sign-upload/', evidence).done(function(data) {
      jquery.ajax({
        url: data.uploadUrl,
        type: 'put',
        crossDomain: true,
        data: fileData,
        contentType: false,
        processData: false,
        xhr: function () {
          var xhr = new window.XMLHttpRequest();
          xhr.upload.addEventListener('progress', _uploadProgress, false);
          return xhr;
        },
        success: function() {
          _uploadSuccessful(data.leaderboardUrl);
        }
      }).error(function(err) {
        _uploadFailed(err);
      });
    }).error(function(err) {
      _uploadFailed(err);
    });
  };

  var _validateGroupSelected = function _validateGroupSelected() {
    if (jquery('#FilterGroup').length > 0 && !jquery('#FilterGroup').val()) {
      _showErrors([
        {
          label: 'Group',
          message: 'Please make a selection.'
        }
      ]);

      jquery('#groupValidation').addClass('has-error');

      return false;
    }

    return true;
  };
  
  var _validateResultForCount = function _validateResultForCount(result) {
    if (isNaN(result) || parseInt(result) < 1 || parseInt(result) > 500) {
      _showErrors([
        {
          label: 'Result',
          message: 'Value must be between 1 and 500.'
        }
      ]);

      jquery('#resultValidation').addClass('has-error');

      return false;
    }
    
    return true;
  };
  
  var _validateResultForTime = function _validateResultForTime(result) {
    var parsedTime = moment(result, 'H:mm:ss', true);
    
    if (!parsedTime.isValid()) {
      _showErrors([
        {
          label: 'Result',
          message: 'Value must be a time in the format hh:mm:ss.'
        }
      ]);

      jquery('#resultValidation').addClass('has-error');

      return false;
    }
    
    var milliseconds = parsedTime.diff(moment().startOf('day'), 'milliseconds');
    
    if (milliseconds < 1 || milliseconds > 2147483647) {
      _showErrors([
        {
          label: 'Result',
          message: 'Value must be a time in the format hh:mm:ss and no greater than 596 hours.'
        }
      ]);

      jquery('#resultValidation').addClass('has-error');

      return false;
    }
    
    return true;
  };
  
  var _validateResultForDistance = function _validateResultForDistance(result) {
    if (isNaN(result) || parseInt(result) < 1 || parseInt(result) > 2147483) {
      _showErrors([
        {
          label: 'Result',
          message: 'Value must be a distance between 1 and 2147483 meters.'
        }
      ]);

      jquery('#resultValidation').addClass('has-error');

      return false;
    }
    
    return true;
  };

  var _validateResult = function _validateResult() {
    var resultElement = jquery('#Result');
    var resultValue = resultElement.val();
    
    if (!resultValue || resultValue.trim().length < 1) {
      _showErrors([
        {
          label: 'Result',
          message: 'Field cannot be blank.'
        }
      ]);

      jquery('#resultValidation').addClass('has-error');
      resultElement.val('');

      return false;
    }
    
    switch (measurement) {
      case 'count':
        return _validateResultForCount(resultValue);
        
      case 'time':
        return _validateResultForTime(resultValue);
        
      case 'distance':
        return _validateResultForCount(resultValue);
    }

    return false;
  };

  var _validate = function _validate() {
    _clearErrors();

    var isGroupValid = _validateGroupSelected();
    var isResultValid = _validateResult();

    return isGroupValid && isResultValid;
  };

  var _init = function _init() {
    measurement = jquery('#Measurement').val();
    
    $(uploaderElement).submit(function(submitEvent) {
      var evidenceElement = uploaderElement.find('#Evidence');

      if (evidenceElement.val()) {
        submitEvent.preventDefault();

        if (_validate()) {
          _renderUploading();
          _uploadEvidence(evidenceElement);
        }
      }
    });
  };

  _init();
};
