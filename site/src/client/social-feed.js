var SocialFeed = function(jquery, likeFlagButtons) {
  'use strict';
  
  var button = null;
  
  var _init = function _init() {
    button = jquery('.social-feed [load-more]');
    button.click(_loadMoreSocialUpdates);
  };
  
  var _loadMoreSocialUpdates = function _loadMoreSocialUpdates() {
    var lastTimestamp = jquery('.social-feed [data-update-timestamp]').last().data('updateTimestamp');
    var userId = jquery('.social-feed').attr('data-user-id');
    var showOnlyMine = jquery('.social-feed[data-show-only-mine]');
    
    if (showOnlyMine.size() > 0){
      showOnlyMine = true;
    } else {
      showOnlyMine = false;
    }

    button.prop('disabled', true);
    jquery.get('/api/social/updates/since/' + lastTimestamp + '/' + userId + '/' + showOnlyMine + '/').done(_appendSocialUpdates);
  };
  
  var _appendSocialUpdates = function _appendSocialUpdates(socialUpdates) {
    var lastEntry = jquery('.social-feed [data-update-timestamp]').last();
    
    var currentIds = _getUpdateIds();
    
    if (socialUpdates) {    
      lastEntry.after(socialUpdates);
      button.prop('disabled', false);
      
      var allIds = _getUpdateIds();
      
      for (var index = 0; index < allIds.length; index++) {
        var id = allIds[index];
        if (currentIds.indexOf(id) === -1) {
          likeFlagButtons.attachHandlers(id);
        }
      }
    } else {
      button.fadeOut();
    }
  };
  
  var _getUpdateIds = function _getUpdateIds() {
    var ids = [];
    
    jquery('[data-update-timestamp]').each(function() {
      ids.push(jquery(this).data('updateId'));
    });
    
    return ids;
  };
  
  _init();
};