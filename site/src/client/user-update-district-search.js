var UserUpdateDistrictSearch = function(jquery, Bloodhound) {
  'use strict';
  
  var _configureLocationTypeahead = function _configureLocationTypeahead(regionId) {
    jquery('#prefetch-edit-district .typeahead').typeahead(null, {
      source: new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '/api/location/districts/' + regionId + '/'
      })
    });
  };

  var _init = function _init() {
    var regionElement = jquery('#Region');
    
    _configureLocationTypeahead(regionElement.val());
    
    regionElement.change(function() {
      jquery('#prefetch-edit-district .typeahead').typeahead('destroy');
      _configureLocationTypeahead(regionElement.val());
    });
  };

  _init();
};