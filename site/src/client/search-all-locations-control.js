var SearchAllLocationsControl = function(jquery, Bloodhound) {
  'use strict';

  var _init = function _init() {
    var locationTypeahead = new Bloodhound({
      datumTokenizer: Bloodhound.tokenizers.whitespace,
      queryTokenizer: Bloodhound.tokenizers.whitespace,
      prefetch: {
        url: '/api/locations/',
        cache: false
      }
    });

    jquery('#search-all-locations .typeahead').typeahead(null, {
      source: locationTypeahead
    });
  };

  _init();
};
