if (process.env.NEW_RELIC_LICENSE_KEY) {
  require('newrelic');
}

var RedisDB = require('./db/redis-db');
var PostgresDB = require('./db/postgres-db');
var Server = require('./server');
var Config = require('./config');
var IoC = require('./ioc');
var throng = require('throng');

var logger = console;
var config = new Config();
logger.log('Environment: %s.', config.isProduction ? 'production' : 'development');

function start() {
  process.on('uncaughtException', function(err) {
    logger.log('Caught exception:', err);
  });
  
  var redisDB = new RedisDB(config.redisURL, logger);
  var postgresDB = new PostgresDB(config, logger);

  redisDB.connect(function connectedToRedisDB(redisClient) {
    postgresDB.connect().then(function connectedToPostgresDB(db) {
      var ioc = new IoC(config, redisClient, db, logger);
      var server = new Server(ioc);
      server.listen(config.port);
    });
  });
}

throng(start, {
  workers: config.workers,
  lifetime: Infinity
});