module.exports = function PageRepository(db) {
  db.registerParameters('get_page', [ 'int', 'text' ]);
  
  var _getPage = function getPage(countryId, name) {
    return db.queryJson('get_page', [ countryId, name ]);
  };

  return {
    getPage: _getPage
  };
};