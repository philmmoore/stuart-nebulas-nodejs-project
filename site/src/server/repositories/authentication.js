module.exports = function AuthenticationRepository(db) {
  db.registerParameters('auth_is_username_or_email_registered', [ 'text' ]);
  db.registerParameters('user_is_email_registered', [ 'text' ]);
  db.registerParameters('auth_login', [ 'text', 'bytea', 'text', 'text' ]);
  db.registerParameters('auth_backdoor_login', [ 'text', 'text', 'text' ]);
  db.registerParameters('auth_password_reset', [ 'text' ]);
  db.registerParameters('auth_is_password_reset_key_valid', [ 'uuid' ]);
  db.registerParameters('auth_reset_password_validate_email_key', [ 'text', 'uuid' ]);
  db.registerParameters('auth_update_password', [ 'int', 'bytea', 'text', 'text' ]);
  db.registerParameters('auth_confirm_email', [ 'uuid', 'text', 'text' ]);

  var _getIsUsernameRegistered = function _getIsUsernameRegistered(usernameOrEmail) {
    return db.queryJson('auth_is_username_or_email_registered', [ usernameOrEmail ]);
  };

  var _getIsEmailRegistered = function _getIsEmailRegistered(email) {
    return db.queryJson('user_is_email_registered', [ email ]);
  };

  var _authenticate = function authenticate(user, hash, ipAddress, session) {
    return db.queryJson('auth_login', [ user, hash, ipAddress, session ]);
  };

  var _backdoorAuthenticate = function _backdoorAuthenticate(user, ipAddress, session) {
    return db.queryJson('auth_backdoor_login', [ user, ipAddress, session ]);
  };

  var _resetPassword = function resetPassword(email) {
    return db.queryJson('auth_password_reset', [ email ]);
  };

  var _isPasswordResetKeyValid = function _isPasswordResetKeyValid(key) {
    return db.queryJson('auth_is_password_reset_key_valid', [ key ]);
  };

  var _validateResetEmailAndKey = function _validateResetEmailAndKey(email, key) {
    return db.queryJson('auth_reset_password_validate_email_key', [ email, key ]);
  };

  var _performPasswordReset = function _performPasswordReset(userId, hash, ipAddress, session) {
    return db.queryJson('auth_update_password', [ userId, hash, ipAddress, session ]);
  };

  var _confirmEmail = function _confirmEmail(key, ipAddress, session) {
    return db.queryJson('auth_confirm_email', [ key, ipAddress, session ]);
  };

  return {
    getIsUsernameRegistered: _getIsUsernameRegistered,
    getIsEmailRegistered: _getIsEmailRegistered,
    resetPassword: _resetPassword,
    authenticate: _authenticate,
    backdoorAuthenticate: _backdoorAuthenticate,
    isPasswordResetKeyValid: _isPasswordResetKeyValid,
    validateResetEmailAndKey: _validateResetEmailAndKey,
    performPasswordReset: _performPasswordReset,
    confirmEmail: _confirmEmail
  };
};