var moment= require('moment');

module.exports = function UserRepository(db) {
  db.registerParameters('configuration_get', [ 'int' ]);
  db.registerParameters('user_get_details', [ 'text' ]);
  db.registerParameters('user_get_notifications', [ 'int' ]);
  db.registerParameters('user_acknowledge_notification', [ 'int', 'int' ]);
  db.registerParameters('user_set_height', [ 'int', 'smallint' ]);
  db.registerParameters('user_set_weight', [ 'int', 'int' ]);
  db.registerParameters('location_get_regions', [ 'int' ]);
  db.registerParameters('location_get_districts_for_region_id', [ 'int' ]);
  db.registerParameters('user_set_location', [ 'int', 'int', 'int' ]);
  db.registerParameters('user_set_gym', [ 'int', 'text' ]);
  db.registerParameters('user_set_routine', [ 'int', 'int' ]);
  db.registerParameters('user_activity', [ 'int', 'timestamp', 'timestamp', 'timestamp', 'timestamp' ]);
  db.registerParameters('user_follow', [ 'text', 'int' ]);
  db.registerParameters('user_unfollow', [ 'text', 'int' ]);
  db.registerParameters('user_set_client', [ 'int', 'text' ]);
  db.registerParameters('user_unset_client', [ 'int', 'text' ]);
  db.registerParameters('user_get_privacy_preferences', [ 'int' ]);
  db.registerParameters('user_set_privacy', [ 'int', 'bool', 'bool', 'bool', 'bool', 'bool', 'bool' ]);
  db.registerParameters('user_get_email_preference', [ 'int' ]);
  db.registerParameters('user_set_email_preference', [ 'int', 'bool', 'bool', 'bool', 'bool', 'bool' ]);
  db.registerParameters('user_get_nominations', [ 'int' ]);
  db.registerParameters('user_reviews', [ 'int', 'timestamp', 'int' ]);
  db.registerParameters('user_review_submit', [ 'int', 'int', 'text', 'text', 'int' ]);
  
  var _getConfiguration = function _getConfiguration(countryId) {
    return db.queryJson('configuration_get', [ countryId ]);
  };
  
  var _getUser = function _getUser(username) {
    return db.queryJson('user_get_details', [ username ]);
  };

  var _getNotifications = function _getNotifications(userId) {
    return db.queryJson('user_get_notifications', [ userId ]);
  };

  var _acknowledgeNotification = function _acknowledgeNotification(userId, notificationId) {
    return db.queryJson('user_acknowledge_notification', [ userId, notificationId ]);
  };
  
  var _updateHeight = function _updateHeight(userId, height) {
    return db.queryJson('user_set_height', [ userId, height ]);
  };
  
  var _updateWeight = function _updateWeight(userId, weight) {
    return db.queryJson('user_set_weight', [ userId, weight ]);
  };
  
  var _getRegions = function _getRegions(countryId) {
    return db.queryJson('location_get_regions', [ countryId ]);
  };
  
  var _getDistrictsForRegionId = function _getDistrictsForRegionId(regionId) {
    return db.queryJson('location_get_districts_for_region_id', [ regionId ]);
  };
  
  var _setLocation = function _setLocation(userId, regionId, districtId) {
    var params = [
      userId,
      regionId,
      districtId
    ];
    
    return db.queryJson('user_set_location', params);
  };
  
  var _setGym = function _setGym(userId, gym) {
    return db.queryJson('user_set_gym', [ userId, gym ]);
  };
  
  var _getRoutineTypes = function _getRoutineTypes() {
    return db.queryJson('routine_type_get');
  };
  
  var _setRoutineType = function _setRoutineType(userId, routineTypeId) {
    return db.queryJson('user_set_routine', [ userId, routineTypeId ]);
  };
  
  var _getActivities = function _getActivities(userId, currentPeriodStart, currentPeriodEnd, previousPeriodStart, previousPeriodEnd) {
    var params = [
      userId,
      currentPeriodStart,
      currentPeriodEnd,
      previousPeriodStart,
      previousPeriodEnd
    ];

    return db.queryJson('user_activity', params);
  };
  
  var _follow = function _follow(username, followerUserId) {
    return db.queryJson('user_follow', [ username, followerUserId ]);
  };
  
  var _unfollow = function _unfollow(username, followerUserId) {
    return db.queryJson('user_unfollow', [ username, followerUserId ]);
  };
  
  var _setClient = function _follow(userId, followerUsername) {
    return db.queryJson('user_set_client', [ userId, followerUsername ]);
  };
  
  var _unsetClient = function _unfollow(userId, followerUsername) {
    return db.queryJson('user_unset_client', [ userId, followerUsername ]);
  };
  
  var _getPrivacyPreferences = function getPrivacyPreferences(userId) {
    return db.queryJson('user_get_privacy_preferences', [ userId ]);
  };
  
  var _setPrivacyPreferences = function _setPrivacyPreferences(privacyPreferences) {
    var params = [
      privacyPreferences.userId,
      privacyPreferences.gymFitEmailOptIn,
      privacyPreferences.partnersEmailOptIn,
      privacyPreferences.gymFitSMSOptIn,
      privacyPreferences.partnersSMSOptIn,
      privacyPreferences.gymFitOnlineOptIn,
      privacyPreferences.partnersOnlineOptIn
    ];

    return db.queryJson('user_set_privacy', params);
  };
  
  var _getEmailPreference = function _getEmailPreference(userId) {
    return db.queryJson('user_get_email_preference', [ userId ]);
  };
  
  var _setEmailPreference = function _setEmailPreference(preference) {
    var params = [
      preference.userId,
      preference.ranking,
      preference.progress,
      preference.news,
      preference.promotion,
      preference.nomination
    ];

    return db.queryJson('user_set_email_preference', params);
  };
  
  var _getNominations = function _getNominations(userId) {
    return db.queryJson('user_get_nominations', [ userId ]);
  };

  var _getReviews = function _getReviews(userId, timestamp, count) {
    // If no count passed then send all reviews back
    if (!count){
      count = null;
    }
    if (!timestamp){
      timestamp = moment();
    }
    return db.queryJson('user_reviews', [ userId, timestamp, count]);
  };  

  var _leaveReview = function _leaveReview(userId, body) {
    return db.queryJson('user_review_submit', [ 
      body.userId,
      userId,
      body.title,
      body.review,
      body.rating
    ]);
  };
  
  return {
    getConfiguration: _getConfiguration,
    getUser: _getUser,
    getNotifications: _getNotifications,
    acknowledgeNotification: _acknowledgeNotification,
    updateHeight: _updateHeight,
    updateWeight: _updateWeight,
    getRegions: _getRegions,
    getDistrictsForRegionId: _getDistrictsForRegionId,
    setLocation: _setLocation,
    setGym: _setGym,
    getRoutineTypes: _getRoutineTypes,
    setRoutineType: _setRoutineType,
    getActivities: _getActivities,
    follow: _follow,
    unfollow: _unfollow,
    setClient: _setClient,
    unsetClient: _unsetClient,
    getPrivacyPreferences: _getPrivacyPreferences,
    setPrivacyPreferences: _setPrivacyPreferences,
    getEmailPreference: _getEmailPreference,
    setEmailPreference: _setEmailPreference,
    getNominations: _getNominations,
    getReviews: _getReviews,
    leaveReview: _leaveReview
  };
};
