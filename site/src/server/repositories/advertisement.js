module.exports = function AdvertisementRepository(db) {
  db.registerParameters('advert_get_for_location', [ 'int', 'int', 'int', 'int' ]);

  var _getAdvertisements = function _getAdvertisements(location) {
    var params = [
        location.countryId,
        location.regionId,
        location.countyId,
        location.districtId
    ];

    return db.queryJson('advert_get_for_location', params);
  };

  return {
    getAdvertisements: _getAdvertisements
  };
};