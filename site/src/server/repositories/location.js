module.exports = function LocationRepository(db) {
  db.registerParameters('location_get_country', [ 'text' ]);
  db.registerParameters('location_get_region', [ 'int', 'text' ]);
  db.registerParameters('location_get_county', [ 'int', 'text' ]);
  db.registerParameters('location_get_district', [ 'int', 'text' ]);
  
  var _getCountry = function _getLocation(countryName) {
    return db.queryJson('location_get_country', [ countryName ]);
  };

  var _getRegion = function _getRegion(countryId, regionName) {
    return db.queryJson('location_get_region', [ countryId, regionName ]);
  };

  var _getCounty = function _getCounty(regionId, countyName) {
    return db.queryJson('location_get_county', [ regionId, countyName ]);
  };

  var _getDistrict = function _getDistrict(countyId, districtName) {
    return db.queryJson('location_get_district', [ countyId, districtName ]);
  };

  return {
    getCountry: _getCountry,
    getRegion: _getRegion,
    getCounty: _getCounty,
    getDistrict: _getDistrict
  };
};
