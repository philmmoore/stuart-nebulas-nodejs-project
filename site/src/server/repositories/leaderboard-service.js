module.exports = function LeaderboardServiceRepository(db) {
  db.registerParameters('get_challenge', [ 'text' ]);
  db.registerParameters('user_get_details', [ 'text' ]);
  db.registerParameters('age_filter_get_range', [ 'text' ]);
  db.registerParameters('weight_filter_get_range', [ 'text' ]);
  db.registerParameters('get_challenge_filters', [ 'int', 'int' ]);
  db.registerParameters('leaderboard_user_position', [ 'int', 'int', 'int', 'int', 'int', 'int', 'timestamp', 'timestamp', 'bit', 'smallint', 'smallint', 'int', 'int', 'bit']);
  
  var _getChallenge = function _getChallenge(challengeName) {
    return db.queryJson('get_challenge', [ challengeName ]);
  };
  
  var _getUser = function _getUser(username) {
    return db.queryJson('user_get_details', [ username ]);
  };
  
  var _getAgeRange = function _getAgeRange(ageRangeName) {
    return db.queryJson('age_filter_get_range', [ ageRangeName ]);
  };

  var _getWeightRange = function _getWeightRange(weightRangeName) {
    return db.queryJson('weight_filter_get_range', [ weightRangeName ]);
  };
  
  var _getFilterOptions = function _getFilterOptions(challengeId, userId) {
    return db.queryJson('get_challenge_filters', [ challengeId, userId ]);
  };
  
  var _getLeaderboardRankings = function _getLeaderboardRankings(filter) {
    var params = [
      filter.challengeId,
      filter.challengeGroupId,
      filter.districtId,
      filter.countyId,
      filter.regionId,
      filter.countryId,
      filter.currentPeriodStart,
      filter.currentPeriodEnd,
      _getIsMaleFilter(filter.filterGender),
      filter.filterAgeMin || null,
      filter.filterAgeMax || null,
      filter.filterWeightMin || null,
      filter.filterWeightMax || null,
      filter.filterOnlyValidated || false ? 1 : 0
    ];

    return db.queryJson('leaderboard_user_position', params).then(function(data) {
      if (data && data.rankings.length > 0) {
        return data.rankings;
      }

      return [];
    });
  };
  
  var _getIsMaleFilter = function _getIsMaleFilter(filterGender) {
    if (filterGender === 'male')
    {
      return '1';
    }
    
    if (filterGender === 'female')
    {
      return '0';
    }
    
    return null;
  };
  
  return {
    getChallenge: _getChallenge,
    getUser: _getUser,
    getAgeRange: _getAgeRange,
    getWeightRange: _getWeightRange,
    getFilterOptions: _getFilterOptions,
    getLeaderboardRankings: _getLeaderboardRankings
  };
};