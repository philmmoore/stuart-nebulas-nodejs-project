module.exports = function ResultRepository(db) {
  db.registerParameters('user_get_notifications', [ 'int' ]);
  db.registerParameters('challenge_get_groups', [ 'int', 'int' ]);
  db.registerParameters('result_submit', [ 'int', 'int', 'int', 'int', 'text' ]);
  db.registerParameters('result_get', [ 'int', 'int' ]);
  db.registerParameters('user_get_nominators_to_notify', [ 'int', 'int' ]);
  db.registerParameters('user_nomination_accepted', [ 'int', 'int', 'int' ]);
  
  var _getNotifications = function _getNotifications(userId) {
    return db.queryJson('user_get_notifications', [ userId ]);
  };

  var _getChallengeGroups = function _getChallengeGroups(challengeId, userId) {
    return db.queryJson('challenge_get_groups', [ challengeId, userId ]);
  };

  var _saveResult = function _saveResult(userId, challengeId, challengeGroupId, result, type) {
    var params = [
      userId,
      challengeId,
      challengeGroupId,
      result,
      type
    ];

    return db.queryJson('result_submit', params);
  };
  
  var _get = function _get(resultId, userId) {
    return db.queryJson('result_get', [ resultId, userId ]);
  };
  
  var _getNominationData = function _getNominationData(userId, challengeId) {
    return db.queryJson('user_get_nominators_to_notify', [ userId, challengeId ]);
  };
  
  var _nominationAccepted = function _nominationAccepted(userId, challengeId, nomineeUserId) {
    return db.queryJson('user_nomination_accepted', [ userId, challengeId, nomineeUserId ]);
  };

  return {
    getNotifications: _getNotifications,
    saveResult: _saveResult,
    getChallengeGroups: _getChallengeGroups,
    get: _get,
    getNominationData: _getNominationData,
    nominationAccepted: _nominationAccepted
  };
};
