module.exports = function LeaderboardRepository(db) {
  db.registerParameters('configuration_get', [ 'int' ]);
  db.registerParameters('get_challenge_filters', [ 'int', 'int' ]);
  db.registerParameters('leaderboard_get_user_filter', [ 'int' ]);
  db.registerParameters('age_filter_get_range', [ 'text' ]);
  db.registerParameters('weight_filter_get_range', [ 'text' ]);
  db.registerParameters('leaderboard_user_position', [ 'int', 'int', 'int', 'int', 'int', 'int', 'timestamp', 'timestamp', 'bit', 'smallint', 'smallint', 'int', 'int', 'bit']);
  db.registerParameters('leaderboard_search', [ 'int', 'int', 'int', 'int', 'int', 'int', 'timestamp', 'timestamp', 'timestamp', 'timestamp', 'int', 'int', 'bit', 'smallint', 'smallint', 'int', 'int', 'bit', 'int']);
  
  var _getConfiguration = function _getConfiguration(countryId) {
    return db.queryJson('configuration_get', [ countryId ]);
  };

  var _getFilters = function _getFilters(challengeId, userId) {
    return db.queryJson('get_challenge_filters', [ challengeId, userId ]);
  };

  var _getUserFilter = function _getUserFilter(userId) {
    return db.queryJson('leaderboard_get_user_filter', [ userId ]).then(function(data) {
      data.isMale = data.isMale === '1' ? true : false;
      return data;
    });
  };

  var _getAgeRange = function _getAgeRange(ageRangeName) {
    return db.queryJson('age_filter_get_range', [ ageRangeName ]);
  };

  var _getWeightRange = function _getWeightRange(weightRangeName) {
    return db.queryJson('weight_filter_get_range', [ weightRangeName ]);
  };

  var _getLeaderboardUserPositions = function _getLeaderboardUserPositions(filter) {
    var params = [
      filter.challengeId,
      filter.challengeGroupId,
      filter.districtId,
      filter.countyId,
      filter.regionId,
      filter.countryId,
      filter.currentPeriodStart,
      filter.currentPeriodEnd,
      filter.isMale === null || filter.isMale === undefined ? null : (filter.isMale ? 1 :0),
      filter.ageMin || null,
      filter.ageMax || null,
      filter.weightMin || null,
      filter.weightMax || null,
      filter.onlyShowValidated === null || filter.onlyShowValidated === undefined ? null : (filter.onlyShowValidated ? 1 :0)
    ];

    return db.queryJson('leaderboard_user_position', params).then(function(data) {
      if (data.rankings.length > 0) {
        return data.rankings;
      }

      return null;
    });
  };

  var _getLeaderboardResult = function _getLeaderboardResult(filter, userId) {
    var params = [
      filter.challengeId,
      filter.challengeGroupId,
      filter.districtId,
      filter.countyId,
      filter.regionId,
      filter.countryId,
      filter.currentPeriodStart,
      filter.currentPeriodEnd,
      filter.lastPeriodStart,
      filter.lastPeriodEnd,
      filter.resultsPerPage,
      filter.page,
      filter.isMale === null || filter.isMale === undefined ? null : (filter.isMale ? 1 :0),
      filter.ageMin || null,
      filter.ageMax || null,
      filter.weightMin || null,
      filter.weightMax || null,
      filter.onlyShowValidated === null || filter.onlyShowValidated === undefined ? null : (filter.onlyShowValidated ? 1 :0),
      userId
    ];

    return db.queryJson('leaderboard_search', params);
  };

  return {
    getConfiguration: _getConfiguration,
    getFilters: _getFilters,
    getUserFilter: _getUserFilter,
    getAgeRange: _getAgeRange,
    getWeightRange: _getWeightRange,
    getLeaderboardUserPositions: _getLeaderboardUserPositions,
    getLeaderboardResult: _getLeaderboardResult
  };
};
