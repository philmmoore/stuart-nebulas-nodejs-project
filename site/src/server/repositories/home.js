module.exports = function HomeRepository(db) {
  db.registerParameters('configuration_get', [ 'int' ]);
  
  var _getConfiguration = function _getConfiguration(countryId) {
    return db.queryJson('configuration_get', [ countryId ]);
  };

  var _getChallenges = function _getChallenges() {
    return db.queryJson('get_challenges');
  };

  return {
    getConfiguration: _getConfiguration,
    getChallenges: _getChallenges
  };
};
