module.exports = function SocialRepository(db) {
  db.registerParameters('social_get_user_updates', [ 'int', 'timestamp', 'int' , 'bool', 'bool']);
  db.registerParameters('social_get_update', [ 'int', 'uuid' ]);
  
  var _getUpdates = function _getUpdates(userId, fromTimestamp, numberToReturn, showOnlyMine, ignoreMine) {

    if (!showOnlyMine && showOnlyMine !== false){
      showOnlyMine = true;
    }
    if (!ignoreMine && ignoreMine !== false){
      ignoreMine = false;
    }

    return db.queryJson('social_get_user_updates', [ userId, fromTimestamp, numberToReturn , showOnlyMine, ignoreMine]);
  };
  
  var _getUpdate = function _getUpdate(userId, updateId) {
    return db.queryJson('social_get_update', [ userId, updateId ]);
  };
  
  return {
    getUpdates: _getUpdates,
    getUpdate: _getUpdate
  };
};