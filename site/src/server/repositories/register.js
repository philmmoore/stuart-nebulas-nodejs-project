module.exports = function RegisterRepository(db) {
  db.registerParameters('user_is_email_registered', [ 'text' ]);
  db.registerParameters('user_is_email_banned', [ 'text' ]);
  db.registerParameters('user_register_is_username_registered', [ 'text' ]);
  db.registerParameters('user_registration_add_step_1_details', [ 'int', 'text', 'bytea' ]);
  db.registerParameters('user_registration_add_step_2_details', [ 'int', 'text', 'text', 'text', 'date', 'int' ]);
  db.registerParameters('user_registration_add_user', [ 'int' ]);
  db.registerParameters('user_set_privacy', [ 'int', 'bool', 'bool', 'bool', 'bool', 'bool', 'bool' ]);
  db.registerParameters('location_get_regions', [ 'int' ]);
  db.registerParameters('location_get_districts_for_user_region', [ 'int' ]);
  db.registerParameters('user_register_set_parameters', [ 'int', 'bit', 'smallint', 'int', 'text' ]);
  db.registerParameters('register_persist_step_6_details', [ 'int', 'text', 'int', 'text', 'bool' ]);
  db.registerParameters('user_get', [ 'int' ]);
  db.registerParameters('user_set_is_gym', [ 'int', 'bool' ]);
  db.registerParameters('user_set_is_pt', [ 'int', 'bool' ]);
  
  var _userSetIsGym = function _userSetIsGym(userId, isGym) {
    return db.queryJson('user_set_is_gym', [ userId, isGym ]);
  };

  var _userSetIsPt = function _userSetIsPt(userId, isPt) {
    return db.queryJson('user_set_is_pt', [ userId, isPt ]);
  };

  var _getIsEmailRegistered = function _getIsEmailRegistered(email) {
    return db.queryJson('user_is_email_registered', [ email ]);
  };
  
  var _getIsEmailBanned = function _getIsEmailBanned(email) {
    return db.queryJson('user_is_email_banned', [ email ]);
  };

  var _getIsUsernameRegistered = function _getIsUsernameRegistered(username) {
    return db.queryJson('user_register_is_username_registered', [ username ]);
  };

  var _addStep1Details = function _addStep1Details(countryId, email, hash) {
    var params = [
      countryId,
      email,
      hash
    ];

    return db.queryJson('user_registration_add_step_1_details', params);
  };

  var _addStep2Details = function _addStep2Details(registrationId, firstName, lastName, username, dateOfBirth, regionId) {
    var params = [
      registrationId,
      firstName,
      lastName,
      username,
      dateOfBirth,
      regionId
    ];

    return db.queryJson('user_registration_add_step_2_details', params);
  };

  var _addUser = function _addUser(registrationId) {
    return db.queryJson('user_registration_add_user', [ registrationId ]);
  };

  var _setPrivacyPreferences = function _setPrivacyPreferences(privacyPreferences) {
    var params = [
      privacyPreferences.userId,
      privacyPreferences.gymFitEmailOptIn,
      privacyPreferences.partnersEmailOptIn,
      privacyPreferences.gymFitSMSOptIn,
      privacyPreferences.partnersSMSOptIn,
      privacyPreferences.gymFitOnlineOptIn,
      privacyPreferences.partnersOnlineOptIn
    ];

    return db.queryJson('user_set_privacy', params);
  };

  var _getRegions = function _getRegions(countryId) {
    return db.queryJson('location_get_regions', [ countryId ]);
  };

  var _getDistrictsForUserRegion = function _getDistrictsForUserRegion(userId) {
    return db.queryJson('location_get_districts_for_user_region', [ userId ]);
  };

  var _setUserParameters = function _setUserParameters(userId, isMale, height, weight, userDistrict) {
    var params = [
      userId,
      isMale,
      height,
      weight,
      userDistrict
    ];

    return db.queryJson('user_register_set_parameters', params);
  };

  var _getRoutineTypes = function _getRoutineTypes() {
    return db.queryJson('routine_type_get');
  };

  var _persistStep6Details = function _persistStep6Details(userId, mobileNumber, routineTypeId, gym, isPersonalTrainer) {
    var params = [
      userId,
      mobileNumber,
      routineTypeId,
      gym,
      isPersonalTrainer
    ];

    return db.queryJson('register_persist_step_6_details', params);
  };

  var _getRegisteredUserDetails = function _getRegisteredUserDetails(userId) {
    return db.queryJson('user_get', [ userId ]);
  };

  return {
    getIsEmailRegistered: _getIsEmailRegistered,
    getIsEmailBanned: _getIsEmailBanned,
    getIsUsernameRegistered: _getIsUsernameRegistered,
    addStep1Details: _addStep1Details,
    addStep2Details: _addStep2Details,
    addUser: _addUser,
    setPrivacyPreferences: _setPrivacyPreferences,
    userSetIsGym: _userSetIsGym,
    userSetIsPt: _userSetIsPt,
    getRegions: _getRegions,
    getDistrictsForUserRegion: _getDistrictsForUserRegion,
    setUserParameters: _setUserParameters,
    getRoutineTypes: _getRoutineTypes,
    persistStep6Details: _persistStep6Details,
    getRegisteredUserDetails: _getRegisteredUserDetails
  };
};
