module.exports = function NominateRepository(db) {
  db.registerParameters('user_get_nomination_suggestions', [ 'int', 'int' ]);
  db.registerParameters('nomination_get_possible_nominees', [ 'int', 'int' ]);
  db.registerParameters('search_user', [ 'text', 'int', 'int' ]);
  db.registerParameters('search_location', [ 'text', 'int', 'int', 'int', 'int', 'int', 'int' ]);
  db.registerParameters('user_nominate', [ 'int', 'int', 'text' ]);
  
  var _getNominationSuggestions = function _getNominationSuggestions(userId, challengeId) {
    return db.queryJson('user_get_nomination_suggestions', [ userId, challengeId ]);
  };
  
  var _getAllowedNominees = function _getAllowedNominees(userId, challengeId) {
    return db.queryJson('nomination_get_possible_nominees', [ userId, challengeId ]);
  };

  var _searchByUser = function _searchByUser(query, resultsPerPage, page) {
    return db.queryJson('search_user', [ query, resultsPerPage, page ]);
  };

  var _searchByLocation = function _searchByLocation(query, districtId, countyId, regionId, countryId, resultsPerPage, page) {
    var params = [
      query,
      districtId,
      countyId,
      regionId,
      countryId,
      resultsPerPage,
      page
    ];
    
    return db.queryJson('search_location', params);
  };
  
  var _nominate = function _nominate(userId, challengeId, nomineeUsername) {
    return db.queryJson('user_nominate', [ userId, challengeId, nomineeUsername ]);
  };

  return {
    getNominationSuggestions: _getNominationSuggestions,
    searchByUser: _searchByUser,
    searchByLocation: _searchByLocation,
    getAllowedNominees: _getAllowedNominees,
    nominate :_nominate
  };
};