module.exports = function LocationResolverServiceRepository(db) {
  var _getAllLocations = function _getAllLocations() {
    return db.queryJson('location_get_all');
  };

  return {
    getAllLocations: _getAllLocations
  };
};