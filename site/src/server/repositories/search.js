module.exports = function SearchRepository(db) {
  db.registerParameters('search_user', [ 'text', 'int', 'int' ]);
  db.registerParameters('search_location', [ 'text', 'int', 'int', 'int', 'int', 'int', 'int' ]);
  
  var _searchByUser = function _searchByUser(query, resultsPerPage, page) {
    return db.queryJson('search_user', [ query, resultsPerPage, page ]);
  };
  
  var _searchByLocation = function _searchByLocation(query, districtId, countyId, regionId, countryId, resultsPerPage, page) {
    var params = [
      query,
      districtId,
      countyId,
      regionId,
      countryId,
      resultsPerPage,
      page
    ];
    
    return db.queryJson('search_location', params);
  };
  
  return {
    searchByUser: _searchByUser,
    searchByLocation: _searchByLocation
  };
};