module.exports = function APIRepository(db) {
  db.registerParameters('location_get_districts_for_region_id', [ 'int' ]);
  db.registerParameters('social_update_like', [ 'int', 'uuid' ]);
  db.registerParameters('social_update_unlike', [ 'int', 'uuid' ]);
  db.registerParameters('social_update_flag', [ 'int', 'uuid' ]);
  db.registerParameters('social_update_unflag', [ 'int', 'uuid' ]);
  db.registerParameters('social_get_user_updates', [ 'int', 'timestamp', 'int' ]);
  db.registerParameters('social_update_delete', [ 'int', 'uuid' ]);

  var _getDistrictsForRegionId = function _getDistrictsForRegionId(regionId) {
    return db.queryJson('location_get_districts_for_region_id', [ regionId ]);
  };

  var _getAllLocationNames = function _getAllLocationNames() {
    return db.queryJson('location_get_all_names');
  };
  
  var _likeSocialUpdate = function _likeSocialUpdate(updateId, userId) {
    return db.queryJson('social_update_like', [ userId, updateId ]);
  };
  
  var _unlikeSocialUpdate = function _unlikeSocialUpdate(updateId, userId) {
    return db.queryJson('social_update_unlike', [ userId, updateId ]);
  };
  
  var _flagSocialUpdate = function _flagSocialUpdate(updateId, userId) {
    return db.queryJson('social_update_flag', [ userId, updateId ]);
  };
  
  var _unflagSocialUpdate = function _unflagSocialUpdate(updateId, userId) {
    return db.queryJson('social_update_unflag', [ userId, updateId ]);
  };
  
  var _getSocialUpdates = function _getSocialUpdates(userId, fromTimestamp, numberToReturn) {
    return db.queryJson('social_get_user_updates', [ userId, fromTimestamp, numberToReturn ]);
  };
  
  var _deleteSocialUpdate = function _deleteSocialUpdate(updateId, userId) {
    return db.queryJson('social_update_delete', [ userId, updateId ]);
  };

  return {
    getDistrictsForRegionId: _getDistrictsForRegionId,
    getAllLocationNames: _getAllLocationNames,
    likeSocialUpdate: _likeSocialUpdate,
    unlikeSocialUpdate: _unlikeSocialUpdate,
    flagSocialUpdate: _flagSocialUpdate,
    unflagSocialUpdate: _unflagSocialUpdate,
    getSocialUpdates: _getSocialUpdates,
    deleteSocialUpdate: _deleteSocialUpdate
  };
};