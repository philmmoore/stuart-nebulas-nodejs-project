module.exports = function ChallengeRepository(db) {
  db.registerParameters('get_challenge', [ 'text' ]);
  db.registerParameters('challenge_get_groups', [ 'int', 'int' ]);
  
  var _getChallenge = function _getChallenge(challengeName) {
    return db.queryJson('get_challenge', [ challengeName ]);
  };

  var _getChallengeGroups = function _getChallengeGroups(challengeId, userId) {
    return db.queryJson('challenge_get_groups', [ challengeId, userId ]);
  };

  var _getChallenges = function _getChallenges() {
    return db.queryJson('get_challenges');
  };

  return {
    getChallenge: _getChallenge,
    getChallengeGroups: _getChallengeGroups,
    getChallenges: _getChallenges
  };
};
