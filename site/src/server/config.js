var Config = function config() {
  var workers = process.env.WEB_CONCURRENCY || 1;
  var redisURL = process.env.REDIS_URL || 'redis://localhost:6379';
  var postgresURL = process.env.DATABASE_URL || 'postgres://localhost:5432';
  var postgresPoolSize = process.env.DATABASE_POOL_SIZE || 4;
  var sessionSecretKey = process.env.REDIS_SESSION_SECRET_KEY || '2^£4n-AHkj2#8S@-qzK';
  var authSecretKey = process.env.AUTH_SECRET_KEY || 'Q[!3&K2h-Z*2 uKQ-8sK£28%9nT!';
  var port = process.env.PORT || 4000;
  var isProduction = process.env.NODE_ENV === 'production';
  var backdoorPassword = process.env.BACKDOOR_PASSWORD || (isProduction ? null : 'dev');
  var basicAuth = process.env.BASIC_AUTH;
  var basicAuthEnabled = basicAuth ? true : false;
  var basicAuthUser = basicAuthEnabled ? basicAuth.split(':')[0] : null;
  var basicAuthPassword = basicAuthEnabled ? basicAuth.split(':')[1] : null;
  var countriesAvailable = (process.env.AVAILABLE_COUNTRIES || (isProduction ? '' : 'gb')).split(',');
  var defaultCountry = process.env.DEFAULT_COUNTRY || 'gb';
  var defaultLanguage = process.env.DEFAULT_LANGUAGE || 'en';
  var targetHost = process.env.TARGET_HOST || 'localhost';
  var disqusShortname = process.env.DISQUS_SHORTNAME || 'gymfitdevelopment';
  var googleAnalyticsTrackingTag = process.env.GOOGLE_ANALYTICS_TRACKING_TAG;
  var isLeaderboardVisible = process.env.IS_LEADERBOARD_VISIBLE ? true : (isProduction ? false : true);
  var isLeaderboardRealtime = process.env.IS_LEADERBOARD_REALTIME ? true : (isProduction ? false : true);

  var imageUploadAWSAccessKeyID = process.env.IMAGE_UPLOAD_AWS_ACCESS_KEY_ID || 'AKIAJHDJZBQAREA5REBA';
  var imageUploadAWSSecretAccessKey = process.env.IMAGE_UPLOAD_AWS_SECRET_ACCESS_KEY || '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';
  var videoUploadAWSAccessKeyID = process.env.VIDEO_UPLOAD_AWS_ACCESS_KEY_ID || 'AKIAJRFNNQQLZRLXRQMA';
  var videoUploadAWSSecretAccessKey = process.env.VIDEO_UPLOAD_AWS_SECRET_ACCESS_KEY || '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';
  var awsRegion = process.env.AWS_REGION_NAME || 'eu-west-1';
  var awsResourcePrefix = process.env.AWS_RESOURCE_PREFIX || 'gymfit-staging-';

  var advertisementImageUrl = process.env.ADVERTISEMENT_IMAGE_URL || 'https://gymfit-staging-adverts.s3.amazonaws.com/';
  var avatarImageUrl = process.env.AVATAR_IMAGE_URL || 'https://gymfit-staging-avatars.s3.amazonaws.com/';
  var evidenceImageUrl = process.env.EVIDENCE_IMAGE_URL || 'https://gymfit-staging-evidence-photos.s3.amazonaws.com/';
  var evidenceVideoUrl = process.env.EVIDENCE_VIDEO_URL || 'https://gymfit-staging-videos.s3.amazonaws.com/';
  var socialImageUrl = process.env.SOCIAL_IMAGE_URL || 'https://gymfit-staging-social-photos.s3.amazonaws.com/';
  var socialVideoUrl = process.env.SOCIAL_VIDEO_URL || 'https://gymfit-staging-misc-videos.s3.amazonaws.com/';
  var publicSite = process.env.PUBLIC_SITE || 'http://localhost:4000/';

  if (isProduction) {
    postgresURL += '?ssl=true';
  }

  return {
    workers: workers,
    redisURL: redisURL,
    postgresURL: postgresURL,
    postgresPoolSize: postgresPoolSize,
    sessionSecretKey: sessionSecretKey,
    authSecretKey: authSecretKey,
    backdoorPassword: backdoorPassword,
    port: port,
    isProduction: isProduction,
    basicAuthEnabled: basicAuthEnabled,
    basicAuthUser: basicAuthUser,
    basicAuthPassword: basicAuthPassword,
    countriesAvailable: countriesAvailable,
    defaultCountry: defaultCountry,
    defaultLanguage: defaultLanguage,
    targetHost: targetHost,
    disqusShortname: disqusShortname,
    googleAnalyticsTrackingTag: googleAnalyticsTrackingTag,
    isLeaderboardVisible: isLeaderboardVisible,
    isLeaderboardRealtime: isLeaderboardRealtime,
    imageUploadConfig: {
      accessKeyId: imageUploadAWSAccessKeyID,
      secretAccessKey: imageUploadAWSSecretAccessKey,
      region: awsRegion
    },
    videoUploadConfig: {
      accessKeyId: videoUploadAWSAccessKeyID,
      secretAccessKey: videoUploadAWSSecretAccessKey,
      region: awsRegion
    },
    queueConfig: {
      accessKeyId: imageUploadAWSAccessKeyID,
      secretAccessKey: imageUploadAWSSecretAccessKey,
      region: awsRegion
    },
    queueUrlPrefix: 'https://sqs.eu-west-1.amazonaws.com/164535666447/' + awsResourcePrefix,
    awsResourcePrefix: awsResourcePrefix,
    advertisementImageUrl: advertisementImageUrl,
    evidenceImageUrl: evidenceImageUrl,
    evidenceVideoUrl: evidenceVideoUrl,
    socialImageUrl: socialImageUrl,
    socialVideoUrl: socialVideoUrl,
    avatarImageUrl: avatarImageUrl,
    publicSite: publicSite
  };
};

module.exports = Config;
