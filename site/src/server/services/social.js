var Promise = require('bluebird');
var moment= require('moment');

module.exports = function SocialService(repository, avatarImageUrl, evidenceImageUrl, evidenceVideoUrl, resultFormatter, socialImageUrl, socialVideoUrl, publicSite, markdownTextFormatter) {
  'use strict';
  
  var NUMBER_OF_UPDATES_TO_RETRIEVE = 10;
  
  var _getUpdates = function _getUpdates(userId, timestamp, showOnlyMine, ignoreMine) {
    if (!timestamp) {
      timestamp = moment();
    }
    return repository.getUpdates(userId, timestamp, NUMBER_OF_UPDATES_TO_RETRIEVE, showOnlyMine, ignoreMine).then(function(data) {
      if (!data) {
        return [];
      }
      
      var updates = [];
      
      for (var index = 0; index < data.length; index++) {
        updates.push(_formatUpdate(data[index]));
      }
      
      return updates;
    });
  };
  
  var _formatUpdate = function _formatUpdate(data) {
    var update = {
      id: data.id,
      userId: data.userId,
      username: data.username,
      firstName: data.firstName,
      lastName: data.lastName,
      likes: data.likes,
      flags: data.flags,
      avatar: avatarImageUrl + (data.avatarUploaded === '1' ? data.userId : 0) + '-40x40.png',
      avatarRetina: avatarImageUrl + (data.avatarUploaded === '1' ? data.userId : 0) + '-80x80.png',
      created: moment(data.created),
      images: [],
      videos: [],
      body: data.body,
      bodyHtml: (data.body ? markdownTextFormatter.format(data.body) : ''),
      userLikes: data.userLikes,
      link: publicSite + 'update/' + data.id + '/'
    };
    
    if (data.result) {
      _appendResultData(data, update);
    }
    
    _appendAttachments(data, update);
    update.type = _getUpdateType(update);
    
    return update;
  };
  
  var _appendResultData = function _appendResultData(data, update) {
    update.result = {
      result: resultFormatter.format(data.result.result, data.result.measurement),
      id: data.result.id,
      challengeName: data.result.challengeName,
      challengeGroupName: data.result.challengeGroupName      
    };
    
    if (data.result.imageValidated) {
      update.images.push(evidenceImageUrl + data.result.id);
    }
    
    if (data.result.videoValidated) {
      update.videos.push({
        videoUrl: evidenceVideoUrl + data.result.id + '.mp4',
        placeholderImageUrl: evidenceVideoUrl + data.result.id + '.jpg'
      });
    }
  };
  
  var _appendAttachments = function _appendAttachments(data, update) {
    data.images.forEach(function(imageId) {
      update.images.push(socialImageUrl + imageId);
    });
    
    data.videos.forEach(function(videoId) {
      update.videos.push({
        videoUrl: socialVideoUrl + videoId + '.mp4',
        placeholderImageUrl: socialVideoUrl + videoId + '.jpg'
      });
    });
  };
  
  var _getUpdateType = function _getUpdateType(update) {
    if (update.result) {
      return 'result';
    }
    
    if (update.videos.length > 0) {
      return 'video';
    }
    
    if (update.images.length > 0) {
      return 'image';
    }
    
    return 'text';
  };
  
  var _getUpdate = function _getUpdate(req, translator) {
    var userId = null;
    
    if (req.session && req.session.user) {
      userId = req.session.user.id;
    }
    
    var updateId = req.params.updateId;
    
    return repository.getUpdate(userId, updateId).then(function(data) {
      if (!data) {
        return Promise.reject('not found');
      }
      
      var update = _formatUpdate(data);
      
      if (update.result) {
        appendResultTitleAndDescription(update, userId, translator);
      } else {
        appendStandardTitleAndDescription(update, userId, translator);
      }
      
      if (update.videos.length > 0) {
        update.embedLink = 'https://gymfit.com/update/' + update.id + '/embed/';
      }
      
      return {
        update: update
      };
    });
  };
  
  var appendResultTitleAndDescription = function _appendResultTitleAndDescription(update, userId, translator) {    
    update.title = (update.id === userId) ? 'My ' : update.firstName + ' ' + update.lastName + '\'s ';
    update.title += update.result.challengeName + ' ' + translator('Challenge') + ' ' + translator('Result');
    update.description = (update.result.challengeGroupName ? update.result.challengeGroupName + ' ' + translator('Group') + ': ' : '') + update.result.result;
  };
  
  var appendStandardTitleAndDescription = function appendStandardTitleAndDescription(update, userId, translator) {
    update.title = (userId === update.userId) ? translator('You') : update.firstName + ' ' + update.lastName;
    update.title += ' ' + translator('Social_Update_Type_' + update.type);
    update.description = update.body;
  };
  
  return {
    getUpdates: _getUpdates,
    getUpdate: _getUpdate
  };
};