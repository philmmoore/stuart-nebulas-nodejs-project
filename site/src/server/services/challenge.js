var Promise = require('bluebird');

module.exports = function ChallengeService(repository, markdownTextFormatter) {
  var _buildData = function _buildData(challenge) {
    challenge.bodyCopy = markdownTextFormatter.format(challenge.bodyCopy);
    challenge.link = challenge.name.toLowerCase().replace(/ /g, '-');
    
    return {
      challenge: challenge,
      title: challenge.name
    };
  };

  var _getChallenge = function _getChallenge(location, params) {
    var challengeName = params.name;

    return repository.getChallenge(challengeName).then(function(challenge) {
      if (!challenge) {
        return Promise.reject('Could not find challenge');
      }

      return _buildData(challenge);
    });
  };
  
  var _getTranslation = function _getTranslation(translationFor, value) {
    return translationFor(value.replace(/ /g, '_'));
  };
  
  var _moveComingSoonChallengesToEnd = function _moveComingSoonChallengesToEnd(categories) {
    for (var categoryGroupIndex = 0; categoryGroupIndex < categories.length; categoryGroupIndex++) {
      var challenges = categories[categoryGroupIndex];
      
      var newList = [];
      
      for (var challengeIndex = 0; challengeIndex < challenges.length; challengeIndex++) {
        var challenge = challenges[challengeIndex];
        
        if (!challenge.comingSoon) {
          newList.push(challenge);
        }
      }
      
      for (var challengeIndexSecondPass = 0; challengeIndexSecondPass < challenges.length; challengeIndexSecondPass++) {
        var challengeSecondPass = challenges[challengeIndexSecondPass];
        
        if (challengeSecondPass.comingSoon) {
          newList.push(challengeSecondPass);
        }
      }
      
      categories[categoryGroupIndex] = newList;
    }
    
    return categories;
  };

  var _getChallenges = function _getChallenges(req, translationFor) {
    return repository.getChallenges().then(function(data) {
      var categoryNames = [];
      var categories = [];
      
      if (!data) {
        data = [];
      }

      for (var index = 0; index < data.length; index++) {
        var challenge = data[index];

        var categoryIndex = categoryNames.indexOf(challenge.category);

        if (categoryIndex === -1) {
          categoryNames.push(challenge.category);
          categoryIndex = categoryNames.length - 1;
          categories.push([]);
        }

        challenge.type = _getTranslation(translationFor, challenge.type);
        challenge.category = _getTranslation(translationFor, challenge.category);
        challenge.link = challenge.name.toLowerCase().replace(/ /g, '-') + '/';
        challenge.colour = _getChallengeColour(challenge.category);

        categories[categoryIndex].push(challenge);
      }

      return _moveComingSoonChallengesToEnd(categories);
    });
  };
  
  var _getChallengeColour = function _getChallengeColour(category) {
    if (category.toLowerCase() === 'strength') {
      return 'purple';
    }
    
    if (category.toLowerCase() === 'cardio') {
      return 'orange';
    }
    
    if (category.toLowerCase() === 'multi') {
      return 'green';
    }
    
    return 'black';
  };

  return {
    getChallenge: _getChallenge,
    getChallenges: _getChallenges
  };
};