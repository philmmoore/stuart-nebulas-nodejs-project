var Promise = require('bluebird');

module.exports = function RemoveVideoService(config, logger) {
  var AWS = require('aws-sdk');
  AWS.config.update(config.videoUploadConfig);
  var s3 = new AWS.S3();
  
  var _delete = function _delete(bucketName, file) {
    return new Promise(function(resolve, reject) {
      var params = {
        Bucket: config.awsResourcePrefix + bucketName,
        Key: file
      };

      s3.deleteObject(params, function(err, data) {
        if (err) {
          return reject(err);
        }

        return resolve();
      });
    });
  };
  
  var _deleteSocialUpdateVideo = function _deleteSocialUpdateVideo(updateId) {
    return Promise.all([
      _delete('misc-videos', updateId + '.png'),
      _delete('misc-videos-to-process', updateId)
    ]);
  };

  return {
    deleteSocialUpdateVideo: _deleteSocialUpdateVideo
  };
};