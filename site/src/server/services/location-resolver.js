module.exports = function LocationResolver(repository) {
  var _formatName = function _formatName(name) {
    return name.toLowerCase().trim().replace(/ /gi, '-');
  };
  
  var _findDistrict = function _findDistrict(locations, locationNameToFind) {
    for (var index = 0; index < locations.length; index++) {
      var location = locations[index];
      var district = _formatName(location.district);
      
      if (locationNameToFind === district) {
        return {
          districtId: location.districtId,
          districtName: location.district,
          countyId: location.countyId,
          countyName: location.county,
          regionId: location.regionId,
          regionName: location.region,
          countryId: location.countryId,
          countryCode: _formatName(location.countryCode)
        };
      }
    }
  };
  
  var _findCounty = function _findCounty(locations, locationNameToFind) {
    for (var index = 0; index < locations.length; index++) {
      var location = locations[index];
      var county = _formatName(location.county);
      
      if (locationNameToFind === county) {
        return {
          countyId: location.countyId,
          countyName: location.county,
          regionId: location.regionId,
          regionName: location.region,
          countryId: location.countryId,
          countryCode: _formatName(location.countryCode)
        };
      }
    }
  };
  
  var _findRegion = function _findRegion(locations, locationNameToFind) {
    for (var index = 0; index < locations.length; index++) {
      var location = locations[index];
      var region = _formatName(location.region);
      
      if (locationNameToFind === region) {
        return {
          regionId: location.regionId,
          regionName: location.region,
          countryId: location.countryId,
          countryCode: _formatName(location.countryCode)
        };
      }
    }
  };
  
  var _findCountry = function _findCountry(locations, locationNameToFind) {
    for (var index = 0; index < locations.length; index++) {
      var location = locations[index];
      
      if (locationNameToFind === _formatName(location.country)) {
        return {
          countryId: location.countryId,
          countryCode: _formatName(location.countryCode)
        };
      }
    }
  };
  
  var _getLocationFromName = function _getLocationFromName(locationName) {
    var locationNameToFind = _formatName(locationName);
    
    return repository.getAllLocations().then(function(locations) {
      var location = _findDistrict(locations, locationNameToFind);
      
      if (!location) {
        location = _findCounty(locations, locationNameToFind);
      }
      
      if (!location) {
        location = _findRegion(locations, locationNameToFind);
      }
      
      if (!location) {
        location = _findCountry(locations, locationNameToFind);
      }
      
      return location || null;
    });
  };
  
  return {
    getLocationFromName: _getLocationFromName
  };
};