var Promise = require('bluebird');
var moment = require('moment');
var stringFormatter = require('../utilities/string-formatter');

module.exports = function LeaderboardService(repository) {
  var NUMBER_OF_RESULTS_TO_SHOW_PER_PAGE = 20;
  var SEASON_START_DATE = '01-OCT-2015';
  
  /*
    required params:
      challengeName
      challengeGroupName
      username and/or location (location overrides user's location)
      
    optional filters:
      filterGender ('male' or 'female')
      filterAge
      filterWeight
      filterOnlyValidated
  */
  var _getUrl = function _getUrl(params) {
    return _populateChallengeData(params).then(function(data) {
      return _populateUserData(data);
    }).then(function(data) {
      return _populateFilterOptions(data);
    }).then(function(data) {
      return _populateChallengeGroupId(data);
    }).then(function(data) {
      return _populateAgeFilterRange(data);
    }).then(function(data) {
      return _populateWeightFilterRange(data);
  }).then(function(data) {
      return _populatePageNumber(data);
    }).then(function(data) {
      return _buildUrl(data);
    });
  };
  
  var _populateChallengeData = function _populateChallengeData(data) {
    var formattedChallengeName = stringFormatter.getUrlFriendlyString(data.challengeName);
    
    return repository.getChallenge(formattedChallengeName).then(function(challenge) {
      data.challenge = challenge;
      return data;
    });
  };
  
  var _populateUserData = function _populateUserData(data) {
    if (!data.username) {
      return data;
    }
    
    return repository.getUser(data.username).then(function(user) {
      data.user = user;
      
      if (!data.location) {
        
        // only as granular as country for launch
        data.location = {
          countryId: user.location.countryId,
          countryCode: user.location.countryCode
        };
      }
      
      return data;
    });
  };
  
  var _populateFilterOptions = function _populateFilterOptions(data) {
    var userId = data.user ? data.user.id : null;
    
    return repository.getFilterOptions(data.challenge.id, userId).then(function(filterOptions) {
      data.filterOptions = filterOptions;
      return data;
    });
  };
  
  var _populateChallengeGroupId = function _populateChallengeGroupId(data) {
    for (var index = 0; index < data.filterOptions.groups.length; index++)
    {
      var group = data.filterOptions.groups[index];
      
      if (stringFormatter.getUrlFriendlyString(group.name) === stringFormatter.getUrlFriendlyString(data.challengeGroupName)) {
        data.challengeGroupId = group.id;
        return data;
      }
    }
    
    return data;
  };
  
  var _populateAgeFilterRange = function _populateAgeFilterRange(data) {
    if (!data.filterAge) {
      return data;
    }
    
    return repository.getAgeRange(data.filterAge).then(function(range) {
      data.filterAgeMin = range.min;
      data.filterAgeMax = range.max;
      
      return data;
    });
  };

  var _populateWeightFilterRange = function _populateWeightFilterRange(data) {
    if (!data.filterWeight) {
      return data;
    }
    
    return repository.getWeightRange(data.filterWeight).then(function(range) {
      data.filterWeightMin = range.min;
      data.filterWeightMax = range.max;
      
      return data;
    });
  };
  
  var _populatePageNumber = function _populatePageNumber(data) {
    if (!data.username) {
      return data;
    }
    
    var filter = {
      challengeId: data.challenge.id,
      challengeGroupId: data.challengeGroupId,
      districtId: data.location.districtId,
      countyId: data.location.countyId,
      regionId: data.location.regionId,
      countryId: data.location.countryId,
      currentPeriodStart: SEASON_START_DATE,
      currentPeriodEnd: moment(),
      filterGender: data.filterGender,
      filterAgeMin: data.filterAgeMin,
      filterAgeMax: data.filterAgeMax,
      filterWeightMin: data.filterWeightMin,
      filterWeightMax: data.filterWeightMax,
      filterOnlyValidated: data.filterOnlyValidated
    };
    
    return repository.getLeaderboardRankings(filter).then(function(leaderboardRankings) {
      if (!leaderboardRankings) {
        return data;
      }
      
      var userPosition = leaderboardRankings.indexOf(data.user.id) + 1;
      var pageNumber = Math.ceil(userPosition / NUMBER_OF_RESULTS_TO_SHOW_PER_PAGE);
      
      if (pageNumber > 1) {
        data.pageNumber = pageNumber;
      }
      
      return data;
    });
  };
  
  var _buildUrl = function _buildUrl(data) {
    var url = [ data.location.countryCode ];

    url.push('leaderboards');

    if (data.location.regionName) {
      url.push(data.location.regionName);
    }
    
    if (data.location.countyName) {
      url.push(data.location.countyName);
    }
    
    if (data.location.districtName) {
      url.push(data.location.districtName);
    }
    
    url.push(data.challengeName);
    url.push(data.challengeGroupName);
    url.push('leaderboard');
    
    if (data.filterGender) {
      url.push(data.filterGender);
    }

    if (data.filterAge) {
      url.push(data.filterAge + 'yrs');
    }

    if (data.filterWeight) {
      url.push(data.filterWeight);
    }

    if (data.filterOnlyValidated) {
      url.push('validated');
    }
    
    if (data.pageNumber) {
      url.push(data.pageNumber);
    }
    
    return '/' + stringFormatter.getUrlFriendlyString(url.join('/')) + '/';
  };
  
  return {
    getUrl: _getUrl
  };
};