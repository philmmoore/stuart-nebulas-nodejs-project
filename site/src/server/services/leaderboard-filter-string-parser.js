module.exports = function LeaderboardUrlParserService() {
  var _getCurrentPageNumber = function _getCurrentPageNumber(segments) {
    var lastSegment = segments[segments.length - 1];

    if (/^\d+$/.test(lastSegment)) {
      return parseInt(lastSegment);
    }

    return 1;
  };

  var _getIsMaleFilter = function _getIsMaleFilter(segments) {
    for(var index = 0; index < segments.length; index++) {
      if (segments[index] === 'male') {
        return true;
      }

      if (segments[index] === 'female') {
        return false;
      }
    }

    return null;
  };

  var _getAgeFilter = function _getAgeFilter(segments) {
    for(var index = 0; index < segments.length; index++) {
      matches = segments[index].match(/^([\d-+]+)yrs$/);

      if (matches) {
        return matches[1];
      }
    }

    return null;
  };

  var _getWeightFilter = function _getWeightFilter(segments) {
    for(var index = 0; index < segments.length; index++) {
      var segment = segments[index];

      if (/\d+kg/.test(segment)) {
        return segment;
      }
    }

    return null;
  };

  var _getOnlyShowValidatedFilter = function _getOnlyShowValidatedFilter(segments) {
    for(var index = 0; index < segments.length; index++) {
      var segment = segments[index];

      if (segment === 'validated') {
        return true;
      }
    }

    return false;
  };

  var _parse = function _parse(url) {
    var segments = url.replace(/\/$/g,'')
                      .toLowerCase()
                      .split('/');

    return {
      page: _getCurrentPageNumber(segments),
      isMale: _getIsMaleFilter(segments),
      age: _getAgeFilter(segments),
      weight: _getWeightFilter(segments),
      onlyShowValidated: _getOnlyShowValidatedFilter(segments)
    };
  };

  return {
    parse: _parse
  };
};
