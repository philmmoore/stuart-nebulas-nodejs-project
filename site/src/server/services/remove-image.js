var Promise = require('bluebird');

module.exports = function RemoveImageService(config, logger) {
  var AWS = require('aws-sdk');
  AWS.config.update(config.imageUploadConfig);
  var s3 = new AWS.S3();
  
  var _delete = function _delete(bucketName, file) {
    return new Promise(function(resolve, reject) {
      var params = {
        Bucket: config.awsResourcePrefix + bucketName,
        Key: file
      };

      s3.deleteObject(params, function(err, data) {
        if (err) {
          return reject(err);
        }

        return resolve();
      });
    });
  };
  
  var _deleteSocialUpdateImage = function _deleteSocialUpdateImage(updateId) {
    return Promise.all([
      _delete('social-photos', updateId + '.png'),
      _delete('social-photos-to-process', updateId)
    ]);
  };

  return {
    deleteSocialUpdateImage: _deleteSocialUpdateImage
  };
};