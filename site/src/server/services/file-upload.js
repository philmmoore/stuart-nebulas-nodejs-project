var Promise = require('bluebird');

module.exports = function FileUploadService(config, resourcePrefix, logger) {
  var AWS = require('aws-sdk');
  AWS.config.update(config);
  var s3 = new AWS.S3();

  var _signUploadRequest = function _signUploadRequest(bucketName, file) {
    return new Promise(function(resolve, reject) {
      var params = {
        Bucket: resourcePrefix + bucketName,
        Key: file.name,
        ContentType: file.type,
        Expires: 60,
        ACL: 'public-read'
      };

      s3.getSignedUrl('putObject', params, function(err, data) {
        if (err) {
          return reject(err);
        }

        return resolve(data);
      });
    });
  };

  return {
    signUploadRequest: _signUploadRequest
  };
};
