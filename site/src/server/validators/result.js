var util = require('./util');
var moment = require('moment');

module.exports = function ResultValidator() {
  var _validateResultForCount = function _validateResultForCount(result, validationErrors) {
    if (isNaN(result) || result < 1 || result > 500) {
      validationErrors.push(['Result', 'Error_Result_Invalid_Count']);
    }
  };
  
  var _validateResultForTime = function _validateResultForTime(form, validationErrors) {
    var parsedTime = moment(form.result, 'H:mm:ss', true);
    
    if (!parsedTime.isValid()) {
      return validationErrors.push(['Result', 'Error_Result_Invalid_Time']);
    }
    
    var milliseconds = parsedTime.diff(moment().startOf('day'), 'milliseconds');
    
    if (milliseconds < 1 || milliseconds > 2147483647) {
      return validationErrors.push(['Result', 'Error_Result_Invalid_Time_Range']);
    }
    
    form.result = milliseconds;
  };  
  
  var _validateResultForDistance = function _validateResultForDistance(result, validationErrors) {
    if (isNaN(result) || result < 1 || result > 2147483) {
      validationErrors.push(['Result', 'Error_Result_Invalid_Distance']);
    }
  };
  
  var _validate = function _validate(form, challengeHasMultipleGroups, measurement) {
    var validationErrors = [];

    if (challengeHasMultipleGroups) {
      if (util.isBlankOrWhitespace(form.group)) {
        validationErrors.push(['Group', 'Error_Field_Blank']);
      }
    }

    if (util.isBlankOrWhitespace(form.result)) {
      validationErrors.push(['Result', 'Error_Field_Blank']);
    } else {
      switch (measurement) {
        case 'count':
          _validateResultForCount(form.result, validationErrors);
          break;
          
        case 'time':
          _validateResultForTime(form, validationErrors);
          break;
          
        case 'distance':
          _validateResultForDistance(form.result, validationErrors);
          break;
      }
    }

    return util.validationPromise(form, validationErrors);
  };

  return {
    validate: _validate
  };
};
