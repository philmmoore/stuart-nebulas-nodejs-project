var util = require('./util');

module.exports = function UserValidator() {
  var _validateHeight = function _validateHeight(form, validationErrors) {
    if (util.isBlankOrWhitespace(form.heightCm) && util.isBlankOrWhitespace(form.heightFeet) && util.isBlankOrWhitespace(form.heightInches)) {
      return validationErrors.push(['Height', 'Error_Field_Blank']);
    }

    if (util.isBlankOrWhitespace(form.heightCm)) {
      form.heightCm = 0;
    }

    if (util.isBlankOrWhitespace(form.heightFeet)) {
      form.heightFeet = 0;
    }

    if (util.isBlankOrWhitespace(form.heightInches)) {
      form.heightInches = 0;
    }

    var value = null;

    if (!util.isBlankOrWhitespace(form.heightCm)) {
      value = parseFloat(form.heightCm);

      if (isNaN(value)) {
        return validationErrors.push(['Height', 'Error_Field_Not_Decimal']);
      }

      var inches = value / (12 * 2.54);
      form.heightFeet = Math.floor(inches);
      form.heightInches = ((inches - form.heightFeet) * 12).toFixed(2);
    } else {
      if (isNaN(form.heightFeet) || isNaN(form.heightInches)) {
        return validationErrors.push(['Height', 'Error_Field_Not_Decimal']);
      }

      value = parseInt(form.heightFeet) * 2.54 * 12;
      value += parseFloat(form.heightInches) * 2.54;

      form.heightCm = value.toFixed(2);
    }

    if (value < 50) {
      return validationErrors.push(['Height', 'Error_Field_Height_Less_Than_50_cm']);
    }

    if (value > 300) {
      return validationErrors.push(['Height', 'Error_Field_Height_Greater_Than_300_cm']);
    }
  };

  var _validateWeight = function _validateWeight(form, validationErrors) {
    if (util.isBlankOrWhitespace(form.weightKg) && util.isBlankOrWhitespace(form.weightLbs) && util.isBlankOrWhitespace(form.weightOz)) {
      return validationErrors.push(['Weight', 'Error_Field_Blank']);
    }

    if (util.isBlankOrWhitespace(form.weightKg)) {
      form.weightKg = 0;
    }

    if (util.isBlankOrWhitespace(form.weightLbs)) {
      form.weightLbs = 0;
    }

    if (util.isBlankOrWhitespace(form.weightOz)) {
      form.weightOz = 0;
    }

    var value = null;

    if (!util.isBlankOrWhitespace(form.weightKg)) {
      value = parseFloat(form.weightKg);

      if (isNaN(value)) {
        return validationErrors.push(['Weight', 'Error_Field_Not_Decimal']);
      }

      var pounds = value * 2.20462;
      form.weightLbs = Math.floor(pounds);
      form.weightOz = ((pounds - form.weightLbs) * 16).toFixed(2);
    } else {
      if (isNaN(form.weightLbs) || isNaN(form.weightOz)) {
        return validationErrors.push(['Weight', 'Error_Field_Not_Decimal']);
      }

      value = parseInt(form.weightLbs) * 0.453592;
      value += parseFloat(form.weightOz) * 0.0283495;
      form.weightKg = value.toFixed(2);
    }

    if (isNaN(value)) {
      return validationErrors.push(['Weight', 'Error_Field_Not_Decimal']);
    }

    if (value < 5) {
      return validationErrors.push(['Weight', 'Error_Field_Weight_Less_Than_5_kg']);
    }

    if (value > 700) {
      return validationErrors.push(['Weight', 'Error_Field_Weight_Greater_Than_700_kg']);
    }
  };

  var _validateUpdateHeight = function _validateUpdateHeight(form) {
    var validationErrors = [];

    _validateHeight(form, validationErrors);

    return util.validationPromise(form, validationErrors);
  };
  
  var _validateUpdateWeight = function _validateUpdateWeight(form) {
    var validationErrors = [];

    _validateWeight(form, validationErrors);

    return util.validationPromise(form, validationErrors);
  };
  
  var _validateLocation = function _validateLocation(form) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(form.district)) {
      validationErrors.push(['District', 'Error_Field_Blank']);
    }

    return util.validationPromise(form, validationErrors);
  };
  
  var _validateUpdateRoutine = function _validateUpdateRoutine(form) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(form.routineId)) {
      validationErrors.push(['Routine', 'Error_Field_Not_Selected']);
    }

    return util.validationPromise(form, validationErrors);
  };
  
  var _validateUpdateGym = function _validateUpdateGym(form) {
    var validationErrors = [];

    if (form.gym && form.gym.length > 256) {
      validationErrors.push(['Gym', 'Error_Field_Too_Long']);
    }

    return util.validationPromise(form, validationErrors);
  };

  var _validateReviewBody = function _validateReviewBody(form) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(form.title)) {
      validationErrors.push(['Title', 'Error_Field_Blank']);
    }

    if (util.isBlankOrWhitespace(form.review)) {
      validationErrors.push(['Review', 'Error_Field_Blank']);
    }

    if (form.rating > 5 || form.rating < 1){
      validationErrors.push(['Rating', 'Error_Field_Value_Not_Between_1_And_5']); 
    }

    return util.validationPromise(form, validationErrors);
  };

  return {
    validateUpdateHeight: _validateUpdateHeight,
    validateUpdateWeight: _validateUpdateWeight,
    validateLocation: _validateLocation,
    validateUpdateRoutine: _validateUpdateRoutine,
    validateUpdateGym: _validateUpdateGym,
    validateReviewBody: _validateReviewBody
  };
};
