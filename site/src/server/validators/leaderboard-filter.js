var util = require('./util');

module.exports = function LeaderboardFilterValidator() {
  var _validate = function _validate(form) {
    var validationErrors = [];

    if (!form.group) {
      validationErrors.push(['Group', 'Error_Field_Not_Selected']);
    }

    return util.validationPromise(form, validationErrors);
  };

  return {
    validate: _validate
  };
};
