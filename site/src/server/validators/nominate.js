var util = require('./util');

module.exports = function NominateValidator() {
  var _validate = function _validate(form) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(form.group)) {
      validationErrors.push(['Group', 'Error_Field_Not_Selected']);
    }

    if (util.isBlankOrWhitespace(form.selectedLocation)) {
      validationErrors.push(['Location', 'Error_Field_Blank']);
    }

    return util.validationPromise(form, validationErrors);
  };

  return {
    validate: _validate
  };
};
