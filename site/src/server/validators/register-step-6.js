var util = require('./util');

module.exports = function RegisterStep6Validator() {
  var _validate = function _validate(form) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(form.mobileNumber)) {
      validationErrors.push(['Mobile_Number', 'Error_Field_Blank']);
    } else if (form.mobileNumber.substring(0, 2) !== '07') {
      validationErrors.push(['Mobile_Number', 'Error_Field_Not_Phone_Number']);
    } else if (form.mobileNumber.length !== 11) {
      validationErrors.push(['Mobile_Number', 'Error_Field_Not_Phone_Number']);
    } else if (!parseInt(form.mobileNumber)) {
      validationErrors.push(['Mobile_Number', 'Error_Field_Not_Phone_Number']);
    }

    if (util.isBlankOrWhitespace(form.routine)) {
      validationErrors.push(['Routine', 'Error_Field_Not_Selected']);
    }
    
    if (form.gym && form.gym.length > 256) {
      validationErrors.push(['Gym', 'Error_Field_Too_Long']);
    }

    return util.validationPromise(form, validationErrors);
  };

  return {
    validate: _validate
  };
};
