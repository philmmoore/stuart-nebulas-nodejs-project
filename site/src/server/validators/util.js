var Promise = require('bluebird');
var emailValidator = require('email-validator');
var isDisposableEmail = require('is-disposable-email');

module.exports.isBlankOrWhitespace = function isBlankOrWhitespace(string) {
  if (!string) {
    return true;
  }

  return string.length < 1 || /^\s+$/.test(string);
};

module.exports.isEmailValid = function isEmailValid(email) {
  return emailValidator.validate(email);
};

module.exports.isEmailDisposable = function isEmailDisposable(email) {
  return isDisposableEmail(email);
};

module.exports.validationPromise = function validationPromise(form, validationErrors) {
  return new Promise(function(resolve, reject) {
    if (validationErrors.length > 0) {
      form.validationErrors = validationErrors;
      return reject(form);
    }

    resolve();
  });
};
