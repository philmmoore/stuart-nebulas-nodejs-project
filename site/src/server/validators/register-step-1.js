var util = require('./util');

module.exports = function RegisterStep1Validator() {
  var _validateEmail = function _validateEmail(email, validationErrors) {
    if (util.isBlankOrWhitespace(email)) {
      return validationErrors.push(['Email_Address', 'Error_Field_Blank']);
    }

    if (!util.isEmailValid(email)) {
      return validationErrors.push(['Email_Address', 'Error_Field_Invalid_Email']);
    }

    if (util.isEmailDisposable(email)) {
      return validationErrors.push(['Email_Address', 'Error_Field_Email_Is_Disposable']);
    }
    
    if (email.length > 256) {
      return validationErrors.push(['Email_Address', 'Error_Field_Too_Long']);
    }
  };

  var _validatePassword = function _validatePassword(password, email, validationErrors) {
    if (util.isBlankOrWhitespace(password)) {
      return validationErrors.push(['Password', 'Error_Field_Blank']);
    }

    if (password.length < 5) {
      return validationErrors.push(['Password', 'Error_Field_Must_Be_At_Least_5_Characters']);
    }
    
    if (password.length > 256) {
      return validationErrors.push(['Password', 'Error_Field_Too_Long']);
    }
    
    if (!password.match(/\d/)) {
      return validationErrors.push(['Password', 'Error_Password_Must_Contain_Numeric']);
    }
    
    if (!util.isBlankOrWhitespace(email) && email.trim().toLowerCase() === password.trim().toLowerCase()) {
      return validationErrors.push(['Password', 'Error_Password_Cannot_Be_Same_As_Email_Address']);
    }
  };

  var _validate = function _validate(form) {
    var validationErrors = [];

    _validateEmail(form.email, validationErrors);
    _validatePassword(form.password, form.email, validationErrors);

    return util.validationPromise(form, validationErrors);
  };

  return {
    validate: _validate
  };
};
