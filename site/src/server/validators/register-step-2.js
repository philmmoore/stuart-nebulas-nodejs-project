var util = require('./util');
var moment = require('moment');

module.exports = function RegisterStep2Validator() {
  var _isValidUsernameFormat = function _isValidUsernameFormat(string) {
    return /^[a-zA-Z0-9-_]+$/.test(string);
  };

  var _validateUsername = function _validateUsername(username, validationErrors) {
    if (util.isBlankOrWhitespace(username)) {
      return validationErrors.push(['Username', 'Error_Field_Blank']);
    }

    if (!_isValidUsernameFormat(username)) {
      return validationErrors.push(['Username', 'Error_Field_Invalid_Username']);
    }

    if (username.length < 3) {
      return validationErrors.push(['Username', 'Error_Field_Must_Be_At_Least_3_Characters']);
    }
    
    if (username.length > 35) {
      return validationErrors.push(['Username', 'Error_Username_Too_Long']);
    }
  };

  var _validateYearOfBirth = function _validateYearOfBirth(yearOfBirth, validationErrors) {
    if (util.isBlankOrWhitespace(yearOfBirth)) {
      return validationErrors.push(['Year_Of_Birth', 'Error_Field_Blank']);
    }

    if (isNaN(yearOfBirth) || yearOfBirth.length > 4) {
      return validationErrors.push(['Year_Of_Birth', 'Error_Field_Invalid_Year']);
    }

    var thisYear = moment().year();

    if (yearOfBirth > thisYear) {
      return validationErrors.push(['Year_Of_Birth', 'Error_Field_Date_cannot_Be_In_Future']);
    }

    if (thisYear - yearOfBirth > 130) {
      return validationErrors.push(['Year_Of_Birth', 'Error_Field_Age_Cannot_Be_Greater_Than_130']);
    }

    if (thisYear - yearOfBirth < 16) {
      return validationErrors.push(['Year_Of_Birth', 'Error_Field_Age_Too_Young']);
    }
  };

  var _validate = function _validate(form) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(form.firstName)) {
      validationErrors.push(['First_Name', 'Error_Field_Blank']);
    }
    
    if (form.firstName && form.firstName.length > 256) {
      validationErrors.push(['First_Name', 'Error_Field_Too_Long']);
    }

    if (util.isBlankOrWhitespace(form.lastName)) {
      validationErrors.push(['Last_Name', 'Error_Field_Blank']);
    }
    
    if (form.lastName && form.lastName.length > 256) {
      validationErrors.push(['Last_Name', 'Error_Field_Too_Long']);
    }

    _validateUsername(form.username, validationErrors);

    if (util.isBlankOrWhitespace(form.regionId)) {
      validationErrors.push(['Region', 'Error_Field_Not_Selected']);
    }

    _validateYearOfBirth(form.yearOfBirth, validationErrors);

    if (util.isBlankOrWhitespace(form.monthOfBirth)) {
      validationErrors.push(['Month_Of_Birth', 'Error_Field_Not_Selected']);
    }

    return util.validationPromise(form, validationErrors);
  };

  return {
    validate: _validate
  };
};
