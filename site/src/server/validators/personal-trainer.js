var util = require('./util');

module.exports = function PersonalTrainerValidator() {
  var _validate = function _validate(form) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(form.userQuery) && util.isBlankOrWhitespace(form.locationQuery)) {
      validationErrors.push(['Query', 'Error_Search_Query_Empty']);
    }

    return util.validationPromise(form, validationErrors);
  };

  return {
    validate: _validate
  };
};