var util = require('./util');

module.exports = function LoginValidator() {
  var _validate = function _validate(form) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(form.emailOrUsername)) {
      validationErrors.push(['Email_Or_Username', 'Error_Field_Blank']);
    }

    if (util.isBlankOrWhitespace(form.password)) {
      validationErrors.push(['Password', 'Error_Field_Blank']);
    }

    return util.validationPromise(form, validationErrors);
  };

  return {
    validate: _validate
  };
};
