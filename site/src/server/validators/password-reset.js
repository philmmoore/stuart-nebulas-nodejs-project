var util = require('./util');

module.exports = function PasswordResetValidator() {
  var _validateEmail = function _validateEmail(email, validationErrors) {
    if (util.isBlankOrWhitespace(email)) {
      return validationErrors.push(['Email_Address', 'Error_Field_Blank']);
    }

    if (!util.isEmailValid(email)) {
      return validationErrors.push(['Email_Address', 'Error_Field_Invalid_Email']);
    }
  };

  var _validate = function _validate(form) {
    var validationErrors = [];
    _validateEmail(form.email, validationErrors);

    return util.validationPromise(form, validationErrors);
  };

  return {
    validate: _validate
  };
};
