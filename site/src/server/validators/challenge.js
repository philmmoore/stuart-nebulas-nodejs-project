var util = require('./util');

module.exports = function ChallengeValidator() {
  var _validate = function _validate(form) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(form.group)) {
      validationErrors.push(['Group', 'Error_Field_Not_Selected']);
    }

    return util.validationPromise(form, validationErrors);
  };

  return {
    validate: _validate
  };
};
