module.exports = function security(targetHost, allowRequestsFromAnyDomain) {
  return function handle(req, res, next) {
    if (!req.secure || (!allowRequestsFromAnyDomain && req.headers.host !== targetHost)) {
      res.redirect('https://' + targetHost);
      return;
    }

    next();
  };
};
