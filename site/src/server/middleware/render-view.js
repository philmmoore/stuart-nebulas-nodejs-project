module.exports = function renderView(logger) {
  return function handle(req, res, next) {
    res.renderView = function renderView(viewName, data) {
      if (data === 404) {
        return res.renderView('pages/not-found');
      }

      if (!data) {
        data = {};
      }
      
      if (data.validationErrors) {
        logger.log('validation error in view \'' + viewName + '\':', JSON.stringify(data.validationErrors));
      }

      data.viewName = viewName;
      res.render(viewName, data);
    };

    next();
  };
};
