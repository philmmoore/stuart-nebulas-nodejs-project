module.exports = function ConfigMiddleware(config) {
  return function handle(req, res, next) {
    res.locals.googleAnalyticsTrackingTag = config.googleAnalyticsTrackingTag;
    res.locals.isProduction = config.isProduction;
    res.locals.isLeaderboardRealtime = config.isLeaderboardRealtime;

    next();
  };
};
