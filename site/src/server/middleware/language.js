module.exports = function language(defaultLanguage, translations, logger) {
  return function handle(req, res, next) {
    var clientLanguage = req.headers['accept-language'];
    var strings = translations[clientLanguage];

    if (!strings) {
      clientLanguage = defaultLanguage;
      strings = translations[clientLanguage];
    }

    res.locals.language = clientLanguage;

    res.locals.string = function(string) {
      return strings[string] || stringNotFound(string, clientLanguage);
    };

    function stringNotFound(string, language) {
      logger.log('Could not find %s translation for %s.', language, string);
      return '%%' + string + '%%';
    }

    next();
  };
};
