module.exports = function redisCache(redisDB) {
  return function handle(req, res, next) {
    redisDB.get('cache:' + req.url, function(err, reply) {
      if (err || reply === null) {
        return next();
      }

      var page = JSON.parse(reply);
      res.render(page.view, page.data);
    });
  };
};
