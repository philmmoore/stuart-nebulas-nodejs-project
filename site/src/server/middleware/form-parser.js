var Busboy = require('busboy');

module.exports = function formParser() {
  return function handle(req, res, next) {
    if (req.method !== 'POST') {
      return next();
    }

    req.body = {};

    var busboy = new Busboy({
      headers: req.headers
    });

    busboy.on('field', function onField(fieldname, val, fieldnameTruncated, valTruncated) {
      req.body[fieldname] = val;
    });

    busboy.on('finish', function onFinish() {
      next();
    });

    req.pipe(busboy);
  };
};
