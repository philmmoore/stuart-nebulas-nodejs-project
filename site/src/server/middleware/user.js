module.exports = function user(avatarImageUrl) {
  return function handle(req, res, next) {
    var user = req.session.user;

    if (user) {
      if (!user.avatar) {
        user.avatar = avatarImageUrl + (user.avatarUploaded === '1' ? user.id : '0') + '-50x50.png';
        user.avatarRetina = avatarImageUrl + (user.avatarUploaded === '1' ? user.id : '0') + '-100x100.png';
      }
    }

    res.locals.loggedInUser = user;
    next();
  };
};
