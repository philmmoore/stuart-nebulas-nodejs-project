var Promise = require('bluebird');
var moment = require('moment');

module.exports = function BlogController(repository, markdownTextFormatter, disqusShortname) {
  var _numberOfPostsPerPage = 5;

  var _getPosts = function _getPosts(location, pageNumber) {
    return repository.getPosts(location.countryId, _numberOfPostsPerPage, pageNumber).then(function(data) {
      if (!data.posts) {
        return Promise.reject();
      }

      data.posts.map(function(post) {
        post.published = moment(post.published);
        post.link = '/' + location.countryCode + '/blog/' + post.published.format('YYYY/MM/') + post.name.toLowerCase().replace(/ /g, '-') + '/';
        
        return post;
      });

      data.page = parseInt(pageNumber || 1);
      return data;
    });
  };

  var _getPost = function _getPost(countryId, name) {
    return repository.getPost(countryId, name).then(function(post) {
      if (!post) {
        return Promise.reject();
      }

      post.bodyCopy = markdownTextFormatter.format(post.bodyCopy);
      post.published = moment(post.published);

      return {
        post: post,
        title: post.name,
        disqusShortname: disqusShortname
      };
    });
  };

  return {
    getPosts: _getPosts,
    getPost: _getPost
  };
};
