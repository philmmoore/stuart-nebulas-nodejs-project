module.exports = function SocialController(socialService) {
  var _getUpdate = function _getUpdate(req, translator) {
    return socialService.getUpdate(req, translator);
  };

  return {
    getUpdate: _getUpdate
  };
};