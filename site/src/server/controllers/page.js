var Promise = require('bluebird');

module.exports = function PageController(repository, markdownTextFormatter) {
  var _getPage = function _getPage(countryId, name) {
    return repository.getPage(countryId, name).then(function(page) {
      if (!page) {
        return Promise.reject();
      }

      return page;
    }).then(function(page) {
      page.bodyCopy = markdownTextFormatter.format(page.bodyCopy);

      return {
        page: page,
        title: page.name
      };
    });
  };

  return {
    getPage: _getPage
  };
};
