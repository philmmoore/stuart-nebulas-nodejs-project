var Promise = require('bluebird');
var moment = require('moment');

module.exports = function RegisterController(repository, passwordHashService, step1Validator, step2Validator, step5Validator, step6Validator, messageQueueService) {
  var COUNTRY_ID = 1;

  var _submitPreRegistration = function _submitPreRegistration(countryId, req) {
    return _submitStep1Details(countryId, req).then(function(){

      // Gets the name then stores secondary details
      var name = req.body.Name.split(' ');
      var firstName = name[0].trim();
      var lastName = (name[1] ? name[1].trim() : '');
      var username = firstName + '-' + lastName + '-' + Math.floor((Math.random() * 999) + 1);

      return repository.addStep2Details(
        req.session.registrationId, 
        firstName,
        lastName,
        username.toLowerCase(),
        '1990-05-16',
        1
      );

    }).then(function(){
      // We've set he basic infor for the user now so lets add the user account
      return repository.addUser(req.session.registrationId);
    }).then(function(user){
      // The user has been created so lets set if they're a gym or not
      if (req.body.Type == 'gym'){
        return repository.userSetIsGym(user.id, true);
      }

      if (req.body.Type == 'pt'){
        return repository.userSetIsPt(user.id, true);
      }
      // If Type is not matched then just resolve the promise
      return Promise.resolve();
    }).catch(function(error){
      return Promise.reject(error);
    });
  };

  var _getStep1DetailsFromSubmittedForm = function _getStep1DetailsFromSubmittedForm(req) {
    return {
      email: req.body.Email,
      password: req.body.Password
    };
  };

  var _getStep2DetailsFromSubmittedForm = function _getStep2DetailsFromSubmittedForm(req) {
    return {
      firstName: req.body.FirstName,
      lastName: req.body.LastName,
      username: req.body.Username,
      regionId: req.body.Region,
      yearOfBirth: req.body.YearOfBirth,
      monthOfBirth: req.body.MonthOfBirth
    };
  };

  var _getStep5DetailsFromSubmittedForm = function _getStep5DetailsFromSubmittedForm(req) {
    return {
      district: req.body.District,
      gender: req.body.Gender,
      heightCm: req.body.HeightCm,
      heightFeet: req.body.HeightFeet,
      heightInches: req.body.HeightInches,
      weightKg: req.body.WeightKg,
      weightLbs: req.body.WeightLbs,
      weightOz: req.body.WeightOz,
      skip: req.body.SkipButton ? true : false
    };
  };

  var _getStep6DetailsFromSubmittedForm = function _getStep6DetailsFromSubmittedForm(req) {
    return {
      mobileNumber: req.body.MobileNumber,
      routine: req.body.Routine,
      gym: req.body.Gym,
      isPersonalTrainer: req.body.IsPersonalTrainer ? true : false,
      skip: req.body.SkipButton ? true : false
    };
  };

  var _submitStep1Details = function _submitStep1Details(countryId, req) {
    var form = _getStep1DetailsFromSubmittedForm(req);
    return step1Validator.validate(form).then(function() {
      return repository.getIsEmailRegistered(form.email);
    }).then(function(isEmailRegistered) {
      if (isEmailRegistered) {
        form.validationErrors = [ [ 'Email_Address', 'Error_Email_Already_Registered' ] ];
        return Promise.reject(form);
      }
      
      return repository.getIsEmailBanned(form.email);
    }).then(function(isEmailBanned) {
      if (isEmailBanned) {
        form.validationErrors = [ [ 'Email_Address', 'Error_Email_Banned' ] ];
        return Promise.reject(form);
      }
      
      return Promise.resolve();
    }).then(function() {
      return passwordHashService.getHash(form.password);
    }).then(function(hash) {
      return repository.addStep1Details(countryId, form.email.toLowerCase(), hash);
    }).then(function(registrationId) {
      req.session.registrationId = registrationId;
      req.session.user = null;
    });
  };

  var _getStep2Details = function _getStep2Details() {
    return repository.getRegions(COUNTRY_ID).then(function(regions) {
      return {
        regions: regions
      };
    });
  };

  var _submitStep2Details = function _submitStep2Details(req) {
    var form = _getStep2DetailsFromSubmittedForm(req);

    return step2Validator.validate(form).then(function() {
      return repository.getIsUsernameRegistered(form.username);
    }).then(function(isUsernameRegistered) {
      if (isUsernameRegistered) {
        form.validationErrors = [ [ 'Username', 'Error_Username_Already_Registered' ] ];
        return Promise.reject(form);
      }
    }).error(function(data) {
      return repository.getRegions(COUNTRY_ID).then(function(regions) {
        data.regions = regions;
        return Promise.reject(data);
      });
    }).then(function() {
      var registrationId = req.session.registrationId;
      var dateOfBirth = moment(form.yearOfBirth, 'YYYY');
      dateOfBirth.month(parseInt(form.monthOfBirth));

      return repository.addStep2Details(registrationId, form.firstName.trim(), form.lastName.trim(), form.username.toLowerCase(), dateOfBirth, form.regionId);
    });
  };

  var _setUserPrivacyPreferences = function _setUserPrivacyPreferences(req, userId) {
    var privacyPreferences = {
      userId: userId,
      gymFitEmailOptIn: req.body.GymFitEmailOptOut ? false : true,
      partnersEmailOptIn: req.body.PartnersEmailOptOut ? false : true,
      gymFitSMSOptIn: req.body.GymFitSMSOptOut ? false : true,
      partnersSMSOptIn: req.body.PartnersSMSOptOut ? false : true,
      gymFitOnlineOptIn: req.body.GymFitOnlineOptOut ? false : true,
      partnersOnlineOptIn: req.body.PartnersOnlineOptOut ? false : true
    };

    return repository.setPrivacyPreferences(privacyPreferences);
  };

  var _submitStep3Details = function _submitStep3Details(req) {
    var registrationId = req.session.registrationId;

    if (req.session.user) {
      var userId = req.session.user.id;
      return _setUserPrivacyPreferences(req, userId).then(function() {
        return repository.getRegisteredUserDetails(userId);
      });
    }

    return repository.addUser(registrationId).then(function(result) {
      return _setUserPrivacyPreferences(req, result.id).then(function() {
        return repository.getRegisteredUserDetails(result.id);
      }).then(function(data) {
        req.session.user = data;
        req.session.flashWelcomeMessage = true;

        var message = JSON.stringify({
          email: result.email,
          key: result.verificationKey
        });

        return messageQueueService.sendMessage('confirm-email', message);
      });
    });
  };

  var _getAllUserDistricts = function _getAllUserDistricts(req) {
    var userId = req.session.user.id;
    return repository.getDistrictsForUserRegion(userId);
  };

  var _isDistrictValid = function _isDistrictValid(userDistrict, districts) {
    for (var index = 0; index < districts.length; index++) {
      if (userDistrict === districts[index].toLowerCase().trim()) {
        return true;
      }
    }

    return false;
  };

  var _submitStep5Details = function _submitStep5Details(req) {
    var form = _getStep5DetailsFromSubmittedForm(req);
    delete req.session.flashWelcomeMessage;

    if (form.skip) {
      return Promise.resolve();
    }

    return step5Validator.validate(form).then(function() {
      var userId = req.session.user.id;
      return repository.getDistrictsForUserRegion(userId);
    }).then(function(districts) {
      var userDistrict = form.district.toLowerCase().trim();

      if (!_isDistrictValid(userDistrict, districts)) {
        form.validationErrors = [ [ 'District', 'Error_Field_Invalid_District' ] ];
        return Promise.reject(form);
      }

      var isMale = form.gender === 'male' ? '1' : '0';
      var height = Math.floor(parseFloat(form.heightCm) * 100);
      var weight = Math.floor(parseFloat(form.weightKg) * 1000);

      return repository.setUserParameters(req.session.user.id, isMale, height, weight, userDistrict);
    });
  };

  var _getRoutineTypes = function _getRoutineTypes() {
    return repository.getRoutineTypes();
  };

  var _submitStep6Details = function _submitStep6Details(countryId, req) {
    var form = _getStep6DetailsFromSubmittedForm(req);
    var userId = req.session.req.session.user.id;

    if (form.skip) {
      return Promise.resolve(req.session.user.username);
    }

    return step6Validator.validate(form).error(function(form) {
      return repository.getRoutineTypes().then(function(routineTypes) {
        form.routineTypes = routineTypes;
        return Promise.reject(form);
      });
    }).then(function() {
      return repository.persistStep6Details(userId, form.mobileNumber, form.routine, form.gym.trim(), form.isPersonalTrainer);
    }).then(function() {
      return req.session.user.username;
    });
  };

  return {
    submitPreRegistration: _submitPreRegistration,
    submitStep1Details: _submitStep1Details,
    submitStep2Details: _submitStep2Details,
    submitStep3Details: _submitStep3Details,
    getAllUserDistricts: _getAllUserDistricts,
    submitStep5Details: _submitStep5Details,
    getRoutineTypes: _getRoutineTypes,
    submitStep6Details: _submitStep6Details,
    getStep2Details: _getStep2Details
  };
};
