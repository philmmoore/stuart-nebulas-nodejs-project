var Promise = require('bluebird');
var moment = require('moment');

module.exports = function LeaderboardController(challengeService, repository, leaderboardFilterStringParser, resultFormatter, leaderboardFilterValidator, evidenceImageUrl, evidenceVideoUrl, avatarImageUrl, leaderboardDateProviderService, locationResolverService, leaderboardService) {
  var NUMBER_OF_RESULTS_TO_SHOW_PER_PAGE = 20;

  var _getSelectValueOrNull = function _getSelectValueOrNull(value) {
    if (value && value !== '-1') {
      return value.toLowerCase().replace(/\s+/g, '');
    }

    return null;
  };

  var _getLeaderboardFilterFromSubmittedForm = function _getLeaderboardFilterFromSubmittedForm(req) {
    return {
      challengeName: _getSelectValueOrNull(req.params.name),
      group: _getSelectValueOrNull(req.body.FilterGroup),
      location: req.body.FilterLocation,
      gender: _getSelectValueOrNull(req.body.FilterGender),
      age: _getSelectValueOrNull(req.body.FilterAge),
      weight: _getSelectValueOrNull(req.body.FilterWeight),
      onlyShowValidated: req.body.FilterOnlyShowValidated ? true : false
    };
  };

  var _setSelectedGroup = function _setSelectedGroup(groups, selectedGroup) {
    if (!selectedGroup) {
      return;
    }

    for (var index = 0; index < groups.length; index++) {
      var group = groups[index];

      if (group.id === selectedGroup) {
        group.selected = true;
        return;
      }
    }
  };

  var _populateFilters = function _populateFilters(challengeId, userId, data) {
    return repository.getFilters(challengeId, userId).then(function(filters) {
      _setSelectedGroup(filters.groups, filters.selectedGroup);

      data.groups = filters.groups;
      data.ageGroups = filters.ageGroups;
      data.weightGroups = filters.weightGroups;

      return data;
    });
  };

  var _getCurrentAreaName = function _getCurrentAreaName(location) {
    if (location.districtId) {
      return location.districtName;
    }

    if (location.countyId) {
      return location.countyName;
    }

    if (location.regionId) {
      return location.regionName;
    }

    if (location.countryId) {
      return location.countryName;
    }

    return null;
  };

  var _getChallenge = function _getChallenge(location, req) {
    return challengeService.getChallenge(location, req.params).then(function(data) {
      data.currentAreaName = _getCurrentAreaName(location);
      var userId = req.session.user ? req.session.user.id : null;
      return _populateFilters(data.challenge.id, userId, data);
    });
  };

  var _setSelectedFilters = function _setSelectedFilters(data) {
    data.ageGroups.forEach(function(group) {
      if (_getSelectValueOrNull(group.name) === data.filter.age) {
        group.selected = true;
      }
    });

    data.weightGroups.forEach(function(group) {
      if (_getSelectValueOrNull(group.name) === data.filter.weight) {
        group.selected = true;
      }
    });
  };


  var _getLeaderboardUrl = function _getLeaderboardUrl(location, req) {
    var form = _getLeaderboardFilterFromSubmittedForm(req);
    
    return _getLeaderboard(location, req).then(function(leaderboard) {      
      return _getLocation(location, form.location).catch(function(err) {
        leaderboard.validationErrors = [ [ 'Location', 'Error_Field_Invalid_Location' ] ];
        return Promise.reject(leaderboard);
      }).then(function(resolvedLocation) {
        var groupName = form.group || leaderboard.groups[0].name;
        
        var params = {
          challengeName: form.challengeName,
          challengeGroupName: groupName,
          username: _getUsername(req.session),
          location: resolvedLocation,
          filterGender: form.gender,
          filterAge: form.age,
          filterWeight: form.weight,
          filterOnlyValidated: form.onlyShowValidated
        };
        
        return leaderboardService.getUrl(params);
      });
    });
  };
  
  var _getLocation = function _getLocation(currentLocation, formLocation) {
    if (!formLocation) {
      return Promise.resolve(currentLocation);
    }
    
    return locationResolverService.getLocationFromName(formLocation).then(function(resolvedLocation) {      
      return resolvedLocation || Promise.reject('could not resolve location');
    });
  };
  
  var _getUsername = function _getUsername(session) {
    if (session && session.user) {
      return session.user.username;
    }
    
    return null;
  };

  var _getGroupId = function _getGroupId(data, req) {
    for (var index = 0; index < data.groups.length; index++) {
      var group = data.groups[index];
      
      var formattedGroupName = group.name.toLowerCase().replace(/ /g, '-');
      var formattedGroupParamName = req.params.group ? req.params.group.toLowerCase() : null;

      if (formattedGroupName === formattedGroupParamName || (req.body && req.body.FilterGroup && group.name === req.body.FilterGroup)) {
        return group.id;
      }
    }

    return null;
  };

  var _formatResults = function _formatResults(leaderboardData) {
    return leaderboardData.results.map(function(result) {
      result.result = resultFormatter.format(result.result, leaderboardData.measurement);
      result.submitted = moment(result.submitted);

      if (result.userHasAvatar === '1') {
        result.avatar = avatarImageUrl + result.userId + '-40x40.png';
        result.avatarRetina = avatarImageUrl + result.userId + '-80x80.png';
      } else {
        result.avatar = avatarImageUrl + '0-40x40.png';
        result.avatarRetina = avatarImageUrl + '0-80x80.png';
      }

      if (result.videoValidated) {
        result.evidence = evidenceVideoUrl + result.id + '.mp4';
      } else if (result.imageValidated) {
        result.evidence = evidenceImageUrl + result.id + '.png';
      }

      return result;
    });
  };

  var _appendAgeRange = function _appendAgeRange(filter) {
    return repository.getAgeRange(filter.age).then(function(data) {
      filter.ageMin = data.min;
      filter.ageMax = data.max;
    });
  };

  var _appendWeightRange = function _appendWeightRange(filter) {
    return repository.getWeightRange(filter.weight).then(function(data) {
      filter.weightMin = data.min;
      filter.weightMax = data.max;
    });
  };

  var _buildFilter = function _buildFilter(filter) {
    var queries = [];

    if (filter.age) {
      queries.push(_appendAgeRange(filter));
    }

    if (filter.weight) {
      queries.push(_appendWeightRange(filter));
    }

    return Promise.all(queries).then(function() {
      return filter;
    });
  };

  var _populateData = function _populateData(location, req, filter, leaderboardUpdateFrequency) {
    var leaderboardDates = leaderboardDateProviderService.getDates(leaderboardUpdateFrequency);

    return Promise.join(_getChallenge(location, req), _buildFilter(filter), function(data, filter) {
      data.challenge.groupId = _getGroupId(data, req);
      filter.challengeId = data.challenge.id;
      filter.challengeGroupId = data.challenge.groupId;
      filter.districtId = location.districtId;
      filter.countyId = location.countyId;
      filter.regionId = location.regionId;
      filter.countryId = location.countryId;
      filter.resultsPerPage = NUMBER_OF_RESULTS_TO_SHOW_PER_PAGE;
      filter.lastPeriodStart = leaderboardDateProviderService.SEASON_START_DATE;
      filter.currentPeriodStart = leaderboardDateProviderService.SEASON_START_DATE;
      filter.lastPeriodEnd = leaderboardDates.previous;
      filter.currentPeriodEnd = leaderboardDates.current;

      data.filter = filter;
      data.page = filter.page;
      data.lastUpdated = leaderboardDates.current;
      data.nextUpdate = leaderboardDates.next;
      data.onlyShowValidated = filter.onlyShowValidated;

      _setSelectedFilters(data);
      return data;
    });
  };

  var _getLeaderboard = function _getLeaderboard(location, req) {
    var filter = leaderboardFilterStringParser.parse(req.params['0']);

    return repository.getConfiguration(location.countryId).then(function(config) {
      return _populateData(location, req, filter, config.leaderboard.updateFrequency);
    }).then(function(data) {
      var userId = 0;
      if (req.session.user){
        userId = req.session.user.id;
      }
      return [ data, repository.getLeaderboardResult(filter, userId) ];
    }).spread(function(data, leaderboardResults) {
      if (leaderboardResults && leaderboardResults.results) {
        data.results = _formatResults(leaderboardResults);
        data.numberOfPages = leaderboardResults.numberOfPages;
      }
      
      if (req.session.resultSubmitSuccessfulFlash) {
        req.session.resultSubmitSuccessfulFlash = null;
        data.resultSubmitSuccessfulFlash = true;
      }
      
      return data;
    });
  };

  return {
    getLeaderboardUrl: _getLeaderboardUrl,
    getLeaderboard: _getLeaderboard
  };
};
