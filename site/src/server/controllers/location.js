module.exports = function LocationController(repository) {
  var _getLocation = function getLocation(country, region, county, district) {
    var location = {};

    return repository.getCountry(country).then(function(data) {
      if (!data) {
        return location;
      }

      location.countryId = data.id;
      location.countryName = data.name;
      location.countryCode = country.toLowerCase();
      location.countryIsAvailable = data.isAvailable;

      if (!region) {
        return location;
      }

      return repository.getRegion(location.countryId, region).then(function(data) {
        if (!data) {
          return location;
        }

        location.regionId = data.id;
        location.regionName = data.name;

        if (!county) {
          return location;
        }

        return repository.getCounty(location.regionId, county).then(function(data) {
          if (!data) {
            return location;
          }

          location.countyId = data.id;
          location.countyName = data.name;

          if (!district) {
            return location;
          }

          return repository.getDistrict(location.countyId, district).then(function(data) {
            if (!data) {
              return location;
            }

            location.districtId = data.id;
            location.districtName = data.name;

            return location;
          });
        });
      });
    });
  };

  return {
    getLocation: _getLocation
  };
};