module.exports = function AdvertisementController(repository, advertisementImageUrl) {
  var _getAdvertisements = function _getAdvertisements(location) {
    return repository.getAdvertisements(location).then(function(advertisements) {
      if (!advertisements) {
        return {};
      }

      return advertisements.map(function(row) {
        var advertisement = {
          link: row.link
        };

        var imagePrefix =  advertisementImageUrl + row.id.toString() + '-';

        if (row.smallHorizontalImageExtension) {
          advertisement.small = imagePrefix + 'sm.' + row.smallHorizontalImageExtension;
        }

        if (row.largeVerticalImageExtension) {
          advertisement.large = imagePrefix + 'lg.' + row.largeVerticalImageExtension;
        }

        return advertisement;
      });
    });
  };

  return {
    getAdvertisements: _getAdvertisements
  };
};
