var Promise = require('bluebird');
var moment = require('moment');
var stringFormatter = require('../utilities/string-formatter');

module.exports = function ResultController(challengeService, repository, resultValidator, imageUploadService, videoUploadService, evidenceImageUrl, evidenceVideoUrl, resultFormatter, messageQueueService, publicSite) {
  var _getResultFromSubmittedForm = function _getResultFromSubmittedForm(req) {
    return {
      group: req.body.FilterGroup,
      result: req.body.Result
    };
  };

  var _setSelectedGroup = function _setSelectedGroup(groups) {
    if (!groups.selectedGroup) {
      return;
    }

    for (var index = 0; index < groups.groups.length; index++) {
      var group = groups.groups[index];
      if (group.id === groups.selectedGroup) {
        group.selected = true;
        return;
      }
    }
  };
  
  var _processNotifications = function _processNotifications(notifications) {
    if (notifications) {
      var locationNotSetIndex = null;
      var weightNotSetIndex = null;
      
      for (var index = 0; index < notifications.length; index++) {
        var notification = notifications[index];
        if (notification.id === 3) {
          locationNotSetIndex = index;
        } else if (notification.id === 4) {
          weightNotSetIndex = index;
        }
      }
      
      if (locationNotSetIndex && weightNotSetIndex) {
        notifications[locationNotSetIndex].title = 'Location and Weight Not Set';
        notifications[locationNotSetIndex].description = 'You need to specify your location and weight before you can submit a result so you can be ranked accordingly.';
        notifications.splice(weightNotSetIndex, 1);
      }
    }
  };

  var _getChallenge = function _getChallenge(location, req) {
    var _challenge = null;

    return challengeService.getChallenge(location, req.params).then(function(data) {
      data.currentAreaName = location.districtName;

      if (!req.session.user) {
        return data;
      }

      return repository.getNotifications(req.session.user.id).then(function(notifications) {
        _processNotifications(notifications);
        
        if (notifications) {
          data.notifications = notifications.filter(function(notification) {
            return notification.id > 1 && notification.id < 5;
          });
        } else {
          data.notifications = [];
        }

        return data;
      });
    }).then(function(challenge) {
      _challenge = challenge;
      return repository.getChallengeGroups(_challenge.challenge.id, req.session.user ? req.session.user.id : null);
    }).then(function(groups) {
      _setSelectedGroup(groups);
      _challenge.groups = groups.groups;
      return _challenge;
    });
  };

  var _getChallengeGroupId = function _getChallengeGroupId(data, groupName) {
    if (!groupName) {
      return false;
    }

    var challengeGroupId = null;

    for (var index = 0; index < data.groups.length; index++) {
      var group = data.groups[index];

      if (group.name === groupName) {
        challengeGroupId = group.id;
        group.selected = true;
        data.selectedGroupName = group.name;
      } else {
        group.selected = false;
      }
    }

    return challengeGroupId;
  };
  
  var _notifyNominated = function _notifyNominated(nominator, challenge, result, measurement, user, resultId) {
    var message = JSON.stringify({
      userId: nominator.id,
      username: nominator.username,
      firstName: nominator.firstName,
      lastName: nominator.lastName,
      email: nominator.email,
      nomineeUserName: user.username,
      nomineeFirstName: user.firstName,
      nomineeLastName: user.lastName,
      challenge: challenge.name,
      result: result,
      measurement: measurement,
      link: publicSite + 'result/' + resultId + '/'
    });

    return messageQueueService.sendMessage('nomination-accepted-user-email', message).then(function() {
      return repository.nominationAccepted(nominator.id, challenge.id, user.id);
    });
  };

  var _submitResult = function _submitResult(location, req, fileType) {
    var form = _getResultFromSubmittedForm(req);
    var _data = null;

    return _getChallenge(location, req).then(function(data) {
      _data = data;
      _data.challengeGroupId = _getChallengeGroupId(_data, form.group);

      if (!_data.challengeGroupId) {
        form.group = null;
      }

      var challengeHasMultipleGroups = data.groups.length > 1;

      if (!challengeHasMultipleGroups) {
        _data.challengeGroupId = data.groups[0].id;
        data.selectedGroupName = data.groups[0].name;
      }
      
      return resultValidator.validate(form, challengeHasMultipleGroups, data.challenge.measurement);
    }).error(function(err) {
      _data.validationErrors = err.validationErrors;
      _data.result = form.result;
      return Promise.reject(_data);
    }).then(function() {
      return repository.saveResult(req.session.user.id,
                                   _data.challenge.id,
                                   _data.challengeGroupId,
                                   form.result,
                                   'site-' + fileType).then(function(resultId) {
        _data.resultId = resultId;
        
        return repository.getNominationData(req.session.user.id, _data.challenge.id);
      });
    }).then(function(nominationData) {
      if (!nominationData) {
        return Promise.resolve();
      }
      
      return Promise.map(nominationData, function(nominator) {
        return _notifyNominated(nominator, _data.challenge, form.result, _data.challenge.measurement, req.session.user, _data.resultId);
      });
    }).then(function() {
      req.session.resultSubmitSuccessfulFlash = true;
      return _data;
    });
  };

  var _signUploadRequest = function _signUploadRequest(location, req) {
    if (!req.session.user) {
      return Promise.reject('not authorised');
    }
    
    var evidence = req.body;
    var fileType = evidence.type.split('/')[0];

    if (fileType !== 'video' && fileType !== 'image') {
      return Promise.reject('unsupported file');
    }

    var _data = null;

    return _submitResult(location, req, fileType).then(function(data) {
      if (!data) {
        return Promise.reject('problem submitting result');
      }
      
      _data = data;
      
      evidence.name = data.resultId.toString();

      if (fileType === 'video') {
        return videoUploadService.signUploadRequest('videos-to-process', evidence);
      }

      return imageUploadService.signUploadRequest('evidence-photos-to-process', evidence);
    }).then(function(url) {
      var leaderboardUrl = stringFormatter.getUrlFriendlyString(publicSite + 'user/' + req.session.user.username + '/latest/' + _data.challenge.name + '/' + _data.selectedGroupName + '/');
      
      return {
        uploadUrl: url,
        leaderboardUrl: leaderboardUrl
      };
    });
  };
  
  var _getResult = function _getResult(req) {
    var resultId = req.params.resultId;
    
    var userId = null;
    
    if (req.session && req.session.user) {
      userId = req.session.user.id;
    }
    
    return repository.get(resultId, userId).then(function(data) {
      data.result = resultFormatter.format(data.result, data.measurement);

      if (data.videoValidated) {
        data.evidence = evidenceVideoUrl + resultId + '.mp4';
        data.videoImage = evidenceVideoUrl + resultId + '.jpg';
      } else if (data.imageValidated) {
        data.evidence = evidenceImageUrl + resultId + '.png';
      }
      
      if (data.group === '-'){
        data.group = null;
      }
      
      data.submitted = moment(data.submitted);      
      data.link = 'https://gymfit.com' + req.url;
      data.resultId = resultId;
      data.embedLink = 'https://gymfit.com' + req.url + 'embed/';
      
      return {
        result: data
      };
    });
  };
  
  var _getVideoLink = function _getVideoLink(req) {
    return new Promise.resolve(evidenceVideoUrl + req.params.resultId + '.mp4');
  };

  return {
    getChallenge: _getChallenge,
    submitResult: _submitResult,
    signUploadRequest: _signUploadRequest,
    getResult: _getResult,
    getVideoLink: _getVideoLink
  };
};
