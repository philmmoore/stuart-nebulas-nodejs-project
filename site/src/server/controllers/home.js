var Promise = require('bluebird');

module.exports = function HomeController(repository, challengeService) {
  var _getData = function _getData(req, locals) {
    return Promise.resolve().then(function() {
      return [
        challengeService.getChallenges(req, locals.string),
        repository.getConfiguration(locals.location.countryId)
      ];
    }).spread(function(categories, configuration) {
      return {
        categories: categories,
        configuration: configuration
      };
    });
  };

  return {
    getData: _getData
  };
};
