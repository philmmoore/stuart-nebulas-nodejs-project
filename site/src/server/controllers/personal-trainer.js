var Promise = require('bluebird');
var moment= require('moment');

module.exports = function PersonalTrainerController(repository, validator, avatarImageUrl, locationResolverService) {
  var NUMBER_OF_RESULTS_TO_SHOW_PER_PAGE = 20;
  
  var _getSearchQueryUrl = function _getSearchQueryUrl(req) {
    var form = {
      userQuery: req.body.UserQuery ? req.body.UserQuery.trim() : null,
      locationQuery: req.body.LocationQuery ? req.body.LocationQuery.trim() : null
    };
    
    return validator.validate(form).then(function() {
      var url = '/gb/personal-trainers/';
      
      if (form.locationQuery) {
        url += 'location/' + form.locationQuery.toLowerCase() + '/';
      }
      
      if (form.userQuery) {
        url += 'user/' + form.userQuery.toLowerCase() + '/';
      }
      
      return url;
    });
  };
  
  var _formatSearchResults = function _formatSearchResults(results) {
    for (var index = 0; index < results.length; index++) {
      var result = results[index];
      
      if (result.avatarUploaded === '1') {
        result.avatar = avatarImageUrl + result.id + '-40x40.png';
        result.avatarRetina = avatarImageUrl + result.id + '-80x80.png';
      } else {
        result.avatar = avatarImageUrl + '0-40x40.png';
        result.avatarRetina = avatarImageUrl + '0-80x80.png';
      }
    }
  };
  
  var _searchByUser = function _searchByUser(form) {
    return repository.searchByUser(form.userQuery, NUMBER_OF_RESULTS_TO_SHOW_PER_PAGE, form.page);
  };
  
  var _searchByLocation = function _searchByLocation(form) {
    return locationResolverService.getLocationFromName(form.locationQuery).then(function(resolvedLocation) {
      if (!resolvedLocation) {
        form.validationErrors = [ [ 'Location', 'Error_Field_Invalid_Location' ] ];
        return Promise.reject(form);
      }
      
      return resolvedLocation;
    }).then(function(location) {
      return repository.searchByLocation(form.userQuery, location.districtId, location.countyId, location.regionId, location.countryId, NUMBER_OF_RESULTS_TO_SHOW_PER_PAGE, form.page);
    });
  };
  
  var _search = function _search(req) {
    var form = {
      userQuery: req.params.user ? req.params.user.trim() : null,
      locationQuery: req.params.location ? req.params.location.trim() : null,
      page: req.params.page
    };
    
    if (isNaN(form.page)) {
      form.page = 1;
    } else {
      form.page = parseInt(form.page);
    }
    
    return validator.validate(form).then(function() {
      return form.locationQuery ? _searchByLocation(form) : _searchByUser(form);
    }).then(function(results) {
      if (results.results) {
        _formatSearchResults(results.results);
        form.results = results.results;
        form.numberOfPages = results.numberOfPages;
      } else {
        form.noResultsFound = true;
      }
      
      return form;
    });
  };

  return {
    getSearchQueryUrl: _getSearchQueryUrl,
    search: _search
  };
};