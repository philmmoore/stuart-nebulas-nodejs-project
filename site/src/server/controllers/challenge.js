var Promise = require('bluebird');

module.exports = function ChallengeController(challengeService, repository, challengeValidator, leaderboardService) {
  var _setSelectedGroup = function _setSelectedGroup(groups) {
    if (!groups.selectedGroup) {
      return;
    }

    for (var index = 0; index < groups.groups.length; index++) {
      var group = groups.groups[index];
      if (group.id === groups.selectedGroup) {
        group.selected = true;
        return;
      }
    }
  };

  var _getChallenge = function _getChallenge(location, req) {
    return challengeService.getChallenge(location, req.params).then(function(data) {
      return repository.getChallengeGroups(data.challenge.id, req.session.user ? req.session.user.id : null).then(function(groups) {
        _setSelectedGroup(groups);

        data.groups = groups.groups;
        
        if (req.session && req.session.user && !data.selectedLocation) {
          var userLocation = req.session.user.location;
          data.selectedLocation = userLocation.districtName || userLocation.regionName;
        }
        
        return data;
      });
    });
  };

  var _getChallenges = function _getChallenges(req, translationFor) {
    return challengeService.getChallenges(req, translationFor).then(function(data) {
      return {
        categories: data
      };
    });
  };

  var _getSubmittedForm = function _getSubmittedForm(req) {
    return {
      group: req.body.FilterGroup,
      selectedLocation: req.body.Location
    };
  };

  var _rejectGetLinkForChallenge = function _rejectGetLinkForChallenge(data, form, validationErrors) {
    data.validationErrors = validationErrors;
    data.selectedLocation = form.selectedLocation;

    if (form.group) {
      data.group = form.group;

      for (var index = 0; index < data.groups.length; index++) {
        var group = data.groups[index];
        group.selected = group.name === form.group;
      }
    }

    return Promise.reject(data);
  };

  var _isGroupForChallenge = function _isGroupForChallenge(groups, group) {
    for (var index = 0; index < groups.length; index++) {
      if (groups[index].name === group) {
        return true;
      }
    }

    return false;
  };

  var _getLinkForChallenge = function _getLinkForChallenge(req, location) {
    var form = _getSubmittedForm(req);

    return _getChallenge(location, req).then(function(data) {
      var challengeHasMultipleGroups = data.groups.length > 1;

      if (!challengeHasMultipleGroups) {
        form.group = data.groups[0].name;
      }

      return challengeValidator.validate(form).error(function(error) {
        return _rejectGetLinkForChallenge(data, form, error.validationErrors);
      }).then(function() {
        if (!_isGroupForChallenge(data.groups, form.group)) {
          return _rejectGetLinkForChallenge(data, form, [ ['Group', 'Error_Field_Invalid_Challenge_Group'] ]);
        }
        
        var username = _getUsername(req.session);
        
        return _getUrl(data.challenge.name, form.group, username, location);
      });
    });
  };
  
  var _getUrl = function _getUrl(challengeName, challengeGroupName, username, location) {
    var params = {
      challengeName: challengeName,
      challengeGroupName: challengeGroupName,
      username: username,
      location: location
    };
    
    return leaderboardService.getUrl(params);
  };
  
  var _getUsername = function _getUsername(session) {
    if (session && session.user) {
      return session.user.username;
    }
    
    return null;
  };

  return {
    getChallenge: _getChallenge,
    getChallenges: _getChallenges,
    getLinkForChallenge: _getLinkForChallenge
  };
};