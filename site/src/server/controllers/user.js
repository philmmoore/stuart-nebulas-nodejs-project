var Promise = require('bluebird');
var moment= require('moment');

module.exports = function UserController(repository, avatarImageUrl, imageUploadService, validator, leaderboardDateProviderService, resultFormatter, socialService, markdownTextFormatter) {
  var COUNTRY_ID = 1;
  
  var _processNotifications = function _processNotifications(notifications) {
    if (notifications) {
      var locationNotSetIndex = null;
      var weightNotSetIndex = null;
      
      for (var index = 0; index < notifications.length; index++) {
        var notification = notifications[index];
        if (notification.id === 3) {
          locationNotSetIndex = index;
        } else if (notification.id === 4) {
          weightNotSetIndex = index;
        }
      }
      
      if (locationNotSetIndex && weightNotSetIndex) {
        notifications[locationNotSetIndex].title = 'Location and Weight Not Set';
        notifications[locationNotSetIndex].description = 'You need to specify your location and weight before you can submit a result so you can be ranked accordingly.';
        notifications.splice(weightNotSetIndex, 1);
      }
    }
  };
  
  var _refreshUserInSession = function _refreshUserInSession(session, user) {
    session.user = user;
    
    if (session.refreshUserAvatar) {
      session.user.avatar = avatarImageUrl + (user.avatarUploaded === '1' ? user.id : '0') + '-50x50.png';
      session.user.avatarRetina = avatarImageUrl + (user.avatarUploaded === '1' ? user.id : '0') + '-100x100.png';
    }
  };
  
  var _hidePrivateDetailsFromPublicProfile = function _hidePrivateDetailsFromPublicProfile(user) {
    user.height = null;
    user.weight = null;
    user.age = null;
  };
  
  var _appendAvatar = function _appendAvatar(user, regularSize, retinaSize) {
    var avatar = avatarImageUrl + (user.avatarUploaded === '1' ? user.id : '0');
    user.avatar = avatar + '-' + regularSize + 'x' + regularSize + '.png';
    user.avatarRetina = avatar + '-' + retinaSize + 'x' + retinaSize + '.png';
  };
  
  var _formatUserData = function _formatUserData(user, userId) {

    if (user.dateOfBirth) {
      user.age = moment().diff(moment(user.dateOfBirth), 'years');
    }

    if (user.height) {
      user.height = user.height / 100;
    }

    if (user.weight) {
      user.weight = user.weight / 1000;
    }

    _appendAvatar(user, 120, 240);
    
    for (var followingIndex = 0; followingIndex < user.following.length; followingIndex++) {
      _appendAvatar(user.following[followingIndex], 120, 240);
    }
    
    for (var followerIndex = 0; followerIndex < user.followers.length; followerIndex++) {
      var follower = user.followers[followerIndex];
      
      if (follower.id === userId) {
        user.isBeingFollowed = true;
      }
      
      _appendAvatar(follower, 120, 240);
    }
    
    for (var clientIndex = 0; clientIndex < user.clients.length; clientIndex++) {
      _appendAvatar(user.clients[clientIndex], 120, 240);
    }
    
    for (var trainerIndex = 0; trainerIndex < user.trainers.length; trainerIndex++) {
      if (user.trainers[trainerIndex].id === userId) {
        user.isClient = true;
      }
    }

  };
  
  var _formatActivities = function _formatActivities(activities, username) {
    if (!activities) {
      return [];
    }
    
    return activities.map(function(activity) {
      var challengeLink = activity.challengeName.toLowerCase().replace(/ /g, '-');
      activity.tryAgainLink = '/gb/new/' + challengeLink + '/';
      activity.nominateLink = '/gb/' + challengeLink + '/nominate/';
      activity.viewLink = '/user/' + username + '/latest/' + challengeLink + '/' + activity.challengeGroupName + '/';
      activity.result = resultFormatter.format(activity.result, activity.measurement);
      activity.submitted = moment(activity.submitted);
      
      return activity;
    });
  };
  
  var _getUser = function _getUser(req) {
    var username = req.params.username;

    return repository.getUser(username).then(function(user) {
      if (!user) {
        return Promise.reject('user not found');
      }
      
      return user;
    }).then(function(user) {

      _formatUserData(user, (req.session.user ? req.session.user.id : 0));
      
      return [
        repository.getConfiguration(COUNTRY_ID),
        {
          user: user
        }
      ];
    }).spread(function(config, data) {
      var leaderboardDates = leaderboardDateProviderService.getDates(config.leaderboard.updateFrequency);
      return repository.getActivities(data.user.id, leaderboardDateProviderService.SEASON_START_DATE, leaderboardDates.current, leaderboardDateProviderService.SEASON_START_DATE, leaderboardDates.previous).then(function(activities) {
        data.activities = _formatActivities(activities, username);
        return data;
      });
    }).then(function(data) {
      return repository.getNominations(data.user.id).then(function(nominations) {
        data.nominations = nominations || [];
        
        for (var index = 0; index < data.nominations.length; index++) {
          var nomination = data.nominations[index];
          nomination.link = '/gb/new/' + nomination.challengeName.toLowerCase().replace(/ /g, '-') + '/';
          
          for (var userIndex = 0; userIndex < nomination.nominators.length; userIndex++) {
            var nominator = nomination.nominators[userIndex];
            _appendAvatar(nominator, 40, 80);
          }
        }
        
        return data;
      });
    }).then(function(data) {

      // This commented code is what's used to 'hide' user social updates, we want them to be 
      // public so we're commenting it out.

      // if (!req.session.user || req.session.user.id !== data.user.id) {
      //   _hidePrivateDetailsFromPublicProfile(data.user);
      //   return data;
      // }

      data.user.showOnlyMine = req.showOnlyMine;
      
      var userId = data.user.id;
      if (req.session.user && req.session.user.id == data.user.id){

        data.isLoggedInUser = true;
        userId = req.session.user.id;

        if (req.showOnlyMine !== false & req.showOnlyMine !== true){
          data.user.showOnlyMine = false;
        }

      } else {

        if (req.showOnlyMine !== false & req.showOnlyMine !== true){
          data.user.showOnlyMine = true;
        }

      }

      // var userId = req.session.user.id;
      // _refreshUserInSession(req.session, data.user);

      return repository.getNotifications(userId).then(function(notifications) {
        _processNotifications(notifications);
        data.notifications = notifications;
        
        return data;
      }).then(function(data) {
        return socialService.getUpdates(userId, moment(), data.user.showOnlyMine).then(function(socialUpdates) {
          data.socialUpdates = socialUpdates;
          
          return data;
        });
      });

    }).then(function(data){

      // If the user is a PT or Gym then we want to add review data to the user object
      if (data.user.isPersonalTrainer || data.user.isGym){

        // Split the users rating so it's readable by the view
        if (data.user.reviews.rating === null){
          data.user.reviews.rating = 0.0;
        }

        // Add decimal point if not found
        data.user.reviews.rating = data.user.reviews.rating.toFixed(1);

        var splitRating = data.user.reviews.rating.toString().split('.');
        data.user.reviews.decimal = {
          star: parseInt(splitRating[0]) + 1,
          rating: parseInt(splitRating[1])
        };

        // Get the user review data
        return _getReviewsFromDate(data.user.id, moment().add(1,'hours')).then(function(reviews){

          // User paginateReviews helper here to to get paginated results
          if (reviews && reviews.length > 0){

            for (var i in reviews){
             _formatUserData(reviews[i].reviewer, reviews[i].reviewer.id);
             reviews[i].bodyHtml = markdownTextFormatter.format(reviews[i].body);
            }

            _paginateReviews(data.user.reviews, reviews, 4);

          } else {

            data.user.reviews.data = [];

          }

          return data;

        });
      }
      return data;
    });
  };
  
  var _paginateReviews = function _paginateReviews(data, reviews, itemsPerPage){
    data.perPage = itemsPerPage;
    data.pages = Math.ceil(data.count/itemsPerPage);
    data.data = [];
    // Loop through the reviews to build the data array of pages
    var page = [];
    var reviewCount = 1;
    for (var index in reviews){
      // console.log(index);
      var review = reviews[index];
      // Add the review to the current page
      page.push(review);
      // Record that we've counted a review
      reviewCount++;
      // Push the page and reset the counter if it hits the per page limit or is the end of the list
      if (reviewCount > itemsPerPage || parseInt(index) + 1 >= reviews.length){
        data.data.push(page);
        reviewCount = 1;
        page = [];
      }
    }

    return data;

  };

  var _acknowledgeNotification = function _acknowledgeNotification(req, action) {
    if (req.session.user.username !== req.params.username) {
      return Promise.reject('not authorised');
    }
    
    var notificationId = action.split('acknowledge-notification-')[1];

    return repository.acknowledgeNotification(req.session.user.id, notificationId).then(function() {
      return _getUser(req);
    });
  };
  
  var _followUser = function _followUser(req) {
    return repository.follow(req.params.username, req.session.user.id).then(function() {
      return _getUser(req);
    });
  };
  
  var _unfollowUser = function _unfollowUser(req) {
    return repository.unfollow(req.params.username, req.session.user.id).then(function() {
      return _getUser(req);
    });
  };
  
  var _setClient = function _setClient(req) {
    return repository.setClient(req.session.user.id, req.params.username).then(function() {
      return _getUser(req);
    });
  };
  
  var _unsetClient = function _unsetClient(req) {
    return repository.unsetClient(req.session.user.id, req.params.username).then(function() {
      return _getUser(req);
    });
  };
  
  var _performAction = function _performAction(req) {
    
    if (!req.session.user) {
      return Promise.reject('not authorised');
    }
    
    var action = Object.keys(req.body)[0];
    
    if (action === 'follow') {
      return _followUser(req);
    } else if (action === 'unfollow') {
      return _unfollowUser(req);
    } else if (action === 'set-client') {
      return _setClient(req);
    } else if (action === 'unset-client') {
      return _unsetClient(req);
    } else if (action.indexOf('acknowledge-notification-') > -1) {
      return _acknowledgeNotification(req, action);
    }
    
    return Promise.reject('not authorised');
  };

  var _signUploadRequest = function _signUploadRequest(req) {
    if (!req.session.user || req.session.user.username !== req.params.username) {
      return Promise.reject('not authorised');
    }

    var file = req.body;
    var fileType = file.type.split('/')[0];

    if (fileType !== 'image') {
      return Promise.reject('unsupported file');
    }

    file.name = req.session.user.id.toString();
    
    req.session.refreshUserAvatar = true;
    return imageUploadService.signUploadRequest('avatars-to-process', file);
  };
  
  var _getUpdateHeightForm = function _getUpdateHeightForm(req) {
    return {
      heightCm: req.body.HeightCm,
      heightFeet: req.body.HeightFeet,
      heightInches: req.body.HeightInches
    };
  };
  
  var _updateHeight = function _updateHeight(req) {
    var form = _getUpdateHeightForm(req);
    
    return validator.validateUpdateHeight(form).then(function() {
      var height = Math.floor(parseFloat(form.heightCm) * 100);
      return repository.updateHeight(req.session.user.id, height);
    });
  };
  
  var _getUpdateWeightForm = function _getUpdateWeightForm(req) {
    return {
      weightKg: req.body.WeightKg,
      weightLbs: req.body.WeightLbs,
      weightOz: req.body.WeightOz
    };
  };
  
  var _updateWeight = function _updateWeight(req) {
    var form = _getUpdateWeightForm(req);
    
    return validator.validateUpdateWeight(form).then(function() {
      var weight = Math.floor(parseFloat(form.weightKg) * 1000);
      return repository.updateWeight(req.session.user.id, weight);
    });
  };
  
  var _getLocationData = function getLocationData(req) {
    return _getUser(req).then(function(data) {
      return repository.getRegions(COUNTRY_ID).then(function(regions) {
        for (var index = 0; index < regions.length; index++) {
          if (data.user.location.regionId === regions[index].id) {
            regions[index].selected = true;
          }
        }
        
        data.regions = regions;
        return data;
      });
    });
  };
  
  var _getLocationForm = function _getLocationForm(req) {
    return {
      regionId: req.body.Region,
      district: req.body.District
    };
  };
  
  var _resolveDistrictId = function _resolveDistrictId(district, districts) {
    for (var index = 0; index < districts.length; index++) {
      if (district.trim().toLowerCase() === districts[index].name.trim().toLowerCase()) {
        return districts[index].id;
      }
    }
    
    return null;
  };
  
  var _updateLocation = function _updateLocation(req) {
    var form = _getLocationForm(req);
    
    return validator.validateLocation(form).error(function(form) {
      return _getLocationData(req).then(function(data) {
        data.validationErrors = form.validationErrors;
        return Promise.reject(data);
      });
    }).then(function() {
      return repository.getDistrictsForRegionId(form.regionId);
    }).then(function(districts) {
      var districtId = _resolveDistrictId(form.district, districts);
      
      if (!districtId) {
        return _getLocationData(req).then(function(data) {
          data.validationErrors = [ [ 'District', 'Error_Field_Invalid_District' ] ];
          data.district = form.district;
          return Promise.reject(data);
        });
      }
      
      return repository.setLocation(req.session.user.id, form.regionId, districtId);
    });
  };
  
  var _getRoutineTypes = function _getRoutineTypes(req) {
    return _getUser(req).then(function(data) {
      return repository.getRoutineTypes().then(function(routineTypes) {
        for (var index = 0; index < routineTypes.length; index++) {
          if (data.user.routineType === routineTypes[index].name) {
            routineTypes[index].selected = true;
          }
        }
        
        data.routineTypes = routineTypes;
        return data;
      });
    });
  };
  
  var _getUpdateRoutineForm = function _getUpdateRoutineForm(req) {
    return {
      routineId: req.body.Routine
    };
  };
  
  var _updateRoutine = function _updateRoutine(req) {
    var form = _getUpdateRoutineForm(req);
    
    return validator.validateUpdateRoutine(form).error(function(form) {
      return _getRoutineTypes(req).then(function(data) {
        data.validationErrors = form.validationErrors;
        return Promise.reject(data);
      });
    }).then(function() {
      return repository.setRoutineType(req.session.user.id, form.routineId);
    });
  };
  
  var _getUpdateGymForm = function _getUpdateGymForm(req) {
    return {
      gym: req.body.Gym
    };
  };
  
  var _updateGym = function _updateGym(req) {
    var form = _getUpdateGymForm(req);
    
    return validator.validateUpdateGym(form).error(function(form) {
      return _getUser(req).then(function(data) {
        return Promise.reject({
          user: form,
          validationErrors: form.validationErrors
        });
      });
    }).then(function() {
      return repository.setGym(req.session.user.id, form.gym.trim());
    });
  };
  
  var _getPrivacyPreferences = function _getPrivacyPreferences(req) {
    return repository.getPrivacyPreferences(req.session.user.id);
  };
  
  var _setUserPrivacyPreferences = function _setUserPrivacyPreferences(req) {
    var privacyPreferences = {
      userId: req.session.user.id,
      gymFitEmailOptIn: req.body.GymFitEmailOptOut ? false : true,
      partnersEmailOptIn: req.body.PartnersEmailOptOut ? false : true,
      gymFitSMSOptIn: req.body.GymFitSMSOptOut ? false : true,
      partnersSMSOptIn: req.body.PartnersSMSOptOut ? false : true,
      gymFitOnlineOptIn: req.body.GymFitOnlineOptOut ? false : true,
      partnersOnlineOptIn: req.body.PartnersOnlineOptOut ? false : true
    };

    return repository.setPrivacyPreferences(privacyPreferences);
  };
  
  var _getNotificationPreferences = function _getNotificationPreferences(req) {
    return repository.getEmailPreference(req.session.user.id);
  };
  
  var _setNotificationPreferences = function _setNotificationPreferences(req) {
    var preference = {
      userId: req.session.user.id,
      ranking: req.body.Ranking ? true : false,
      progress: req.body.Progress ? true : false,
      news: req.body.News ? true : false,
      promotion: req.body.Promotion ? true : false,
      nomination: req.body.Nomination ? true : false
    };

    return repository.setEmailPreference(preference);
  };
  
  var _getReviewsFromDate = function _getReviewsFromDate(userId, timestamp, count){
    return repository.getReviews(userId, timestamp, count);
  };

  var _leaveReview = function _leaveReview(userId, body){

    return validator.validateReviewBody(body).error(function(form) {
      var data = {};
        data.validationErrors = form.validationErrors;
        return Promise.reject(data);
    }).then(function() {
      return repository.leaveReview(userId, body);
    });

  };

  return {
    getUser: _getUser,
    performAction: _performAction,
    signUploadRequest: _signUploadRequest,
    updateHeight: _updateHeight,
    updateWeight: _updateWeight,
    getLocationData: _getLocationData,
    updateLocation: _updateLocation,
    getRoutineTypes: _getRoutineTypes,
    updateRoutine: _updateRoutine,
    updateGym: _updateGym,
    getPrivacyPreferences: _getPrivacyPreferences,
    setUserPrivacyPreferences: _setUserPrivacyPreferences,
    getNotificationPreferences: _getNotificationPreferences,
    setNotificationPreferences: _setNotificationPreferences,
    getReviewsFromDate: _getReviewsFromDate,
    leaveReview: _leaveReview
  };
};