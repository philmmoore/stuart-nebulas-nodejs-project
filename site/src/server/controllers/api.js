var Promise = require('bluebird');
var moment = require('moment');

module.exports = function APIController(repository, socialService, removeImageService, removeVideoService, messageQueueService) {
  var _getDistrictsByRegionId = function _getDistrictsByRegionId(regionId) {
    if (isNaN(regionId)) {
      return Promise.reject('invalid region id.');
    }
    
    return repository.getDistrictsForRegionId(regionId).then(function(data) {
      return data.map(function(item) {
        return item.name;
      });
    });
  };
  
  var _getAllLocationNames = function _getAllLocationNames() {
    return repository.getAllLocationNames();
  };
  
  var _flagSocialUpdate = function _flagSocialUpdate(req) {
    var updateId = req.params.updateId;
    
    if (!req.session || !req.session.user) {
      return Promise.reject('unauthorised');
    }
    
    return repository.flagSocialUpdate(updateId, req.session.user.id).then(function(response) {
      var message = JSON.stringify({
        updateId: updateId
      });

      return messageQueueService.sendMessage('social-update-flagged-email', message).then(function() {
        return response;
      });
    });
  };
  
  var _unflagSocialUpdate = function _unflagSocialUpdate(req) {
    var updateId = req.params.updateId;
    
    if (!req.session || !req.session.user) {
      return Promise.reject('unauthorised');
    }
    
    return repository.unflagSocialUpdate(updateId, req.session.user.id);
  };
  
  var _likeSocialUpdate = function _likeSocialUpdate(req) {
    var updateId = req.params.updateId;
    
    if (!req.session || !req.session.user) {
      return Promise.reject('unauthorised');
    }
    
    return repository.likeSocialUpdate(updateId, req.session.user.id);
  };
  
  var _unlikeSocialUpdate = function _unlikeSocialUpdate(req) {
    var updateId = req.params.updateId;
    
    if (!req.session || !req.session.user) {
      return Promise.reject('unauthorised');
    }
    
    return repository.unlikeSocialUpdate(updateId, req.session.user.id);
  };
  
  var _getSocialUpdates = function _getSocialUpdates(req) {

    // if (!req.session || !req.session.user) {
    //   return Promise.reject('unauthorised');
    // }
    var showOnlyMine = req.params.showOnlyMine;
    var userId = req.params.userId;
    var timestamp = req.params.timestamp;
    
    if (!moment(timestamp).isValid()) {
      return Promise.reject('invalid timestamp.');
    }

    return socialService.getUpdates(userId, timestamp, showOnlyMine).then(function(socialUpdates) {
      return {
        socialUpdates: socialUpdates
      };
    });
  };
  
  var _deleteSocialUpdate = function _deleteSocialUpdate(req) {
    var updateId = req.params.updateId;
    
    if (!req.session || !req.session.user) {
      return Promise.reject('unauthorised');
    }
    
    return repository.deleteSocialUpdate(updateId, req.session.user.id).then(function(data) {
      var tasks = [];
      
      for (var imageIndex = 0; imageIndex < data.images.length; imageIndex++) {
        tasks.push(removeImageService.deleteSocialUpdateImage(data.images[imageIndex]));
      }
      
      for (var videoIndex = 0; videoIndex < data.videos.length; videoIndex++) {
        tasks.push(removeVideoService.deleteSocialUpdateVideo(data.videos[videoIndex]));
      }
      
      return Promise.all(tasks);
    }).then(function() {
      return { success: true };
    });
  };

  return {
    getDistrictsByRegionId: _getDistrictsByRegionId,
    getAllLocationNames: _getAllLocationNames,
    flagSocialUpdate: _flagSocialUpdate,
    unflagSocialUpdate: _unflagSocialUpdate,
    likeSocialUpdate: _likeSocialUpdate,
    unlikeSocialUpdate: _unlikeSocialUpdate,
    getSocialUpdates: _getSocialUpdates,
    deleteSocialUpdate: _deleteSocialUpdate
  };
};