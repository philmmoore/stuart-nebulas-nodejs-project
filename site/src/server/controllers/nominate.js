var Promise = require('bluebird');

module.exports = function NominateController(challengeService, repository, avatarImageUrl, validator, locationResolverService, messageQueueService, publicSite) {
  var NUMBER_OF_RESULTS_TO_SHOW_PER_PAGE = 20;
  
  var _addUser = function _addUser(items, nominationSuggestions, nominationSuggestionIds) {
    for (var index = 0; index < items.length; index++) {
      var follower = items[index];
      
      if (follower.avatarUploaded === '1') {
        follower.avatar = avatarImageUrl + follower.id + '-40x40.png';
        follower.avatarRetina = avatarImageUrl + follower.id + '-80x80.png';
      } else {
        follower.avatar = avatarImageUrl + '0-40x40.png';
        follower.avatarRetina = avatarImageUrl + '0-80x80.png';
      }
      
      if (nominationSuggestionIds.indexOf(follower.id) === -1) {
        nominationSuggestions.push(follower);
        nominationSuggestionIds.push(follower.id);
      }
    }
  };
  
  var _formatSuggestions = function _formatSuggestions(suggestions) {
    if (!suggestions) {
      return [];
    }
    
    for (var index = 0; index < suggestions.length; index++) {
      var suggestion = suggestions[index];
      
      if (suggestion.avatarUploaded === '1') {
        suggestion.avatar = avatarImageUrl + suggestion.id + '-40x40.png';
        suggestion.avatarRetina = avatarImageUrl + suggestion.id + '-80x80.png';
      } else {
        suggestion.avatar = avatarImageUrl + '0-40x40.png';
        suggestion.avatarRetina = avatarImageUrl + '0-80x80.png';
      }
    }
  };
  
  var _getChallenge = function _getChallenge(location, req) {
    return challengeService.getChallenge(location, req.params).then(function(data) {
      data.searchLink = '/gb/' + data.challenge.name.toLowerCase().replace(/ /g, '-') + '/nominate/';
      
      return repository.getNominationSuggestions(req.session.user.id, data.challenge.id).then(function(nominationSuggestions) {
        _formatSuggestions(nominationSuggestions);
        data.nominationSuggestions = nominationSuggestions;
        
        return data;
      });
    });
  };
  
  var _getData = function _getData(location, req) {
    var userNominatedSuccess = req.session.userNominatedSuccess;
    req.session.userNominatedSuccess = null;
    
    return _getChallenge(location, req).then(function(data) {
      data.userNominatedSuccess = userNominatedSuccess;
      return data;
    });
  };
  
  var _getSearchQueryUrl = function _getSearchQueryUrl(location, req) {
    var form = {
      userQuery: req.body.UserQuery ? req.body.UserQuery.trim() : null,
      locationQuery: req.body.LocationQuery ? req.body.LocationQuery.trim() : null
    };
    
    return challengeService.getChallenge(location, req.params).then(function(challenge) {
      return validator.validate(form).error(function(error) {
        return _getChallenge(location, req).then(function(data) {
          data.validationErrors = error.validationErrors;
          return Promise.reject(data);
        });
      }).then(function() {
        var url = '/gb/' + challenge.challenge.name.toLowerCase().replace(/ /g, '-') + '/nominate/search/';
        
        if (form.locationQuery) {
          url += 'location/' + form.locationQuery.toLowerCase() + '/';
        }
        
        if (form.userQuery) {
          url += 'user/' + form.userQuery.toLowerCase() + '/';
        }
        
        return url;
      });
    });
  };
  
  var _formatSearchResults = function _formatSearchResults(results, allowedNominees) {
    for (var index = 0; index < results.length; index++) {
      var result = results[index];
      
      if (result.avatarUploaded === '1') {
        result.avatar = avatarImageUrl + result.id + '-40x40.png';
        result.avatarRetina = avatarImageUrl + result.id + '-80x80.png';
      } else {
        result.avatar = avatarImageUrl + '0-40x40.png';
        result.avatarRetina = avatarImageUrl + '0-80x80.png';
      }
      
      result.canNominate = false;
      
      if (allowedNominees && allowedNominees.users) {
        for (var allowedNomineeIndex = 0; allowedNomineeIndex < allowedNominees.users.length; allowedNomineeIndex++) {
          if (result.id === allowedNominees.users[allowedNomineeIndex]) {
            result.canNominate = true;
            break;
          }
        }
      }      
    }
  };
  
  var _searchByUser = function _searchByUser(form) {
    return repository.searchByUser(form.userQuery, NUMBER_OF_RESULTS_TO_SHOW_PER_PAGE, form.page);
  };
  
  var _searchByLocation = function _searchByLocation(challengeData, form, userId) {
    return locationResolverService.getLocationFromName(form.locationQuery).then(function(resolvedLocation) {
      if (!resolvedLocation) {
        return repository.getNominationSuggestions(userId, challengeData.challenge.id).then(function(nominationSuggestions) {
          _formatSuggestions(nominationSuggestions);
          
          challengeData.nominationSuggestions = nominationSuggestions;
          challengeData.validationErrors = [ [ 'Location', 'Error_Field_Invalid_Location' ] ];
          challengeData.userQuery = form.userQuery;
          challengeData.locationQuery = form.locationQuery;
          challengeData.searchLink = '/gb/' + challengeData.challenge.link + '/nominate/';
          return Promise.reject(challengeData);
        });
      }
      
      return resolvedLocation;
    }).then(function(location) {
      return repository.searchByLocation(form.userQuery, location.districtId, location.countyId, location.regionId, location.countryId, NUMBER_OF_RESULTS_TO_SHOW_PER_PAGE, form.page);
    });
  };
  
  var _search = function _search(location, req) {
    var form = {
      userQuery: req.params.user ? req.params.user.trim() : null,
      locationQuery: req.params.location ? req.params.location.trim() : null,
      page: req.params.page || 1
    };
    
    var userId = req.session.user.id;
    
    return challengeService.getChallenge(location, req.params).then(function(data) {
      return validator.validate(form).then(function() {
        return [
          form.locationQuery ? _searchByLocation(data, form, userId) : _searchByUser(form),
          repository.getAllowedNominees(userId, data.challenge.id)
        ];
      }).spread(function(results, allowedNominees) {        
        if (results.results) {
          _formatSearchResults(results.results, allowedNominees);
          form.results = results.results;
          form.numberOfPages = results.numberOfPages;
        } else {
          form.noResultsFound = true;
        }
        
        form.challenge = data.challenge;
        form.searchLink = '/gb/' + data.challenge.link + '/nominate/';
        
        return form;
      });
    });
  };
  
  var _nominate = function _nominate(req, location) {
    return challengeService.getChallenge(location, req.params).then(function(challengeData) {
      var userId = req.session.user.id;
      
      return repository.nominate(userId, challengeData.challenge.id, req.params.username).then(function(nominationData) {
        if (!nominationData || !nominationData.email) {
          return Promise.resolve(nominationData);
        }
        
        var message = JSON.stringify({
          username: req.session.user.username,
          firstName: req.session.user.firstName,
          lastName: req.session.user.lastName,
          nomineeEmail: nominationData.email,
          nomineeFirstName: nominationData.firstName,
          nomineeLastName: nominationData.lastName,
          nomineeUserName: nominationData.username,
          challenge: challengeData.challenge.name,
          challengeLink: publicSite + 'gb/new/' + challengeData.challenge.link + '/'
        });

        return messageQueueService.sendMessage('nominate-user-email', message).then(function() {
          return nominationData;
        });
      }).then(function(nominationData) {
        if (nominationData) {
          req.session.userNominatedSuccess = nominationData.firstName + ' ' + nominationData.lastName;
        }
        
        return '/gb/' + challengeData.challenge.link + '/nominate/';
      });
    });
  };
  
  return {
    getData: _getData,
    getSearchQueryUrl: _getSearchQueryUrl,
    search: _search,
    nominate: _nominate
  };
};