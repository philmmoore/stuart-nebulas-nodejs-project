var stringFormatter = require('../utilities/string-formatter');

module.exports = function ChallengeRoute(router, controller) {
  router.get('/:country/leaderboards/:name/', function(req, res, next) {
    controller.getChallenge(res.locals.location, req).then(function(data) {
      data.pageUrl = req.path;
      if (data.groups.length === 1) {
        var url = stringFormatter.getUrlFriendlyString(req.path + data.groups[0].name + '/leaderboard/');
        res.redirect(url);
      } else {
        res.renderView('pages/challenge/details', data);
      }
    }).catch(function(err) {
      next();
    });
  });

  router.post('/:country/leaderboards/:name/', function(req, res) {
    controller.getLinkForChallenge(req, res.locals.location).then(function(link) {
      res.redirect(link);
    }).error(function(data) {
      res.renderView('pages/challenge/details', data);
    });
  });

  router.get('/:country/challenges/', function(req, res) {
    controller.getChallenges(req, res.locals.string).then(function(data) {
      data.pageUrl = req.path;
      res.renderView('pages/challenge/challenges', data);
    });
  });

  router.get('/:country/leaderboards/', function(req, res) {
    controller.getChallenges(req, res.locals.string).then(function(data) {
      data.pageUrl = req.path;
      res.renderView('pages/challenge/leaderboards', data);
    });
  });

};