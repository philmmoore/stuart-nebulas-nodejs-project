module.exports = function LeaderboardRoute(router, controller) {
  router.get('/:country/:region?/:county?/:district?/leaderboards/:name/:group/leaderboard/*', function(req, res, next) {
    controller.getLeaderboard(res.locals.location, req).then(function(data) {
      res.renderView('pages/challenge/leaderboard', data);
    }).catch(function(err) {
      next();
    });
  });

  router.post('/:country/:region?/:county?/:district?/leaderboards/:name/:group/leaderboard/*', function(req, res, next) {
    controller.getLeaderboardUrl(res.locals.location, req).then(function(url) {
      res.redirect(url);
    }).error(function(data) {
      res.renderView('pages/challenge/leaderboard', data);
    });
  });

};