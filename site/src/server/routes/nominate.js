module.exports = function NominateRoute(router, controller) {
  router.get('/:country/:name/nominate/', function(req, res, next) {
    controller.getData(res.locals.location, req).then(function(data) {
      res.renderView('pages/nominate/search', data);
    });
  });
  
  router.post('/:country/:name/nominate/', function(req, res) {
    controller.getSearchQueryUrl(res.locals.location, req).then(function(data) {
      return res.redirect(data);
    }).catch(function(data) {
      return res.renderView('pages/nominate/search', data);
    });
  });
  
  router.get('/:country/:name/nominate/search/user/:user/:page?/', function(req, res) {
    controller.search(res.locals.location, req).then(function(data) {
      return res.renderView('pages/nominate/search', data);
    }).catch(function(data) {
      return res.renderView('pages/nominate/search', data);
    });
  });
  
  router.get('/:country/:name/nominate/search/location/:location/:page?/', function(req, res) {
    controller.search(res.locals.location, req).then(function(data) {
      return res.renderView('pages/nominate/search', data);
    }).catch(function(data) {
      return res.renderView('pages/nominate/search', data);
    });
  });
  
  router.get('/:country/:name/nominate/search/location/:location/user/:user/:page?/', function(req, res) {
    controller.search(res.locals.location, req).then(function(data) {
      return res.renderView('pages/nominate/search', data);
    }).catch(function(data) {
      return res.renderView('pages/nominate/search', data);
    });
  });
  
  router.get('/:country/:name/nominate/:username/', function(req, res) {
    controller.nominate(req, res.locals.location).then(function(target) {
      res.redirect(target);
    });
  });
};