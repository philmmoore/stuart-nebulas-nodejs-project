module.exports = function GeoRedirectRoute(router, countriesAvailable, defaultCountry) {
  router.get('*', function(req, res, next) {
    // var countryCode = (req.headers['cf-ipcountry'] || defaultCountry).toLowerCase();
    var countryCode = defaultCountry.toLowerCase();
    var countryLandingPage = '/' + countryCode + '/';

    if (countriesAvailable.indexOf(countryCode) === -1) {
      if (req.url !== countryLandingPage) {
        return res.redirect(countryLandingPage);
      }

      return res.renderView('pages/not-available');
    }

    if (req.url.substring(0, 4) !== countryLandingPage) {
      return res.redirect(countryLandingPage);
    }

    next();
  });

  router.post('*', function(req, res, next) {
    // var countryCode = (req.headers['cf-ipcountry'] || defaultCountry).toLowerCase();
    var countryCode = defaultCountry.toLowerCase();
    var countryLandingPage = '/' + countryCode + '/';

    if (countriesAvailable.indexOf(countryCode) === -1) {
      if (req.url !== countryLandingPage) {
        return res.redirect(countryLandingPage);
      }

      return res.renderView('pages/not-available');
    }

    if (req.url.substring(0, 4) !== countryLandingPage) {
      return res.redirect(countryLandingPage);
    }
    
    next();
  });
};
