module.exports = function PageRoute(router, controller) {
  router.get('/:country/:name/', function(req, res, next) {
    controller.getPage(res.locals.location.countryId, req.params.name).then(function(data) {
      data.pageUrl = req.path;
      res.renderView('pages/page', data);
    }).catch(function(err) {
      next();
    });
  });
};
