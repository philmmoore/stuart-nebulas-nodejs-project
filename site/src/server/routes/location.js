module.exports = function LocationRoute(router, controller) {
  router.get('/:country/:region?/:county?/:district?/*', function(req, res, next) {
    controller.getLocation(req.params.country, req.params.region, req.params.county, req.params.district).then(function(location) {
      res.locals.location = location;
      next();
    });
  });

  router.post('/:country/:region?/:county?/:district?/*', function(req, res, next) {
    controller.getLocation(req.params.country, req.params.region, req.params.county, req.params.district).then(function(location) {
      res.locals.location = location;
      next();
    });
  });
};
