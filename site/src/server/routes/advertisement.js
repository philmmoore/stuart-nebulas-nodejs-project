module.exports = function AdvertisementRoute(router, controller) {
  router.get('/:country/:region?/:county?/:district?/*', function(req, res, next) {
    var location = res.locals.loggedInUser ? res.locals.loggedInUser.location : res.locals.location;
    controller.getAdvertisements(location).then(function(advertisements) {
      res.locals.advertisements = advertisements;
      next();
    });
  });

  router.post('/:country/:region?/:county?/:district?/*', function(req, res, next) {
    var location = res.locals.loggedInUser ? res.locals.loggedInUser.location : res.locals.location;
    controller.getAdvertisements(location).then(function(advertisements) {
      res.locals.advertisements = advertisements;
      next();
    });
  });
};
