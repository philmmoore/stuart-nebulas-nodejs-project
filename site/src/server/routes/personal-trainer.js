module.exports = function PersonalTrainerRoute(router, controller) {
  router.get('/:country/personal-trainers/', function(req, res) {
    return res.renderView('pages/personal-trainer/search');
  });
  
  router.post('/:country/personal-trainers/', function(req, res) {
    controller.getSearchQueryUrl(req).then(function(data) {
      return res.redirect(data);
    }).error(function(data) {
      return res.renderView('pages/personal-trainer/search', data);
    });
  });
  
  router.get('/:country/personal-trainers/user/:user/:page?/', function(req, res) {
    controller.search(req).then(function(data) {
      return res.renderView('pages/personal-trainer/search', data);
    }).error(function(data) {
      return res.renderView('pages/personal-trainer/search', data);
    });
  });
  
  router.get('/:country/personal-trainers/location/:location/:page?/', function(req, res) {
    controller.search(req).then(function(data) {
      return res.renderView('pages/personal-trainer/search', data);
    }).error(function(data) {
      return res.renderView('pages/personal-trainer/search', data);
    });
  });
  
  router.get('/:country/personal-trainers/location/:location/user/:user/:page?/', function(req, res) {
    controller.search(req).then(function(data) {
      return res.renderView('pages/personal-trainer/search', data);
    }).error(function(data) {
      return res.renderView('pages/personal-trainer/search', data);
    });
  });
};