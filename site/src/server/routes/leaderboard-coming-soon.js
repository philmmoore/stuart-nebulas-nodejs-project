module.exports = function LeaderboardRoute(router, isLeaderboardVisible) {
  router.get('*/leaderboard/*', function(req, res, next) {
    return isLeaderboardVisible ? next() : res.renderView('pages/challenge/leaderboard-coming-soon');
  });

  router.post('*/leaderboard/*', function(req, res, next) {
    return isLeaderboardVisible ? next() : res.renderView('pages/challenge/leaderboard-coming-soon');
  });
};
