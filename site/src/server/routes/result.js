var stringFormatter = require('../utilities/string-formatter');

module.exports = function ResultRoute(router, controller) {
  var _replaceSidebarLinkText = function _replaceSidebarLinkText(data, text) {
    var linkRegex = /(?:'#sidebar'.*\">)(.*?)(?:<\/a>)/g;
    var regexResult = linkRegex.exec(data.bodyCopy);
    
    if (regexResult && regexResult.length === 2) {
      data.bodyCopy = data.bodyCopy.replace(regexResult[1], text);
    }
  };
  
  var _removeSidebarLinkText = function _removeSidebarLinkText(data) {
    data.bodyCopy = data.bodyCopy.replace('<a href="#" class="visible-xs-inline" onclick=', '<a href="#" class="hidden" onclick=');
  };
  
  router.get('/:country/challenges/:name/', function(req, res, next) {
    controller.getChallenge(res.locals.location, req).then(function(data) {
      data.pageUrl = req.path;
      if (!res.locals.loggedInUser || data.notifications.length < 1) {
        data.showSidebar = true;
        _replaceSidebarLinkText(data.challenge, res.locals.string('Scroll_To_Submit_Result'));
      } else {
        _removeSidebarLinkText(data.challenge);
      }
      
      res.renderView('pages/challenge/submit-result', data);
    }).error(function(err) {
      next();
    });
  });

  router.post('/:country/challenges/:name/', function(req, res, next) {
    controller.submitResult(res.locals.location, req, 'none').then(function(data) {
      data.pageUrl = req.path;
      var url = stringFormatter.getUrlFriendlyString('/user/' + req.session.user.username + '/latest/' + data.challenge.name + '/' + data.selectedGroupName + '/');
      res.redirect(url);
    }).error(function(data) {
      if (data.notifications.length < 1) {
        data.showSidebar = true;
        _replaceSidebarLinkText(data.challenge, res.locals.string('Scroll_To_Submit_Result'));
      }
      
      res.renderView('pages/challenge/submit-result', data);
    });
  });

  router.post('/:country/challenges/:name/sign-upload/', function(req, res) {
    controller.signUploadRequest(res.locals.location, req).then(function(data) {
      res.json(data);
    }).catch(function(data) {
      res.status(400).json(data);
    });
  });
  
  router.get('/result/:resultId/', function(req, res, next) {
    controller.getResult(req).then(function(data) {
      res.renderView('pages/result/details', data);
    }).catch(function(err) {
      next();
    });
  });
  
  router.get('/result/:resultId/embed/', function(req, res, next) {
    controller.getVideoLink(req).then(function(videoLink) {
      res.renderView('pages/result/embed', { videoLink: videoLink });
    }).catch(function(err) {
      next();
    });
  });
};