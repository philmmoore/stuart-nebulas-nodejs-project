module.exports = function NotFoundRoute(router) {
  router.get('*', function(req, res) {
    res.renderView('pages/not-found');
  });
};
