module.exports = function UserRoute(router, controller, leaderboardService) {
  router.get('/user/:username/', function(req, res) {
    controller.getUser(req).then(function(data) {
      data.pageUrl = req.path;
      res.renderView('pages/user/initial', data);
    }).catch(function(err) {
      console.log(err);
      res.renderView(null, 404);
    });
  });

  router.post('/user/:username/', function(req, res) {
    controller.performAction(req).then(function(data) {
      data.pageUrl = req.path;
      res.renderView('pages/user/initial', data);
    }).catch(function(err) {
      res.renderView(null, 404);
    });
  });

  router.get('/user/:username/followers/', function(req, res) {
    controller.getUser(req).then(function(data) {
      data.pageUrl = req.path;
      res.renderView('pages/user/followers', data);
    }).catch(function(err) {
      res.renderView(null, 404);
    });
  });

  router.get('/user/:username/following/', function(req, res) {
    controller.getUser(req).then(function(data) {
      data.pageUrl = req.path;
      res.renderView('pages/user/following', data);
    }).catch(function(err) {
      res.renderView(null, 404);
    });
  });

  router.get('/user/:username/clients/', function(req, res) {
    controller.getUser(req).then(function(data) {
      data.pageUrl = req.path;
      res.renderView('pages/user/clients', data);
    }).catch(function(err) {
      res.renderView(null, 404);
    });
  });
  
  router.get('/user/:username/posts/', function(req, res) {
    req.showOnlyMine = true;
    controller.getUser(req).then(function(data) {
      data.pageUrl = req.path;
      res.renderView('pages/user/posts', data);
    }).catch(function(err) {
      res.renderView(null, 404);
    });
  });
  
  router.get('/user/:username/feed/', function(req, res) {
    req.showOnlyMine = false;
    controller.getUser(req).then(function(data) {
      data.pageUrl = req.path;
      // Only allow the logged in user to view the global user feed
      if (!req.session || !req.session.user || req.session.user.username !== req.params.username) {
        res.redirect('/user/' + req.params.username + '/');
      } else {
        res.renderView('pages/user/posts', data);      
      }
    }).catch(function(err) {
      res.renderView(null, 404);
    });
  });
  
  router.get('/user/:username/reviews/page/:page/', function(req, res) {
    controller.getUser(req).then(function(data) {
      data.pageUrl = req.path;
      data.isContentPage = true;
      // We want the index value of the page so we minus 1
      data.pageIndex = parseInt(req.params.page) - 1;
      data.page = parseInt(req.params.page);
      data.pageTitle = data.user.firstName + " " + data.user.lastName + "<strong>Reviews</strong>";
      res.renderView('pages/user/reviews', data);
    }).catch(function(err) {
      console.log(err);
      res.renderView(null, 404);
    });
  });

  router.post('/user/:username/reviews/leave-review/', function(req, res) {
    controller.getUser(req).then(function(data) {
      data.pageUrl = req.path;
      data.isContentPage = true;
      data.pageTitle = "Leave a review for <strong>" + data.user.firstName + " " + data.user.lastName + "</strong>";
      data.rating = req.body.rating;

      // If the user is posting their review we should handle that action
      if (req.body.userId){
        controller.leaveReview(req.session.user.id, req.body).then(function(response){
          res.redirect('/user/' + req.params.username + '/');
        }).catch(function(response){
          data.validationErrors = response.validationErrors;
          res.renderView('pages/user/leave-review', data);
        });
      } else {
        res.renderView('pages/user/leave-review', data);
      }

    }).catch(function(err) {
      console.log(err);
      res.renderView(null, 404);
    });
  });

  router.get('/user/:username/update/*', function(req, res, next) {
    var username = req.params.username;
    if (!req.session || !req.session.user || req.session.user.username !== req.params.username) {
      res.redirect('/user/' + username + '/');
    } else {
      next();
    }
  });
  
  router.post('/user/:username/update/*', function(req, res, next) {
    var username = req.params.username;
    if (!req.session || !req.session.user || req.session.user.username !== req.params.username) {
      res.send(403).end();
    } else {
      next();
    }
  });
  
  router.get('/user/:username/update/', function(req, res) {
    controller.getUser(req).then(function(data) {
      res.renderView('pages/user/edit', data);
    }).error(function(data) {
      req.redirect('/login/');
    });
  });
  
  router.post('/user/:username/update/sign-upload/', function(req, res) {
    controller.signUploadRequest(req).then(function(data) {
      res.json(data);
    }).catch(function(data) {
      res.status(400).json(data);
    });
  });
  
  router.get('/user/:username/update/height/', function(req, res) {
    res.renderView('pages/user/edit-height');
  });
  
  router.post('/user/:username/update/height/', function(req, res) {
    controller.updateHeight(req).then(function() {
      res.redirect('../');
    }).error(function(data) {
      res.renderView('pages/user/edit-height', data);
    });
  });
  
  router.get('/user/:username/update/weight/', function(req, res) {
    res.renderView('pages/user/edit-weight');
  });
  
  router.post('/user/:username/update/weight/', function(req, res) {
    controller.updateWeight(req).then(function() {
      res.redirect('../');
    }).error(function(data) {
      res.renderView('pages/user/edit-weight', data);
    });
  });
  
  router.get('/user/:username/update/location/', function(req, res) {
    controller.getLocationData(req).then(function(data) {
      res.renderView('pages/user/edit-location', data);
    });
  });
  
  router.post('/user/:username/update/location/', function(req, res) {
    controller.updateLocation(req).then(function(data) {
      res.redirect('../');
    }).error(function(data) {
      res.renderView('pages/user/edit-location', data);
    });
  });
  
  router.get('/user/:username/update/routine/', function(req, res) {
    controller.getRoutineTypes(req).then(function(data) {
      res.renderView('pages/user/edit-routine', data);
    });
  });
  
  router.post('/user/:username/update/routine/', function(req, res) {
    controller.updateRoutine(req).then(function() {
      res.redirect('../');
    }).error(function(data) {
      res.renderView('pages/user/edit-routine', data);
    });
  });
  
  router.get('/user/:username/update/gym/', function(req, res) {
    controller.getUser(req).then(function(data) {
      return res.renderView('pages/user/edit-gym', data);
    });
  });
  
  router.post('/user/:username/update/gym/', function(req, res) {
    controller.updateGym(req).then(function() {
      res.redirect('../');
    }).error(function(data) {
      return res.renderView('pages/user/edit-gym', data);
    });
  });
  
  router.get('/user/:username/update/privacy/', function(req, res) {
    controller.getPrivacyPreferences(req).then(function(data) {
      return res.renderView('pages/user/edit-privacy-preferences', data);
    });
  });
  
  router.post('/user/:username/update/privacy/', function(req, res) {
    controller.setUserPrivacyPreferences(req).then(function() {
      res.redirect('../');
    });
  });
  
  router.get('/user/:username/update/notifications/', function(req, res) {
    controller.getNotificationPreferences(req).then(function(data) {
      return res.renderView('pages/user/edit-notification', data);
    });
  });
  
  router.post('/user/:username/update/notifications/', function(req, res) {
    controller.setNotificationPreferences(req).then(function() {
      res.redirect('../');
    });
  });
  
  router.get('/user/:username/latest/:challengeName/:groupName/', function(req, res) {
    var params = {
      username: req.params.username,
      challengeName: req.params.challengeName,
      challengeGroupName: req.params.groupName
    };
    
    leaderboardService.getUrl(params).then(function(url) {
      res.redirect(url);
    });
  });
};