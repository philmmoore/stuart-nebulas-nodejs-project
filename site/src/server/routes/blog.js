module.exports = function BlogRoute(router, controller) {
  router.get('/:country/blog/', function(req, res, next) {
    controller.getPosts(res.locals.location).then(function(data) {
      res.renderView('pages/blog/list', data);
    });
  });

  router.get('/:country/blog/:pageNumber([0-9]+)/', function(req, res, next) {
    controller.getPosts(res.locals.location, req.params.pageNumber).then(function(data) {
      res.renderView('pages/blog/list', data);
    }).catch(function() {
      res.renderView(null, 404);
    });
  });

  router.get('/:country/blog/:year/:month/:name/', function(req, res, next) {
    controller.getPost(res.locals.location.countryId, req.params.name).then(function(data) {
      res.renderView('pages/blog/post', data);
    }).catch(function() {
      res.renderView(null, 404);
    });
  });
};
