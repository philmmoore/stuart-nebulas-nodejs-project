module.exports = function SearchRoute(router, controller) {
  router.get('/search/', function(req, res) {
    return res.renderView('pages/search/search');
  });
  
  router.post('/search/', function(req, res) {
    controller.getSearchQueryUrl(req).then(function(data) {
      // Because this was setup with a redirect we need to give the redirect some context
      // we'll do that by creating a search session that'll retain the users search preference
      req.session.search = req.body;
      return res.redirect(data);
    }).error(function(data) {
      return res.renderView('pages/search/search', data);
    });
  });
  
  // Temporarily disabling user search
  //
  // router.get('/search/user/:user/:page?/', function(req, res) {
  //   controller.search(req).then(function(data) {
  //     return res.renderView('pages/search/search', data);
  //   }).error(function(data) {
  //     return res.renderView('pages/search/search', data);
  //   });
  // });
  // 
  // router.get('/search/location/:location/user/:user/:page?/', function(req, res) {
  //   controller.search(req).then(function(data) {
  //     return res.renderView('pages/search/search', data);
  //   }).error(function(data) {
  //     return res.renderView('pages/search/search', data);
  //   });
  // });
  
  router.get('/search/location/:location/:page?/', function(req, res) {

	// If the user has previously performed a search then use their defaults
	var dataSearch;
	if (req.session.search){
		dataSearch = req.session.search;
	}

	controller.search(req).then(function(data) {
		if (dataSearch){
			data.search = dataSearch;
		}
		console.log(data);
		return res.renderView('pages/search/search', data);
    }).error(function(data) {
      return res.renderView('pages/search/search', data);
    });
  });

};