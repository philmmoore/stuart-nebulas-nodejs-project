var stringFormatter = require('../utilities/string-formatter');

module.exports = function SocialRoute(router, controller) {
  router.get('/update/:updateId/', function(req, res, next) {
    controller.getUpdate(req, res.locals.string).then(function(data) {
      res.renderView('pages/social/details', data);
    }).catch(function(err) {
      next();
    });
  });
  
  router.get('/update/:updateId/embed/', function(req, res, next) {
    controller.getUpdate(req, res.locals.string).then(function(data) {
      res.renderView('pages/social/embed', data);
    }).catch(function(err) {
      next();
    });
  });
};