module.exports = function APIRoute(router, controller, logger) {
  router.get('/api/location/districts/:regionId/', function(req, res) {
    controller.getDistrictsByRegionId(req.params.regionId).then(function(data) {
      res.json(data);
    }).catch(function(data) {
      logger.log('gymfit-error: /api/location/districts/:regionId/', data);
      res.status(400).json(data);
    });
  });
  
  router.get('/api/locations/', function(req, res) {
    controller.getAllLocationNames().then(function(data) {
      res.json(data);
    });
  });

  router.get('/api/social/updates/since/:timestamp/:userId/:showOnlyMine?', function(req, res) {
    controller.getSocialUpdates(req).then(function(data) {
      res.renderView('pages/api/updates', data);
    }).catch(function(data) {
      logger.log('gymfit-error: /api/social/updates/since/:timestamp/:userId/', data);
      res.status(400).json(data);
    });
  });
  
  router.get('/api/social/update/:updateId/flag/', function(req, res) {
    controller.flagSocialUpdate(req).then(function(data) {
      res.json(data);
    }).catch(function(data) {
      logger.log('gymfit-error: /api/social/update/:updateId/flag/', data);
      res.status(400).json(data);
    });
  });
  
  router.get('/api/social/update/:updateId/unflag/', function(req, res) {
    controller.unflagSocialUpdate(req).then(function(data) {
      res.json(data);
    }).catch(function(data) {
      logger.log('gymfit-error: /api/social/update/:updateId/unflag/', data);
      res.status(400).json(data);
    });
  });
  
  router.get('/api/social/update/:updateId/like/', function(req, res) {
    controller.likeSocialUpdate(req).then(function(data) {
      res.json(data);
    }).catch(function(data) {
      logger.log('gymfit-error: /api/social/update/:updateId/like/', data);
      res.status(400).json(data);
    });
  });
  
  router.get('/api/social/update/:updateId/unlike/', function(req, res) {
    controller.unlikeSocialUpdate(req).then(function(data) {
      res.json(data);
    }).catch(function(data) {
      logger.log('gymfit-error: /api/social/update/:updateId/unlike/', data);
      res.status(400).json(data);
    });
  });
  
  router.get('/api/social/update/:updateId/delete/', function(req, res) {
    controller.deleteSocialUpdate(req).then(function(data) {
      res.json(data);
    }).catch(function(data) {
      logger.log('gymfit-error: /api/social/update/:updateId/delete/', data);
      res.status(400).json(data);
    });
  });
};