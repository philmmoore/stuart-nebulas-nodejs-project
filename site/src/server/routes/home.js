module.exports = function HomeRoute(router, controller) {
  router.get('/:country/', function(req, res) {
    controller.getData(req, res.locals).then(function(data) {
      
      if (!res.locals.loggedInUser) {
        data.advertisements = null;
      }
      
      res.renderView('pages/home', data);
    });
  });
};
