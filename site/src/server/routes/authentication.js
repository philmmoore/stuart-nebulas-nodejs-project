module.exports = function AuthenticationRoute(router, controller) {
  router.get('/login/', function(req, res) {
    if (req.session.user) {
      return res.redirect('/user/' + req.session.user.username + '/');
    }

    res.renderView('pages/authentication/login');
  });

  router.post('/login/', function(req, res) {
    controller.authenticate(req).then(function(location) {
      res.redirect(location);
    }).error(function(data) {
      res.renderView('pages/authentication/login', data);
    });
  });

  router.get('/logout/', function(req, res) {
    controller.logout(req.session);
    res.redirect('/');
  });

  router.get('/reset-password/', function(req, res) {
    res.renderView('pages/authentication/reset-password');
  });

  router.post('/reset-password/', function(req, res) {
    controller.resetPassword(req).then(function(data) {
      res.renderView('pages/authentication/reset-password-email-sent');
    }).error(function(data) {
      res.renderView('pages/authentication/reset-password', data);
    });
  });

  router.get('/reset-password/:key/', function(req, res) {
    controller.isPasswordResetKeyValid(req.params.key).then(function(data) {
      res.renderView('pages/authentication/new-password');
    }).catch(function() {
      res.redirect('/login/');
    });
  });

  router.post('/reset-password/:key/', function(req, res) {
    controller.performPasswordReset(req, req.params.key).then(function(user) {
      res.redirect('/user/' + user + '/');
    }).error(function(data) {
      res.renderView('pages/authentication/new-password', data);
    });
  });

  router.get('/confirm-email/:key/', function(req, res) {
    controller.confirmEmail(req).then(function(user) {
      res.redirect('/user/' + user + '/');
    }).catch(function() {
      res.redirect('/login/');
    });
  });
};
