module.exports = function LoginEnforcerRoute(router, controller) {
  var _enforce = function _enforce(req, res, next, title, message) {
    if (req.session && req.session.user) {
      return next();
    }

    var data = {
      loginReasonTitle: title,
      loginReasonMessage: message,
      redirectOnSuccess: req.url
    };

    res.renderView('pages/authentication/login', data);
  };
  
  // Enforce login to view user profiles
  // router.get('/user/*', function(req, res, next) {
  //   _enforce(req, res, next, 'Login_Required_To_View_Profiles_Title', 'Login_Required_To_View_Profiles_Message');
  // });
  
  // router.post('/user/*', function(req, res, next) {
  //   _enforce(req, res, next, 'Login_Required_To_View_Profiles_Title', 'Login_Required_To_View_Profiles_Message');
  // });

  // router.get('*/leaderboard/*', function(req, res, next) {
  //   _enforce(req, res, next, 'Login_Required_To_View_Leaderboard_Title', 'Login_Required_To_View_Leaderboard_Message');
  // });

  // router.post('*/leaderboard/*', function(req, res, next) {
  //   _enforce(req, res, next, 'Login_Required_To_View_Leaderboard_Title', 'Login_Required_To_View_Leaderboard_Message');
  // });
};
