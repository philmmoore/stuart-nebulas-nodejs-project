var markdown = require('markdown').markdown;

var videoIFrame = '<div class="object video"><iframe src="https://www.youtube.com/embed/{video}?rel=0" allowfullscreen></iframe></div>';

var _processVideos = function _processVideos(text) {
  var regex = /<p>((http(s)?:\/\/)?(www.)?youtu(?:\.be|be\.com)\/(?:.*v(?:\/|=)|(?:.*\/)?)([\w'-]+))<\/p>/ig;
  var matches = text.match(regex);

  if (matches === null) {
    return text;
  }

  for (var index = 0; index < matches.length; index++) {

    var match = matches[index];

    var videoString = text.substr(text.indexOf(match), match.length);

    var videoRegex = /<p>(.*?)<\/p>/ig;
    var videoUrl = videoRegex.exec(videoString);
    videoUrl = videoUrl[1];

    var videoId = videoUrl.split('&')[0];
    videoId = videoId.replace('watch?v=', '');
    videoId = videoId.split('/')[videoId.split('/').length - 1];

    text = text.replace(videoString, videoIFrame.replace('{video}', videoId));

  }

  return text;

};

var _processFeatures = function _processFeatures(text) {
  var regex = /href="gymfit-feature:.*"/ig;
  var matches = text.match(regex);

  if (matches === null) {
    return text;
  }

  for(var index = 0; index < matches.length; index++) {
    var match = matches[index];
    text = text.replace(match, 'href="#" class="visible-xs-inline" onclick="$(\'html, body\').animate({scrollTop: $(\'#sidebar\').offset().top - 74}, 300);$(\'#sidebar input\').focus();$(\'#sidebar select\').focus();return false;"');
  }

  return text;
};

var _processImages = function _processImages(text) {
  var regex = /<img /ig;
  var matches = text.match(regex);

  if (matches === null) {
    return text;
  }

  for(var index = 0; index < matches.length; index++) {
    var match = matches[index];
    text = text.replace(match, '<img class="img-responsive" ');
  }

  return text;
};

var _format = function _format(text) {
  var markdownFormattedText = markdown.toHTML(text);

  markdownFormattedText = _processVideos(markdownFormattedText);
  markdownFormattedText = _processFeatures(markdownFormattedText);
  markdownFormattedText = _processImages(markdownFormattedText);
  
  return markdownFormattedText;
};

module.exports.format = _format;
