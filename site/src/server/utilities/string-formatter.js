module.exports.getUrlFriendlyString = function _getUrlFriendlyString(string) {
  if (!string) {
    return string;
  }

  return string.trim().toLowerCase().replace(/ /g, '-');
};