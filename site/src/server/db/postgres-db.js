var Promise = require('bluebird');
var pg = require('pg');
var QueryJson = require('./query-json');

module.exports = function PostgresDB(config, logger) {
  pg.defaults.poolSize = config.postgresPoolSize;
  
  var _connect = function _connect() {
    return new Promise(function(resolve, reject) {
      logger.log('Connecting to Postgres...');

      pg.connect(config.postgresURL, function(err, client, done) {
        if (err) {
          return reject(err);
        }
        
        done();

        logger.log('Connected to Postgres.');
        resolve(new QueryJson(pg, config.postgresURL, config.isProduction, logger));
      });
    });
  };

  return {
    connect: _connect
  };
};
