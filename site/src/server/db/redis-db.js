var redis = require('redis');
var url =  require('url');

module.exports = function RedisDB(connectionString, logger) {
  var redisUrl = url.parse(connectionString);

  var _connect = function _connect(success) {
    logger.log('Connecting to Redis...');

    var options = {};


    var client = redis.createClient(redisUrl.port, redisUrl.hostname, options);

    if (redisUrl.auth) {
      client.auth(redisUrl.auth.split(':')[1]);
    }

    client.on('error', function (err) {
      return logger.error('RedisDB:' + err);
    });

    client.on('ready', function () {
      logger.log('Connected to Redis.');
      success(client);
    });
  };

  return {
    connect: _connect
  };
};
