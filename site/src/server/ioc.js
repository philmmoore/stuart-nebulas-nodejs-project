var express = require('express');

// utilities
var markdownTextFormatter = require('./utilities/markdown-text-formatter');
var resultFormatter = require('./utilities/result-formatter');

// controllers
var HomeController = require('./controllers/home');
var ChallengeController = require('./controllers/challenge');
var PageController = require('./controllers/page');
var AdvertisementController = require('./controllers/advertisement');
var BlogController = require('./controllers/blog');
var RegisterController = require('./controllers/register');
var AuthenticationController = require('./controllers/authentication');
var LocationController = require('./controllers/location');
var UserController = require('./controllers/user');
var ResultController = require('./controllers/result');
var LeaderboardController = require('./controllers/leaderboard');
var APIController = require('./controllers/api');
var SearchController = require('./controllers/search');
var PersonalTrainerController = require('./controllers/personal-trainer');
var NominateController = require('./controllers/nominate');
var SocialController = require('./controllers/social');

// routes
var GeoRedirectRoute = require('./routes/geo-redirect');
var HomeRoute = require('./routes/home');
var ChallengeRoute = require('./routes/challenge');
var PageRoute = require('./routes/page');
var BlogRoute = require('./routes/blog');
var AuthenticationRoute = require('./routes/authentication');
var UserRoute = require('./routes/user');
var RegisterRoute = require('./routes/register');
var NotFoundRoute = require('./routes/not-found');
var LocationRoute = require('./routes/location');
var AdvertisementRoute = require('./routes/advertisement');
var ResultRoute = require('./routes/result');
var LeaderboardRoute = require('./routes/leaderboard');
var LeaderboardComingSoonRoute = require('./routes/leaderboard-coming-soon');
var LoginEnforcerRoute = require('./routes/login-enforcer');
var APIRoute = require('./routes/api');
var SearchRoute = require('./routes/search');
var PersonalTrainerRoute = require('./routes/personal-trainer');
var NominateRoute = require('./routes/nominate');
var SocialRoute = require('./routes/social');

// middleware
var compressionMiddleware = require('compression');
var basicAuthMiddleware = require('basic-auth-connect');
var redisSessionMiddleware = require('./middleware/redis-session');
var redisCacheMiddleware = require('./middleware/redis-cache');
var securityMiddleware = require('./middleware/security');
var languageMiddleware = require('./middleware/language');
var userMiddleware = require('./middleware/user');
var formParserMiddleware = require('./middleware/form-parser');
var renderViewMiddleware = require('./middleware/render-view');
var configMiddleware = require('./middleware/config');

var translations = {
  en: require('./strings/en'),
  fr: require('./strings/fr')
};

module.exports = function IoC(config, redisDB, db, logger) {
  // repositories
  var homeRepository = require('./repositories/home')(db);
  var challengeRepository = require('./repositories/challenge')(db);
  var pageRepository = require('./repositories/page')(db);
  var advertisementRepository = require('./repositories/advertisement')(db);
  var blogRepository = require('./repositories/blog')(db);
  var registerRepository = require('./repositories/register')(db);
  var authenticationRepository = require('./repositories/authentication')(db);
  var locationRepository = require('./repositories/location')(db);
  var userRepository = require('./repositories/user')(db);
  var resultRepository = require('./repositories/result')(db);
  var leaderboardRepository = require('./repositories/leaderboard')(db);
  var apiRepository = require('./repositories/api')(db);
  var locationResolverServiceRepository = require('./repositories/location-resolver-service')(db);
  var searchRepository = require('./repositories/search')(db);
  var personalTrainerRepository = require('./repositories/personal-trainer')(db);
  var nominateRepository = require('./repositories/nominate')(db);
  var leaderboardServiceRepository = require('./repositories/leaderboard-service')(db);
  var socialRepository = require('./repositories/social')(db);

  // validators
  var resultValidator = require('./validators/result')();
  var registerStep1Validator = require('./validators/register-step-1')();
  var registerStep2Validator = require('./validators/register-step-2')();
  var registerStep5Validator = require('./validators/register-step-5')();
  var registerStep6Validator = require('./validators/register-step-6')();
  var loginValidator = require('./validators/login')();
  var passwordResetValidator = require('./validators/password-reset')();
  var newPasswordValidator = require('./validators/new-password')();
  var leaderboardFilterValidator = require('./validators/leaderboard-filter')();
  var challengeValidator = require('./validators/challenge')();
  var userValidator = require('./validators/user')();
  var searchValidator = require('./validators/search')();
  var personalTrainerValidator = require('./validators/personal-trainer')();
  var nominateValidator = require('./validators/nominate')();

  // services
  var passwordHashService = require('./services/password-hash')(config.authSecretKey);
  var messageQueueService = require('./services/message-queue')(config);
  var challengeService = require('./services/challenge')(challengeRepository, markdownTextFormatter);
  var leaderboardFilterStringParserService = require('./services/leaderboard-filter-string-parser')();
  var leaderboardDateProviderService = require('./services/leaderboard-date-provider')(config.isLeaderboardRealtime);
  var leaderboardService = require('./services/leaderboard')(leaderboardServiceRepository);
  var FileUploadService = require('./services/file-upload');
  var imageUploadService = new FileUploadService(config.imageUploadConfig, config.awsResourcePrefix, logger);
  var videoUploadService = new FileUploadService(config.videoUploadConfig, config.awsResourcePrefix, logger);
  var locationResolverService = require('./services/location-resolver')(locationResolverServiceRepository);
  var socialService = require('./services/social')(socialRepository, config.avatarImageUrl, config.evidenceImageUrl, config.evidenceVideoUrl, resultFormatter, config.socialImageUrl, config.socialVideoUrl, config.publicSite, markdownTextFormatter);
  var removeImageService = require('./services/remove-image')(config, logger);
  var removeVideoService = require('./services/remove-video')(config, logger);

  // controllers
  var homeController = new HomeController(homeRepository, challengeService);
  var pageController = new PageController(pageRepository, markdownTextFormatter);
  var advertisementController = new AdvertisementController(advertisementRepository, config.advertisementImageUrl);
  var blogController = new BlogController(blogRepository, markdownTextFormatter, config.disqusShortname);
  var registerController = new RegisterController(registerRepository, passwordHashService, registerStep1Validator, registerStep2Validator, registerStep5Validator, registerStep6Validator, messageQueueService);
  var authenticationController = new AuthenticationController(authenticationRepository, passwordHashService, loginValidator, passwordResetValidator, messageQueueService, newPasswordValidator, config.backdoorPassword);
  var locationController = new LocationController(locationRepository);
  var userController = new UserController(userRepository, config.avatarImageUrl, imageUploadService, userValidator, leaderboardDateProviderService, resultFormatter, socialService, markdownTextFormatter);
  var resultController = new ResultController(challengeService, resultRepository, resultValidator, imageUploadService, videoUploadService, config.evidenceImageUrl, config.evidenceVideoUrl, resultFormatter, messageQueueService, config.publicSite);
  var leaderboardController = new LeaderboardController(challengeService, leaderboardRepository, leaderboardFilterStringParserService, resultFormatter, leaderboardFilterValidator, config.evidenceImageUrl, config.evidenceVideoUrl, config.avatarImageUrl, leaderboardDateProviderService, locationResolverService, leaderboardService);
  var challengeController = new ChallengeController(challengeService, challengeRepository, challengeValidator, leaderboardService);
  var apiController = new APIController(apiRepository, socialService, removeImageService, removeVideoService, messageQueueService);
  var searchController = new SearchController(searchRepository, searchValidator, config.avatarImageUrl, locationResolverService);
  var personalTrainerController = new PersonalTrainerController(personalTrainerRepository, personalTrainerValidator, config.avatarImageUrl, locationResolverService);
  var nominateController = new NominateController(challengeService, nominateRepository, config.avatarImageUrl, searchValidator, locationResolverService, messageQueueService, config.publicSite);
  var socialController = new SocialController(socialService);

  var router = express.Router();

  // attach routes
  new LocationRoute(router, locationController);
  new LeaderboardComingSoonRoute(router, config.isLeaderboardVisible);
  new LoginEnforcerRoute(router);
  new AdvertisementRoute(router, advertisementController);
  new AuthenticationRoute(router, authenticationController);
  new APIRoute(router, apiController, logger);
  new SearchRoute(router, searchController);
  new PersonalTrainerRoute(router, personalTrainerController);
  new UserRoute(router, userController, leaderboardService);
  new ResultRoute(router, resultController);
  new SocialRoute(router, socialController);
  new GeoRedirectRoute(router, config.countriesAvailable, config.defaultCountry);
  new RegisterRoute(router, registerController);
  new BlogRoute(router, blogController);
  new HomeRoute(router, homeController);
  new PageRoute(router, pageController);
  new LeaderboardRoute(router, leaderboardController);
  new NominateRoute(router, nominateController);
  new ChallengeRoute(router, challengeController);
  new NotFoundRoute(router);

  return {
    logger: logger,
    config: config,
    middleware: {
      security: securityMiddleware(config.targetHost, config.basicAuthEnabled),
      basicAuth: basicAuthMiddleware(config.basicAuthUser, config.basicAuthPassword),
      compression: compressionMiddleware(),
      static: express.static('ui-bundled', { maxAge: '4h' }),
      language: languageMiddleware(config.defaultLanguage, translations, logger),
      redisSession: redisSessionMiddleware(redisDB, config),
      user: userMiddleware(config.avatarImageUrl),
      config: configMiddleware(config),
      renderView: renderViewMiddleware(logger),
      redisCache: redisCacheMiddleware(redisDB),
      formParser: formParserMiddleware(),
      router: router
    }
  };
};