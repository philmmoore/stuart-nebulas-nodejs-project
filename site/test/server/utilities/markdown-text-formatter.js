var assert = require('chai').assert;
var formatter = require('../../../src/server/utilities/markdown-text-formatter');

describe('challenge-body-text-formatter', function() {
  describe('#format()', function() {

    it('Should format the text as markdown', function() {
      var text = '## Testing';
      var result = formatter.format(text);

      assert.equal(result, '<h2>Testing</h2>');
    });

    it('Should replace youtu.be video links with player controls', function() {
      var text = 'https://youtu.be/JZQA08SlJnM';
      var result = formatter.format(text);

      assert.equal(result, '<div class="object video"><iframe src="https://www.youtube.com/embed/JZQA08SlJnM?rel=0" allowfullscreen></iframe></div>');
    });

    it('Should replace YouTube video links with player controls', function() {
      var text = 'https://www.youtube.com/watch?v=gRVjAtPip0Y';
      var result = formatter.format(text);

      assert.equal(result, '<div class="object video"><iframe src="https://www.youtube.com/embed/gRVjAtPip0Y?rel=0" allowfullscreen></iframe></div>');
    });

  });
});
