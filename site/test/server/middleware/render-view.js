var assert = require('chai').assert;
var renderView = require('../../../src/server/middleware/render-view');

describe('render view middleware', function() {
  describe('#handle()', function() {
    it('should attach a renderView method to req that will call req.render and pass the view name to the template, and invoke next', function() {
      var req = null;
      var renderResult = null;

      var res = {
        render: function(viewName, data) {
          renderResult = {
            viewName: viewName,
            data: data
          };
        }
      };

      var nextWasInvoked = false;
      var next = function() {
        nextWasInvoked = true;
      };

      var middleware = renderView();
      middleware(req, res, next);

      assert.isTrue(nextWasInvoked);

      var viewName = 'test';
      var data = {
        test: true
      };

      res.renderView(viewName, data);

      assert.deepEqual(renderResult, {
        viewName: viewName,
        data: {
          test: true,
          viewName: viewName
        }
      });
    });
  });

  describe('#handle()', function() {
    it('calling renderView should add the viewname to the data even if it is null', function() {
      var req = null;
      var renderResult = null;

      var res = {
        render: function(viewName, data) {
          renderResult = {
            viewName: viewName,
            data: data
          };
        }
      };

      var nextWasInvoked = false;
      var next = function() {
        nextWasInvoked = true;
      };

      var middleware = renderView();
      middleware(req, res, next);

      assert.isTrue(nextWasInvoked);

      var viewName = 'test';
      var data = null;

      res.renderView(viewName, data);

      assert.deepEqual(renderResult, {
        viewName: viewName,
        data: {
          viewName: viewName
        }
      });
    });
  });
});
