var assert = require('chai').assert;
var formParser = require('../../../src/server/middleware/form-parser');

describe('form parser middleware', function() {
  describe('#handle()', function() {
    it('should ignore GET requests and invoke next', function() {
      var req = {
        method: 'GET'
      };
      var res = null;

      var nextWasInvoked = false;
      var next = function() {
        nextWasInvoked = true;
      };

      var middleware = formParser();
      middleware(req, res, next);

      assert.isTrue(nextWasInvoked);
    });
  });
});
