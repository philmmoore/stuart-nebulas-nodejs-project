var assert = require('chai').assert;
var redisCache = require('../../../src/server/middleware/redis-cache');

describe('redis cache middleware', function() {
  describe('#handle()', function() {
    it('given url exists in cache should render the correct template and finish processing', function() {
      var expectedCacheKey = 'cache:/testing/test';
      var expectedView = 'view';
      var expectedData = {
        success: true
      };

      var redisDB = {
        get: function(query, callback) {
          assert.equal(expectedCacheKey, query);

          return callback(undefined, JSON.stringify({
            view: expectedView,
            data: expectedData
          }));
        }
      };
      var req = {
        url: '/testing/test'
      };
      var renderWasInvoked = false;
      var res = {
        render: function(view, data) {
          renderWasInvoked = true;
          assert.equal(expectedView, view);
          assert.deepEqual(expectedData, data);
        }
      };
      var nextWasInvoked = false;
      var next = function() {
        nextWasInvoked = true;
      };
      var middleware = redisCache(redisDB);

      middleware(req, res, next);

      assert.isTrue(renderWasInvoked);
      assert.isFalse(nextWasInvoked);
    });

    it('given url does not exist in cache should do nothing and pass control to the next middleware plugin', function() {
      var expectedCacheKey = 'cache:/testing/test';
      var redisDB = {
        get: function(query, callback) {
          assert.equal(expectedCacheKey, query);

          return callback(null, null);
        }
      };
      var req = {
        url: '/testing/test'
      };
      var renderWasInvoked = false;
      var res = {
        render: function(view, data) {
          renderWasInvoked = true;
        }
      };
      var nextWasInvoked = false;
      var next = function() {
        nextWasInvoked = true;
      };
      var middleware = redisCache(redisDB);

      middleware(req, res, next);

      assert.isTrue(nextWasInvoked);
      assert.isFalse(renderWasInvoked);
    });
  });
});
