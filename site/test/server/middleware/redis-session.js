var assert = require('chai').assert;
var sinon = require('sinon');
var redis = require('fakeredis');
var redisSession = require('../../../src/server/middleware/redis-session');

describe('redis session', function() {
  describe('#handle()', function() {
    it('should not error', function() {
      var redisDB = redis.createClient();
      var config = {
        sessionSecretKey: 'secret'
      };

      var server = redisSession(redisDB, config);
    });
  });
});
