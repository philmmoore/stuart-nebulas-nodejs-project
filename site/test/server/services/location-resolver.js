var assert = require('chai').assert;
var Promise = require('bluebird');

var LocationResolverService = require('../../../src/server/services/location-resolver');

describe('Location Resolver Service', function() {
  describe('#resolveFromName()', function() {
    var repositoryData =
    [
      {
        district: 'Salford',
        districtId: 23,
        county: 'Greater Manchester',
        countyId: 124,
        region: 'North West England',
        regionId: 223,
        country: 'Great Britain',
        countryId: 4,
        countryCode: 'gb'
      }
    ];
    
    var repository = {
      getAllLocations: function() {
        return Promise.resolve(repositoryData);
      }
    };
    
    var locationResolverService = new LocationResolverService(repository);
    
    it('given Salford should resolve district', function() {
      locationResolverService.getLocationFromName('Salford').then(function(result) {
        assert.deepEqual(result, {
          districtId: 23,
          districtName: 'Salford',
          countyId: 124,
          countyName: 'Greater Manchester',
          regionId: 223,
          regionName: 'North West England',
          countryId: 4,
          countryCode: 'gb'
        });
      });
    });
    
    it('given Greater Manchester should resolve county', function() {
      locationResolverService.getLocationFromName('Greater Manchester').then(function(result) {
        assert.deepEqual(result, {
          countyId: 124,
          countyName: 'Greater Manchester',
          regionId: 223,
          regionName: 'North West England',
          countryId: 4,
          countryCode: 'gb'
        });
      });
    });
    
    it('given North West England should resolve region', function() {
      locationResolverService.getLocationFromName('North West England ').then(function(result) {
        assert.deepEqual(result, {
          regionId: 223,
          regionName: 'North West England',
          countryId: 4,
          countryCode: 'gb'
        });
      });
    });
    
    it('given Great Britain should resolve country', function() {
      locationResolverService.getLocationFromName(' Great Britain ').then(function(result) {
        assert.deepEqual(result, {
          countryId: 4,
          countryCode: 'gb'
        });
      });
    });
    
    it('given FAIL should return null', function() {
      locationResolverService.getLocationFromName('FAIL').then(function(result) {
        assert.isNull(result);
      });
    });
  });
});
