var assert = require('chai').assert;
var LeaderboardFilterStringParserService = require('../../../src/server/services/leaderboard-filter-string-parser');

describe('leaderboard url parser service', function() {
  var parser = new LeaderboardFilterStringParserService();

  describe('#parse()', function() {
    it('should parse page number', function() {
      var testCases = [
        ['20-29yrs/2/', 2],
        ['4/', 4],
        ['male/9/', 9],
        ['20-29yrs/', 1],
        ['', 1],
        ['male/', 1],
        ['under50kg/20/', 20]
      ];

      testCases.forEach(function(testCase) {
        var url = testCase[0];
        var expected = testCase[1];

        var result = parser.parse(url);

        assert.strictEqual(result.page, expected);
      });
    });

    it('should parse gender', function() {
      var testCases = [
        ['20-29yrs/2/', null],
        ['FEMALE/', false],
        ['4/', null],
        ['male/', true],
        ['under50kg/4/', null]
      ];

      testCases.forEach(function(testCase) {
        var url = testCase[0];
        var expected = testCase[1];

        var result = parser.parse(url);

        assert.strictEqual(result.isMale, expected);
      });
    });

    it('should parse age', function() {
      var testCases = [
        ['20-29yrS/2/', '20-29'],
        ['female/', null],
        ['16yrs/', '16'],
        ['', null],
        ['80+yrs/2/', '80+'],
        ['under50kg/9/', null]
      ];

      testCases.forEach(function(testCase) {
        var url = testCase[0];
        var expected = testCase[1];

        var result = parser.parse(url);

        assert.strictEqual(result.age, expected);
      });
    });

    it('should parse weight', function() {
      var testCases = [
        ['100kg+/', '100kg+'],
        ['female/15yrs/70KG-79kg/', '70kg-79kg'],
        ['under50kg/', 'under50kg'],
        ['', null],
        ['80+yrs/2/', null]
      ];

      testCases.forEach(function(testCase) {
        var url = testCase[0];
        var expected = testCase[1];

        var result = parser.parse(url);

        assert.strictEqual(result.weight, expected);
      });
    });

    it('should parse only showing validated results', function() {
      var testCases = [
        ['100kg+/', false],
        ['female/15yrs/70KG-79kg/validated/', true],
        ['female/15yrs/70KG-79kg/validated/2', true],
        ['validated/under50kg/', true],
        ['', false],
        ['80+yrs/2/', false]
      ];

      testCases.forEach(function(testCase) {
        var url = testCase[0];
        var expected = testCase[1];

        var result = parser.parse(url);

        assert.strictEqual(result.onlyShowValidated, expected);
      });
    });
  });
});
