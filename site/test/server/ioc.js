var assert = require('chai').assert;
var redis = require('fakeredis');
var IoC = require('../../src/server/ioc');

describe('IoC', function() {
  describe('construct', function() {
    it('should construct without issues', function() {
      var config = {
        sessionSecretKey: 'secret',
        imageUploadConfig: {},
        videoUploadConfig: {},
        queueConfig: {}
      };
      var postgresDB = {
        registerParameters: function() {
          
        }
      };
      var logger = {};
      var redisDB = redis.createClient();

      new IoC(config, redisDB, postgresDB, logger);
    });
  });
});
