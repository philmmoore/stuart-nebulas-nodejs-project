// var assert = require('chai').assert;
// var GeoRedirectRoute = require('../../../src/server/routes/geo-redirect');

// describe('GeoRedirectRoute', function() {
//   describe('construct', function() {
//     it('given host header has country which is available should redirect to country landing page if entering on /', function(done) {
//       var country = 'fr';
//       var defaultCountry = 'gb';
//       var countriesAvailable = [ country ];

//       var req = {
//         headers: {
//           'cf-ipcountry': 'FR'
//         },
//         url: '/'
//       };
//       var res = {
//         redirect: function(url) {
//           assert.equal(url, '/' + country + '/');
//           done();
//         }
//       };

//       var next = function() {};

//       var routeFound = false;

//       var router = {
//         get: function(route, callback) {
//           if (route === '*') {
//             routeFound = true;
//             callback(req, res, next);
//           }
//         },
//         post: function(route, callback) {}
//       };

//       new GeoRedirectRoute(router, countriesAvailable, defaultCountry);

//       assert.isTrue(routeFound);
//     });

//     it('given host header is not present should redirect to the default country landing page if entering on /', function(done) {
//       var defaultCountry = 'gb';
//       var countriesAvailable = [ defaultCountry ];

//       var req = {
//         headers: {},
//         url: '/'
//       };
//       var res = {
//         redirect: function(url) {
//           assert.equal(url, '/' + defaultCountry + '/');
//           done();
//         }
//       };

//       var next = function() {};

//       var routeFound = false;

//       var router = {
//         get: function(route, callback) {
//           if (route === '*') {
//             routeFound = true;
//             callback(req, res, next);
//           }
//         },
//         post: function(route, callback) {}
//       };

//       new GeoRedirectRoute(router, countriesAvailable, defaultCountry);

//       assert.isTrue(routeFound);
//     });

//     it('given host header has country which is available should redirect to country landing page', function(done) {
//       var country = 'fr';
//       var defaultCountry = 'gb';
//       var countriesAvailable = [ country ];

//       var req = {
//         headers: {
//           'cf-ipcountry': country
//         },
//         url: '/'
//       };
//       var res = {
//         redirect: function(url) {
//           assert.equal(url, '/' + country + '/');
//           done();
//         }
//       };

//       var next = function() {};

//       var routeFound = false;

//       var router = {
//         get: function(route, callback) {
//           if (route === '*') {
//             routeFound = true;
//             callback(req, res, next);
//           }
//         },
//         post: function(route, callback) {}
//       };

//       new GeoRedirectRoute(router, countriesAvailable, defaultCountry);

//       assert.isTrue(routeFound);
//     });

//     it('given host header has country which is not available should render country not available', function(done) {
//       var country = 'fr';
//       var defaultCountry = 'gb';
//       var countriesAvailable = [];

//       var req = {
//         headers: {
//           'cf-ipcountry': country
//         },
//         url: '/'
//       };
//       var res = {
//         redirect: function(url) {
//           assert.equal(url, '/' + country + '/');
//           done();
//         }
//       };

//       var next = function() {};

//       var routeFound = false;

//       var router = {
//         get: function(route, callback) {
//           if (route === '*') {
//             routeFound = true;
//             callback(req, res, next);
//           }
//         },
//         post: function(route, callback) {}
//       };

//       new GeoRedirectRoute(router, countriesAvailable, defaultCountry);

//       assert.isTrue(routeFound);
//     });
//   });
// });
