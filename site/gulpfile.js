var gulp = require('gulp');
var gulpif = require('gulp-if');
var path = require('path');
var mocha = require('gulp-mocha');
var jshint = require('gulp-jshint');
var less = require('gulp-less');
var minifyCss = require('gulp-minify-css');
var jadelint = require('gulp-jadelint');
var istanbul = require('gulp-istanbul');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var resize = require('gulp-image-resize');
var rename = require("gulp-rename");
var imagemin = require('gulp-imagemin');

var production = process.env.NODE_ENV === 'production';
var allJavaScriptFiles = ['src/**/*.js', 'test/**/*.js', '*.js'];
var istanbulReporters = production ? ['text'] : ['text-summary', 'lcov'];
var mochaReporter = production ? 'spec' : 'nyan';
var watchMode = false;

gulp.task('test', function (callback) {
  gulp.src('src/**/*.js')
    .pipe(istanbul({ includeUntested: true }))
    .pipe(istanbul.hookRequire())
    .on('finish', function () {
      gulp.src('test/**/*.js')
        .pipe(mocha({ reporter: mochaReporter }))
        .on('error', handleTestError)
        .pipe(istanbul.writeReports({ reporters: istanbulReporters }))
        .pipe(istanbul.enforceThresholds({
          thresholds: {
            global: watchMode ? 0 : 5
          }
        }))
        .on('end', callback);
    });
});

function handleTestError(err) {
  if (watchMode) {
    this.emit('end');
    return;
  }

  throw err;
}

gulp.task('lint', function() {
  return gulp.src(allJavaScriptFiles)
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(jshint.reporter('fail'));
});

gulp.task('jade-lint', function () {
    return gulp.src('views/**/*.jade')
      .pipe(jadelint());
});

gulp.task('scripts', function() {
  return gulp.src(['ui/lib/*.js', 'src/client/*.js'])
    .pipe(concat('app.js'))
    .pipe(gulpif(production, uglify()))
    .pipe(gulp.dest('ui-bundled'));
});

gulp.task('less', function() {
  return gulp.src('ui/less/style.less')
    .pipe(less())
    .pipe(gulpif(production, minifyCss()))
    .pipe(gulp.dest('ui-bundled'));
});

gulp.task('fonts', function() {
  return gulp.src('ui/fonts/**/*')
    .pipe(gulp.dest('ui-bundled/fonts'));
});

gulp.task('static', function() {
  return gulp.src('ui/static/**/*')
    .pipe(gulp.dest('ui-bundled'));
});

gulp.task('watch', function() {
  watchMode = true;

  gulp.watch(allJavaScriptFiles, ['test', 'lint']);
  gulp.watch('ui/less/**/*.less', ['less']);
  gulp.watch('src/client/*.js', ['scripts']);
  gulp.watch('ui/fonts/**/*', ['fonts']);
  gulp.watch('views/**/*.jade', ['jade-lint']);
});

gulp.task('quality', ['test', 'lint', 'jade-lint']);
gulp.task('default', ['quality', 'scripts', 'less', 'fonts', 'static']);
