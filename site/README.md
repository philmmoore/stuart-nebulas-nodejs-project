# Site
GymFit site

## Pre-requisite Software
You'll need the same setup as per the admin project.
Aslo if on OSX in order for gulp-responsive to work, run `brew install homebrew/science/vips`

## Running
To get up and running:

1. Get the admin project set up and run it's `gulp` to populate the DB:
1. `npm install`
1. `gulp`
1. `npm start`
1. Browse to http://localhost:3000 to see the admin page
1. npm install on the site project
1. Run gulp
1. nom start to launch the site
1. Browse to http://localhost:4000 to see the site

# Front End
For design work there is livereload support:

    node livereload

Design mobile first, use media queries to override for larger screens.
Smallest screen is iPhone 5
Lowest version if IE supported - 9
