var moment = require('moment');

var _formatTime = function _formatTime(value) {
  var duration = moment.duration(value, 'milliseconds');
  var result = '';
  
  if (duration.hours() > 0) {
    result += duration.hours() + ' hr' + (duration.hours() > 1 ? 's ' : ' ');
    duration.subtract(duration.hours(), 'hours');
  }
  
  if (duration.minutes() > 0) {
    result += duration.minutes() + ' min' + (duration.minutes() > 1 ? 's ' : ' ');
    duration.subtract(duration.minutes(), 'minutes');
  }
  
  result += duration.asSeconds() + 's';
  
  return result.trim();
};

var _formatDistance = function _formatDistance(value) {
  if (value >= 1000000) {
    return (parseFloat(value) / 1000000).toFixed(2) + 'km';
  }

  if (value >= 1000) {
    return (parseFloat(value) / 1000).toFixed(2) + 'm';
  }
  
  return value + 'mm';
};

module.exports.format = function _format(value, measurement) {
  if (measurement === 'time') {
    return _formatTime(value);
  }

  if (measurement === 'distance') {
    return _formatDistance(value);
  }
  
  if (measurement === 'client-count') {
    return value + ' clients';
  }

  return value + ' reps';
};