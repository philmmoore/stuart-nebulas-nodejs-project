var populateParents = function populateParents(tree, areas, translationFor) {
  for (var index = 0; index < areas.length; index++) {
    var area = areas[index];

    if (!area.parent) {
      area.indentation = 0;
      area.name = translationFor('Country');
      tree.push(area);
    }
  }
};

var populateChildren = function populateChildren(parent, areas) {
  parent.children = [];

  for (var index = 0; index < areas.length; index++) {
    var area = areas[index];

    if (area.parent === parent.id) {
      area.indentation = parent.indentation + 1;
      populateChildren(area, areas);
      parent.children.push(area);
    }
  }
};

var flattenNodes = function flattenNodes(output, node) {
  output.push({
    id: node.id,
    name: node.name,
    parent: node.parent,
    indentation: node.indentation
  });

  for (var index = 0; index < node.children.length; index++) {
    flattenNodes(output, node.children[index]);
  }
};

module.exports = function format(areas, translationFor) {
  var tree = [];

  populateParents(tree, areas, translationFor);

  for (var index = 0; index < tree.length; index++) {
    populateChildren(tree[index], areas);
  }

  var result = [];

  for (var parentIndex = 0; parentIndex < tree.length; parentIndex++) {
    flattenNodes(result, tree[parentIndex]);
  }

  return result;
};
