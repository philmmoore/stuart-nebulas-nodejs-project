var util = require('./util');

module.exports = function EmailValidator() {
  var _validate = function _validate(form) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(form.name)) {
      validationErrors.push(['Name', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.typeId)) {
      validationErrors.push(['Type', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.subject)) {
      validationErrors.push(['Subject', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.plainBody)) {
      validationErrors.push(['Plain_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.htmlBody)) {
      validationErrors.push(['Html_Body', 'Error_Field_Blank']);
    }
    
    return util.validationPromise(validationErrors);
  };

  return {
    validate: _validate
  };
};