var util = require('./util');

module.exports = function NotificationValidator() {
  var _validate = function _validate(form) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(form.title)) {
      validationErrors.push(['Title', 'Error_Field_Blank']);
    }

    if (util.isBlankOrWhitespace(form.description)) {
      validationErrors.push(['Description', 'Error_Field_Blank']);
    }

    if (!form.typeId) {
      validationErrors.push(['Type', 'Error_Field_Blank']);
    }

    return util.validationPromise(validationErrors);
  };

  return {
    validate: _validate
  };
};
