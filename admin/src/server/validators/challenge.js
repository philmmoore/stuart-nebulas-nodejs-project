var util = require('./util');

module.exports = function ChallengeValidator() {
  var _validateName = function _validateName(name, validationErrors) {
    if (!name || util.isBlankOrWhitespace(name)) {
      return validationErrors.push(['Name', 'Error_Field_Blank']);
    }

    if (!util.isUrlFriendly(name)) {
      return validationErrors.push(['Name', 'Error_Field_Not_Url_Friendly']);
    }

    if (name.length > 100) {
      return validationErrors.push(['Name', 'Error_Field_Over_100_Characters']);
    }
  };

  var _validate = function _validate(challenge) {
    var validationErrors = [];

    _validateName(challenge.name, validationErrors);

    if (!challenge.categoryId) {
      validationErrors.push(['Category', 'Error_Field_Blank']);
    }

    if (!challenge.typeId) {
      validationErrors.push(['Type', 'Error_Field_Blank']);
    }

    if (!challenge.groups || util.isBlankOrWhitespace(challenge.groups)) {
      validationErrors.push(['Groups', 'Error_Field_Blank']);
    }

    if (challenge.unresolvedGroups && challenge.unresolvedGroups.length > 0) {
      validationErrors.push(['Groups', 'Error_Could_Not_Find_Groups', '"' + challenge.unresolvedGroups.join(', ') + '".']);
    }

    if (!challenge.bodyCopy || util.isBlankOrWhitespace(challenge.bodyCopy)) {
      validationErrors.push(['Body_Copy', 'Error_Field_Blank']);
    }

    if (!challenge.submissionMessage || util.isBlankOrWhitespace(challenge.submissionMessage)) {
      validationErrors.push(['Submission_Message', 'Error_Field_Blank']);
    }

    return util.validationPromise(validationErrors);
  };

  return {
    validate: _validate
  };
};
