var util = require('./util');

module.exports = function EmailTemplateValidator() {
  var _validate = function _validate(form) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(form.standardTemplate)) {
      validationErrors.push(['Standard_Template', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.emailConfirmationSubject)) {
      validationErrors.push(['Email_Confirmation_Subject', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.emailConfirmationHtmlBody)) {
      validationErrors.push(['Email_Confirmation_Html_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.emailConfirmationPlainBody)) {
      validationErrors.push(['Email_Confirmation_Plain_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.passwordResetSubject)) {
      validationErrors.push(['Password_Reset_Subject', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.passwordResetHtmlBody)) {
      validationErrors.push(['Password_Reset_Html_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.passwordResetPlainBody)) {
      validationErrors.push(['Password_Reset_Plain_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.resultRejectedSubject)) {
      validationErrors.push(['Result_Rejected_Subject', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.resultRejectedHtmlBody)) {
      validationErrors.push(['Result_Rejected_Html_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.resultRejectedPlainBody)) {
      validationErrors.push(['Result_Rejected_Plain_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userBlockedSubject)) {
      validationErrors.push(['User_Blocked_Subject', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userBlockedHtmlBody)) {
      validationErrors.push(['User_Blocked_Html_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userBlockedPlainBody)) {
      validationErrors.push(['User_Blocked_Plain_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userUnblockedSubject)) {
      validationErrors.push(['User_Unblocked_Subject', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userUnblockedHtmlBody)) {
      validationErrors.push(['User_Unblocked_Html_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userUnblockedPlainBody)) {
      validationErrors.push(['User_Unblocked_Plain_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userAvatarRemovedSubject)) {
      validationErrors.push(['User_Avatar_Removed_Subject', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userAvatarRemovedHtmlBody)) {
      validationErrors.push(['User_Avatar_Removed_Html_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userAvatarRemovedPlainBody)) {
      validationErrors.push(['User_Avatar_Removed_Plain_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.registerReminderSubject)) {
      validationErrors.push(['Register_Reminder_Subject', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.registerReminderHtmlBody)) {
      validationErrors.push(['Register_Reminder_Html_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.registerReminderPlainBody)) {
      validationErrors.push(['Register_Reminder_Plain_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userChallengeProgressSubject)) {
      validationErrors.push(['User_Challenge_Progress_Subject', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userChallengeProgressHtmlBody)) {
      validationErrors.push(['User_Challenge_Progress_Html_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userChallengeProgressPlainBody)) {
      validationErrors.push(['User_Challenge_Progress_Plain_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.leaderboardRankingSubject)) {
      validationErrors.push(['Leaderboard_Ranking_Subject', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.leaderboardRankingHtmlBody)) {
      validationErrors.push(['Leaderboard_Ranking_Html_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.leaderboardRankingPlainBody)) {
      validationErrors.push(['Leaderboard_Ranking_Plain_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userDeletedSubject)) {
      validationErrors.push(['User_Deleted_Subject', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userDeletedHtmlBody)) {
      validationErrors.push(['User_Deleted_Html_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userDeletedPlainBody)) {
      validationErrors.push(['User_Deleted_Plain_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userNominatedSubject)) {
      validationErrors.push(['User_Nominated_Subject', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userNominatedHtmlBody)) {
      validationErrors.push(['User_Nominated_Html_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userNominatedPlainBody)) {
      validationErrors.push(['User_Nominated_Plain_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userNominationAcceptedSubject)) {
      validationErrors.push(['User_Nomination_Accepted_Subject', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userNominationAcceptedHtmlBody)) {
      validationErrors.push(['User_Nomination_Accepted_Html_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userNominationAcceptedPlainBody)) {
      validationErrors.push(['User_Nomination_Accepted_Plain_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.socialUpdateRejectedSubject)) {
      validationErrors.push(['Social_Update_Rejected_Subject', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.socialUpdateRejectedHtmlBody)) {
      validationErrors.push(['Social_Update_Rejected_Html_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.socialUpdateRejectedPlainBody)) {
      validationErrors.push(['Social_Update_Rejected_Plain_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userPasswordChangedSubject)) {
      validationErrors.push(['User_Password_Changed_Subject', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userPasswordChangedHtmlBody)) {
      validationErrors.push(['User_Password_Changed_Html_Body', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(form.userPasswordChangedPlainBody)) {
      validationErrors.push(['User_Password_Changed_Plain_Body', 'Error_Field_Blank']);
    }
    
    return util.validationPromise(validationErrors);
  };

  return {
    validate: _validate
  };
};