var util = require('./util');

module.exports = function UserValidator() {
  var _validate = function _validate(form) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(form.reason)) {
      validationErrors.push(['Reason', 'Error_Field_Blank']);
    }

    return util.validationPromise(validationErrors);
  };

  return {
    validate: _validate
  };
};