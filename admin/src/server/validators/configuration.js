var util = require('./util');

module.exports = function ConfigurationValidator() {
  var _validate = function _validate(form) {
    var validationErrors = [];

    if (form.leaderboard.updateFrequency.length < 1) {
      validationErrors.push(['Update_Frequency', 'Error_Field_Nothing_Selected']);
    }

    return util.validationPromise(validationErrors);
  };

  return {
    validate: _validate
  };
};
