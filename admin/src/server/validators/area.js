var util = require('./util');

module.exports = function AreaValidator() {
  var _validate = function _validate(form) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(form.name)) {
      validationErrors.push(['Name', 'Error_Field_Blank']);
    } else {
      if (!util.isUrlFriendly(form.name)) {
        validationErrors.push(['Name', 'Error_Field_Not_Url_Friendly']);
      }
    }

    return util.validationPromise(validationErrors);
  };

  var _validateRegion = function _validateRegion(form) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(form.name)) {
      validationErrors.push(['Name', 'Error_Field_Blank']);
    } else {
      if (!util.isUrlFriendly(form.name)) {
        validationErrors.push(['Name', 'Error_Field_Not_Url_Friendly']);
      }
    }

    if (util.isBlankOrWhitespace(form.latitude)) {
      validationErrors.push(['Latitude', 'Error_Field_Blank']);
    } else {
      if (!util.isNumber(form.latitude)){
        validationErrors.push(['Latitude', 'Error_Field_NaN']);
      }
    }

    if (util.isBlankOrWhitespace(form.longitude)) {
      validationErrors.push(['Longitude', 'Error_Field_Blank']);
    } else {
      if (!util.isNumber(form.longitude)){
        validationErrors.push(['Longitude', 'Error_Field_NaN']);
      }
    }

    return util.validationPromise(validationErrors);
  };

  var _validateDistrict = function _validateDistrict(form) {
    var validationErrors = [];

    if (util.isBlankOrWhitespace(form.name)) {
      validationErrors.push(['Name', 'Error_Field_Blank']);
    } else {
      if (!util.isUrlFriendly(form.name)) {
        validationErrors.push(['Name', 'Error_Field_Not_Url_Friendly']);
      }
    }

    if (util.isBlankOrWhitespace(form.latitude)) {
      validationErrors.push(['Latitude', 'Error_Field_Blank']);
    } else {
      if (!util.isNumber(form.latitude)){
        validationErrors.push(['Latitude', 'Error_Field_NaN']);
      }
    }

    if (util.isBlankOrWhitespace(form.longitude)) {
      validationErrors.push(['Longitude', 'Error_Field_Blank']);
    } else {
      if (!util.isNumber(form.longitude)){
        validationErrors.push(['Longitude', 'Error_Field_NaN']);
      }
    }

    return util.validationPromise(validationErrors);
  };


  return {
    validate: _validate,
    validateRegion: _validateRegion,
    validateDistrict: _validateDistrict
  };
};
