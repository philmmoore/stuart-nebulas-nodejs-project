var util = require('./util');
var moment = require('moment');

module.exports = function BlogValidator() {
  var _validateName = function _validateName(name, validationErrors) {
    if (util.isBlankOrWhitespace(name)) {
      return validationErrors.push(['Name', 'Error_Field_Blank']);
    }

    if (!util.isUrlFriendly(name)) {
      return validationErrors.push(['Name', 'Error_Field_Not_Url_Friendly']);
    }

    if (name.length > 100) {
      return validationErrors.push(['Name', 'Error_Field_Over_100_Characters']);
    }
  };
  
  var _validate = function _validate(post) {
    var validationErrors = [];

    _validateName(post.name, validationErrors);
    
    if (!moment(post.published, 'DD/MM/YYYY HH:mm:ss a', true).isValid()) {
      validationErrors.push(['Published', 'Error_Field_Invalid_DateTime']);
    }

    if (util.isBlankOrWhitespace(post.bodyCopy)) {
      validationErrors.push(['Body_Copy', 'Error_Field_Blank']);
    }
    
    if (util.isBlankOrWhitespace(post.summary)) {
      validationErrors.push(['Summary', 'Error_Field_Blank']);
    }

    return util.validationPromise(validationErrors);
  };

  return {
    validate: _validate
  };
};