var util = require('./util');

module.exports = function PageValidator() {
  var _validateName = function _validateName(name, validationErrors) {
    if (!name || util.isBlankOrWhitespace(name)) {
      return validationErrors.push(['Name', 'Error_Field_Blank']);
    }

    if (!util.isUrlFriendly(name)) {
      return validationErrors.push(['Name', 'Error_Field_Not_Url_Friendly']);
    }

    if (name.length > 100) {
      return validationErrors.push(['Name', 'Error_Field_Over_100_Characters']);
    }
  };

  var _validate = function _validate(page) {
    var validationErrors = [];

    _validateName(page.name, validationErrors);

    if (!page.bodyCopy || util.isBlankOrWhitespace(page.bodyCopy)) {
      validationErrors.push(['Body_Copy', 'Error_Field_Blank']);
    }

    return util.validationPromise(validationErrors);
  };

  return {
    validate: _validate
  };
};
