var util = require('./util');

module.exports = function AdvertisementValidator() {
  var _validateName = function _validateName(name, validationErrors) {
    if (!name || util.isBlankOrWhitespace(name)) {
      return validationErrors.push(['Name', 'Error_Field_Blank']);
    }

    if (name.length > 100) {
      return validationErrors.push(['Name', 'Error_Field_Over_100_Characters']);
    }
  };

  var _validateFileIsPNG = function _validateFileIsPNG(file, propertyName, resolution, validationErrors) {
    if (!file) {
      return;
    }

    if (file.extension !== 'png') {
      return validationErrors.push([propertyName, 'Error_Field_Not_PNG_File']);
    }

    if (file.width !== resolution.width || file.height !== resolution.height){
      var expectedResolution = resolution.width + 'x' + resolution.height + 'px.';
      return validationErrors.push([propertyName, 'Error_Field_Image_Not_Correct_Resolution', expectedResolution]);
    }
  };

  var _validate = function _validate(form) {
    var validationErrors = [];

    _validateName(form.name, validationErrors);

    if (!util.isBlankOrWhitespace(form.link) && !util.isUrlValid(form.link)) {
      validationErrors.push(['Link', 'Error_Field_Not_Valid_Hyperlink']);
    }

    var smallImageResolution = {
      width: 375,
      height: 50
    };

    var largeHorizontalImageResolution = {
      width: 150,
      height: 500
    };

    _validateFileIsPNG(form.smallHorizontalImage, 'Small_Horizontal_Image', smallImageResolution, validationErrors);
    _validateFileIsPNG(form.largeVerticalImage, 'Large_Vertical_Image', largeHorizontalImageResolution, validationErrors);

    return util.validationPromise(validationErrors);
  };

  return {
    validate: _validate
  };
};
