var Promise = require('bluebird');
var validUrl = require('valid-url');
var emailValidator = require('email-validator');
var isDisposableEmail = require('is-disposable-email');

module.exports.isBlankOrWhitespace = function isBlankOrWhitespace(string) {
  if (!string) {
    return true;
  }

  return string.length < 1 || /^\s+$/.test(string);
};

module.exports.isNumber = function isNumber(number) {
  return (isNaN(number) ? false : true);
};

module.exports.isUrlValid = function isUrlValid(url) {
  return validUrl.isUri(url);
};

module.exports.isUrlFriendly = function isUrlFriendly(string) {
  return /^[a-zA-Z0-9- ]+$/.test(string);
};

module.exports.isEmailValid = function isEmailValid(email) {
  return emailValidator.validate(email);
};

module.exports.isEmailDisposable = function isEmailDisposable(email) {
  return isDisposableEmail(email);
};

module.exports.isValidUUID = function isValidUUID(uuid) {
  return /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i.test(uuid);
};

module.exports.validationPromise = function validationPromise(validationErrors) {
  return new Promise(function(resolve, reject) {
    if (validationErrors.length > 0) {
      return reject(validationErrors);
    }

    resolve();
  });
};