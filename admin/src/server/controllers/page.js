var Promise = require('bluebird');

module.exports = function PageController(repository, validator, publicSite) {
  var country = 1;

  var _getPageFromSubmittedForm = function _getPageFromSubmittedForm(req) {
    return {
      id: req.params.id,
      name: req.body.Name,
      bodyCopy: req.body.BodyCopy
    };
  };

  var _getAllPages = function _getAllPages(session) {
    var lastSavedItem = session.lastSavedItem;
    var lastSavedItemMessage = session.lastSavedItemMessage;
    session.lastSavedItem = null;
    session.lastSavedItemMessage = null;

    return repository.getAllPages(country).then(function(pages) {
      pages = pages.map(function(page) {
        page.url = publicSite + 'gb/' + page.name.toLowerCase().replace(/ /g, '-') + '/';
        
        return page;
      });
      
      return {
        pages: pages,
        lastSavedItem: lastSavedItem,
        lastSavedItemMessage: lastSavedItemMessage
      };
    });
  };

  var _getPage = function _getPage(id) {
    return repository.getPage(id).then(function(page) {
      return {
        page: page
      };
    });
  };

  var _savePage = function _savePage(req) {
    var form = _getPageFromSubmittedForm(req);

    return validator.validate(form).catch(function(err) {
      return Promise.reject({
        page: form,
        validationErrors: err
      });
    }).then(function() {
      return repository.persistPage(form).catch(function(err) {
        var validationErrors = [];

        if (err.constraint === 'page_enforce_names_unique_index') {
          validationErrors.push(['Name', 'Error_Page_Name_Not_Unique']);
        }

        return Promise.reject({
          page: form,
          validationErrors: validationErrors
        });
      });
    }).then(function() {
      req.session.lastSavedItem = form.name;
    });
  };
  
  var _deletePage = function _deletePage(req) {
    return repository.getPage(req.params.id).then(function(page) {
      return repository.deletePage(page.id).then(function() {
        req.session.lastSavedItem = page.name;
        req.session.lastSavedItemMessage = 'Deleted';
      });
    });
  };

  return {
    getAllPages: _getAllPages,
    getPage: _getPage,
    savePage: _savePage,
    deletePage: _deletePage
  };
};
