var Promise = require('bluebird');

module.exports = function ConfigurationController(repository, validator) {
  var countryId = 1;

  var _getConfiguration = function _getConfiguration(req) {
    var lastSavedItem = req.session.lastSavedItem;
    req.session.lastSavedItem = null;

    return repository.getConfiguration(countryId).then(function(data) {
      data.lastSavedItem = lastSavedItem;
      return data;
    });
  };

  var _getLeaderboardUpdateFrequency = function _getLeaderboardUpdateFrequency(req) {
    var data = [];

    Object.keys(req.body).forEach(function(key) {
      if (key.indexOf('LeaderboardUpdateFrequency-') > -1) {
        data.push(parseInt(key.split('-')[1]));
      }
    });

    return data;
  };

  var _buildConfigurationFromSubmittedForm = function _buildConfigurationFromSubmittedForm(req) {
    return {
      leaderboard: {
        updateFrequency: _getLeaderboardUpdateFrequency(req)
      },
      customHomePageHTML: req.body.CustomHomePageHTML || null
    };
  };

  var _setConfiguration = function _setConfiguration(req) {
    var configuration = _buildConfigurationFromSubmittedForm(req);

    return validator.validate(configuration).catch(function(err) {
      configuration.validationErrors = err;
      return Promise.reject(configuration);
    }).then(function() {
      return repository.setConfiguration(countryId, JSON.stringify(configuration));
    }).then(function() {
      req.session.lastSavedItem = 'Configuration';
    });
  };

  return {
    getConfiguration: _getConfiguration,
    setConfiguration: _setConfiguration
  };
};
