var Promise = require('bluebird');

module.exports = function NotificationController(repository, validator) {
  var _getNotificationFromSubmittedForm = function _getNotificationFromSubmittedForm(req) {
    return {
      id: req.params.id,
      typeId: req.body.Type,
      isUserDismissable: req.body.UserDismissable ? true : false,
      title: req.body.Title,
      description: req.body.Description
    };
  };

  var _getAllNotifications = function _getAllNotifications(session) {
    var lastSavedItem = session.lastSavedItem;
    session.lastSavedItem = null;

    return repository.getAllNotifications().then(function(notifications) {
      return {
        notifications: notifications,
        lastSavedItem: lastSavedItem
      };
    });
  };

  var _getNewNotification = function _getNewNotification() {
    return repository.getNotificationTypes().then(function(notificationTypes) {
      return {
        notification: {},
        notificationTypes: notificationTypes
      };
    });
  };

  var _getNotification = function _getNotification(req) {
    var id = parseInt(req.params.id);
    return repository.getNotification(id);
  };

  var _saveNotification = function _saveNotification(req) {
    var form = _getNotificationFromSubmittedForm(req);

    return validator.validate(form).catch(function(err) {
      return repository.getNotificationTypes().then(function(notificationTypes) {
        return Promise.reject({
          notification: form,
          notificationTypes: notificationTypes,
          validationErrors: err
        });
      });
    }).then(function() {
      return repository.persistNotification(form);
    }).then(function() {
      req.session.lastSavedItem = form.name;
    });
  };

  return {
    getAllNotifications: _getAllNotifications,
    getNewNotification: _getNewNotification,
    getNotification: _getNotification,
    saveNotification: _saveNotification
  };
};
