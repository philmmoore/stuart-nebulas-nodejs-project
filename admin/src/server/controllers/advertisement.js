var Promise = require('bluebird');

module.exports = function AdvertisementController(repository, validator, areaFormatter, imageUploadRepository, advertisementImageUrl) {
  var countryId = 1;

  var _getAdvertisementFromSubmittedForm = function _getAdvertisementFromSubmittedForm(req) {
    var selectedRegions = [];
    var selectedCounties = [];
    var selectedDistricts = [];

    Object.keys(req.body).forEach(function(key) {
      if (key.indexOf(':') > -1) {
        var data = key.split(':');

        if (data[0] === 'r') {
          selectedRegions.push(parseInt(data[1]));
        } else if (data[0] === 'c') {
          selectedCounties.push(parseInt(data[1]));
        } else if (data[0] === 'd') {
          selectedDistricts.push(parseInt(data[1]));
        }
      }
    });

    return {
      id: req.params.id,
      name: req.body.Name,
      link: req.body.Link,
      countrySelected: req.body.countrySelected ? true : false,
      regions: selectedRegions,
      counties: selectedCounties,
      districts: selectedDistricts,
      smallHorizontalImage: req.body.SmallHorizontalImage,
      largeVerticalImage: req.body.LargeVerticalImage
    };
  };

  var _uploadImage = function _uploadImage(advertisementId, type, image) {
    if (!image) {
      return Promise.resolve();
    }

    image.name = advertisementId + '-' + type + '.' + image.extension;

    return imageUploadRepository.upload(image, 'adverts').then(function() {
      return repository.associateImageWithAdvertisement(advertisementId, type, image.extension);
    });
  };

  var _uploadImages = function _uploadImages(advertisement) {
    return Promise.all([
      _uploadImage(advertisement.id, 'sm', advertisement.smallHorizontalImage),
      _uploadImage(advertisement.id, 'lg', advertisement.largeVerticalImage)
    ]);
  };

  var _populateAreas = function _populateAreas(data) {
    return repository.getAllRegions(countryId).then(function(regions) {
      if (!regions) {
        return [];
      }

      regions = regions.map(function(region) {
        if (data.advertisement.regions && data.advertisement.regions.indexOf(region.id) > -1) {
          region.selected = true;
        }

        return region;
      });

      data.areas = regions;
      return regions;
    }).each(function(region) {
      return repository.getAllCounties(region.id).then(function(counties) {
        if (!counties) {
          return [];
        }

        counties = counties.map(function(county) {
          if (data.advertisement.counties && data.advertisement.counties.indexOf(county.id) > -1) {
            county.selected = true;
          }

          return county;
        });

        region.counties = counties;
        return counties;
      }).each(function(county) {
        return repository.getAllDistricts(county.id).then(function(districts) {
          if (!districts) {
            return;
          }

          county.districts = districts.map(function(district) {
            if (data.advertisement.districts && data.advertisement.districts.indexOf(district.id) > -1) {
              district.selected = true;
            }

            return district;
          });
        });
      });
    });
  };

  var _populateCountrySelection = function _populateCountrySelection(countryId, advertId, data) {
    return repository.getCountrySelection(countryId, advertId).then(function(countrySelection) {
      data.country = {
        name: countrySelection.name,
        selected: data.advertisement.countrySelected !== undefined ? data.advertisement.countrySelected : countrySelection.hasAdvert === 1
      };

      return data;
    });
  };

  var _getAllAdvertisements = function _getAllAdvertisements(session) {
    var lastSavedItem = session.lastSavedItem;
    session.lastSavedItem = null;

    return repository.getAllAdvertisements(countryId).then(function(advertisements) {
      return {
        advertisements: advertisements,
        lastSavedItem: lastSavedItem
      };
    });
  };

  var _getNewAdvertisement = function _getNewAdvertisement() {
    var data = {
      advertisement: {}
    };

    return _populateAreas(data).then(function() {
      return _populateCountrySelection(countryId, null, data);
    }).then(function() {
      return data;
    });
  };

  var _getAdvertisement = function _getAdvertisement(req) {
    var id = parseInt(req.params.id);
    var data = {};

    return repository.getAdvertisement(id).then(function(advertisement) {
      data.advertisement = advertisement;
    }).then(function() {
      return _populateCountrySelection(countryId, data.advertisement.id, data);
    }).then(function() {
      return _populateAreas(data);
    }).then(function() {
      if (data.advertisement.smallHorizontalImageExtension) {
        data.advertisement.smallHorizontalImagePreview = advertisementImageUrl + id + '-sm.' + data.advertisement.smallHorizontalImageExtension;
      }

      if (data.advertisement.largeVerticalImageExtension) {
        data.advertisement.largeVerticalImageExtensionPreview = advertisementImageUrl + id + '-lg.' + data.advertisement.largeVerticalImageExtension;
      }

      return data;
    });
  };

  var _saveAdvertisement = function _saveAdvertisement(req) {
    var form = _getAdvertisementFromSubmittedForm(req);

    return validator.validate(form).catch(function(err) {
      data = {
        advertisement: form,
        validationErrors: err
      };

      return _populateCountrySelection(countryId, data.advertisement.id, data).then(function() {
        return _populateAreas(data);
      }).then(function() {
        return Promise.reject(data);
      });
    }).then(function() {
      return repository.persistAdvertisement(countryId, form);
    }).then(function(advertisementId) {
        form.id = advertisementId;
        return _uploadImages(form);
    }).then(function() {
      req.session.lastSavedItem = form.name;
    });
  };

  return {
    getAllAdvertisements: _getAllAdvertisements,
    getNewAdvertisement: _getNewAdvertisement,
    getAdvertisement: _getAdvertisement,
    saveAdvertisement: _saveAdvertisement
  };
};
