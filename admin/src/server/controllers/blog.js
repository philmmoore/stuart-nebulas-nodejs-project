var moment = require('moment');
var Promise = require('bluebird');

module.exports = function BlogController(repository, validator, publicSite) {
  var COUNTRY_ID = 1;
  
  var _getPostFromSubmittedForm = function _getPostFromSubmittedForm(req) {
    return {
      id: req.params.id,
      name: req.body.Name,
      published: req.body.Published,
      bodyCopy: req.body.BodyCopy,
      summary: req.body.Summary
    };
  };

  var _getAllPosts = function _getAllPosts(session) {
    var lastSavedItem = session.lastSavedItem;
    var lastSavedItemMessage = session.lastSavedItemMessage;
    session.lastSavedItem = null;
    session.lastSavedItemMessage = null;
    
    return repository.getAllPosts(COUNTRY_ID).then(function(posts) {
      posts = posts.map(function(post) {
        post.published = moment(post.published);
        post.url = publicSite + 'gb/blog/' + post.published.format('YYYY/MM/') + post.name.toLowerCase().replace(/ /g, '-') + '/';
        
        return post;
      });

      return {
        posts: posts,
        lastSavedItem: lastSavedItem,
        lastSavedItemMessage: lastSavedItemMessage
      };
    });
  };

  var _getPost = function _getPost(id) {
    return repository.getPost(id).then(function(post) {
      post.published = moment(post.published).format('DD/MM/YYYY HH:mm:ss a');
      
      return {
        post: post
      };
    });
  };

  var _savePost = function _savePost(req) {
    var post = _getPostFromSubmittedForm(req);
    
    return validator.validate(post).error(function(validationErrors) {
      return Promise.reject({
        post: post,
        validationErrors: validationErrors
      });
    }).then(function() {
      post.published = moment(post.published, 'DD/MM/YYYY HH:mm:ss a', true).toISOString();
      return repository.persistPost(post);
    }).then(function() {
      req.session.lastSavedItem = post.name;
    });
  };
  
  var _deletePost = function _deletePost(req) {
    return repository.getPost(req.params.id).then(function(post) {
      return repository.deletePost(post.id).then(function() {
        req.session.lastSavedItem = post.name;
        req.session.lastSavedItemMessage = 'Deleted';
      });
    });
  };
  
  return {
    getAllPosts: _getAllPosts,
    getPost: _getPost,
    savePost: _savePost,
    deletePost: _deletePost
  };
};