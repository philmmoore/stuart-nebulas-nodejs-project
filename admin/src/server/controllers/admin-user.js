var Promise = require('bluebird');

module.exports = function AdminUserController(repository, validator, passwordHashService) {
  var COUNTRY_ID = 1;
  
  var _getAllUsers = function _getAllUsers(req) {
    var lastSavedItem = req.session.lastSavedItem;
    var lastSavedItemMessage = req.session.lastSavedItemMessage;
    req.session.lastSavedItem = null;
    req.session.lastSavedItemMessage = null;
    
    return repository.getAllUsers().then(function(data) {
      return {
        users: data,
        lastSavedItem: lastSavedItem,
        lastSavedItemMessage: lastSavedItemMessage
      };
    });
  };
  
  var _get = function _get(req) {
    return repository.get(req.params.userId).then(function(data) {
      return {
        user: data
      };
    });
  };
  
  var _delete = function _delete(req) {
    var form = {
      userId: req.params.userId
    };
    
    return repository.get(form.userId).then(function(data) {
      if (data.username === req.session.user) {
        return Promise.reject({
          user: data
        });
      }
      
      return repository.delete(form.userId);
    }).then(function(data) {
      req.session.lastSavedItem = form.userId;
      req.session.lastSavedItemMessage = 'Admin_User_Deleted';
    });
  };
  
  var _save = function _save(req) {
    var form = {
      username: req.body.Username,
      password: req.body.Password
    };
    
    return validator.validate(form).error(function(validationErrors) {
      return Promise.reject({
        user: form,
        validationErrors: validationErrors
      });
    }).then(function() {
      return passwordHashService.getHash(form.password);
    }).then(function(hash) {
      return repository.persist(COUNTRY_ID, form.username, hash);
    }).then(function() {
      req.session.lastSavedItem = form.username;
    });
  };

  return {
    getAllUsers: _getAllUsers,
    get: _get,
    delete: _delete,
    save: _save
  };
};