var Promise = require('bluebird');

module.exports = function ChallengeController(repository, validator, publicSite) {
  var _getTranslation = function _getTranslation(translationFor, value) {
    return translationFor(value.replace(/ /g, '_'));
  };

  var _formatGroups = function _formatGroups(groups) {
    return groups.map(function(group) {
      return group.value;
    }).join(', ');
  };

  var _getChallengeFromSubmittedForm = function _getChallengeFromSubmittedForm(req) {
    return {
      id: req.params.id,
      name: req.body.Name,
      categoryId: req.body.Category,
      typeId: req.body.Type,
      groups: req.body.Groups,
      bodyCopy: req.body.BodyCopy,
      submissionMessage: req.body.SubmissionMessage,
      hidden: req.body.Hidden || false,
      comingSoon: req.body.ComingSoon || false,
    };
  };

  var _resolveChallengeGroups = function _resolveChallengeGroups(challenge) {
    if (challenge.groups.length < 1) {
      return Promise.resolve(challenge);
    }

    return repository.getGroups().then(function(groups) {
      challenge.groupIds = [];
      challenge.unresolvedGroups = [];

      challenge.groups.split(',').forEach(function(group) {
        group = group.trim();

        if (group.length < 1) {
          return;
        }

        for (var index = 0; index < groups.length; index++) {
          var dbGroup = groups[index];

          if (dbGroup.value.toLowerCase() == group.toLowerCase()) {
            if (challenge.groupIds.indexOf(dbGroup.id) === -1) {
              challenge.groupIds.push(dbGroup.id);
            }

            return;
          }
        }

        challenge.unresolvedGroups.push(group);
      });

      return challenge;
    });
  };

  var _setChallengeCategory = function _setChallengeCategory(challenge, categories) {
    for (var index = 0; index < categories.length; index++) {
      var category = categories[index];
      if (challenge.categoryId === category.id) {
        challenge.category = category.name;
        return;
      }
    }
  };

  var _setChallengeType = function _setChallengeType(challenge, types) {
    for (var index = 0; index < types.length; index++) {
      var challengeType = types[index];
      if (challenge.typeId === challengeType.id) {
        challenge.type = challengeType.name;
        return;
      }
    }
  };

  var _formatChallenge = function _formatChallenge(challenge, categories, types, translationFor) {
    _setChallengeCategory(challenge, categories);
    _setChallengeType(challenge, types);
    _translateChallengeCategoryAndType(challenge, translationFor);

    if (Array.isArray(challenge.groups)) {
      challenge.groups = challenge.groups.join(', ');
    }
  };

  var _translateChallengeCategoryAndType = function _translateChallengeCategoryAndType(challenge, translationFor) {
    challenge.category = _getTranslation(translationFor, challenge.category);
    challenge.type = _getTranslation(translationFor, challenge.type);
  };

  var _formatChallenges = function _formatChallenges(challenges, translationFor) {
    if (challenges) {
      for (var index = 0; index < challenges.length; index++) {
        var challenge = challenges[index];
        
        challenge.url = publicSite + 'gb/' + challenge.name.toLowerCase().replace(/ /g, '-') + '/';
        _translateChallengeCategoryAndType(challenge, translationFor);
      }
    }

    return challenges;
  };

  var _getFormattedChallenges = function _getFormattedChallenges(translationFor) {
    return repository.getAllChallenges().then(function(challenges) {
      return {
        challenges: _formatChallenges(challenges, translationFor)
      };
    });
  };

  var _getAllChallenges = function _getAllChallenges(session, translationFor) {
    var lastSavedItem = session.lastSavedItem;
    var lastSavedItemMessage = session.lastSavedItemMessage;
    session.lastSavedItem = null;
    session.lastSavedItemMessage = null;

    return _getFormattedChallenges(translationFor).then(function(data) {
      data.lastSavedItem = lastSavedItem;
      data.lastSavedItemMessage = lastSavedItemMessage;
      
      return data;
    });
  };

  var _getNewChallenge = function _getNewChallenge(translationFor) {
    return Promise.resolve([
      repository.getCategories(),
      repository.getTypes(),
      repository.getGroups()
    ]).spread(function(categories, types, groups) {
      return {
        challenge: {},
        categories: categories,
        types: types,
        availableGroups: _formatGroups(groups)
      };
    });
  };

  var _getChallenge = function _getChallenge(id, translationFor) {
    return Promise.resolve([
      repository.getChallenge(id),
      repository.getCategories(),
      repository.getTypes(),
      repository.getGroups()
    ]).spread(function(challenge, categories, types, groups) {
      _formatChallenge(challenge, categories, types, translationFor);

      return {
        challenge: challenge,
        categories: categories,
        types: types,
        availableGroups: _formatGroups(groups)
      };
    });
  };

  var _rejectWithValidationErrors = function _rejectWithValidationErrors(challenge, validationErrors, translationFor) {
    return _getNewChallenge(translationFor).then(function(data) {
      data.challenge = challenge;
      data.validationErrors = validationErrors;

      return Promise.reject(data);
    });
  };

  var _saveChallenge = function _saveChallenge(req, translationFor) {
    var challenge = _getChallengeFromSubmittedForm(req);

    return _resolveChallengeGroups(challenge).then(function(challenge) {
      return validator.validate(challenge).error(function(validationErrors) {
        return _rejectWithValidationErrors(challenge, validationErrors, translationFor);
      }).then(function() {
        return repository.persistChallenge(challenge).error(function(data) {
          var validationErrors = [['Name', 'Error_Challenge_Name_Not_Unique']];
          return _rejectWithValidationErrors(challenge, validationErrors, translationFor);
        });
      }).then(function() {
        req.session.lastSavedItem = challenge.name;
      });
    });
  };
  
  var _deleteChallenge = function _deleteChallenge(req, translationFor) {
    var challengeId = parseInt(req.params.id);
    
    return repository.getChallenge(challengeId).then(function(challenge) {
      return repository.deleteChallenge(challengeId).then(function() {
        return challenge;
      });
    }).then(function(deletedChallenge) {
      req.session.lastSavedItem = deletedChallenge.name;
      req.session.lastSavedItemMessage = 'Deleted';
    });
  };

  return {
    getAllChallenges: _getAllChallenges,
    getNewChallenge: _getNewChallenge,
    getChallenge: _getChallenge,
    saveChallenge: _saveChallenge,
    deleteChallenge: _deleteChallenge
  };
};
