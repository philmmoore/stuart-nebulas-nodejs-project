var Promise = require('bluebird');
var moment = require('moment');

module.exports = function ResultController(repository, validator, publicSite, socialImageUrl, socialVideoUrl, messageQueueService, removeImageUploadService, removeVideoUploadService) {
  var _getSocialUpdates = function _getSocialUpdates(req) {
    return repository.getSocialUpdates().then(function(socialUpdates) {
      var model = {
        lastSavedItem: req.session.lastSavedItem,
        lastSavedItemMessage: req.session.lastSavedItemMessage
      };
      
      req.session.lastSavedItem = null;
      req.session.lastSavedItemMessage = null;
      
      model.socialUpdates = socialUpdates || [];
      
      for (var index = 0; index < model.socialUpdates.length; index++) {
        _formatSocialUpdate(model.socialUpdates[index]);
      }
      
      return model;
    });
  };
  
  var _get = function _get(req) {
    return repository.get(req.params.updateId).then(function(socialUpdate) {
      _formatSocialUpdate(socialUpdate);
      
      return {
        socialUpdate: socialUpdate
      };
    });
  };
  
  var _approve = function _approve(req) {
    return repository.approve(req.params.updateId).then(function() {
      req.session.lastSavedItem = req.params.updateId;
      req.session.lastSavedItemMessage = 'Approved_Social_Update';
    });
  };
  
  var _formatSocialUpdate = function _formatSocialUpdate(socialUpdate) {
    socialUpdate.created = moment(socialUpdate.created);
    socialUpdate.userLink = publicSite + 'user/' + socialUpdate.username + '/';
    socialUpdate.link = '/social-update/' + socialUpdate.id + '/';
    
    _populateAttachments(socialUpdate);
  };
  
  var _populateAttachments = function _populateAttachments(socialUpdate) {
    socialUpdate.attachments = [];
    
    for (var videoIndex = 0; videoIndex < socialUpdate.videos.length; videoIndex ++) {
      socialUpdate.attachments.push({
        file: socialVideoUrl + socialUpdate.videos[videoIndex] + '.mp4',
        type: 'Video',
        deleteLink: '/social-update/video/' + socialUpdate.videos[videoIndex] + '/delete/'
      });
    }
    
    for (var imageIndex = 0; imageIndex < socialUpdate.images.length; imageIndex ++) {
      socialUpdate.attachments.push({
        file: socialImageUrl + socialUpdate.images[imageIndex] + '.png',
        type: 'Image',
        deleteLink: '/social-update/image/' + socialUpdate.images[imageIndex] + '/delete/'
      });
    }
  };
  
  var _deleteImage = function _deleteImage(req) {
    return _removeImage(req.params.imageId).then(function() {
      req.session.lastSavedItem = req.params.imageId;
      req.session.lastSavedItemMessage = 'Social_Update_Image_Deleted';
    });
  };
  
  var _deleteVideo = function _deleteVideo(req) {
    return _removeVideo(req.params.videoId).then(function() {
      req.session.lastSavedItem = req.params.videoId;
      req.session.lastSavedItemMessage = 'Social_Update_Video_Deleted';
    });
  };
  
  var _reject = function _reject(req) {
    var form = {
      updateId: req.params.updateId,
      reason: req.body.Reason
    };
    
    return validator.validate(form).catch(function(err) {
      return _get(req).then(function(data) {
        data.validationErrors = err;
        return Promise.reject(data);
      });
    }).then(function() {
      return repository.reject(form.updateId, form.reason);
    }).then(function(data) {
      var message = JSON.stringify({
        email: data.email,
        submitted: data.created,
        reason: data.reason
      });

      return messageQueueService.sendMessage('social-update-rejected-email', message).then(function() {
        var tasks = [];
        
        for (var imageIndex = 0; imageIndex < data.images.length; imageIndex++) {
          tasks.push(_removeImage(data.images[imageIndex]));
        }
        
        for (var videoIndex = 0; videoIndex < data.videos.length; videoIndex++) {
          tasks.push(_removeVideo(data.videos[videoIndex]));
        }
        
        return Promise.all(tasks);
      });
    }).then(function() {
      req.session.lastSavedItem = form.updateId;
      req.session.lastSavedItemMessage = 'Rejected_Social_Update';
    });
  };
  
  var _removeImage = function _removeImage(id) {
    return removeImageUploadService.deleteSocialUpdateImage(id).then(function() {
      return repository.deleteImage(id);
    });
  };
  
  var _removeVideo = function _removeVideo(id) {
    return removeVideoUploadService.deleteSocialUpdateVideo(id).then(function() {
      return repository.deleteVideo(id);
    });
  };
  
  return {
    getSocialUpdates: _getSocialUpdates,
    get: _get,
    approve: _approve,
    deleteImage: _deleteImage,
    deleteVideo: _deleteVideo,
    reject: _reject
  };
};