var Promise = require('bluebird');
var moment = require('moment');

module.exports = function DashboardController(repository, publicSite, evidenceImageUrl, evidenceVideoUrl, resultFormatter) {
  var _getData = function _getData() {
    return Promise.all([
      repository.getLatestResults(),
      repository.getChartData(),
      repository.getCharityData()
    ]).spread(function(latestResults, chartData, charityData) {
      return {
        latestResults: _formatLatestResults(latestResults),
        chartData: _formatChartData(chartData),
        charityData: charityData
      };
    });
  };
  
  var _formatLatestResults = function _formatLatestResults(results) {
    if (!results) {
      return [];
    }
    
    for (var index = 0; index < results.length; index++) {
      var result = results[index];
      
      result.userLink = publicSite + 'user/' + result.username + '/';
      result.result = resultFormatter.format(result.result, result.measurement);
      result.submitted = moment(result.submitted);
      result.resultLink = publicSite + 'result/' + result.resultId + '/';
      
      if (result.imageValidated) {
        result.evidenceLink = evidenceImageUrl + result.resultId + '.png';
      } else if (result.videoValidated) {
        result.evidenceLink = evidenceVideoUrl + result.resultId + '.mp4';
      }
    }
    
    return results;
  };
  
  var _formatChartData = function _formatChartData(chartData) {
    for (var index = 0; index < chartData.length; index++) {
      var chartDataRow = chartData[index];
      chartDataRow.date = moment(chartDataRow.date).format('YYYY-MM-DD');
    }
    
    return chartData;
  };

  return {
    getData: _getData
  };
};