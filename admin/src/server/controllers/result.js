var Promise = require('bluebird');
var moment = require('moment');

module.exports = function ResultController(repository, validator, publicSite, evidenceImageUrl, evidenceVideoUrl, resultFormatter, messageQueueService, removeImageUploadService, removeVideoUploadService) {
  var _formatResult = function(result) {
    result.userLink = publicSite + 'user/' + result.username + '/';
    result.result = resultFormatter.format(result.result, result.measurement);
    result.flagPercent = result.flagPercent.toFixed(2);
    result.resultLink = publicSite + 'result/' + result.resultId + '/';
    
    if (result.imageValidated) {
      result.evidenceLink = evidenceImageUrl + result.resultId + '.png';
    } else if (result.videoValidated) {
      result.evidenceLink = evidenceVideoUrl + result.resultId + '.mp4';
    }
  };
  
  var _getFlaggedResults = function _getFlaggedResults(req) {
    return repository.getFlaggedResults().then(function(flaggedResults) {
      var model = {
        lastSavedItem: req.session.lastSavedItem,
        lastSavedItemMessage: req.session.lastSavedItemMessage
      };
      
      req.session.lastSavedItem = null;
      req.session.lastSavedItemMessage = null;
      
      if (!flaggedResults) {
        model.flaggedResults = null;
        return model;
      }

      flaggedResults = flaggedResults.map(function(result) {
        _formatResult(result);        
        return result;
      });
      
      model.flaggedResults = flaggedResults;
      return model;
    });
  };
  
  var _approve = function _approve(req) {
    return repository.approve(req.params.resultId).then(function() {
      req.session.lastSavedItem = req.params.resultId;
      req.session.lastSavedItemMessage = 'Approved_Result';
    });
  };
  
  var _get = function _get(req) {
    return repository.get(req.params.resultId).then(function(data) {
      _formatResult(data);
      
      return {
        result: data
      };
    });
  };
  
  var _reject = function _reject(req) {
    var form = {
      resultId: req.params.resultId,
      reason: req.body.Reason
    };
    
    return validator.validate(form).catch(function(err) {
      return _get(req).then(function(data) {
        data.validationErrors = err;
        return Promise.reject(data);
      });
    }).then(function() {
      return repository.reject(form.resultId, form.reason);
    }).then(function() {
      return  _get(req);
    }).then(function(data) {
      var result = data.result;
      
      var message = JSON.stringify({
        email: result.email,
        challengeName: result.challengeName,
        challengeGroup: result.challengeGroup,
        result: result.result,
        submitted: result.submitted,
        reason: form.reason
      });

      return messageQueueService.sendMessage('result-rejected-email', message).then(function() {
        if (result.imageValidated) {
          return removeImageUploadService.deleteResultEvidence(result.resultId);
        }
        
        if (result.videoValidated) {
          return removeVideoUploadService.deleteResultEvidence(result.resultId);
        }
        
        return Promise.resolve();
      });  
    }).then(function() {
      req.session.lastSavedItem = form.resultId;
      req.session.lastSavedItemMessage = 'Rejected_Result';
    });
  };

  return {
    getFlaggedResults: _getFlaggedResults,
    approve: _approve,
    get: _get,
    reject: _reject
  };
};