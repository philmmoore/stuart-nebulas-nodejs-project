var Promise = require('bluebird');

module.exports = function AreaController(repository, validator) {
  var countryId = 1;

  var _getEditForm = function _getEditForm(req) {
    return {
      id: req.params.id,
      name: req.body.Name
    };
  };

  var _getEditRegionForm = function _getEditRegionForm(req){
    return {
      id: req.params.id,
      name: req.body.Name,
      latitude: req.body.Latitude,
      longitude: req.body.Longitude
    };
  };

  var _getEditDistrictForm = function _getEditDistrictForm(req){
    return {
      id: req.params.id,
      name: req.body.Name,
      latitude: req.body.Latitude,
      longitude: req.body.Longitude
    };
  };

  var _getLastSavedItem = function _getLastSavedItem(req) {
    var lastSavedItem = req.session.lastSavedItem;
    req.session.lastSavedItem = null;
    return lastSavedItem;
  };

  var _getAllRegions = function _getAllRegions(req) {
    return repository.getAllRegions(countryId).then(function(regions) {
      return {
        regions: regions,
        lastSavedItem: _getLastSavedItem(req)
      };
    });
  };

  var _editRegion = function _editRegion(req) {
    return repository.getAllCounties(req.params.id).then(function(data) {
      return {
        region: {
          id: data.id,
          name: data.name,
          latitude: data.latitude,
          longitude: data.longitude
        },
        counties: data.counties,
        lastSavedItem: _getLastSavedItem(req)
      };
    });
  };

  var _saveRegion = function _saveRegion(req) {
    var form = _getEditRegionForm(req);

    return validator.validateRegion(form).error(function(err) {
      return repository.getAllCounties(form.id).then(function(data) {
        return Promise.reject({
          region: form,
          validationErrors: err,
          counties: data ? data.counties : null,
          lastSavedItem: _getLastSavedItem(req)
        });
      });
    }).then(function() {
     return repository.persistRegion(countryId, form);
    }).then(function() {
      req.session.lastSavedItem = form.name;
    });
  };

  var _editCounty = function _editCounty(req) {
    return repository.getAllDistricts(req.params.id).then(function(data) {
      return {
        county: {
          id: data.id,
          name: data.name,
          latitude: data.latitude,
          longitude: data.longitude
        },
        districts: data.districts,
        lastSavedItem: _getLastSavedItem(req)
      };
    });
  };

  var _saveCounty = function _saveCounty(req) {
    var form = _getEditForm(req);

    return validator.validate(form).error(function(err) {
      return repository.getAllDistricts(form.id).then(function(data) {
        return Promise.reject({
          county: form,
          validationErrors: err,
          districts: data ? data.districts : null,
          lastSavedItem: _getLastSavedItem(req)
        });
      });
    }).then(function() {
     return repository.persistCounty(req.params.regionId, form);
    }).then(function() {
      req.session.lastSavedItem = form.name;
    });
  };

  var _editDistrict = function _editDistrict(req) {
    return repository.getDistrict(req.params.id).then(function(data) {
      return {
        district: data,
        lastSavedItem: _getLastSavedItem(req)
      };
    });
  };

  var _saveDistrict = function _saveDistrict(req) {
    var form = _getEditDistrictForm(req);

    return validator.validateDistrict(form).error(function(err) {
      return Promise.reject({
        district: form,
        validationErrors: err,
        lastSavedItem: _getLastSavedItem(req)
    });
    }).then(function() {
     return repository.persistDistrict(req.params.countyId, form);
    }).then(function() {
      req.session.lastSavedItem = form.name;
    });
  };

  return {
    getAllRegions: _getAllRegions,
    editRegion: _editRegion,
    saveRegion: _saveRegion,
    editCounty: _editCounty,
    saveCounty: _saveCounty,
    editDistrict: _editDistrict,
    saveDistrict: _saveDistrict
  };
};
