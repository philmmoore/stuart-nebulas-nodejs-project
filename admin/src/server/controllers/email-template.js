var Promise = require('bluebird');

module.exports = function EmailTemplateController(repository, validator) {
  var COUNTRY_ID = 1;
  
  var _getAllTemplates = function _getAllTemplates(req) {
    var lastSavedItem = req.session.lastSavedItem;
    req.session.lastSavedItem = null;

    return repository.getAllTemplates(COUNTRY_ID).then(function(data) {
      if (data) {
        data.lastSavedItem = lastSavedItem;
      }
      
      return data;
    });
  };
  
  var _getForm = function _getForm(req) {
    return {
      standardTemplate: req.body.StandardTemplate,
      emailConfirmationSubject: req.body.EmailConfirmationSubject,
      emailConfirmationHtmlBody: req.body.EmailConfirmationHtmlBody,
      emailConfirmationPlainBody: req.body.EmailConfirmationPlainBody,
      passwordResetSubject: req.body.PasswordResetSubject,
      passwordResetHtmlBody: req.body.PasswordResetHtmlBody,
      passwordResetPlainBody: req.body.PasswordResetPlainBody,
      resultRejectedSubject: req.body.ResultRejectedSubject,
      resultRejectedHtmlBody: req.body.ResultRejectedHtmlBody,
      resultRejectedPlainBody: req.body.ResultRejectedPlainBody,
      userBlockedSubject: req.body.UserBlockedSubject,
      userBlockedHtmlBody: req.body.UserBlockedHtmlBody,
      userBlockedPlainBody: req.body.UserBlockedPlainBody,
      userUnblockedSubject: req.body.UserUnblockedSubject,
      userUnblockedHtmlBody: req.body.UserUnblockedHtmlBody,
      userUnblockedPlainBody: req.body.UserUnblockedPlainBody,
      userAvatarRemovedSubject: req.body.UserAvatarRemovedSubject,
      userAvatarRemovedHtmlBody: req.body.UserAvatarRemovedHtmlBody,
      userAvatarRemovedPlainBody: req.body.UserAvatarRemovedPlainBody,
      registerReminderSubject: req.body.RegisterReminderSubject,
      registerReminderHtmlBody: req.body.RegisterReminderHtmlBody,
      registerReminderPlainBody: req.body.RegisterReminderPlainBody,
      userChallengeProgressSubject: req.body.UserChallengeProgressSubject,
      userChallengeProgressHtmlBody: req.body.UserChallengeProgressHtmlBody,
      userChallengeProgressPlainBody: req.body.UserChallengeProgressPlainBody,
      leaderboardRankingSubject: req.body.LeaderboardRankingSubject,
      leaderboardRankingHtmlBody: req.body.LeaderboardRankingHtmlBody,
      leaderboardRankingPlainBody: req.body.LeaderboardRankingPlainBody,
      userDeletedSubject: req.body.UserDeletedSubject,
      userDeletedHtmlBody: req.body.UserDeletedHtmlBody,
      userDeletedPlainBody: req.body.UserDeletedPlainBody,
      userNominatedSubject: req.body.UserNominatedSubject,
      userNominatedHtmlBody: req.body.UserNominatedHtmlBody,
      userNominatedPlainBody: req.body.UserNominatedPlainBody,
      userNominationAcceptedSubject: req.body.UserNominationAcceptedSubject,
      userNominationAcceptedHtmlBody: req.body.UserNominationAcceptedHtmlBody,
      userNominationAcceptedPlainBody: req.body.UserNominationAcceptedPlainBody,
      socialUpdateRejectedSubject: req.body.SocialUpdateRejectedSubject,
      socialUpdateRejectedHtmlBody: req.body.SocialUpdateRejectedHtmlBody,
      socialUpdateRejectedPlainBody: req.body.SocialUpdateRejectedPlainBody,
      userPasswordChangedSubject: req.body.UserPasswordChangedSubject,
      userPasswordChangedHtmlBody: req.body.UserPasswordChangedHtmlBody,
      userPasswordChangedPlainBody: req.body.UserPasswordChangedPlainBody
    };
  };
  
  var _setAllTemplates = function _setAllTemplates(req) {
    var form = _getForm(req);
    
    return validator.validate(form).catch(function(err) {
      form.validationErrors = err;
      return Promise.reject(form);
    }).then(function() {
      repository.setTemplate(COUNTRY_ID,
        form.standardTemplate,
        form.emailConfirmationSubject,
        form.emailConfirmationHtmlBody,
        form.emailConfirmationPlainBody,
        form.passwordResetSubject,
        form.passwordResetHtmlBody,
        form.passwordResetPlainBody,
        form.resultRejectedSubject,
        form.resultRejectedHtmlBody,
        form.resultRejectedPlainBody,
        form.userBlockedSubject,
        form.userBlockedHtmlBody,
        form.userBlockedPlainBody,
        form.userUnblockedSubject,
        form.userUnblockedHtmlBody,
        form.userUnblockedPlainBody,
        form.userAvatarRemovedSubject,
        form.userAvatarRemovedHtmlBody,
        form.userAvatarRemovedPlainBody,
        form.registerReminderSubject,
        form.registerReminderHtmlBody,
        form.registerReminderPlainBody,
        form.userChallengeProgressSubject,
        form.userChallengeProgressHtmlBody,
        form.userChallengeProgressPlainBody,
        form.leaderboardRankingSubject,
        form.leaderboardRankingHtmlBody,
        form.leaderboardRankingPlainBody,
        form.userDeletedSubject,
        form.userDeletedHtmlBody,
        form.userDeletedPlainBody,
        form.userNominatedSubject,
        form.userNominatedHtmlBody,
        form.userNominatedPlainBody,
        form.userNominationAcceptedSubject,
        form.userNominationAcceptedHtmlBody,
        form.userNominationAcceptedPlainBody,
        form.socialUpdateRejectedSubject,
        form.socialUpdateRejectedHtmlBody,
        form.socialUpdateRejectedPlainBody,
        form.userPasswordChangedSubject,
        form.userPasswordChangedHtmlBody,
        form.userPasswordChangedPlainBody);
    }).then(function() {
      req.session.lastSavedItem = 'Email Templates';
    });
  };

  return {
    getAllTemplates: _getAllTemplates,
    setAllTemplates: _setAllTemplates
  };
};