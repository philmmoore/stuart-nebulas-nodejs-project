var Promise = require('bluebird');

module.exports = function EmailController(repository, validator, messageQueueService, adminSite, publicSite) {
  var COUNTRY_ID = 1;
  
  var _getAllEmails = function _getAllEmails(req) {
    var lastSavedItem = req.session.lastSavedItem;
    var lastSavedItemMessage = req.session.lastSavedItemMessage;
    req.session.lastSavedItem = null;
    req.session.lastSavedItemMessage = null;
    
    return repository.getAllForCountry(COUNTRY_ID).then(function(emails) {
      return {
        emails: emails || [],
        lastSavedItem: lastSavedItem,
        lastSavedItemMessage: lastSavedItemMessage
      };
    });
  };
  
  var _getTypes = function _getTypes() {
    return repository.getTypes();
  };
  
  var _getSubmittedForm = function _getSubmittedForm(req) {
    return {
      id: req.params.id,
      name: req.body.Name,
      typeId: parseInt(req.body.Type),
      subject: req.body.Subject,
      plainBody: req.body.PlainBody,
      htmlBody: req.body.HtmlBody,
    };
  };
  
  var _saveEmail = function _saveEmail(req) {
    var form = _getSubmittedForm(req);
    
    return validator.validate(form).error(function(validationErrors) {
      return _getTypes().then(function(types) {
        return Promise.reject({
          email: form,
          types: types,
          validationErrors: validationErrors
        });
      });
    }).then(function() {
      return repository.save(COUNTRY_ID, form);
    }).then(function() {
      req.session.lastSavedItem = form.name;
    });
  };
  
  var _getEmail = function _getEmail(id) {
    return Promise.resolve([
      repository.get(id),
      _getTypes()
    ]).spread(function(email, types) {
      return {
        email: email,
        types: types
      };
    });
  };
  
  var _preview = function _preview(req) {
    return repository.get(req.params.id).then(function(emailData) {
      return repository.getAllTemplates(COUNTRY_ID).then(function(templateData) {
        var htmlBody = templateData.standardTemplate.replace(/{{SUBJECT}}/g, emailData.subject)
                                                    .replace(/{{BODY}}/g, emailData.htmlBody);

        return repository.getApprovalKey(emailData.typeId, emailData.subject, emailData.plainBody, htmlBody, emailData.name).then(function(approval) {
          var link = adminSite + 'email/approve/' + approval.key + '/';
          var footerText = '<p style="color:red;">WARNING - TO APPROVE THIS EMAIL AND SEND BULK EMAILS CLICK HERE:</p><a style="color:red;" href="' + link + '">' + link + '</a>';
          
          htmlBody = htmlBody.replace(/{{FOOTER_TEXT}}/g, footerText);
          
          var message = JSON.stringify({
            email: req.session.user,
            name: emailData.name,
            subject: emailData.subject,
            plainBody: emailData.plainBody,
            htmlBody: htmlBody          
          });
          
          return messageQueueService.sendMessage('bulk-email', message).then(function() {
            req.session.lastSavedItem = emailData.name;
            req.session.lastSavedItemMessage = 'Preview_Email_Sent';
          });
        });
      });
    });
  };

  var _sendEmail = function _sendEmail(user, emailData) {
    var link = publicSite + 'user/' + user.username + '/update/';
    var footerText = 'To change your privacy and notification preferences: <a href="' + link + '">' + link + '</a>';

    var htmlBody = emailData.htmlBody.replace(/{{FOOTER_TEXT}}/g, footerText)
                                     .replace(/{{USER_FIRST_NAME}}/g, user.firstName)
                                     .replace(/{{USER_LAST_NAME}}/g, user.lastName)
                                     .replace(/{{USER_USERNAME}}/g, user.username);
                                     
    var plainBody = emailData.plainBody.replace(/{{USER_FIRST_NAME}}/g, user.firstName)
                                       .replace(/{{USER_LAST_NAME}}/g, user.lastName)
                                       .replace(/{{USER_USERNAME}}/g, user.username);

    var message = JSON.stringify({
      email: user.email,
      name: emailData.name,
      subject: emailData.subject,
      plainBody: plainBody,
      htmlBody: htmlBody
    });

    return messageQueueService.sendMessage('bulk-email', message);
  };

  var _approve = function _approve(req) {
    return repository.getApprovedEmailData(req.params.key).then(function(emailData) {
      if (!emailData) {
        req.session.lastSavedItem = '0';
        req.session.lastSavedItemMessage = 'Bulk_Emails_Sent';
        return;
      }

      return repository.getBulkEmailUsers(req.params.key).then(function(users) {
        if (!users) {
          req.session.lastSavedItem = '0';
          req.session.lastSavedItemMessage = 'Bulk_Emails_Sent';
          return;
        }
        
        return Promise.each(users, function(user) {
          return _sendEmail(user, emailData);
        }).then(function() {
          req.session.lastSavedItem = users.length;
          req.session.lastSavedItemMessage = 'Bulk_Emails_Sent';
        });
      });
    });
  };
  
  return {
    getAllEmails: _getAllEmails,
    getTypes: _getTypes,
    saveEmail: _saveEmail,
    getEmail: _getEmail,
    preview: _preview,
    approve: _approve
  };
};