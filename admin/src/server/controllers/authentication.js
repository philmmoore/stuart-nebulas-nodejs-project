var Promise = require('bluebird');

module.exports = function AuthenticationController(repository, passwordHashService) {
  var _authenticate = function authenticate(req, callback) {
    var user = req.body.user ? req.body.user.trim() : null;
    var pass = req.body.pass;
    
    return passwordHashService.getHash(pass).then(function(hash) {
      return repository.authenticate(user, hash);
    }).then(function(result) {
      if (result === 1) {
        req.session.user = user;
        return Promise.resolve();
      }

      return Promise.reject('invalid login credentials');
    });
  };

  var _logout = function _logout(session) {
    session.destroy();
  };
  
  return {
    authenticate: _authenticate,
    logout: _logout
  };
};