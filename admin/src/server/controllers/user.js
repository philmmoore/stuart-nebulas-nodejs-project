var Promise = require('bluebird');
var moment = require('moment');

module.exports = function UserController(repository, validator, messageQueueService, removeImageUploadService, removeVideoUploadService, redisDB, publicSite) {
  var _formatUser = function _formatUser(user) {
    user.registrationDate = moment(user.registrationDate);
    user.lastLoginDate = moment(user.lastLoginDate);
    user.link = publicSite + 'user/' + user.username + '/';
    user.avatarUploaded = user.avatarUploaded === '1' ? true : false;
  };
  
  var _getFeaturedUsers = function _getFeaturedUsers(req) {
    return repository.getFeaturedUsers().then(function(users) {

      var model = {
        lastSavedItem: req.session.lastSavedItem,
        lastSavedItemMessage: req.session.lastSavedItemMessage
      };
      
      req.session.lastSavedItem = null;
      req.session.lastSavedItemMessage = null;
      
      if (!users) {
        model.users = null;
        return model;
      }

      users = users.map(function(user) {
        _formatUser(user);
        return user;
      });

      model.users = users;
      return model;
    });
  };
  

  var _getAllUsers = function _getAllUsers(req) {
    return repository.getAllUsers().then(function(users) {
      var model = {
        lastSavedItem: req.session.lastSavedItem,
        lastSavedItemMessage: req.session.lastSavedItemMessage
      };
      
      req.session.lastSavedItem = null;
      req.session.lastSavedItemMessage = null;
      
      if (!users) {
        model.users = null;
        return model;
      }

      users = users.map(function(user) {
        _formatUser(user);
        return user;
      });

      model.users = users;
      return model;
    });
  };
  
  var _get = function _get(req) {
    return repository.get(req.params.userId).then(function(data) {
      _formatUser(data);
      
      return {
        user: data
      };
    });
  };
  
  var _getSessionKeys = function _getSessionKeys(userId) {
    return repository.getSessions(userId).then(function(sessions) {
      if (!sessions) {
        return [];
      }
      
      var keys = [];
      
      for (var index = 0; index < sessions.length; index++) {
        var session = sessions[index];
        
        if (session) {
          keys.push('site-session:' + session.session);
        }
      }
      
      return keys;
    });
  };
  
  var _deleteRedisKey = function _deleteRedisKey(key) {
    return new Promise(function(resolve, reject) {
      redisDB.del(key, function(err, reply) {
        return resolve();
      });
    });
  };
  
  var _deleteUserRedisSessions = function _deleteUserRedisSessions(userId) {
    return _getSessionKeys(userId).then(function(sessionKeys) {
      return Promise.map(sessionKeys, function(sessionKey) {
        return _deleteRedisKey(sessionKey);
      });
    });
  };
  
  var _block = function _block(req) {
    var form = {
      userId: req.params.userId,
      reason: req.body.Reason
    };
    
    return validator.validate(form).catch(function(err) {
      return _get(req).then(function(data) {
        data.validationErrors = err;
        data.reason = form.reason;
        
        return Promise.reject(data);
      });
    }).then(function() {
      return repository.block(form.userId, form.reason);
    }).then(function(data) {      
      var message = JSON.stringify({
        email: data.email,
        reason: form.reason
      });

      return messageQueueService.sendMessage('user-blocked-email', message);
    }).then(function() {
      return _deleteUserRedisSessions(form.userId);
    }).then(function() {      
      req.session.lastSavedItem = req.params.userId;
      req.session.lastSavedItemMessage = 'User_Blocked';
    });
  };
  
  var _unblock = function _unblock(req) {
    return repository.unblock(req.params.userId).then(function(data) {
      var message = JSON.stringify({
        email: data.email
      });

      return messageQueueService.sendMessage('user-unblocked-email', message);
    }).then(function() {
      req.session.lastSavedItem = req.params.userId;
      req.session.lastSavedItemMessage = 'User_Unblocked';
    });
  };
  
  var _removeAvatar = function _removeAvatar(req) {
    return repository.removeAvatar(req.params.userId).then(function(data) {
      return removeImageUploadService.deleteAvatar(req.params.userId).then(function() {
        var message = JSON.stringify({
          email: data.email
        });
        
        return messageQueueService.sendMessage('user-avatar-removed-email', message);
      });
    }).then(function() {
      req.session.lastSavedItem = req.params.userId;
      req.session.lastSavedItemMessage = 'Avatar_Removed';
    });
  };
  
  var _deleteAvatar = function _deleteAvatar(userId) {
    return repository.removeAvatar(userId).then(function() {
      return removeImageUploadService.deleteAvatar(userId);
    });
  };
  
  var _removeResult = function _removeResult(result) {
    if (result.imageValidated) {
      return removeImageUploadService.deleteResultEvidence(result.id);
    }
    
    if (result.videoValidated) {
      return removeVideoUploadService.deleteResultEvidence(result.id);
    }
    
    return Promise.resolve();
  };
  
  var _delete = function _delete(req) {
    var form = {
      userId: req.params.userId,
      reason: req.body.Reason
    };
    
    var user = null;
    
    return validator.validate(form).catch(function(err) {
      return _get(req).then(function(data) {
        data.validationErrors = err;
        data.reason = form.reason;
        
        return Promise.reject(data);
      });
    }).then(function() {
      return repository.get(form.userId);
    }).then(function(userData) {
      user = userData;
      
      if (user.avatarUploaded)
      {
        return _deleteAvatar(form.userId);
      }
      
      return Promise.resolve();
    }).then(function() {
      return repository.getAllResults(form.userId);
    }).then(function(results) {
      if (!results) {
        return Promise.resolve();
      }
      
      return Promise.map(results, function(result) {
        return _removeResult(result);
      });
    }).then(function() {
      return _deleteUserRedisSessions(form.userId);
    }).then(function() {
      return repository.delete(form.userId, form.reason);
    }).then(function() {
      var message = JSON.stringify({
        email: user.email,
        reason: form.reason
      });

      return messageQueueService.sendMessage('user-deleted-email', message);
    }).then(function() {
      req.session.lastSavedItem = user.username;
      req.session.lastSavedItemMessage = 'User_Deleted';
    });
  };
  
  var _removeBio = function _removeBio(req) {
    return repository.removeBio(req.params.userId).then(function() {
      req.session.lastSavedItem = req.params.userId;
      req.session.lastSavedItemMessage = 'Bio_Removed';
    });
  };

  var _removeFeatured = function _removeFeatured(req) {
    return repository.removeFeatured(req.params.userId).then(function() {
      req.session.lastSavedItem = req.params.userId;
      req.session.lastSavedItemMessage = 'Featured_User_Removed';
    });
  };

  var _featureUser = function _featureUser(req) {
    return repository.featureUser(req.body.userId).then(function() {
      req.session.lastSavedItem = req.body.userId;
      req.session.lastSavedItemMessage = 'Featured_User';
    });
  };

  return {
    getAllUsers: _getAllUsers,
    getFeaturedUsers: _getFeaturedUsers,
    get: _get,
    block: _block,
    unblock: _unblock,
    removeAvatar: _removeAvatar,
    delete: _delete,
    removeBio: _removeBio,
    removeFeatured: _removeFeatured,
    featureUser: _featureUser
  };
};