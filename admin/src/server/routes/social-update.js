module.exports = function UserRoute(router, controller) {
  router.get('/social-updates/', function(req, res) {
    controller.getSocialUpdates(req).then(function(data) {
      res.renderView('pages/social-update/list', data);
    });
  });
  
  router.get('/social-update/:updateId/approve/', function(req, res) {
    controller.approve(req).then(function() {
      res.redirect('/social-updates/');
    });
  });
  
  router.get('/social-update/:updateId/', function(req, res) {
    controller.get(req).then(function(data) {
      res.renderView('pages/social-update/view', data);
    });
  });
  
  router.get('/social-update/:updateId/reject/', function(req, res) {
    controller.get(req).then(function(data) {
      res.renderView('pages/social-update/reject', data);
    });
  });
  
  router.post('/social-update/:updateId/reject/', function(req, res) {
    controller.reject(req).then(function() {
      res.redirect('/social-updates/');
    }).error(function(data) {
      res.renderView('pages/social-update/reject', data);
    });
  });
  
  router.get('/social-update/image/:imageId/delete/', function(req, res) {
    controller.deleteImage(req).then(function(data) {
      res.redirect('/social-updates/');
    });
  });
  
  router.get('/social-update/video/:videoId/delete/', function(req, res) {
    controller.deleteVideo(req).then(function(data) {
      res.redirect('/social-updates/');
    });
  });
};