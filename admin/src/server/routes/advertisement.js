module.exports = function AdvertisementRoute(router, controller) {
  router.get('/advertisements/', function(req, res) {
    controller.getAllAdvertisements(req.session).then(function(data) {
      res.renderView('pages/advertisement/list', data);
    });
  });

  router.get('/advertisement/new/', function(req, res) {
    controller.getNewAdvertisement(res.locals.string).then(function(data) {
      res.renderView('pages/advertisement/new', data);
    });
  });

  router.post('/advertisement/new/', function(req, res) {
    controller.saveAdvertisement(req).then(function() {
      res.redirect('/advertisements/');
    }).error(function(data) {
      return res.renderView('pages/advertisement/new', data);
    });
  });

  router.get('/advertisement/:id', function(req, res) {
    controller.getAdvertisement(req).then(function(data) {
      res.renderView('pages/advertisement/edit', data);
    });
  });

  router.post('/advertisement/:id', function(req, res) {
    controller.saveAdvertisement(req).then(function(result) {
      res.redirect('/advertisements/');
    }).error(function(data) {
      return res.renderView('pages/advertisement/edit', data);
    });
  });
};
