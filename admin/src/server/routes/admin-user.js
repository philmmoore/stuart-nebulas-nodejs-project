module.exports = function AdminUserRoute(router, controller) {
  router.get('/admin-users/', function(req, res) {
    controller.getAllUsers(req).then(function(data) {
      res.renderView('pages/admin-user/list', data);
    });
  });
  
  router.get('/admin-user/new/', function(req, res) {
    res.renderView('pages/admin-user/new', {user: {}});
  });
  
  router.post('/admin-user/new/', function(req, res) {
    controller.save(req).then(function() {
      res.redirect('/admin-users/');
    }).error(function(data) {
      return res.renderView('pages/admin-user/new', data);
    });
  });
  
  router.get('/admin-user/:userId/delete/', function(req, res) {
    controller.get(req).then(function(data) {
      res.renderView('pages/admin-user/delete', data);
    });
  });
  
  router.post('/admin-user/:userId/delete/', function(req, res) {
    controller.delete(req).then(function() {
      res.redirect('/admin-users/');
    }).error(function(data) {
      res.renderView('pages/admin-user/delete', data);
    });
  });
};