module.exports = function ChallengeRoute(router, controller) {
  router.get('/challenges/', function(req, res) {
    controller.getAllChallenges(req.session, res.locals.string).then(function(data) {
      res.renderView('pages/challenge/list', data);
    });
  });

  router.get('/challenge/new/', function(req, res) {
    controller.getNewChallenge(res.locals.string).then(function(data) {
      res.renderView('pages/challenge/new', data);
    });
  });

  router.post('/challenge/new/', function(req, res) {
    controller.saveChallenge(req, res.locals.string).then(function() {
      res.redirect('/challenges/');
    }).error(function(data) {
      return res.renderView('pages/challenge/new', data);
    });
  });

  router.get('/challenge/:id/', function(req, res) {
    controller.getChallenge(parseInt(req.params.id), res.locals.string).then(function(data) {
      res.renderView('pages/challenge/edit', data);
    });
  });

  router.post('/challenge/:id/', function(req, res) {
    controller.saveChallenge(req, res.locals.string).then(function() {
      res.redirect('/challenges/');
    }).error(function(data) {
      return res.renderView('pages/challenge/edit', data);
    });
  });
  
  router.get('/challenge/:id/delete/', function(req, res) {
    controller.getChallenge(parseInt(req.params.id), res.locals.string).then(function(data) {
      res.renderView('pages/challenge/delete', data);
    });
  });
  
  router.post('/challenge/:id/delete/', function(req, res) {
    controller.deleteChallenge(req).then(function() {
      res.redirect('/challenges/');
    });
  });
};
