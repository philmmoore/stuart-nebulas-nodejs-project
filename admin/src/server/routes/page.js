module.exports = function PageRoute(router, controller) {
  router.get('/pages/', function(req, res) {
    controller.getAllPages(req.session).then(function(data) {
      res.renderView('pages/page/list', data);
    });
  });

  router.get('/page/new/', function(req, res) {
    res.renderView('pages/page/new', {
      page: {}
    });
  });

  router.post('/page/new/', function(req, res) {
    controller.savePage(req).then(function() {
      res.redirect('/pages/');
    }).error(function(data) {
        res.renderView('pages/page/new', data);
    });
  });

  router.get('/page/:id/', function(req, res) {
    controller.getPage(parseInt(req.params.id)).then(function(data) {
      res.renderView('pages/page/edit', data);
    });
  });

  router.post('/page/:id/', function(req, res) {
    controller.savePage(req).then(function() {
      res.redirect('/pages/');
    }).error(function(data) {
      res.renderView('pages/page/edit', data);
    });
  });
  
  router.get('/page/:id/delete/', function(req, res) {
    controller.getPage(parseInt(req.params.id)).then(function(data) {
      res.renderView('pages/page/delete', data);
    });
  });
  
  router.post('/page/:id/delete/', function(req, res) {
    controller.deletePage(req).then(function() {
      res.redirect('/pages/');
    });
  });
};
