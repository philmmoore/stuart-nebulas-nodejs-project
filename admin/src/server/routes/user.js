module.exports = function UserRoute(router, controller) {
  router.get('/users/', function(req, res) {
    controller.getAllUsers(req).then(function(data) {
      return data;
    }).then(function(users){

      var data = {
        "users" : users,
        "featured" : []
      };

      controller.getFeaturedUsers(req).then(function(featured){
        data.featured = featured;
        res.renderView('pages/user/list', data);
      });
      
    });
  });
  
  router.get('/user/:userId/remove-avatar/', function(req, res) {
    controller.removeAvatar(req).then(function() {
      res.redirect('/users/');
    });
  });
  
  router.get('/user/:userId/remove-bio/', function(req, res) {
    controller.removeBio(req).then(function() {
      res.redirect('/users/');
    });
  });
  
  router.get('/user/:userId/unblock/', function(req, res) {
    controller.unblock(req).then(function() {
      res.redirect('/users/');
    });
  });
  
  router.get('/user/:userId/block/', function(req, res) {
    controller.get(req).then(function(data) {
      res.renderView('pages/user/block', data);
    });
  });
  
  router.post('/user/:userId/block/', function(req, res) {
    controller.block(req).then(function() {
      res.redirect('/users/');
    }).error(function(data) {
      res.renderView('pages/user/block', data);
    });
  });
  
  router.get('/user/:userId/delete/', function(req, res) {
    controller.get(req).then(function(data) {
      res.renderView('pages/user/delete', data);
    });
  });
  
  router.post('/user/:userId/delete/', function(req, res) {
    controller.delete(req).then(function() {
      res.redirect('/users/');
    }).error(function(data) {
      res.renderView('pages/user/delete', data);
    });
  });

  router.get('/user/featured/:userId/delete/', function(req, res) {
    controller.removeFeatured(req).then(function() {
      res.redirect('/users/');
    });
  });

  router.get('/user/featured/new/', function(req, res) {
    controller.getAllUsers(req).then(function(data) {
      res.renderView('pages/user/feature', data);
    });
  });

  router.post('/user/featured/new/', function(req, res) {
    controller.featureUser(req).then(function(data){
      controller.getAllUsers(req).then(function(data) {
        res.redirect('/users/');
      });    
    });
  });

};