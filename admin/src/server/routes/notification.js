module.exports = function NotificationRoute(router, controller) {
  router.get('/notifications/', function(req, res) {
    controller.getAllNotifications(req.session).then(function(data) {
      res.renderView('pages/notification/list', data);
    });
  });

  router.get('/notification/new/', function(req, res) {
    controller.getNewNotification().then(function(data) {
      res.renderView('pages/notification/new', data);
    });
  });

  router.post('/notification/new/', function(req, res) {
    controller.saveNotification(req).then(function() {
      res.redirect('/notifications/');
    }).error(function(data) {
      res.renderView('pages/notification/new', data);
    });
  });

  router.get('/notification/:id/', function(req, res) {
    controller.getNotification(req).then(function(data) {
      res.renderView('pages/notification/edit', data);
    });
  });

  router.post('/notification/:id/', function(req, res) {
    controller.saveNotification(req).then(function() {
      res.redirect('/notifications/');
    }).error(function(data) {
      return res.renderView('pages/notification/edit', data);
    });
  });
};
