module.exports = function DashboardRoute(router, controller) {
  router.get('/', function(req, res) {
    controller.getData().then(function(data) {
      res.renderView('pages/dashboard/home', data);
    });
  });
};