module.exports = function UserRoute(router, controller) {
  router.get('/results/', function(req, res) {
    controller.getFlaggedResults(req).then(function(data) {
      res.renderView('pages/result/list', data);
    });
  });
  
  router.get('/result/:resultId/approve/', function(req, res) {
    controller.approve(req).then(function() {
      res.redirect('/results/');
    });
  });
  
  router.get('/result/:resultId/reject/', function(req, res) {
    controller.get(req).then(function(data) {
      res.renderView('pages/result/reject', data);
    });
  });
  
  router.post('/result/:resultId/reject/', function(req, res) {
    controller.reject(req).then(function() {
      res.redirect('/results/');
    }).error(function(data) {
      res.renderView('pages/result/reject', data);
    });
  });
};