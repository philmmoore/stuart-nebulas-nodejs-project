module.exports = function EmailRoute(router, controller) {
  router.get('/emails/', function(req, res) {
    controller.getAllEmails(req).then(function(data) {
      res.renderView('pages/email/list', data);
    });
  });
  
  router.get('/email/new/', function(req, res) {
    controller.getTypes().then(function(types) {
      res.renderView('pages/email/new', {
        email: {},
        types: types
      });
    });
  });

  router.post('/email/new/', function(req, res) {
    controller.saveEmail(req).then(function() {
      res.redirect('/emails/');
    }).catch(function(data) {
      return res.renderView('pages/email/new', data);
    });
  });

  router.get('/email/:id/', function(req, res) {
    controller.getEmail(parseInt(req.params.id)).then(function(data) {
      res.renderView('pages/email/edit', data);
    });
  });

  router.post('/email/:id/', function(req, res) {
    controller.saveEmail(req).then(function() {
      res.redirect('/emails/');
    }).catch(function(data) {
      return res.renderView('pages/email/edit', data);
    });
  });
  
  router.get('/email/:id/preview/', function(req, res) {
    controller.preview(req).then(function(data) {
      res.redirect('/emails/');
    });
  });
  
  router.get('/email/approve/:key/', function(req, res) {
    controller.approve(req).then(function(data) {
      res.redirect('/emails/');
    });
  });
};
