module.exports = function ConfigurationRoute(router, controller) {
  router.get('/configuration/', function(req, res) {
    controller.getConfiguration(req).then(function(data) {
      res.renderView('pages/configuration/edit', data);
    });
  });

  router.post('/configuration/', function(req, res) {
    controller.setConfiguration(req).then(function(result) {
      res.redirect('/configuration/');
    }).error(function(data) {
      return res.renderView('pages/configuration/edit', data);
    });
  });
};
