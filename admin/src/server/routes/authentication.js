var express = require('express');
var AuthenticationController = require('../controllers/authentication');

module.exports = function AuthenticationRoute(router, controller) {
  router.get('/login/', function(req, res) {
    if (!req.session.user) {
      return res.renderView('pages/login');
    }

    return res.redirect('/');
  });

  router.get('/logout/', function(req, res) {
    controller.logout(req.session);
    res.redirect('/login/');
  });

  router.post('/login/', function(req, res) {
    controller.authenticate(req).then(function() {
      res.redirect('/');
    }).catch(function(error) {
      res.renderView('pages/login', {
        error: true,
        user: req.body.user
      });
    });
  });
};