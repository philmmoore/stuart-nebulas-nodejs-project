var moment = require('moment');

module.exports = function BlogRoute(router, controller) {
  router.get('/blog-posts/', function(req, res) {
    controller.getAllPosts(req.session).then(function(data) {
      res.renderView('pages/blog/list', data);
    });
  });

  router.get('/blog-posts/new/', function(req, res) {
    res.renderView('pages/blog/new', {
      post: {
        published: moment()
      }
    });
  });

  router.post('/blog-posts/new/', function(req, res) {
    controller.savePost(req).then(function() {
      res.redirect('/blog-posts/');
    }).error(function(data) {
      return res.renderView('pages/blog/new', data);
    });
  });

  router.get('/blog-posts/:id/', function(req, res) {
    controller.getPost(parseInt(req.params.id)).then(function(data) {
      res.renderView('pages/blog/edit', data);
    });
  });

  router.post('/blog-posts/:id/', function(req, res) {
    controller.savePost(req).then(function() {
      res.redirect('/blog-posts/');
    }).error(function(data) {
      return res.renderView('pages/blog/edit', data);
    });
  });
  
  router.get('/blog-posts/:id/delete/', function(req, res) {
    controller.getPost(parseInt(req.params.id)).then(function(data) {
      res.renderView('pages/blog/delete', data);
    });
  });
  
  router.post('/blog-posts/:id/delete/', function(req, res) {
    controller.deletePost(req).then(function() {
      res.redirect('/blog-posts/');
    });
  });
};
