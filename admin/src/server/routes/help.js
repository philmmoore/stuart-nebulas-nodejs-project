module.exports = function HelpRoute(router) {
  router.get('/help/', function(req, res) {
    res.renderView('pages/help/list');
  });
};
