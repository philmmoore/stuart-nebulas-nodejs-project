module.exports = function AreaRoute(router, controller) {
  router.get('/regions/', function(req, res) {
    controller.getAllRegions(req).then(function(data) {
      res.renderView('pages/area/list-regions', data);
    });
  });

  router.get('/region/new/', function(req, res) {
    res.renderView('pages/area/new-region', {
      region: {}
    });
  });

  router.post('/region/new/', function(req, res) {
    controller.saveRegion(req).then(function(data) {
      res.redirect('/regions/');
    }).error(function(data) {
      res.renderView('pages/area/new-region', data);
    });
  });

  router.get('/region/:id/', function(req, res) {
    controller.editRegion(req).then(function(data) {
      res.renderView('pages/area/edit-region', data);
    });
  });

  router.post('/region/:id/', function(req, res) {
    controller.saveRegion(req).then(function(data) {
      res.redirect('/regions/');
    }).error(function(data) {
      res.renderView('pages/area/edit-region', data);
    });
  });

  router.get('/region/:regionId/county/new/', function(req, res) {
    res.renderView('pages/area/new-county', {
      county: {}
    });
  });

  router.post('/region/:regionId/county/new/', function(req, res) {
    controller.saveCounty(req).then(function(data) {
      res.redirect('../../');
    }).error(function(data) {
      res.renderView('pages/area/new-county', data);
    });
  });

  router.get('/region/:regionId/county/:id/', function(req, res) {
    controller.editCounty(req).then(function(data) {
      res.renderView('pages/area/edit-county', data);
    });
  });

  router.post('/region/:regionId/county/:id/', function(req, res) {
    controller.saveCounty(req).then(function(data) {
      res.redirect('../../');
    }).error(function(data) {
      res.renderView('pages/area/edit-county', data);
    });
  });

  router.get('/region/:regionId/county/:countyId/district/new/', function(req, res) {
    res.renderView('pages/area/new-district', {
      district: {}
    });
  });

  router.post('/region/:regionId/county/:countyId/district/new/', function(req, res) {
    controller.saveDistrict(req).then(function(data) {
      res.redirect('../../');
    }).error(function(data) {
      res.renderView('pages/area/new-district', data);
    });
  });

  router.get('/region/:regionId/county/:countyId/district/:id/', function(req, res) {
    controller.editDistrict(req).then(function(data) {
      res.renderView('pages/area/edit-district', data);
    });
  });

  router.post('/region/:regionId/county/:countyId/district/:id/', function(req, res) {
    controller.saveDistrict(req).then(function(data) {
      res.redirect('../../');
    }).error(function(data) {
      res.renderView('pages/area/edit-district', data);
    });
  });
};
