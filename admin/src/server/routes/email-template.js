module.exports = function EmailTemplateRoute(router, controller) {
  router.get('/email-templates/', function(req, res) {
    controller.getAllTemplates(req).then(function(data) {
      res.renderView('pages/email-template/edit', data);
    });
  });
  
  router.post('/email-templates/', function(req, res) {
    controller.setAllTemplates(req).then(function(result) {
      res.redirect('/email-templates/');
    }).error(function(data) {
      return res.renderView('pages/email-template/edit', data);
    });
  });
};
