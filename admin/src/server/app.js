if (process.env.NEW_RELIC_LICENSE_KEY) {
  require('newrelic');
}

var RedisDB = require('./db/redis-db');
var PostgresDB = require('./db/postgres-db');
var Server = require('./server');
var Config = require('./config');
var IoC = require('./ioc');

var logger = console;

process.on('uncaughtException', function(err) {
  logger.log('Caught exception:', err);
});

var config = new Config();
logger.log('Environment: %s.', config.isProduction ? 'production' : 'development');

var redisDB = new RedisDB(config.redisURL, logger);
redisDB.connect(function connectedToRedisDB(redisClient) {
  var postgresDB = new PostgresDB(config, logger);
  postgresDB.connect(function connectedToPostgresDB(db) {
    var ioc = new IoC(config, redisClient, db, logger);
    var server = new Server(ioc);
    server.listen(config.port);
  });
});
