var Promise = require('bluebird');

module.exports = function ImageUploadRepository(config, logger) {
  var AWS = require('aws-sdk');
  AWS.config.update(config.imageUploadConfig);
  var s3 = new AWS.S3();

  var _upload = function _upload(image, bucketName) {
    return new Promise(function(resolve, reject) {
      var params = {
        Bucket: config.awsResourcePrefix + bucketName,
        Key: image.name,
        Body: image.data,
        ContentType: image.mimeType
      };

      s3.upload(params, function(err, data) {
        if (err) {
          return reject(err);
        }

        return resolve();
      });
    });
  };

  return {
    upload: _upload
  };
};
