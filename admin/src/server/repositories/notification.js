module.exports = function NotificationRepository(db) {
  db.registerParameters('notification_get', [ 'int' ]);
  db.registerParameters('notification_save', [ 'int', 'int', 'bit', 'text', 'text' ]);
  
  var _getAllNotifications = function getAllNotifications() {
    return db.queryJson('notification_get_all');
  };

  var _getNotification = function getNotification(notificationId) {
    return db.queryJson('notification_get', [ notificationId ]);
  };

  var _getNotificationTypes = function getNotificationTypes() {
    return db.queryJson('notification_get_types');
  };

  var _persistNotification = function persistNotification(notification) {
    var params = [
      notification.id,
      notification.typeId,
      notification.isUserDismissable ? 1 : 0,
      notification.title,
      notification.description
    ];

    return db.queryJson('notification_save', params);
  };

  return {
    getAllNotifications: _getAllNotifications,
    getNotification: _getNotification,
    getNotificationTypes: _getNotificationTypes,
    persistNotification: _persistNotification
  };
};
