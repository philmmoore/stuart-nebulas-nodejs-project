module.exports = function AreaRepository(db) {
  db.registerParameters('area_get_all_regions', [ 'int' ]);
  db.registerParameters('area_save_region', [ 'int', 'int', 'text', 'numeric', 'numeric']);
  db.registerParameters('area_get_all_counties', [ 'int' ]);
  db.registerParameters('area_save_county', [ 'int', 'int', 'text' ]);
  db.registerParameters('area_get_all_districts', [ 'int' ]);
  db.registerParameters('area_get_district', [ 'int' ]);
  db.registerParameters('area_save_district', [ 'int', 'int', 'text', 'numeric', 'numeric' ]);
  
  var _getAllRegions = function getAllRegions(countryId) {
    return db.queryJson('area_get_all_regions', [ countryId ]);
  };

  var _persistRegion = function _persistRegion(countryId, region) {
    var params = [
      countryId,
      region.id,
      region.name,
      region.latitude,
      region.longitude,
    ];

    return db.queryJson('area_save_region', params);
  };

  var _getAllCounties = function getAllCounties(regionId) {
    return db.queryJson('area_get_all_counties', [ regionId ]);
  };

  var _persistCounty = function _persistCounty(regionId, county) {
    var params = [
      regionId,
      county.id,
      county.name
    ];

    return db.queryJson('area_save_county', params);
  };

  var _getAllDistricts = function getAllDistricts(countyId) {
    return db.queryJson('area_get_all_districts', [ countyId ]);
  };

  var _getDistrict = function getDistrict(districtId) {
    return db.queryJson('area_get_district', [ districtId ]);
  };

  var _persistDistrict = function _persistDistrict(countyId, district) {
    var params = [
      countyId,
      district.id,
      district.name,
      district.latitude,
      district.longitude
    ];

    return db.queryJson('area_save_district', params);
  };

  return {
    getAllRegions: _getAllRegions,
    persistRegion: _persistRegion,
    getAllCounties: _getAllCounties,
    persistCounty: _persistCounty,
    getAllDistricts: _getAllDistricts,
    getDistrict: _getDistrict,
    persistDistrict: _persistDistrict
  };
};
