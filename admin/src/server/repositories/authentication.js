module.exports = function AuthenticationRepository(db) {
  db.registerParameters('user_login', [ 'text', 'bytea' ]);
  
  var _authenticate = function _authenticate(user, hash) {
    return db.queryJson('user_login', [ user, hash ]);
  };
  
  return {
    authenticate: _authenticate
  };
};