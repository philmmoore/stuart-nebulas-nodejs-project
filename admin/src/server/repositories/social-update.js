module.exports = function ResultRepository(db) {
  db.registerParameters('social_update_get', [ 'uuid' ]);
  db.registerParameters('social_update_approve', [ 'uuid' ]);
  db.registerParameters('social_update_delete_image', [ 'uuid' ]);
  db.registerParameters('social_update_delete_video', [ 'uuid' ]);
  db.registerParameters('social_update_reject', [ 'uuid', 'text' ]);
  
  var _getSocialUpdates = function _getSocialUpdates() {
    return db.queryJson('social_update_get_all');
  };
  
  var _get = function _get(updateId) {
    return db.queryJson('social_update_get', [ updateId ]);
  };
  
  var _approve = function _approve(updateId) {
    return db.queryJson('social_update_approve', [ updateId ]);
  };
  
  var _deleteImage = function _deleteImage(imageId) {
    return db.queryJson('social_update_delete_image', [ imageId ]);
  };
  
  var _deleteVideo = function _deleteVideo(videoId) {
    return db.queryJson('social_update_delete_video', [ videoId ]);
  };
  
  var _reject = function _reject(updateId, reason) {
    return db.queryJson('social_update_reject', [ updateId, reason ]);
  };
  
  return {
    getSocialUpdates: _getSocialUpdates,
    get: _get,
    approve: _approve,
    deleteImage: _deleteImage,
    deleteVideo: _deleteVideo,
    reject: _reject
  };
};