module.exports = function UserRepository(db) {
  db.registerParameters('user_get', [ 'int' ]);
  db.registerParameters('user_block', [ 'int', 'text' ]);
  db.registerParameters('user_get_sessions', [ 'int' ]);
  db.registerParameters('user_unblock', [ 'int' ]);
  db.registerParameters('user_remove_avatar', [ 'int' ]);
  db.registerParameters('user_remove_bio', [ 'int' ]);
  db.registerParameters('user_delete', [ 'int', 'text' ]);
  db.registerParameters('user_get_all_results', [ 'int' ]);
  db.registerParameters('user_featured_get', []);
  db.registerParameters('user_featured_delete', ['int']);
  db.registerParameters('user_featured_persist', ['int']);
  
  var _getAllUsers = function getAllUsers() {
    return db.queryJson('user_get_all');
  };
  
  var _get = function get(userId) {
    return db.queryJson('user_get', [ userId ]);
  };
  
  var _block = function _block(userId, reason) {
    return db.queryJson('user_block', [ userId, reason ]);
  };
  
  var _getSessions = function _getSessions(userId) {
    return db.queryJson('user_get_sessions', [ userId, ]);
  };
  
  var _unblock = function _unblock(userId) {
    return db.queryJson('user_unblock', [ userId ]);
  };
  
  var _removeAvatar = function _removeAvatar(userId) {
    return db.queryJson('user_remove_avatar', [ userId ]);
  };
  
  var _removeBio = function _removeBio(userId) {
    return db.queryJson('user_remove_bio', [ userId ]);
  };
  
  var _delete = function _delete(userId, reason) {
    return db.queryJson('user_delete', [ userId, reason ]);
  };
  
  var _getAllResults = function _getAllResults(userId) {
    return db.queryJson('user_get_all_results', [ userId ]);
  };

  var _getFeaturedUsers = function _getFeaturedUsers() {
    return db.queryJson('user_featured_get');
  };

  var _removeFeatured = function _removeFeatured(userId) {
    return db.queryJson('user_featured_delete', [ userId ]);
  };

  var _featureUser = function _featureUser(userId) {
    return db.queryJson('user_featured_persist', [ userId ]);
  };

  return {
    getAllUsers: _getAllUsers,
    get: _get,
    block: _block,
    getSessions: _getSessions,
    unblock: _unblock,
    removeAvatar: _removeAvatar,
    removeBio: _removeBio,
    delete: _delete,
    getAllResults: _getAllResults,
    getFeaturedUsers: _getFeaturedUsers,
    removeFeatured: _removeFeatured,
    featureUser: _featureUser
  };
};