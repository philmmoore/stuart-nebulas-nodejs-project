module.exports = function AdvertisementRepository(db) {
  db.registerParameters('get_advertisements', [ 'int' ]);
  db.registerParameters('get_advertisement', [ 'int' ]);
  db.registerParameters('get_country_selection', [ 'int', 'int' ]);
  db.registerParameters('get_all_regions_for_country', [ 'int' ]);
  db.registerParameters('get_all_counties_for_region', [ 'int' ]);
  db.registerParameters('get_all_districts_for_county', [ 'int' ]);
  db.registerParameters('advert_save', [ 'int', 'int', 'text', 'text', 'bit', 'int[]', 'int[]', 'int[]' ]);
  db.registerParameters('advert_attach_image', [ 'int', 'text', 'text' ]);
  
  var _getAllAdvertisements = function _getAllAdvertisements(countryId) {
    return db.queryJson('get_advertisements', [ countryId ]);
  };

  var _getAdvertisement = function _getAdvertisement(advertisementId) {
    return db.queryJson('get_advertisement', [ advertisementId ]);
  };

  var _getCountrySelection = function _getCountrySelection(countryId, advertisementId) {
    return db.queryJson('get_country_selection', [ countryId, advertisementId ]);
  };

  var _getAllRegions = function _getAllRegions(countryId) {
    return db.queryJson('get_all_regions_for_country', [ countryId ]);
  };

  var _getAllCounties = function _getAllCounties(regionId) {
    return db.queryJson('get_all_counties_for_region', [ regionId ]);
  };

  var _getAllDistricts = function _getAllDistricts(countyId) {
    return db.queryJson('get_all_districts_for_county', [ countyId ]);
  };

  var _persistAdvertisement = function _persistAdvertisement(countryId, advertisement) {
    var params = [
      countryId,
      advertisement.id,
      advertisement.name,
      advertisement.link,
      advertisement.countrySelected ? 1 :0,
      advertisement.regions,
      advertisement.counties,
      advertisement.districts
    ];

    return db.queryJson('advert_save', params);
  };

  var _associateImageWithAdvertisement = function _associateImageWithAdvertisement(advertId, type, extension) {
    return db.queryJson('advert_attach_image', [ advertId, type, extension ]);
  };

  return {
    getAllAdvertisements: _getAllAdvertisements,
    getAdvertisement: _getAdvertisement,
    getCountrySelection: _getCountrySelection,
    getAllRegions: _getAllRegions,
    getAllCounties: _getAllCounties,
    getAllDistricts: _getAllDistricts,
    persistAdvertisement: _persistAdvertisement,
    associateImageWithAdvertisement: _associateImageWithAdvertisement
  };
};
