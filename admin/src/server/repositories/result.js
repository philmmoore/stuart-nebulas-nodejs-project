module.exports = function ResultRepository(db) {
  db.registerParameters('result_approve', [ 'int' ]);
  db.registerParameters('result_get', [ 'int' ]);
  db.registerParameters('result_reject', [ 'int', 'text' ]);
  
  var _getFlaggedResults = function _getFlaggedResults() {
    return db.queryJson('result_flagged');
  };
  
  var _approve = function _approve(resultId) {
    return db.queryJson('result_approve', [ resultId ]);
  };
  
  var _get = function _get(resultId) {
    return db.queryJson('result_get', [ resultId ]);
  };
  
  var _reject = function _reject(resultId, reason) {
    return db.queryJson('result_reject', [ resultId, reason ]);
  };

  return {
    getFlaggedResults: _getFlaggedResults,
    approve: _approve,
    get: _get,
    reject: _reject
  };
};