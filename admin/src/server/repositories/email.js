module.exports = function EmailRepository(db) {
  db.registerParameters('email_get_all_for_country', [ 'int' ]);
  db.registerParameters('email_save', [ 'int', 'int', 'text', 'int', 'text', 'text', 'text' ]);
  db.registerParameters('email_get', [ 'int' ]);
  db.registerParameters('email_template_get', [ 'int' ]);
  db.registerParameters('email_get_approval_key', [ 'int', 'text', 'text', 'text', 'text' ]);
  db.registerParameters('email_get_approved_email', [ 'uuid' ]);
  db.registerParameters('email_get_all_users', [ 'uuid' ]);
  
  var _getAllForCountry = function _getAllForCountry(countryId) {
    return db.queryJson('email_get_all_for_country', [ countryId ]);
  };
  
  var _getTypes = function _getTypes() {
    return db.queryJson('email_get_types');
  };
  
  var _save = function _save(countryId, email) {
    var params = [
      countryId,
      email.id,
      email.name,
      email.typeId,
      email.subject,
      email.plainBody,
      email.htmlBody
    ];
    
    return db.queryJson('email_save', params);
  };
  
  var _get = function _get(id) {
    return db.queryJson('email_get', [ id ]);
  };
  
  var _getAllTemplates = function _getAllTemplates(countryId) {
    return db.queryJson('email_template_get', [ countryId ]);
  };
  
  var _getApprovalKey = function _getApprovalKey(typeId, subject, plainBody, htmlBody, name) {
    var params = [
      typeId,
      subject,
      plainBody,
      htmlBody,
      name
    ];
    
    return db.queryJson('email_get_approval_key', params);
  };
  
  var _getApprovedEmailData = function _getApprovedEmailData(key) {
    return db.queryJson('email_get_approved_email', [ key ]);
  };
  
  var _getBulkEmailUsers = function _getBulkEmailUsers(key) {
    return db.queryJson('email_get_all_users', [ key ]);
  };
  
  return {
    getAllForCountry: _getAllForCountry,
    getTypes: _getTypes,
    save: _save,
    get: _get,
    getAllTemplates: _getAllTemplates,
    getApprovalKey: _getApprovalKey,
    getApprovedEmailData: _getApprovedEmailData,
    getBulkEmailUsers: _getBulkEmailUsers
  };
};