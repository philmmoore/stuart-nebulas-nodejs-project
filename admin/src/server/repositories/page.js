module.exports = function PageRepository(db) {
  db.registerParameters('get_all_pages_for_country', [ 'int' ]);
  db.registerParameters('get_page', [ 'int' ]);
  db.registerParameters('save_page', [ 'int', 'int', 'text', 'text' ]);
  db.registerParameters('page_delete', [ 'int' ]);
  
  var _getAllPages = function _getAllPages(countryId) {
    return db.queryJson('get_all_pages_for_country', [ countryId ]);
  };

  var _getPage = function _getPage(pageId) {
    return db.queryJson('get_page', [ pageId ]);
  };

  var _persistPage = function _persistPage(page) {
    var params = [
      1,
      page.id,
      page.name,
      page.bodyCopy
    ];

    return db.queryJson('save_page', params);
  };
  
  var _deletePage = function _deletePage(pageId) {
    return db.queryJson('page_delete', [ pageId ]);
  };

  return {
    getAllPages: _getAllPages,
    getPage: _getPage,
    persistPage: _persistPage,
    deletePage: _deletePage
  };
};
