module.exports = function AdminUserRepository(db) {
  db.registerParameters('admin_user_get', [ 'int' ]);
  db.registerParameters('admin_user_delete', [ 'int' ]);
  db.registerParameters('admin_user_persist', [ 'int', 'text', 'bytea' ]);
  
  var _getAllUsers = function getAllUsers() {
    return db.queryJson('admin_user_get_all');
  };
  
  var _get = function get(userId) {
    return db.queryJson('admin_user_get', [ userId ]);
  };
  
  var _delete = function _delete(userId) {
    return db.queryJson('admin_user_delete', [ userId ]);
  };
  
  var _persist = function _persist(countryId, username, hash) {
    return db.queryJson('admin_user_persist', [ countryId, username, hash ]);
  };
  
  return {
    getAllUsers: _getAllUsers,
    get: _get,
    delete: _delete,
    persist: _persist
  };
};