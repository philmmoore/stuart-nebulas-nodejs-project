module.exports = function BlogRepository(db) {
  var COUNTRY_ID = 1;
  
  db.registerParameters('blog_get_all_posts_for_country', [ 'int' ]);
  db.registerParameters('blog_get_post', [ 'int' ]);
  db.registerParameters('blog_save_post', [ 'int', 'int', 'timestamp', 'text', 'text', 'text' ]);
  db.registerParameters('blog_post_delete', [ 'int' ]);
  
  var _getAllPosts = function _getAllPosts(countryId) {
    return db.queryJson('blog_get_all_posts_for_country', [ countryId ]);
  };

  var _getPost = function _getPage(postId) {
    return db.queryJson('blog_get_post', [ postId ]);
  };

  var _persistPost = function _persistPost(post) {
    var params = [
      COUNTRY_ID,
      post.id,
      post.published,
      post.name,
      post.bodyCopy,
      post.summary
    ];

    return db.queryJson('blog_save_post', params);
  };
  
  var _deletePost = function _deletePost(postId) {
    return db.queryJson('blog_post_delete', [ postId ]);
  };

  return {
    getAllPosts: _getAllPosts,
    getPost: _getPost,
    persistPost: _persistPost,
    deletePost: _deletePost
  };
};
