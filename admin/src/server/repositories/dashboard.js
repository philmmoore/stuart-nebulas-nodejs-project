module.exports = function DashboardRepository(db) {
  var _getLatestResults = function _getLatestResults() {
    return db.queryJson('home_latest_results');
  };
  
  var _getChartData = function _getChartData() {
    return db.queryJson('dashboard_chart');
  };
  
  var _getCharityData = function _getCharityData() {
    return db.queryJson('get_charity_donation_counts');
  };

  return {
    getLatestResults: _getLatestResults,
    getChartData: _getChartData,
    getCharityData: _getCharityData
  };
};