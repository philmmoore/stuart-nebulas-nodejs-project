module.exports = function ChallengeRepository(db) {
  db.registerParameters('challenge_get_challenge', [ 'int' ]);
  db.registerParameters('challenge_save', [ 'int', 'text', 'int', 'int', 'int[]', 'text', 'text', 'bool', 'bool' ]);
  db.registerParameters('challenge_delete', [ 'int' ]);
  
  var _getAllChallenges = function getAllChallenges() {
    return db.queryJson('challenge_get_challenges');
  };

  var _getChallenge = function getChallenge(challengeId) {
    return db.queryJson('challenge_get_challenge', [ challengeId ]);
  };

  var _getGroups = function getGroups() {
    return db.queryJson('challenge_get_groups');
  };

  var _getTypes = function _getTypes() {
    return db.queryJson('challenge_get_types');
  };
  
  var _getCategories = function _getCategories() {
    return db.queryJson('challenge_get_categories');
  };

  var _persistChallenge = function persistChallenge(challenge) {
    var params = [
      challenge.id,
      challenge.name,
      challenge.categoryId,
      challenge.typeId,
      challenge.groupIds,
      challenge.bodyCopy,
      challenge.submissionMessage,
      challenge.hidden,
      challenge.comingSoon
    ];

    return db.queryJson('challenge_save', params);
  };
  
  var _deleteChallenge = function _deleteChallenge(challengeId) {
    return db.queryJson('challenge_delete', [ challengeId ]);
  };

  return {
    getAllChallenges: _getAllChallenges,
    getChallenge: _getChallenge,
    getGroups: _getGroups,
    getTypes: _getTypes,
    getCategories: _getCategories,
    persistChallenge: _persistChallenge,
    deleteChallenge: _deleteChallenge
  };
};
