module.exports = function ConfigurationRepository(db) {
  db.registerParameters('configuration_get', [ 'int' ]);
  db.registerParameters('configuration_set', [ 'int', 'json' ]);
  
  var _getConfiguration = function _getConfiguration(countryId) {
    return db.queryJson('configuration_get', [ countryId ]);
  };

  var _setConfiguration = function _setConfiguration(countryId, configuration) {
    return db.queryJson('configuration_set', [ countryId, configuration ]);
  };

  return {
    getConfiguration: _getConfiguration,
    setConfiguration: _setConfiguration
  };
};
