var Promise = require('bluebird');

module.exports = function RemoveVideoUploadService(config, logger) {
  var AWS = require('aws-sdk');
  AWS.config.update(config.videoUploadConfig);
  var s3 = new AWS.S3();
  
  var _delete = function _delete(bucketName, file) {
    return new Promise(function(resolve, reject) {
      var params = {
        Bucket: config.awsResourcePrefix + bucketName,
        Key: file
      };

      s3.deleteObject(params, function(err, data) {
        if (err) {
          return reject(err);
        }

        return resolve();
      });
    });
  };

  var _deleteResultEvidence = function _deleteResultEvidence(resultId) {
    return Promise.all([
      _delete('videos', resultId.toString() + '.mp4'),
      _delete('videos-to-process', resultId.toString())
    ]);
  };
  
  var _deleteSocialUpdateVideo = function _deleteSocialUpdateVideo(updateId) {
    return Promise.all([
      _delete('misc-videos', updateId + '.png'),
      _delete('misc-videos-to-process', updateId)
    ]);
  };

  return {
    deleteResultEvidence: _deleteResultEvidence,
    deleteSocialUpdateVideo: _deleteSocialUpdateVideo
  };
};
