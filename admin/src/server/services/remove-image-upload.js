var Promise = require('bluebird');

module.exports = function RemoveImageUploadService(config, logger) {
  var AWS = require('aws-sdk');
  AWS.config.update(config.imageUploadConfig);
  var s3 = new AWS.S3();
  
  var _delete = function _delete(bucketName, file) {
    return new Promise(function(resolve, reject) {
      var params = {
        Bucket: config.awsResourcePrefix + bucketName,
        Key: file
      };

      s3.deleteObject(params, function(err, data) {
        if (err) {
          return reject(err);
        }

        return resolve();
      });
    });
  };

  var _deleteResultEvidence = function _deleteResultEvidence(resultId) {
    return Promise.all([
      _delete('evidence-photos', resultId.toString() + '.png'),
      _delete('evidence-photos-to-process', resultId.toString())
    ]);
  };
  
  var _deleteSocialUpdateImage = function _deleteSocialUpdateImage(updateId) {
    return Promise.all([
      _delete('social-photos', updateId + '.png'),
      _delete('social-photos-to-process', updateId)
    ]);
  };
  
  var _deleteAvatar = function _deleteAvatar(userId) {
    return Promise.all([
      _delete('avatars', userId.toString() + '-50x50.png'),
      _delete('avatars', userId.toString() + '-100x100.png'),
      _delete('avatars', userId.toString() + '-40x40.png'),
      _delete('avatars', userId.toString() + '-80x80.png'),
      _delete('avatars', userId.toString() + '-120x120.png'),
      _delete('avatars', userId.toString() + '-240x240.png'),
      _delete('avatars', userId.toString() + '-170x170.png'),
      _delete('avatars-to-process', userId.toString())
    ]);
  };

  return {
    deleteResultEvidence: _deleteResultEvidence,
    deleteAvatar: _deleteAvatar,
    deleteSocialUpdateImage: _deleteSocialUpdateImage
  };
};
