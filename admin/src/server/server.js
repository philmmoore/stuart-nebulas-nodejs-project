var express = require('express');

module.exports = function Server(ioc) {
  var server = express();

  server.set('view engine', 'jade');
  server.set('x-powered-by', false);

  if (ioc.config.isProduction) {
    server.set('trust proxy', 1);
    server.use(ioc.middleware.security);
  }

  server.use(ioc.middleware.compression);
  server.use(ioc.middleware.static);
  server.use(ioc.middleware.config);
  server.use(ioc.middleware.redisSession);
  server.use(ioc.middleware.ensureAuthenticated);
  server.use(ioc.middleware.user);
  server.use(ioc.middleware.language);
  server.use(ioc.middleware.formParser);
  server.use(ioc.middleware.renderView);
  server.use(ioc.middleware.router);

  server.locals.pretty = !ioc.config.isProduction;
  
  var _listen = function _listen(port) {
    server.listen(port, function listening() {
      ioc.logger.log('Listening on port %s.', port);
    });
  };
  
  return {
    listen: _listen
  };
};