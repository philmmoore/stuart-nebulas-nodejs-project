var session = require('express-session');
var RedisStore = require('connect-redis')(session);

module.exports = function redisSession(redisDB, config) {
  var store = new RedisStore({
    client: redisDB,
    db: 0,
    prefix: 'admin-session:'
  });

  return session({
    store: store,
    secret: config.sessionSecretKey,
    resave: true,
    saveUninitialized: false,
    name: 'gymfit-admin',
    cookie: { secure: config.isProduction }
  });
};
