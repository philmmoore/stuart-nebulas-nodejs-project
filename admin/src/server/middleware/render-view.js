module.exports = function renderView() {
  return function handle(req, res, next) {
    res.renderView = function renderView(viewName, data) {
      if (!data) {
        data = {};
      }
      
      data.viewName = viewName;
      res.render(viewName, data);
    };

    next();
  };
};
