var Busboy = require('busboy');
var Stream = require('stream');
var mime = require('mime-types');
var gm = require('gm').subClass({ imageMagick: true });
var async = require('async');

module.exports = function formParser() {
  var isFileImage = function isFileImage(file) {
    return file.mimeType.split('/')[0] === 'image';
  };

  var processImage = function(image, callback) {
    gm(image.data).size(function(err, size) {
      if (err) {
        callback(err);
      }

      image.width = size.width;
      image.height = size.height;

      callback(null);
    });
  };

  var getFileExtension = function(filename, mimetype) {
    var lastDotIndex = filename.lastIndexOf('.');

    if (lastDotIndex > -1) {
      return filename.substring(lastDotIndex + 1).toLowerCase();
    }

    return mime.extension(mimetype);
  };

  return function handle(req, res, next) {
    if (req.method !== 'POST') {
      return next();
    }

    req.body = {};

    var busboy = new Busboy({
      headers: req.headers
    });

    var images = [];

    busboy.on('file', function onFile(fieldname, file, filename, encoding, mimetype) {
      var length = 0;
      var fileData = new Buffer(0);

      file.on('data', function onData(data) {
        fileData = Buffer.concat([fileData, data]);
        length += data.length;
      });

      file.on('end', function onFileEnd() {
        if (length > 0) {
          var uploadedFile = {
            fileName: filename,
            encoding: encoding,
            mimeType: mimetype,
            length: length,
            data: fileData,
            extension: getFileExtension(filename, mimetype)
          };

          if (isFileImage(uploadedFile)) {
            images.push(uploadedFile);
          }

          req.body[fieldname] = uploadedFile;
        }
      });
    });

    busboy.on('field', function onField(fieldname, val, fieldnameTruncated, valTruncated) {
      req.body[fieldname] = val;
    });

    busboy.on('finish', function onFinish() {
      async.each(images, processImage, function(err) {
        if (err) {
          throw err;
        }

        next();
      });
    });

    req.pipe(busboy);
  };
};
