module.exports = function ConfigMiddleware(config) {
  return function handle(req, res, next) {
    res.locals.isLive = config.isLive;

    next();
  };
};
