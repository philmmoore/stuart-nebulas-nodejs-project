module.exports = function ensureAuthenticated() {
  return function handle(req, res, next) {
    var isUserLoggedIn = req.session.user ? true : false;

    if (isUserLoggedIn && req.url === '/login/') {
      return res.redirect('/');
    }

    if (!isUserLoggedIn && req.url !== '/login/') {
      return res.redirect('/login/');
    }

    next();
  };
};
