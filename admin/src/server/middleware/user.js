module.exports = function user() {
  return function handle(req, res, next) {
    res.locals.username = req.session.user;
    next();
  };
};
