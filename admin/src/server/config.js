var Config = function config() {
  var redisURL = process.env.REDIS_URL || 'redis://localhost:6379';
  var postgresURL = process.env.DATABASE_URL || 'postgres://localhost:5432';
  var postgresPoolSize = process.env.DATABASE_POOL_SIZE || 4;
  var sessionSecretKey = process.env.REDIS_SESSION_SECRET_KEY || '82hG#A2*£o;3-Hq2{';
  var port = process.env.PORT || 3000;
  var isProduction = process.env.NODE_ENV === 'production';
  var defaultLanguage = process.env.DEFAULT_LANGUAGE || 'en';
  var authSecretKey = process.env.AUTH_SECRET_KEY || '@98-S% 08iJQ*&@j]-!93jknQ';
  var targetHost = process.env.TARGET_HOST;
  var isLive = targetHost === 'admin.gymfit.com';

  var imageUploadAWSAccessKeyID = process.env.IMAGE_UPLOAD_AWS_ACCESS_KEY_ID || 'AKIAJHDJZBQAREA5REBA';
  var imageUploadAWSSecretAccessKey = process.env.IMAGE_UPLOAD_AWS_SECRET_ACCESS_KEY || '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';
  var videoUploadAWSAccessKeyID = process.env.VIDEO_UPLOAD_AWS_ACCESS_KEY_ID || 'AKIAJRFNNQQLZRLXRQMA';
  var videoUploadAWSSecretAccessKey = process.env.VIDEO_UPLOAD_AWS_SECRET_ACCESS_KEY || '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';
  var awsRegion = process.env.AWS_REGION_NAME || 'eu-west-1';
  var awsResourcePrefix = process.env.AWS_RESOURCE_PREFIX || 'gymfit-development-';

  var advertisementImageUrl = process.env.ADVERTISEMENT_IMAGE_URL || 'https://s3-eu-west-1.amazonaws.com/gymfit-development-adverts/';
  var evidenceImageUrl = process.env.EVIDENCE_IMAGE_URL || 'https://s3-eu-west-1.amazonaws.com/gymfit-development-evidence-photos/';
  var evidenceVideoUrl = process.env.EVIDENCE_VIDEO_URL || 'https://gymfit-development-videos.s3.amazonaws.com/';
  var socialImageUrl = process.env.SOCIAL_IMAGE_URL || 'https://gymfit-development-social-photos.s3.amazonaws.com/';
  var socialVideoUrl = process.env.SOCIAL_VIDEO_URL || 'https://gymfit-development-misc-videos.s3.amazonaws.com/';
  var publicSite = process.env.PUBLIC_SITE || 'http://localhost:4000/';
  var adminSite = targetHost ? 'https://' + targetHost + '/' : 'http://localhost:3000/';

  if (isProduction) {
    postgresURL += '?ssl=true';
  }

  return {
    redisURL: redisURL,
    postgresURL: postgresURL,
    postgresPoolSize: postgresPoolSize,
    sessionSecretKey: sessionSecretKey,
    port: port,
    isProduction: isProduction,
    defaultLanguage: defaultLanguage,
    authSecretKey: authSecretKey,
    targetHost: targetHost,
    isLive: isLive,
    imageUploadConfig: {
      accessKeyId: imageUploadAWSAccessKeyID,
      secretAccessKey: imageUploadAWSSecretAccessKey,
      region: awsRegion
    },
    videoUploadConfig: {
      accessKeyId: videoUploadAWSAccessKeyID,
      secretAccessKey: videoUploadAWSSecretAccessKey,
      region: awsRegion
    },
    queueConfig: {
      accessKeyId: imageUploadAWSAccessKeyID,
      secretAccessKey: imageUploadAWSSecretAccessKey,
      region: awsRegion
    },
    queueUrlPrefix: 'https://sqs.eu-west-1.amazonaws.com/164535666447/' + awsResourcePrefix,
    awsResourcePrefix: awsResourcePrefix,
    advertisementImageUrl: advertisementImageUrl,
    evidenceImageUrl: evidenceImageUrl,
    evidenceVideoUrl: evidenceVideoUrl,
    socialImageUrl: socialImageUrl,
    socialVideoUrl: socialVideoUrl,
    publicSite: publicSite,
    adminSite: adminSite
  };
};

module.exports = Config;
