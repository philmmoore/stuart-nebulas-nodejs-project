var redis = require('redis');
var url =  require('url');

var RedisDB = function RedisDB(connectionString, logger) {
  this.redisUrl = url.parse(connectionString);
  this.logger = logger;
};

RedisDB.prototype.connect = function connect(success) {
  var logger = this.logger;
  logger.log('Connecting to Redis...');

  var options = {};
  var client = redis.createClient(this.redisUrl.port, this.redisUrl.hostname, options);

  if (this.redisUrl.auth) {
    client.auth(this.redisUrl.auth.split(':')[1]);
  }

  client.on('error', function (err) {
    return logger.error('RedisDB:' + err);
  });

  client.on('ready', function () {
    logger.log('Connected to Redis.');
    success(client);
  });
};

module.exports = RedisDB;
