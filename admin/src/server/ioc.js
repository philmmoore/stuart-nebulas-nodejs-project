var express = require('express');

// repositories
var AuthenticationRepository = require('./repositories/authentication');
var ChallengeRepository = require('./repositories/challenge');
var AdvertisementRepository = require('./repositories/advertisement');
var ImageUploadRepository = require('./repositories/image-upload');
var PageRepository = require('./repositories/page');
var BlogRepository = require('./repositories/blog');
var NotificationRepository = require('./repositories/notification');
var UserRepository = require('./repositories/user');
var DashboardRepository = require('./repositories/dashboard');
var AreaRepository = require('./repositories/area');
var ConfigurationRepository = require('./repositories/configuration');
var EmailTemplateRepository = require('./repositories/email-template');
var ResultRepository = require('./repositories/result');
var EmailRepository = require('./repositories/email');
var AdminUserRepository = require('./repositories/admin-user');
var SocialUpdateRepository = require('./repositories/social-update');

// validators
var ChallengeValidator = require('./validators/challenge');
var AdvertisementValidator = require('./validators/advertisement');
var PageValidator = require('./validators/page');
var BlogValidator = require('./validators/blog');
var NotificationValidator = require('./validators/notification');
var AreaValidator = require('./validators/area');
var ConfigurationValidator = require('./validators/configuration');
var EmailTemplateValidator = require('./validators/email-template');
var ResultValidator = require('./validators/result');
var UserValidator = require('./validators/user');
var EmailValidator = require('./validators/email');
var AdminUserValidator = require('./validators/admin-user');
var SocialUpdateValidator = require('./validators/social-update');

// utilities
var areaFormatter = require('./utilities/area-formatter');
var resultFormatter = require('./utilities/result-formatter');

// controllers
var AuthenticationController = require('./controllers/authentication');
var ChallengeController = require('./controllers/challenge');
var AdvertisementController = require('./controllers/advertisement');
var PageController = require('./controllers/page');
var BlogController = require('./controllers/blog');
var NotificationController = require('./controllers/notification');
var UserController = require('./controllers/user');
var DashboardController = require('./controllers/dashboard');
var AreaController = require('./controllers/area');
var ConfigurationController = require('./controllers/configuration');
var EmailTemplateController = require('./controllers/email-template');
var ResultController = require('./controllers/result');
var EmailController = require('./controllers/email');
var AdminUserController = require('./controllers/admin-user');
var SocialUpdateController = require('./controllers/social-update');

// routes
var AuthenticationRoute = require('./routes/authentication');
var ChallengeRoute = require('./routes/challenge');
var DashboardRoute = require('./routes/dashboard');
var AdvertisementRoute = require('./routes/advertisement');
var PageRoute = require('./routes/page');
var BlogRoute = require('./routes/blog');
var NotificationRoute = require('./routes/notification');
var UserRoute = require('./routes/user');
var AreaRoute = require('./routes/area');
var ConfigurationRoute = require('./routes/configuration');
var EmailTemplateRoute = require('./routes/email-template');
var ResultRoute = require('./routes/result');
var EmailRoute = require('./routes/email');
var HelpRoute = require('./routes/help');
var AdminUserRoute = require('./routes/admin-user');
var SocialUpdateRoute = require('./routes/social-update');

// middleware
var compressionMiddleware = require('compression');
var redisSessionMiddleware = require('./middleware/redis-session');
var securityMiddleware = require('./middleware/security');
var ensureAuthenticatedMiddleware = require('./middleware/ensure-authenticated');
var languageMiddleware = require('./middleware/language');
var userMiddleware = require('./middleware/user');
var formParserMiddleware = require('./middleware/form-parser');
var renderViewMiddleware = require('./middleware/render-view');
var configMiddleware = require('./middleware/config');

var translations = {
  en: require('./strings/en'),
  fr: require('./strings/fr')
};

module.exports = function IoC(config, redisDB, db, logger) {
  // ioc
  var authenticationRepository = new AuthenticationRepository(db);
  var challengeRepository = new ChallengeRepository(db);
  var advertisementRepository = new AdvertisementRepository(db);
  var imageUploadRepository = new ImageUploadRepository(config, logger);
  var pageRepository = new PageRepository(db);
  var blogRepository = new BlogRepository(db);
  var notificationRepository = new NotificationRepository(db);
  var userRepository = new UserRepository(db);
  var dashboardRepository = new DashboardRepository(db);
  var areaRepository = new AreaRepository(db);
  var configurationRepository = new ConfigurationRepository(db);
  var emailTemplateRepository = new EmailTemplateRepository(db);
  var resultRepository = new ResultRepository(db);
  var emailRepository = new EmailRepository(db);
  var adminUserRepository = new AdminUserRepository(db);
  var socialUpdateRepository = new SocialUpdateRepository(db);

  var challengeValidator = new ChallengeValidator();
  var advertisementValidator = new AdvertisementValidator();
  var pageValidator = new PageValidator();
  var blogValidator = new BlogValidator();
  var notificationValidator = new NotificationValidator();
  var areaValidator = new AreaValidator();
  var configurationValidator = new ConfigurationValidator();
  var emailTemplateValidator = new EmailTemplateValidator();
  var resultValidator = new ResultValidator();
  var userValidator = new UserValidator();
  var emailValidator = new EmailValidator();
  var adminUserValidator = new AdminUserValidator();
  var socialUpdateValidator = new SocialUpdateValidator();
  
  // services
  var passwordHashService = require('./services/password-hash')(config.authSecretKey);
  var messageQueueService = require('./services/message-queue')(config);
  var removeImageUploadService = require('./services/remove-image-upload')(config, logger);
  var removeVideoUploadService = require('./services/remove-video-upload')(config, logger);

  var authenticationController = new AuthenticationController(authenticationRepository, passwordHashService);
  var challengeController = new ChallengeController(challengeRepository, challengeValidator, config.publicSite);
  var advertisementController = new AdvertisementController(advertisementRepository, advertisementValidator, areaFormatter, imageUploadRepository, config.advertisementImageUrl);
  var pageController = new PageController(pageRepository, pageValidator, config.publicSite);
  var blogController = new BlogController(blogRepository, blogValidator, config.publicSite);
  var notificationController = new NotificationController(notificationRepository, notificationValidator);
  var userController = new UserController(userRepository, userValidator, messageQueueService, removeImageUploadService, removeVideoUploadService, redisDB, config.publicSite);
  var dashboardController = new DashboardController(dashboardRepository, config.publicSite, config.evidenceImageUrl, config.evidenceVideoUrl, resultFormatter);
  var areaController = new AreaController(areaRepository, areaValidator);
  var configurationController = new ConfigurationController(configurationRepository, configurationValidator);
  var emailTemplateController = new EmailTemplateController(emailTemplateRepository, emailTemplateValidator);
  var resultController = new ResultController(resultRepository, resultValidator, config.publicSite, config.evidenceImageUrl, config.evidenceVideoUrl, resultFormatter, messageQueueService, removeImageUploadService, removeVideoUploadService);
  var emailController = new EmailController(emailRepository, emailValidator, messageQueueService, config.adminSite, config.publicSite);
  var adminUserController = new AdminUserController(adminUserRepository, adminUserValidator, passwordHashService);
  var socialUpdateController = new SocialUpdateController(socialUpdateRepository, socialUpdateValidator, config.publicSite, config.socialImageUrl, config.socialVideoUrl, messageQueueService, removeImageUploadService, removeVideoUploadService);

  var router = express.Router();

  // attach routes
  new ChallengeRoute(router, challengeController);
  new DashboardRoute(router, dashboardController);
  new AuthenticationRoute(router, authenticationController);
  new AdvertisementRoute(router, advertisementController);
  new PageRoute(router, pageController);
  new BlogRoute(router, blogController);
  new NotificationRoute(router, notificationController);
  new UserRoute(router, userController);
  new AreaRoute(router, areaController);
  new ConfigurationRoute(router, configurationController);
  new EmailTemplateRoute(router, emailTemplateController);
  new ResultRoute(router, resultController);
  new EmailRoute(router, emailController);
  new HelpRoute(router);
  new AdminUserRoute(router, adminUserController);
  new SocialUpdateRoute(router, socialUpdateController);

  return {
    logger: logger,
    config: config,
    middleware: {
      security: securityMiddleware(config.targetHost),
      compression: compressionMiddleware(),
      static: express.static('ui-bundled'),
      language: languageMiddleware(config.defaultLanguage, translations, logger),
      redisSession: redisSessionMiddleware(redisDB, config),
      config: configMiddleware(config),
      user: userMiddleware(),
      ensureAuthenticated: ensureAuthenticatedMiddleware(),
      renderView: renderViewMiddleware(),
      formParser: formParserMiddleware(),
      router: router
    }
  };
};
