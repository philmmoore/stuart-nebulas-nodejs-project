var assert = require('chai').assert;
var redis = require('fakeredis');
var Server = require('../../src/server/server');

describe('server', function() {
  describe('construct', function() {
    it('should construct without issues', function() {
      var middleware = function(req, res, next) {};

      var ioc = {
        config: {
          sessionSecretKey: 'secret',
          isProduction: false
        },
        middleware: {
          security: middleware,
          compression: middleware,
          static: middleware,
          language: middleware,
          redisSession: middleware,
          user: middleware,
          renderView: middleware,
          formParser: middleware,
          router: middleware,
          ensureAuthenticated: middleware,
          config: middleware
        }
      };

      var redisDB = redis.createClient();
      var postgresDB = {};
      var logger = {};
      var router = function(req, res, next) {};

      var server = new Server(ioc);
    });
  });
});
