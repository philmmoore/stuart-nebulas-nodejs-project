var assert = require('chai').assert;
var user = require('../../../src/server/middleware/user');

describe('user middleware', function() {
  describe('#handle()', function() {
    it('given user is logged in, username should be set on res.locals', function() {
      var username = 'test@test.com';
      var req = {
        session: {
          user: username
        }
      };
      var res = {
        locals: {}
      };
      var nextWasInvoked = false;
      var next = function() {
        nextWasInvoked = true;
      };
      var middleware = user();

      middleware(req, res, next);

      assert.isTrue(nextWasInvoked);
      assert.equal(res.locals.username, username);
    });
  });
});
