var assert = require('chai').assert;
var language = require('../../../src/server/middleware/language');

describe('language middleware', function() {
  describe('#handle()', function() {
    it('given client does not expose accept-language header should set res.locals.language to the default language', function() {
      var defaultLanguage = 'en';
      var expectedTranslation = 'pass';
      var translations = {};
      var req = {
        headers: {}
      };
      var res = {
        locals: {}
      };
      var nextWasInvoked = false;
      var next = function() {
        nextWasInvoked = true;
      };
      var middleware = language(defaultLanguage, translations);

      middleware(req, res, next);

      assert.isTrue(nextWasInvoked);
      assert.equal(defaultLanguage, res.locals.language);
    });

    it('given client exposes an accept-language header should set res.locals.language to the client language if the language is supported', function() {
      var defaultLanguage = 'ro';
      var expectedTranslation = 'pass';
      var translations = {
        en: {}
      };
      var clientLanguage = 'en';
      var req = {
        headers: {
          'accept-language': clientLanguage
        }
      };
      var res = {
        locals: {}
      };
      var nextWasInvoked = false;
      var next = function() {
        nextWasInvoked = true;
      };
      var middleware = language(defaultLanguage, translations);

      middleware(req, res, next);

      assert.isTrue(nextWasInvoked);
      assert.equal(clientLanguage, res.locals.language);
    });

    it('given client exposes an accept-language header should set res.locals.language to the default language if the client language is not supported', function() {
      var defaultLanguage = 'ro';
      var expectedTranslation = 'pass';
      var translations = {};
      var clientLanguage = 'en';
      var req = {
        headers: {
          'accept-language': clientLanguage
        }
      };
      var res = {
        locals: {}
      };
      var nextWasInvoked = false;
      var next = function() {
        nextWasInvoked = true;
      };
      var middleware = language(defaultLanguage, translations);

      middleware(req, res, next);

      assert.isTrue(nextWasInvoked);
      assert.equal(defaultLanguage, res.locals.language);
    });

    it('given translation for client language exists should return the correct translation', function() {
      var defaultLanguage = 'ro';
      var expectedTranslation = 'pass';
      var translations = {
        en: {
          test: expectedTranslation
        }
      };
      var clientLanguage = 'en';
      var req = {
        headers: {
          'accept-language': clientLanguage
        }
      };
      var res = {
        locals: {}
      };
      var nextWasInvoked = false;
      var next = function() {
        nextWasInvoked = true;
      };
      var middleware = language(defaultLanguage, translations);

      middleware(req, res, next);

      var translation = res.locals.string('test');

      assert.isTrue(nextWasInvoked);
      assert.equal(expectedTranslation, translation);
    });

    it('given translation for language does not exist should return the correct translation', function() {
      var defaultLanguage = 'zh';
      var translations = {
        zh: {}
      };
      var req = {
        headers: {}
      };
      var res = {
        locals: {}
      };
      var nextWasInvoked = false;
      var next = function() {
        nextWasInvoked = true;
      };
      var loggedMessage;
      var logger = {
        log: function(message) {
          loggedMessage = message;
        }
      };
      var middleware = language(defaultLanguage, translations, logger);

      middleware(req, res, next);

      var translation = res.locals.string('test');

      assert.isTrue(nextWasInvoked);
      assert.equal('%%test%%', translation);
      assert.isString(loggedMessage);
    });
  });
});
