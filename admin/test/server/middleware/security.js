var assert = require('chai').assert;
var sinon = require('sinon');
var security = require('../../../src/server/middleware/security');

describe('security middleware', function() {
  describe('#handle()', function() {
    it('given insecure domain should redirect to secure domain', function() {
      var req = {
        headers: {
          host: 'insecure.com'
        }
      };

      var redirectSpy = sinon.spy();
      var res = {
        redirect: redirectSpy
      };

      var targetHost = 'secure.com';
      var middleware = security(targetHost);
      var nextSpy = sinon.spy();

      middleware(req, res, nextSpy);

      assert.isTrue(redirectSpy.calledOnce);
      assert.isTrue(redirectSpy.calledWith('https://secure.com'));
      assert.isFalse(nextSpy.called);
    });

    it('given secure domain but via http should redirect to secure domain over http', function() {
      var req = {
        headers: {
          host: 'secure.com'
        }
      };

      var redirectSpy = sinon.spy();
      var res = {
        redirect: redirectSpy
      };

      var targetHost = 'secure.com';
      var middleware = security(targetHost);
      var nextSpy = sinon.spy();

      middleware(req, res, nextSpy);

      assert.isTrue(redirectSpy.calledOnce);
      assert.isTrue(redirectSpy.calledWith('https://secure.com'));
      assert.isFalse(nextSpy.called);
    });

    it('given secure domain over https should not do anything', function() {
      var req = {
        secure: true,
        headers: {
          host: 'secure.com'
        }
      };

      var redirectSpy = sinon.spy();
      var res = {
        redirect: redirectSpy
      };

      var targetHost = 'secure.com';
      var middleware = security(targetHost);
      var nextSpy = sinon.spy();

      middleware(req, res, nextSpy);

      assert.isFalse(redirectSpy.called);
      assert.isTrue(nextSpy.called);
    });
  });
});
