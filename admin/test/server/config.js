var assert = require('chai').assert;
var Config = require('../../src/server/config');

describe('config', function() {
  describe('construct', function() {
    it('should return configuration', function() {
      var config = new Config();

      assert.isString(config.redisURL);
      assert.isString(config.postgresURL);
      assert.isString(config.sessionSecretKey);
      assert.isNumber(config.port);
      assert.isBoolean(config.isProduction);
    });
  });
});
