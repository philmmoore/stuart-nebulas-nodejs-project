var assert = require('chai').assert;
var formatter = require('../../../src/server/utilities/area-formatter');

describe('Area Formatter', function() {
  describe('#format()', function() {
    it('parents should have level 0 indentation', function() {
      var areas = [
        {
          id: 12,
          name: 'test',
          parent: null
        }
      ];

      var translator = function(string) {
        return '%' + string + '%';
      };

      var result = formatter(areas, translator);

      assert.deepEqual(result[0], {
        id: 12,
        name: '%Country%',
        parent: null,
        indentation: 0
      });
    });

    it('given one parent, it should have level 1 indentation', function() {
      var areas = [
        {
          id: 12,
          name: 'test',
          parent: null
        },
        {
          id: 29,
          name: 'test-2',
          parent: 12
        }
      ];

      var translator = function(string) {
        return '%' + string + '%';
      };

      var result = formatter(areas, translator);

      assert.deepEqual(result, [
        {
          id: 12,
          name: '%Country%',
          parent: null,
          indentation: 0
        },
        {
          id: 29,
          name: 'test-2',
          parent: 12,
          indentation: 1
        }
      ]);
    });

    it('given two parents, it should have level 2 indentation', function() {
      var areas = [
        {
          id: 9,
          name: 'test-3',
          parent: 29
        },
        {
          id: 12,
          name: 'test',
          parent: null
        },
        {
          id: 29,
          name: 'test-2',
          parent: 12
        }
      ];

      var translator = function(string) {
        return '%' + string + '%';
      };

      var result = formatter(areas, translator);

      assert.deepEqual(result, [
        {
          id: 12,
          name: '%Country%',
          parent: null,
          indentation: 0
        },
        {
          id: 29,
          name: 'test-2',
          parent: 12,
          indentation: 1
        },
        {
          id: 9,
          name: 'test-3',
          parent: 29,
          indentation: 2
        }
      ]);
    });
  });
});
