var assert = require("assert");

describe('ChallengeParser', function() {
  //var parser = new ChallengeParser();

  describe('#parse()', function () {
    it('should resolve region', function () {
      var expected = {
        region: 'North West',
        area: 'Greater Manchester',
        local_area: 'Salford',
        challenge: 'Press Ups',
        season: 'Summer 2015',
        sex: 'Male',
        age: '20-29',
        weight: '56-60',
        height: '22-80',
        validated: true,
        page: 15
      };

      var query = 'north_west/greater_manchester/salford/press_ups/summer-2015/male/20-29/56-60kg/22-80cm/validated/15';
      //var result = parser.parse(query);


    });
  });
});
