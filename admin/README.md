# Admin
GymFit admin

## Pre-requisite Software
You'll need the following installed on your computer.
* Node.js 4.2.2 or later - http://nodejs.org
* Redis - http://redis.io
* Redis Desktop Manager - http://redisdesktop.com/
* PostgreSQL - For OS X try http://redisdesktop.com/. Handy to have a GUI as well. Try http://www.pgadmin.org/
* ImageMagick - http://www.imagemagick.org/, for OS X use `brew install imagemagick`
* Postgres for pg-native `brew install postgres`

## Running
To get up and running:

1. Make sure Redis is running locally
2. Make sure PostgreSQL is running locally
3. `npm install`
4. `gulp`
5. `npm start`
6. Browse to http://localhost:3000 and login with the following credentials:

    `user: dev@gymfit.com`   
    `pass: dev`

## Important Notes
if changing the leaderboard algorithm the following need updating so all leaderboard implementations are consistent:
* sp_gymfit_site_leaderboard_search
* sp_gymfit_site_leaderboard_user_position
* sp_gymfit_site_user_activity
* sp_gymfit_api_user_activity
* sp_gymfit_api_result_get

# Local Test data
In order to get test data on your system, first clear out your local DB by running the drop-all.sql, then run gulp to populate the structure, and finally run the test-data.sql script.
