(function(jquery, c3, chartData) {
  'use strict';
  
  var _init = function _init() {
    var columnData = [
      ['date'],
      ['registrations'],
      ['resultSubmissions'],
      ['imagesUploaded'],
      ['videosUploaded'],
      ['logins'],
      ['likes']
    ];
    
    for (var index = 0; index < chartData.length; index++) {
      var row = chartData[index];
      
      columnData[0].push(row.date);
      columnData[1].push(row.registrations);
      columnData[2].push(row.resultSubmissions);
      columnData[3].push(row.imagesUploaded);
      columnData[4].push(row.videosUploaded);
      columnData[5].push(row.logins);
      columnData[6].push(row.likes);
    }
    
    var chart = c3.generate({
      bindto: '#chart-placeholder',
      data: {
        x: 'date',
        columns: columnData,
        names: {
          date: 'Date',
          registrations: 'Registrations',
          resultSubmissions: 'Results Submitted',
          imagesUploaded: 'Images Uploaded',
          videosUploaded: 'Videos Uploaded',
          logins: 'Logins',
          likes: 'Likes'
        }
      },
      axis: {
        x: {
          type: 'timeseries',
          tick: {
            format: '%Y-%m-%d',
          }
        }
      },
      grid: {
        x: {
          show: true
        },
        y: {
          show: true
        }
      }
    });
  };
  
  jquery(_init);
})($, c3, chartData);