var through = require('through2');
var PostgresDB = require('./postgres-db');
var Config = require('../src/server/config');

var plugin = function() {
  var config = new Config();
  var logger = console;
  var postgresDB = new PostgresDB(config.postgresURL, logger);
  var currentVersion = -1;

  function getCurrentVersion(postgresClient, callback) {
    postgresClient.query('SELECT version FROM migration', function(err, result) {
      if (result) {
        currentVersion = result.rows[0].version;
        logger.log('Current migration version: %d', currentVersion);
      } else {
        logger.log('No current migration version.');
      }

      callback();
    });
  }

  function performMigration(postgresClient, migrationNumber, file, callback) {
    if (currentVersion >= migrationNumber) {
      postgresClient.end();
      return callback();
    }

    logger.log('Performing migration %d...', migrationNumber);

    var fileContents = file.contents.toString('utf8');

    postgresClient.query(fileContents, function(err, result) {
      if (err) {
        postgresClient.end();
        throw err;
      }

      postgresClient.query('UPDATE migration SET version = ' + migrationNumber, function(err, updateResult) {
        if (err) {
          postgresClient.end();
          throw err;
        }

        logger.log('Finished migration %d.', migrationNumber);

        postgresClient.end();
        callback();
      });
    });
  }

  return through.obj(function(file, enc, callback){
    if (file.isNull() || !file.isBuffer()) {
      return callback();
    }

    var migrationNumber = parseInt(file.path.split('/').pop().split('-')[0]);

    if (currentVersion >= migrationNumber) {
      return callback();
    }

    postgresDB.connect(function connectedToPostgresDB(postgresClient) {
      if (currentVersion < 0) {
        getCurrentVersion(postgresClient, function() {
          return performMigration(postgresClient, migrationNumber, file, callback);
        });
      } else {
        return performMigration(postgresClient, migrationNumber, file, callback);
      }
    });
  });
};

module.exports = plugin;
