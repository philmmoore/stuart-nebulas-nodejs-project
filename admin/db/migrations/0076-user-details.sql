CREATE TABLE relationship_status (
  id SERIAL PRIMARY KEY,
  value TEXT NOT NULL
);

INSERT INTO relationship_status (value)
VALUES  ('Single'),
        ('In a relationship'),
        ('Engaged'),
        ('Married'),
        ('It''s complicated'),
        ('In an open relationship'),
        ('Widowed'),
        ('In a domestic partnership'),
        ('In a civil union');

CREATE TABLE user_relationship_status (
  user_id INTEGER REFERENCES "user" (id) NOT NULL,
  relationship_status_id INTEGER REFERENCES relationship_status (id) NOT NULL,
  PRIMARY KEY (user_id, relationship_status_id)
);

CREATE TABLE routine_type (
  id SERIAL PRIMARY KEY,
  value TEXT NOT NULL
);

INSERT INTO routine_type (value)
VALUES  ('Beginner'),
        ('Intermediate'),
        ('Serious'),
        ('Fitness Freak');

CREATE TABLE user_routine_type (
  user_id INTEGER REFERENCES "user" (id) NOT NULL,
  routine_type_id INTEGER REFERENCES routine_type (id) NOT NULL,
  PRIMARY KEY (user_id, routine_type_id)
);

CREATE TABLE gym (
  id SERIAL PRIMARY KEY,
  country_id INTEGER REFERENCES country (id) NOT NULL,
  value TEXT NOT NULL
);

INSERT INTO gym (country_id, value)
VALUES  (1, 'Other'),
        (1, 'PureGym'),
        (1, 'Virgin Active'),
        (1, 'LA fitness'),
        (1, 'David Lloyd');

CREATE TABLE user_gym (
  user_id INTEGER REFERENCES "user" (id) NOT NULL,
  gym_id INTEGER REFERENCES gym (id) NOT NULL,
  PRIMARY KEY (user_id, gym_id)
);

CREATE FUNCTION sp_gymfit_site_register_get_step_6_details (
  _country_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
		(
			SELECT
							array
							(
                  SELECT row_to_json(a)
                  FROM (
                          SELECT			id,
                                      value
                          FROM				relationship_status
                  ) AS a
              )  AS "relationshipStatuses",
  						array
  						(
                SELECT row_to_json(b)
                FROM (
  							         SELECT			id,
                                    value
  							         FROM				routine_type
                ) AS b
  						) AS "routineTypes",
							array
							(
                SELECT row_to_json(c)
                FROM (
        								SELECT			id,
                                    value
        								FROM				gym
                        WHERE       country_id = _country_id
                ) AS c
							) AS "gyms"
		) AS result_to_return;
$$
LANGUAGE sql;

ALTER TABLE "user" ADD occupation TEXT;
ALTER TABLE "user" ADD nationality TEXT;

CREATE FUNCTION sp_gymfit_site_register_persist_step_6_details (
  _user_id INT,
  _occupation TEXT,
  _relationship_status_id INT,
  _nationality TEXT,
  _routine_type_id INT,
  _gym_id INT
)
RETURNS JSON AS
$$
	UPDATE   "user"
  SET     occupation = _occupation,
          nationality = _nationality
  WHERE   id = _user_id;

  INSERT INTO user_relationship_status (user_id, relationship_status_id)
  VALUES (_user_id, _relationship_status_id);

  INSERT INTO user_routine_type (user_id, routine_type_id)
  VALUES (_user_id, _routine_type_id);

  INSERT INTO user_gym (user_id, gym_id)
  VALUES (_user_id, _gym_id);

  SELECT to_json(1);
$$
LANGUAGE sql;
