DROP FUNCTION sp_gymfit_site_get_adverts_for_location(INT, INT, INT, INT);

CREATE OR REPLACE FUNCTION sp_gymfit_site_advert_get_for_location
(
	_country_id INT,
	_region_id INT,
	_county_id INT,
	_district_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))	FROM (
		SELECT					a.id,
										a.link,
										a.small_horizontal_image_extension AS "smallHorizontalImageExtension",
										a.large_vertical_image_extension AS "largeVerticalImageExtension"
		FROM						advert AS a
		LEFT OUTER JOIN advert_to_area AS country ON country.advert_id = a.id AND country.area_id = _country_id
		LEFT OUTER JOIN advert_to_area AS region ON region.advert_id = a.id AND region.area_id = _region_id
		LEFT OUTER JOIN advert_to_area AS county ON county.advert_id = a.id AND county.area_id = _county_id
		LEFT OUTER JOIN advert_to_area AS district ON district.advert_id = a.id AND district.area_id = _district_id
		ORDER BY 				district.area_id,
										county.area_id,
										region.area_id,
										country.area_id
		LIMIT						2
	) AS result;
$$
LANGUAGE sql;
