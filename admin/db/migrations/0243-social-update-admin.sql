ALTER TABLE social_update ADD COLUMN approved TIMESTAMP;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_social_update_get_all()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
	(
    SELECT		      u.id,
                    CASE WHEN COUNT(uf) < 1 THEN 0
                		ELSE
                			ROUND(100.0 * COUNT(uf) / (COUNT(ul) + COUNT(uf)), 2)
                		END AS "flagPercent",
                		COUNT(uf) AS "flagCount",
                		COUNT(ul) AS "likeCount",
                		u.body,
                    u.created,
                    
                    array
                    (
                      SELECT			id
                      FROM				social_update_video
                      WHERE				update_id = u.id
                      AND         processed IS NOT NULL
                    ) AS "videos",
                    array
                    (
                      SELECT			id
                      FROM				social_update_image
                      WHERE				update_id = u.id
                      AND         processed IS NOT NULL
                    ) AS "images",
                    array
                    (
                      SELECT      t.value
                      FROM				social_update_to_social_update_tag AS ut
                      JOIN        social_update_tag AS t ON ut.tag_id = t.id
                      WHERE				ut.update_id = u.id
                    ) AS "tags",

                		us.id AS "userId",
                		us.username AS "username",
                		
                		r.id AS "resultId",
                		r.result
    FROM		        social_update AS u
    LEFT OUTER JOIN social_update_like AS ul ON ul.update_id = u.id
    LEFT OUTER JOIN social_update_flag AS uf ON uf.update_id = u.id
    LEFT OUTER JOIN	social_update_to_result AS ur ON u.id = ur.update_id
    JOIN						"user" AS us on us.id = u.user_id
    LEFT OUTER JOIN	result AS r ON ur.result_id = r.id
    WHERE           u.approved IS NULL
    GROUP BY        u.id, r.id, us.id
    ORDER BY        "flagPercent" DESC,
                    u.created DESC
	) AS result_to_return;
$$
LANGUAGE sql;