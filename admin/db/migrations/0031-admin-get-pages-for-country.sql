CREATE FUNCTION sp_admin_get_all_pages_for_country
(
	_countryId INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM
		(
			SELECT		id,
								name
			FROM			page
			WHERE			country = _countryId
			ORDER BY	name
		) AS result;
$$
LANGUAGE sql;
