CREATE TABLE user_email_verification (
	key UUID PRIMARY KEY DEFAULT gen_random_uuid(),
	user_id INTEGER REFERENCES "user" (id) NOT NULL,
	created TIMESTAMP NOT NULL default current_timestamp
);

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_registration_add_user
(
	_registration_id INT
)
RETURNS JSON AS
$$
	DECLARE
		_user_id INT;
		_verification_key UUID;
	BEGIN
		INSERT INTO	"user" (username, hash, first_name, last_name, date_of_birth, email)
		SELECT			username,
								hash,
								first_name,
								last_name,
								date_of_birth,
								email
		FROM				user_registration
		WHERE				id = _registration_id
		RETURNING		id
		INTO				_user_id;

		DELETE FROM	user_registration
		WHERE				id = _registration_id;

		INSERT INTO	user_email_verification (user_id)
		VALUES			(_user_id)
		RETURNING		key
		INTO				_verification_key;

		RETURN row_to_json(result)
		FROM (
			SELECT			id,
									username,
									_verification_key AS "verificationKey"
			FROM				"user"
			WHERE				id = _user_id
		) AS result;
	END;
$$
LANGUAGE plpgsql;
