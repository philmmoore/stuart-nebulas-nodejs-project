DELETE FROM	user_registration
WHERE				email IN (SELECT email FROM "user");

CREATE TABLE email_type
(
  id SERIAL PRIMARY KEY,
  value TEXT NOT NULL
);

INSERT INTO email_type (value)
VALUES
	('News'),
	('Promotion');

CREATE TABLE email
(
  id SERIAL PRIMARY KEY,
  country INTEGER REFERENCES country (id) NOT NULL,
  type_id INTEGER REFERENCES email_type (id) NOT NULL,
  name TEXT NOT NULL,
  subject TEXT NOT NULL,
  plaintext_body TEXT NOT NULL,
  html_body TEXT NOT NULL
);

CREATE OR REPLACE FUNCTION sp_gymfit_admin_email_get_all_for_country
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM
		(
			SELECT		e.id,
								e.name,
                t.value AS type,
                e.subject
			FROM			email AS e
      JOIN      email_type AS t on t.id = e.type_id
			WHERE			e.country = _country_id
			ORDER BY	e.name
		) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_email_get
(
	_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
		(
			SELECT	e.id,
              e.name,
              e.type_id AS "typeId",
              e.subject,
              e.plaintext_body AS "plainBody",
              e.html_body AS "htmlBody"
			FROM		email AS e
			WHERE		e.id = _id
			LIMIT		1
		) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_email_save
(
	_country_id INT,
	_id INT,
	_name TEXT,
	_type_id INT,
  _subject TEXT,
  _plaintext_body TEXT,
  _html_body TEXT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	email
		SET			name = _name,
						type_id = _type_id,
            subject = _subject,
            plaintext_body = _plaintext_body,
            html_body = _html_body
		WHERE		id = _id;

		IF NOT FOUND THEN
			INSERT INTO email (country, name, type_id, subject, plaintext_body, html_body)
			VALUES			(_country_id, _name, _type_id, _subject, _plaintext_body, _html_body)
			RETURNING		id
			INTO				_id;
		END IF;

		RETURN to_json(_id);
	END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_email_get_types()
RETURNS JSON AS
$$
  SELECT array_to_json(array_agg(result))
  FROM
    (
      SELECT		id,
                value
      FROM			email_type
      ORDER BY	value
    ) AS result;
$$
LANGUAGE sql;