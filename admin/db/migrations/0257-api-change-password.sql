CREATE OR REPLACE FUNCTION sp_gymfit_api_auth_change_password
(
  _user_id INT,
  _current_hash BYTEA,
  _new_hash BYTEA,
  _device_id TEXT
)
RETURNS JSON AS
$$
  DECLARE
    _found_user_id INT;
    
  BEGIN
    UPDATE      "user"
    SET         hash = _new_hash
    WHERE       id = _user_id
    AND         hash = _current_hash
    RETURNING   id
    INTO        _found_user_id;
    
    INSERT INTO	user_audit (user_id, activity, message)
    SELECT      _found_user_id,
                'app password change',
                _device_id
    FROM        "user"
    WHERE       id = _found_user_id;

    DELETE
    FROM        user_password_reset
    WHERE       user_id = _found_user_id;
    
    RETURN to_json(_found_user_id IS NOT NULL);
  END
$$
LANGUAGE plpgsql;

ALTER TABLE email_template ADD COLUMN user_password_changed_subject TEXT NOT NULL DEFAULT 'Password Changed';
ALTER TABLE email_template ADD COLUMN user_password_changed_html_body TEXT NOT NULL DEFAULT '<p>Your password was changed. If this was not you please email us at <a href="mailto:admin@gymfit.com">admin@gymfit.com</a>.</p>';
ALTER TABLE email_template ADD COLUMN user_password_changed_plain_body TEXT NOT NULL DEFAULT 'Your password was changed. If this was not you please email us at admin@gymfit.com.';

DROP FUNCTION IF EXISTS sp_gymfit_admin_email_template_set(INT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT);

CREATE OR REPLACE FUNCTION sp_gymfit_admin_email_template_set
(
	_country_id INT,
	_standard_template TEXT,
	_email_confirmation_subject TEXT,
	_email_confirmation_html_body TEXT,
	_email_confirmation_plain_body TEXT,
	_password_reset_subject TEXT,
	_password_reset_html_body TEXT,
	_password_reset_plain_body TEXT,
	_result_rejected_subject TEXT,
	_result_rejected_html_body TEXT,
	_result_rejected_plain_body TEXT,
	_user_blocked_subject TEXT,
	_user_blocked_html_body TEXT,
	_user_blocked_plain_body TEXT,
	_user_unblocked_subject TEXT,
	_user_unblocked_html_body TEXT,
	_user_unblocked_plain_body TEXT,
	_user_avatar_removed_subject TEXT,
	_user_avatar_removed_html_body TEXT,
	_user_avatar_removed_plain_body TEXT,
	_register_reminder_subject TEXT,
	_register_reminder_html_body TEXT,
	_register_reminder_plain_body TEXT,
	_user_challenge_progress_subject TEXT,
	_user_challenge_progress_html_body TEXT,
	_user_challenge_progress_plain_body TEXT,
	_leaderboard_ranking_subject TEXT,
	_leaderboard_ranking_html_body TEXT,
	_leaderboard_ranking_plain_body TEXT,
	_user_deleted_subject TEXT,
	_user_deleted_html_body TEXT,
	_user_deleted_plain_body TEXT,
	_user_nominated_subject TEXT,
	_user_nominated_html_body TEXT,
	_user_nominated_plain_body TEXT,
	_user_nomination_accepted_subject TEXT,
	_user_nomination_accepted_html_body TEXT,
	_user_nomination_accepted_plain_body TEXT,
  _social_update_rejected_subject TEXT,
  _social_update_rejected_html_body TEXT,
  _social_update_rejected_plain_body TEXT,
  _user_password_changed_subject TEXT,
  _user_password_changed_html_body TEXT,
  _user_password_changed_plain_body TEXT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	email_template
		SET			standard_template = _standard_template,
						email_confirmation_subject = _email_confirmation_subject,
						email_confirmation_html_body = _email_confirmation_html_body,
						email_confirmation_plain_body = _email_confirmation_plain_body,
						password_reset_subject = _password_reset_subject,
						password_reset_html_body = _password_reset_html_body,
						password_reset_plain_body = _password_reset_plain_body,
						result_rejected_subject = _result_rejected_subject,
						result_rejected_html_body = _result_rejected_html_body,
						result_rejected_plain_body = _result_rejected_plain_body,
						user_blocked_subject = _user_blocked_subject,
						user_blocked_html_body = _user_blocked_html_body,
						user_blocked_plain_body = _user_blocked_plain_body,
						user_unblocked_subject = _user_unblocked_subject,
						user_unblocked_html_body = _user_unblocked_html_body,
						user_unblocked_plain_body = _user_unblocked_plain_body,
						user_avatar_removed_subject = _user_avatar_removed_subject,
						user_avatar_removed_html_body =	_user_avatar_removed_html_body,
						user_avatar_removed_plain_body = _user_avatar_removed_plain_body,
						register_reminder_subject = _register_reminder_subject,
						register_reminder_html_body = _register_reminder_html_body,
						register_reminder_plain_body = _register_reminder_plain_body,
						user_challenge_progress_subject = _user_challenge_progress_subject,
						user_challenge_progress_html_body = _user_challenge_progress_html_body,
						user_challenge_progress_plain_body = _user_challenge_progress_plain_body,
						leaderboard_ranking_subject = _leaderboard_ranking_subject,
						leaderboard_ranking_html_body = _leaderboard_ranking_html_body,
						leaderboard_ranking_plain_body = _leaderboard_ranking_plain_body,
						user_deleted_subject = _user_deleted_subject,
						user_deleted_html_body = _user_deleted_html_body,
						user_deleted_plain_body = _user_deleted_plain_body,
						user_nominated_subject = _user_nominated_subject,
						user_nominated_html_body = _user_nominated_html_body,
						user_nominated_plain_body = _user_nominated_plain_body,
						user_nomination_accepted_subject = _user_nomination_accepted_subject,
						user_nomination_accepted_html_body = _user_nomination_accepted_html_body,
						user_nomination_accepted_plain_body = _user_nomination_accepted_plain_body,
            social_update_rejected_subject = _social_update_rejected_subject,
            social_update_rejected_html_body = _social_update_rejected_html_body,
            social_update_rejected_plain_body = _social_update_rejected_plain_body,
            user_password_changed_subject = _user_password_changed_subject,
            user_password_changed_html_body = _user_password_changed_html_body,
            user_password_changed_plain_body = _user_password_changed_plain_body
		WHERE		country_id = _country_id;

		RETURN to_json(1);
	END
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_email_template_get
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)	FROM (
		SELECT	standard_template AS "standardTemplate",
						email_confirmation_subject AS "emailConfirmationSubject",
						email_confirmation_html_body AS "emailConfirmationHtmlBody",
						email_confirmation_plain_body AS "emailConfirmationPlainBody",
						password_reset_subject AS "passwordResetSubject",
						password_reset_html_body AS "passwordResetHtmlBody",
						password_reset_plain_body AS "passwordResetPlainBody",
						result_rejected_subject AS "resultRejectedSubject",
						result_rejected_html_body AS "resultRejectedHtmlBody",
						result_rejected_plain_body AS "resultRejectedPlainBody",
						user_blocked_subject AS "userBlockedSubject",
						user_blocked_html_body AS "userBlockedHtmlBody",
						user_blocked_plain_body AS "userBlockedPlainBody",
						user_unblocked_subject AS "userUnblockedSubject",
						user_unblocked_html_body AS "userUnblockedHtmlBody",
						user_unblocked_plain_body AS "userUnblockedPlainBody",
						user_avatar_removed_subject AS "userAvatarRemovedSubject",
						user_avatar_removed_html_body AS "userAvatarRemovedHtmlBody",
						user_avatar_removed_plain_body AS "userAvatarRemovedPlainBody",
						register_reminder_subject AS "registerReminderSubject",
						register_reminder_html_body AS "registerReminderHtmlBody",
						register_reminder_plain_body AS "registerReminderPlainBody",
						user_challenge_progress_subject AS "userChallengeProgressSubject",
						user_challenge_progress_html_body AS "userChallengeProgressHtmlBody",
						user_challenge_progress_plain_body AS "userChallengeProgressPlainBody",
						leaderboard_ranking_subject AS "leaderboardRankingSubject",
						leaderboard_ranking_html_body AS "leaderboardRankingHtmlBody",
						leaderboard_ranking_plain_body AS "leaderboardRankingPlainBody",
						user_deleted_subject AS "userDeletedSubject",
						user_deleted_html_body AS "userDeletedHtmlBody",
						user_deleted_plain_body AS "userDeletedPlainBody",
						user_nominated_subject AS "userNominatedSubject",
						user_nominated_html_body AS "userNominatedHtmlBody",
						user_nominated_plain_body AS "userNominatedPlainBody",
						user_nomination_accepted_subject AS "userNominationAcceptedSubject",
						user_nomination_accepted_html_body AS "userNominationAcceptedHtmlBody",
						user_nomination_accepted_plain_body AS "userNominationAcceptedPlainBody",
            social_update_rejected_subject AS "socialUpdateRejectedSubject",
            social_update_rejected_html_body AS "socialUpdateRejectedHtmlBody",
            social_update_rejected_plain_body AS "socialUpdateRejectedPlainBody",
            user_password_changed_subject AS "userPasswordChangedSubject",
            user_password_changed_html_body AS "userPasswordChangedHtmlBody",
            user_password_changed_plain_body AS "userPasswordChangedPlainBody"
		FROM		email_template
		WHERE		country_id = _country_id
	) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_task_runner_email_template_get
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)	FROM (
		SELECT	standard_template AS "standardTemplate",
						email_confirmation_subject AS "emailConfirmationSubject",
						email_confirmation_html_body AS "emailConfirmationHtmlBody",
						email_confirmation_plain_body AS "emailConfirmationPlainBody",
						password_reset_subject AS "passwordResetSubject",
						password_reset_html_body AS "passwordResetHtmlBody",
						password_reset_plain_body AS "passwordResetPlainBody",
						result_rejected_subject AS "resultRejectedSubject",
						result_rejected_html_body AS "resultRejectedHtmlBody",
						result_rejected_plain_body AS "resultRejectedPlainBody",
						user_blocked_subject AS "userBlockedSubject",
						user_blocked_html_body AS "userBlockedHtmlBody",
						user_blocked_plain_body AS "userBlockedPlainBody",
						user_unblocked_subject AS "userUnblockedSubject",
						user_unblocked_html_body AS "userUnblockedHtmlBody",
						user_unblocked_plain_body AS "userUnblockedPlainBody",
						user_avatar_removed_subject AS "userAvatarRemovedSubject",
						user_avatar_removed_html_body AS "userAvatarRemovedHtmlBody",
						user_avatar_removed_plain_body AS "userAvatarRemovedPlainBody",
						register_reminder_subject AS "registerReminderSubject",
						register_reminder_html_body AS "registerReminderHtmlBody",
						register_reminder_plain_body AS "registerReminderPlainBody",
						user_challenge_progress_subject AS "userChallengeProgressSubject",
						user_challenge_progress_html_body AS "userChallengeProgressHtmlBody",
						user_challenge_progress_plain_body AS "userChallengeProgressPlainBody",
						leaderboard_ranking_subject AS "leaderboardRankingSubject",
						leaderboard_ranking_html_body AS "leaderboardRankingHtmlBody",
						leaderboard_ranking_plain_body AS "leaderboardRankingPlainBody",
						user_deleted_subject AS "userDeletedSubject",
						user_deleted_html_body AS "userDeletedHtmlBody",
						user_deleted_plain_body AS "userDeletedPlainBody",
						user_nominated_subject AS "userNominatedSubject",
						user_nominated_html_body AS "userNominatedHtmlBody",
						user_nominated_plain_body AS "userNominatedPlainBody",
						user_nomination_accepted_subject AS "userNominationAcceptedSubject",
						user_nomination_accepted_html_body AS "userNominationAcceptedHtmlBody",
						user_nomination_accepted_plain_body AS "userNominationAcceptedPlainBody",
            social_update_rejected_subject AS "socialUpdateRejectedSubject",
            social_update_rejected_html_body AS "socialUpdateRejectedHtmlBody",
            social_update_rejected_plain_body AS "socialUpdateRejectedPlainBody",
            user_password_changed_subject AS "userPasswordChangedSubject",
            user_password_changed_html_body AS "userPasswordChangedHtmlBody",
            user_password_changed_plain_body AS "userPasswordChangedPlainBody"
		FROM		email_template
		WHERE		country_id = _country_id
	) AS result;
$$
LANGUAGE sql;