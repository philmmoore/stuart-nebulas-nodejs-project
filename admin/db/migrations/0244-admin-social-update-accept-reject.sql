ALTER TABLE social_update ADD COLUMN rejected TIMESTAMP;
ALTER TABLE social_update ADD COLUMN rejected_reason TEXT;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_social_update_get_all()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
	(
    SELECT		      u.id,
                    CASE WHEN COUNT(uf) < 1 THEN 0
                		ELSE
                			ROUND(100.0 * COUNT(uf) / (COUNT(ul) + COUNT(uf)), 2)
                		END AS "flagPercent",
                		COUNT(uf) AS "flagCount",
                		COUNT(ul) AS "likeCount",
                		u.body,
                    u.created,
                    
                    array
                    (
                      SELECT			id
                      FROM				social_update_video
                      WHERE				update_id = u.id
                      AND         processed IS NOT NULL
                    ) AS "videos",
                    array
                    (
                      SELECT			id
                      FROM				social_update_image
                      WHERE				update_id = u.id
                      AND         processed IS NOT NULL
                    ) AS "images",
                    array
                    (
                      SELECT      t.value
                      FROM				social_update_to_social_update_tag AS ut
                      JOIN        social_update_tag AS t ON ut.tag_id = t.id
                      WHERE				ut.update_id = u.id
                    ) AS "tags",

                		us.id AS "userId",
                		us.username AS "username"
    FROM		        social_update AS u
    LEFT OUTER JOIN social_update_like AS ul ON ul.update_id = u.id
    LEFT OUTER JOIN social_update_flag AS uf ON uf.update_id = u.id
    LEFT OUTER JOIN	social_update_to_result AS ur ON u.id = ur.update_id
    JOIN						"user" AS us on us.id = u.user_id
    WHERE           u.approved IS NULL
    AND             u.rejected IS NULL
    AND             ur IS NULL
    GROUP BY        u.id, us.id
    ORDER BY        "flagPercent" DESC,
                    u.created DESC
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_task_runner_social_update_image_processed
(
  _id UUID
)
RETURNS JSON AS
$$
  UPDATE  social_update_image
  SET     processed = current_timestamp
  WHERE   id = _id;
  
  UPDATE  social_update
  SET     approved = NULL
  WHERE   id = (SELECT update_id FROM social_update_image WHERE id = _id);
  
  SELECT to_json(1);
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_task_runner_social_update_video_processed
(
  _id UUID
)
RETURNS JSON AS
$$
  UPDATE  social_update_video
  SET     processed = current_timestamp
  WHERE   id = _id;
  
  UPDATE  social_update
  SET     approved = NULL
  WHERE   id = (SELECT update_id FROM social_update_video WHERE id = _id);
  
  SELECT to_json(1);
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_social_get_global_updates
(
  _from_timestamp TIMESTAMP,
  _number_of_updates INT
)
RETURNS JSON AS
$$
  SELECT array_to_json(array_agg(result_to_return))
  FROM
  (
    SELECT        ud.id,
                  ud.created,
                  ud.user_id AS "userId",
                  ud.body,
                  u.username,
                  u.first_name AS "firstName",
                  u.last_name AS "lastName",
                  u.avatar_uploaded AS "avatarUploaded",
                  (SELECT COUNT(*) FROM social_update_like WHERE update_id = ud.id) AS "likes",
                  (SELECT COUNT(*) FROM social_update_flag WHERE update_id = ud.id) AS "flags",
                  (
                      SELECT row_to_json(a) AS "result"
                      FROM
                      (
                              SELECT      r.id,
                                          r.result,
                                          r.submitted,
                                          c.id AS "challengeId",
                                          c.name AS "challengeName",
                                          cg.id AS "challengeGroupId",
                                          cg.value AS "challengeGroupName",
                                          cc.value AS "category",
                                          cc.id AS "categoryId",
                                          ct.measurement AS "measurement",
                                          r.image_validated AS "imageValidated",
                                          r.video_validated AS "videoValidated"
                              FROM    result AS r
                              JOIN    social_update_to_result AS ur ON ur.result_id = r.id
                              JOIN    challenge AS c ON r.challenge_id = c.id
                              JOIN    challenge_group AS cg ON r.challenge_group_id = cg.id
                              JOIN    challenge_type AS ct on c.type = ct.id
                              JOIN    challenge_category AS cc ON cc.id = c.category
                              WHERE   ur.update_id = ud.id
                              AND     r.excluded IS NULL
                      ) AS a
                  ),
                  array
                  (
                    SELECT			id
                    FROM				social_update_video
                    WHERE				update_id = ud.id
                    AND         processed IS NOT NULL
                  ) AS "videos",
                  array
                  (
                    SELECT			id
                    FROM				social_update_image
                    WHERE				update_id = ud.id
                    AND         processed IS NOT NULL
                  ) AS "images",
                  array
                  (
                    SELECT      t.value
                    FROM				social_update_to_social_update_tag AS ut
                    JOIN        social_update_tag AS t ON ut.tag_id = t.id
                    WHERE				ut.update_id = ud.id
                  ) AS "tags"
    FROM          social_update AS ud
    JOIN          "user" AS u ON ud.user_id = u.id
    WHERE         ud.created < _from_timestamp
    AND           u.blocked IS NULL
    AND           ud.rejected IS NULL
    ORDER BY      ud.created DESC
    LIMIT         _number_of_updates
  ) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_social_get_user_updates
(
  _user_id INT,
  _from_timestamp TIMESTAMP,
  _number_of_updates INT
)
RETURNS JSON AS
$$
  SELECT array_to_json(array_agg(result_to_return))
  FROM
  (
    SELECT        ud.id,
                  ud.created,
                  ud.user_id AS "userId",
                  ud.body,
                  u.username,
                  u.first_name AS "firstName",
                  u.last_name AS "lastName",
                  u.avatar_uploaded AS "avatarUploaded",
                  (SELECT COUNT(*) FROM social_update_like WHERE update_id = ud.id) AS "likes",
                  (SELECT COUNT(*) FROM social_update_flag WHERE update_id = ud.id) AS "flags",
                  (
                      SELECT row_to_json(a) AS "result"
                      FROM
                      (
                              SELECT      r.id,
                                          r.result,
                                          r.submitted,
                                          c.id AS "challengeId",
                                          c.name AS "challengeName",
                                          cg.id AS "challengeGroupId",
                                          cg.value AS "challengeGroupName",
                                          cc.value AS "category",
                                          cc.id AS "categoryId",
                                          ct.measurement AS "measurement",
                                          r.image_validated AS "imageValidated",
                                          r.video_validated AS "videoValidated"
                              FROM    result AS r
                              JOIN    social_update_to_result AS ur ON ur.result_id = r.id
                              JOIN    challenge AS c ON r.challenge_id = c.id
                              JOIN    challenge_group AS cg ON r.challenge_group_id = cg.id
                              JOIN    challenge_type AS ct on c.type = ct.id
                              JOIN    challenge_category AS cc ON cc.id = c.category
                              WHERE   ur.update_id = ud.id
                              AND     r.excluded IS NULL
                      ) AS a
                  ),
                  array
                  (
                    SELECT			id
                    FROM				social_update_video
                    WHERE				update_id = ud.id
                    AND         processed IS NOT NULL
                  ) AS "videos",
                  array
                  (
                    SELECT			id
                    FROM				social_update_image
                    WHERE				update_id = ud.id
                    AND         processed IS NOT NULL
                  ) AS "images",
                  array
                  (
                    SELECT      t.value
                    FROM				social_update_to_social_update_tag AS ut
                    JOIN        social_update_tag AS t ON ut.tag_id = t.id
                    WHERE				ut.update_id = ud.id
                  ) AS "tags"
    FROM          social_update AS ud
    JOIN          "user" AS u ON ud.user_id = u.id
    WHERE         ud.created < _from_timestamp
    AND           u.blocked IS NULL
    AND           ud.rejected IS NULL
    AND
    (
      ud.user_id = _user_id
      OR						ud.user_id IN
                    (
                      SELECT client_user_id FROM user_client WHERE user_id = _user_id
                      UNION
                      SELECT user_id FROM user_follower WHERE follower_user_id = _user_id
                    )
    )
    ORDER BY      ud.created DESC
    LIMIT         _number_of_updates
  ) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_social_update_get
(
  _id UUID
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
    SELECT		      u.id,
                    CASE WHEN COUNT(uf) < 1 THEN 0
                		ELSE
                			ROUND(100.0 * COUNT(uf) / (COUNT(ul) + COUNT(uf)), 2)
                		END AS "flagPercent",
                		COUNT(uf) AS "flagCount",
                		COUNT(ul) AS "likeCount",
                		u.body,
                    u.created,
                    
                    array
                    (
                      SELECT			id
                      FROM				social_update_video
                      WHERE				update_id = u.id
                      AND         processed IS NOT NULL
                    ) AS "videos",
                    array
                    (
                      SELECT			id
                      FROM				social_update_image
                      WHERE				update_id = u.id
                      AND         processed IS NOT NULL
                    ) AS "images",
                    array
                    (
                      SELECT      t.value
                      FROM				social_update_to_social_update_tag AS ut
                      JOIN        social_update_tag AS t ON ut.tag_id = t.id
                      WHERE				ut.update_id = u.id
                    ) AS "tags",

                		us.id AS "userId",
                		us.username AS "username"
    FROM		        social_update AS u
    LEFT OUTER JOIN social_update_like AS ul ON ul.update_id = u.id
    LEFT OUTER JOIN social_update_flag AS uf ON uf.update_id = u.id
    LEFT OUTER JOIN	social_update_to_result AS ur ON u.id = ur.update_id
    JOIN						"user" AS us on us.id = u.user_id
    WHERE           u.id = _id
    GROUP BY        u.id, us.id
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_social_update_approve
(
	_id UUID
)
RETURNS JSON AS
$$
	WITH rows AS
	(
		UPDATE		social_update
		SET				approved = current_timestamp,
							rejected = NULL,
							rejected_reason = NULL
		WHERE 		id = _id
		AND				approved IS NULL
		RETURNING 1
	)
	SELECT to_json(COUNT(*)) FROM rows;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_social_update_delete_image
(
	_id UUID
)
RETURNS JSON AS
$$
	WITH rows AS
	(
		DELETE FROM social_update_image
    WHERE id = _id
		RETURNING 1
	)
	SELECT to_json(COUNT(*)) FROM rows;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_social_update_delete_video
(
	_id UUID
)
RETURNS JSON AS
$$
	WITH rows AS
	(
		DELETE FROM social_update_video
    WHERE id = _id
		RETURNING 1
	)
	SELECT to_json(COUNT(*)) FROM rows;
$$
LANGUAGE sql;