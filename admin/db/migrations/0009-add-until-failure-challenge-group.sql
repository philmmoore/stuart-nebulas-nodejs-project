INSERT INTO challenge_group (value) VALUES ('Until Failure');

INSERT INTO challenge_to_challenge_group (challenge_id, challenge_group_id) VALUES
  (9, 38),
  (10, 38);

ALTER TABLE challenge ADD CONSTRAINT challenge_unique_name UNIQUE (name);
