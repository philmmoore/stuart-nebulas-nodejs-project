CREATE OR REPLACE FUNCTION sp_gymfit_task_runner_social_update_image_processed
(
  _id UUID
)
RETURNS JSON AS
$$
  UPDATE  social_update_image
  SET     processed = current_timestamp
  WHERE   id = _id;
  
  SELECT to_json(1);
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_task_runner_social_update_video_processed
(
  _id UUID
)
RETURNS JSON AS
$$
  UPDATE  social_update_video
  SET     processed = current_timestamp
  WHERE   id = _id;
  
  SELECT to_json(1);
$$
LANGUAGE sql;