CREATE FUNCTION sp_gymfit_site_user_confirm_email
(
	_key UUID
)
RETURNS JSON AS
$$
	DECLARE
		_username TEXT;
	BEGIN
		DELETE
		FROM				user_email_verification AS ev
		USING				"user" AS u
		WHERE				ev.user_id = u.id
		AND					ev.key = _key
		RETURNING		u.username
		INTO				_username;

		RETURN to_json(_username);
	END
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_registration_add_user
(
	_registration_id INT
)
RETURNS JSON AS
$$
	DECLARE
		_user_id INT;
	BEGIN
		INSERT INTO	"user" (username, hash, first_name, last_name, date_of_birth, email)
		SELECT			username,
								hash,
								first_name,
								last_name,
								date_of_birth,
								email
		FROM				user_registration
		WHERE				id = _registration_id
		RETURNING		id
		INTO				_user_id;

		DELETE FROM	user_registration
		WHERE				id = _registration_id;

		INSERT INTO	user_email_verification (user_id)
		VALUES			(_user_id);

		RETURN row_to_json(result)
		FROM (
			SELECT					u.id,
											u.username,
											u.email,
											ev.key AS "verificationKey"
			FROM						"user" AS u
			LEFT OUTER JOIN	user_email_verification AS ev ON ev.user_id = u.id
			WHERE						u.id = _user_id
		) AS result;
	END;
$$
LANGUAGE plpgsql;
