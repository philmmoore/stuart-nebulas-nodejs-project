CREATE OR REPLACE FUNCTION sp_gymfit_api_users_near_me(lat numeric, lon numeric, radius numeric, user_is_pt boolean, user_is_gym boolean, userid numeric, show_following boolean)
 RETURNS json
 LANGUAGE sql
AS $function$
    
            SELECT array_to_json(array_agg(result_to_return))
            FROM (
                SELECT 
                    DISTINCT(u.id),
                    u.username,
                    u.first_name as "firstName",
                    u.last_name as "lastName",
                    u.is_male as "isMale",
                    u.is_personal_trainer as "isPersonalTrainer",
                    u.is_gym as "isGym",
                    u.bio,
                    d.name as "district",
                    point(lon, lat) <@> point(d.longitude, d.latitude)::point AS "distance",
                    u.avatar_uploaded AS "avatarUploaded"
               FROM user_district ud
                JOIN "user" u ON u.id = ud.user_id
                JOIN district d ON d.id = ud.district_id
                WHERE (ud.district_id IN (
                        SELECT 
                           d.id
                        FROM district d
                        WHERE (point(lon, lat) <@> point(d.longitude, d.latitude)) < radius -- SEARCH RADIUS IN MILES
                        GROUP by d.id
                    )
                ) AND u.is_personal_trainer = user_is_pt 
                    AND u.is_gym = user_is_gym 
                    AND u.id != userId
                    AND u.id NOT IN (
                        SELECT                  following.id
                        FROM                    "user" AS following
                        JOIN                    user_follower AS uf on uf.user_id = following.id
                        WHERE                   uf.follower_user_id = userId
                        AND                     (show_following != TRUE)
                        GROUP BY uf.created, following.id
                        ORDER BY                uf.created DESC
                    ) 


                ORDER by distance ASC
            ) as "result_to_return"
        $function$
