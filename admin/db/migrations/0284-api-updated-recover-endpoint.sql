CREATE OR REPLACE FUNCTION sp_gymfit_api_auth_recover_username(_email text)
 RETURNS json
 LANGUAGE sql
AS $function$

SELECT to_json(result_to_return)
  FROM
  (
    SELECT          email, username
    FROM            "user"
    WHERE           email = _email
  ) AS result_to_return;

$function$
