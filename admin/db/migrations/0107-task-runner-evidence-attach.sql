CREATE OR REPLACE FUNCTION sp_gymfit_task_runner_evidence_set (
  _result_id INT,
  _is_image BIT
)
RETURNS JSON AS
$$
BEGIN
	UPDATE  result
  SET     image_validated = current_timestamp
  WHERE   id = _result_id
	AND			_is_image = '1';

	UPDATE  result
  SET     video_validated = current_timestamp
	WHERE   id = _result_id
	AND			_is_image = '0';

  RETURN to_json(1);
END;
$$
LANGUAGE plpgsql;
