ALTER TABLE blog_post RENAME COLUMN created TO published;

DROP FUNCTION IF EXISTS sp_gymfit_admin_blog_save_post(INT, INT, TEXT, TEXT);
DROP FUNCTION IF EXISTS sp_gymfit_admin_blog_save_post(INT, INT, TEXT, TEXT, TEXT);

CREATE OR REPLACE FUNCTION sp_gymfit_admin_blog_save_post
(
	_country_id INT,
	_post_id INT,
	_published TIMESTAMP,
	_name TEXT,
	_body_copy TEXT,
	_summary TEXT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	blog_post
		SET			name = _name,
						published = _published,
						body_copy = _body_copy,
						summary = _summary
		WHERE		id = _post_id;

		IF NOT FOUND THEN
			INSERT INTO blog_post (country, published, name, body_copy, summary)
			VALUES			(_country_id, _published, _name, _body_copy, _summary)
			RETURNING		id
			INTO				_post_id;
		END IF;

		RETURN to_json(_post_id);
	END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_blog_get_all_posts_for_country
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM
		(
			SELECT		id,
								name,
								published
			FROM			blog_post
			WHERE			country = _country_id
			ORDER BY	published DESC
		) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_blog_get_post
(
	_post_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
		(
			SELECT	id,
							name,
							body_copy AS "bodyCopy",
							summary,
							published
			FROM		blog_post
			WHERE		id = _post_id
			LIMIT		1
		) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_blog_get_posts
(
	_country_id INT,
	_posts_per_page INT,
	_page INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
		(
				SELECT	ceiling(count(*)::DECIMAL / _posts_per_page) AS "numberOfPages",
								count(*) AS "numberOfPosts",
								(
									SELECT array_to_json(array_agg(result_b))
									FROM (
										SELECT		name,
															summary,
															published
										FROM			blog_post
										WHERE			country = _country_id
										ORDER BY	published DESC
										OFFSET		(_page - 1) * _posts_per_page
										LIMIT			_posts_per_page
									) AS result_b
							) AS "posts"
			FROM 		blog_post
	) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_blog_get_post
(
	_country_id INT,
	_name TEXT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM (
		SELECT	name,
						body_copy AS "bodyCopy",
						published
		FROM		blog_post
		WHERE		country = _country_id
		AND			LOWER(REPLACE(name, ' ', '-')) = LOWER(_name)
		LIMIT		1
	) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_blog_get_posts
(
	_country_id INT,
	_posts_per_page INT,
	_page INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
		(
				SELECT	ceiling(count(*)::DECIMAL / _posts_per_page) AS "numberOfPages",
								count(*) AS "numberOfPosts",
								(
									SELECT array_to_json(array_agg(result_b))
									FROM (
										SELECT		id,
															name,
															summary,
															published
										FROM			blog_post
										WHERE			country = _country_id
										ORDER BY	published DESC
										OFFSET		(_page - 1) * _posts_per_page
										LIMIT			_posts_per_page
									) AS result_b
							) AS "posts"
			FROM 		blog_post
	) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_blog_get_post
(
	_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM (
		SELECT	name,
						body_copy AS "bodyCopy",
						published
		FROM		blog_post
		WHERE		id = _id
	) AS result;
$$
LANGUAGE sql;
