CREATE OR REPLACE FUNCTION sp_gymfit_api_challenge_get()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
		(
			SELECT
							array
							(
                  SELECT row_to_json(a)
                  FROM (
                          SELECT			cg.id,
                                      cg.value AS "name"
                          FROM				challenge_group AS cg
													INNER JOIN	challenge_to_challenge_group AS ccg ON ccg.challenge_group_id = cg.id
													WHERE				ccg.challenge_id = c.id
                  ) AS a
              ) AS "groups",
							c.id,
							c.name,
							cc.value AS "category",
							ct.value AS "type",
							ct.measurement,
							c.body_copy AS "bodyCopy",
							c.submission_message AS "submissionMessage"
			FROM		challenge AS c
			LEFT OUTER JOIN challenge_category AS cc ON cc.id = c.category
			LEFT OUTER JOIN	challenge_type AS ct ON ct.id = c.type
		) AS result_to_return;
$$
LANGUAGE sql;
