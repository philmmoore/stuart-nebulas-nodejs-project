CREATE OR REPLACE FUNCTION sp_gymfit_api_social_update_search
(
  _user_id INT,
  _search_term TEXT
)
RETURNS JSON AS
$$
  SELECT array_to_json(array_agg(result_to_return))
  FROM
  (
    SELECT        ud.id,
                  ud.created,
                  ud.user_id AS "userId",
                  ud.body,
                  u.username,
                  u.first_name AS "firstName",
                  u.last_name AS "lastName",
                  u.avatar_uploaded AS "avatarUploaded",
                  u.bio,
                  (SELECT COUNT(*) FROM social_update_like WHERE update_id = ud.id) AS "likes",
                  (SELECT COUNT(*) FROM social_update_flag WHERE update_id = ud.id) AS "flags",
                  (
                      SELECT row_to_json(a) AS "result"
                      FROM
                      (
                              SELECT      r.id,
                                          r.result,
                                          r.submitted,
                                          c.id AS "challengeId",
                                          c.name AS "challengeName",
                                          cg.id AS "challengeGroupId",
                                          cg.value AS "challengeGroupName",
                                          cc.value AS "category",
                                          cc.id AS "categoryId",
                                          ct.measurement AS "measurement",
                                          r.image_validated AS "imageValidated",
                                          r.video_validated AS "videoValidated"
                              FROM    result AS r
                              JOIN    social_update_to_result AS ur ON ur.result_id = r.id
                              JOIN    challenge AS c ON r.challenge_id = c.id
                              JOIN    challenge_group AS cg ON r.challenge_group_id = cg.id
                              JOIN    challenge_type AS ct on c.type = ct.id
                              JOIN    challenge_category AS cc ON cc.id = c.category
                              WHERE   ur.update_id = ud.id
                              AND     r.excluded IS NULL
                      ) AS a
                  ),
                  array
                  (
                    SELECT			id
                    FROM				social_update_video
                    WHERE				update_id = ud.id
                    AND         processed IS NOT NULL
                  ) AS "videos",
                  array
                  (
                    SELECT			id
                    FROM				social_update_image
                    WHERE				update_id = ud.id
                    AND         processed IS NOT NULL
                  ) AS "images",
                  array
                  (
                    SELECT      t.value
                    FROM				social_update_to_social_update_tag AS ut
                    JOIN        social_update_tag AS t ON ut.tag_id = t.id
                    WHERE				ut.update_id = ud.id
                  ) AS "tags",
                  CASE WHEN (SELECT TRUE FROM social_update_like WHERE user_id = _user_id AND update_id = ud.id) = TRUE THEN TRUE ELSE FALSE END AS "userLikes",
                  CASE WHEN (SELECT TRUE FROM social_update_flag WHERE user_id = _user_id AND update_id = ud.id) = TRUE THEN TRUE ELSE FALSE END AS "userFlagged"
    FROM          social_update AS ud
    JOIN          "user" AS u ON ud.user_id = u.id
    JOIN          social_update_to_social_update_tag AS sut ON sut.update_id = ud.id
    JOIN          social_update_tag AS t ON t.id = sut.tag_id
    WHERE         LOWER(TRIM(leading '#@' FROM t.value)) LIKE '%' || LOWER(TRIM(leading '#@' FROM _search_term)) || '%'
    AND           u.blocked IS NULL
    AND           ud.rejected IS NULL
    ORDER BY      ud.created DESC
  ) AS result_to_return;
$$
LANGUAGE sql;