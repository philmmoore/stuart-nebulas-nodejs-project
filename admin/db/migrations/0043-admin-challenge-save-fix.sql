CREATE OR REPLACE FUNCTION sp_gymfit_admin_challenge_save
(
	_challengeId INT,
	_name TEXT,
	_category INT,
	_type INT,
	_group_ids INT[],
	_body_copy TEXT,
	_submission_message TEXT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	challenge
		SET			name = _name,
						category = _category,
						type = _type,
						body_copy = _body_copy,
						submission_message = _submission_message
		WHERE		id = _challengeId;

		IF FOUND THEN
			DELETE FROM	challenge_to_challenge_group
			WHERE				challenge_id = _challengeId;
		ELSE
			INSERT INTO challenge (name, category, type, body_copy, submission_message)
			VALUES			(_name, _category, _type, _body_copy, _submission_message)
			RETURNING		id
			INTO				_challengeId;
		END IF;

		INSERT INTO		challenge_to_challenge_group (challenge_id, challenge_group_id)
		SELECT				_challengeId,
									_group_id
		FROM					unnest(_group_ids) AS _group_id;

		RETURN to_json(_challengeId);
	END;
$$
LANGUAGE plpgsql;
