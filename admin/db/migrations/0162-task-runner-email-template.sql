CREATE FUNCTION sp_gymfit_task_runner_email_template_get
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)	FROM (
		SELECT	template AS "standardTemplate"
		FROM		email_template
		WHERE		country_id = _country_id
		LIMIT		1
	) AS result;
$$
LANGUAGE sql;