CREATE OR REPLACE FUNCTION sp_gymfit_shared_social_update_like
(
  _user_id INT,
  _update_id UUID
)
RETURNS JSON AS
$$
  INSERT INTO social_update_like (update_id, user_id)
  SELECT      _update_id,
              _user_id
  WHERE       NOT EXISTS (SELECT * FROM social_update_like WHERE update_id = _update_id AND user_id = _user_id);
  
  DELETE FROM social_update_flag
  WHERE       update_id = _update_id
  AND         user_id = _user_id;
  
  SELECT row_to_json(result)
  FROM
  (
    SELECT  (SELECT COUNT(*) FROM social_update_like WHERE update_id = _update_id) AS "likes",
            (SELECT COUNT(*) FROM social_update_flag WHERE update_id = _update_id) AS "flags"
  ) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_shared_social_update_unlike
(
  _user_id INT,
  _update_id UUID
)
RETURNS JSON AS
$$
  DELETE FROM social_update_like
  WHERE       update_id = _update_id
  AND         user_id = _user_id;
  
  SELECT row_to_json(result)
  FROM
  (
    SELECT  (SELECT COUNT(*) FROM social_update_like WHERE update_id = _update_id) AS "likes",
            (SELECT COUNT(*) FROM social_update_flag WHERE update_id = _update_id) AS "flags"
  ) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_shared_social_update_flag
(
  _user_id INT,
  _update_id UUID
)
RETURNS JSON AS
$$  
  INSERT INTO social_update_flag (update_id, user_id)
  SELECT      _update_id,
              _user_id
  WHERE       NOT EXISTS (SELECT * FROM social_update_flag WHERE update_id = _update_id AND user_id = _user_id);
  
  DELETE FROM social_update_like
  WHERE       update_id = _update_id
  AND         user_id = _user_id;
  
  SELECT row_to_json(result)
  FROM
  (
    SELECT  (SELECT COUNT(*) FROM social_update_like WHERE update_id = _update_id) AS "likes",
            (SELECT COUNT(*) FROM social_update_flag WHERE update_id = _update_id) AS "flags"
  ) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_shared_social_update_unflag
(
  _user_id INT,
  _update_id UUID
)
RETURNS JSON AS
$$  
  DELETE FROM social_update_flag
  WHERE       update_id = _update_id
  AND         user_id = _user_id;
  
  SELECT row_to_json(result)
  FROM
  (
    SELECT  (SELECT COUNT(*) FROM social_update_like WHERE update_id = _update_id) AS "likes",
            (SELECT COUNT(*) FROM social_update_flag WHERE update_id = _update_id) AS "flags"
  ) AS result;
$$
LANGUAGE sql;


CREATE OR REPLACE FUNCTION sp_gymfit_api_social_update_like
(
  _user_id INT,
  _update_id UUID
)
RETURNS JSON AS
$$
  SELECT sp_gymfit_shared_social_update_like(_user_id, _update_id);
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_social_update_unlike
(
  _user_id INT,
  _update_id UUID
)
RETURNS JSON AS
$$
  SELECT sp_gymfit_shared_social_update_unlike(_user_id, _update_id);
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_social_update_flag
(
  _user_id INT,
  _update_id UUID
)
RETURNS JSON AS
$$
  SELECT sp_gymfit_shared_social_update_flag(_user_id, _update_id);
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_social_update_unflag
(
  _user_id INT,
  _update_id UUID
)
RETURNS JSON AS
$$
  SELECT sp_gymfit_shared_social_update_unflag(_user_id, _update_id);
$$
LANGUAGE sql;


DROP TABLE result_feedback;
DROP FUNCTION sp_gymfit_site_result_like(int, int);
DROP FUNCTION sp_gymfit_site_result_flag(int, int);

CREATE OR REPLACE FUNCTION sp_gymfit_site_result_like
(
	_result_id BIGINT,
	_user_id INT
)
RETURNS JSON AS
$$
  SELECT sp_gymfit_shared_social_update_like(_user_id, (SELECT update_id FROM social_update_to_result WHERE result_id = _result_id));
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_result_unlike
(
	_result_id BIGINT,
	_user_id INT
)
RETURNS JSON AS
$$
  SELECT sp_gymfit_shared_social_update_unlike(_user_id, (SELECT update_id FROM social_update_to_result WHERE result_id = _result_id));
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_result_flag
(
	_result_id BIGINT,
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT sp_gymfit_shared_social_update_flag(_user_id, (SELECT update_id FROM social_update_to_result WHERE result_id = _result_id));
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_result_unflag
(
	_result_id BIGINT,
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT sp_gymfit_shared_social_update_unflag(_user_id, (SELECT update_id FROM social_update_to_result WHERE result_id = _result_id));
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_result_flagged()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
	(
		SELECT					ROUND(100.0 * COUNT(uf) / (COUNT(ul) + COUNT(uf)), 2) AS "flagPercent",
										COUNT(uf) AS "flagCount",
										COUNT(ul) AS "likeCount",
										r.id AS "resultId",
										r.result,
										r.image_validated AS "imageValidated",
										r.video_validated AS "videoValidated",
										u.id AS "userId",
										u.username AS "username",
										c.name AS "challengeName",
										cg.value AS "challengeGroup",
										ct.measurement AS "measurement",
										(SELECT COUNT(*) FROM result WHERE excluded IS NOT NULL AND user_id = u.id) AS "numberOfExcludedResults"
		FROM						social_update_to_result AS ur
    LEFT OUTER JOIN social_update_like AS ul ON ul.update_id = ur.update_id
    LEFT OUTER JOIN social_update_flag AS uf ON uf.update_id = ur.update_id
		JOIN						result AS r on r.id = ur.result_id
		JOIN						"user" AS u on u.id = r.user_id
		JOIN						challenge AS c on c.id = r.challenge_id
		JOIN						challenge_group AS cg on cg.id = r.challenge_group_id
		JOIN						challenge_type AS ct on ct.id = c.type
		WHERE						r.excluded IS NULL
		AND							r.approved IS NULL
		AND							(SELECT COUNT(*) FROM social_update_flag WHERE update_id = ur.update_id) > 0
		GROUP BY				r.id,
										u.id,
										c.id,
										cg.id,
										ct.id
	 	ORDER BY				"flagPercent" DESC
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_result_get
(
	_result_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
		(
			SELECT					CASE WHEN COUNT(uf) < 1 THEN 0
											ELSE
												ROUND(100.0 * COUNT(uf) / (COUNT(ul) + COUNT(uf)), 2)
											END AS "flagPercent",
                      COUNT(uf) AS "flagCount",
  										COUNT(ul) AS "likeCount",
											r.id AS "resultId",
											r.result,
											r.submitted,
											r.image_validated AS "imageValidated",
											r.video_validated AS "videoValidated",
											u.id AS "userId",
											u.username AS "username",
											u.email,
											c.name AS "challengeName",
											cg.value AS "challengeGroup",
											ct.measurement AS "measurement",
											(SELECT COUNT(*) FROM result WHERE excluded IS NOT NULL AND user_id = u.id) AS "numberOfExcludedResults"
			FROM						result AS r
      JOIN            social_update_to_result AS ur ON ur.result_id = r.id
      LEFT OUTER JOIN social_update_like AS ul ON ul.update_id = ur.update_id
      LEFT OUTER JOIN social_update_flag AS uf ON uf.update_id = ur.update_id
			JOIN						"user" AS u on u.id = r.user_id
			JOIN						challenge AS c on c.id = r.challenge_id
			JOIN						challenge_group AS cg on cg.id = r.challenge_group_id
			JOIN						challenge_type AS ct on ct.id = c.type
			WHERE						r.id = _result_id
			GROUP BY				r.id,
											u.id,
											c.id,
											cg.id,
											ct.id
		) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_dashboard_chart()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
		(
			SELECT 	date_trunc('day', date_row.generate_series) AS "date",
							(
								SELECT count(*) FROM "user" WHERE date_trunc('day', created) = date_trunc('day', date_row.generate_series)
							) AS "registrations",
							(
								SELECT count(*) FROM result WHERE date_trunc('day', submitted) = date_trunc('day', date_row.generate_series) AND excluded IS NULL
							) AS "resultSubmissions",
							(
								SELECT count(*) FROM result WHERE date_trunc('day', image_validated) = date_trunc('day', date_row.generate_series) AND excluded IS NULL
							) AS "imagesUploaded",
							(
								SELECT count(*) FROM result WHERE date_trunc('day', video_validated) = date_trunc('day', date_row.generate_series) AND excluded IS NULL
							) AS "videosUploaded",
							(
								SELECT count(DISTINCT user_id) FROM user_audit WHERE date_trunc('day', timestamp) = date_trunc('day', date_row.generate_series)
							) AS "logins",
							(
								SELECT count(*) FROM social_update_like WHERE date_trunc('day', created) = date_trunc('day', date_row.generate_series)
							) AS "likes"
				FROM
				(
				    SELECT generate_series
				    (
				        current_timestamp - interval '30 days',
				        current_timestamp,
				        '1 day'
				     )
				) AS date_row
		) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_home_latest_results()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
	(
		SELECT					(SELECT COUNT(*) FROM social_update_flag WHERE update_id = ur.update_id) AS "flagCount",
                    (SELECT COUNT(*) FROM social_update_like WHERE update_id = ur.update_id) AS "likeCount",
										r.id AS "resultId",
										r.result,
										r.image_validated AS "imageValidated",
										r.video_validated AS "videoValidated",
										r.submitted,
										u.id AS "userId",
										u.username AS "username",
										c.name AS "challengeName",
										cg.value AS "challengeGroup",
										ct.measurement AS "measurement",
										(SELECT COUNT(*) FROM result WHERE excluded IS NOT NULL AND user_id = u.id) AS "numberOfExcludedResults"
		FROM						result AS r
    JOIN            social_update_to_result AS ur ON ur.result_id = r.id
		JOIN						"user" AS u on u.id = r.user_id
		JOIN						challenge AS c on c.id = r.challenge_id
		JOIN						challenge_group AS cg on cg.id = r.challenge_group_id
		JOIN						challenge_type AS ct on ct.id = c.type
		WHERE						r.excluded IS NULL
		AND							r.approved IS NULL
		GROUP BY				r.id,
                    ur.update_id,
										u.id,
										c.id,
										cg.id,
										ct.id
	 	ORDER BY				r.submitted DESC
		LIMIT						20
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_leaderboard_search
(
	_challenge_id INT,
	_challenge_group_id INT,
	_district_id INT,
	_county_id INT,
	_region_id INT,
	_country_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_last_period_start TIMESTAMP,
	_last_period_end TIMESTAMP,
	_results_per_page INT,
	_page INT,
	_is_male BIT,
	_age_min SMALLINT,
	_age_max SMALLINT,
	_weight_min INT,
	_weight_max INT,
	_only_validated BIT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		WITH filtered_results AS
		(
			SELECT				r.id,
										r.submitted,
										r.result,
										r.user_id,
										r.image_validated,
										r.video_validated,
										ct.measurement,
                    ct.min_first,
										u.username,
										u.avatar_uploaded,
										u.first_name,
										u.last_name,
                    sur.update_id
			FROM					result AS r
      JOIN          social_update_to_result AS sur ON sur.result_id = r.id
			JOIN					"user" AS u ON r.user_id = u.id
			JOIN 					challenge AS c ON c.id = r.challenge_id
			JOIN					challenge_type AS ct ON ct.id = c.type			
			WHERE					r.submitted >= _last_period_start
			AND						r.submitted <= _this_period_end
			AND						r.challenge_id = _challenge_id
			AND						r.challenge_group_id = _challenge_group_id
			AND						(r.district_id = _district_id OR _district_id IS NULL)
			AND						(r.county_id = _county_id OR _county_id IS NULL)
			AND						(r.region_id = _region_id OR _region_id IS NULL)
			AND						(r.country_id = _country_id OR _country_id IS NULL)
			AND						(r.age >= _age_min OR _age_min IS NULL)
			AND						(r.age <= _age_max OR _age_max IS NULL)
			AND						(r.weight >= _weight_min OR _weight_min IS NULL)
			AND						(r.weight <= _weight_max OR _weight_max IS NULL)
			AND						((r.image_validated IS NOT NULL OR r.video_validated IS NOT NULL) OR _only_validated != '1')
			AND						(u.is_male = _is_male OR _is_male IS NULL)
			AND						r.excluded IS NULL
		)
		
		SELECT					(
											SELECT 				ceiling(COUNT(DISTINCT user_id)::DECIMAL / _results_per_page) AS "numberOfPages"
											FROM					filtered_results
											WHERE					submitted >= _this_period_start
										) AS "numberOfPages",
		
										(
											SELECT				measurement
											FROM					filtered_results
											LIMIT					1
										) AS "measurement",
										
										(
											SELECT array_to_json(array_agg(paged_results))
											FROM
											(
												
												
												
												
												WITH current_ranges AS
												(
													SELECT			row_number() over (ORDER BY MAX(frr.result) DESC) AS max_rank,
																			row_number() over (ORDER BY MIN(frr.result)) AS min_rank,
																			(
																				SELECT		id
																				FROM			filtered_results
																				WHERE 		result = MAX(frr.result)
																				AND				user_id = frr.user_id
																				ORDER BY 	video_validated DESC NULLS LAST,
																				 					image_validated DESC NULLS LAST,
																									submitted DESC
																				LIMIT 		1
																			) AS max_result_id,
																			(
																				SELECT 		id
																				FROM 			filtered_results
																				WHERE 		result = MIN(frr.result)
																				AND 			user_id = frr.user_id
																				ORDER BY 	video_validated DESC NULLS LAST,
																				 					image_validated DESC NULLS LAST,
																									submitted DESC
																				LIMIT 		1
																			) AS min_result_id,
																			frr.user_id
													FROM 				filtered_results AS frr
													WHERE				submitted >= _this_period_start
													GROUP BY		frr.user_id
												)
												
												SELECT				fr.id,
																			CASE
																				WHEN fr.min_first = TRUE THEN rg.min_rank
																				ELSE rg.max_rank
																			END AS "rank",
																			fr.result,
																			fr.image_validated AS "imageValidated",
																			fr.video_validated AS "videoValidated",
																			fr.user_id AS "userId",
																			fr.username,
																			fr.avatar_uploaded AS "userHasAvatar",
									                    fr.first_name "firstName",
									                    fr.last_name "lastName",
																			(
																				SELECT      COUNT(*)
								                      	FROM				social_update_like
																				WHERE				update_id = fr.update_id
																			) AS "likeCount",
																			(
                                        SELECT      COUNT(*)
								                      	FROM				social_update_flag
																				WHERE				update_id = fr.update_id
																			) AS "flagCount",
																			(
																				
																				
																				WITH last_rank AS
																				(
																					SELECT			lfr.user_id,
																											CASE
																												WHEN fr.min_first = TRUE THEN
                                                          row_number() over (ORDER BY MIN(lfr.result))
																												ELSE
																													row_number() over (ORDER BY MAX(lfr.result) DESC)
																											END AS last_rank_result
																					FROM 				filtered_results AS lfr
																					WHERE				lfr.submitted <= _last_period_end
																					GROUP BY		lfr.user_id
																				)
																				SELECT	last_rank_result
																				FROM		last_rank
																				WHERE		user_id = fr.user_id
																				LIMIT 1
																				
																				
																			) AS "lastRank"
												FROM					current_ranges AS rg
												JOIN					filtered_results AS fr ON fr.id = CASE
																																					WHEN fr.min_first = TRUE THEN rg.min_result_id
																																					ELSE rg.max_result_id
																																				END
												ORDER BY 		  rank
												OFFSET				(_page - 1) * _results_per_page
												LIMIT					_results_per_page
																															
																															
																															
                      ) AS paged_results
										) AS "results"
		
		FROM filtered_results
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_user_delete
(
	_user_id INT,
	_reason TEXT
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO banned_email
		(
			email,
			reason
		)
		SELECT			email,
								_reason
		FROM				"user"
		WHERE				id = _user_id;
		
		DELETE FROM	social_notification
		WHERE				user_id = _user_id;
		
		DELETE FROM	user_email_verification
		WHERE				user_id = _user_id;
		
		DELETE FROM	user_password_reset
		WHERE				user_id = _user_id;
	
		DELETE FROM	user_district
		WHERE				user_id = _user_id;
		
		DELETE FROM	user_region
		WHERE				user_id = _user_id;

		DELETE FROM user_height
		WHERE				user_id = _user_id;

		DELETE FROM user_weight
		WHERE				user_id = _user_id;
    
    DELETE FROM social_update_like
    WHERE				update_id IN
    (
      SELECT    id
      FROM      social_update
      WHERE			user_id = _user_id
    )
    OR          user_id = _user_id;
    
    DELETE FROM social_update_flag
    WHERE				update_id IN
    (
      SELECT    id
      FROM      social_update
      WHERE			user_id = _user_id
    )
    OR          user_id = _user_id;
    
    DELETE FROM social_update_video
    WHERE				update_id IN
    (
      SELECT    id
      FROM      social_update
      WHERE			user_id = _user_id
    );
    
    DELETE FROM social_update_image
    WHERE				update_id IN
    (
      SELECT    id
      FROM      social_update
      WHERE			user_id = _user_id
    );
    
    DELETE FROM social_update_to_social_update_tag
    WHERE				update_id IN
    (
      SELECT    id
      FROM      social_update
      WHERE			user_id = _user_id
    );
    
    DELETE FROM social_update_to_result
    WHERE				update_id IN
    (
      SELECT    id
      FROM      social_update
      WHERE			user_id = _user_id
    );
    
    DELETE FROM social_update
    WHERE				user_id = _user_id;

    
    
    
    
    DELETE FROM user_nomination
		WHERE				user_id = _user_id
		OR					nominee_user_id = _user_id;

		DELETE FROM result
		WHERE				user_id = _user_id;
		
		DELETE FROM user_routine_type
		WHERE				user_id = _user_id;
		
		DELETE FROM user_audit
		WHERE				user_id = _user_id;
		
		DELETE FROM user_notification
		WHERE				user_id = _user_id;
		
		DELETE FROM user_privacy_preferences
		WHERE				user_id = _user_id;
		
		DELETE FROM user_email_preference
		WHERE				user_id = _user_id;
		
		DELETE FROM	user_follower
		WHERE				user_id = _user_id
		OR					follower_user_id = _user_id;
		
		DELETE FROM	user_client
		WHERE				user_id = _user_id
		OR					client_user_id = _user_id;

		DELETE FROM "user"
		WHERE 			id = _user_id;

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_challenge_delete
(
	_challenge_id INT
)
RETURNS JSON AS
$$
	BEGIN
    DELETE FROM social_update_like
    WHERE				update_id IN
    (
      SELECT    u.update_id
      FROM      social_update_to_result AS u
      JOIN      result AS r on r.id = u.result_id
      WHERE			r.challenge_id = _challenge_id
    );
    
    DELETE FROM social_update_flag
    WHERE				update_id IN
    (
      SELECT    u.update_id
      FROM      social_update_to_result AS u
      JOIN      result AS r on r.id = u.result_id
      WHERE			r.challenge_id = _challenge_id
    );
    
    DELETE FROM social_update_video
    WHERE				update_id IN
    (
      SELECT    u.update_id
      FROM      social_update_to_result AS u
      JOIN      result AS r on r.id = u.result_id
      WHERE			r.challenge_id = _challenge_id
    );
    
    DELETE FROM social_update_image
    WHERE				update_id IN
    (
      SELECT    u.update_id
      FROM      social_update_to_result AS u
      JOIN      result AS r on r.id = u.result_id
      WHERE			r.challenge_id = _challenge_id
    );
    
    DELETE FROM social_update_to_social_update_tag
    WHERE				update_id IN
    (
      SELECT    u.update_id
      FROM      social_update_to_result AS u
      JOIN      result AS r on r.id = u.result_id
      WHERE			r.challenge_id = _challenge_id
    );
    
    DELETE FROM social_update_to_result
    WHERE				update_id IN
    (
      SELECT    u.update_id
      FROM      social_update_to_result AS u
      JOIN      result AS r on r.id = u.result_id
      WHERE			r.challenge_id = _challenge_id
    );
    
    DELETE FROM social_update
    WHERE				id IN
    (
      SELECT            su.id
      FROM              social_update AS su
      LEFT OUTER JOIN   social_update_like AS sul ON su.id = sul.update_id
      LEFT OUTER JOIN   social_update_flag AS suf ON su.id = suf.update_id
      LEFT OUTER JOIN   social_update_video AS suv ON su.id = suv.update_id
      LEFT OUTER JOIN   social_update_image AS sui ON su.id = sui.update_id
      LEFT OUTER JOIN   social_update_to_result AS sur ON su.id = sur.update_id
      WHERE             sul IS NULL
      AND               suf IS NULL
      AND               suv IS NULL
      AND               sui IS NULL
      AND               sur IS NULL
      AND               su.body IS NULL
    );
    
		DELETE FROM result WHERE challenge_id = _challenge_id;
		DELETE FROM challenge_to_challenge_group WHERE challenge_id = _challenge_id;
		DELETE FROM challenge WHERE id = _challenge_id;

		RETURN to_json(1);
	END
$$
LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS sp_gymfit_admin_challenge_save(INT, TEXT, INT, INT, INT[], TEXT, TEXT);