CREATE OR REPLACE FUNCTION sp_gymfit_site_result_get
(
	_result_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
		(
			SELECT					r.result,
											r.submitted,
											r.image_validated AS "imageValidated",
											r.video_validated AS "videoValidated",
											r.challenge_id AS "challengeId",
											r.challenge_group_id AS "challengeGroupId",
											r.user_id AS "userId",
											u.username,
											u.first_name AS "firstName",
											u.last_name AS "lastName",
											u.avatar_uploaded AS "avatarUploaded",
											c.name AS "challengeName",
											cc.value AS "category",
											ct.measurement AS "measurement"
			FROM						result AS r
			JOIN						"user" AS u ON u.id = r.user_id
			JOIN						challenge AS c on c.id = r.challenge_id
			JOIN 						challenge_category AS cc on c.category = cc.id
			JOIN 						challenge_type AS ct on c.type = ct.id
			WHERE						r.id = _result_id
			AND							r.excluded IS NULL
			LIMIT						1
		) AS result_to_return;
$$
LANGUAGE sql;
