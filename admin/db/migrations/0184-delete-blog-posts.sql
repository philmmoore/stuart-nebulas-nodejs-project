ALTER TABLE blog_post ADD COLUMN summary TEXT;
UPDATE blog_post SET summary=body_copy;
ALTER TABLE blog_post ALTER COLUMN summary SET NOT NULL;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_blog_get_post
(
	_post_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
		(
			SELECT	id,
							name,
							body_copy AS "bodyCopy",
							summary
			FROM		blog_post
			WHERE		id = _post_id
			LIMIT		1
		) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_blog_save_post
(
	_country_id INT,
	_post_id INT,
	_name TEXT,
	_body_copy TEXT,
	_summary TEXT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	blog_post
		SET			name = _name,
						body_copy = _body_copy,
						summary = _summary
		WHERE		id = _post_id;

		IF NOT FOUND THEN
			INSERT INTO blog_post (country, name, body_copy, summary)
			VALUES			(_country_id, _name, _body_copy, _summary)
			RETURNING		id
			INTO				_post_id;
		END IF;

		RETURN to_json(_post_id);
	END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_blog_get_posts
(
	_country_id INT,
	_posts_per_page INT,
	_page INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
		(
				SELECT	ceiling(count(*)::DECIMAL / _posts_per_page) AS "numberOfPages",
								(
									SELECT array_to_json(array_agg(result_b))
									FROM (
										SELECT		name,
															summary
										FROM			blog_post
										WHERE			country = _country_id
										ORDER BY	created DESC
										OFFSET		(_page - 1) * _posts_per_page
										LIMIT			_posts_per_page
									) AS result_b
							) AS "posts"
			FROM 		blog_post
	) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_blog_post_delete
(
	_id INT
)
RETURNS JSON AS
$$
	BEGIN
		DELETE FROM blog_post where id = _id;

		RETURN to_json(1);
	END
$$
LANGUAGE plpgsql;

