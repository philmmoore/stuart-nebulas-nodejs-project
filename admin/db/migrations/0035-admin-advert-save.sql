CREATE FUNCTION sp_admin_advert_save
(
	_countryId INT,
	_advertId INT,
	_name TEXT,
	_link TEXT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	advert
		SET			name = _name,
						link = _link
		WHERE		id = _advertId;

		IF NOT FOUND THEN
			INSERT INTO advert (country, name, link)
			VALUES			(_countryId, _name, _link)
			RETURNING		id
			INTO				_advertId;
		END IF;

		RETURN to_json(_advertId);
	END;
$$
LANGUAGE plpgsql;
