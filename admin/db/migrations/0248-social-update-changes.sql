DROP FUNCTION IF EXISTS sp_gymfit_site_result_like(BIGINT, INT);
DROP FUNCTION IF EXISTS sp_gymfit_site_result_unlike(BIGINT, INT);
DROP FUNCTION IF EXISTS sp_gymfit_site_result_flag(BIGINT, INT);
DROP FUNCTION IF EXISTS sp_gymfit_site_result_unflag(BIGINT, INT);

CREATE OR REPLACE FUNCTION sp_gymfit_site_social_update_like
(
  _user_id INT,
  _update_id UUID
)
RETURNS JSON AS
$$
  SELECT sp_gymfit_shared_social_update_like(_user_id, _update_id);
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_social_update_unlike
(
  _user_id INT,
  _update_id UUID
)
RETURNS JSON AS
$$
  SELECT sp_gymfit_shared_social_update_unlike(_user_id, _update_id);
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_social_update_flag
(
  _user_id INT,
  _update_id UUID
)
RETURNS JSON AS
$$
  SELECT sp_gymfit_shared_social_update_flag(_user_id, _update_id);
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_social_update_unflag
(
  _user_id INT,
  _update_id UUID
)
RETURNS JSON AS
$$
  SELECT sp_gymfit_shared_social_update_unflag(_user_id, _update_id);
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_social_get_user_updates
(
  _user_id INT,
  _from_timestamp TIMESTAMP,
  _number_of_updates INT
)
RETURNS JSON AS
$$
  SELECT array_to_json(array_agg(result_to_return))
  FROM
  (
    SELECT        ud.id,
                  ud.created,
                  ud.user_id AS "userId",
                  ud.body,
                  u.username,
                  u.first_name AS "firstName",
                  u.last_name AS "lastName",
                  u.avatar_uploaded AS "avatarUploaded",
                  (SELECT COUNT(*) FROM social_update_like WHERE update_id = ud.id) AS "likes",
                  (SELECT COUNT(*) FROM social_update_flag WHERE update_id = ud.id) AS "flags",
                  (
                      SELECT row_to_json(a) AS "result"
                      FROM
                      (
                              SELECT      r.id,
                                          r.result,
                                          r.submitted,
                                          c.id AS "challengeId",
                                          c.name AS "challengeName",
                                          cg.id AS "challengeGroupId",
                                          cg.value AS "challengeGroupName",
                                          cc.value AS "category",
                                          cc.id AS "categoryId",
                                          ct.measurement AS "measurement",
                                          r.image_validated AS "imageValidated",
                                          r.video_validated AS "videoValidated"
                              FROM    result AS r
                              JOIN    social_update_to_result AS ur ON ur.result_id = r.id
                              JOIN    challenge AS c ON r.challenge_id = c.id
                              JOIN    challenge_group AS cg ON r.challenge_group_id = cg.id
                              JOIN    challenge_type AS ct on c.type = ct.id
                              JOIN    challenge_category AS cc ON cc.id = c.category
                              WHERE   ur.update_id = ud.id
                              AND     r.excluded IS NULL
                      ) AS a
                  ),
                  array
                  (
                    SELECT			id
                    FROM				social_update_video
                    WHERE				update_id = ud.id
                    AND         processed IS NOT NULL
                  ) AS "videos",
                  array
                  (
                    SELECT			id
                    FROM				social_update_image
                    WHERE				update_id = ud.id
                    AND         processed IS NOT NULL
                  ) AS "images",
                  array
                  (
                    SELECT      t.value
                    FROM				social_update_to_social_update_tag AS ut
                    JOIN        social_update_tag AS t ON ut.tag_id = t.id
                    WHERE				ut.update_id = ud.id
                  ) AS "tags",
                  CASE WHEN (SELECT TRUE FROM social_update_like WHERE user_id = _user_id AND update_id = ud.id) = TRUE THEN TRUE ELSE FALSE END AS "userLikes"
    FROM          social_update AS ud
    JOIN          "user" AS u ON ud.user_id = u.id
    WHERE         ud.created < _from_timestamp
    AND           u.blocked IS NULL
    AND           ud.rejected IS NULL
    AND
    (
      ud.user_id = _user_id
      OR						ud.user_id IN
                    (
                      SELECT client_user_id FROM user_client WHERE user_id = _user_id
                      UNION
                      SELECT user_id FROM user_follower WHERE follower_user_id = _user_id
                    )
    )
    ORDER BY      ud.created DESC
    LIMIT         _number_of_updates
  ) AS result_to_return;
$$
LANGUAGE sql;

DROP FUNCTION IF EXISTS sp_gymfit_site_leaderboard_search(INT, INT, INT, INT, INT, INT, TIMESTAMP, TIMESTAMP, TIMESTAMP, TIMESTAMP, INT, INT, BIT, SMALLINT, SMALLINT, INT, INT, BIT);

CREATE OR REPLACE FUNCTION sp_gymfit_site_leaderboard_search
(
	_challenge_id INT,
	_challenge_group_id INT,
	_district_id INT,
	_county_id INT,
	_region_id INT,
	_country_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_last_period_start TIMESTAMP,
	_last_period_end TIMESTAMP,
	_results_per_page INT,
	_page INT,
	_is_male BIT,
	_age_min SMALLINT,
	_age_max SMALLINT,
	_weight_min INT,
	_weight_max INT,
	_only_validated BIT,
  _user_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		WITH filtered_results AS
		(
			SELECT				r.id,
										r.submitted,
										r.result,
										r.user_id,
										r.image_validated,
										r.video_validated,
										ct.measurement,
                    ct.min_first,
										u.username,
										u.avatar_uploaded,
										u.first_name,
										u.last_name,
                    sur.update_id
			FROM					result AS r
      JOIN          social_update_to_result AS sur ON sur.result_id = r.id
			JOIN					"user" AS u ON r.user_id = u.id
			JOIN 					challenge AS c ON c.id = r.challenge_id
			JOIN					challenge_type AS ct ON ct.id = c.type
			WHERE					r.submitted >= _last_period_start
			AND						r.submitted <= _this_period_end
			AND						r.challenge_id = _challenge_id
			AND						r.challenge_group_id = _challenge_group_id
			AND						(r.district_id = _district_id OR _district_id IS NULL)
			AND						(r.county_id = _county_id OR _county_id IS NULL)
			AND						(r.region_id = _region_id OR _region_id IS NULL)
			AND						(r.country_id = _country_id OR _country_id IS NULL)
			AND						(r.age >= _age_min OR _age_min IS NULL)
			AND						(r.age <= _age_max OR _age_max IS NULL)
			AND						(r.weight >= _weight_min OR _weight_min IS NULL)
			AND						(r.weight <= _weight_max OR _weight_max IS NULL)
			AND						((r.image_validated IS NOT NULL OR r.video_validated IS NOT NULL) OR _only_validated != '1')
			AND						(u.is_male = _is_male OR _is_male IS NULL)
			AND						r.excluded IS NULL
		)
		
		SELECT					(
											SELECT 				ceiling(COUNT(DISTINCT user_id)::DECIMAL / _results_per_page) AS "numberOfPages"
											FROM					filtered_results
											WHERE					submitted >= _this_period_start
										) AS "numberOfPages",
		
										(
											SELECT				measurement
											FROM					filtered_results
											LIMIT					1
										) AS "measurement",
										
										(
											SELECT array_to_json(array_agg(paged_results))
											FROM
											(
												
												
												
												
												WITH current_ranges AS
												(
													SELECT			row_number() over (ORDER BY MAX(frr.result) DESC) AS max_rank,
																			row_number() over (ORDER BY MIN(frr.result)) AS min_rank,
																			(
																				SELECT		id
																				FROM			filtered_results
																				WHERE 		result = MAX(frr.result)
																				AND				user_id = frr.user_id
																				ORDER BY 	video_validated DESC NULLS LAST,
																				 					image_validated DESC NULLS LAST,
																									submitted DESC
																				LIMIT 		1
																			) AS max_result_id,
																			(
																				SELECT 		id
																				FROM 			filtered_results
																				WHERE 		result = MIN(frr.result)
																				AND 			user_id = frr.user_id
																				ORDER BY 	video_validated DESC NULLS LAST,
																				 					image_validated DESC NULLS LAST,
																									submitted DESC
																				LIMIT 		1
																			) AS min_result_id,
																			frr.user_id
													FROM 				filtered_results AS frr
													WHERE				submitted >= _this_period_start
													GROUP BY		frr.user_id
												)
												
												SELECT				fr.id,
																			CASE
																				WHEN fr.min_first = TRUE THEN rg.min_rank
																				ELSE rg.max_rank
																			END AS "rank",
																			fr.result,
																			fr.image_validated AS "imageValidated",
																			fr.video_validated AS "videoValidated",
																			fr.user_id AS "userId",
																			fr.username,
																			fr.avatar_uploaded AS "userHasAvatar",
									                    fr.first_name "firstName",
									                    fr.last_name "lastName",
                                      fr.update_id AS "updateId",
																			(
																				SELECT      COUNT(*)
								                      	FROM				social_update_like
																				WHERE				update_id = fr.update_id
																			) AS "likes",
																			(
                                        SELECT      COUNT(*)
								                      	FROM				social_update_flag
																				WHERE				update_id = fr.update_id
																			) AS "flags",
                                      CASE WHEN (SELECT TRUE FROM social_update_like WHERE user_id = _user_id AND update_id = fr.update_id) = TRUE THEN TRUE ELSE FALSE END AS "userLikes",
																			(
																				
																				
																				WITH last_rank AS
																				(
																					SELECT			lfr.user_id,
																											CASE
																												WHEN fr.min_first = TRUE THEN
                                                          row_number() over (ORDER BY MIN(lfr.result))
																												ELSE
																													row_number() over (ORDER BY MAX(lfr.result) DESC)
																											END AS last_rank_result
																					FROM 				filtered_results AS lfr
																					WHERE				lfr.submitted <= _last_period_end
																					GROUP BY		lfr.user_id
																				)
																				SELECT	last_rank_result
																				FROM		last_rank
																				WHERE		user_id = fr.user_id
																				LIMIT 1
																				
																				
																			) AS "lastRank"
												FROM					current_ranges AS rg
												JOIN					filtered_results AS fr ON fr.id = CASE
																																					WHEN fr.min_first = TRUE THEN rg.min_result_id
																																					ELSE rg.max_result_id
																																				END
												ORDER BY 		  rank
												OFFSET				(_page - 1) * _results_per_page
												LIMIT					_results_per_page
																															
																															
																															
                      ) AS paged_results
										) AS "results"
		
		FROM filtered_results
	) AS result_to_return;
$$
LANGUAGE sql;

DROP FUNCTION IF EXISTS sp_gymfit_site_result_get(INT);

CREATE OR REPLACE FUNCTION sp_gymfit_site_result_get
(
	_result_id INT,
  _user_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
		(
			SELECT					r.result,
											r.submitted,
											r.image_validated AS "imageValidated",
											r.video_validated AS "videoValidated",
											r.challenge_id AS "challengeId",
											r.challenge_group_id AS "challengeGroupId",
											r.user_id AS "userId",
											u.username,
											u.first_name AS "firstName",
											u.last_name AS "lastName",
											u.avatar_uploaded AS "avatarUploaded",
											c.name AS "challengeName",
											cc.value AS "category",
											ct.measurement AS "measurement",
											cg.value AS "group",
											r.submitted,
                      sur.update_id AS "updateId",
                      CASE WHEN (SELECT TRUE FROM social_update_like WHERE user_id = _user_id AND update_id = sur.update_id) = TRUE THEN TRUE ELSE FALSE END AS "userLikes",
                      (
                        SELECT      COUNT(*)
                        FROM				social_update_like
                        WHERE				update_id = sur.update_id
                      ) AS "likes",
                      (
                        SELECT      COUNT(*)
                        FROM				social_update_flag
                        WHERE				update_id = sur.update_id
                      ) AS "flags"
			FROM						result AS r
      JOIN            social_update_to_result AS sur ON r.id = sur.result_id
			JOIN						"user" AS u ON u.id = r.user_id
			JOIN						challenge AS c on c.id = r.challenge_id
			JOIN 						challenge_category AS cc on c.category = cc.id
			JOIN 						challenge_type AS ct on c.type = ct.id
			JOIN 						challenge_group AS cg on r.challenge_group_id = cg.id
			WHERE						r.id = _result_id
			AND							r.excluded IS NULL
			LIMIT						1
		) AS result_to_return;
$$
LANGUAGE sql;

DROP FUNCTION sp_gymfit_api_social_get_global_updates(TIMESTAMP, INT);

CREATE OR REPLACE FUNCTION sp_gymfit_api_social_get_global_updates
(
  _user_id INT,
  _from_timestamp TIMESTAMP,
  _number_of_updates INT
)
RETURNS JSON AS
$$
  SELECT array_to_json(array_agg(result_to_return))
  FROM
  (
    SELECT        ud.id,
                  ud.created,
                  ud.user_id AS "userId",
                  ud.body,
                  u.username,
                  u.first_name AS "firstName",
                  u.last_name AS "lastName",
                  u.avatar_uploaded AS "avatarUploaded",
                  (SELECT COUNT(*) FROM social_update_like WHERE update_id = ud.id) AS "likes",
                  (SELECT COUNT(*) FROM social_update_flag WHERE update_id = ud.id) AS "flags",
                  (
                      SELECT row_to_json(a) AS "result"
                      FROM
                      (
                              SELECT      r.id,
                                          r.result,
                                          r.submitted,
                                          c.id AS "challengeId",
                                          c.name AS "challengeName",
                                          cg.id AS "challengeGroupId",
                                          cg.value AS "challengeGroupName",
                                          cc.value AS "category",
                                          cc.id AS "categoryId",
                                          ct.measurement AS "measurement",
                                          r.image_validated AS "imageValidated",
                                          r.video_validated AS "videoValidated"
                              FROM    result AS r
                              JOIN    social_update_to_result AS ur ON ur.result_id = r.id
                              JOIN    challenge AS c ON r.challenge_id = c.id
                              JOIN    challenge_group AS cg ON r.challenge_group_id = cg.id
                              JOIN    challenge_type AS ct on c.type = ct.id
                              JOIN    challenge_category AS cc ON cc.id = c.category
                              WHERE   ur.update_id = ud.id
                              AND     r.excluded IS NULL
                      ) AS a
                  ),
                  array
                  (
                    SELECT			id
                    FROM				social_update_video
                    WHERE				update_id = ud.id
                    AND         processed IS NOT NULL
                  ) AS "videos",
                  array
                  (
                    SELECT			id
                    FROM				social_update_image
                    WHERE				update_id = ud.id
                    AND         processed IS NOT NULL
                  ) AS "images",
                  array
                  (
                    SELECT      t.value
                    FROM				social_update_to_social_update_tag AS ut
                    JOIN        social_update_tag AS t ON ut.tag_id = t.id
                    WHERE				ut.update_id = ud.id
                  ) AS "tags",
                  CASE WHEN (SELECT TRUE FROM social_update_like WHERE user_id = _user_id AND update_id = ud.id) = TRUE THEN TRUE ELSE FALSE END AS "userLikes"
    FROM          social_update AS ud
    JOIN          "user" AS u ON ud.user_id = u.id
    WHERE         ud.created < _from_timestamp
    AND           u.blocked IS NULL
    AND           ud.rejected IS NULL
    ORDER BY      ud.created DESC
    LIMIT         _number_of_updates
  ) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_social_get_user_updates
(
  _user_id INT,
  _from_timestamp TIMESTAMP,
  _number_of_updates INT
)
RETURNS JSON AS
$$
  SELECT array_to_json(array_agg(result_to_return))
  FROM
  (
    SELECT        ud.id,
                  ud.created,
                  ud.user_id AS "userId",
                  ud.body,
                  u.username,
                  u.first_name AS "firstName",
                  u.last_name AS "lastName",
                  u.avatar_uploaded AS "avatarUploaded",
                  (SELECT COUNT(*) FROM social_update_like WHERE update_id = ud.id) AS "likes",
                  (SELECT COUNT(*) FROM social_update_flag WHERE update_id = ud.id) AS "flags",
                  (
                      SELECT row_to_json(a) AS "result"
                      FROM
                      (
                              SELECT      r.id,
                                          r.result,
                                          r.submitted,
                                          c.id AS "challengeId",
                                          c.name AS "challengeName",
                                          cg.id AS "challengeGroupId",
                                          cg.value AS "challengeGroupName",
                                          cc.value AS "category",
                                          cc.id AS "categoryId",
                                          ct.measurement AS "measurement",
                                          r.image_validated AS "imageValidated",
                                          r.video_validated AS "videoValidated"
                              FROM    result AS r
                              JOIN    social_update_to_result AS ur ON ur.result_id = r.id
                              JOIN    challenge AS c ON r.challenge_id = c.id
                              JOIN    challenge_group AS cg ON r.challenge_group_id = cg.id
                              JOIN    challenge_type AS ct on c.type = ct.id
                              JOIN    challenge_category AS cc ON cc.id = c.category
                              WHERE   ur.update_id = ud.id
                              AND     r.excluded IS NULL
                      ) AS a
                  ),
                  array
                  (
                    SELECT			id
                    FROM				social_update_video
                    WHERE				update_id = ud.id
                    AND         processed IS NOT NULL
                  ) AS "videos",
                  array
                  (
                    SELECT			id
                    FROM				social_update_image
                    WHERE				update_id = ud.id
                    AND         processed IS NOT NULL
                  ) AS "images",
                  array
                  (
                    SELECT      t.value
                    FROM				social_update_to_social_update_tag AS ut
                    JOIN        social_update_tag AS t ON ut.tag_id = t.id
                    WHERE				ut.update_id = ud.id
                  ) AS "tags",
                  CASE WHEN (SELECT TRUE FROM social_update_like WHERE user_id = _user_id AND update_id = ud.id) = TRUE THEN TRUE ELSE FALSE END AS "userLikes"
    FROM          social_update AS ud
    JOIN          "user" AS u ON ud.user_id = u.id
    WHERE         ud.created < _from_timestamp
    AND           u.blocked IS NULL
    AND           ud.rejected IS NULL
    AND
    (
      ud.user_id = _user_id
      OR						ud.user_id IN
                    (
                      SELECT client_user_id FROM user_client WHERE user_id = _user_id
                      UNION
                      SELECT user_id FROM user_follower WHERE follower_user_id = _user_id
                    )
    )
    ORDER BY      ud.created DESC
    LIMIT         _number_of_updates
  ) AS result_to_return;
$$
LANGUAGE sql;