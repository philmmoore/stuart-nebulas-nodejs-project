CREATE OR REPLACE FUNCTION sp_gymfit_site_auth_confirm_email
(
	_key UUID
)
RETURNS JSON AS
$$
	DECLARE
		_user_id INT;
	BEGIN
		DELETE
		FROM				user_email_verification AS ev
		USING				"user" AS u
		WHERE				ev.user_id = u.id
		AND					ev.key = _key
		RETURNING		u.id
		INTO				_user_id;

		UPDATE			user_notification
		SET					acknowledged = current_timestamp
		WHERE				user_id = _user_id
		AND					notification_id = 2;

		RETURN sp_gymfit_site_user_get(_user_id);
	END
$$
LANGUAGE plpgsql;