CREATE OR REPLACE FUNCTION sp_gymfit_admin_home_result_coverage()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
	(
		SELECT
			c.name AS "challenge",
			cg.value AS "challengeGroup",
			(
				SELECT array_to_json(array_agg(district))
				FROM
				(
					SELECT					d.name,
													COUNT(DISTINCT r.user_id) AS "count"
					FROM						district AS d
					LEFT OUTER JOIN	user_district AS ud
					ON							d.id = ud.district_id
					LEFT OUTER JOIN	result AS r
					ON							r.user_id = ud.user_id
					AND							r.challenge_id = c.id
					AND							r.challenge_group_id = cg.id
					GROUP BY				d.name
					ORDER BY				d.name
				) AS district
			) AS "results"
			FROM						challenge AS c
			LEFT OUTER JOIN	challenge_to_challenge_group AS ctg
			ON							ctg.challenge_id = c.id
			LEFT OUTER JOIN	challenge_group AS cg
			ON							cg.id = ctg.challenge_group_id
			ORDER BY				c.name,
											cg.value
		) AS result_to_return;
$$
LANGUAGE sql;
