CREATE OR REPLACE FUNCTION sp_gymfit_site_search_location(_search_query text, _district_id integer, _county_id integer, _region_id integer, _country_id integer, _results_per_page integer, _page integer)
 RETURNS json
 LANGUAGE sql
AS $function$
    SELECT row_to_json(result_to_return)
    FROM
    (
        SELECT
        (
            SELECT array_to_json(array_agg(results))
            FROM
            (
                SELECT                  u.id,
                                                u.username,
                                                u.first_name AS "firstName",
                                                u.last_name AS "lastName",
                                                u.avatar_uploaded AS "avatarUploaded"
                FROM                        "user" AS u
                LEFT OUTER JOIN district AS d ON d.id = (SELECT district_id FROM user_district WHERE user_id = u.id ORDER BY CREATED DESC LIMIT 1)
                LEFT OUTER JOIN region AS r ON r.id = (SELECT region_id FROM user_region WHERE user_id = u.id ORDER BY CREATED DESC LIMIT 1)
                LEFT OUTER JOIN county AS c ON c.id = d.county_id
                LEFT OUTER JOIN country AS ct ON ct.id = r.country_id
                WHERE                       (d.id = _district_id OR _district_id IS NULL)
                AND                         (c.id = _county_id OR _county_id IS NULL)
                AND                         (r.id = _region_id OR _region_id IS NULL)
                AND                         (ct.id = _country_id OR _country_id IS NULL)
                AND                         (
                                                    LOWER(u.username) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
                                                    OR LOWER(u.first_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
                                                    OR LOWER(u.last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
                                                    OR LOWER(u.first_name || u.last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
                                                    OR _search_query IS NULL
                                                )
        ORDER BY        LOWER(first_name)
                OFFSET                  (_page - 1) * _results_per_page
                LIMIT                       _results_per_page
            ) AS results
        ) AS "results",
        (
            SELECT                  ceiling(COUNT(u.id)::DECIMAL / _results_per_page)
            FROM                        "user" AS u
            LEFT OUTER JOIN district AS d ON d.id = (SELECT district_id FROM user_district WHERE user_id = u.id ORDER BY CREATED DESC LIMIT 1)
            LEFT OUTER JOIN region AS r ON r.id = (SELECT region_id FROM user_region WHERE user_id = u.id ORDER BY CREATED DESC LIMIT 1)
            LEFT OUTER JOIN county AS c ON c.id = d.county_id
            LEFT OUTER JOIN country AS ct ON ct.id = r.country_id
            WHERE                       (d.id = _district_id OR _district_id IS NULL)
            AND                         (c.id = _county_id OR _county_id IS NULL)
            AND                         (r.id = _region_id OR _region_id IS NULL)
            AND                         (ct.id = _country_id OR _country_id IS NULL)
            AND                         (
                                                LOWER(u.username) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
                                                OR LOWER(u.first_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
                                                OR LOWER(u.last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
                                                OR LOWER(u.first_name || u.last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
                                                OR _search_query IS NULL
                                            )
        ) AS "numberOfPages",
        (
            SELECT                  count(1)
            FROM                        "user" AS u
            LEFT OUTER JOIN district AS d ON d.id = (SELECT district_id FROM user_district WHERE user_id = u.id ORDER BY CREATED DESC LIMIT 1)
            LEFT OUTER JOIN region AS r ON r.id = (SELECT region_id FROM user_region WHERE user_id = u.id ORDER BY CREATED DESC LIMIT 1)
            LEFT OUTER JOIN county AS c ON c.id = d.county_id
            LEFT OUTER JOIN country AS ct ON ct.id = r.country_id
            WHERE                       (d.id = _district_id OR _district_id IS NULL)
            AND                         (c.id = _county_id OR _county_id IS NULL)
            AND                         (r.id = _region_id OR _region_id IS NULL)
            AND                         (ct.id = _country_id OR _country_id IS NULL)
            AND                         (
                                                LOWER(u.username) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
                                                OR LOWER(u.first_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
                                                OR LOWER(u.last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
                                                OR LOWER(u.first_name || u.last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
                                                OR _search_query IS NULL
                                            )
        ) AS "totalResults"
    ) AS result_to_return;
$function$
