ALTER TABLE challenge DROP country;
ALTER TABLE challenge RENAME bodycopy TO body_copy;
ALTER TABLE challenge RENAME submissionmessage TO submission_message;
ALTER TABLE challenge_to_challenge_group RENAME challengeGroup TO challenge_group_id;
ALTER TABLE challenge_to_challenge_group RENAME challenge TO challenge_id;
