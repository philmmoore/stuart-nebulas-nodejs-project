DROP FUNCTION IF EXISTS sp_gymfit_site_social_get_user_updates();
CREATE OR REPLACE FUNCTION sp_gymfit_site_social_get_user_updates(_user_id integer, _from_timestamp timestamp without time zone, _number_of_updates integer, _show_only_mine boolean, _ignore_mine boolean)
 RETURNS json
 LANGUAGE sql
AS $function$
  SELECT array_to_json(array_agg(result_to_return))
  FROM
  (
    SELECT        ud.id,
                  ud.created,
                  ud.user_id AS "userId",
                  ud.body,
                  u.username,
                  u.first_name AS "firstName",
                  u.last_name AS "lastName",
                  u.is_personal_trainer AS "isPersonalTrainer",
                  u.is_gym AS "isGym",
                  u.avatar_uploaded AS "avatarUploaded",
                  u.bio,
                  (SELECT COUNT(*) FROM social_update_like WHERE update_id = ud.id) AS "likes",
                  (SELECT COUNT(*) FROM social_update_flag WHERE update_id = ud.id) AS "flags",
                  (
                      SELECT row_to_json(a) AS "result"
                      FROM
                      (
                              SELECT      r.id,
                                          r.result,
                                          r.submitted,
                                          c.id AS "challengeId",
                                          c.name AS "challengeName",
                                          cg.id AS "challengeGroupId",
                                          cg.value AS "challengeGroupName",
                                          cc.value AS "category",
                                          cc.id AS "categoryId",
                                          ct.measurement AS "measurement",
                                          r.image_validated AS "imageValidated",
                                          r.video_validated AS "videoValidated"
                              FROM    result AS r
                              JOIN    social_update_to_result AS ur ON ur.result_id = r.id
                              JOIN    challenge AS c ON r.challenge_id = c.id
                              JOIN    challenge_group AS cg ON r.challenge_group_id = cg.id
                              JOIN    challenge_type AS ct on c.type = ct.id
                              JOIN    challenge_category AS cc ON cc.id = c.category
                              WHERE   ur.update_id = ud.id
                              AND     r.excluded IS NULL
                      ) AS a
                  ),
                  array
                  (
                    SELECT          id
                    FROM                social_update_video
                    WHERE               update_id = ud.id
                    AND         processed IS NOT NULL
                  ) AS "videos",
                  array
                  (
                    SELECT          id
                    FROM                social_update_image
                    WHERE               update_id = ud.id
                    AND         processed IS NOT NULL
                  ) AS "images",
                  array
                  (
                    SELECT      t.value
                    FROM                social_update_to_social_update_tag AS ut
                    JOIN        social_update_tag AS t ON ut.tag_id = t.id
                    WHERE               ut.update_id = ud.id
                  ) AS "tags",
                  CASE WHEN (SELECT TRUE FROM social_update_like WHERE user_id = _user_id AND update_id = ud.id) = TRUE THEN TRUE ELSE FALSE END AS "userLikes",
                  CASE WHEN (SELECT TRUE FROM social_update_flag WHERE user_id = _user_id AND update_id = ud.id) = TRUE THEN TRUE ELSE FALSE END AS "userFlagged"
    FROM          social_update AS ud
    JOIN          "user" AS u ON ud.user_id = u.id
    WHERE         ud.created < _from_timestamp
    AND           u.blocked IS NULL
    AND           ud.rejected IS NULL
    AND
    (
      (_ignore_mine = FALSE AND ud.user_id = _user_id)
      OR            (_show_only_mine = FALSE AND ud.user_id IN
                    (
                      SELECT client_user_id FROM user_client WHERE user_id = _user_id
                      UNION
                      SELECT user_id FROM user_follower WHERE follower_user_id = _user_id
                    ))
    )
    ORDER BY      ud.created DESC
    LIMIT         _number_of_updates
  ) AS result_to_return;
$function$;

DROP FUNCTION IF EXISTS sp_gymfit_api_social_get_user_updates();
CREATE OR REPLACE FUNCTION sp_gymfit_api_social_get_user_updates(_user_id integer, _from_timestamp timestamp without time zone, _number_of_updates integer, _show_only_mine boolean, _ignore_mine boolean)
 RETURNS json
 LANGUAGE sql
AS $function$
    SELECT sp_gymfit_site_social_get_user_updates(_user_id, _from_timestamp, _number_of_updates, _show_only_mine, _ignore_mine);
$function$;



