ALTER TABLE email_template RENAME COLUMN template TO standard_template;
ALTER TABLE email_template ADD COLUMN email_confirmation_subject TEXT;
ALTER TABLE email_template ADD COLUMN email_confirmation_html_body TEXT;
ALTER TABLE email_template ADD COLUMN email_confirmation_plain_body TEXT;
ALTER TABLE email_template ADD COLUMN password_reset_subject TEXT;
ALTER TABLE email_template ADD COLUMN password_reset_html_body TEXT;
ALTER TABLE email_template ADD COLUMN password_reset_plain_body TEXT;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_email_template_get
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)	FROM (
		SELECT	standard_template AS "standardTemplate",
						email_confirmation_subject AS "emailConfirmationSubject",
						email_confirmation_html_body AS "emailConfirmationHtmlBody",
						email_confirmation_plain_body AS "emailConfirmationPlainBody",
						password_reset_subject AS "passwordResetSubject",
						password_reset_html_body AS "passwordResetHtmlBody",
						password_reset_plain_body AS "passwordResetPlainBody"
		FROM		email_template
		WHERE		country_id = _country_id
		LIMIT		1
	) AS result;
$$
LANGUAGE sql;

DROP FUNCTION sp_gymfit_admin_email_template_set(INT, TEXT);

CREATE FUNCTION sp_gymfit_admin_email_template_set
(
	_country_id INT,
	_standardTemplate TEXT,
	_emailConfirmationSubject TEXT,
	_emailConfirmationHtmlBody TEXT,
	_emailConfirmationPlainBody TEXT,
	_passwordResetSubject TEXT,
	_passwordResetHtmlBody TEXT,
	_passwordResetPlainBody TEXT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	email_template
		SET			standard_template = _standardTemplate,
						email_confirmation_subject = _emailConfirmationSubject,
						email_confirmation_html_body = _emailConfirmationHtmlBody,
						email_confirmation_plain_body = _emailConfirmationPlainBody,
						password_reset_subject = _passwordResetSubject,
						password_reset_html_body = _passwordResetHtmlBody,
						password_reset_plain_body = _passwordResetPlainBody
		WHERE		country_id = _country_id;

		RETURN to_json(1);
	END
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_task_runner_email_template_get
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)	FROM (
		SELECT	standard_template AS "standardTemplate",
						email_confirmation_subject AS "emailConfirmationSubject",
						email_confirmation_html_body AS "emailConfirmationHtmlBody",
						email_confirmation_plain_body AS "emailConfirmationPlainBody",
						password_reset_subject AS "passwordResetSubject",
						password_reset_html_body AS "passwordResetHtmlBody",
						password_reset_plain_body AS "passwordResetPlainBody"
		FROM		email_template
		WHERE		country_id = _country_id
		LIMIT		1
	) AS result;
$$
LANGUAGE sql;

UPDATE email_template SET	email_confirmation_subject = 'Welcome to GymFit!',
													email_confirmation_html_body = 'Please verify your email address by clicking on the link below: {{EMAIL_CONFIRMATION_LINK}}',
													email_confirmation_plain_body = 'Please verify your email address by clicking on the link below: {{EMAIL_CONFIRMATION_LINK}}',
													password_reset_subject = 'GymFit Password Reset',
													password_reset_html_body = 'Somebody has requested help logging into your GymFit account. If this was you you can reset your password by going to: {{PASSWORD_RESET_LINK}}',
													password_reset_plain_body = 'Somebody has requested help logging into your GymFit account. If this was you you can reset your password by going to: {{PASSWORD_RESET_LINK}}';

ALTER TABLE email_template ALTER email_confirmation_subject SET NOT NULL;
ALTER TABLE email_template ALTER email_confirmation_html_body SET NOT NULL;
ALTER TABLE email_template ALTER email_confirmation_plain_body SET NOT NULL;
ALTER TABLE email_template ALTER password_reset_subject SET NOT NULL;
ALTER TABLE email_template ALTER password_reset_html_body SET NOT NULL;
ALTER TABLE email_template ALTER password_reset_plain_body SET NOT NULL;