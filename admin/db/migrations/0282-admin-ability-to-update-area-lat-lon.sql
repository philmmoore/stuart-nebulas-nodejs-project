DO $$ 
    BEGIN
                
        CREATE OR REPLACE FUNCTION sp_gymfit_admin_area_get_all_counties(_region_id integer)
         RETURNS json
         LANGUAGE sql
        AS $function$
            SELECT row_to_json(result)
            FROM
                (
                    SELECT  r.id,
                                    r.name,
                                    r.latitude,
                                    r.longitude,
                                    (
                                        SELECT array_to_json(array_agg(result_b))
                                        FROM
                                            (
                                                SELECT          c.id,
                                                                        c.name,
                                                                        (
                                                                            array
                                                                            (
                                                                                SELECT          d.name
                                                                                FROM                district AS d
                                                                                WHERE               d.county_id = c.id
                                                                                ORDER BY        d.name
                                                                            ) 
                                                                        ) AS districts
                                                FROM                county as c
                                                WHERE               c.region_id = r.id
                                                ORDER BY        c.name
                                            ) AS result_b
                                ) AS "counties"
                FROM        region AS r
                WHERE       r.id = _region_id
            ) AS result
        $function$;

    END;
$$;

DO $$
    BEGIN

    DROP FUNCTION IF EXISTS sp_gymfit_admin_area_save_region();
    CREATE OR REPLACE FUNCTION sp_gymfit_admin_area_save_region(_country_id integer, _region_id integer, _name text, _latitude numeric, _longitude numeric)
     RETURNS json
     LANGUAGE plpgsql
    AS $function$
        BEGIN
            UPDATE  region
            SET         name = _name,
                        latitude = _latitude, 
                        longitude = _longitude
            WHERE       id = _region_id;

            IF NOT FOUND THEN
                INSERT INTO region (country_id, name, latitude, longitude)
                VALUES          (_country_id, _name, _latitude, _longitude)
                RETURNING       id
                INTO                _region_id;
            END IF;

            RETURN to_json(_region_id);
        END;
    $function$;

    END;
$$;

DO $$
    BEGIN

        CREATE OR REPLACE FUNCTION sp_gymfit_admin_area_get_district(_district_id integer)
         RETURNS json
         LANGUAGE sql
        AS $function$
            SELECT row_to_json(result)
            FROM
                (
                    SELECT  d.id,
                            d.name,
                            d.latitude,
                            d.longitude
                    FROM        district AS d
                    WHERE       d.id = _district_id
            ) AS result
        $function$;

        DROP FUNCTION IF EXISTS sp_gymfit_admin_area_save_district();
        CREATE OR REPLACE FUNCTION sp_gymfit_admin_area_save_district(_county_id integer, _district_id integer, _name text, _latitude numeric, _longitude numeric)
         RETURNS json
         LANGUAGE plpgsql
        AS $function$
            BEGIN
                UPDATE  district
                SET         name = _name,
                            latitude = _latitude,
                            longitude = _longitude
                WHERE       id = _district_id;

                IF NOT FOUND THEN
                    INSERT INTO district (county_id, name, latitude, longitude)
                    VALUES          (_county_id, _name, _latitude, _longitude)
                    RETURNING       id
                    INTO                _district_id;
                END IF;

                RETURN to_json(_district_id);
            END;
        $function$;

    END;
$$;

DO $$
    BEGIN

        CREATE OR REPLACE FUNCTION sp_gymfit_admin_area_get_all_districts(_county_id integer)
         RETURNS json
         LANGUAGE sql
        AS $function$
            SELECT row_to_json(result)
            FROM
                (
                    SELECT  c.id,
                                    c.name,
                                    (
                                        SELECT array_to_json(array_agg(result_b))
                                        FROM
                                            (
                                                SELECT          d.id,
                                                                        d.name,
                        d.latitude,
                        d.longitude
                                                FROM                district as d
                                                WHERE               d.county_id = c.id
                                                ORDER BY        d.name
                                            ) AS result_b
                                ) AS "districts"
                FROM        county AS c
                WHERE       c.id = _county_id
            ) AS result
        $function$;

    END;
$$;

DO $$
    BEGIN

        CREATE OR REPLACE FUNCTION sp_gymfit_admin_area_get_all_regions(_country_id integer)
         RETURNS json
         LANGUAGE sql
        AS $function$
            SELECT array_to_json(array_agg(result))
            FROM
                (
                    SELECT          r.id,
                                            r.name, 
                                            r.latitude,
                                            r.longitude,
                                            (
                                                array
                                                (
                                                    SELECT          c.name
                                                    FROM                county AS c
                                                    WHERE               c.region_id = r.id
                                                    ORDER BY        c.name
                                                )
                                            ) AS counties
                    FROM                region as r
                    WHERE               r.country_id = 1
                    ORDER BY        r.name
                ) AS result;
        $function$;

    END;
$$;
