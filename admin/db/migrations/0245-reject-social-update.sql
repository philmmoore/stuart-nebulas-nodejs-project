CREATE OR REPLACE FUNCTION sp_gymfit_admin_social_update_reject
(
  _id UUID,
  _reason TEXT
)
RETURNS JSON AS
$$
  UPDATE  social_update
  SET     rejected = current_timestamp,
          rejected_reason = _reason
  WHERE   id = _id;

	SELECT row_to_json(result_to_return)
	FROM
	(
    SELECT		      u.created,
                    us.email,
                    _reason AS "reason",
                    array
                    (
                      SELECT			id
                      FROM				social_update_video
                      WHERE				update_id = u.id
                      AND         processed IS NOT NULL
                    ) AS "videos",
                    array
                    (
                      SELECT			id
                      FROM				social_update_image
                      WHERE				update_id = u.id
                      AND         processed IS NOT NULL
                    ) AS "images"
    FROM		        social_update AS u
    JOIN						"user" AS us on us.id = u.user_id
    WHERE           u.id = _id
	) AS result_to_return;
$$
LANGUAGE sql;