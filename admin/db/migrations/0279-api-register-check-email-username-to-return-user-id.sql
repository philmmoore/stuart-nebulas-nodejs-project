CREATE OR REPLACE FUNCTION sp_gymfit_api_register_is_email_registered(_email text)
  RETURNS json AS
$BODY$
  SELECT array_to_json(array_agg(result_to_return))
  FROM (
    SELECT    TRUE as result, id AS "userId"
    FROM      "user"
    WHERE     email = LOWER(_email)
  ) AS result_to_return;
$BODY$
  LANGUAGE sql VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION sp_gymfit_api_register_is_username_registered(_username text)
  RETURNS json AS
$BODY$
  SELECT array_to_json(array_agg(result_to_return))
  FROM (
    SELECT    TRUE as result, id AS "userId"
    FROM      "user"
    WHERE     username = LOWER(_username)
  ) AS result_to_return;
$BODY$
  LANGUAGE sql VOLATILE
  COST 100;