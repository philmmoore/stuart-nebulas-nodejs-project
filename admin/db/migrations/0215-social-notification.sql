DROP FUNCTION sp_gymfit_site_social_get_notifications(INT);
DROP FUNCTION sp_gymfit_api_social_get_notifications(INT);

CREATE OR REPLACE FUNCTION sp_gymfit_site_social_get_notifications
(
	_user_id INT,
	_since TIMESTAMP
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM (
		SELECT		      n.id,
										n.created,
										n.user_id AS "userId",
										n.notification,
										u.username,
										u.first_name AS "firstName",
										u.last_name AS "lastName",
										u.avatar_uploaded AS "avatarUploaded"
		FROM			      social_notification AS n
		JOIN						"user" AS u ON n.user_id = u.id
		WHERE						n.created > _since
		AND							(n.user_id = _user_id
		OR							n.user_id IN
			(
				SELECT client_user_id FROM user_client WHERE user_id = _user_id
				UNION
				SELECT user_id FROM user_follower WHERE follower_user_id = _user_id
			))
		AND							u.blocked IS NULL
		ORDER BY 	      n.created DESC
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_social_get_notifications
(
	_user_id INT,
	_since TIMESTAMP
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM (
		SELECT		      n.id,
										n.created,
										n.user_id AS "userId",
										n.notification,
										u.username,
										u.first_name AS "firstName",
										u.last_name AS "lastName",
										u.avatar_uploaded AS "avatarUploaded"
		FROM			      social_notification AS n
		JOIN						"user" AS u ON n.user_id = u.id
		WHERE						n.created > _since
		AND							(n.user_id = _user_id
		OR							n.user_id IN
			(
				SELECT client_user_id FROM user_client WHERE user_id = _user_id
				UNION
				SELECT user_id FROM user_follower WHERE follower_user_id = _user_id
			))
		AND							u.blocked IS NULL
		ORDER BY 	      n.created DESC
) AS result_to_return;
$$
LANGUAGE sql;

TRUNCATE social_notification;

INSERT INTO social_notification
(
	user_id,
	created,
	notification
)
SELECT 
		x.user_id,
		x.submitted,
		(SELECT row_to_json(social_notification)
		FROM
		(
			SELECT					'result'::TEXT AS "type",
											r.id AS "resultId",
											r.result,
											u.id AS "userId",
											u.username,
											u.first_name AS "firstName",
											u.last_name AS "lastName",
											u.avatar_uploaded AS "avatarUploaded",
											c.id AS "challengeId",
											c.name AS "challengeName",
											cg.id AS "challengeGroupId",
											cg.value AS "challengeGroupName",
											cc.value AS "challengeCategory",
											cc.id AS "challengeCategoryId",
											ct.measurement AS "measurement"
			FROM						result AS r
			JOIN						"user" AS u ON u.id = r.user_id
			JOIN						challenge AS c on c.id = r.challenge_id
			JOIN 						challenge_group AS cg ON r.challenge_group_id = cg.id
			JOIN 						challenge_type AS ct on c.type = ct.id
			JOIN 						challenge_category AS cc ON cc.id = c.category
			WHERE						r.id = x.id
		) AS social_notification)
	FROM result AS x
	WHERE excluded IS NULL;

INSERT INTO social_notification
(
	user_id,
	created,
	notification
)
SELECT 
		x.user_id,
		CASE WHEN x.image_validated IS NULL THEN x.video_validated ELSE x.image_validated END,
		(SELECT row_to_json(social_notification)
		FROM
		(
			SELECT					'result-evidence'::TEXT AS "type",
											r.id AS "resultId",
											r.result,
											u.id AS "userId",
											u.username,
											u.first_name AS "firstName",
											u.last_name AS "lastName",
											u.avatar_uploaded AS "avatarUploaded",
											c.id AS "challengeId",
											c.name AS "challengeName",
											cg.id AS "challengeGroupId",
											cg.value AS "challengeGroupName",
											cc.value AS "challengeCategory",
											cc.id AS "challengeCategoryId",
											ct.measurement AS "measurement",
											r.image_validated AS "imageValidated",
											r.video_validated AS "videoValidated"
			FROM						result AS r
			JOIN						"user" AS u ON u.id = r.user_id
			JOIN						challenge AS c on c.id = r.challenge_id
			JOIN 						challenge_group AS cg ON r.challenge_group_id = cg.id
			JOIN 						challenge_type AS ct on c.type = ct.id
			JOIN 						challenge_category AS cc ON cc.id = c.category

			WHERE						r.id = x.id
		) AS social_notification)
	FROM result AS x
	WHERE excluded IS NULL
	AND (image_validated IS NOT NULL OR video_validated IS NOT NULL);