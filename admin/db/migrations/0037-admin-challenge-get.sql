CREATE FUNCTION sp_admin_challenge_get_challenge
(
	_challenge_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
		(
			SELECT	id,
							name,
							category - 1 AS category,
							type - 1 AS type,
							body_copy AS "bodyCopy",
							submission_message AS "submissionMessage",
							(
								array
								(
									SELECT			cg.value
									FROM				challenge_group AS cg
									INNER JOIN	challenge_to_challenge_group AS ccg
									ON 					ccg.challenge_id = c.id
									AND					ccg.challenge_group_id = cg.id
									ORDER BY		cg.id
								)
							) AS groups
      FROM 		challenge AS c
      WHERE 	c.id = _challenge_id
			LIMIT		1
		) AS result;
$$
LANGUAGE sql;
