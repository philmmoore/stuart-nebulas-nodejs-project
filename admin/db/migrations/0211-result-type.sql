ALTER TABLE result ADD COLUMN type TEXT;

DROP FUNCTION sp_gymfit_site_result_submit(INT, INT, INT, INT);

CREATE OR REPLACE FUNCTION sp_gymfit_site_result_submit
(
	_user_id INT,
	_challenge_id INT,
	_challenge_group_id INT,
	_result INT,
	_type TEXT
)
RETURNS JSON AS
$$
	DECLARE
		_result_id INT;
	BEGIN
		INSERT INTO	result (user_id,
												challenge_id,
												challenge_group_id,
												result,
												country_id,
												region_id,
												county_id,
												district_id,
                        age,
                        weight,
												type)
		SELECT      _user_id,
          			_challenge_id,
          			_challenge_group_id,
          			_result,
          			cty.id,
          			r.id,
          			c.id,
          			ud.district_id,
                extract(year FROM age(date_of_birth)),
                (
                  SELECT    weight
                  FROM      user_weight
                  WHERE     user_id = _user_id
                  ORDER BY  created DESC
                  LIMIT     1
								),
								_type
		FROM        "user" AS u
    INNER JOIN  user_district AS ud ON u.id = ud.user_id
    INNER JOIN  district AS d ON ud.district_id = d.id
    INNER JOIN  county AS c ON d.county_id = c.id
    INNER JOIN  region AS r ON c.region_id = r.id
    INNER JOIN  country AS cty ON r.country_id = cty.id
    WHERE       u.id = _user_id
		ORDER BY		ud.created DESC
		LIMIT 1
		RETURNING		id
		INTO				_result_id;

		RETURN to_json(_result_id);
	END
$$
LANGUAGE plpgsql;