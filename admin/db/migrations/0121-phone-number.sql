ALTER TABLE "user" ADD mobile_number TEXT;
ALTER TABLE "user" DROP COLUMN nationality;
ALTER TABLE "user" DROP COLUMN occupation;
DROP TABLE user_relationship_status;
DROP TABLE relationship_status;

CREATE OR REPLACE FUNCTION sp_gymfit_site_register_persist_step_6_details (
  _user_id INT,
  _mobile_number TEXT,
  _routine_type_id INT,
  _gym_id INT
)
RETURNS JSON AS
$$
BEGIN
	UPDATE   "user"
  SET     mobile_number = _mobile_number
  WHERE   id = _user_id;

  IF _routine_type_id IS NOT NULL THEN
    INSERT INTO user_routine_type (user_id, routine_type_id)
    VALUES (_user_id, _routine_type_id);
  END IF;

  IF _gym_id IS NOT NULL THEN
    INSERT INTO user_gym (user_id, gym_id)
    VALUES (_user_id, _gym_id);
  END IF;

  RETURN sp_gymfit_site_user_get(_user_id);
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_register_get_step_6_details (
  _country_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
		(
			SELECT
  						array
  						(
                SELECT row_to_json(b)
                FROM (
  							         SELECT			id,
                                    value
  							         FROM				routine_type
                ) AS b
  						) AS "routineTypes",
							array
							(
                SELECT row_to_json(c)
                FROM (
        								SELECT			id,
                                    value
        								FROM				gym
                        WHERE       country_id = _country_id
                ) AS c
							) AS "gyms"
		) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_registration_add_user
(
	_registration_id INT
)
RETURNS JSON AS
$$
	DECLARE
		_user_id INT;
	BEGIN
		INSERT INTO	"user" (username, hash, first_name, last_name, date_of_birth, email, avatar_uploaded)
		SELECT			username,
								hash,
								first_name,
								last_name,
								date_of_birth,
								email,
                '0'
		FROM				user_registration
		WHERE				id = _registration_id
		RETURNING		id
		INTO				_user_id;

		DELETE FROM	user_registration
		WHERE				id = _registration_id;

		INSERT INTO	user_email_verification (user_id)
		VALUES			(_user_id);

		INSERT INTO	user_notification (user_id, notification_id)
		VALUES			(_user_id, 1),
								(_user_id, 2),
								(_user_id, 3),
								(_user_id, 4);

		RETURN row_to_json(result)
		FROM (
			SELECT					u.id,
											u.username,
											u.email,
											ev.key AS "verificationKey"
			FROM						"user" AS u
			LEFT OUTER JOIN	user_email_verification AS ev ON ev.user_id = u.id
			WHERE						u.id = _user_id
		) AS result;
	END;
$$
LANGUAGE plpgsql;
