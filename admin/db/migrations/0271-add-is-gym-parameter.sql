-- Column: is_gym
ALTER TABLE "user" ADD  is_gym boolean;
ALTER TABLE "user" ALTER is_gym SET DEFAULT false;
