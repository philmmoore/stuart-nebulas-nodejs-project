CREATE OR REPLACE FUNCTION sp_gymfit_site_location_get_regions
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM (
		SELECT						id,
											name
		FROM							region
		WHERE							country_id = _country_id
		ORDER BY					name
	) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_location_get_counties
(
	_region_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM (
		SELECT						id,
											name
		FROM							county
		WHERE							region_id = _region_id
		ORDER BY					name
	) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_location_get_districts
(
	_county_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM (
		SELECT						id,
											name
		FROM							district
		WHERE							county_id = _county_id
		ORDER BY					name
	) AS result;
$$
LANGUAGE sql;
