CREATE FUNCTION sp_gymfit_site_user_update_password
(
	_user_id INT,
	_hash BYTEA
)
RETURNS JSON AS
$$
	DECLARE
		_username TEXT;
	BEGIN
		DELETE
		FROM				user_password_reset
		WHERE				user_id = _user_id;

		UPDATE			"user" SET hash = _hash
		WHERE				id = _user_id
		RETURNING		username
		INTO				_username;

		RETURN to_json(_username);
	END
$$
LANGUAGE plpgsql;
