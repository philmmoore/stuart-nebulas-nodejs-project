ALTER TABLE "user" ADD COLUMN blocked TIMESTAMP;
ALTER TABLE "user" ADD COLUMN block_reason TEXT;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_user_get_all()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
		(
			SELECT			u.id,
									u.username,
									u.first_name AS "firstName",
									u.last_name AS "lastName",
									u.created AS "registrationDate",
									(
										SELECT		timestamp
										FROM			user_audit
										WHERE			user_id = u.id
										ORDER BY	timestamp DESC
										LIMIT 		1
									) AS "lastLoginDate",
									u.blocked,
									u.avatar_uploaded AS "avatarUploaded"
			FROM				"user" AS u
			ORDER BY		u.username
		) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_user_get
(
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
		(
			SELECT			u.id,
									u.username,
									u.first_name AS "firstName",
									u.last_name AS "lastName",
									u.created AS "registrationDate",
									(
										SELECT		timestamp
										FROM			user_audit
										WHERE			user_id = u.id
										ORDER BY	timestamp DESC
										LIMIT 		1
									) AS "lastLoginDate",
									u.blocked,
									u.avatar_uploaded AS "avatarUploaded"
			FROM				"user" AS u
			WHERE				u.id = _user_id
		) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_user_unblock
(
	_user_id INT
)
RETURNS JSON AS
$$
	UPDATE		"user"
	SET				blocked = NULL,
						block_reason = NULL
	WHERE 		id = _user_id
	AND				blocked IS NOT NULL;
	
	SELECT row_to_json(result_to_return)
	FROM
		(
			SELECT			u.email
			FROM				"user" AS u
			WHERE				u.id = _user_id
		) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_user_block
(
	_user_id INT,
	_reason TEXT
)
RETURNS JSON AS
$$
	UPDATE		"user"
	SET				blocked = current_timestamp,
						block_reason = _reason
	WHERE 		id = _user_id
	AND				blocked IS NULL;
	
	SELECT row_to_json(result_to_return)
	FROM
		(
			SELECT			u.email
			FROM				"user" AS u
			WHERE				u.id = _user_id
		) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_user_remove_avatar
(
	_user_id INT
)
RETURNS JSON AS
$$
	UPDATE		"user"
	SET				avatar_uploaded = NULL
	WHERE 		id = _user_id
	AND				avatar_uploaded IS NOT NULL;
	
	SELECT row_to_json(result_to_return)
	FROM
		(
			SELECT			u.email
			FROM				"user" AS u
			WHERE				u.id = _user_id
		) AS result_to_return;
$$
LANGUAGE sql;