DROP FUNCTION sp_gymfit_site_get_challenge_groups(TEXT);

CREATE TABLE age_group (
	id SERIAL PRIMARY KEY,
  name TEXT NOT NULL,
	min SMALLINT NOT NULL,
	max SMALLINT NOT NULL
);

INSERT INTO age_group (name, min, max)
VALUES
	('13', 13, 13),
	('14', 14, 14),
	('15', 15, 15),
	('16', 16, 16),
	('17', 17, 17),
	('18', 18, 18),
	('19', 19, 19),
	('20 - 29', 20, 29),
	('30 - 39', 30, 39),
	('40 - 49', 40, 49),
	('50 - 59', 50, 59),
	('60 - 69', 60, 69),
	('70 - 79', 70, 79),
	('80+', 80, 130);

	CREATE TABLE weight_group (
		id SERIAL PRIMARY KEY,
	  name TEXT NOT NULL,
		min INT NOT NULL,
		max INT NOT NULL
	);

	INSERT INTO weight_group (name, min, max)
	VALUES
		('Under 50kg', 5000, 49000),
		('50kg - 59kg', 50000, 59000),
		('60kg - 69kg', 60000, 69000),
		('70kg - 79kg', 70000, 79000),
		('80kg - 89kg', 80000, 89000),
		('90kg - 99kg', 90000, 99000),
		('100kg+', 100000, 700000);

CREATE OR REPLACE FUNCTION sp_gymfit_site_get_challenge_filters
(
	_challenge_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
		(
			SELECT
							array
							(
                  SELECT row_to_json(a)
                  FROM (
                          SELECT			cg.id,
                                      cg.value AS "name"
                          FROM				challenge_group AS cg
													INNER JOIN	challenge_to_challenge_group AS ccg ON ccg.challenge_group_id = cg.id
													WHERE				ccg.challenge_id = c.id
                  ) AS a
              )  AS "groups",
							array
							(
                  SELECT row_to_json(a)
                  FROM (
                          SELECT			id,
                                      name
                          FROM				age_group
                  ) AS a
              )  AS "ageGroups",
							array
							(
                  SELECT row_to_json(a)
                  FROM (
                          SELECT			id,
                                      name
                          FROM				weight_group
                  ) AS a
              )  AS "weightGroups"
			FROM		challenge AS c
			WHERE		c.id = _challenge_id
		) AS result_to_return;
$$
LANGUAGE sql;
