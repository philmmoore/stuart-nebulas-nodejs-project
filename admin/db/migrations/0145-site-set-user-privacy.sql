CREATE OR REPLACE FUNCTION sp_gymfit_site_user_set_privacy
(
	_user_id 								INT,
	_gymfit_email_opt_in		BOOLEAN,
	_partners_email_opt_in	BOOLEAN,
	_gymfit_sms_opt_in			BOOLEAN,
	_partners_sms_opt_in		BOOLEAN,
	_gymfit_online_opt_in		BOOLEAN,
	_partners_online_opt_in	BOOLEAN
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO	user_privacy_preferences
		(
			user_id,
			gymfit_email_opt_in,
			partners_email_opt_in,
			gymfit_sms_opt_in,
			partners_sms_opt_in,
			gymfit_online_opt_in,
			partners_online_opt_in
		)
		VALUES
		(
			_user_id,
			_gymfit_email_opt_in,
			_partners_email_opt_in,
			_gymfit_sms_opt_in,
			_partners_sms_opt_in,
			_gymfit_online_opt_in,
			_partners_online_opt_in
		);

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;
