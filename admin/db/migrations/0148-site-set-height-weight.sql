CREATE OR REPLACE FUNCTION sp_gymfit_api_user_set_weight
(
	_user_id INT,
	_weight INT
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO	user_weight (user_id, weight)
		VALUES (_user_id, _weight);

		UPDATE			user_notification
		SET					acknowledged = current_timestamp
		WHERE				user_id = _user_id
		AND					notification_id = 4
		AND					acknowledged IS NULL;

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;

CREATE FUNCTION sp_gymfit_site_user_set_weight
(
	_user_id INT,
	_weight INT
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO	user_weight (user_id, weight)
		VALUES (_user_id, _weight);

		UPDATE			user_notification
		SET					acknowledged = current_timestamp
		WHERE				user_id = _user_id
		AND					notification_id = 4
		AND					acknowledged IS NULL;

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;

CREATE FUNCTION sp_gymfit_site_user_set_height
(
	_user_id INT,
	_height SMALLINT
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO	user_height (user_id, height)
		VALUES (_user_id, _height);

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_get
(
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		SELECT					u.id,
										u.username,
										u.first_name AS "firstName",
										u.last_name AS "lastName",
										u.is_male AS "isMale",
										u.date_of_birth AS "dateOfBirth",
										u.avatar_uploaded AS "avatarUploaded",
                    u.is_personal_trainer AS "isPersonalTrainer",
										(
											SELECT row_to_json(result_location)
											FROM
											(
												SELECT
												d.id AS "districtId",
												d.name AS "districtName",
												c.id AS "countyId",
												c.name AS "countyName",
												r.id AS "regionId",
												r.name AS "regionName",
												ct.id AS "countryId",
												ct.name AS "countryName",
												ct.code AS "countryCode"
											) AS result_location
										) AS "location",
			              (
											SELECT		height
											FROM 			user_height
											WHERE 		user_id = _user_id
											ORDER BY	created DESC
											LIMIT 1
										) AS height,
										(
											SELECT		weight
											FROM 			user_weight
											WHERE 		user_id = _user_id
											ORDER BY	created DESC
											LIMIT 1
										) AS weight,
			              rt.value AS "routineType",
										g.value AS "gym" 
		FROM						"user" AS u
		LEFT OUTER JOIN	user_district AS ud
		ON							u.id = ud.user_id
		LEFT OUTER JOIN	district AS d
		ON							ud.district_id = d.id
		LEFT OUTER JOIN	county AS c
		ON							d.county_id = c.id
		LEFT OUTER JOIN region AS r
		ON							c.region_id = r.id
		LEFT OUTER JOIN	country AS ct
		ON							r.country_id = ct.id
    LEFT OUTER JOIN user_routine_type AS ur
    ON              u.id = ur.user_id
    LEFT OUTER JOIN routine_type AS rt
    ON              ur.routine_type_id = rt.id
		LEFT OUTER JOIN user_gym AS ug
    ON              u.id = ug.user_id
		LEFT OUTER JOIN gym AS g
    ON              g.id = ug.gym_id
		WHERE						u.id = _user_id
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_gym_get
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
		(
			SELECT		id,
								value as name
			FROM			gym
			WHERE			country_id = _country_id
		) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_routine_type_get()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
		(
			SELECT		id,
								value as name
			FROM			routine_type
		) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_set_gym
(
	_user_id INT,
	_gym_id INT
)
RETURNS JSON AS
$$
	BEGIN
		DELETE FROM	user_gym
		WHERE				user_id = _user_id;

		IF _gym_id IS NOT NULL THEN
			INSERT INTO	user_gym (user_id, gym_id)
			VALUES (_user_id, _gym_id);
		END IF;

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_user_set_gym
(
	_user_id INT,
	_gym_id INT
)
RETURNS JSON AS
$$
	BEGIN
		DELETE FROM	user_gym
		WHERE				user_id = _user_id;

		IF _gym_id IS NOT NULL THEN
			INSERT INTO	user_gym (user_id, gym_id)
			VALUES (_user_id, _gym_id);
		END IF;

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_set_routine
(
	_user_id INT,
	_routine_type_id INT
)
RETURNS JSON AS
$$
	BEGIN
		DELETE FROM	user_routine_type
		WHERE				user_id = _user_id;

		INSERT INTO	user_routine_type (user_id, routine_type_id)
		VALUES (_user_id, _routine_type_id);

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;

INSERT INTO gym(id, country_id, value) VALUES (1, 1, 'None');

INSERT INTO 		user_gym (gym_id, user_id)
SELECT 					1, rt.user_id
FROM 						user_routine_type AS rt
LEFT OUTER JOIN user_gym AS ug ON rt.user_id = ug.user_id
WHERE 					ug.user_id IS NULL
