INSERT INTO user_privacy_preferences
(
	user_id,
	gymfit_email_opt_in,
	partners_email_opt_in,
	gymfit_sms_opt_in,
	partners_sms_opt_in,
	gymfit_online_opt_in,
	partners_online_opt_in
)
SELECT DISTINCT u.id,
								TRUE,
								TRUE,
								TRUE,
								TRUE,
								TRUE,
								TRUE
FROM "user" 		AS u
LEFT OUTER JOIN user_privacy_preferences AS p ON p.user_id = u.id
WHERE 					p IS NULL;

CREATE TABLE user_email_preference
(
	user_id			INT REFERENCES "user" (id),
	updated			TIMESTAMP DEFAULT current_timestamp,
	ranking			BOOLEAN NOT NULL,
	progress		BOOLEAN NOT NULL,
	news 				BOOLEAN NOT NULL,
	promotion		BOOLEAN NOT NULL
);

INSERT INTO user_email_preference (user_id, ranking, progress, news, promotion)
SELECT			id,
						TRUE,
						TRUE,
						TRUE,
						TRUE
FROM				"user";

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_get_email_preference
(
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		SELECT					ranking,
										progress,
										news,
										promotion
		FROM						user_email_preference
		WHERE						user_id = _user_id
		ORDER BY				updated DESC
		LIMIT						1
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_set_email_preference
(
	_user_id 		INT,
	_ranking		BOOLEAN,
	_progress		BOOLEAN,
	_news				BOOLEAN,
	_promotion	BOOLEAN
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO	user_email_preference
		(
			user_id,
			ranking,
			progress,
			news,
			promotion
		)
		VALUES
		(
			_user_id,
			_ranking,
			_progress,
			_news,
			_promotion
		);

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;