CREATE OR REPLACE FUNCTION sp_gymfit_site_user_activity
(
	_user_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_previous_period_start TIMESTAMP,
	_previous_period_end TIMESTAMP
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
	(
		SELECT
			DISTINCT
			c.id AS "challengeId",
			c.name AS "challengeName",
			cg.id AS "challengeGroupId",
			cg.value AS "challengeGroupName",
			cc.value AS "category",
			cc.id AS "categoryId",
			ct.measurement AS "measurement",
			
			(SELECT MAX(r.result)
			FROM 			result AS r
			WHERE 		r.challenge_id = ee.challenge_id
			AND 			r.excluded IS NULL
			AND 			r.challenge_group_id = ee.challenge_group_id
			AND 			r.user_id = _user_id) AS "result",
			
			(SELECT 	id
			FROM 			result AS r
			WHERE 		r.challenge_id = ee.challenge_id
			AND 			r.excluded IS NULL
			AND 			r.challenge_group_id = ee.challenge_group_id
			AND 			r.user_id = _user_id
			ORDER BY	result DESC LIMIT 1) AS "resultId",

			(SELECT xy.row_number FROM
				(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
				r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _this_period_start
				AND				r.submitted <= _this_period_end
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			WHERE xy.user_id = _user_id) AS "worldCurrent",
			
			(SELECT xy.row_number FROM
				(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
				r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _previous_period_start
				AND				r.submitted <= _previous_period_end
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			WHERE xy.user_id = _user_id) AS "worldPrevious",
			
			(SELECT COUNT(*) FROM
				(SELECT DISTINCT r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _this_period_start
				AND				r.submitted <= _this_period_end
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			) AS "worldTotal",
			
			(SELECT xy.row_number FROM
				(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
				r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _this_period_start
				AND				r.submitted <= _this_period_end
				AND 			r.excluded IS NULL
				AND				r.country_id = (SELECT country_id FROM region WHERE id = (SELECT region_id FROM county WHERE id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1))))
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			WHERE xy.user_id = _user_id) AS "countryCurrent",
			
			(SELECT xy.row_number FROM
				(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
				r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _previous_period_start
				AND				r.submitted <= _previous_period_end
				AND 			r.excluded IS NULL
				AND				r.country_id = (SELECT country_id FROM region WHERE id = (SELECT region_id FROM county WHERE id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1))))
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			WHERE xy.user_id = _user_id) AS "countryPrevious",
			
			(SELECT COUNT(*) FROM
				(SELECT DISTINCT r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _this_period_start
				AND				r.submitted <= _this_period_end
				AND 			r.excluded IS NULL
				AND				r.country_id = (SELECT country_id FROM region WHERE id = (SELECT region_id FROM county WHERE id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1))))
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			) AS "countryTotal",

			(SELECT xy.row_number FROM
				(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
				r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _this_period_start
				AND				r.submitted <= _this_period_end
				AND 			r.excluded IS NULL
				AND				r.region_id = (SELECT region_id FROM county WHERE id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1)))
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			WHERE xy.user_id = _user_id) as "regionCurrent",
			
			(SELECT xy.row_number FROM
				(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
				r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _previous_period_start
				AND				r.submitted <= _previous_period_end
				AND 			r.excluded IS NULL
				AND				r.region_id = (SELECT region_id FROM county WHERE id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1)))
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			WHERE xy.user_id = _user_id) as "regionPrevious",
			
			(SELECT COUNT(*) FROM
				(SELECT DISTINCT r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _this_period_start
				AND				r.submitted <= _this_period_end
				AND 			r.excluded IS NULL
				AND				r.region_id = (SELECT region_id FROM county WHERE id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1)))
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			) AS "regionTotal",

			(SELECT xy.row_number FROM
				(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
				r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _this_period_start
				AND				r.submitted <= _this_period_end
				AND 			r.excluded IS NULL
				AND				r.county_id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1))
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			WHERE xy.user_id = _user_id) as "countyCurrent",
			
			(SELECT xy.row_number FROM
				(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
				r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _previous_period_start
				AND				r.submitted <= _previous_period_end
				AND 			r.excluded IS NULL
				AND				r.county_id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1))
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			WHERE xy.user_id = _user_id) as "countyPrevious",
			
			(SELECT COUNT(*) FROM
				(SELECT DISTINCT r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _this_period_start
				AND				r.submitted <= _this_period_end
				AND 			r.excluded IS NULL
				AND				r.county_id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1))
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			) AS "countyTotal",

			(SELECT xy.row_number FROM
				(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
				r.user_id
				FROM			result AS r
				WHERE			r.challenge_id = ee.challenge_id
				AND				r.submitted >= _this_period_start
				AND				r.submitted <= _this_period_end
				AND 			r.excluded IS NULL
				AND				r.district_id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1)
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY	r.user_id) AS xy
			WHERE xy.user_id = _user_id) as "districtCurrent",
			
			(SELECT xy.row_number FROM
				(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
				r.user_id
				FROM			result AS r
				WHERE			r.challenge_id = ee.challenge_id
				AND				r.submitted >= _previous_period_start
				AND				r.submitted <= _previous_period_end
				AND 			r.excluded IS NULL
				AND				r.district_id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1)
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY	r.user_id) AS xy
			WHERE xy.user_id = _user_id) as "districtPrevious",
			
			(SELECT COUNT(*) FROM
				(SELECT DISTINCT r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _this_period_start
				AND				r.submitted <= _this_period_end
				AND 			r.excluded IS NULL
				AND				r.district_id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1)
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			) AS "districtTotal"
		FROM result AS ee

		LEFT OUTER JOIN challenge AS c ON ee.challenge_id = c.id
		LEFT OUTER JOIN challenge_group AS cg ON ee.challenge_group_id = cg.id
		LEFT OUTER JOIN challenge_type AS ct on c.type = ct.id
		LEFT OUTER JOIN challenge_category AS cc ON cc.id = c.category

		WHERE	ee.challenge_id IN (SELECT challenge_id FROM result WHERE user_id = _user_id)
		AND	ee.challenge_group_id IN (SELECT challenge_group_id FROM result WHERE user_id = _user_id)
		AND (SELECT COUNT(r.result)
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				AND 			r.user_id = _user_id) > 0

	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_user_activity
(
	_user_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_previous_period_start TIMESTAMP,
	_previous_period_end TIMESTAMP
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
	(
		SELECT
			DISTINCT
			c.id AS "challengeId",
			c.name AS "challengeName",
			cg.id AS "challengeGroupId",
			cg.value AS "challengeGroupName",
			cc.value AS "category",
			cc.id AS "categoryId",
			ct.measurement AS "measurement",
			
			(SELECT MAX(r.result)
			FROM 			result AS r
			WHERE 		r.challenge_id = ee.challenge_id
			AND 			r.excluded IS NULL
			AND 			r.challenge_group_id = ee.challenge_group_id
			AND 			r.user_id = _user_id) AS "result",

			(SELECT xy.row_number FROM
				(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
				r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _this_period_start
				AND				r.submitted <= _this_period_end
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			WHERE xy.user_id = _user_id) AS "worldCurrent",
			
			(SELECT xy.row_number FROM
				(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
				r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _previous_period_start
				AND				r.submitted <= _previous_period_end
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			WHERE xy.user_id = _user_id) AS "worldPrevious",
			
			(SELECT COUNT(*) FROM
				(SELECT DISTINCT r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _this_period_start
				AND				r.submitted <= _this_period_end
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			) AS "worldTotal",
			
			(SELECT xy.row_number FROM
				(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
				r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _this_period_start
				AND				r.submitted <= _this_period_end
				AND 			r.excluded IS NULL
				AND				r.country_id = (SELECT country_id FROM region WHERE id = (SELECT region_id FROM county WHERE id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1))))
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			WHERE xy.user_id = _user_id) AS "countryCurrent",
			
			(SELECT xy.row_number FROM
				(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
				r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _previous_period_start
				AND				r.submitted <= _previous_period_end
				AND 			r.excluded IS NULL
				AND				r.country_id = (SELECT country_id FROM region WHERE id = (SELECT region_id FROM county WHERE id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1))))
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			WHERE xy.user_id = _user_id) AS "countryPrevious",
			
			(SELECT COUNT(*) FROM
				(SELECT DISTINCT r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _this_period_start
				AND				r.submitted <= _this_period_end
				AND 			r.excluded IS NULL
				AND				r.country_id = (SELECT country_id FROM region WHERE id = (SELECT region_id FROM county WHERE id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1))))
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			) AS "countryTotal",

			(SELECT xy.row_number FROM
				(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
				r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _this_period_start
				AND				r.submitted <= _this_period_end
				AND 			r.excluded IS NULL
				AND				r.region_id = (SELECT region_id FROM county WHERE id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1)))
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			WHERE xy.user_id = _user_id) as "regionCurrent",
			
			(SELECT xy.row_number FROM
				(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
				r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _previous_period_start
				AND				r.submitted <= _previous_period_end
				AND 			r.excluded IS NULL
				AND				r.region_id = (SELECT region_id FROM county WHERE id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1)))
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			WHERE xy.user_id = _user_id) as "regionPrevious",
			
			(SELECT COUNT(*) FROM
				(SELECT DISTINCT r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _this_period_start
				AND				r.submitted <= _this_period_end
				AND 			r.excluded IS NULL
				AND				r.region_id = (SELECT region_id FROM county WHERE id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1)))
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			) AS "regionTotal",

			(SELECT xy.row_number FROM
				(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
				r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _this_period_start
				AND				r.submitted <= _this_period_end
				AND 			r.excluded IS NULL
				AND				r.county_id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1))
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			WHERE xy.user_id = _user_id) as "countyCurrent",
			
			(SELECT xy.row_number FROM
				(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
				r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _previous_period_start
				AND				r.submitted <= _previous_period_end
				AND 			r.excluded IS NULL
				AND				r.county_id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1))
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			WHERE xy.user_id = _user_id) as "countyPrevious",
			
			(SELECT COUNT(*) FROM
				(SELECT DISTINCT r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _this_period_start
				AND				r.submitted <= _this_period_end
				AND 			r.excluded IS NULL
				AND				r.county_id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1))
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			) AS "countyTotal",

			(SELECT xy.row_number FROM
				(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
				r.user_id
				FROM			result AS r
				WHERE			r.challenge_id = ee.challenge_id
				AND				r.submitted >= _this_period_start
				AND				r.submitted <= _this_period_end
				AND 			r.excluded IS NULL
				AND				r.district_id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1)
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY	r.user_id) AS xy
			WHERE xy.user_id = _user_id) as "districtCurrent",
			
			(SELECT xy.row_number FROM
				(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
				r.user_id
				FROM			result AS r
				WHERE			r.challenge_id = ee.challenge_id
				AND				r.submitted >= _previous_period_start
				AND				r.submitted <= _previous_period_end
				AND 			r.excluded IS NULL
				AND				r.district_id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1)
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY	r.user_id) AS xy
			WHERE xy.user_id = _user_id) as "districtPrevious",
			
			(SELECT COUNT(*) FROM
				(SELECT DISTINCT r.user_id
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND				r.submitted >= _this_period_start
				AND				r.submitted <= _this_period_end
				AND 			r.excluded IS NULL
				AND				r.district_id = (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1)
				AND 			r.challenge_group_id = ee.challenge_group_id
				GROUP BY 	r.user_id) AS xy
			) AS "districtTotal"
		FROM result AS ee

		LEFT OUTER JOIN challenge AS c ON ee.challenge_id = c.id
		LEFT OUTER JOIN challenge_group AS cg ON ee.challenge_group_id = cg.id
		LEFT OUTER JOIN challenge_type AS ct on c.type = ct.id
		LEFT OUTER JOIN challenge_category AS cc ON cc.id = c.category

		WHERE	ee.challenge_id IN (SELECT challenge_id FROM result WHERE user_id = _user_id)
		AND	ee.challenge_group_id IN (SELECT challenge_group_id FROM result WHERE user_id = _user_id)
		AND (SELECT COUNT(r.result)
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				AND 			r.user_id = _user_id) > 0

	) AS result_to_return;
$$
LANGUAGE sql;