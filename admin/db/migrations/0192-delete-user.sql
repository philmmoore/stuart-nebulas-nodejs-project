CREATE TABLE banned_email
(
	email TEXT NOT NULL PRIMARY KEY,
	created TIMESTAMP DEFAULT current_timestamp NOT NULL,
	reason TEXT NOT NULL
);

CREATE OR REPLACE FUNCTION sp_gymfit_admin_user_delete
(
	_user_id INT,
	_reason TEXT
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO banned_email
		(
			email,
			reason
		)
		SELECT			email,
								_reason
		FROM				"user"
		WHERE				id = _user_id;
		
		DELETE FROM	user_email_verification
		WHERE				user_id = _user_id;
		
		DELETE FROM	user_password_reset
		WHERE				user_id = _user_id;
	
		DELETE FROM	user_district
		WHERE				user_id = _user_id;
		
		DELETE FROM	user_region
		WHERE				user_id = _user_id;

		DELETE FROM user_height
		WHERE				user_id = _user_id;

		DELETE FROM user_weight
		WHERE				user_id = _user_id;

		DELETE FROM result_feedback
		WHERE				user_id = _user_id;

		DELETE FROM result
		WHERE				user_id = _user_id;
		
		DELETE FROM user_routine_type
		WHERE				user_id = _user_id;
		
		DELETE FROM user_audit
		WHERE				user_id = _user_id;
		
		DELETE FROM user_notification
		WHERE				user_id = _user_id;
		
		DELETE FROM user_privacy_preferences
		WHERE				user_id = _user_id;
		
		DELETE FROM user_email_preference
		WHERE				user_id = _user_id;
		
		DELETE FROM	user_follower
		WHERE				user_id = _user_id
		OR					follower_user_id = _user_id;
		
		DELETE FROM	user_client
		WHERE				user_id = _user_id
		OR					client_user_id = _user_id;

		DELETE FROM "user"
		WHERE 			id = _user_id;

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_user_get_all_results
(
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
		(
			SELECT		id,
								image_validated AS "imageValidated",
								video_validated AS "videoValidated"
			FROM			result
			WHERE			user_id = _user_id
		) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_user_get
(
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
		(
			SELECT			u.id,
									u.username,
									u.first_name AS "firstName",
									u.last_name AS "lastName",
									u.created AS "registrationDate",
									(
										SELECT		timestamp
										FROM			user_audit
										WHERE			user_id = u.id
										ORDER BY	timestamp DESC
										LIMIT 		1
									) AS "lastLoginDate",
									u.blocked,
									u.avatar_uploaded AS "avatarUploaded",
									u.email
			FROM				"user" AS u
			WHERE				u.id = _user_id
		) AS result_to_return;
$$
LANGUAGE sql;

CREATE FUNCTION sp_gymfit_site_user_is_email_banned
(
	_email TEXT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM (
		SELECT		TRUE as result
		FROM			banned_email
		WHERE			email = LOWER(_email)
		LIMIT			1
	) AS result_to_return;
$$
LANGUAGE sql;

ALTER TABLE email_template ADD COLUMN user_deleted_subject TEXT NOT NULL DEFAULT 'Account Deleted';
ALTER TABLE email_template ADD COLUMN user_deleted_html_body TEXT NOT NULL DEFAULT 'Your account has been deleted.';
ALTER TABLE email_template ADD COLUMN user_deleted_plain_body TEXT NOT NULL DEFAULT 'Your account has been deleted.';

DROP FUNCTION IF EXISTS sp_gymfit_admin_email_template_set(INT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT);

CREATE OR REPLACE FUNCTION sp_gymfit_admin_email_template_set
(
	_country_id INT,
	_standard_template TEXT,
	_email_confirmation_subject TEXT,
	_email_confirmation_html_body TEXT,
	_email_confirmation_plain_body TEXT,
	_password_reset_subject TEXT,
	_password_reset_html_body TEXT,
	_password_reset_plain_body TEXT,
	_result_rejected_subject TEXT,
	_result_rejected_html_body TEXT,
	_result_rejected_plain_body TEXT,
	_user_blocked_subject TEXT,
	_user_blocked_html_body TEXT,
	_user_blocked_plain_body TEXT,
	_user_unblocked_subject TEXT,
	_user_unblocked_html_body TEXT,
	_user_unblocked_plain_body TEXT,
	_user_avatar_removed_subject TEXT,
	_user_avatar_removed_html_body TEXT,
	_user_avatar_removed_plain_body TEXT,
	_register_reminder_subject TEXT,
	_register_reminder_html_body TEXT,
	_register_reminder_plain_body TEXT,
	_user_challenge_progress_subject TEXT,
	_user_challenge_progress_html_body TEXT,
	_user_challenge_progress_plain_body TEXT,
	_leaderboard_ranking_subject TEXT,
	_leaderboard_ranking_html_body TEXT,
	_leaderboard_ranking_plain_body TEXT,
	_user_deleted_subject TEXT,
	_user_deleted_html_body TEXT,
	_user_deleted_plain_body TEXT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	email_template
		SET			standard_template = _standard_template,
						email_confirmation_subject = _email_confirmation_subject,
						email_confirmation_html_body = _email_confirmation_html_body,
						email_confirmation_plain_body = _email_confirmation_plain_body,
						password_reset_subject = _password_reset_subject,
						password_reset_html_body = _password_reset_html_body,
						password_reset_plain_body = _password_reset_plain_body,
						result_rejected_subject = _result_rejected_subject,
						result_rejected_html_body = _result_rejected_html_body,
						result_rejected_plain_body = _result_rejected_plain_body,
						user_blocked_subject = _user_blocked_subject,
						user_blocked_html_body = _user_blocked_html_body,
						user_blocked_plain_body = _user_blocked_plain_body,
						user_unblocked_subject = _user_unblocked_subject,
						user_unblocked_html_body = _user_unblocked_html_body,
						user_unblocked_plain_body = _user_unblocked_plain_body,
						user_avatar_removed_subject = _user_avatar_removed_subject,
						user_avatar_removed_html_body =	_user_avatar_removed_html_body,
						user_avatar_removed_plain_body = _user_avatar_removed_plain_body,
						register_reminder_subject = _register_reminder_subject,
						register_reminder_html_body = _register_reminder_html_body,
						register_reminder_plain_body = _register_reminder_plain_body,
						user_challenge_progress_subject = _user_challenge_progress_subject,
						user_challenge_progress_html_body = _user_challenge_progress_html_body,
						user_challenge_progress_plain_body = _user_challenge_progress_plain_body,
						leaderboard_ranking_subject = _leaderboard_ranking_subject,
						leaderboard_ranking_html_body = _leaderboard_ranking_html_body,
						leaderboard_ranking_plain_body = _leaderboard_ranking_plain_body,
						user_deleted_subject = _user_deleted_subject,
						user_deleted_html_body = _user_deleted_html_body,
						user_deleted_plain_body = _user_deleted_plain_body
		WHERE		country_id = _country_id;

		RETURN to_json(1);
	END
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_email_template_get
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)	FROM (
		SELECT	standard_template AS "standardTemplate",
						email_confirmation_subject AS "emailConfirmationSubject",
						email_confirmation_html_body AS "emailConfirmationHtmlBody",
						email_confirmation_plain_body AS "emailConfirmationPlainBody",
						password_reset_subject AS "passwordResetSubject",
						password_reset_html_body AS "passwordResetHtmlBody",
						password_reset_plain_body AS "passwordResetPlainBody",
						result_rejected_subject AS "resultRejectedSubject",
						result_rejected_html_body AS "resultRejectedHtmlBody",
						result_rejected_plain_body AS "resultRejectedPlainBody",
						user_blocked_subject AS "userBlockedSubject",
						user_blocked_html_body AS "userBlockedHtmlBody",
						user_blocked_plain_body AS "userBlockedPlainBody",
						user_unblocked_subject AS "userUnblockedSubject",
						user_unblocked_html_body AS "userUnblockedHtmlBody",
						user_unblocked_plain_body AS "userUnblockedPlainBody",
						user_avatar_removed_subject AS "userAvatarRemovedSubject",
						user_avatar_removed_html_body AS "userAvatarRemovedHtmlBody",
						user_avatar_removed_plain_body AS "userAvatarRemovedPlainBody",
						register_reminder_subject AS "registerReminderSubject",
						register_reminder_html_body AS "registerReminderHtmlBody",
						register_reminder_plain_body AS "registerReminderPlainBody",						
						user_challenge_progress_subject AS "userChallengeProgressSubject",
						user_challenge_progress_html_body AS "userChallengeProgressHtmlBody",
						user_challenge_progress_plain_body AS "userChallengeProgressPlainBody",
						leaderboard_ranking_subject AS "leaderboardRankingSubject",
						leaderboard_ranking_html_body AS "leaderboardRankingHtmlBody",
						leaderboard_ranking_plain_body AS "leaderboardRankingPlainBody",
						user_deleted_subject AS "userDeletedSubject",
						user_deleted_html_body AS "userDeletedHtmlBody",
						user_deleted_plain_body AS "userDeletedPlainBody"
		FROM		email_template
		WHERE		country_id = _country_id
	) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_task_runner_email_template_get
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)	FROM (
		SELECT	standard_template AS "standardTemplate",
						email_confirmation_subject AS "emailConfirmationSubject",
						email_confirmation_html_body AS "emailConfirmationHtmlBody",
						email_confirmation_plain_body AS "emailConfirmationPlainBody",
						password_reset_subject AS "passwordResetSubject",
						password_reset_html_body AS "passwordResetHtmlBody",
						password_reset_plain_body AS "passwordResetPlainBody",
						result_rejected_subject AS "resultRejectedSubject",
						result_rejected_html_body AS "resultRejectedHtmlBody",
						result_rejected_plain_body AS "resultRejectedPlainBody",
						user_blocked_subject AS "userBlockedSubject",
						user_blocked_html_body AS "userBlockedHtmlBody",
						user_blocked_plain_body AS "userBlockedPlainBody",
						user_unblocked_subject AS "userUnblockedSubject",
						user_unblocked_html_body AS "userUnblockedHtmlBody",
						user_unblocked_plain_body AS "userUnblockedPlainBody",
						user_avatar_removed_subject AS "userAvatarRemovedSubject",
						user_avatar_removed_html_body AS "userAvatarRemovedHtmlBody",
						user_avatar_removed_plain_body AS "userAvatarRemovedPlainBody",
						register_reminder_subject AS "registerReminderSubject",
						register_reminder_html_body AS "registerReminderHtmlBody",
						register_reminder_plain_body AS "registerReminderPlainBody",						
						user_challenge_progress_subject AS "userChallengeProgressSubject",
						user_challenge_progress_html_body AS "userChallengeProgressHtmlBody",
						user_challenge_progress_plain_body AS "userChallengeProgressPlainBody",
						leaderboard_ranking_subject AS "leaderboardRankingSubject",
						leaderboard_ranking_html_body AS "leaderboardRankingHtmlBody",
						leaderboard_ranking_plain_body AS "leaderboardRankingPlainBody",
						user_deleted_subject AS "userDeletedSubject",
						user_deleted_html_body AS "userDeletedHtmlBody",
						user_deleted_plain_body AS "userDeletedPlainBody"
		FROM		email_template
		WHERE		country_id = _country_id
	) AS result;
$$
LANGUAGE sql;