INSERT INTO area (country, name)
VALUES
  (1, 'gb');

UPDATE  area
SET     parent = 137
WHERE   parent IS NULL
AND     ID != 137;
