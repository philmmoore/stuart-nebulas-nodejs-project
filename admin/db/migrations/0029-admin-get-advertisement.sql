CREATE FUNCTION sp_admin_get_advertisement
(
	_advertisementId INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
		(
		SELECT	advert.id,
				advert.name,
				advert.link,
				advert.small_horizontal_image_extension "smallHorizontalImageExtension",
				advert.large_vertical_image_extension "largeVerticalImageExtension",
				(
					array
					(
						SELECT	advert_to_area.area_id
						FROM	advert_to_area AS advert_to_area
						WHERE	advert_to_area.advert_id = advert.id
					)
				) AS areas
		FROM	advert AS advert
		WHERE	advert.id = _advertisementId
		LIMIT	1
		) AS result;
$$
LANGUAGE sql;
