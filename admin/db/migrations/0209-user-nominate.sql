CREATE OR REPLACE FUNCTION sp_gymfit_site_user_get_nomination_suggestions
(
	_user_id INT,
	_challenge_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
		(
			(
				SELECT		u.id AS "id",
									u.username,
									u.first_name AS "firstName",
									u.last_name AS "lastName",
									u.avatar_uploaded AS "avatarUploaded"
				FROM			"user" AS u
				JOIN			user_client AS c ON c.user_id = u.id
				WHERE			c.client_user_id = _user_id
				AND				(SELECT id FROM result WHERE user_id = u.id AND challenge_id = _challenge_id LIMIT 1) IS NULL
				AND				(SELECT id FROM user_nomination WHERE nominee_user_id = u.id AND challenge_id = _challenge_id AND user_id = _user_id LIMIT 1) IS NULL
				ORDER BY	c.created DESC
			)
			UNION
			(
				SELECT		u.id AS "id",
									u.username,
									u.first_name AS "firstName",
									u.last_name AS "lastName",
									u.avatar_uploaded AS "avatarUploaded"
				FROM			"user" AS u
				JOIN			user_client AS c ON c.client_user_id = u.id
				WHERE			c.user_id = _user_id
				AND				(SELECT id FROM result WHERE user_id = u.id AND challenge_id = _challenge_id LIMIT 1) IS NULL
				AND				(SELECT id FROM user_nomination WHERE nominee_user_id = u.id AND challenge_id = _challenge_id AND user_id = _user_id LIMIT 1) IS NULL
				ORDER BY	c.created DESC
			)
			UNION
			(
				SELECT		u.id AS "id",
									u.username,
									u.first_name AS "firstName",
									u.last_name AS "lastName",
									u.avatar_uploaded AS "avatarUploaded"
				FROM			"user" AS u
				JOIN			user_follower AS f ON f.user_id = u.id
				WHERE			f.follower_user_id = _user_id
				AND				(SELECT id FROM result WHERE user_id = u.id AND challenge_id = _challenge_id LIMIT 1) IS NULL
				AND				(SELECT id FROM user_nomination WHERE nominee_user_id = u.id AND challenge_id = _challenge_id AND user_id = _user_id LIMIT 1) IS NULL
				ORDER BY	f.created DESC
			)
			UNION
			(
				SELECT		u.id AS "id",
									u.username,
									u.first_name AS "firstName",
									u.last_name AS "lastName",
									u.avatar_uploaded AS "avatarUploaded"
				FROM			"user" AS u
				JOIN			user_follower AS f ON f.follower_user_id = u.id
				WHERE			f.user_id = _user_id
				AND				(SELECT id FROM result WHERE user_id = u.id AND challenge_id = _challenge_id LIMIT 1) IS NULL
				AND				(SELECT id FROM user_nomination WHERE nominee_user_id = u.id AND challenge_id = _challenge_id AND user_id = _user_id LIMIT 1) IS NULL
				ORDER BY	f.created DESC
			)
		) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_nomination_get_possible_nominees
(
	_user_id INT,
	_challenge_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
		(
			SELECT array
			(
				SELECT	id
				FROM		"user" AS u
				WHERE 	u.id != _user_id
				AND   	(SELECT id FROM result WHERE user_id = u.id AND challenge_id = _challenge_id LIMIT 1) IS NULL
				AND   	(SELECT id FROM user_nomination WHERE nominee_user_id = u.id AND challenge_id = _challenge_id AND user_id = _user_id LIMIT 1) IS NULL
				AND			u.id != _user_id
			) AS "users"
		) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_nomination_accepted
(
	_user_id INT,
	_challenge_id INT,
	_nominee_user_id INT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE 	user_nomination SET accepted=current_timestamp
		WHERE		user_id = _user_id
		AND			challenge_id = _challenge_id
		AND			nominee_user_id = _nominee_user_id;	
			
		RETURN to_json(1);
	END
$$
LANGUAGE plpgsql;