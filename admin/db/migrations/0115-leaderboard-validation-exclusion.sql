DROP FUNCTION IF EXISTS sp_gymfit_site_leaderboard_search(INT, INT, INT, INT, INT, INT, TIMESTAMP, TIMESTAMP, TIMESTAMP, TIMESTAMP, INT, INT, BIT, SMALLINT, SMALLINT, INT, INT);
DROP FUNCTION IF EXISTS sp_gymfit_site_leaderboard_user_position(INT, INT, INT, INT, INT, INT, TIMESTAMP, TIMESTAMP, BIT, SMALLINT, SMALLINT, INT, INT);

ALTER TABLE result ADD excluded TIMESTAMP;
ALTER TABLE result ADD exclude_reason TEXT;

CREATE OR REPLACE FUNCTION sp_gymfit_site_leaderboard_search
(
	_challenge_id INT,
	_challenge_group_id INT,
	_district_id INT,
	_county_id INT,
	_region_id INT,
	_country_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_last_period_start TIMESTAMP,
	_last_period_end TIMESTAMP,
	_results_per_page INT,
	_page INT,
	_is_male BIT,
	_age_min SMALLINT,
	_age_max SMALLINT,
	_weight_min INT,
	_weight_max INT,
	_only_validated BIT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		SELECT
		(
			SELECT array_to_json(array_agg(results))
			FROM
			(
				SELECT			row_number() over (ORDER BY MAX(r.result) DESC) AS "rank",
										u.id AS "userId",
										u.username,
										u.avatar_uploaded AS "userHasAvatar",
                    u.first_name "firstName",
                    u.last_name "lastName",
										(SELECT MAX(r.result)) AS "result",
										(
											SELECT			rs.id
											FROM				result AS rs
											WHERE				rs.user_id = u.id
											AND					rs.result = MAX(r.result)
											AND					rs.image_validated IS NOT NULL
											AND					rs.submitted >= _this_period_start
											AND					rs.submitted <= _this_period_end
											AND					rs.challenge_id = _challenge_id
											AND					rs.challenge_group_id = _challenge_group_id
											AND					(rs.district_id = _district_id OR _district_id IS NULL)
											AND					(rs.county_id = _county_id OR _county_id IS NULL)
											AND					(rs.region_id = _region_id OR _region_id IS NULL)
											AND					(rs.country_id = _country_id OR _country_id IS NULL)
											AND					(rs.age >= _age_min OR _age_min IS NULL)
											AND					(rs.age <= _age_max OR _age_max IS NULL)
											AND					(rs.weight >= _weight_min OR _weight_min IS NULL)
											AND					(rs.weight <= _weight_max OR _weight_max IS NULL)
											AND					((rs.image_validated IS NOT NULL OR rs.video_validated IS NOT NULL) OR _only_validated != '1')
											AND					rs.excluded IS NULL
											AND					(u.is_male = _is_male OR _is_male IS NULL)
											ORDER BY		rs.image_validated DESC
											LIMIT				1
										) AS "imageValidated",
										(
											SELECT			rs.id
											FROM				result AS rs
											WHERE				rs.user_id = u.id
											AND					rs.result = MAX(r.result)
											AND					rs.video_validated IS NOT NULL
											AND					rs.submitted >= _this_period_start
											AND					rs.submitted <= _this_period_end
											AND					rs.challenge_id = _challenge_id
											AND					rs.challenge_group_id = _challenge_group_id
											AND					(rs.district_id = _district_id OR _district_id IS NULL)
											AND					(rs.county_id = _county_id OR _county_id IS NULL)
											AND					(rs.region_id = _region_id OR _region_id IS NULL)
											AND					(rs.country_id = _country_id OR _country_id IS NULL)
											AND					(rs.age >= _age_min OR _age_min IS NULL)
											AND					(rs.age <= _age_max OR _age_max IS NULL)
											AND					(rs.weight >= _weight_min OR _weight_min IS NULL)
											AND					(rs.weight <= _weight_max OR _weight_max IS NULL)
											AND					((rs.image_validated IS NOT NULL OR rs.video_validated IS NOT NULL) OR _only_validated != '1')
											AND					rs.excluded IS NULL
											AND					(u.is_male = _is_male OR _is_male IS NULL)
											ORDER BY		rs.video_validated DESC
											LIMIT				1
										) AS "videoValidated",
										(
											SELECT lastRankings.rank
											FROM
											(
												SELECT			row_number() over (ORDER BY MAX(rs.result) DESC) AS rank,
																		urs.id AS "user_id"
												FROM				result AS rs
												INNER JOIN	"user" AS urs
												ON					rs.user_id = urs.id
												WHERE				rs.submitted >= _last_period_start
												AND					rs.submitted <= _last_period_end
												AND					rs.challenge_id = _challenge_id
												AND					rs.challenge_group_id = _challenge_group_id
												AND					(rs.district_id = _district_id OR _district_id IS NULL)
												AND					(rs.county_id = _county_id OR _county_id IS NULL)
												AND					(rs.region_id = _region_id OR _region_id IS NULL)
												AND					(rs.country_id = _country_id OR _country_id IS NULL)
												AND					(rs.age >= _age_min OR _age_min IS NULL)
												AND					(rs.age <= _age_max OR _age_max IS NULL)
												AND					(rs.weight >= _weight_min OR _weight_min IS NULL)
												AND					(rs.weight <= _weight_max OR _weight_max IS NULL)
												AND					((rs.image_validated IS NOT NULL OR rs.video_validated IS NOT NULL) OR _only_validated != '1')
												AND					rs.excluded IS NULL
												AND					(u.is_male = _is_male OR _is_male IS NULL)
												GROUP BY		urs.id
												ORDER BY		rank
											) AS lastRankings
											WHERE lastRankings.user_id = u.id
										) AS "lastRank"
				FROM				result AS r
				INNER JOIN	"user" AS u
				ON					r.user_id = u.id
				WHERE				r.submitted >= _this_period_start
				AND					r.submitted <= _this_period_end
				AND					r.challenge_id = _challenge_id
				AND					r.challenge_group_id = _challenge_group_id
				AND					(r.district_id = _district_id OR _district_id IS NULL)
				AND					(r.county_id = _county_id OR _county_id IS NULL)
				AND					(r.region_id = _region_id OR _region_id IS NULL)
				AND					(r.country_id = _country_id OR _country_id IS NULL)
				AND					(r.age >= _age_min OR _age_min IS NULL)
				AND					(r.age <= _age_max OR _age_max IS NULL)
				AND					(r.weight >= _weight_min OR _weight_min IS NULL)
				AND					(r.weight <= _weight_max OR _weight_max IS NULL)
				AND					((r.image_validated IS NOT NULL OR r.video_validated IS NOT NULL) OR _only_validated != '1')
				AND					r.excluded IS NULL
				AND					(u.is_male = _is_male OR _is_male IS NULL)
				GROUP BY		u.id
				ORDER BY		rank
				OFFSET			(_page - 1) * _results_per_page
				LIMIT				_results_per_page
			) AS results
		) AS "results",
		(
			SELECT				ceiling(COUNT(DISTINCT r.user_id)::DECIMAL / _results_per_page)
			FROM					result AS r
			INNER JOIN		"user" AS u
			ON						r.user_id = u.id
			WHERE					r.submitted >= _this_period_start
			AND						r.submitted <= _this_period_end
			AND						r.challenge_id = _challenge_id
			AND						r.challenge_group_id = _challenge_group_id
			AND						(r.district_id = _district_id OR _district_id IS NULL)
			AND						(r.county_id = _county_id OR _county_id IS NULL)
			AND						(r.region_id = _region_id OR _region_id IS NULL)
			AND						(r.country_id = _country_id OR _country_id IS NULL)
			AND						(r.age >= _age_min OR _age_min IS NULL)
			AND						(r.age <= _age_max OR _age_max IS NULL)
			AND						(r.weight >= _weight_min OR _weight_min IS NULL)
			AND						(r.weight <= _weight_max OR _weight_max IS NULL)
			AND						((r.image_validated IS NOT NULL OR r.video_validated IS NOT NULL) OR _only_validated != '1')
			AND						r.excluded IS NULL
			AND						(u.is_male = _is_male OR _is_male IS NULL)
		) AS "numberOfPages",
		(
			SELECT				ct.measurement
			FROM					challenge_type AS ct
			LEFT OUTER JOIN challenge AS c ON c.type = ct.id
			WHERE					c.id = _challenge_id
		) AS "measurement"
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_leaderboard_user_position
(
	_challenge_id INT,
	_challenge_group_id INT,
	_district_id INT,
	_county_id INT,
	_region_id INT,
	_country_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_is_male BIT,
	_age_min SMALLINT,
	_age_max SMALLINT,
	_weight_min INT,
	_weight_max INT,
	_only_validated BIT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		SELECT array(
			SELECT			u.id
			from				result AS r
			INNER JOIN	"user" AS u
			ON					r.user_id = u.id
			WHERE				r.submitted >= _this_period_start
			AND					r.submitted <= _this_period_end
			AND					r.challenge_id = _challenge_id
			AND					r.challenge_group_id = _challenge_group_id
			AND					(r.district_id = _district_id OR _district_id IS NULL)
			AND					(r.county_id = _county_id OR _county_id IS NULL)
			AND					(r.region_id = _region_id OR _region_id IS NULL)
			AND					(r.country_id = _country_id OR _country_id IS NULL)
			AND					(r.age >= _age_min OR _age_min IS NULL)
			AND					(r.age <= _age_max OR _age_max IS NULL)
			AND					(r.weight >= _weight_min OR _weight_min IS NULL)
			AND					(r.weight <= _weight_max OR _weight_max IS NULL)
			AND					((r.image_validated IS NOT NULL OR r.video_validated IS NOT NULL) OR _only_validated != '1')
			AND					r.excluded IS NULL
			AND					(u.is_male = _is_male OR _is_male IS NULL)
			GROUP BY		u.id
			ORDER BY		row_number() over (ORDER BY MAX(r.result) DESC)
		) AS rankings
	) AS result_to_return
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_home_result_coverage()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
	(
		SELECT
			c.name AS "challenge",
			cg.value AS "challengeGroup",
			(
				SELECT array_to_json(array_agg(district))
				FROM
				(
					SELECT					d.name,
													COUNT(DISTINCT r.user_id) AS "count"
					FROM						district AS d
					LEFT OUTER JOIN	result AS r
					ON							r.district_id = d.id
					AND							r.challenge_id = c.id
					AND							r.challenge_group_id = cg.id
					AND							r.excluded IS NULL
					GROUP BY				d.name
					ORDER BY				d.name
				) AS district
			) AS "results"
			FROM						challenge AS c
			LEFT OUTER JOIN	challenge_to_challenge_group AS ctg
			ON							ctg.challenge_id = c.id
			LEFT OUTER JOIN	challenge_group AS cg
			ON							cg.id = ctg.challenge_group_id
			ORDER BY				c.name,
											cg.value
		) AS result_to_return;
$$
LANGUAGE sql;
