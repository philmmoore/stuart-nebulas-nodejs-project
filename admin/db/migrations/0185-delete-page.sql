CREATE OR REPLACE FUNCTION sp_gymfit_admin_page_delete
(
	_id INT
)
RETURNS JSON AS
$$
	BEGIN
		DELETE FROM page where id = _id;

		RETURN to_json(1);
	END
$$
LANGUAGE plpgsql;

