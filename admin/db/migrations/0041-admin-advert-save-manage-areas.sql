DROP FUNCTION sp_gymfit_admin_advert_save(INT, INT, TEXT, TEXT);

CREATE FUNCTION sp_gymfit_admin_advert_save
(
	_countryId INT,
	_advertId INT,
	_name TEXT,
	_link TEXT,
	_areas INT[]
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	advert
		SET			name = _name,
						link = _link
		WHERE		id = _advertId;

		IF FOUND THEN
			DELETE FROM	advert_to_area
			WHERE				advert_id = _advertId;
		ELSE
			INSERT INTO advert (country, name, link)
			VALUES			(_countryId, _name, _link)
			RETURNING		id
			INTO				_advertId;
		END IF;

		INSERT INTO		advert_to_area (advert_id, area_id)
		SELECT				_advertId,
									area
		FROM					unnest(_areas) AS area;

		RETURN to_json(_advertId);
	END;
$$
LANGUAGE plpgsql;
