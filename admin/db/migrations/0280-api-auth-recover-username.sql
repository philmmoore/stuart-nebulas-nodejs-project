CREATE OR REPLACE FUNCTION sp_gymfit_api_auth_recover_username(_userid integer)
  RETURNS json AS
$BODY$

SELECT to_json(result_to_return)
  FROM
  (
    SELECT          email, username
    FROM            "user"
    WHERE           id = _userId
  ) AS result_to_return;

$BODY$
  LANGUAGE sql VOLATILE
  COST 100;
