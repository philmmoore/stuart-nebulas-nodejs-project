CREATE OR REPLACE FUNCTION sp_gymfit_api_auth_change_password
(
  _user_id INT,
  _current_hash BYTEA,
  _new_hash BYTEA,
  _device_id TEXT
)
RETURNS JSON AS
$$
  DECLARE
    _found_user_id INT;
    
  BEGIN
    UPDATE      "user"
    SET         hash = _new_hash
    WHERE       id = _user_id
    AND         hash = _current_hash
    RETURNING   id
    INTO        _found_user_id;
    
    INSERT INTO	user_audit (user_id, activity, message)
    SELECT      _found_user_id,
                'app password change',
                _device_id
    FROM        "user"
    WHERE       id = _found_user_id;

    DELETE
    FROM        user_password_reset
    WHERE       user_id = _found_user_id;
    
    RETURN row_to_json(result)
    FROM
    (
        SELECT  email
        FROM    "user"
        WHERE   id = _found_user_id
    ) AS result;
  END
$$
LANGUAGE plpgsql;