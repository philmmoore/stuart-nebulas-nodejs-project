CREATE FUNCTION sp_gymfit_site_user_associate_with_district
(
	_user_id INT,
	_district_id INT
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO	user_district (user_id, district_id)
		VALUES (_user_id, _district_id);

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;
