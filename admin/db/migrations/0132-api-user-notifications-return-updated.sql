CREATE OR REPLACE FUNCTION sp_gymfit_api_user_acknowledge_notification
(
	_user_id INT,
	_notification_id INT
)
RETURNS JSON AS
$$
  UPDATE			user_notification
  SET					acknowledged = current_timestamp
  WHERE				user_id = _user_id
  AND					notification_id = _notification_id;

	SELECT to_json((
		SELECT	COUNT(*)
		FROM 		user_notification
		WHERE		user_id = _user_id
  	AND			notification_id = _notification_id
	));
$$
LANGUAGE sql;
