CREATE OR REPLACE FUNCTION sp_gymfit_api_leaderboard_search
(
	_challenge_id INT,
	_challenge_group_id INT,
	_district_id INT,
	_county_id INT,
	_region_id INT,
	_country_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_last_period_start TIMESTAMP,
	_last_period_end TIMESTAMP,
	_results_per_page INT,
	_page INT,
	_is_male BIT,
	_age_min SMALLINT,
	_age_max SMALLINT,
	_weight_min INT,
	_weight_max INT,
	_only_validated BIT
)
RETURNS JSON AS
$$
	BEGIN
		RETURN sp_gymfit_site_leaderboard_search
		(
			_challenge_id,
			_challenge_group_id,
			_district_id,
			_county_id,
			_region_id,
			_country_id,
			_this_period_start,
			_this_period_end,
			_last_period_start,
			_last_period_end,
			_results_per_page,
			_page,
			_is_male,
			_age_min,
			_age_max,
			_weight_min,
			_weight_max,
			_only_validated
		);
	END
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_leaderboard_user_position
(
	_challenge_id INT,
	_challenge_group_id INT,
	_district_id INT,
	_county_id INT,
	_region_id INT,
	_country_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_is_male BIT,
	_age_min SMALLINT,
	_age_max SMALLINT,
	_weight_min INT,
	_weight_max INT,
	_only_validated BIT
)
RETURNS JSON AS
$$
	BEGIN
		RETURN sp_gymfit_site_leaderboard_user_position
		(
			_challenge_id,
			_challenge_group_id,
			_district_id,
			_county_id,
			_region_id,
			_country_id,
			_this_period_start,
			_this_period_end,
			_is_male,
			_age_min,
			_age_max,
			_weight_min,
			_weight_max,
			_only_validated
		);
	END
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_configuration_get
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT	configuration
	FROM		configuration
	WHERE		country_id = _country_id
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_challenge_get_groups
(
	_challenge_id INT,
  _user_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
		(
			SELECT
							array
							(
                  SELECT row_to_json(a)
                  FROM (
                          SELECT			cg.id,
                                      cg.value AS "name"
                          FROM				challenge_group AS cg
													INNER JOIN	challenge_to_challenge_group AS ccg ON ccg.challenge_group_id = cg.id
													WHERE				ccg.challenge_id = c.id
                  ) AS a
              ) AS "groups",
              (
                SELECT    challenge_group_id
                FROM      result
                WHERE     user_id = _user_id
                AND       challenge_id = _challenge_id
                ORDER BY  submitted DESC
                LIMIT     1
              ) AS "selectedGroup"
			FROM		challenge AS c
			WHERE		c.id = _challenge_id
		) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_location_get_all_names()
RETURNS JSON AS
$$
	SELECT array_to_json(array(
		SELECT		name
		FROM			district
		UNION
		SELECT		name
		FROM			county
		UNION
		SELECT		name
		FROM			region
		UNION
		SELECT		name
		FROM			country
	));
$$
LANGUAGE sql;
