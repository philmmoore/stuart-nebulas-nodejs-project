CREATE TABLE configuration (
	country_id INT REFERENCES country (id) PRIMARY KEY,
	configuration JSON
);

insert into configuration(country_id, configuration) VALUES(1, '{"leaderboard":{"updateFrequency":[7]}}');

CREATE OR REPLACE FUNCTION sp_gymfit_admin_configuration_get
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT	configuration
	FROM		configuration
	WHERE		country_id = _country_id
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_configuration_set
(
	_country_id INT,
	_configuration JSON
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	configuration
		SET			configuration=_configuration
		WHERE		country_id = _country_id;

		RETURN to_json(1);
	END
$$
LANGUAGE plpgsql;
