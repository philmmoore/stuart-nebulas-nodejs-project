CREATE EXTENSION pgcrypto;

CREATE TABLE user_password_reset (
	key UUID PRIMARY KEY DEFAULT gen_random_uuid(),
	user_id INTEGER REFERENCES "user" (id) NOT NULL,
	created TIMESTAMP NOT NULL default current_timestamp
);

CREATE FUNCTION sp_gymfit_site_user_password_reset
(
	_email TEXT
)
RETURNS JSON AS
$$
DECLARE
	_key UUID;
BEGIN
	DELETE
	FROM				user_password_reset
	WHERE				user_id = (SELECT id FROM "user" where email = _email);

	INSERT INTO	user_password_reset (user_id)
	SELECT			id
	FROM				"user"
	WHERE				email = _email
	RETURNING		key
	INTO				_key;

	RETURN to_json(_key);
END
$$
LANGUAGE plpgsql;
