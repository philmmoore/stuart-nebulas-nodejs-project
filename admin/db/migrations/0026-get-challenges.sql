CREATE OR REPLACE FUNCTION sp_site_get_challenges()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))	FROM (
		SELECT		name,
							category - 1 AS category
		FROM			challenge
		ORDER BY 	name
	) AS result;
$$
LANGUAGE sql;
