CREATE OR REPLACE FUNCTION sp_gymfit_site_user_get
(
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		SELECT					u.id,
										u.username,
										u.first_name AS "firstName",
										u.last_name AS "lastName",
										u.is_male AS "isMale",
										u.date_of_birth AS "dateOfBirth",
										u.avatar_uploaded AS "avatarUploaded",
                    u.is_personal_trainer AS "isPersonalTrainer",
										(
											SELECT row_to_json(result_location)
											FROM
											(
												SELECT
												d.id AS "districtId",
												d.name AS "districtName",
												c.id AS "countyId",
												c.name AS "countyName",
												r.id AS "regionId",
												r.name AS "regionName",
												ct.id AS "countryId",
												ct.name AS "countryName",
												ct.code AS "countryCode"
											) AS result_location
										) AS "location",
			              (
											SELECT		height
											FROM 			user_height
											WHERE 		user_id = _user_id
											ORDER BY	created DESC
											LIMIT 1
										) AS height,
										(
											SELECT		weight
											FROM 			user_weight
											WHERE 		user_id = _user_id
											ORDER BY	created DESC
											LIMIT 1
										) AS weight,
			              rt.value AS "routineType",
										g.value AS "gym",
										array
										(
			                  SELECT row_to_json(a)
			                  FROM
												(
													SELECT 					follower.id,
																					follower.username,
																					follower.first_name AS "firstName",
																					follower.last_name AS "lastName",
																					follower.avatar_uploaded AS "avatarUploaded"
													FROM						"user" AS follower
													JOIN						user_follower AS uf ON uf.follower_user_id = follower.id
													WHERE						uf.user_id = _user_id
													ORDER BY				uf.created DESC
			                  ) AS a
			              ) AS "followers",
										array
										(
			                  SELECT row_to_json(a)
			                  FROM
												(
													SELECT 					following.id,
																					following.username,
																					following.first_name AS "firstName",
																					following.last_name AS "lastName",
																					following.avatar_uploaded AS "avatarUploaded"
													FROM						"user" AS following
													JOIN						user_follower AS uf on uf.user_id = following.id
													WHERE						uf.follower_user_id = _user_id
													ORDER BY				uf.created DESC
			                  ) AS a
			              ) AS "following",
										array
										(
			                  SELECT row_to_json(a)
			                  FROM
												(
													SELECT 					client.id,
																					client.username,
																					client.first_name AS "firstName",
																					client.last_name AS "lastName",
																					client.avatar_uploaded AS "avatarUploaded"
													FROM						"user" AS client
													JOIN						user_client AS uf on uf.client_user_id = client.id
													WHERE						uf.user_id = _user_id
													ORDER BY				uf.created DESC
			                  ) AS a
			              ) AS "clients",
										array
										(
			                  SELECT row_to_json(a)
			                  FROM
												(
													SELECT 					trainer.id,
																					trainer.username,
																					trainer.first_name AS "firstName",
																					trainer.last_name AS "lastName",
																					trainer.avatar_uploaded AS "avatarUploaded"
													FROM						"user" AS trainer
													JOIN						user_client AS uf on uf.user_id = trainer.id
													WHERE						uf.client_user_id = _user_id
													ORDER BY				uf.created DESC
			                  ) AS a
			              ) AS "trainers",
										u.blocked
		FROM						"user" AS u
		LEFT OUTER JOIN	user_district AS ud
		ON							u.id = ud.user_id
		LEFT OUTER JOIN	user_region AS ur
		ON							u.id = ur.user_id
		LEFT OUTER JOIN	district AS d
		ON							ud.district_id = d.id
		LEFT OUTER JOIN	county AS c
		ON							d.county_id = c.id
		LEFT OUTER JOIN region AS r
		ON							ur.region_id = r.id
		LEFT OUTER JOIN	country AS ct
		ON							r.country_id = ct.id
    LEFT OUTER JOIN user_routine_type AS urt
    ON              u.id = urt.user_id
    LEFT OUTER JOIN routine_type AS rt
    ON              urt.routine_type_id = rt.id
		LEFT OUTER JOIN user_gym AS ug
    ON              u.id = ug.user_id
		LEFT OUTER JOIN gym AS g
    ON              g.id = ug.gym_id
		WHERE						u.id = _user_id
		ORDER BY				ud.created DESC,
										ur.created DESC
		LIMIT						1
	) AS result_to_return;
$$
LANGUAGE sql;

ALTER TABLE user_audit ADD COLUMN session TEXT;

DROP FUNCTION sp_gymfit_site_auth_login(TEXT, BYTEA, TEXT);

CREATE OR REPLACE FUNCTION sp_gymfit_site_auth_login
(
	_username_or_email TEXT,
	_hash BYTEA,
	_ip_address TEXT,
	_session TEXT
)
RETURNS JSON AS
$$
	DECLARE
  _user_id INT;

	BEGIN
		SELECT			id
		INTO				_user_id
		FROM				"user"
		WHERE				hash = _hash
		AND					(LOWER(username) = LOWER(_username_or_email) OR LOWER(email) = LOWER(_username_or_email));

		IF _user_id IS NOT NULL THEN
			INSERT INTO	user_audit (user_id, activity, message, session)
			VALUES			(_user_id,
									'login',
									_ip_address,
									_session);

			DELETE
			FROM				user_password_reset
			WHERE				user_id = _user_id;
		END IF;

		RETURN sp_gymfit_site_user_get(_user_id);
	END
$$
LANGUAGE plpgsql;

DROP FUNCTION sp_gymfit_site_auth_backdoor_login(TEXT, TEXT);

CREATE OR REPLACE FUNCTION sp_gymfit_site_auth_backdoor_login
(
	_username_or_email TEXT,
	_ip_address TEXT,
	_session TEXT
)
RETURNS JSON AS
$$
	DECLARE
  _user_id INT;

	BEGIN
		SELECT			id
		INTO				_user_id
		FROM				"user"
		WHERE				(LOWER(username) = LOWER(_username_or_email) OR LOWER(email) = LOWER(_username_or_email));

		IF _user_id IS NOT NULL THEN
			INSERT INTO	user_audit (user_id, activity, message, session)
			VALUES			(_user_id,
									'backdoor login',
									_ip_address,
									_session);
		END IF;

		RETURN sp_gymfit_site_user_get(_user_id);
	END
$$
LANGUAGE plpgsql;

DROP FUNCTION sp_gymfit_site_auth_update_password(INT, BYTEA);

CREATE OR REPLACE FUNCTION sp_gymfit_site_auth_update_password
(
	_user_id INT,
	_hash BYTEA,
	_ip_address TEXT,
	_session TEXT
)
RETURNS JSON AS
$$
	BEGIN
		DELETE
		FROM				user_password_reset
		WHERE				user_id = _user_id;

		UPDATE			"user" SET hash = _hash
		WHERE				id = _user_id;
		
		INSERT INTO	user_audit (user_id, activity, message, session)
		VALUES			(_user_id,
								'password reset',
								_ip_address,
								_session);

		RETURN sp_gymfit_site_user_get(_user_id);
	END
$$
LANGUAGE plpgsql;

DROP FUNCTION sp_gymfit_site_auth_confirm_email(UUID);

CREATE OR REPLACE FUNCTION sp_gymfit_site_auth_confirm_email
(
	_key UUID,
	_ip_address TEXT,
	_session TEXT
)
RETURNS JSON AS
$$
	DECLARE
		_user_id INT;
	BEGIN
		DELETE
		FROM				user_email_verification AS ev
		USING				"user" AS u
		WHERE				ev.user_id = u.id
		AND					ev.key = _key
		RETURNING		u.id
		INTO				_user_id;

		UPDATE			user_notification
		SET					acknowledged = current_timestamp
		WHERE				user_id = _user_id
		AND					notification_id = 2;
		
		INSERT INTO	user_audit (user_id, activity, message, session)
		VALUES			(_user_id,
								'confirm email',
								_ip_address,
								_session);

		RETURN sp_gymfit_site_user_get(_user_id);
	END
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_user_get_sessions
(
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
		(
			SELECT			session
			FROM				user_audit
			WHERE				user_id = _user_id
		) AS result_to_return;
$$
LANGUAGE sql;