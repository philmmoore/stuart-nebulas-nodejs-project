CREATE OR REPLACE FUNCTION sp_gymfit_site_user_get(_user_id integer)
 RETURNS json
 LANGUAGE sql
AS $function$
	SELECT row_to_json(result_to_return)
	FROM
	(
		SELECT					u.id,
										u.username,
										u.first_name AS "firstName",
										u.last_name AS "lastName",
										u.mobile_number AS "mobileNumber",
										u.is_male AS "isMale",
										u.date_of_birth AS "dateOfBirth",
										u.avatar_uploaded AS "avatarUploaded",
				                    u.is_personal_trainer AS "isPersonalTrainer",
										(
											SELECT row_to_json(result_location)
											FROM
											(
												SELECT
												d.id AS "districtId",
												d.name AS "districtName",
												c.id AS "countyId",
												c.name AS "countyName",
												r.id AS "regionId",
												r.name AS "regionName",
												ct.id AS "countryId",
												ct.name AS "countryName",
												ct.code AS "countryCode"
											) AS result_location
										) AS "location",
			              (
											SELECT		height
											FROM 			user_height
											WHERE 		user_id = _user_id
											ORDER BY	created DESC
											LIMIT 1
										) AS height,
										(
											SELECT		weight
											FROM 			user_weight
											WHERE 		user_id = _user_id
											ORDER BY	created DESC
											LIMIT 1
										) AS weight,
			              rt.value AS "routineType",
										u.gym,
										array
										(
			                  SELECT row_to_json(a)
			                  FROM
												(
													SELECT 					follower.id,
																					follower.username,
																					follower.first_name AS "firstName",
																					follower.last_name AS "lastName",
																					follower.avatar_uploaded AS "avatarUploaded"
													FROM						"user" AS follower
													JOIN						user_follower AS uf ON uf.follower_user_id = follower.id
													WHERE						uf.user_id = _user_id
													ORDER BY				uf.created DESC
			                  ) AS a
			              ) AS "followers",
										array
										(
			                  SELECT row_to_json(a)
			                  FROM
												(
													SELECT 					following.id,
																					following.username,
																					following.first_name AS "firstName",
																					following.last_name AS "lastName",
																					following.avatar_uploaded AS "avatarUploaded"
													FROM						"user" AS following
													JOIN						user_follower AS uf on uf.user_id = following.id
													WHERE						uf.follower_user_id = _user_id
													ORDER BY				uf.created DESC
			                  ) AS a
			              ) AS "following",
										array
										(
			                  SELECT row_to_json(a)
			                  FROM
												(
													SELECT 					client.id,
																					client.username,
																					client.first_name AS "firstName",
																					client.last_name AS "lastName",
																					client.avatar_uploaded AS "avatarUploaded"
													FROM						"user" AS client
													JOIN						user_client AS uf on uf.client_user_id = client.id
													WHERE						uf.user_id = _user_id
													ORDER BY				uf.created DESC
			                  ) AS a
			              ) AS "clients",
										array
										(
			                  SELECT row_to_json(a)
			                  FROM
												(
													SELECT 					trainer.id,
																					trainer.username,
																					trainer.first_name AS "firstName",
																					trainer.last_name AS "lastName",
																					trainer.avatar_uploaded AS "avatarUploaded"
													FROM						"user" AS trainer
													JOIN						user_client AS uf on uf.user_id = trainer.id
													WHERE						uf.client_user_id = _user_id
													ORDER BY				uf.created DESC
			                  ) AS a
			              ) AS "trainers",
										u.blocked,
                    u.bio
		FROM						"user" AS u
		LEFT OUTER JOIN	user_district AS ud
		ON							u.id = ud.user_id
		LEFT OUTER JOIN	user_region AS ur
		ON							u.id = ur.user_id
		LEFT OUTER JOIN	district AS d
		ON							ud.district_id = d.id
		LEFT OUTER JOIN	county AS c
		ON							d.county_id = c.id
		LEFT OUTER JOIN region AS r
		ON							ur.region_id = r.id
		LEFT OUTER JOIN	country AS ct
		ON							r.country_id = ct.id
    LEFT OUTER JOIN user_routine_type AS urt
    ON              u.id = urt.user_id
    LEFT OUTER JOIN routine_type AS rt
    ON              urt.routine_type_id = rt.id
		WHERE						u.id = _user_id
		ORDER BY				ud.created DESC,
										ur.created DESC
		LIMIT						1
	) AS result_to_return;
$function$
