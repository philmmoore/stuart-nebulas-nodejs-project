CREATE OR REPLACE FUNCTION sp_gymfit_site_user_get
(
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		SELECT					u.id,
										u.username,
										u.first_name AS "firstName",
										u.last_name AS "lastName",
										u.is_male AS "isMale",
										u.date_of_birth AS "dateOfBirth",
										u.avatar_uploaded AS "avatarUploaded",
                    u.is_personal_trainer AS "isPersonalTrainer",
										(
											SELECT row_to_json(result_location)
											FROM
											(
												SELECT
												d.id AS "districtId",
												d.name AS "districtName",
												c.id AS "countyId",
												c.name AS "countyName",
												r.id AS "regionId",
												r.name AS "regionName",
												ct.id AS "countryId",
												ct.name AS "countryName",
												ct.code AS "countryCode"
											) AS result_location
										) AS "location",
			              (
											SELECT		height
											FROM 			user_height
											WHERE 		user_id = _user_id
											ORDER BY	created DESC
											LIMIT 1
										) AS height,
										(
											SELECT		weight
											FROM 			user_weight
											WHERE 		user_id = _user_id
											ORDER BY	created DESC
											LIMIT 1
										) AS weight,
			              rt.value AS "routineType",
										u.gym,
										array
										(
			                  SELECT row_to_json(a)
			                  FROM
												(
													SELECT 					follower.id,
																					follower.username,
																					follower.first_name AS "firstName",
																					follower.last_name AS "lastName",
																					follower.avatar_uploaded AS "avatarUploaded"
													FROM						"user" AS follower
													JOIN						user_follower AS uf ON uf.follower_user_id = follower.id
													WHERE						uf.user_id = _user_id
													ORDER BY				uf.created DESC
			                  ) AS a
			              ) AS "followers",
										array
										(
			                  SELECT row_to_json(a)
			                  FROM
												(
													SELECT 					following.id,
																					following.username,
																					following.first_name AS "firstName",
																					following.last_name AS "lastName",
																					following.avatar_uploaded AS "avatarUploaded"
													FROM						"user" AS following
													JOIN						user_follower AS uf on uf.user_id = following.id
													WHERE						uf.follower_user_id = _user_id
													ORDER BY				uf.created DESC
			                  ) AS a
			              ) AS "following",
										array
										(
			                  SELECT row_to_json(a)
			                  FROM
												(
													SELECT 					client.id,
																					client.username,
																					client.first_name AS "firstName",
																					client.last_name AS "lastName",
																					client.avatar_uploaded AS "avatarUploaded"
													FROM						"user" AS client
													JOIN						user_client AS uf on uf.client_user_id = client.id
													WHERE						uf.user_id = _user_id
													ORDER BY				uf.created DESC
			                  ) AS a
			              ) AS "clients",
										array
										(
			                  SELECT row_to_json(a)
			                  FROM
												(
													SELECT 					trainer.id,
																					trainer.username,
																					trainer.first_name AS "firstName",
																					trainer.last_name AS "lastName",
																					trainer.avatar_uploaded AS "avatarUploaded"
													FROM						"user" AS trainer
													JOIN						user_client AS uf on uf.user_id = trainer.id
													WHERE						uf.client_user_id = _user_id
													ORDER BY				uf.created DESC
			                  ) AS a
			              ) AS "trainers",
										u.blocked,
                    u.bio
		FROM						"user" AS u
		LEFT OUTER JOIN	user_district AS ud
		ON							u.id = ud.user_id
		LEFT OUTER JOIN	user_region AS ur
		ON							u.id = ur.user_id
		LEFT OUTER JOIN	district AS d
		ON							ud.district_id = d.id
		LEFT OUTER JOIN	county AS c
		ON							d.county_id = c.id
		LEFT OUTER JOIN region AS r
		ON							ur.region_id = r.id
		LEFT OUTER JOIN	country AS ct
		ON							r.country_id = ct.id
    LEFT OUTER JOIN user_routine_type AS urt
    ON              u.id = urt.user_id
    LEFT OUTER JOIN routine_type AS rt
    ON              urt.routine_type_id = rt.id
		WHERE						u.id = _user_id
		ORDER BY				ud.created DESC,
										ur.created DESC
		LIMIT						1
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_search_user
(
	_search_query TEXT,
	_results_per_page INT,
	_page INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		SELECT
		(
			SELECT array_to_json(array_agg(results))
			FROM
			(
				SELECT					id,
												username,
												first_name AS "firstName",
												last_name AS "lastName",
												avatar_uploaded AS "avatarUploaded",
                        bio
				FROM						"user"
				WHERE						LOWER(username) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
				OR							LOWER(first_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
				OR							LOWER(last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
				OR							LOWER(first_name || last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
        ORDER BY        LOWER(first_name)
				OFFSET					(_page - 1) * _results_per_page
				LIMIT						_results_per_page
			) AS results
		) AS "results",
		(
			SELECT				ceiling(COUNT(id)::DECIMAL / _results_per_page)
			FROM						"user"
			WHERE						LOWER(username) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
			OR							LOWER(first_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
			OR							LOWER(last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
			OR							LOWER(first_name || last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
		) AS "numberOfPages"
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_user_search
(
	_search_query TEXT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
	(
		SELECT					id,
										username,
										first_name AS "firstName",
										last_name AS "lastName",
										avatar_uploaded AS "avatarUploaded",
                    bio
		FROM						"user"
		WHERE						LOWER(username) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
		OR							LOWER(first_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
		OR							LOWER(last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
		OR							LOWER(first_name || last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_result_get
(
	_result_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_previous_period_start TIMESTAMP,
	_previous_period_end TIMESTAMP
)
RETURNS JSON AS
$$	
	BEGIN
		RETURN row_to_json(result_to_return)
		FROM
		(
			SELECT
				c.id AS "challengeId",
				c.name AS "challengeName",
				cg.id AS "challengeGroupId",
				cg.value AS "challengeGroupName",
				cc.value AS "category",
				cc.id AS "categoryId",
				ct.measurement AS "measurement",
				
				ee.result,
				ee.submitted,
				ee.image_validated AS "imageValidated",
				ee.video_validated AS "videoValidated",
				
				u.id AS "userId",
				u.username,
				u.first_name AS "firstName",
				u.last_name AS "lastName",
				u.avatar_uploaded AS "avatarUploaded",
        u.bio,
				
				
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.district_id = ee.district_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "districtTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.county_id = ee.county_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "countyTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.region_id = ee.region_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "regionTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.country_id = ee.country_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "countryTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "worldTotal",
				
				
				
				
				
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM			result AS r
						WHERE			r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.district_id = ee.district_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "districtCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM			result AS r
						WHERE			r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.district_id = ee.district_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "districtPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.county_id = ee.county_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "countyCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.county_id = ee.county_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "countyPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.region_id = ee.region_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "regionCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.region_id = ee.region_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "regionPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.country_id = ee.country_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "countryCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.country_id = ee.country_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "countryPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "worldCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
			) AS "worldPrevious",
				
				
				
				
				
			(
				SELECT 		CASE WHEN ct.min_first = TRUE THEN
                    MIN(r.result)
									ELSE
										MAX(r.result)
									END
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				AND 			r.user_id = u.id
			) AS "result",
			
			(
				SELECT 		MAX(r.submitted)
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				AND 			r.user_id = u.id
			) AS "submitted"
				
				
				
			FROM 						result AS ee
			JOIN						"user" AS u ON u.id = ee.user_id
			JOIN 						challenge AS c ON ee.challenge_id = c.id
			JOIN 						challenge_group AS cg ON ee.challenge_group_id = cg.id
			JOIN 						challenge_type AS ct on c.type = ct.id
			JOIN 						challenge_category AS cc ON cc.id = c.category
			WHERE						ee.id = _result_id
      AND             c.hidden = FALSE

		) AS result_to_return;
	END
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_leaderboard_search
(
	_challenge_id INT,
	_challenge_group_id INT,
	_district_id INT,
	_county_id INT,
	_region_id INT,
	_country_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_last_period_start TIMESTAMP,
	_last_period_end TIMESTAMP,
	_results_per_page INT,
	_page INT,
	_is_male BIT,
	_age_min SMALLINT,
	_age_max SMALLINT,
	_weight_min INT,
	_weight_max INT,
	_only_validated BIT,
  _user_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		WITH filtered_results AS
		(
			SELECT				r.id,
										r.submitted,
										r.result,
										r.user_id,
										r.image_validated,
										r.video_validated,
										ct.measurement,
                    ct.min_first,
										u.username,
										u.avatar_uploaded,
										u.first_name,
										u.last_name,
                    u.bio,
                    sur.update_id
			FROM					result AS r
      JOIN          social_update_to_result AS sur ON sur.result_id = r.id
			JOIN					"user" AS u ON r.user_id = u.id
			JOIN 					challenge AS c ON c.id = r.challenge_id
			JOIN					challenge_type AS ct ON ct.id = c.type
			WHERE					r.submitted >= _last_period_start
			AND						r.submitted <= _this_period_end
			AND						r.challenge_id = _challenge_id
			AND						r.challenge_group_id = _challenge_group_id
			AND						(r.district_id = _district_id OR _district_id IS NULL)
			AND						(r.county_id = _county_id OR _county_id IS NULL)
			AND						(r.region_id = _region_id OR _region_id IS NULL)
			AND						(r.country_id = _country_id OR _country_id IS NULL)
			AND						(r.age >= _age_min OR _age_min IS NULL)
			AND						(r.age <= _age_max OR _age_max IS NULL)
			AND						(r.weight >= _weight_min OR _weight_min IS NULL)
			AND						(r.weight <= _weight_max OR _weight_max IS NULL)
			AND						((r.image_validated IS NOT NULL OR r.video_validated IS NOT NULL) OR _only_validated != '1')
			AND						(u.is_male = _is_male OR _is_male IS NULL)
			AND						r.excluded IS NULL
		)
		
		SELECT					(
											SELECT 				ceiling(COUNT(DISTINCT user_id)::DECIMAL / _results_per_page) AS "numberOfPages"
											FROM					filtered_results
											WHERE					submitted >= _this_period_start
										) AS "numberOfPages",
		
										(
											SELECT				measurement
											FROM					filtered_results
											LIMIT					1
										) AS "measurement",
										
										(
											SELECT array_to_json(array_agg(paged_results))
											FROM
											(
												
												
												
												
												WITH current_ranges AS
												(
													SELECT			row_number() over (ORDER BY MAX(frr.result) DESC) AS max_rank,
																			row_number() over (ORDER BY MIN(frr.result)) AS min_rank,
																			(
																				SELECT		id
																				FROM			filtered_results
																				WHERE 		result = MAX(frr.result)
																				AND				user_id = frr.user_id
																				ORDER BY 	video_validated DESC NULLS LAST,
																				 					image_validated DESC NULLS LAST,
																									submitted DESC
																				LIMIT 		1
																			) AS max_result_id,
																			(
																				SELECT 		id
																				FROM 			filtered_results
																				WHERE 		result = MIN(frr.result)
																				AND 			user_id = frr.user_id
																				ORDER BY 	video_validated DESC NULLS LAST,
																				 					image_validated DESC NULLS LAST,
																									submitted DESC
																				LIMIT 		1
																			) AS min_result_id,
																			frr.user_id
													FROM 				filtered_results AS frr
													WHERE				submitted >= _this_period_start
													GROUP BY		frr.user_id
												)
												
												SELECT				fr.id,
																			CASE
																				WHEN fr.min_first = TRUE THEN rg.min_rank
																				ELSE rg.max_rank
																			END AS "rank",
																			fr.result,
                                      fr.submitted,
																			fr.image_validated AS "imageValidated",
																			fr.video_validated AS "videoValidated",
																			fr.user_id AS "userId",
																			fr.username,
																			fr.avatar_uploaded AS "userHasAvatar",
									                    fr.first_name "firstName",
									                    fr.last_name "lastName",
                                      fr.bio,
                                      fr.update_id AS "updateId",
																			(
																				SELECT      COUNT(*)
								                      	FROM				social_update_like
																				WHERE				update_id = fr.update_id
																			) AS "likes",
																			(
                                        SELECT      COUNT(*)
								                      	FROM				social_update_flag
																				WHERE				update_id = fr.update_id
																			) AS "flags",
                                      CASE WHEN (SELECT TRUE FROM social_update_like WHERE user_id = _user_id AND update_id = fr.update_id) = TRUE THEN TRUE ELSE FALSE END AS "userLikes",
                                      CASE WHEN (SELECT TRUE FROM social_update_flag WHERE user_id = _user_id AND update_id = fr.update_id) = TRUE THEN TRUE ELSE FALSE END AS "userFlagged",
																			(
																				
																				
																				WITH last_rank AS
																				(
																					SELECT			lfr.user_id,
																											CASE
																												WHEN fr.min_first = TRUE THEN
                                                          row_number() over (ORDER BY MIN(lfr.result))
																												ELSE
																													row_number() over (ORDER BY MAX(lfr.result) DESC)
																											END AS last_rank_result
																					FROM 				filtered_results AS lfr
																					WHERE				lfr.submitted <= _last_period_end
																					GROUP BY		lfr.user_id
																				)
																				SELECT	last_rank_result
																				FROM		last_rank
																				WHERE		user_id = fr.user_id
																				LIMIT 1
																				
																				
																			) AS "lastRank"
												FROM					current_ranges AS rg
												JOIN					filtered_results AS fr ON fr.id = CASE
																																					WHEN fr.min_first = TRUE THEN rg.min_result_id
																																					ELSE rg.max_result_id
																																				END
												ORDER BY 		  rank
												OFFSET				(_page - 1) * _results_per_page
												LIMIT					_results_per_page
																															
																															
																															
                      ) AS paged_results
										) AS "results"
		
		FROM filtered_results
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_social_get_global_updates
(
  _user_id INT,
  _from_timestamp TIMESTAMP,
  _number_of_updates INT
)
RETURNS JSON AS
$$
  SELECT array_to_json(array_agg(result_to_return))
  FROM
  (
    SELECT        ud.id,
                  ud.created,
                  ud.user_id AS "userId",
                  ud.body,
                  u.username,
                  u.first_name AS "firstName",
                  u.last_name AS "lastName",
                  u.avatar_uploaded AS "avatarUploaded",
                  u.bio,
                  (SELECT COUNT(*) FROM social_update_like WHERE update_id = ud.id) AS "likes",
                  (SELECT COUNT(*) FROM social_update_flag WHERE update_id = ud.id) AS "flags",
                  (
                      SELECT row_to_json(a) AS "result"
                      FROM
                      (
                              SELECT      r.id,
                                          r.result,
                                          r.submitted,
                                          c.id AS "challengeId",
                                          c.name AS "challengeName",
                                          cg.id AS "challengeGroupId",
                                          cg.value AS "challengeGroupName",
                                          cc.value AS "category",
                                          cc.id AS "categoryId",
                                          ct.measurement AS "measurement",
                                          r.image_validated AS "imageValidated",
                                          r.video_validated AS "videoValidated"
                              FROM    result AS r
                              JOIN    social_update_to_result AS ur ON ur.result_id = r.id
                              JOIN    challenge AS c ON r.challenge_id = c.id
                              JOIN    challenge_group AS cg ON r.challenge_group_id = cg.id
                              JOIN    challenge_type AS ct on c.type = ct.id
                              JOIN    challenge_category AS cc ON cc.id = c.category
                              WHERE   ur.update_id = ud.id
                              AND     r.excluded IS NULL
                      ) AS a
                  ),
                  array
                  (
                    SELECT			id
                    FROM				social_update_video
                    WHERE				update_id = ud.id
                    AND         processed IS NOT NULL
                  ) AS "videos",
                  array
                  (
                    SELECT			id
                    FROM				social_update_image
                    WHERE				update_id = ud.id
                    AND         processed IS NOT NULL
                  ) AS "images",
                  array
                  (
                    SELECT      t.value
                    FROM				social_update_to_social_update_tag AS ut
                    JOIN        social_update_tag AS t ON ut.tag_id = t.id
                    WHERE				ut.update_id = ud.id
                  ) AS "tags",
                  CASE WHEN (SELECT TRUE FROM social_update_like WHERE user_id = _user_id AND update_id = ud.id) = TRUE THEN TRUE ELSE FALSE END AS "userLikes",
                  CASE WHEN (SELECT TRUE FROM social_update_flag WHERE user_id = _user_id AND update_id = ud.id) = TRUE THEN TRUE ELSE FALSE END AS "userFlagged"
    FROM          social_update AS ud
    JOIN          "user" AS u ON ud.user_id = u.id
    WHERE         ud.created < _from_timestamp
    AND           u.blocked IS NULL
    AND           ud.rejected IS NULL
    ORDER BY      ud.created DESC
    LIMIT         _number_of_updates
  ) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_social_get_user_updates
(
  _user_id INT,
  _from_timestamp TIMESTAMP,
  _number_of_updates INT
)
RETURNS JSON AS
$$
  SELECT array_to_json(array_agg(result_to_return))
  FROM
  (
    SELECT        ud.id,
                  ud.created,
                  ud.user_id AS "userId",
                  ud.body,
                  u.username,
                  u.first_name AS "firstName",
                  u.last_name AS "lastName",
                  u.avatar_uploaded AS "avatarUploaded",
                  u.bio,
                  (SELECT COUNT(*) FROM social_update_like WHERE update_id = ud.id) AS "likes",
                  (SELECT COUNT(*) FROM social_update_flag WHERE update_id = ud.id) AS "flags",
                  (
                      SELECT row_to_json(a) AS "result"
                      FROM
                      (
                              SELECT      r.id,
                                          r.result,
                                          r.submitted,
                                          c.id AS "challengeId",
                                          c.name AS "challengeName",
                                          cg.id AS "challengeGroupId",
                                          cg.value AS "challengeGroupName",
                                          cc.value AS "category",
                                          cc.id AS "categoryId",
                                          ct.measurement AS "measurement",
                                          r.image_validated AS "imageValidated",
                                          r.video_validated AS "videoValidated"
                              FROM    result AS r
                              JOIN    social_update_to_result AS ur ON ur.result_id = r.id
                              JOIN    challenge AS c ON r.challenge_id = c.id
                              JOIN    challenge_group AS cg ON r.challenge_group_id = cg.id
                              JOIN    challenge_type AS ct on c.type = ct.id
                              JOIN    challenge_category AS cc ON cc.id = c.category
                              WHERE   ur.update_id = ud.id
                              AND     r.excluded IS NULL
                      ) AS a
                  ),
                  array
                  (
                    SELECT			id
                    FROM				social_update_video
                    WHERE				update_id = ud.id
                    AND         processed IS NOT NULL
                  ) AS "videos",
                  array
                  (
                    SELECT			id
                    FROM				social_update_image
                    WHERE				update_id = ud.id
                    AND         processed IS NOT NULL
                  ) AS "images",
                  array
                  (
                    SELECT      t.value
                    FROM				social_update_to_social_update_tag AS ut
                    JOIN        social_update_tag AS t ON ut.tag_id = t.id
                    WHERE				ut.update_id = ud.id
                  ) AS "tags",
                  CASE WHEN (SELECT TRUE FROM social_update_like WHERE user_id = _user_id AND update_id = ud.id) = TRUE THEN TRUE ELSE FALSE END AS "userLikes",
                  CASE WHEN (SELECT TRUE FROM social_update_flag WHERE user_id = _user_id AND update_id = ud.id) = TRUE THEN TRUE ELSE FALSE END AS "userFlagged"
    FROM          social_update AS ud
    JOIN          "user" AS u ON ud.user_id = u.id
    WHERE         ud.created < _from_timestamp
    AND           u.blocked IS NULL
    AND           ud.rejected IS NULL
    AND
    (
      ud.user_id = _user_id
      OR						ud.user_id IN
                    (
                      SELECT client_user_id FROM user_client WHERE user_id = _user_id
                      UNION
                      SELECT user_id FROM user_follower WHERE follower_user_id = _user_id
                    )
    )
    ORDER BY      ud.created DESC
    LIMIT         _number_of_updates
  ) AS result_to_return;
$$
LANGUAGE sql;