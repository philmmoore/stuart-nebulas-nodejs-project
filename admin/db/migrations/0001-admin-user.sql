CREATE TABLE country (
  id SERIAL PRIMARY KEY,
  code CHARACTER(2) NOT NULL
);

CREATE TABLE admin_user (
  id SERIAL PRIMARY KEY,
  country INTEGER REFERENCES country (id) NOT NULL,
  username TEXT NOT NULL,
  password TEXT NOT NULL,
  created TIMESTAMP NOT NULL default current_timestamp,
  last_login TIMESTAMP
);

INSERT INTO country (code)
VALUES ('gb');

INSERT INTO admin_user (country, username, password)
VALUES (1, 'phil@wearehinge.com', 'password');
