CREATE OR REPLACE FUNCTION sp_gymfit_site_user_activity
(
	_user_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_previous_period_start TIMESTAMP,
	_previous_period_end TIMESTAMP
)
RETURNS JSON AS
$$
	DECLARE
		_district_id INT;
		_county_id INT;
		_region_id INT;
		_country_id INT;
		
	BEGIN
		SELECT		district_id
		FROM			user_district
		WHERE			user_id = _user_id
		ORDER BY	created DESC
		LIMIT			1
		INTO			_district_id;
		
		SELECT		county_id
		FROM			district
		WHERE			id = _district_id
		INTO			_county_id;
		
		SELECT		region_id
		FROM			county
		WHERE			id = _county_id
		INTO			_region_id;
		
		SELECT		country_id
		FROM			region
		WHERE			id = _region_id
		INTO			_country_id;

		RETURN array_to_json(array_agg(result_to_return))
		FROM
		(
			SELECT
				DISTINCT
				c.id AS "challengeId",
				c.name AS "challengeName",
				cg.id AS "challengeGroupId",
				cg.value AS "challengeGroupName",
				cc.value AS "category",
				cc.id AS "categoryId",
				ct.measurement AS "measurement",
				
				
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.district_id = _district_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "districtTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.county_id = _county_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "countyTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.region_id = _region_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "regionTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.country_id = _country_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "countryTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "worldTotal",
				
				
				
				
				
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.measurement = 'count' THEN
												row_number() over (ORDER BY MAX(r.result) DESC)
											ELSE
												row_number() over (ORDER BY MIN(r.result))
											END AS row_number,
											r.user_id
						FROM			result AS r
						WHERE			r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.district_id = _district_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "districtCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.measurement = 'count' THEN
												row_number() over (ORDER BY MAX(r.result) DESC)
											ELSE
												row_number() over (ORDER BY MIN(r.result))
											END AS row_number,
											r.user_id
						FROM			result AS r
						WHERE			r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.district_id = _district_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "districtPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.measurement = 'count' THEN
												row_number() over (ORDER BY MAX(r.result) DESC)
											ELSE
												row_number() over (ORDER BY MIN(r.result))
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.county_id = _county_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "countyCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.measurement = 'count' THEN
												row_number() over (ORDER BY MAX(r.result) DESC)
											ELSE
												row_number() over (ORDER BY MIN(r.result))
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.county_id = _county_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "countyPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.measurement = 'count' THEN
												row_number() over (ORDER BY MAX(r.result) DESC)
											ELSE
												row_number() over (ORDER BY MIN(r.result))
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.region_id = _region_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "regionCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.measurement = 'count' THEN
												row_number() over (ORDER BY MAX(r.result) DESC)
											ELSE
												row_number() over (ORDER BY MIN(r.result))
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.region_id = _region_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "regionPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.measurement = 'count' THEN
												row_number() over (ORDER BY MAX(r.result) DESC)
											ELSE
												row_number() over (ORDER BY MIN(r.result))
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.country_id = _country_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "countryCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.measurement = 'count' THEN
												row_number() over (ORDER BY MAX(r.result) DESC)
											ELSE
												row_number() over (ORDER BY MIN(r.result))
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.country_id = _country_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "countryPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.measurement = 'count' THEN
												row_number() over (ORDER BY MAX(r.result) DESC)
											ELSE
												row_number() over (ORDER BY MIN(r.result))
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "worldCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.measurement = 'count' THEN
												row_number() over (ORDER BY MAX(r.result) DESC)
											ELSE
												row_number() over (ORDER BY MIN(r.result))
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
			) AS "worldPrevious",
				
				
				
				
				
			(
				SELECT 		CASE WHEN ct.measurement = 'count' THEN
										MAX(r.result)
									ELSE
										MIN(r.result)
									END
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				AND 			r.user_id = _user_id
			) AS "result",
			
			(
				SELECT 		MAX(r.submitted)
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				AND 			r.user_id = _user_id
			) AS "submitted"
				
				
				
			FROM 						result AS ee
			JOIN 						challenge AS c ON ee.challenge_id = c.id
			JOIN 						challenge_group AS cg ON ee.challenge_group_id = cg.id
			JOIN 						challenge_type AS ct on c.type = ct.id
			JOIN 						challenge_category AS cc ON cc.id = c.category
			WHERE						ee.challenge_id IN (SELECT challenge_id FROM result WHERE user_id = _user_id)
			AND							ee.challenge_group_id IN (SELECT challenge_group_id FROM result WHERE user_id = _user_id)
			AND
			(
				SELECT COUNT(r.result)
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				AND 			r.user_id = _user_id
			) > 0
			ORDER BY				submitted DESC

		) AS result_to_return;
	END
$$
LANGUAGE plpgsql;