CREATE OR REPLACE FUNCTION sp_gymfit_site_configuration_get
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT	configuration
	FROM		configuration
	WHERE		country_id = _country_id
$$
LANGUAGE sql;
