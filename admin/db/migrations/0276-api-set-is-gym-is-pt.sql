CREATE OR REPLACE FUNCTION sp_gymfit_api_user_set_is_gym(
    _user_id integer,
    _is_gym boolean)
  RETURNS json AS
$BODY$
  UPDATE "user"
  SET   is_gym = _is_gym, 
        is_personal_trainer = false
  WHERE id = _user_id;
  SELECT  to_json(
    (
      SELECT  COUNT(id) = 1
      FROM    "user"
      WHERE   id = _user_id
    )
  );
$BODY$
  LANGUAGE sql VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION sp_gymfit_api_user_set_is_personal_trainer(
    _user_id integer,
    _is_pt boolean)
  RETURNS json AS
$BODY$
  UPDATE "user"
  SET   is_personal_trainer = _is_pt, 
        is_gym = false
  WHERE id = _user_id;
  SELECT  to_json(
    (
      SELECT  COUNT(id) = 1
      FROM    "user"
      WHERE   id = _user_id
    )
  );
$BODY$
  LANGUAGE sql VOLATILE
  COST 100;
