CREATE TABLE user_registration (
	id SERIAL PRIMARY KEY,
	created TIMESTAMP NOT NULL default current_timestamp,
	country INTEGER REFERENCES country (id) NOT NULL,
	email TEXT NOT NULL,
	hash BYTEA NOT NULL
);

CREATE FUNCTION sp_gymfit_site_user_registration_add_step_1_details
(
	_country_id INT,
	_email TEXT,
	_hash BYTEA
)
RETURNS JSON AS
$$
	DECLARE
		_registration_id INT;

	BEGIN
		INSERT INTO user_registration (country, email, hash)
		VALUES			(_country_id, _email, _hash)
		RETURNING		id
		INTO				_registration_id;

		RETURN to_json(_registration_id);
	END;
$$
LANGUAGE plpgsql;
