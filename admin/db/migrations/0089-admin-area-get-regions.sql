CREATE OR REPLACE FUNCTION sp_gymfit_admin_area_get_all_regions
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM
		(
			SELECT			r.id,
									r.name,
									(
										array
										(
											SELECT			c.name
											FROM				county AS c
											WHERE				c.region_id = r.id
											ORDER BY		c.name
										)
									) AS counties
			FROM				region as r
			WHERE				r.country_id = _country_id
			ORDER BY		r.name
		) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_area_get_all_counties
(
	_region_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
		(
			SELECT	r.id,
							r.name,
							(
								SELECT array_to_json(array_agg(result_b))
								FROM
									(
										SELECT			c.id,
																c.name,
																(
																	array
																	(
																		SELECT			d.name
																		FROM				district AS d
																		WHERE				d.county_id = c.id
																		ORDER BY		d.name
																	)
																) AS districts
										FROM				county as c
										WHERE				c.region_id = r.id
										ORDER BY		c.name
									) AS result_b
						) AS "counties"
		FROM 		region AS r
		WHERE		r.id = _region_id
	) AS result
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_area_get_all_districts
(
	_county_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
		(
			SELECT	c.id,
							c.name,
							(
								SELECT array_to_json(array_agg(result_b))
								FROM
									(
										SELECT			d.id,
																d.name
										FROM				district as d
										WHERE				d.county_id = c.id
										ORDER BY		d.name
									) AS result_b
						) AS "districts"
		FROM 		county AS c
		WHERE		c.id = _county_id
	) AS result
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_area_get_district
(
	_district_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
		(
			SELECT	d.id,
							d.name
			FROM 		district AS d
			WHERE		d.id = _district_id
	) AS result
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_area_save_region
(
	_country_id INT,
	_region_id INT,
	_name TEXT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	region
		SET			name = _name
		WHERE		id = _region_id;

		IF NOT FOUND THEN
			INSERT INTO region (country, name)
			VALUES			(_country_id, _name)
			RETURNING		id
			INTO				_region_id;
		END IF;

		RETURN to_json(_region_id);
	END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_area_save_county
(
	_region_id INT,
	_county_id INT,
	_name TEXT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	county
		SET			name = _name
		WHERE		id = _county_id;

		IF NOT FOUND THEN
			INSERT INTO county (region_id, name)
			VALUES			(_region_id, _name)
			RETURNING		id
			INTO				_county_id;
		END IF;

		RETURN to_json(_county_id);
	END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_area_save_district
(
	_county_id INT,
	_district_id INT,
	_name TEXT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	district
		SET			name = _name
		WHERE		id = _district_id;

		IF NOT FOUND THEN
			INSERT INTO district (county_id, name)
			VALUES			(_county_id, _name)
			RETURNING		id
			INTO				_district_id;
		END IF;

		RETURN to_json(_district_id);
	END;
$$
LANGUAGE plpgsql;
