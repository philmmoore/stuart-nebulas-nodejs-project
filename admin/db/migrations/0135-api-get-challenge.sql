CREATE OR REPLACE FUNCTION sp_gymfit_api_challenge_get_categories()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
		(
			SELECT	value AS "name",
							id
			FROM		challenge_category
		) AS result_to_return;
$$
LANGUAGE sql;
