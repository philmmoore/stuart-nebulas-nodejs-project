CREATE OR REPLACE FUNCTION sp_gymfit_site_user_get
(
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		SELECT					u.id,
										u.username,
										u.first_name AS "firstName",
										u.last_name AS "lastName",
										u.is_male AS "isMale",
										u.date_of_birth AS "dateOfBirth",
										u.avatar_uploaded AS "avatarUploaded",
                    u.is_personal_trainer AS "isPersonalTrainer",
										(
											SELECT row_to_json(result_location)
											FROM
											(
												SELECT
												d.id AS "districtId",
												d.name AS "districtName",
												c.id AS "countyId",
												c.name AS "countyName",
												r.id AS "regionId",
												r.name AS "regionName",
												ct.id AS "countryId",
												ct.name AS "countryName",
												ct.code AS "countryCode"
											) AS result_location
										) AS "location",
			              (
											SELECT		height
											FROM 			user_height
											WHERE 		user_id = _user_id
											ORDER BY	created DESC
											LIMIT 1
										) AS height,
										(
											SELECT		weight
											FROM 			user_weight
											WHERE 		user_id = _user_id
											ORDER BY	created DESC
											LIMIT 1
										) AS weight,
			              rt.value AS "routineType",
										g.value AS "gym" 
		FROM						"user" AS u
		LEFT OUTER JOIN	user_district AS ud
		ON							u.id = ud.user_id
		LEFT OUTER JOIN	user_region AS ur
		ON							u.id = ur.user_id
		LEFT OUTER JOIN	district AS d
		ON							ud.district_id = d.id
		LEFT OUTER JOIN	county AS c
		ON							d.county_id = c.id
		LEFT OUTER JOIN region AS r
		ON							ur.region_id = r.id
		LEFT OUTER JOIN	country AS ct
		ON							r.country_id = ct.id
    LEFT OUTER JOIN user_routine_type AS urt
    ON              u.id = urt.user_id
    LEFT OUTER JOIN routine_type AS rt
    ON              urt.routine_type_id = rt.id
		LEFT OUTER JOIN user_gym AS ug
    ON              u.id = ug.user_id
		LEFT OUTER JOIN gym AS g
    ON              g.id = ug.gym_id
		WHERE						u.id = _user_id
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_location_get_districts_for_region_id
(
	_region_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
	(
		SELECT			d.id,
								d.name
		FROM				district AS d
		INNER JOIN	county AS c on c.id = d.county_id
		INNER JOIN	region AS r on r.id = c.region_id
		WHERE				r.id = _region_id
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_set_location
(
	_user_id INT,
	_region_id INT,
	_district_id INT
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO	user_region (user_id, region_id)
		VALUES (_user_id, _region_id);
		
		INSERT INTO	user_district (user_id, district_id)
		VALUES (_user_id, _district_id);

		UPDATE			user_notification
		SET					acknowledged = current_timestamp
		WHERE				user_id = _user_id
		AND					notification_id = 3;

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_user_set_district
(
	_user_id INT,
	_district_id INT
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO	user_region (user_id, region_id)
		VALUES (_user_id, (	SELECT 			c.region_id
												FROM 				district AS d 
												INNER JOIN 	county AS c ON c.id = d.county_id
												WHERE 			d.id = _district_id));
	
		INSERT INTO	user_district (user_id, district_id)
		VALUES (_user_id, _district_id);

		UPDATE			user_notification
		SET					acknowledged = current_timestamp
		WHERE				user_id = _user_id
		AND					notification_id = 3;

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;
