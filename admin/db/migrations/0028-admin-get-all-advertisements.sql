CREATE FUNCTION sp_admin_get_advertisements
(
	_countryId INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))	FROM (
		SELECT 		a.id,
					 		a.name,
					 		a.link,
	          	(
								array
								(
									SELECT			ar.name
									FROM				area as ar
									INNER JOIN	advert_to_area AS aaa
		            	ON 					aaa.advert_id = a.id
		            	AND 				aaa.area_id = ar.id
									ORDER BY		ar.name
								)
	         		) AS areas
		FROM	 		advert AS a
		WHERE  		country = _countryId
		ORDER BY 	name
	) AS result;
$$
LANGUAGE sql;
