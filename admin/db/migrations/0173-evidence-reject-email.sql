ALTER TABLE email_template DROP CONSTRAINT email_template_pkey;
CREATE UNIQUE INDEX email_template_country_id ON email_template (country_id);
ALTER TABLE email_template ADD PRIMARY KEY USING INDEX email_template_country_id;
ALTER TABLE email_template DROP COLUMN id;

ALTER TABLE email_template ADD COLUMN result_rejected_subject TEXT NOT NULL DEFAULT 'Your recent result was rejected';
ALTER TABLE email_template ADD COLUMN result_rejected_html_body TEXT NOT NULL DEFAULT 'Your recent result was rejected. Challenge: {{CHALLENGE_NAME}} ({{CHALLENGE_GROUP}}), Result: {{RESULT_VALUE}}, Submitted: {{RESULT_DATE}}, Reason: {{REJECT_REASON}}';
ALTER TABLE email_template ADD COLUMN result_rejected_plain_body TEXT NOT NULL DEFAULT 'Your recent result was rejected. Challenge: {{CHALLENGE_NAME}} ({{CHALLENGE_GROUP}}), Result: {{RESULT_VALUE}}, Submitted: {{RESULT_DATE}}, Reason: {{REJECT_REASON}}';

DROP FUNCTION sp_gymfit_admin_email_template_set(INT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT, TEXT);

CREATE FUNCTION sp_gymfit_admin_email_template_set
(
	_country_id INT,
	_standard_template TEXT,
	_email_confirmation_subject TEXT,
	_email_confirmation_html_body TEXT,
	_email_confirmation_plain_body TEXT,
	_password_reset_subject TEXT,
	_password_reset_html_body TEXT,
	_password_reset_plain_body TEXT,
	_result_rejected_subject TEXT,
	_result_rejected_html_body TEXT,
	_result_rejected_plain_body TEXT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	email_template
		SET			standard_template = _standard_template,
						email_confirmation_subject = _email_confirmation_subject,
						email_confirmation_html_body = _email_confirmation_html_body,
						email_confirmation_plain_body = _email_confirmation_plain_body,
						password_reset_subject = _password_reset_subject,
						password_reset_html_body = _password_reset_html_body,
						password_reset_plain_body = _password_reset_plain_body,
						result_rejected_subject = _result_rejected_subject,
						result_rejected_html_body = _result_rejected_html_body,
						result_rejected_plain_body = _result_rejected_plain_body
		WHERE		country_id = _country_id;

		RETURN to_json(1);
	END
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_email_template_get
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)	FROM (
		SELECT	standard_template AS "standardTemplate",
						email_confirmation_subject AS "emailConfirmationSubject",
						email_confirmation_html_body AS "emailConfirmationHtmlBody",
						email_confirmation_plain_body AS "emailConfirmationPlainBody",
						password_reset_subject AS "passwordResetSubject",
						password_reset_html_body AS "passwordResetHtmlBody",
						password_reset_plain_body AS "passwordResetPlainBody",
						result_rejected_subject AS "resultRejectedSubject",
						result_rejected_html_body AS "resultRejectedHtmlBody",
						result_rejected_plain_body AS "resultRejectedPlainBody"
		FROM		email_template
		WHERE		country_id = _country_id
	) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_task_runner_email_template_get
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)	FROM (
		SELECT	standard_template AS "standardTemplate",
						email_confirmation_subject AS "emailConfirmationSubject",
						email_confirmation_html_body AS "emailConfirmationHtmlBody",
						email_confirmation_plain_body AS "emailConfirmationPlainBody",
						password_reset_subject AS "passwordResetSubject",
						password_reset_html_body AS "passwordResetHtmlBody",
						password_reset_plain_body AS "passwordResetPlainBody",
						result_rejected_subject AS "resultRejectedSubject",
						result_rejected_html_body AS "resultRejectedHtmlBody",
						result_rejected_plain_body AS "resultRejectedPlainBody"
		FROM		email_template
		WHERE		country_id = _country_id
	) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_result_get
(
	_result_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
		(
			SELECT					ROUND(100.0 * COUNT(CASE WHEN f.flag = TRUE THEN 1 END) / COUNT(f.flag), 2) AS "flagPercent",
											COUNT(CASE WHEN f.flag = TRUE THEN 1 END) AS "flagCount",
											COUNT(CASE WHEN f.flag = FALSE THEN 1 END) AS "likeCount",
											r.id AS "resultId",
											r.result,
											r.submitted,
											r.image_validated AS "imageValidated",
											r.video_validated AS "videoValidated",
											u.id AS "userId",
											u.username AS "username",
											u.email,
											c.name AS "challengeName",
											cg.value AS "challengeGroup",
											ct.measurement AS "measurement",
											(SELECT COUNT(*) FROM result WHERE excluded IS NOT NULL AND user_id = u.id) AS "numberOfExcludedResults"
			FROM						result_feedback AS f
			JOIN						result AS r on r.id = f.result_id
			JOIN						"user" AS u on u.id = r.user_id
			JOIN						challenge AS c on c.id = r.challenge_id
			JOIN						challenge_group AS cg on cg.id = r.challenge_group_id
			JOIN						challenge_type AS ct on ct.id = c.type
			WHERE						r.id = _result_id
			GROUP BY				r.id,
											u.id,
											c.id,
											cg.id,
											ct.id
		) AS result_to_return;
$$
LANGUAGE sql;
