DROP FUNCTION IF EXISTS sp_gymfit_api_result_get(INT);

CREATE OR REPLACE FUNCTION sp_gymfit_api_result_get
(
	_result_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_previous_period_start TIMESTAMP,
	_previous_period_end TIMESTAMP
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
		(
			SELECT					rs.result,
											rs.submitted,
											rs.image_validated AS "imageValidated",
											rs.video_validated AS "videoValidated",
											rs.challenge_id AS "challengeId",
											rs.challenge_group_id AS "challengeGroupId",
											rs.user_id AS "userId",
											u.username,
											u.first_name AS "firstName",
											u.last_name AS "lastName",
											u.avatar_uploaded AS "avatarUploaded",
											
											(SELECT xy.row_number FROM
												(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
												r.user_id
												FROM 			result AS r
												WHERE 		r.challenge_id = rs.challenge_id
												AND				r.submitted >= _this_period_start
												AND				r.submitted <= _this_period_end
												AND 			r.excluded IS NULL
												AND 			r.challenge_group_id = rs.challenge_group_id
												GROUP BY 	r.user_id) AS xy
											WHERE xy.user_id = u.id) AS "worldCurrent",
											
											(SELECT xy.row_number FROM
												(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
												r.user_id
												FROM 			result AS r
												WHERE 		r.challenge_id = rs.challenge_id
												AND				r.submitted >= _previous_period_start
												AND				r.submitted <= _previous_period_end
												AND 			r.excluded IS NULL
												AND 			r.challenge_group_id = rs.challenge_group_id
												GROUP BY 	r.user_id) AS xy
											WHERE xy.user_id = u.id) AS "worldPrevious",
											
											(SELECT COUNT(*) FROM
												(SELECT DISTINCT r.user_id
												FROM 			result AS r
												WHERE 		r.challenge_id = rs.challenge_id
												AND				r.submitted >= _this_period_start
												AND				r.submitted <= _this_period_end
												AND 			r.excluded IS NULL
												AND 			r.challenge_group_id = rs.challenge_group_id
												GROUP BY 	r.user_id) AS xy
											) AS "worldTotal",
											
											(SELECT xy.row_number FROM
												(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
												r.user_id
												FROM 			result AS r
												WHERE 		r.challenge_id = rs.challenge_id
												AND				r.submitted >= _this_period_start
												AND				r.submitted <= _this_period_end
												AND 			r.excluded IS NULL
												AND				r.country_id = (SELECT country_id FROM region WHERE id = (SELECT region_id FROM county WHERE id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = u.id ORDER BY created DESC LIMIT 1))))
												AND 			r.challenge_group_id = rs.challenge_group_id
												GROUP BY 	r.user_id) AS xy
											WHERE xy.user_id = u.id) AS "countryCurrent",
											
											(SELECT xy.row_number FROM
												(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
												r.user_id
												FROM 			result AS r
												WHERE 		r.challenge_id = rs.challenge_id
												AND				r.submitted >= _previous_period_start
												AND				r.submitted <= _previous_period_end
												AND 			r.excluded IS NULL
												AND				r.country_id = (SELECT country_id FROM region WHERE id = (SELECT region_id FROM county WHERE id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = u.id ORDER BY created DESC LIMIT 1))))
												AND 			r.challenge_group_id = rs.challenge_group_id
												GROUP BY 	r.user_id) AS xy
											WHERE xy.user_id = u.id) AS "countryPrevious",
											
											(SELECT COUNT(*) FROM
												(SELECT DISTINCT r.user_id
												FROM 			result AS r
												WHERE 		r.challenge_id = rs.challenge_id
												AND				r.submitted >= _this_period_start
												AND				r.submitted <= _this_period_end
												AND 			r.excluded IS NULL
												AND				r.country_id = (SELECT country_id FROM region WHERE id = (SELECT region_id FROM county WHERE id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = u.id ORDER BY created DESC LIMIT 1))))
												AND 			r.challenge_group_id = rs.challenge_group_id
												GROUP BY 	r.user_id) AS xy
											) AS "countryTotal",

											(SELECT xy.row_number FROM
												(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
												r.user_id
												FROM 			result AS r
												WHERE 		r.challenge_id = rs.challenge_id
												AND				r.submitted >= _this_period_start
												AND				r.submitted <= _this_period_end
												AND 			r.excluded IS NULL
												AND				r.region_id = (SELECT region_id FROM county WHERE id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = u.id ORDER BY created DESC LIMIT 1)))
												AND 			r.challenge_group_id = rs.challenge_group_id
												GROUP BY 	r.user_id) AS xy
											WHERE xy.user_id = u.id) as "regionCurrent",
											
											(SELECT xy.row_number FROM
												(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
												r.user_id
												FROM 			result AS r
												WHERE 		r.challenge_id = rs.challenge_id
												AND				r.submitted >= _previous_period_start
												AND				r.submitted <= _previous_period_end
												AND 			r.excluded IS NULL
												AND				r.region_id = (SELECT region_id FROM county WHERE id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = u.id ORDER BY created DESC LIMIT 1)))
												AND 			r.challenge_group_id = rs.challenge_group_id
												GROUP BY 	r.user_id) AS xy
											WHERE xy.user_id = u.id) as "regionPrevious",
											
											(SELECT COUNT(*) FROM
												(SELECT DISTINCT r.user_id
												FROM 			result AS r
												WHERE 		r.challenge_id = rs.challenge_id
												AND				r.submitted >= _this_period_start
												AND				r.submitted <= _this_period_end
												AND 			r.excluded IS NULL
												AND				r.region_id = (SELECT region_id FROM county WHERE id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = u.id ORDER BY created DESC LIMIT 1)))
												AND 			r.challenge_group_id = rs.challenge_group_id
												GROUP BY 	r.user_id) AS xy
											) AS "regionTotal",

											(SELECT xy.row_number FROM
												(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
												r.user_id
												FROM 			result AS r
												WHERE 		r.challenge_id = rs.challenge_id
												AND				r.submitted >= _this_period_start
												AND				r.submitted <= _this_period_end
												AND 			r.excluded IS NULL
												AND				r.county_id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = u.id ORDER BY created DESC LIMIT 1))
												AND 			r.challenge_group_id = rs.challenge_group_id
												GROUP BY 	r.user_id) AS xy
											WHERE xy.user_id = u.id) as "countyCurrent",
											
											(SELECT xy.row_number FROM
												(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
												r.user_id
												FROM 			result AS r
												WHERE 		r.challenge_id = rs.challenge_id
												AND				r.submitted >= _previous_period_start
												AND				r.submitted <= _previous_period_end
												AND 			r.excluded IS NULL
												AND				r.county_id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = u.id ORDER BY created DESC LIMIT 1))
												AND 			r.challenge_group_id = rs.challenge_group_id
												GROUP BY 	r.user_id) AS xy
											WHERE xy.user_id = u.id) as "countyPrevious",
											
											(SELECT COUNT(*) FROM
												(SELECT DISTINCT r.user_id
												FROM 			result AS r
												WHERE 		r.challenge_id = rs.challenge_id
												AND				r.submitted >= _this_period_start
												AND				r.submitted <= _this_period_end
												AND 			r.excluded IS NULL
												AND				r.county_id = (SELECT county_id FROM district WHERE id = (SELECT district_id FROM user_district WHERE user_id = u.id ORDER BY created DESC LIMIT 1))
												AND 			r.challenge_group_id = rs.challenge_group_id
												GROUP BY 	r.user_id) AS xy
											) AS "countyTotal",

											(SELECT xy.row_number FROM
												(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
												r.user_id
												FROM			result AS r
												WHERE			r.challenge_id = rs.challenge_id
												AND				r.submitted >= _this_period_start
												AND				r.submitted <= _this_period_end
												AND 			r.excluded IS NULL
												AND				r.district_id = (SELECT district_id FROM user_district WHERE user_id = u.id ORDER BY created DESC LIMIT 1)
												AND 			r.challenge_group_id = rs.challenge_group_id
												GROUP BY	r.user_id) AS xy
											WHERE xy.user_id = u.id) as "districtCurrent",
											
											(SELECT xy.row_number FROM
												(SELECT row_number() over (ORDER BY MAX(r.result) DESC),
												r.user_id
												FROM			result AS r
												WHERE			r.challenge_id = rs.challenge_id
												AND				r.submitted >= _previous_period_start
												AND				r.submitted <= _previous_period_end
												AND 			r.excluded IS NULL
												AND				r.district_id = (SELECT district_id FROM user_district WHERE user_id = u.id ORDER BY created DESC LIMIT 1)
												AND 			r.challenge_group_id = rs.challenge_group_id
												GROUP BY	r.user_id) AS xy
											WHERE xy.user_id = u.id) as "districtPrevious",
											
											(SELECT COUNT(*) FROM
												(SELECT DISTINCT r.user_id
												FROM 			result AS r
												WHERE 		r.challenge_id = rs.challenge_id
												AND				r.submitted >= _this_period_start
												AND				r.submitted <= _this_period_end
												AND 			r.excluded IS NULL
												AND				r.district_id = (SELECT district_id FROM user_district WHERE user_id = u.id ORDER BY created DESC LIMIT 1)
												AND 			r.challenge_group_id = rs.challenge_group_id
												GROUP BY 	r.user_id) AS xy
											) AS "districtTotal"
											
			FROM						result AS rs
			JOIN						"user" AS u ON u.id = rs.user_id
			WHERE						rs.id = _result_id
		) AS result_to_return;
$$
LANGUAGE sql;