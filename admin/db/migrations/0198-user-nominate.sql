CREATE TABLE user_nomination
(
	id SERIAL PRIMARY KEY,
	user_id INT NOT NULL,
	nominee_user_id INT NOT NULL,
	challenge_id INT NOT NULL,
	nominated TIMESTAMP DEFAULT current_timestamp NOT NULL,
	accepted TIMESTAMP
);

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_get_nominations
(
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
		(
			SELECT		c.name AS "challengeName",
								cc.value AS "challengeCategory",
								(
									SELECT
									(
										SELECT array_to_json(array_agg(nominator_list))
										FROM
										(
											SELECT	u.id AS "id",
															u.username,
															u.first_name AS "firstName",
															u.last_name AS "lastName",
															u.avatar_uploaded AS "avatarUploaded"
											FROM		"user" AS u
											JOIN		user_nomination AS un ON un.user_id = u.id
											WHERE		un.nominee_user_id = _user_id
											AND			un.challenge_id = c.id
										) AS nominator_list
									)
								) AS "nominators"
			FROM			user_nomination AS n
			JOIN			challenge AS c on c.id = n.challenge_id
			JOIN			challenge_category AS cc on cc.id = c.category
			WHERE			n.nominee_user_id = _user_id
			AND				n.accepted IS NULL
			GROUP BY	c.id,
								cc.id
		) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_get_nomination_suggestions
(
	_user_id INT,
	_challenge_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
		(
			(
				SELECT		u.id AS "id",
									u.username,
									u.first_name AS "firstName",
									u.last_name AS "lastName",
									u.avatar_uploaded AS "avatarUploaded"
				FROM			"user" AS u
				JOIN			user_client AS c ON c.user_id = u.id
				WHERE			c.client_user_id = _user_id
				AND				(SELECT id FROM result WHERE user_id = u.id AND challenge_id = _challenge_id LIMIT 1) IS NULL
				AND				(SELECT id FROM user_nomination WHERE user_id = u.id AND challenge_id = _challenge_id LIMIT 1) IS NULL
				ORDER BY	c.created DESC
			)
			UNION
			(
				SELECT		u.id AS "id",
									u.username,
									u.first_name AS "firstName",
									u.last_name AS "lastName",
									u.avatar_uploaded AS "avatarUploaded"
				FROM			"user" AS u
				JOIN			user_client AS c ON c.client_user_id = u.id
				WHERE			c.user_id = _user_id
				AND				(SELECT id FROM result WHERE user_id = u.id AND challenge_id = _challenge_id LIMIT 1) IS NULL
				AND				(SELECT id FROM user_nomination WHERE user_id = u.id AND challenge_id = _challenge_id LIMIT 1) IS NULL
				ORDER BY	c.created DESC
			)
			UNION
			(
				SELECT		u.id AS "id",
									u.username,
									u.first_name AS "firstName",
									u.last_name AS "lastName",
									u.avatar_uploaded AS "avatarUploaded"
				FROM			"user" AS u
				JOIN			user_follower AS f ON f.user_id = u.id
				WHERE			f.follower_user_id = _user_id
				AND				(SELECT id FROM result WHERE user_id = u.id AND challenge_id = _challenge_id LIMIT 1) IS NULL
				AND				(SELECT id FROM user_nomination WHERE user_id = u.id AND challenge_id = _challenge_id LIMIT 1) IS NULL
				ORDER BY	f.created DESC
			)
			UNION
			(
				SELECT		u.id AS "id",
									u.username,
									u.first_name AS "firstName",
									u.last_name AS "lastName",
									u.avatar_uploaded AS "avatarUploaded"
				FROM			"user" AS u
				JOIN			user_follower AS f ON f.follower_user_id = u.id
				WHERE			f.user_id = _user_id
				AND				(SELECT id FROM result WHERE user_id = u.id AND challenge_id = _challenge_id LIMIT 1) IS NULL
				AND				(SELECT id FROM user_nomination WHERE user_id = u.id AND challenge_id = _challenge_id LIMIT 1) IS NULL
				ORDER BY	f.created DESC
			)
		) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_nomination_get_possible_nominees
(
	_user_id INT,
	_challenge_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
		(
			SELECT array
			(
				SELECT	id
				FROM		"user" AS u
				WHERE 	u.id != _user_id
				AND   	(SELECT id FROM result WHERE user_id = u.id AND challenge_id = _challenge_id LIMIT 1) IS NULL
				AND   	(SELECT id FROM user_nomination WHERE user_id = u.id AND challenge_id = _challenge_id LIMIT 1) IS NULL
				AND			u.id != _user_id
			) AS "users"
		) AS result_to_return;
$$
LANGUAGE sql;