CREATE OR REPLACE FUNCTION sp_gymfit_site_leaderboard_search
(
	_challenge_id INT,
	_challenge_group_id INT,
	_district_id INT,
	_county_id INT,
	_region_id INT,
	_country_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_last_period_start TIMESTAMP,
	_last_period_end TIMESTAMP,
	_results_per_page INT,
	_page INT,
	_is_male BIT,
	_age_min SMALLINT,
	_age_max SMALLINT,
	_weight_min INT,
	_weight_max INT,
	_only_validated BIT,
  _user_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		WITH filtered_results AS
		(
			SELECT				r.id,
										r.submitted,
										r.result,
										r.user_id,
										r.image_validated,
										r.video_validated,
										ct.measurement,
                    ct.min_first,
										u.username,
										u.avatar_uploaded,
										u.first_name,
										u.last_name,
                    sur.update_id
			FROM					result AS r
      JOIN          social_update_to_result AS sur ON sur.result_id = r.id
			JOIN					"user" AS u ON r.user_id = u.id
			JOIN 					challenge AS c ON c.id = r.challenge_id
			JOIN					challenge_type AS ct ON ct.id = c.type
			WHERE					r.submitted >= _last_period_start
			AND						r.submitted <= _this_period_end
			AND						r.challenge_id = _challenge_id
			AND						r.challenge_group_id = _challenge_group_id
			AND						(r.district_id = _district_id OR _district_id IS NULL)
			AND						(r.county_id = _county_id OR _county_id IS NULL)
			AND						(r.region_id = _region_id OR _region_id IS NULL)
			AND						(r.country_id = _country_id OR _country_id IS NULL)
			AND						(r.age >= _age_min OR _age_min IS NULL)
			AND						(r.age <= _age_max OR _age_max IS NULL)
			AND						(r.weight >= _weight_min OR _weight_min IS NULL)
			AND						(r.weight <= _weight_max OR _weight_max IS NULL)
			AND						((r.image_validated IS NOT NULL OR r.video_validated IS NOT NULL) OR _only_validated != '1')
			AND						(u.is_male = _is_male OR _is_male IS NULL)
			AND						r.excluded IS NULL
		)
		
		SELECT					(
											SELECT 				ceiling(COUNT(DISTINCT user_id)::DECIMAL / _results_per_page) AS "numberOfPages"
											FROM					filtered_results
											WHERE					submitted >= _this_period_start
										) AS "numberOfPages",
		
										(
											SELECT				measurement
											FROM					filtered_results
											LIMIT					1
										) AS "measurement",
										
										(
											SELECT array_to_json(array_agg(paged_results))
											FROM
											(
												
												
												
												
												WITH current_ranges AS
												(
													SELECT			row_number() over (ORDER BY MAX(frr.result) DESC) AS max_rank,
																			row_number() over (ORDER BY MIN(frr.result)) AS min_rank,
																			(
																				SELECT		id
																				FROM			filtered_results
																				WHERE 		result = MAX(frr.result)
																				AND				user_id = frr.user_id
																				ORDER BY 	video_validated DESC NULLS LAST,
																				 					image_validated DESC NULLS LAST,
																									submitted DESC
																				LIMIT 		1
																			) AS max_result_id,
																			(
																				SELECT 		id
																				FROM 			filtered_results
																				WHERE 		result = MIN(frr.result)
																				AND 			user_id = frr.user_id
																				ORDER BY 	video_validated DESC NULLS LAST,
																				 					image_validated DESC NULLS LAST,
																									submitted DESC
																				LIMIT 		1
																			) AS min_result_id,
																			frr.user_id
													FROM 				filtered_results AS frr
													WHERE				submitted >= _this_period_start
													GROUP BY		frr.user_id
												)
												
												SELECT				fr.id,
																			CASE
																				WHEN fr.min_first = TRUE THEN rg.min_rank
																				ELSE rg.max_rank
																			END AS "rank",
																			fr.result,
                                      fr.submitted,
																			fr.image_validated AS "imageValidated",
																			fr.video_validated AS "videoValidated",
																			fr.user_id AS "userId",
																			fr.username,
																			fr.avatar_uploaded AS "userHasAvatar",
									                    fr.first_name "firstName",
									                    fr.last_name "lastName",
                                      fr.update_id AS "updateId",
																			(
																				SELECT      COUNT(*)
								                      	FROM				social_update_like
																				WHERE				update_id = fr.update_id
																			) AS "likes",
																			(
                                        SELECT      COUNT(*)
								                      	FROM				social_update_flag
																				WHERE				update_id = fr.update_id
																			) AS "flags",
                                      CASE WHEN (SELECT TRUE FROM social_update_like WHERE user_id = _user_id AND update_id = fr.update_id) = TRUE THEN TRUE ELSE FALSE END AS "userLikes",
                                      CASE WHEN (SELECT TRUE FROM social_update_flag WHERE user_id = _user_id AND update_id = fr.update_id) = TRUE THEN TRUE ELSE FALSE END AS "userFlagged",
																			(
																				
																				
																				WITH last_rank AS
																				(
																					SELECT			lfr.user_id,
																											CASE
																												WHEN fr.min_first = TRUE THEN
                                                          row_number() over (ORDER BY MIN(lfr.result))
																												ELSE
																													row_number() over (ORDER BY MAX(lfr.result) DESC)
																											END AS last_rank_result
																					FROM 				filtered_results AS lfr
																					WHERE				lfr.submitted <= _last_period_end
																					GROUP BY		lfr.user_id
																				)
																				SELECT	last_rank_result
																				FROM		last_rank
																				WHERE		user_id = fr.user_id
																				LIMIT 1
																				
																				
																			) AS "lastRank"
												FROM					current_ranges AS rg
												JOIN					filtered_results AS fr ON fr.id = CASE
																																					WHEN fr.min_first = TRUE THEN rg.min_result_id
																																					ELSE rg.max_result_id
																																				END
												ORDER BY 		  rank
												OFFSET				(_page - 1) * _results_per_page
												LIMIT					_results_per_page
																															
																															
																															
                      ) AS paged_results
										) AS "results"
		
		FROM filtered_results
	) AS result_to_return;
$$
LANGUAGE sql;

DROP FUNCTION IF EXISTS sp_gymfit_api_leaderboard_search(INT, INT, INT, INT, INT, INT, TIMESTAMP, TIMESTAMP, TIMESTAMP, TIMESTAMP, INT, INT, BIT, SMALLINT, SMALLINT, INT, INT, BIT);

CREATE OR REPLACE FUNCTION sp_gymfit_api_leaderboard_search
(
	_challenge_id INT,
	_challenge_group_id INT,
	_district_id INT,
	_county_id INT,
	_region_id INT,
	_country_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_last_period_start TIMESTAMP,
	_last_period_end TIMESTAMP,
	_results_per_page INT,
	_page INT,
	_is_male BIT,
	_age_min SMALLINT,
	_age_max SMALLINT,
	_weight_min INT,
	_weight_max INT,
	_only_validated BIT,
  _user_id INT
)
RETURNS JSON AS
$$
	BEGIN
		RETURN sp_gymfit_site_leaderboard_search
		(
			_challenge_id,
			_challenge_group_id,
			_district_id,
			_county_id,
			_region_id,
			_country_id,
			_this_period_start,
			_this_period_end,
			_last_period_start,
			_last_period_end,
			_results_per_page,
			_page,
			_is_male,
			_age_min,
			_age_max,
			_weight_min,
			_weight_max,
			_only_validated,
      _user_id
		);
	END
$$
LANGUAGE plpgsql;