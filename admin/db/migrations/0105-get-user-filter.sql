CREATE FUNCTION sp_gymfit_site_leaderboard_get_user_filter
(
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
		(
			SELECT			LOWER(REPLACE(ag.name, ' ', '')) AS age,
									(
		                SELECT    	LOWER(REPLACE(w.name, ' ', ''))
		                FROM 				weight_group AS w
		                INNER JOIN 	user_weight AS uw
										ON					w.min <= uw.weight
										AND					w.max >= uw.weight
		                WHERE     	user_id = u.id
		                ORDER BY  	created DESC
		                LIMIT     	1
									) AS weight,
									u.is_male AS "isMale"
			FROM 				"user" AS u
			INNER JOIN	age_group AS ag
			ON					ag.min <= extract(year FROM age(u.date_of_birth))
			AND					ag.max >= extract(year FROM age(u.date_of_birth))
			WHERE 			u.id = _user_id
		) AS result;
$$
LANGUAGE sql;
