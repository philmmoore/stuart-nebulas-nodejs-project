CREATE OR REPLACE FUNCTION sp_gymfit_api_user_set_district
(
	_user_id INT,
	_district_id INT
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO	user_district (user_id, district_id)
		VALUES (_user_id, _district_id);

		UPDATE			user_notification
		SET					acknowledged = current_timestamp
		WHERE				user_id = _user_id
		AND					notification_id = 3;

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_user_set_weight
(
	_user_id INT,
	_weight INT
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO	user_weight (user_id, weight)
		VALUES (_user_id, _weight);

		UPDATE			user_notification
		SET					acknowledged = current_timestamp
		WHERE				user_id = _user_id
		AND					notification_id = 4;

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION sp_gymfit_api_user_set_height
(
	_user_id INT,
	_height SMALLINT
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO	user_height (user_id, height)
		VALUES (_user_id, _height);

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;
