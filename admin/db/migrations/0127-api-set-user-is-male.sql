CREATE OR REPLACE FUNCTION sp_gymfit_api_user_set_is_male
(
	_user_id INT,
	_is_male BIT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	"user"
		SET			is_male = _is_male
		WHERE		id = _user_id;

		RETURN to_json((SELECT COUNT(*) FROM "user" WHERE id = _user_id)::int);
	END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_user_set_mobile
(
	_user_id INT,
	_mobile_number TEXT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	"user"
		SET			mobile_number = _mobile_number
		WHERE		id = _user_id;

		RETURN to_json((SELECT COUNT(*) FROM "user" WHERE id = _user_id)::int);
	END;
$$
LANGUAGE plpgsql;
