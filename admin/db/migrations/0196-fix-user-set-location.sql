CREATE OR REPLACE FUNCTION sp_gymfit_site_user_set_location
(
	_user_id INT,
	_region_id INT,
	_district_id INT
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO	user_region (user_id, region_id)
		SELECT 			_user_id,
								_region_id
		WHERE 			(SELECT region_id FROM user_region WHERE user_id = _user_id ORDER BY created DESC LIMIT 1) != _region_id
		OR					(SELECT region_id FROM user_region WHERE user_id = _user_id ORDER BY created DESC LIMIT 1) IS NULL;
		
		INSERT INTO	user_district (user_id, district_id)
		SELECT 			_user_id,
								_district_id
		WHERE 			(SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1) != _district_id
		OR					(SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1) IS NULL;

		UPDATE			user_notification
		SET					acknowledged = current_timestamp
		WHERE				user_id = _user_id
		AND					notification_id = 3
		AND					acknowledged IS NULL;

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;