TRUNCATE TABLE gym CASCADE;

INSERT INTO gym (country_id, value)
VALUES (1, 'Other'),
			 (1, 'LA Fitness'),
			 (1, 'Fitness First'),
			 (1, 'Gym World'),
			 (1, 'David Lloyd Leisure'),
			 (1, 'Cannons Health Club'),
			 (1, 'Esporta'),
			 (1, 'Next Generation Clubs'),
			 (1, 'Virgin Active Health Clubs'),
			 (1, 'Holmes Place Health Clubs'),
			 (1, 'LivingWell'),
			 (1, 'KissGyms'),
			 (1, 'Pure Gym'),
			 (1, 'DW Fitness'),
			 (1, 'Bannatynes'),
			 (1, 'Total Fitness'),
			 (1, 'Living Well'),
			 (1, 'Village Hotels'),
			 (1, 'Xerciser4less'),
 		 	 (1, 'The Gym Group');
