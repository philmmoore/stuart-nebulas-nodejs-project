ALTER TABLE "user" ADD is_male BIT;

DROP TABLE user_weight;

CREATE TABLE user_weight (
	id SERIAL PRIMARY KEY,
	created TIMESTAMP NOT NULL default current_timestamp,
	user_id INTEGER REFERENCES "user" (id) NOT NULL,
  weight INTEGER NOT NULL -- in grams, max: 2.14748e6kg (heaviest man = 635.03kg)
);

CREATE FUNCTION sp_gymfit_site_user_register_set_parameters
(
	_user_id INT,
	_is_male BIT,
	_height SMALLINT,
	_weight INT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE "user" set is_male = _is_male
		WHERE id = _user_id;

		INSERT INTO	user_height (user_id, height)
		VALUES (_user_id, _height);

		INSERT INTO	user_weight (user_id, weight)
		VALUES (_user_id, _weight);

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;
