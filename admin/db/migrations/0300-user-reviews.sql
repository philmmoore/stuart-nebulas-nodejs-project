-- Create the reviews table
CREATE TABLE IF NOT EXISTS user_reviews (
	id SERIAL,
	user_id INT REFERENCES "user" (id) NOT NULL,
	reviewer_user_id INT REFERENCES "user" (id) NOT NULL,
	title TEXT,
	body TEXT,
	rating INT,
	created TIMESTAMP NOT NULL DEFAULT current_timestamp,
	last_updated TIMESTAMP NOT NULL DEFAULT current_timestamp,
	PRIMARY KEY (id, user_id, reviewer_user_id)
);

-- Get all user reviews
CREATE OR REPLACE FUNCTION sp_gymfit_site_user_reviews(_user_id integer)
 RETURNS json
 LANGUAGE sql
AS $function$
   
	SELECT array_to_json(array_agg(result_to_return))
	FROM (
		SELECT 
			id,
			reviewer_user_id AS "reviewerUserId",
			title AS "title",
			body AS "body",
			rating AS "rating",
			created AS "created",
			last_updated AS "lastUpdated"
		FROM user_reviews
		WHERE user_id = _user_id
		ORDER BY last_updated DESC
	) as "result_to_return"

$function$;

-- Get all user reviews, number of reviews to return
CREATE OR REPLACE FUNCTION sp_gymfit_site_user_reviews(_user_id integer, _number_to_return integer)
 RETURNS json
 LANGUAGE sql
AS $function$
   
	SELECT array_to_json(array_agg(result_to_return))
	FROM (
		SELECT 
			id,
			reviewer_user_id AS "reviewerUserId",
			title AS "title",
			body AS "body",
			rating AS "rating",
			created AS "created",
			last_updated AS "lastUpdated"
		FROM user_reviews
		WHERE user_id = _user_id
		ORDER BY last_updated DESC
		LIMIT _number_to_return
	) as "result_to_return"

$function$;

-- Pluck a specific review
CREATE OR REPLACE FUNCTION sp_gymfit_site_user_review(_user_id integer, _review_id integer)
 RETURNS json
 LANGUAGE sql
AS $function$
   
	SELECT row_to_json(result_to_return)
	FROM (
		SELECT 
			id,
			reviewer_user_id AS "reviewerUserId",
			title AS "title",
			body AS "body",
			rating AS "rating",
			created AS "created",
			last_updated AS "lastUpdated"
		FROM user_reviews
		WHERE id = _review_id
		AND user_id = _user_id
		ORDER BY last_updated DESC
		LIMIT 1
	) as "result_to_return"

$function$;

-- Add a review for a user
CREATE OR REPLACE FUNCTION sp_gymfit_site_user_review_submit(_user_id integer, _reviewer_user_id integer, _title text, _body text, _rating integer)
 RETURNS json
 LANGUAGE plpgsql
AS $function$

	DECLARE
	_review_id INT;

	BEGIN

		-- Set default review id to 0
		_review_id = 0;

		-- Check its a valid user so we don't break constraints
		IF NOT EXISTS (SELECT 1 FROM "user" WHERE id = _user_id) THEN
			RETURN to_json(0);
		END IF;

		-- Check its a valid user so we don't break constraints
		IF NOT EXISTS (SELECT 1 FROM "user" WHERE id = _reviewer_user_id) THEN
			RETURN to_json(0);
		END IF;
		
		-- Check if a review already exists, if it does then we'll update the review rather than posting a new one
		SELECT id INTO _review_id FROM user_reviews WHERE user_id = _user_id AND reviewer_user_id = _reviewer_user_id;

		IF (_review_id > 0) THEN
			UPDATE user_reviews 
			SET	title = _title,
				body = _body,
				rating = _rating,
				last_updated = NOW()
			WHERE id = _review_id;
			RETURN sp_gymfit_api_user_review(_user_id, _review_id);
		ELSE

			INSERT INTO user_reviews
			(user_id, reviewer_user_id, title, body, rating, created, last_updated)
			VALUES
			(_user_id, _reviewer_user_id, _title, _body, _rating, NOW(), NOW())
			RETURNING id INTO _review_id;
			
			RETURN sp_gymfit_api_user_review(_user_id, _review_id);

		END IF;

	END;

$function$;

-- Delete a review for user
CREATE OR REPLACE FUNCTION sp_gymfit_site_user_review_delete(_user_id integer, _review_id integer)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
   
	BEGIN

		IF (SELECT 1 FROM user_reviews WHERE id = _review_id AND user_id = _user_id) THEN 
			DELETE FROM user_reviews WHERE id = _review_id AND user_id = _user_id;
			RETURN to_json(1);
		ELSE 
			RETURN to_json(0);
		END IF;

 	END;

$function$;

-- Count number of user reviews
CREATE OR REPLACE FUNCTION sp_gymfit_site_user_reviews_count(_user_id integer)
 RETURNS json
 LANGUAGE sql
AS $function$
   
	SELECT 
		to_json(count(1))
	FROM user_reviews
	WHERE user_id = _user_id;

$function$;

-- Review rating
CREATE OR REPLACE FUNCTION sp_gymfit_site_user_reviews_rating(_user_id integer)
 RETURNS json
 LANGUAGE sql
AS $function$
   
	SELECT to_json(round(sum(rating)/count(1)::numeric, 1))
	FROM user_reviews
	WHERE user_id = _user_id;

$function$;


-- API

CREATE OR REPLACE FUNCTION sp_gymfit_api_user_reviews(_user_id integer, _number_to_return integer)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
   
	BEGIN

		if (_number_to_return > 0) THEN 
			RETURN sp_gymfit_site_user_reviews(_user_id, _number_to_return);
		ELSE 
			RETURN sp_gymfit_site_user_reviews(_user_id);
		END IF;

	END;

$function$;

CREATE OR REPLACE FUNCTION sp_gymfit_api_user_review(_user_id integer, _review_id integer)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
   
	BEGIN
		RETURN sp_gymfit_site_user_review(_user_id, _review_id);
	END;

$function$;

CREATE OR REPLACE FUNCTION sp_gymfit_api_user_review_submit(_user_id integer, _reviewer_user_id integer, _title text, _body text, _rating integer)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
   
	BEGIN
		RETURN sp_gymfit_site_user_review_submit(_user_id, _reviewer_user_id, _title, _body, _rating);
	END;

$function$;




CREATE OR REPLACE FUNCTION sp_gymfit_api_user_review_delete(_user_id integer, _review_id integer)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
   
	BEGIN

		RETURN sp_gymfit_site_user_review_delete(_user_id, _review_id);

 	END;

$function$;



CREATE OR REPLACE FUNCTION sp_gymfit_site_user_get(_user_id integer)
 RETURNS json
 LANGUAGE sql
AS $function$
    SELECT row_to_json(result_to_return)
    FROM
    (
        SELECT                  u.id,
                                        u.username,
                                        u.first_name AS "firstName",
                                        u.last_name AS "lastName",
                                        u.mobile_number AS "mobileNumber",
                                        u.is_male AS "isMale",
                                        u.date_of_birth AS "dateOfBirth",
                                        u.avatar_uploaded AS "avatarUploaded",
                                    u.is_personal_trainer AS "isPersonalTrainer",
                                    u.is_gym AS "isGym",
										(
											SELECT row_to_json(reviews) FROM (
												SELECT 
													sp_gymfit_site_user_reviews_count(9) AS "count",
													sp_gymfit_site_user_reviews_rating(9) AS "rating"
											) AS reviews
										) AS "reviews",
                                        (
                                            SELECT row_to_json(result_location)
                                            FROM
                                            (
                                                SELECT
                                                d.id AS "districtId",
                                                d.name AS "districtName",
                                                d.latitude AS "districtLatitude",
                                                d.longitude AS "districtLongitude",
                                                c.id AS "countyId",
                                                c.name AS "countyName",
                                                r.id AS "regionId",
                                                r.name AS "regionName",
                                                r.latitude AS "regionLatitude",
                                                r.longitude AS "regionLongitude",
                                                ct.id AS "countryId",
                                                ct.name AS "countryName",
                                                ct.code AS "countryCode"
                                            ) AS result_location
                                        ) AS "location",
                          (
                                            SELECT      height
                                            FROM            user_height
                                            WHERE       user_id = _user_id
                                            ORDER BY    created DESC
                                            LIMIT 1
                                        ) AS height,
                                        (
                                            SELECT      weight
                                            FROM            user_weight
                                            WHERE       user_id = _user_id
                                            ORDER BY    created DESC
                                            LIMIT 1
                                        ) AS weight,
                          rt.value AS "routineType",
                                        u.gym,
                                        array
                                        (
                              SELECT row_to_json(a)
                              FROM
                                                (
                                                    SELECT                  follower.id,
                                                                                    follower.username,
                                                                                    follower.first_name AS "firstName",
                                                                                    follower.last_name AS "lastName",
                                                                                    follower.avatar_uploaded AS "avatarUploaded"
                                                    FROM                        "user" AS follower
                                                    JOIN                        user_follower AS uf ON uf.follower_user_id = follower.id
                                                    WHERE                       uf.user_id = _user_id
                                                    ORDER BY                uf.created DESC
                              ) AS a
                          ) AS "followers",
                                        array
                                        (
                              SELECT row_to_json(a)
                              FROM
                                                (
                                                    SELECT                  following.id,
                                                                                    following.username,
                                                                                    following.first_name AS "firstName",
                                                                                    following.last_name AS "lastName",
                                                                                    following.avatar_uploaded AS "avatarUploaded"
                                                    FROM                        "user" AS following
                                                    JOIN                        user_follower AS uf on uf.user_id = following.id
                                                    WHERE                       uf.follower_user_id = _user_id
                                                    ORDER BY                uf.created DESC
                              ) AS a
                          ) AS "following",
                                        array
                                        (
                              SELECT row_to_json(a)
                              FROM
                                                (
                                                    SELECT                  client.id,
                                                                                    client.username,
                                                                                    client.first_name AS "firstName",
                                                                                    client.last_name AS "lastName",
                                                                                    client.avatar_uploaded AS "avatarUploaded"
                                                    FROM                        "user" AS client
                                                    JOIN                        user_client AS uf on uf.client_user_id = client.id
                                                    WHERE                       uf.user_id = _user_id
                                                    ORDER BY                uf.created DESC
                              ) AS a
                          ) AS "clients",
                                        array
                                        (
                              SELECT row_to_json(a)
                              FROM
                                                (
                                                    SELECT                  trainer.id,
                                                                                    trainer.username,
                                                                                    trainer.first_name AS "firstName",
                                                                                    trainer.last_name AS "lastName",
                                                                                    trainer.avatar_uploaded AS "avatarUploaded"
                                                    FROM                        "user" AS trainer
                                                    JOIN                        user_client AS uf on uf.user_id = trainer.id
                                                    WHERE                       uf.client_user_id = _user_id
                                                    ORDER BY                uf.created DESC
                              ) AS a
                          ) AS "trainers",
                                        u.blocked,
                    u.bio
					  
        FROM                        "user" AS u
        LEFT OUTER JOIN user_district AS ud
        ON                          u.id = ud.user_id
        LEFT OUTER JOIN user_region AS ur
        ON                          u.id = ur.user_id
        LEFT OUTER JOIN district AS d
        ON                          ud.district_id = d.id
        LEFT OUTER JOIN county AS c
        ON                          d.county_id = c.id
        LEFT OUTER JOIN region AS r
        ON                          ur.region_id = r.id
        LEFT OUTER JOIN country AS ct
        ON                          r.country_id = ct.id
    LEFT OUTER JOIN user_routine_type AS urt
    ON              u.id = urt.user_id
    LEFT OUTER JOIN routine_type AS rt
    ON              urt.routine_type_id = rt.id
        WHERE                       u.id = _user_id
        ORDER BY                ud.created DESC,
                                        ur.created DESC
        LIMIT                       1
    ) AS result_to_return;
$function$;
