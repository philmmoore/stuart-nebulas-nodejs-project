ALTER TABLE "user" ADD email TEXT UNIQUE;

UPDATE "user" SET username='antfie', email='antfie@gmail.com';

ALTER TABLE "user" ALTER COLUMN email SET NOT NULL;

CREATE FUNCTION sp_gymfit_site_user_is_email_registered
(
	_email TEXT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM (
		SELECT		TRUE as result
		FROM			"user"
		WHERE			email = LOWER(_email)
		LIMIT			1
	) AS result_to_return;
$$
LANGUAGE sql;
