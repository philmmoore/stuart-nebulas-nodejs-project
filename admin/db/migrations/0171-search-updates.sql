CREATE OR REPLACE FUNCTION sp_gymfit_api_user_search
(
	_search_query TEXT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
	(
		SELECT					id,
										username,
										first_name AS "firstName",
										last_name AS "lastName",
										avatar_uploaded AS "avatarUploaded"
		FROM						"user"
		WHERE						LOWER(username) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
		OR							LOWER(first_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
		OR							LOWER(last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
		OR							LOWER(first_name || last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
	) AS result_to_return;
$$
LANGUAGE sql;

DROP FUNCTION sp_gymfit_site_user_search(TEXT);

CREATE OR REPLACE FUNCTION sp_gymfit_site_search_user
(
	_search_query TEXT,
	_results_per_page INT,
	_page INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		SELECT
		(
			SELECT array_to_json(array_agg(results))
			FROM
			(
				SELECT					id,
												username,
												first_name AS "firstName",
												last_name AS "lastName",
												avatar_uploaded AS "avatarUploaded"
				FROM						"user"
				WHERE						LOWER(username) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
				OR							LOWER(first_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
				OR							LOWER(last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
				OR							LOWER(first_name || last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
				OFFSET					(_page - 1) * _results_per_page
				LIMIT						_results_per_page
			) AS results
		) AS "results",
		(
			SELECT				ceiling(COUNT(id)::DECIMAL / _results_per_page)
			FROM						"user"
			WHERE						LOWER(username) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
			OR							LOWER(first_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
			OR							LOWER(last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
			OR							LOWER(first_name || last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
		) AS "numberOfPages"
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_search_location
(
	_search_query TEXT,
	_district_id INT,
	_county_id INT,
	_region_id INT,
	_country_id INT,
	_results_per_page INT,
	_page INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		SELECT
		(
			SELECT array_to_json(array_agg(results))
			FROM
			(
				SELECT					u.id,
												u.username,
												u.first_name AS "firstName",
												u.last_name AS "lastName",
												u.avatar_uploaded AS "avatarUploaded"
				FROM						"user" AS u
				LEFT OUTER JOIN district AS d ON d.id = (SELECT district_id FROM user_district WHERE user_id = u.id ORDER BY CREATED DESC LIMIT 1)
				LEFT OUTER JOIN region AS r ON r.id = (SELECT region_id FROM user_region WHERE user_id = u.id ORDER BY CREATED DESC LIMIT 1)
				LEFT OUTER JOIN county AS c ON c.id = d.county_id
				LEFT OUTER JOIN country AS ct ON ct.id = r.country_id
				WHERE						(d.id = _district_id OR _district_id IS NULL)
				AND							(c.id = _county_id OR _county_id IS NULL)
				AND							(r.id = _region_id OR _region_id IS NULL)
				AND							(ct.id = _country_id OR _country_id IS NULL)
				AND							(
													LOWER(u.username) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
													OR LOWER(u.first_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
													OR LOWER(u.last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
													OR LOWER(u.first_name || u.last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
													OR _search_query IS NULL
												)
				OFFSET					(_page - 1) * _results_per_page
				LIMIT						_results_per_page
			) AS results
		) AS "results",
		(
			SELECT					ceiling(COUNT(u.id)::DECIMAL / _results_per_page)
			FROM						"user" AS u
			LEFT OUTER JOIN district AS d ON d.id = (SELECT district_id FROM user_district WHERE user_id = u.id ORDER BY CREATED DESC LIMIT 1)
			LEFT OUTER JOIN region AS r ON r.id = (SELECT region_id FROM user_region WHERE user_id = u.id ORDER BY CREATED DESC LIMIT 1)
			LEFT OUTER JOIN county AS c ON c.id = d.county_id
			LEFT OUTER JOIN country AS ct ON ct.id = r.country_id
			WHERE						(d.id = _district_id OR _district_id IS NULL)
			AND							(c.id = _county_id OR _county_id IS NULL)
			AND							(r.id = _region_id OR _region_id IS NULL)
			AND							(ct.id = _country_id OR _country_id IS NULL)
			AND							(
												LOWER(u.username) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
												OR LOWER(u.first_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
												OR LOWER(u.last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
												OR LOWER(u.first_name || u.last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
												OR _search_query IS NULL
											)
		) AS "numberOfPages"
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_set_location
(
	_user_id INT,
	_region_id INT,
	_district_id INT
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO	user_region (user_id, region_id)
		SELECT 			_user_id,
								_region_id
		WHERE (SELECT region_id FROM user_region WHERE user_id = _user_id ORDER BY created DESC LIMIT 1) != _region_id;
		
		INSERT INTO	user_district (user_id, district_id)
		SELECT 			_user_id,
								_district_id
		WHERE (SELECT district_id FROM user_district WHERE user_id = _user_id ORDER BY created DESC LIMIT 1) != _district_id;

		UPDATE			user_notification
		SET					acknowledged = current_timestamp
		WHERE				user_id = _user_id
		AND					notification_id = 3
		AND					acknowledged IS NULL;

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;

INSERT INTO 		user_region (user_id, region_id)
SELECT					ud.user_id,
								c.region_id
FROM 						user_district AS ud
JOIN 						district AS d ON d.id = ud.district_id
JOIN 						county AS c ON c.id = d.county_id
LEFT OUTER JOIN user_region AS ur ON ud.user_id = ur.user_id
WHERE 					ur.user_id IS NULL;