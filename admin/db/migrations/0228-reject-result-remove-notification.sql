DELETE
FROM		social_notification
WHERE		id IN
(
	SELECT 	n.id
	FROM		social_notification AS n
	JOIN		result AS r ON json_extract_path_text(n.notification, 'resultId')::bigint = r.id
	WHERE 	r.excluded IS NOT NULL
);

CREATE OR REPLACE FUNCTION sp_gymfit_admin_result_reject
(
	_result_id INT,
	_reason TEXT
)
RETURNS JSON AS
$$
	DELETE
	FROM		social_notification
	WHERE		id IN
	(
		SELECT 	n.id
		FROM		social_notification AS n
		JOIN		result AS r ON json_extract_path_text(n.notification, 'resultId')::bigint = r.id
		WHERE 	r.id = _result_id
		AND			r.excluded IS NULL
	);
	
	WITH rows AS
	(
		UPDATE		result
		SET				excluded = current_timestamp,
							exclude_reason = _reason,
							approved = NULL
		WHERE 		id = _result_id
		AND				excluded IS NULL
		RETURNING 1
	)
	SELECT to_json(COUNT(*)) FROM rows;
$$
LANGUAGE sql;