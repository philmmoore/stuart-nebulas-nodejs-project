CREATE OR REPLACE FUNCTION sp_gymfit_admin_notification_get_all()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM
		(
			SELECT			n.id,
									t.value as "type",
									n.title,
                	n.description
			FROM				notification AS n
			INNER JOIN	notification_type AS t
			ON					t.id = n.type
			ORDER BY		title
		) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_notification_get
(
	_notification_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
	(
			SELECT
			(
				SELECT array_to_json(array_agg(result_a))
				FROM
				(
					SELECT		id,
										value
					FROM			notification_type
					ORDER BY	value
				) AS result_a
			) AS "notificationTypes",
			(
				SELECT row_to_json(result_b)
				FROM
				(
					SELECT		n.id,
										n.title,
										n.description,
										n.user_dismissable AS "isUserDismissable",
										n.type as "typeId"
				) AS result_b
			) AS "notification"
			FROM						notification AS n
			LEFT OUTER JOIN	notification_type AS nt
			ON							nt.id = n.type
			WHERE						n.id = _notification_id
			LIMIT						1
		) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_notification_get_types()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_a))
		FROM
		(
			SELECT		id,
								value
			FROM			notification_type
			ORDER BY	value
		) AS result_a
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_notification_save
(
	_notification_id INT,
	_notification_type_id INT,
	_is_user_dismissable BIT,
	_title TEXT,
	_description TEXT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	notification
		SET			type = _notification_type_id,
						user_dismissable = _is_user_dismissable,
						title = _title,
						description = _description
		WHERE		id = _notification_id;

		IF NOT FOUND THEN
			INSERT INTO notification (type, user_dismissable, title, description)
			VALUES			(_notification_type_id, _is_user_dismissable, _title, _description)
			RETURNING		id
			INTO				_notification_id;
		END IF;

		RETURN to_json(_notification_id);
	END;
$$
LANGUAGE plpgsql;
