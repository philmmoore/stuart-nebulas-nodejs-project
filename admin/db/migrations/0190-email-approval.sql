UPDATE email_template SET standard_template='<html>
  <head>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <style>
      body {
        font-family: "Montserrat";
        background: #f0f0f0;
        text-align: center;
      }
      
      .content {
        margin-top: 25px;
        width: 500px;
        margin-left: auto;
        margin-right: auto;
        background: white;
        padding: 15px;
        border-radius: 6px;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        box-shadow: 10px 10px 50px #888;
        text-align: center;
      }
      
      .logo {
        width: 200px;
        padding-top: 25px;
        padding-bottom: 25px;
      }
      
      h1, a, a:hover, a:focus {
        color: #6e2d91;
      }
      
      a {
        text-decoration: none;
      }
      
      .box {
        padding: 15px;
        border-radius: 6px;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        background: #e1e1e1;
        margin-top: 15px;
      }
      
      .footer {
        font-size: 0.2em;
      }
    </style>
  </head>
  <body>
    <div class="content">
      <img src="https://gymfit.com/purple-logo.svg" class="logo" />
      <h1>{{SUBJECT}}</h1>
      {{BODY}}
      <p>TEAM GYMFIT</p>
    </div>
    <p class="footer">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    </p>
    <p class="footer">
      {{FOOTER_TEXT}}
    </p>
  </body>
</html>'
WHERE standard_template = '<html>
  <head>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <style>
      body {
        font-family: "Montserrat";
        background: #f0f0f0;
        text-align: center;
      }
      
      .content {
        margin-top: 25px;
        width: 500px;
        margin-left: auto;
        margin-right: auto;
        background: white;
        padding: 15px;
        border-radius: 6px;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        box-shadow: 10px 10px 50px #888;
        text-align: center;
      }
      
      .logo {
        width: 200px;
        padding-top: 25px;
        padding-bottom: 25px;
      }
      
      h1, a, a:hover, a:focus {
        color: #6e2d91;
      }
      
      a {
        text-decoration: none;
      }
      
      .box {
        padding: 15px;
        border-radius: 6px;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        background: #e1e1e1;
        margin-top: 15px;
      }
      
      .footer {
        font-size: 0.2em;
      }
    </style>
  </head>
  <body>
    <div class="content">
      <img src="https://gymfit.com/purple-logo.svg" class="logo" />
      <h1>{{SUBJECT}}</h1>
      {{BODY}}
      <p>TEAM GYMFIT</p>
    </div>
    <p class="footer">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    </p>
  </body>
</html>';

CREATE TABLE email_approval
(
  key UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  created TIMESTAMP DEFAULT current_timestamp NOT NULL,
  email_type_id INT REFERENCES email_type (id) NOT NULL,
  subject TEXT NOT NULL,
  plain_body TEXT NOT NULL,
  html_body TEXT NOT NULL,
  name TEXT NOT NULL,
  approved TIMESTAMP
);

CREATE OR REPLACE FUNCTION sp_gymfit_admin_email_get_approval_key
(
	_email_type_id INT,
  _subject TEXT,
  _plain_body TEXT,
  _html_body TEXT,
  _name TEXT
)
RETURNS JSON AS
$$
	DECLARE
		_key UUID;
	BEGIN
    INSERT INTO email_approval
    (
                email_type_id,
                subject,
                plain_body,
                html_body,
                name
    )
    VALUES
    (
                _email_type_id,
                _subject,
                _plain_body,
                _html_body,
                _name
    )	
    RETURNING		key
    INTO				_key;
    
		RETURN row_to_json(result_to_return)
		FROM (
			SELECT   _key AS key
		) AS result_to_return;
	END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_email_get_approved_email
(
	_key UUID
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		SELECT	email_type_id AS "emailTypeId",
            subject,
            plain_body AS "plainBody",
            html_body AS "htmlBody",
            name
		FROM		email_approval AS e
		WHERE		key = _key
		AND         approved IS NULL
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_email_get_all_users
(
    _key UUID
)
RETURNS JSON AS
$$
    DECLARE
		_email_type_id INT;
	BEGIN

	SELECT  email_type_id
	INTO    _email_type_id
	FROM    email_approval
	WHERE   key = _key;

	UPDATE  email_approval
	SET     approved=current_timestamp
	WHERE   key = _key;

	RETURN array_to_json(array_agg(result_to_return))
	FROM
	(
    WITH data AS
    (
      SELECT			u.email,
            			u.first_name,
            			u.last_name,
            			u.username,
            			(
            				SELECT 
            					CASE
            						WHEN _email_type_id = 1 THEN news
            						WHEN _email_type_id = 2 THEN promotion
            					END

            				 FROM user_email_preference WHERE user_id = u.id ORDER BY updated DESC LIMIT 1
            			) AS email_preference,
            			(
            				SELECT gymfit_email_opt_in
            				 FROM user_privacy_preferences WHERE user_id = u.id ORDER BY updated DESC LIMIT 1
            			) AS gymfit_email_opt_in
      FROM			"user" AS u
    )
    SELECT	email,
    	      first_name AS "firstName",
    	      last_name AS "lastName",
            username
    FROM    data
    WHERE   gymfit_email_opt_in IS TRUE
    AND     email_preference IS TRUE
	) AS result_to_return;
	END;
$$
LANGUAGE plpgsql;