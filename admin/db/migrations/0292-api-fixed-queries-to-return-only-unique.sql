CREATE OR REPLACE FUNCTION sp_gymfit_admin_user_featured_get()
 RETURNS json
 LANGUAGE sql
AS $function$
  SELECT array_to_json(array_agg(result_to_return))
  FROM
  (
    SELECT          u.id,
                    u.username,
                    u.first_name AS "firstName",
                    u.last_name AS "lastName",
                    u.is_personal_trainer AS "isPersonalTrainer",
                    u.is_gym AS "isGym",
                    u.avatar_uploaded AS "avatarUploaded",
                    u.bio,
                     (          
                        SELECT row_to_json(result_location)
                        FROM
                        (
                            SELECT
                            r.name AS "region",
                            d.name AS "district"
                        ) AS result_location
                     ) AS "location"
    FROM            featured_users fu
    JOIN "user" u ON u.id = fu.user_id
    JOIN user_district ud ON ud.user_id = u.id
    JOIN user_region ur ON ur.user_id = u.id
    JOIN region r ON r.id = ur.region_id
    JOIN district d ON d.id = ud.district_id
    GROUP BY u.id, r.name, d.name
  ) AS result_to_return;

$function$;


CREATE OR REPLACE FUNCTION public.sp_gymfit_api_users_near_me(lat numeric, lon numeric, radius numeric, user_is_pt boolean, user_is_gym boolean, userid numeric, show_following boolean)
 RETURNS json
 LANGUAGE sql
AS $function$
    
            SELECT array_to_json(array_agg(result_to_return))
            FROM (
                SELECT 
                    u.id,
                    u.username,
                    u.first_name as "firstName",
                    u.last_name as "lastName",
                    u.is_male as "isMale",
                    u.is_personal_trainer as "isPersonalTrainer",
                    u.is_gym as "isGym",
                    u.bio,
                    point(lon, lat) <@> point(d.longitude, d.latitude)::point AS "distance",
                    u.avatar_uploaded AS "avatarUploaded",
                      (
                        SELECT row_to_json(location_result) 
                        FROM (
                            SELECT
                            d.name AS "district",
                            r.name AS "region"
                        ) AS location_result
                      ) AS "location"
                FROM user_district ud
                JOIN "user" u ON u.id = ud.user_id
                JOIN district d ON d.id = ud.district_id
                JOIN user_region ur ON ur.user_id = u.id
                JOIN region r ON r.id = ur.region_id
                WHERE (ud.district_id IN (
                        SELECT 
                           d.id
                        FROM district d
                        WHERE (point(lon, lat) <@> point(d.longitude, d.latitude)) < radius -- SEARCH RADIUS IN MILES
                        GROUP by d.id
                    )
                ) AND u.is_personal_trainer = user_is_pt 
                    AND u.is_gym = user_is_gym 
                    AND u.id != userId
                    AND u.id NOT IN (
                        SELECT                  following.id
                        FROM                    "user" AS following
                        JOIN                    user_follower AS uf on uf.user_id = following.id
                        WHERE                   uf.follower_user_id = userId
                        AND                     (show_following != TRUE)
                        GROUP BY uf.created, following.id
                        ORDER BY                uf.created DESC
                    ) 

                  GROUP BY u.id, d.name, r.name, d.longitude, d.latitude
                ORDER by distance ASC
            ) as "result_to_return"
        $function$;
