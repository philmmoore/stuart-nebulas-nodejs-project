CREATE OR REPLACE FUNCTION sp_gymfit_admin_user_remove_bio
(
	_user_id INT
)
RETURNS JSON AS
$$
	UPDATE		"user"
	SET				bio = NULL
	WHERE 		id = _user_id;
	
  SELECT    to_json(
  (
    SELECT  COUNT(id) = 1
    FROM    "user"
    WHERE   id = _user_id
  ));
$$
LANGUAGE sql;