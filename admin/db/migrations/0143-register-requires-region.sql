ALTER TABLE user_registration ADD region_id INT REFERENCES region (id);

DROP FUNCTION sp_gymfit_site_user_registration_add_step_2_details(INT, TEXT, TEXT, TEXT, DATE);

CREATE FUNCTION sp_gymfit_site_user_registration_add_step_2_details
(
	_registration_id INT,
	_first_name TEXT,
	_last_name TEXT,
	_username TEXT,
	_date_of_birth DATE,
  _region_id INT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE			user_registration
		SET					first_name = _first_name,
								last_name = _last_name,
								username = _username,
								date_of_birth = _date_of_birth,
                region_id = _region_id
		WHERE				id = _registration_id
		RETURNING		id
		INTO				_registration_id;

		RETURN to_json(_registration_id);
	END;
$$
LANGUAGE plpgsql;

CREATE TABLE user_region
(
  id SERIAL PRIMARY KEY,
  created TIMESTAMP DEFAULT current_timestamp,
  user_id INT REFERENCES "user" (id) NOT NULL,
  region_id INT REFERENCES region (id) NOT NULL
);

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_registration_add_user
(
	_registration_id INT
)
RETURNS JSON AS
$$
	DECLARE
		_user_id INT;
	BEGIN
		INSERT INTO	"user" (username, hash, first_name, last_name, date_of_birth, email, avatar_uploaded)
		SELECT			username,
								hash,
								first_name,
								last_name,
								date_of_birth,
								email,
                '0'
		FROM				user_registration
		WHERE				id = _registration_id
		RETURNING		id
		INTO				_user_id;

		INSERT INTO	user_region (user_id, region_id)
		VALUES			(_user_id, (SELECT region_id FROM user_registration WHERE id = _registration_id));

		DELETE FROM	user_registration
		WHERE				id = _registration_id;

		INSERT INTO	user_email_verification (user_id)
		VALUES			(_user_id);



		INSERT INTO	user_notification (user_id, notification_id)
		VALUES			(_user_id, 1),
								(_user_id, 2),
								(_user_id, 3),
								(_user_id, 4);

		RETURN row_to_json(result)
		FROM (
			SELECT					u.id,
											u.email,
											ev.key AS "verificationKey"
			FROM						"user" AS u
			LEFT OUTER JOIN	user_email_verification AS ev ON ev.user_id = u.id
			WHERE						u.id = _user_id
		) AS result;
	END;
$$
LANGUAGE plpgsql;
