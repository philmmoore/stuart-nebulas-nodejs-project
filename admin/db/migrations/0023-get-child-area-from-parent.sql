CREATE OR REPLACE FUNCTION sp_site_get_child_area_from_parent_area
(
	_parentId INT,
	_name TEXT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)	FROM (
		SELECT id,
				name
		FROM	area
		WHERE	parent = _parentId
		AND		LOWER(REPLACE(name, ' ', '-')) = LOWER(_name)
		LIMIT 1
	) AS result;
$$
LANGUAGE sql;
