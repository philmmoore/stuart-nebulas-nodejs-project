CREATE OR REPLACE FUNCTION sp_gymfit_site_blog_get_posts
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM (
		SELECT		name,
							body_copy AS "bodyCopy"
		FROM			blog_post
		WHERE			country = _country_id
		ORDER BY	name
		LIMIT			10
	) AS result;
$$
LANGUAGE sql;
