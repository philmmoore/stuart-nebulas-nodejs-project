CREATE OR REPLACE FUNCTION sp_gymfit_site_user_get_nominators_to_notify
(
	_user_id INT,
	_challenge_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
		(
			SELECT	u.id,
							u.first_name AS "firstName",
							u.last_name AS "lastName",
							u.username,
							u.email
			FROM		"user" AS u
			JOIN		user_nomination AS n ON n.user_id = u.id
			WHERE		(SELECT nomination FROM user_email_preference WHERE user_id = u.id ORDER BY updated DESC LIMIT 1) IS TRUE
			AND 		(SELECT gymfit_email_opt_in FROM user_privacy_preferences WHERE user_id = u.id ORDER BY updated DESC LIMIT 1) IS TRUE
			AND			n.challenge_id = _challenge_id
			AND			n.nominee_user_id = _user_id
			AND			n.accepted IS NULL
		) AS result_to_return;
$$
LANGUAGE sql;