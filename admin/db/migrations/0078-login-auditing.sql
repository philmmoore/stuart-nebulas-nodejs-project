ALTER TABLE user_audit ADD user_id INT REFERENCES "user" (id) NOT NULL;

DROP FUNCTION sp_gymfit_site_user_get_details(TEXT, BYTEA);

CREATE FUNCTION sp_gymfit_site_user_get_details
(
	_username_or_email TEXT,
	_hash BYTEA,
	_ip_address TEXT
)
RETURNS JSON AS
$$
	INSERT INTO	user_audit (user_id, activity, message)
	SELECT			id,
							'login',
							_ip_address
	FROM				"user"
	WHERE				hash = _hash
	AND					(LOWER(username) = LOWER(_username_or_email) OR LOWER(email) = LOWER(_username_or_email));

	SELECT row_to_json(result)
	FROM (
		SELECT	id,
						username
		FROM		"user"
		WHERE		hash = _hash
		AND			(LOWER(username) = LOWER(_username_or_email) OR LOWER(email) = LOWER(_username_or_email))
		LIMIT		1
	) AS result;
$$
LANGUAGE sql;
