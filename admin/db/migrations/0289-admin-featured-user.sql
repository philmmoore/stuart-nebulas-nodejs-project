-- Drop the table if it exists
DROP TABLE IF EXISTS featured_users;

-- Create the featured users table
CREATE TABLE featured_users (
  id SERIAL PRIMARY KEY,
  user_id INTEGER REFERENCES "user" (id) NOT NULL
);

-- Featured user persist admin & api
DROP FUNCTION IF EXISTS sp_gymfit_admin_featured_user_persist();
CREATE OR REPLACE FUNCTION sp_gymfit_admin_user_featured_persist(_user_id integer)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
    BEGIN

        -- Ensures only one record will exist per user
        DELETE FROM featured_users WHERE user_id = _user_id;

        -- Adds the record
        INSERT INTO featured_users (user_id) VALUES (_user_id);

        RETURN to_json(1);

    END;
$function$;

DROP FUNCTION IF EXISTS sp_gymfit_api_user_featured_persist();
CREATE OR REPLACE FUNCTION sp_gymfit_api_user_featured_persist(_user_id integer)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
    BEGIN

        PERFORM sp_gymfit_admin_user_featured_persist(_user_id);
        RETURN to_json(1);

    END;
$function$;


-- Featured user delete
DROP FUNCTION IF EXISTS sp_gymfit_admin_featured_user_delete();
CREATE OR REPLACE FUNCTION sp_gymfit_admin_user_featured_delete(_user_id integer)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
    BEGIN

        -- Ensures only one record will exist per user
        DELETE FROM featured_users WHERE user_id = _user_id;
        
        RETURN to_json(1);

    END;
$function$;

DROP FUNCTION IF EXISTS sp_gymfit_api_featured_user_delete();
CREATE OR REPLACE FUNCTION sp_gymfit_api_user_featured_delete(_user_id integer)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
    BEGIN

        PERFORM sp_gymfit_admin_user_featured_delete(_user_id);
        RETURN to_json(1);

    END;
$function$;

-- Featured user get
DROP FUNCTION IF EXISTS sp_gymfit_admin_featured_user_get();
CREATE OR REPLACE FUNCTION sp_gymfit_admin_user_featured_get()
 RETURNS json
 LANGUAGE sql
AS $function$
  SELECT array_to_json(array_agg(result_to_return))
  FROM
  (
    SELECT          u.id,
                    u.username,
                    u.first_name AS "firstName",
                    u.last_name AS "lastName",
                    u.is_personal_trainer AS "isPersonalTrainer",
                    u.is_gym AS "isGym",
                    u.avatar_uploaded AS "avatarUploaded",
                    u.bio
    FROM            featured_users fu
    LEFT JOIN "user" u ON u.id = fu.user_id
  ) AS result_to_return;
$function$;

CREATE OR REPLACE FUNCTION sp_gymfit_api_featured_user_get()
 RETURNS json
 LANGUAGE plpgsql
AS $function$
    BEGIN

        RETURN sp_gymfit_admin_user_featured_get();

    END;
$function$;

