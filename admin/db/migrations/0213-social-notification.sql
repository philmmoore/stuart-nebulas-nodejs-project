CREATE TABLE social_notification
(
	id SERIAL PRIMARY KEY,
	created TIMESTAMP DEFAULT current_timestamp NOT NULL,
	user_id INT REFERENCES "user" (id) NOT NULL,
	notification JSON NOT NULL
);

CREATE OR REPLACE FUNCTION sp_gymfit_site_social_get_notifications
(
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM (
		SELECT		      n.id,
										n.created,
										n.user_id AS "userId",
										n.notification,
										u.username,
										u.first_name AS "firstName",
										u.last_name AS "lastName",
										u.avatar_uploaded AS "avatarUploaded"
		FROM			      social_notification AS n
		JOIN						"user" AS u ON n.user_id = u.id
		WHERE						(n.user_id = _user_id
		OR							n.user_id IN
			(
				SELECT client_user_id FROM user_client WHERE user_id = _user_id
				UNION
				SELECT user_id FROM user_follower WHERE follower_user_id = _user_id
			))
		AND							u.blocked IS NULL
		ORDER BY 	      n.created DESC
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_social_get_notifications
(
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM (
		SELECT		      n.id,
										n.created,
										n.user_id AS "userId",
										n.notification,
										u.username,
										u.first_name AS "firstName",
										u.last_name AS "lastName",
										u.avatar_uploaded AS "avatarUploaded"
		FROM			      social_notification AS n
		JOIN						"user" AS u ON n.user_id = u.id
		WHERE						(n.user_id = _user_id
		OR							n.user_id IN
			(
				SELECT client_user_id FROM user_client WHERE user_id = _user_id
				UNION
				SELECT user_id FROM user_follower WHERE follower_user_id = _user_id
			))
		AND							u.blocked IS NULL
		ORDER BY 	      n.created DESC
) AS result_to_return;
$$
LANGUAGE sql;