CREATE OR REPLACE FUNCTION sp_gymfit_api_challenge_activity_chart
(
  _user_id INT,
  _challenge_id INT,
  _challenge_group_id INT
)
RETURNS JSON AS
$$
  SELECT row_to_json(result_to_return)
  FROM
  (
    SELECT  c.name AS "challengeName",
            cg.value AS "challengeGroupName",
            ct.measurement AS "measurement",
            cc.value AS "category",
            (
              SELECT array_to_json(array_agg(results))
              FROM
              (
                SELECT        submitted,
                              result
                FROM          result
                WHERE         user_id = _user_id
                AND           challenge_id = _challenge_id
                AND           challenge_group_id = _challenge_group_id
                ORDER BY      submitted
              ) AS results
            ) AS "results"
    FROM            challenge AS c
    LEFT OUTER JOIN challenge_to_challenge_group AS ccg ON c.id = ccg.challenge_id
    LEFT OUTER JOIN challenge_group AS cg ON ccg.challenge_group_id = cg.id
    LEFT OUTER JOIN challenge_type AS ct on c.type = ct.id
    LEFT OUTER JOIN challenge_category AS cc ON cc.id = c.category
    WHERE           c.id = _challenge_id
    AND             cg.id = _challenge_group_id
  ) AS result_to_return;
$$
LANGUAGE sql;