CREATE FUNCTION sp_admin_user_login(_username TEXT, _hash BYTEA) RETURNS INT AS $$
DECLARE
  _row_count INT;
BEGIN
	INSERT INTO admin_audit (activity, message)
	SELECT 'user_login' AS activity, id
	FROM admin_user
	WHERE username = _username
  AND hash = _hash;

  GET DIAGNOSTICS _row_count = ROW_COUNT;
  RETURN _row_count;
END;
$$
LANGUAGE plpgsql;
