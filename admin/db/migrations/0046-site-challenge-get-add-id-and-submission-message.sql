CREATE OR REPLACE FUNCTION sp_gymfit_site_get_challenge
(
	_name TEXT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)	FROM (
		SELECT	id,
						name,
						body_copy AS "bodyCopy",
						submission_message AS "submissionMessage"
		FROM		challenge
		WHERE		LOWER(REPLACE(name, ' ', '-')) = LOWER(_name)
		LIMIT		1
	) AS result;
$$
LANGUAGE sql;
