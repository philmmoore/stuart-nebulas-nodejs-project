CREATE FUNCTION sp_gymfit_site_user_register_is_username_registered
(
	_username TEXT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM (
		SELECT		TRUE as result
		FROM			"user"
		WHERE			username = LOWER(_username)
		LIMIT			1
	) AS result_to_return;
$$
LANGUAGE sql;
