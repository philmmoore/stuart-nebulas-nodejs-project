CREATE TABLE "user" (
	id SERIAL PRIMARY KEY,
  username TEXT NOT NULL,
  hash BYTEA NOT NULL,
	first_name TEXT,
	last_name TEXT,
	date_of_birth DATE,
  created TIMESTAMP NOT NULL default current_timestamp
);

CREATE TABLE region (
	id SERIAL PRIMARY KEY,
	country_id INTEGER REFERENCES country (id) NOT NULL,
	name TEXT NOT NULL
);

CREATE TABLE county (
	id SERIAL PRIMARY KEY,
	region_id INTEGER REFERENCES region (id) NOT NULL,
	name TEXT NOT NULL
);

CREATE TABLE district (
	id SERIAL PRIMARY KEY,
	county_id INTEGER REFERENCES county (id) NOT NULL,
	name TEXT NOT NULL
);

CREATE TABLE user_district (
	id SERIAL PRIMARY KEY,
	created TIMESTAMP NOT NULL default current_timestamp,
	user_id INTEGER REFERENCES "user" (id) NOT NULL,
  district_id INTEGER REFERENCES district (id) NOT NULL
);

CREATE TABLE user_height (
	id SERIAL PRIMARY KEY,
	created TIMESTAMP NOT NULL default current_timestamp,
	user_id INTEGER REFERENCES "user" (id) NOT NULL,
	height SMALLINT NOT NULL -- in mm, max: 32.76m (tallest man = 2.51m)
);

CREATE TABLE user_weight (
	id SERIAL PRIMARY KEY,
	created TIMESTAMP NOT NULL default current_timestamp,
	user_id INTEGER REFERENCES "user" (id) NOT NULL,
  weight INTEGER NOT NULL -- in grams, max: 2.14748e6kg (heaviest man = 635.03kg)
);

CREATE TABLE result (
	id BIGSERIAL PRIMARY KEY,
	submitted TIMESTAMP NOT NULL default current_timestamp,
	user_id INTEGER REFERENCES "user" (id) NOT NULL,
	challenge_id INTEGER REFERENCES challenge (id) NOT NULL,
	challenge_group_id INTEGER REFERENCES challenge_group (id) NOT NULL,
	result INT NOT NULL,
	validated TIMESTAMP
);
