ALTER TABLE blog_post ADD created TIMESTAMP default current_timestamp;
UPDATE blog_post SET created=current_timestamp;
ALTER TABLE blog_post ALTER created SET NOT NULL;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_blog_get_all_posts_for_country
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM
		(
			SELECT		id,
								name,
								created
			FROM			blog_post
			WHERE			country = _country_id
			ORDER BY	created DESC
		) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_blog_get_posts
(
	_country_id INT,
	_posts_per_page INT,
	_page INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
		(
				SELECT	ceiling(count(*)::DECIMAL / _posts_per_page) AS "numberOfPages",
								(
									SELECT array_to_json(array_agg(result_b))
									FROM (
										SELECT		name,
															body_copy AS "bodyCopy"
										FROM			blog_post
										WHERE			country = _country_id
										ORDER BY	created DESC
										OFFSET		(_page - 1) * _posts_per_page
										LIMIT			_posts_per_page
									) AS result_b
							) AS "posts"
			FROM 		blog_post
	) AS result;
$$
LANGUAGE sql;
