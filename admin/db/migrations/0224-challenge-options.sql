ALTER TABLE challenge_type ADD COLUMN min_first BOOL;
UPDATE challenge_type SET min_first = FALSE WHERE measurement = 'count' OR measurement = 'distance';
UPDATE challenge_type SET min_first = TRUE WHERE measurement = 'time';

ALTER TABLE challenge_type ALTER COLUMN min_first SET NOT NULL;

UPDATE challenge_type SET value = 'Shortest time taken' WHERE value = 'Time taken';

INSERT INTO challenge_type (value, measurement, min_first) VALUES ('Longest time taken', 'time', FALSE);




CREATE OR REPLACE FUNCTION sp_gymfit_site_leaderboard_search
(
	_challenge_id INT,
	_challenge_group_id INT,
	_district_id INT,
	_county_id INT,
	_region_id INT,
	_country_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_last_period_start TIMESTAMP,
	_last_period_end TIMESTAMP,
	_results_per_page INT,
	_page INT,
	_is_male BIT,
	_age_min SMALLINT,
	_age_max SMALLINT,
	_weight_min INT,
	_weight_max INT,
	_only_validated BIT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		WITH filtered_results AS
		(
			SELECT				r.id,
										r.submitted,
										r.result,
										r.user_id,
										r.image_validated,
										r.video_validated,
										ct.measurement,
                    ct.min_first,
										u.username,
										u.avatar_uploaded,
										u.first_name,
										u.last_name
			FROM					result AS r
			JOIN					"user" AS u ON r.user_id = u.id
			JOIN 					challenge AS c ON c.id = r.challenge_id
			JOIN					challenge_type AS ct ON ct.id = c.type			
			WHERE					r.submitted >= _last_period_start
			AND						r.submitted <= _this_period_end
			AND						r.challenge_id = _challenge_id
			AND						r.challenge_group_id = _challenge_group_id
			AND						(r.district_id = _district_id OR _district_id IS NULL)
			AND						(r.county_id = _county_id OR _county_id IS NULL)
			AND						(r.region_id = _region_id OR _region_id IS NULL)
			AND						(r.country_id = _country_id OR _country_id IS NULL)
			AND						(r.age >= _age_min OR _age_min IS NULL)
			AND						(r.age <= _age_max OR _age_max IS NULL)
			AND						(r.weight >= _weight_min OR _weight_min IS NULL)
			AND						(r.weight <= _weight_max OR _weight_max IS NULL)
			AND						((r.image_validated IS NOT NULL OR r.video_validated IS NOT NULL) OR _only_validated != '1')
			AND						(u.is_male = _is_male OR _is_male IS NULL)
			AND						r.excluded IS NULL
		)
		
		SELECT					(
											SELECT 				ceiling(COUNT(DISTINCT user_id)::DECIMAL / _results_per_page) AS "numberOfPages"
											FROM					filtered_results
											WHERE					submitted >= _this_period_start
										) AS "numberOfPages",
		
										(
											SELECT				measurement
											FROM					filtered_results
											LIMIT					1
										) AS "measurement",
										
										(
											SELECT array_to_json(array_agg(paged_results))
											FROM
											(
												
												
												
												
												WITH current_ranges AS
												(
													SELECT			row_number() over (ORDER BY MAX(frr.result) DESC) AS max_rank,
																			row_number() over (ORDER BY MIN(frr.result)) AS min_rank,
																			(
																				SELECT		id
																				FROM			filtered_results
																				WHERE 		result = MAX(frr.result)
																				AND				user_id = frr.user_id
																				ORDER BY 	video_validated DESC NULLS LAST,
																				 					image_validated DESC NULLS LAST,
																									submitted DESC
																				LIMIT 		1
																			) AS max_result_id,
																			(
																				SELECT 		id
																				FROM 			filtered_results
																				WHERE 		result = MIN(frr.result)
																				AND 			user_id = frr.user_id
																				ORDER BY 	video_validated DESC NULLS LAST,
																				 					image_validated DESC NULLS LAST,
																									submitted DESC
																				LIMIT 		1
																			) AS min_result_id,
																			frr.user_id
													FROM 				filtered_results AS frr
													WHERE				submitted >= _this_period_start
													GROUP BY		frr.user_id
												)
												
												SELECT				fr.id,
																			CASE
																				WHEN fr.min_first = TRUE THEN rg.min_rank
																				ELSE rg.max_rank
																			END AS "rank",
																			fr.result,
																			fr.image_validated AS "imageValidated",
																			fr.video_validated AS "videoValidated",
																			fr.user_id AS "userId",
																			fr.username,
																			fr.avatar_uploaded AS "userHasAvatar",
									                    fr.first_name "firstName",
									                    fr.last_name "lastName",
																			(
																				SELECT      COUNT(DISTINCT user_id)
								                      	FROM				result_feedback
																				WHERE				result_id = fr.id
																				AND         flag = FALSE
																			) AS "likeCount",
																			(
																				SELECT      COUNT(DISTINCT user_id)
								                      	FROM				result_feedback
																				WHERE				result_id = fr.id
																				AND         flag = TRUE
																			) AS "flagCount",
																			(
																				
																				
																				WITH last_rank AS
																				(
																					SELECT			lfr.user_id,
																											CASE
																												WHEN fr.min_first = TRUE THEN
                                                          row_number() over (ORDER BY MIN(lfr.result))
																												ELSE
																													row_number() over (ORDER BY MAX(lfr.result) DESC)
																											END AS last_rank_result
																					FROM 				filtered_results AS lfr
																					WHERE				lfr.submitted <= _last_period_end
																					GROUP BY		lfr.user_id
																				)
																				SELECT	last_rank_result
																				FROM		last_rank
																				WHERE		user_id = fr.user_id
																				LIMIT 1
																				
																				
																			) AS "lastRank"
												FROM					current_ranges AS rg
												JOIN					filtered_results AS fr ON fr.id = CASE
																																					WHEN fr.min_first = TRUE THEN rg.min_result_id
																																					ELSE rg.max_result_id
																																				END
												ORDER BY 		  rank
												OFFSET				(_page - 1) * _results_per_page
												LIMIT					_results_per_page
																															
																															
																															
                      ) AS paged_results
										) AS "results"
		
		FROM filtered_results
	) AS result_to_return;
$$
LANGUAGE sql;








CREATE OR REPLACE FUNCTION sp_gymfit_site_leaderboard_user_position
(
	_challenge_id INT,
	_challenge_group_id INT,
	_district_id INT,
	_county_id INT,
	_region_id INT,
	_country_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_is_male BIT,
	_age_min SMALLINT,
	_age_max SMALLINT,
	_weight_min INT,
	_weight_max INT,
	_only_validated BIT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		WITH filtered_results AS
		(
			SELECT				r.id,
										r.submitted,
										r.result,
										r.user_id,
										r.image_validated,
										r.video_validated,
										ct.measurement,
                    ct.min_first,
										u.username,
										u.avatar_uploaded,
										u.first_name,
										u.last_name
			FROM					result AS r
			JOIN					"user" AS u ON r.user_id = u.id
			JOIN 					challenge AS c ON c.id = r.challenge_id
			JOIN					challenge_type AS ct ON ct.id = c.type			
			WHERE					r.submitted >= _this_period_start
			AND						r.submitted <= _this_period_end
			AND						r.challenge_id = _challenge_id
			AND						r.challenge_group_id = _challenge_group_id
			AND						(r.district_id = _district_id OR _district_id IS NULL)
			AND						(r.county_id = _county_id OR _county_id IS NULL)
			AND						(r.region_id = _region_id OR _region_id IS NULL)
			AND						(r.country_id = _country_id OR _country_id IS NULL)
			AND						(r.age >= _age_min OR _age_min IS NULL)
			AND						(r.age <= _age_max OR _age_max IS NULL)
			AND						(r.weight >= _weight_min OR _weight_min IS NULL)
			AND						(r.weight <= _weight_max OR _weight_max IS NULL)
			AND						((r.image_validated IS NOT NULL OR r.video_validated IS NOT NULL) OR _only_validated != '1')
			AND						(u.is_male = _is_male OR _is_male IS NULL)
			AND						r.excluded IS NULL
		)
		
	
		SELECT array (
		
			
			
			WITH current_ranges AS
			(
				SELECT			row_number() over (ORDER BY MAX(frr.result) DESC) AS max_rank,
										row_number() over (ORDER BY MIN(frr.result)) AS min_rank,
										(
											SELECT		id
											FROM			filtered_results
											WHERE 		result = MAX(frr.result)
											AND				user_id = frr.user_id
											ORDER BY 	video_validated DESC NULLS LAST,
											 					image_validated DESC NULLS LAST,
																submitted DESC
											LIMIT 		1
										) AS max_result_id,
										(
											SELECT 		id
											FROM 			filtered_results
											WHERE 		result = MIN(frr.result)
											AND 			user_id = frr.user_id
											ORDER BY 	video_validated DESC NULLS LAST,
											 					image_validated DESC NULLS LAST,
																submitted DESC
											LIMIT 		1
										) AS min_result_id,
										frr.user_id
				FROM 				filtered_results AS frr
				WHERE				submitted >= _this_period_start
				GROUP BY		frr.user_id
			)
			
			SELECT				fr.user_id AS "userId"
			FROM					current_ranges AS rg
			JOIN					filtered_results AS fr ON fr.id = CASE
																												WHEN fr.min_first = TRUE THEN rg.min_result_id
																												ELSE rg.max_result_id
																											END
			ORDER BY 		  CASE
											WHEN fr.min_first = TRUE THEN rg.min_rank
											ELSE rg.max_rank
										END
			
						
																						
    ) AS rankings
		
		FROM filtered_results
	) AS result_to_return;
$$
LANGUAGE sql;



CREATE OR REPLACE FUNCTION sp_gymfit_site_user_activity
(
	_user_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_previous_period_start TIMESTAMP,
	_previous_period_end TIMESTAMP
)
RETURNS JSON AS
$$
	DECLARE
		_district_id INT;
		_county_id INT;
		_region_id INT;
		_country_id INT;
		
	BEGIN
		SELECT		district_id
		FROM			user_district
		WHERE			user_id = _user_id
		ORDER BY	created DESC
		LIMIT			1
		INTO			_district_id;
		
		SELECT		county_id
		FROM			district
		WHERE			id = _district_id
		INTO			_county_id;
		
		SELECT		region_id
		FROM			county
		WHERE			id = _county_id
		INTO			_region_id;
		
		SELECT		country_id
		FROM			region
		WHERE			id = _region_id
		INTO			_country_id;

		RETURN array_to_json(array_agg(result_to_return))
		FROM
		(
			SELECT
				DISTINCT
				c.id AS "challengeId",
				c.name AS "challengeName",
				cg.id AS "challengeGroupId",
				cg.value AS "challengeGroupName",
				cc.value AS "category",
				cc.id AS "categoryId",
				ct.measurement AS "measurement",
				
				
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.district_id = _district_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "districtTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.county_id = _county_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "countyTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.region_id = _region_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "regionTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.country_id = _country_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "countryTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "worldTotal",
				
				
				
				
				
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM			result AS r
						WHERE			r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.district_id = _district_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "districtCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM			result AS r
						WHERE			r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.district_id = _district_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "districtPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.county_id = _county_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "countyCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.county_id = _county_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "countyPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.region_id = _region_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "regionCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.region_id = _region_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "regionPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.country_id = _country_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "countryCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.country_id = _country_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "countryPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "worldCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
			) AS "worldPrevious",
				
				
				
				
				
			(
				SELECT 		CASE WHEN ct.min_first = TRUE THEN
                    MIN(r.result)
									ELSE
										MAX(r.result)
									END
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				AND 			r.user_id = _user_id
			) AS "result",
			
			(
				SELECT 		MAX(r.submitted)
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				AND 			r.user_id = _user_id
			) AS "submitted"
				
				
				
			FROM 						result AS ee
			JOIN 						challenge AS c ON ee.challenge_id = c.id
			JOIN 						challenge_group AS cg ON ee.challenge_group_id = cg.id
			JOIN 						challenge_type AS ct on c.type = ct.id
			JOIN 						challenge_category AS cc ON cc.id = c.category
			WHERE						ee.challenge_id IN (SELECT challenge_id FROM result WHERE user_id = _user_id)
			AND							ee.challenge_group_id IN (SELECT challenge_group_id FROM result WHERE user_id = _user_id)
			AND
			(
				SELECT COUNT(r.result)
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				AND 			r.user_id = _user_id
			) > 0
			ORDER BY				submitted DESC

		) AS result_to_return;
	END
$$
LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION sp_gymfit_api_result_get
(
	_result_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_previous_period_start TIMESTAMP,
	_previous_period_end TIMESTAMP
)
RETURNS JSON AS
$$	
	BEGIN
		RETURN row_to_json(result_to_return)
		FROM
		(
			SELECT
				c.id AS "challengeId",
				c.name AS "challengeName",
				cg.id AS "challengeGroupId",
				cg.value AS "challengeGroupName",
				cc.value AS "category",
				cc.id AS "categoryId",
				ct.measurement AS "measurement",
				
				ee.result,
				ee.submitted,
				ee.image_validated AS "imageValidated",
				ee.video_validated AS "videoValidated",
				
				u.id AS "userId",
				u.username,
				u.first_name AS "firstName",
				u.last_name AS "lastName",
				u.avatar_uploaded AS "avatarUploaded",
				
				
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.district_id = ee.district_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "districtTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.county_id = ee.county_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "countyTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.region_id = ee.region_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "regionTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.country_id = ee.country_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "countryTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "worldTotal",
				
				
				
				
				
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM			result AS r
						WHERE			r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.district_id = ee.district_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "districtCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM			result AS r
						WHERE			r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.district_id = ee.district_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "districtPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.county_id = ee.county_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "countyCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.county_id = ee.county_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "countyPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.region_id = ee.region_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "regionCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.region_id = ee.region_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "regionPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.country_id = ee.country_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "countryCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.country_id = ee.country_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "countryPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "worldCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
			) AS "worldPrevious",
				
				
				
				
				
			(
				SELECT 		CASE WHEN ct.min_first = TRUE THEN
                    MIN(r.result)
									ELSE
										MAX(r.result)
									END
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				AND 			r.user_id = u.id
			) AS "result",
			
			(
				SELECT 		MAX(r.submitted)
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				AND 			r.user_id = u.id
			) AS "submitted"
				
				
				
			FROM 						result AS ee
			JOIN						"user" AS u ON u.id = ee.user_id
			JOIN 						challenge AS c ON ee.challenge_id = c.id
			JOIN 						challenge_group AS cg ON ee.challenge_group_id = cg.id
			JOIN 						challenge_type AS ct on c.type = ct.id
			JOIN 						challenge_category AS cc ON cc.id = c.category
			WHERE						ee.id = _result_id

		) AS result_to_return;
	END
$$
LANGUAGE plpgsql;