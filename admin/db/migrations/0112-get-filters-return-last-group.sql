DROP FUNCTION IF EXISTS sp_gymfit_site_get_challenge_filters(INT);

CREATE OR REPLACE FUNCTION sp_gymfit_site_get_challenge_filters
(
	_challenge_id INT,
  _user_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
		(
			SELECT
							array
							(
                  SELECT row_to_json(a)
                  FROM (
                          SELECT			cg.id,
                                      cg.value AS "name"
                          FROM				challenge_group AS cg
													INNER JOIN	challenge_to_challenge_group AS ccg ON ccg.challenge_group_id = cg.id
													WHERE				ccg.challenge_id = c.id
                  ) AS a
              ) AS "groups",
              (
                SELECT    challenge_group_id
                FROM      result
                WHERE     user_id = _user_id
                AND       challenge_id = _challenge_id
                ORDER BY  submitted DESC
                LIMIT     1
              ) AS "selectedGroup",
							array
							(
                  SELECT row_to_json(a)
                  FROM (
                          SELECT			id,
                                      name
                          FROM				age_group
                  ) AS a
              ) AS "ageGroups",
							array
							(
                  SELECT row_to_json(a)
                  FROM (
                          SELECT			id,
                                      name
                          FROM				weight_group
                  ) AS a
              ) AS "weightGroups"
			FROM		challenge AS c
			WHERE		c.id = _challenge_id
		) AS result_to_return;
$$
LANGUAGE sql;
