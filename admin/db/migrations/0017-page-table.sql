CREATE TABLE page (
  id SERIAL PRIMARY KEY,
  country INTEGER REFERENCES country (id) NOT NULL,
  name TEXT NOT NULL,
  body_copy TEXT
);
