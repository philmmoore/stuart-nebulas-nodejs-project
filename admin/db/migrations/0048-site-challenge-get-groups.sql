CREATE FUNCTION sp_gymfit_site_get_challenge_groups
(
	_challenge_name TEXT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
	(
		SELECT array(
			SELECT			cg.value
			FROM				challenge_group AS cg
			INNER JOIN	challenge_to_challenge_group AS ccg ON cg.id = ccg.challenge_group_id
			INNER JOIN	challenge AS c ON c.id = ccg.challenge_id
			WHERE				LOWER(REPLACE(c.name, ' ', '-')) = LOWER('running')
		) AS groups
	) AS result;
$$
LANGUAGE sql;
