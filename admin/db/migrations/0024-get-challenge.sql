CREATE OR REPLACE FUNCTION sp_site_get_challenge
(
	_name TEXT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)	FROM (
		SELECT	name,
						body_copy AS "bodyCopy"
		FROM		challenge
		WHERE		LOWER(REPLACE(name, ' ', '-')) = LOWER(_name)
		LIMIT		1
	) AS result;
$$
LANGUAGE sql;
