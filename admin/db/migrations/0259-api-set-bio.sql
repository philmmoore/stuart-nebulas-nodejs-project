ALTER TABLE "user" ADD COLUMN bio TEXT;

CREATE OR REPLACE FUNCTION sp_gymfit_api_user_set_bio
(
  _user_id INT,
  _bio TEXT
)
RETURNS JSON AS
$$
  UPDATE "user"
  SET   bio = _bio
  WHERE id = _user_id;
  
  SELECT  to_json(
    (
      SELECT  COUNT(id) = 1
      FROM    "user"
      WHERE   id = _user_id
    )
  );
$$
LANGUAGE sql;