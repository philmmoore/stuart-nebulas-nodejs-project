CREATE TABLE blog_post (
  id SERIAL PRIMARY KEY,
  country INTEGER REFERENCES country (id) NOT NULL,
  name TEXT NOT NULL,
  body_copy TEXT
);

INSERT INTO blog_post (country, name, body_copy)
VALUES
				(1, 'Test Post 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
				(1, 'Test Post 2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
				(1, 'Test Post 3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');

CREATE FUNCTION sp_gymfit_admin_blog_get_all_posts_for_country
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM
		(
			SELECT		id,
								name
			FROM			blog_post
			WHERE			country = _country_id
			ORDER BY	name
		) AS result;
$$
LANGUAGE sql;

CREATE FUNCTION sp_gymfit_admin_blog_get_post
(
	_post_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
		(
			SELECT	id,
							name,
							body_copy "bodyCopy"
			FROM		blog_post
			WHERE		id = _post_id
			LIMIT		1
		) AS result;
$$
LANGUAGE sql;

CREATE FUNCTION sp_gymfit_admin_blog_save_post
(
	_country_id INT,
	_post_id INT,
	_name TEXT,
	_body_copy TEXT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	blog_post
		SET			name = _name,
						body_copy = _body_copy
		WHERE		id = _post_id;

		IF NOT FOUND THEN
			INSERT INTO blog_post (country, name, body_copy)
			VALUES			(_country_id, _name, _body_copy)
			RETURNING		id
			INTO				_post_id;
		END IF;

		RETURN to_json(_post_id);
	END;
$$
LANGUAGE plpgsql;
