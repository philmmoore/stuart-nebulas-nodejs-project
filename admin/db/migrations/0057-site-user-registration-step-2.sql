ALTER TABLE user_registration ADD first_name TEXT;
ALTER TABLE user_registration ADD last_name TEXT;
ALTER TABLE user_registration ADD username TEXT;
ALTER TABLE user_registration ADD date_of_birth DATE;

CREATE FUNCTION sp_gymfit_site_user_registration_add_step_2_details
(
	_registration_id INT,
	_first_name TEXT,
	_last_name TEXT,
	_username TEXT,
	_date_of_birth DATE
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE			user_registration
		SET					first_name = _first_name,
								last_name = _last_name,
								username = _username,
								date_of_birth = _date_of_birth
		WHERE				id = _registration_id
		RETURNING		id
		INTO				_registration_id;

		RETURN to_json(_registration_id);
	END;
$$
LANGUAGE plpgsql;
