CREATE OR REPLACE FUNCTION sp_gymfit_site_register_persist_step_6_details (
  _user_id INT,
  _mobile_number TEXT,
  _routine_type_id INT,
  _gym_id INT,
  _is_personal_trainer BOOLEAN
)
RETURNS JSON AS
$$
BEGIN
	UPDATE   "user"
  SET     mobile_number = _mobile_number,
          is_personal_trainer = _is_personal_trainer
  WHERE   id = _user_id;

  IF _routine_type_id IS NOT NULL THEN
    INSERT INTO user_routine_type (user_id, routine_type_id)
    VALUES (_user_id, _routine_type_id);
  END IF;

  IF _gym_id IS NOT NULL THEN
    INSERT INTO user_gym (user_id, gym_id)
    VALUES (_user_id, _gym_id);
  END IF;

  RETURN to_json(1);
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_registration_add_user
(
	_registration_id INT
)
RETURNS JSON AS
$$
	DECLARE
		_user_id INT;
	BEGIN
		INSERT INTO	"user" (username, hash, first_name, last_name, date_of_birth, email, avatar_uploaded)
		SELECT			username,
								hash,
								first_name,
								last_name,
								date_of_birth,
								email,
                '0'
		FROM				user_registration
		WHERE				id = _registration_id
		RETURNING		id
		INTO				_user_id;

		DELETE FROM	user_registration
		WHERE				id = _registration_id;

		INSERT INTO	user_email_verification (user_id)
		VALUES			(_user_id);

		INSERT INTO	user_notification (user_id, notification_id)
		VALUES			(_user_id, 1),
								(_user_id, 2),
								(_user_id, 3),
								(_user_id, 4);

		RETURN row_to_json(result)
		FROM (
			SELECT					u.id,
											u.email,
											ev.key AS "verificationKey"
			FROM						"user" AS u
			LEFT OUTER JOIN	user_email_verification AS ev ON ev.user_id = u.id
			WHERE						u.id = _user_id
		) AS result;
	END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_update_password
(
	_user_id INT,
	_hash BYTEA
)
RETURNS JSON AS
$$
	DECLARE
		_username TEXT;
	BEGIN
		DELETE
		FROM				user_password_reset
		WHERE				user_id = _user_id;

		UPDATE			"user" SET hash = _hash
		WHERE				id = _user_id
		RETURNING		username
		INTO				_username;

		RETURN sp_gymfit_site_user_get(_user_id);
	END
$$
LANGUAGE plpgsql;
