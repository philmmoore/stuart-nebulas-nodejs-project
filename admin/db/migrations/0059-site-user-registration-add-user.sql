CREATE FUNCTION sp_gymfit_site_user_registration_add_user
(
	_registration_id INT
)
RETURNS JSON AS
$$
	DECLARE
		_user_id INT;
	BEGIN
		INSERT INTO	"user" (username, hash, first_name, last_name, date_of_birth, email)
		SELECT			username,
								hash,
								first_name,
								last_name,
								date_of_birth,
								email
		FROM				user_registration
		WHERE				id = _registration_id
		RETURNING		id
		INTO				_user_id;

		DELETE FROM	user_registration
		WHERE				id = _registration_id;

		RETURN to_json(_user_id);
	END;
$$
LANGUAGE plpgsql;
