CREATE OR REPLACE FUNCTION sp_gymfit_api_gym_get
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
		(
			SELECT		id,
								value as name
			FROM			gym
			WHERE			country_id = _country_id
		) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_routine_type_get()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
		(
			SELECT		id,
								value as name
			FROM			routine_type
		) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_user_set_gym
(
	_user_id INT,
	_gym_id INT
)
RETURNS JSON AS
$$
	BEGIN
		DELETE FROM	user_gym
		WHERE				user_id = _user_id;

		INSERT INTO	user_gym (user_id, gym_id)
		VALUES (_user_id, _gym_id);

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_user_set_routine
(
	_user_id INT,
	_routine_type_id INT
)
RETURNS JSON AS
$$
	BEGIN
		DELETE FROM	user_routine_type
		WHERE				user_id = _user_id;

		INSERT INTO	user_routine_type (user_id, routine_type_id)
		VALUES (_user_id, _routine_type_id);

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;
