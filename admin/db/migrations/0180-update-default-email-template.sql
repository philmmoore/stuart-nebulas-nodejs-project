UPDATE email_template SET standard_template='<html>
  <head>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <style>
      body {
        font-family: "Montserrat";
        background: #f0f0f0;
        text-align: center;
      }
      
      .content {
        margin-top: 25px;
        width: 500px;
        margin-left: auto;
        margin-right: auto;
        background: white;
        padding: 15px;
        border-radius: 6px;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        box-shadow: 10px 10px 50px #888;
        text-align: center;
      }
      
      .logo {
        width: 200px;
        padding-top: 25px;
        padding-bottom: 25px;
      }
      
      h1, a, a:hover, a:focus {
        color: #6e2d91;
      }
      
      a {
        text-decoration: none;
      }
      
      .box {
        padding: 15px;
        border-radius: 6px;
        -webkit-border-radius: 6px;
        -moz-border-radius: 6px;
        background: #e1e1e1;
        margin-top: 15px;
      }
      
      .footer {
        font-size: 0.2em;
      }
    </style>
  </head>
  <body>
    <div class="content">
      <img src="https://gymfit.com/purple-logo.svg" class="logo" />
      <h1>{{SUBJECT}}</h1>
      {{BODY}}
      <p>TEAM GYMFIT</p>
    </div>
    <p class="footer">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    </p>
  </body>
</html>'
WHERE standard_template = '<html></html>';