CREATE OR REPLACE FUNCTION sp_gymfit_api_user_get_notifications
(
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
	(
		SELECT			n.id,
								n.title,
								n.description,
								n.user_dismissable AS "isUserDismissable",
								LOWER(nt.value) AS "type"
		FROM				user_notification AS un
		INNER JOIN	notification AS n
		ON					n.id = un.notification_id
		INNER JOIN	notification_type AS nt
		ON					n.type = nt.id
		WHERE				un.user_id = _user_id
		AND					un.acknowledged IS NULL
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_user_acknowledge_notification
(
	_user_id INT,
	_notification_id INT
)
RETURNS JSON AS
$$
  UPDATE			user_notification
  SET					acknowledged = current_timestamp
  WHERE				user_id = _user_id
  AND					notification_id = _notification_id;

	SELECT to_json(1);
$$
LANGUAGE sql;
