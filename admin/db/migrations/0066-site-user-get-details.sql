CREATE FUNCTION sp_gymfit_site_user_get_details
(
	_username TEXT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM (
		SELECT						first_name AS "firstName",
											last_name AS "lastName",
											is_male AS "isMale"
		FROM							"user"
		WHERE							username = LOWER(_username)
		LIMIT							1
	) AS result;
$$
LANGUAGE sql;
