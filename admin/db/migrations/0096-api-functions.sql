CREATE OR REPLACE FUNCTION sp_gymfit_api_auth_login
(
	_username_or_email TEXT,
	_hash BYTEA,
	_device_id TEXT
)
RETURNS JSON AS
$$
	DECLARE
  _user_id INT;

	BEGIN
		SELECT			id
		INTO				_user_id
		FROM				"user"
		WHERE				hash = _hash
		AND					(LOWER(username) = LOWER(_username_or_email) OR LOWER(email) = LOWER(_username_or_email));

		IF _user_id IS NOT NULL THEN
			INSERT INTO	user_audit (user_id, activity, message)
			VALUES			(_user_id,
									'app login',
									_device_id);

			DELETE
			FROM				user_password_reset
			WHERE				user_id = _user_id;
		END IF;

		RETURN sp_gymfit_site_user_get(_user_id);
	END
$$
LANGUAGE plpgsql;
