CREATE TABLE result_feedback
(
	result_id INT REFERENCES result (id) NOT NULL,
	user_id INT REFERENCES "user" (id) NOT NULL,
	flag BOOL NOT NULL,
	created TIMESTAMP DEFAULT current_timestamp NOT NULL,
	PRIMARY KEY (result_id, user_id)
);

ALTER TABLE result ADD COLUMN approved TIMESTAMP;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_result_flagged()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
	(
		SELECT					ROUND(100.0 * COUNT(CASE WHEN f.flag = TRUE THEN 1 END) / COUNT(f.flag), 2) AS "flagPercent",
										COUNT(CASE WHEN f.flag = TRUE THEN 1 END) AS "flagCount",
										COUNT(CASE WHEN f.flag = FALSE THEN 1 END) AS "likeCount",
										r.id AS "resultId",
										r.result,
										r.image_validated AS "imageValidated",
										r.video_validated AS "videoValidated",
										u.id AS "userId",
										u.username AS "username",
										c.name AS "challengeName",
										cg.value AS "challengeGroup",
										ct.measurement AS "measurement",
										(SELECT COUNT(*) FROM result WHERE excluded IS NOT NULL AND user_id = u.id) AS "numberOfExcludedResults"
		FROM						result_feedback AS f
		JOIN						result AS r on r.id = f.result_id
		JOIN						"user" AS u on u.id = r.user_id
		JOIN						challenge AS c on c.id = r.challenge_id
		JOIN						challenge_group AS cg on cg.id = r.challenge_group_id
		JOIN						challenge_type AS ct on ct.id = c.type
		WHERE						r.excluded IS NULL
		AND							r.approved IS NULL
		AND							(SELECT COUNT(*) FROM result_feedback WHERE result_id = f.result_id AND flag = TRUE) > 0
		GROUP BY				r.id,
										u.id,
										c.id,
										cg.id,
										ct.id
	 	ORDER BY				"flagPercent" DESC
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_result_approve
(
	_result_id INT
)
RETURNS JSON AS
$$
	WITH rows AS
	(
		UPDATE		result
		SET				approved = current_timestamp,
							excluded = NULL,
							exclude_reason = NULL
		WHERE 		id = _result_id
		AND				approved IS NULL
		RETURNING 1
	)
	SELECT to_json(COUNT(*)) FROM rows;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_result_reject
(
	_result_id INT,
	_reason TEXT
)
RETURNS JSON AS
$$
	WITH rows AS
	(
		UPDATE		result
		SET				excluded = current_timestamp,
							exclude_reason = _reason,
							approved = NULL
		WHERE 		id = _result_id
		AND				excluded IS NULL
		RETURNING 1
	)
	SELECT to_json(COUNT(*)) FROM rows;
$$
LANGUAGE sql;

CREATE FUNCTION sp_gymfit_admin_result_get
(
	_result_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
		(
			SELECT					ROUND(100.0 * COUNT(CASE WHEN f.flag = TRUE THEN 1 END) / COUNT(f.flag), 2) AS "flagPercent",
											COUNT(CASE WHEN f.flag = TRUE THEN 1 END) AS "flagCount",
											COUNT(CASE WHEN f.flag = FALSE THEN 1 END) AS "likeCount",
											r.id AS "resultId",
											r.result,
											r.image_validated AS "imageValidated",
											r.video_validated AS "videoValidated",
											u.id AS "userId",
											u.username AS "username",
											c.name AS "challengeName",
											cg.value AS "challengeGroup",
											ct.measurement AS "measurement",
											(SELECT COUNT(*) FROM result WHERE excluded IS NOT NULL AND user_id = u.id) AS "numberOfExcludedResults"
			FROM						result_feedback AS f
			JOIN						result AS r on r.id = f.result_id
			JOIN						"user" AS u on u.id = r.user_id
			JOIN						challenge AS c on c.id = r.challenge_id
			JOIN						challenge_group AS cg on cg.id = r.challenge_group_id
			JOIN						challenge_type AS ct on ct.id = c.type
			WHERE						r.id = _result_id
			GROUP BY				r.id,
											u.id,
											c.id,
											cg.id,
											ct.id
		) AS result_to_return;
$$
LANGUAGE sql;
