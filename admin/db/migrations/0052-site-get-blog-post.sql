CREATE OR REPLACE FUNCTION sp_gymfit_site_blog_get_post
(
	_country_id INT,
	_name TEXT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM (
		SELECT	name,
						body_copy AS "bodyCopy"
		FROM		blog_post
		WHERE		country = _country_id
		AND			LOWER(REPLACE(name, ' ', '-')) = LOWER(_name)
		LIMIT		1
	) AS result;
$$
LANGUAGE sql;
