DROP FUNCTION IF EXISTS sp_gymfit_site_register_persist_step_6_details(INT, TEXT, INT, TEXT, INT, INT);

UPDATE	challenge_category
SET			value='Multi'
WHERE		id = 3;

INSERT INTO challenge_category (value)
VALUES ('Special');

INSERT INTO challenge
(
  name,
  category,
  type,
  body_copy,
  submission_message
)
VALUES
(
  'Personal Trainer',
  4,
  2,
  '##Lorem Ipsum
dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.

https://www.youtube.com/watch?v=XQu8TTBmGhA',
'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
);

INSERT INTO challenge_group (value)
VALUES ('-');

INSERT INTO challenge_to_challenge_group (challenge_id, challenge_group_id)
VALUES (
  (SELECT id FROM challenge WHERE category = 4 LIMIT 1),
  (SELECT id FROM challenge_group WHERE value = '-' LIMIT 1)
);

CREATE OR REPLACE FUNCTION sp_gymfit_site_get_challenges()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))	FROM (
		SELECT		      c.name,
							      cc.value AS "category",
                    ct.value AS "type"
		FROM			      challenge AS c
    LEFT OUTER JOIN challenge_category AS cc ON c.category = cc.id
    LEFT OUTER JOIN challenge_type AS ct ON c.type = ct.id
		ORDER BY 	      c.name
	) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_challenge_get_challenges()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM
		(
			SELECT		      c.id,
      								c.name,
      								c.category AS categoryId,
                      cg.value AS category,
      								c.type AS typeId,
                      ct.value AS type,
      								c.body_copy AS "bodyCopy",
      								c.submission_message AS "submissionMessage",
      								(
      									array
      									(
      										SELECT			cg.value
      										FROM				challenge_group AS cg
      										INNER JOIN	challenge_to_challenge_group AS ccg
      										ON 					ccg.challenge_id = c.id
      										AND					ccg.challenge_group_id = cg.id
      										ORDER BY		cg.id
      									)
      								) AS groups
      FROM 			      challenge AS c
      LEFT OUTER JOIN challenge_category AS cg on cg.id = c.category
      LEFT OUTER JOIN challenge_type AS ct on ct.id = c.type
			ORDER BY	      c.name
		) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_challenge_get_challenge
(
	_challenge_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
		(
			SELECT	id,
							name,
							category AS "categoryId",
							type AS "typeId",
							body_copy AS "bodyCopy",
							submission_message AS "submissionMessage",
							(
								array
								(
									SELECT			cg.value
									FROM				challenge_group AS cg
									INNER JOIN	challenge_to_challenge_group AS ccg
									ON 					ccg.challenge_id = c.id
									AND					ccg.challenge_group_id = cg.id
									ORDER BY		cg.id
								)
							) AS groups
      FROM 		challenge AS c
      WHERE 	c.id = _challenge_id
			LIMIT		1
		) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_challenge_get_categories()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM
		(
			SELECT	  id,
							  value AS "name"
      FROM 		  challenge_category
      ORDER BY  value
		) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_challenge_get_types()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM
		(
			SELECT	  id,
							  value AS "name"
      FROM 		  challenge_type
      ORDER BY  value
		) AS result;
$$
LANGUAGE sql;
