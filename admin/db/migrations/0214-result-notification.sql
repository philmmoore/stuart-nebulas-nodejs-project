CREATE OR REPLACE FUNCTION sp_gymfit_shared_result_notification
(
	_user_id INT,
	_result_id BIGINT
)
RETURNS VOID AS
$$
	INSERT INTO social_notification (user_id, notification)
	VALUES (
		_user_id,
		(
			SELECT row_to_json(social_notification)
			FROM
			(
				SELECT					'result'::TEXT AS "type",
												r.id AS "resultId",
												r.result,
												u.id AS "userId",
												u.username,
												u.first_name AS "firstName",
												u.last_name AS "lastName",
												u.avatar_uploaded AS "avatarUploaded",
												c.id AS "challengeId",
												c.name AS "challengeName",
												cg.id AS "challengeGroupId",
												cg.value AS "challengeGroupName",
												cc.value AS "challengeCategory",
												cc.id AS "challengeCategoryId",
												ct.measurement AS "measurement"
				FROM						result AS r
				JOIN						"user" AS u ON u.id = r.user_id
				JOIN						challenge AS c on c.id = r.challenge_id
				JOIN 						challenge_group AS cg ON r.challenge_group_id = cg.id
				JOIN 						challenge_type AS ct on c.type = ct.id
				JOIN 						challenge_category AS cc ON cc.id = c.category
				WHERE						r.id = _result_id
			) AS social_notification
		)
	);
$$
LANGUAGE sql;

SELECT	sp_gymfit_shared_result_notification(user_id, id)
FROM 		result
WHERE		excluded IS NULL;

CREATE OR REPLACE FUNCTION sp_gymfit_site_result_submit
(
	_user_id INT,
	_challenge_id INT,
	_challenge_group_id INT,
	_result INT,
	_type TEXT
)
RETURNS JSON AS
$$
	DECLARE
		_result_id INT;
	BEGIN
		INSERT INTO	result (user_id,
												challenge_id,
												challenge_group_id,
												result,
												country_id,
												region_id,
												county_id,
												district_id,
                        age,
                        weight,
												type)
		SELECT      _user_id,
          			_challenge_id,
          			_challenge_group_id,
          			_result,
          			cty.id,
          			r.id,
          			c.id,
          			ud.district_id,
                extract(year FROM age(date_of_birth)),
                (
                  SELECT    weight
                  FROM      user_weight
                  WHERE     user_id = _user_id
                  ORDER BY  created DESC
                  LIMIT     1
								),
								_type
		FROM        "user" AS u
    INNER JOIN  user_district AS ud ON u.id = ud.user_id
    INNER JOIN  district AS d ON ud.district_id = d.id
    INNER JOIN  county AS c ON d.county_id = c.id
    INNER JOIN  region AS r ON c.region_id = r.id
    INNER JOIN  country AS cty ON r.country_id = cty.id
    WHERE       u.id = _user_id
		ORDER BY		ud.created DESC
		LIMIT 1
		RETURNING		id
		INTO				_result_id;
		
		PERFORM sp_gymfit_shared_result_notification(_user_id, _result_id);

		RETURN to_json(_result_id);
	END
$$
LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION sp_gymfit_api_result_submit
(
	_user_id INT,
	_challenge_id INT,
	_challenge_group_id INT,
	_result INT
)
RETURNS JSON AS
$$
	DECLARE
		_result_id INT;
	BEGIN
		INSERT INTO	result (
													user_id,
													challenge_id,
													challenge_group_id,
													result,
													country_id,
													region_id,
													county_id,
													district_id,
	                        age,
	                        weight
												)
		SELECT      _user_id,
          			_challenge_id,
          			_challenge_group_id,
          			_result,
          			cty.id,
          			r.id,
          			c.id,
          			ud.district_id,
                extract(year FROM age(date_of_birth)),
                (
                  SELECT    weight
                  FROM      user_weight
                  WHERE     user_id = _user_id
                  ORDER BY  created DESC
                  LIMIT     1
								)
		FROM        "user" AS u
    INNER JOIN  user_district AS ud ON u.id = ud.user_id
    INNER JOIN  district AS d ON ud.district_id = d.id
    INNER JOIN  county AS c ON d.county_id = c.id
    INNER JOIN  region AS r ON c.region_id = r.id
    INNER JOIN  country AS cty ON r.country_id = cty.id
    WHERE       u.id = _user_id
		ORDER BY		ud.created DESC
		LIMIT 1
		RETURNING		id
		INTO				_result_id;
		
		PERFORM sp_gymfit_shared_result_notification(_user_id, _result_id);

		RETURN to_json(_result_id);
	END
$$
LANGUAGE plpgsql;

DROP FUNCTION sp_gymfit_task_runner_evidence_set(INT, BIT);

CREATE OR REPLACE FUNCTION sp_gymfit_task_runner_evidence_set
(
  _result_id BIGINT,
  _is_image BIT
)
RETURNS JSON AS
$$
BEGIN
	UPDATE  result
  SET     image_validated = current_timestamp
  WHERE   id = _result_id
	AND			_is_image = '1';

	UPDATE  result
  SET     video_validated = current_timestamp
	WHERE   id = _result_id
	AND			_is_image = '0';
	
	INSERT INTO social_notification (user_id, notification)
	VALUES (
		(SELECT user_id FROM result WHERE id = _result_id),
		(
			SELECT row_to_json(social_notification)
			FROM
			(
				SELECT					'result-evidence'::TEXT AS "type",
												r.id AS "resultId",
												r.result,
												u.id AS "userId",
												u.username,
												u.first_name AS "firstName",
												u.last_name AS "lastName",
												u.avatar_uploaded AS "avatarUploaded",
												c.id AS "challengeId",
												c.name AS "challengeName",
												cg.id AS "challengeGroupId",
												cg.value AS "challengeGroupName",
												cc.value AS "challengeCategory",
												cc.id AS "challengeCategoryId",
												ct.measurement AS "measurement",
												r.image_validated AS "imageValidated",
												r.video_validated AS "videoValidated"
				FROM						result AS r
				JOIN						"user" AS u ON u.id = r.user_id
				JOIN						challenge AS c on c.id = r.challenge_id
				JOIN 						challenge_group AS cg ON r.challenge_group_id = cg.id
				JOIN 						challenge_type AS ct on c.type = ct.id
				JOIN 						challenge_category AS cc ON cc.id = c.category

				WHERE						r.id = _result_id
			) AS social_notification
		)
	);

  RETURN to_json(1);
END;
$$
LANGUAGE plpgsql;

INSERT INTO social_notification (user_id, notification)
SELECT 
		x.user_id,
		(SELECT row_to_json(social_notification)
		FROM
		(
			SELECT					'result-evidence'::TEXT AS "type",
											r.id AS "resultId",
											r.result,
											u.id AS "userId",
											u.username,
											u.first_name AS "firstName",
											u.last_name AS "lastName",
											u.avatar_uploaded AS "avatarUploaded",
											c.id AS "challengeId",
											c.name AS "challengeName",
											cg.id AS "challengeGroupId",
											cg.value AS "challengeGroupName",
											cc.value AS "challengeCategory",
											cc.id AS "challengeCategoryId",
											ct.measurement AS "measurement",
											r.image_validated AS "imageValidated",
											r.video_validated AS "videoValidated"
			FROM						result AS r
			JOIN						"user" AS u ON u.id = r.user_id
			JOIN						challenge AS c on c.id = r.challenge_id
			JOIN 						challenge_group AS cg ON r.challenge_group_id = cg.id
			JOIN 						challenge_type AS ct on c.type = ct.id
			JOIN 						challenge_category AS cc ON cc.id = c.category

			WHERE						r.id = x.id
		) AS social_notification)
	FROM result AS x
	WHERE excluded IS NULL
	AND (image_validated IS NOT NULL OR video_validated IS NOT NULL);