CREATE OR REPLACE FUNCTION sp_gymfit_api_user_follow
(
	_user_id INT,
	_follower_user_id INT
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO	user_follower (user_id, follower_user_id)
		SELECT			_user_id, _follower_user_id
		WHERE
    	NOT EXISTS (
        SELECT created FROM user_follower WHERE user_id = _user_id AND follower_user_id = _follower_user_id
    	);

		RETURN to_json(1);
	END
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_user_unfollow
(
	_user_id INT,
	_follower_user_id INT
)
RETURNS JSON AS
$$
	BEGIN
		DELETE FROM	user_follower
		WHERE 			user_id = _user_id AND follower_user_id = _follower_user_id;
		
		RETURN to_json(1);
	END
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_user_set_client
(
	_user_id INT,
	_client_user_id INT
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO	user_client (user_id, client_user_id)
		SELECT			_user_id, _client_user_id
		WHERE
    	NOT EXISTS (
        SELECT created FROM user_client WHERE user_id = _user_id AND client_user_id = _client_user_id
    	);

		RETURN to_json(1);
	END
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_user_unset_client
(
	_user_id INT,
	_client_user_id INT
)
RETURNS JSON AS
$$
	BEGIN
		DELETE FROM	user_client
		WHERE 			user_id = _user_id AND client_user_id = _client_user_id;
		
		RETURN to_json(1);
	END
$$
LANGUAGE plpgsql;