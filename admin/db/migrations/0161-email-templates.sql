CREATE OR REPLACE FUNCTION sp_gymfit_api_user_search
(
	_search_query TEXT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
	(
		SELECT					id,
										username,
										first_name AS "firstName",
										last_name AS "lastName",
										avatar_uploaded AS "avatarUploaded"
		FROM						"user"
		WHERE						LOWER(username) LIKE LOWER(_search_query) || '%'
		OR							LOWER(first_name) LIKE LOWER(_search_query) || '%'
		OR							LOWER(last_name) LIKE LOWER(_search_query) || '%'
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_search
(
	_search_query TEXT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
	(
		SELECT					id,
										username,
										first_name AS "firstName",
										last_name AS "lastName",
										avatar_uploaded AS "avatarUploaded"
		FROM						"user"
		WHERE						LOWER(username) LIKE LOWER(_search_query) || '%'
		OR							LOWER(first_name) LIKE LOWER(_search_query) || '%'
		OR							LOWER(last_name) LIKE LOWER(_search_query) || '%'
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE TABLE email_template
(
  id SERIAL PRIMARY KEY,
  country_id INT REFERENCES country (id) NOT NULL,
  template TEXT NOT NULL
);

INSERT INTO email_template (country_id, template)
VALUES (1, '<html></html>');

CREATE FUNCTION sp_gymfit_admin_email_template_get
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)	FROM (
		SELECT	template AS "standardTemplate"
		FROM		email_template
		WHERE		country_id = _country_id
		LIMIT		1
	) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_email_template_set
(
	_country_id INT,
	_template TEXT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	email_template
		SET			template = _template
		WHERE		country_id = _country_id;

		RETURN to_json(1);
	END
$$
LANGUAGE plpgsql;
