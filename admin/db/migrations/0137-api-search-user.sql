CREATE OR REPLACE FUNCTION sp_gymfit_api_user_search
(
	_search_query TEXT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
	(
		SELECT					id,
										username,
										first_name AS "firstName",
										last_name AS "lastName",
										avatar_uploaded AS "avatarUploaded"
		FROM						"user"
		WHERE						username LIKE _search_query || '%'
		OR							first_name LIKE _search_query || '%'
		OR							last_name LIKE _search_query || '%'
	) AS result_to_return;
$$
LANGUAGE sql;
