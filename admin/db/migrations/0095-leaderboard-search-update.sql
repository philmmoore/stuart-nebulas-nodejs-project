ALTER TABLE result RENAME COLUMN validated TO image_validated;
ALTER TABLE result ADD video_validated TIMESTAMP;
ALTER TABLE challenge_type ADD measurement TEXT;

UPDATE 	challenge_type
SET 		measurement='time'
WHERE		id = 1;

UPDATE 	challenge_type
SET 		measurement='count'
WHERE		id IN (2, 3);

UPDATE 	challenge_type
SET 		measurement='distance'
WHERE		id IN (4, 5);

ALTER TABLE challenge_type ALTER measurement SET NOT NULL;

DROP FUNCTION sp_gymfit_site_leaderboard_search(INT, INT);

CREATE OR REPLACE FUNCTION sp_gymfit_site_leaderboard_search
(
	_challenge_id INT,
	_challenge_group_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_last_period_start TIMESTAMP,
	_last_period_end TIMESTAMP,
	_results_per_page INT,
	_page INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		SELECT
		(
			SELECT array_to_json(array_agg(results))
			FROM
			(
				SELECT			rank() over (ORDER BY r.result DESC),
										r.user_id AS "userId",
										u.username,
										u.first_name "firstName",
										u.last_name "lastName",
										r.image_validated AS "imageValidated",
										r.video_validated AS "videoValidated",
										MAX(r.result) AS "result"
				from				result AS r
				INNER JOIN	"user" AS u
				ON					r.user_id = u.id
				WHERE				r.submitted >= _this_period_start
				AND					r.submitted <= _this_period_end
				AND					r.challenge_id = _challenge_id
				AND					r.challenge_group_id = _challenge_group_id
				GROUP BY		r.user_id,
										r.result,
										u.username,
										u.first_name,
										u.last_name,
										r.image_validated,
										r.video_validated
				ORDER BY		MAX(r.result) DESC
				OFFSET			(_page - 1) * _results_per_page
				LIMIT				_results_per_page
			) AS results
		) AS "results",
		(
			SELECT				ceiling(COUNT(DISTINCT r.user_id)::DECIMAL / _results_per_page)
			FROM					result AS r
			WHERE					r.submitted >= _this_period_start
			AND						r.submitted <= _this_period_end
			AND						r.challenge_id = _challenge_id
			AND						r.challenge_group_id = _challenge_group_id
		) AS "numberOfPages",
		(
			SELECT				ct.measurement
			FROM					challenge_type AS ct
			LEFT OUTER JOIN challenge AS c ON c.type = ct.id
			WHERE					c.id = _challenge_id
		) AS "measurement"
	) AS result_to_return;
$$
LANGUAGE sql;
