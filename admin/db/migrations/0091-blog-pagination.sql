DROP FUNCTION sp_gymfit_site_blog_get_posts(INT);

CREATE OR REPLACE FUNCTION sp_gymfit_site_blog_get_posts
(
	_country_id INT,
	_posts_per_page INT,
	_page INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
		(
			SELECT	ceiling(count(*)::DECIMAL / _posts_per_page) AS "numberOfPages",
							(
								SELECT array_to_json(array_agg(result_b))
								FROM (
									SELECT		name,
														body_copy AS "bodyCopy"
									FROM			blog_post
									WHERE			country = _country_id
									ORDER BY	name
									OFFSET		(_page - 1) * _posts_per_page
									LIMIT			_posts_per_page
								) AS result_b
						) AS "posts"
		FROM 		blog_post
	) AS result;
$$
LANGUAGE sql;
