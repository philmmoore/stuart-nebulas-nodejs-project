CREATE OR REPLACE FUNCTION sp_gymfit_task_runner_leaderboard_exclude_unvalidated
(
	_days_to_keep_unvalidated_results_on_leaderboard INT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	result
		SET			excluded=current_timestamp,
						exclude_reason='not validated'
		WHERE		image_validated IS NULL
		AND			video_validated IS NULL
		AND			date_part('day', current_timestamp - submitted) > _days_to_keep_unvalidated_results_on_leaderboard
		AND			excluded IS NULL;

		RETURN to_json(1);
	END
$$
LANGUAGE plpgsql;
