CREATE OR REPLACE FUNCTION sp_gymfit_api_user_get_details
(
	_username TEXT
)
RETURNS JSON AS
$$
	SELECT sp_gymfit_site_user_get(
		(
			SELECT	id
			FROM		"user"
			WHERE		username = LOWER(_username)
			LIMIT		1
		)
	);
$$
LANGUAGE sql;
