CREATE OR REPLACE FUNCTION public.sp_gymfit_api_user_search(_search_query text)
  RETURNS json AS
$BODY$
  SELECT array_to_json(array_agg(result_to_return))
  FROM
  (
    SELECT          id,
                    username,
                    first_name AS "firstName",
                    last_name AS "lastName",
                    is_personal_trainer AS "isPersonalTrainer",
                    is_gym AS "isGym",
                    avatar_uploaded AS "avatarUploaded",
                    bio
    FROM            "user"
    WHERE           LOWER(username) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
    OR              LOWER(first_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
    OR              LOWER(last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
    OR              LOWER(first_name || last_name) LIKE LOWER(REPLACE(_search_query, ' ', '')) || '%'
  ) AS result_to_return;
$BODY$
  LANGUAGE sql VOLATILE
  COST 100;