CREATE FUNCTION sp_gymfit_site_user_get
(
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		SELECT		u.id,
							u.username,
							u.first_name AS "firstName",
							u.last_name AS "lastName",
							u.is_male AS "isMale",
							u.date_of_birth AS "dateOfBirth",
							(
								SELECT row_to_json(result_location)
								FROM
								(
									SELECT
									(
										SELECT row_to_json(result_a)
										FROM
										(
											SELECT	d.id,
												d.name
										) AS result_a
									) AS "district",
									(
										SELECT row_to_json(result_b)
										FROM
										(
											SELECT	c.id,
												c.name
										) AS result_b
									) AS "county",
									(
										SELECT row_to_json(result_c)
										FROM
										(
											SELECT	r.id,
												r.name
										) AS result_c
									) AS "region",
									(
										SELECT row_to_json(result_d)
										FROM
										(
											SELECT	ct.id,
												ct.name,
												ct.code
										) AS result_d
									) AS "country"
								) AS result_location
							) AS "location"
		FROM		"user" AS u
		LEFT OUTER JOIN	user_district AS ud
		ON							u.id = ud.user_id
		LEFT OUTER JOIN	district AS d
		ON							ud.district_id = d.id
		LEFT OUTER JOIN	county AS c
		ON							d.county_id = c.id
		LEFT OUTER JOIN region AS r
		ON							c.region_id = r.id
		LEFT OUTER JOIN	country AS ct
		ON							r.country_id = ct.id
		WHERE						u.id = _user_id
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_get_details
(
	_username_or_email TEXT,
	_hash BYTEA,
	_ip_address TEXT
)
RETURNS JSON AS
$$
	DECLARE
  _user_id INT;

	BEGIN
		SELECT			id
		INTO				_user_id
		FROM				"user"
		WHERE				hash = _hash
		AND					(LOWER(username) = LOWER(_username_or_email) OR LOWER(email) = LOWER(_username_or_email));

		IF _user_id IS NOT NULL THEN
			INSERT INTO	user_audit (user_id, activity, message)
			VALUES			(_user_id,
									'login',
									_ip_address);

			DELETE
			FROM				user_password_reset
			WHERE				user_id = _user_id;
		END IF;

		RETURN sp_gymfit_site_user_get(_user_id);
	END
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_get_details
(
	_username TEXT
)
RETURNS JSON AS
$$
	SELECT sp_gymfit_site_user_get(
		(
			SELECT	id
			FROM		"user"
			WHERE		username = LOWER(_username)
			LIMIT		1
		)
	);
$$
LANGUAGE sql;
