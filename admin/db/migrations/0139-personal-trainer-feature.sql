ALTER TABLE "user" ADD is_personal_trainer BOOLEAN DEFAULT FALSE NOT NULL;

DROP FUNCTION IF EXISTS sp_gymfit_site_register_persist_step_6_details(INT, TEXT, INT, INT);

CREATE OR REPLACE FUNCTION sp_gymfit_site_register_persist_step_6_details (
  _user_id INT,
  _mobile_number TEXT,
  _routine_type_id INT,
  _gym_id INT,
  _is_personal_trainer BOOLEAN
)
RETURNS JSON AS
$$
BEGIN
	UPDATE   "user"
  SET     mobile_number = _mobile_number,
          is_personal_trainer = _is_personal_trainer
  WHERE   id = _user_id;

  IF _routine_type_id IS NOT NULL THEN
    INSERT INTO user_routine_type (user_id, routine_type_id)
    VALUES (_user_id, _routine_type_id);
  END IF;

  IF _gym_id IS NOT NULL THEN
    INSERT INTO user_gym (user_id, gym_id)
    VALUES (_user_id, _gym_id);
  END IF;

  RETURN sp_gymfit_site_user_get(_user_id);
END;
$$
LANGUAGE plpgsql;
