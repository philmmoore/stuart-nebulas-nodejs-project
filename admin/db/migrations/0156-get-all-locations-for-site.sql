CREATE OR REPLACE FUNCTION sp_gymfit_site_location_get_all()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
		(
			SELECT						d.name AS "district",
												d.id AS "districtId",
												c.name AS "county",
												c.id AS "countyId",
												r.name AS "region",
												r.id AS "regionId",
												ct.name AS "country",
												ct.id AS "countryId",
												ct.code AS "countryCode"
			FROM							district AS d
			LEFT OUTER JOIN 	county AS c ON d.county_id = c.id
			LEFT OUTER JOIN 	region AS r ON c.region_id = r.id
			LEFT OUTER JOIN 	country AS ct ON r.country_id = ct.id
		) AS result_to_return;
$$
LANGUAGE sql;
