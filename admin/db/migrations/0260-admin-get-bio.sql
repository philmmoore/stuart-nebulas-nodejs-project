CREATE OR REPLACE FUNCTION sp_gymfit_admin_user_get_all()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
		(
			SELECT			u.id,
									u.username,
									u.first_name AS "firstName",
									u.last_name AS "lastName",
									u.created AS "registrationDate",
									(
										SELECT		timestamp
										FROM			user_audit
										WHERE			user_id = u.id
										ORDER BY	timestamp DESC
										LIMIT 		1
									) AS "lastLoginDate",
									u.blocked,
									u.avatar_uploaded AS "avatarUploaded",
                  u.bio
			FROM				"user" AS u
			ORDER BY		u.username
		) AS result_to_return;
$$
LANGUAGE sql;