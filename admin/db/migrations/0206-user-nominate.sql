ALTER TABLE user_email_preference ADD column nomination BOOL;
UPDATE user_email_preference SET nomination=TRUE;
ALTER TABLE user_email_preference ALTER COLUMN nomination SET NOT NULL;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_nominate
(
	_user_id INT,
	_challenge_id INT,
	_username TEXT
)
RETURNS JSON AS
$$
	DECLARE
		_nominee_user_id INT;
	BEGIN
		INSERT INTO user_nomination
		(
			user_id,
			challenge_id,
			nominee_user_id
		)
		SELECT		_user_id,
							_challenge_id,
							u.id
		FROM 			"user" AS u
		WHERE			u.username = LOWER(_username)
		AND				(SELECT COUNT(*) FROM user_nomination WHERE user_id = _user_id AND challenge_id = _challenge_id AND nominee_user_id = u.id) = 0
		RETURNING nominee_user_id
		INTO 			_nominee_user_id;
		
		RETURN row_to_json(result_to_return)
		FROM
		(
			SELECT	id,
							first_name AS "firstName",
							last_name AS "lastName",
							username,
							(CASE
								(SELECT nomination FROM user_email_preference WHERE user_id = _nominee_user_id ORDER BY updated DESC LIMIT 1)
								AND (SELECT gymfit_email_opt_in FROM user_privacy_preferences WHERE user_id = _nominee_user_id ORDER BY updated DESC LIMIT 1)
								WHEN TRUE
								THEN email
								ELSE NULL
								END) AS "email"
			FROM		"user"
			WHERE		id = _nominee_user_id
		) AS result_to_return;
	END
$$
LANGUAGE plpgsql;

DROP FUNCTION sp_gymfit_site_user_set_email_preference(INT, BOOLEAN, BOOLEAN, BOOLEAN, BOOLEAN);

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_set_email_preference
(
	_user_id 		INT,
	_ranking		BOOLEAN,
	_progress		BOOLEAN,
	_news				BOOLEAN,
	_promotion	BOOLEAN,
	_nomination BOOLEAN
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO	user_email_preference
		(
			user_id,
			ranking,
			progress,
			news,
			promotion,
			nomination
		)
		VALUES
		(
			_user_id,
			_ranking,
			_progress,
			_news,
			_promotion,
			_nomination
		);

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_get_email_preference
(
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		SELECT					ranking,
										progress,
										news,
										promotion,
										nomination
		FROM						user_email_preference
		WHERE						user_id = _user_id
		ORDER BY				updated DESC
		LIMIT						1
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_set_privacy
(
	_user_id 								INT,
	_gymfit_email_opt_in		BOOLEAN,
	_partners_email_opt_in	BOOLEAN,
	_gymfit_sms_opt_in			BOOLEAN,
	_partners_sms_opt_in		BOOLEAN,
	_gymfit_online_opt_in		BOOLEAN,
	_partners_online_opt_in	BOOLEAN
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO	user_privacy_preferences
		(
			user_id,
			gymfit_email_opt_in,
			partners_email_opt_in,
			gymfit_sms_opt_in,
			partners_sms_opt_in,
			gymfit_online_opt_in,
			partners_online_opt_in
		)
		VALUES
		(
			_user_id,
			_gymfit_email_opt_in,
			_partners_email_opt_in,
			_gymfit_sms_opt_in,
			_partners_sms_opt_in,
			_gymfit_online_opt_in,
			_partners_online_opt_in
		);
		
		INSERT INTO user_email_preference (user_id, ranking, progress, news, promotion, nomination)
		SELECT			id,
								TRUE,
								TRUE,
								TRUE,
								TRUE,
								TRUE
		FROM				"user"
		WHERE				id NOT IN (SELECT DISTINCT user_id from user_email_preference)
		AND					id = _user_id;

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_user_set_privacy
(
	_user_id 								INT,
	_gymfit_email_opt_in		BOOLEAN,
	_partners_email_opt_in	BOOLEAN,
	_gymfit_sms_opt_in			BOOLEAN,
	_partners_sms_opt_in		BOOLEAN,
	_gymfit_online_opt_in		BOOLEAN,
	_partners_online_opt_in	BOOLEAN
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO	user_privacy_preferences
		(
			user_id,
			gymfit_email_opt_in,
			partners_email_opt_in,
			gymfit_sms_opt_in,
			partners_sms_opt_in,
			gymfit_online_opt_in,
			partners_online_opt_in
		)
		VALUES
		(
			_user_id,
			_gymfit_email_opt_in,
			_partners_email_opt_in,
			_gymfit_sms_opt_in,
			_partners_sms_opt_in,
			_gymfit_online_opt_in,
			_partners_online_opt_in
		);

		INSERT INTO user_email_preference (user_id, ranking, progress, news, promotion, nomination)
		SELECT			id,
								TRUE,
								TRUE,
								TRUE,
								TRUE,
								TRUE
		FROM				"user"
		WHERE				id NOT IN (SELECT DISTINCT user_id from user_email_preference)
		AND					id = _user_id;

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;