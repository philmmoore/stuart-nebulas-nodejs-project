CREATE FUNCTION sp_admin_save_page
(
	_countryId INT,
	_pageId INT,
	_name TEXT,
	_body_copy TEXT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	page
		SET			name = _name,
						body_copy = _body_copy
		WHERE		id = _pageId;

		IF NOT FOUND THEN
			INSERT INTO page (country, name, body_copy)
			VALUES			(_countryId, _name, _body_copy)
			RETURNING		id
			INTO				_pageId;
		END IF;

		RETURN to_json(_pageId);
	END;
$$
LANGUAGE plpgsql;
