CREATE OR REPLACE FUNCTION sp_site_get_adverts_for_location
(
	_country INT,
	_region INT,
	_county INT,
	_district INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))	FROM (
		SELECT		id,
							link,
							small_horizontal_image_extension AS "smallHorizontalImageExtension",
							large_vertical_image_extension AS "largeVerticalImageExtension"
	  FROM		 	advert
		WHERE		 	id IN (
			SELECT advert_id
			FROM (
				SELECT   advert_id, 1 AS priority
				FROM     advert_to_area
				WHERE    area_id = _district

				UNION

				SELECT   advert_id, 2 AS priority
				FROM     advert_to_area
				WHERE    area_id = _county

				UNION

				SELECT   advert_id, 3 AS priority
				FROM     advert_to_area
				WHERE    area_id = _region

				UNION

				SELECT   advert_id, 4 AS priority
				FROM     advert_to_area
				WHERE    area_id = _country
			) AS id
			ORDER BY priority
			LIMIT 2
		)
	) AS result;
$$
LANGUAGE sql;
