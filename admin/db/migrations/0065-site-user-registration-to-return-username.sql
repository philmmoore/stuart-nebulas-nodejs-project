CREATE OR REPLACE FUNCTION sp_gymfit_site_user_registration_add_user
(
	_registration_id INT
)
RETURNS JSON AS
$$
	DECLARE
		_user_id INT;
	BEGIN
		INSERT INTO	"user" (username, hash, first_name, last_name, date_of_birth, email)
		SELECT			username,
								hash,
								first_name,
								last_name,
								date_of_birth,
								email
		FROM				user_registration
		WHERE				id = _registration_id
		RETURNING		id
		INTO				_user_id;

		DELETE FROM	user_registration
		WHERE				id = _registration_id;

		RETURN row_to_json(result)
		FROM (
			SELECT			id,
									username
			FROM				"user"
			WHERE				id = _user_id
		) AS result;
	END;
$$
LANGUAGE plpgsql;
