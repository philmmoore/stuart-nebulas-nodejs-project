CREATE OR REPLACE FUNCTION sp_gymfit_task_runner_get_abandoned_registration_emails()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
	(
		SELECT	DISTINCT email
		FROM	user_registration
		WHERE	created < CURRENT_DATE - interval '1 day'
		AND	reminder_email_sent IS NULL
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_task_runner_mark_abandoned_registration_reminded
(
	_email TEXT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	user_registration
		SET			reminder_email_sent = current_timestamp
		WHERE		email = _email
		AND			reminder_email_sent IS NULL;

		RETURN to_json(1);
	END
$$
LANGUAGE plpgsql;