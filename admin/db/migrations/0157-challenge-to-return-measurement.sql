CREATE OR REPLACE FUNCTION sp_gymfit_site_get_challenge
(
	_name TEXT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)	FROM (
		SELECT	c.id,
						c.name,
						c.body_copy AS "bodyCopy",
						c.submission_message AS "submissionMessage",
						cc.value AS "category",
						ct.measurement AS "measurement"
		FROM		challenge AS c
		LEFT OUTER JOIN challenge_category AS cc on c.category = cc.id
		LEFT OUTER JOIN challenge_type AS ct on c.type = ct.id
		WHERE		LOWER(REPLACE(c.name, ' ', '-')) = LOWER(_name)
		LIMIT		1
	) AS result;
$$
LANGUAGE sql;
