CREATE TABLE notification_type (
  id SERIAL PRIMARY KEY,
  value TEXT NOT NULL
);

INSERT INTO notification_type (value)
VALUES
	('Info'),
	('Warning'),
	('Danger');

CREATE TABLE notification (
	id SERIAL PRIMARY KEY,
	type INT REFERENCES notification_type (id) NOT NULL,
  title TEXT NOT NULL,
	user_dismissable BIT NOT NULL,
	description TEXT NOT NULL
);

INSERT INTO notification (type, user_dismissable, title, description)
VALUES
	(1, '1', 'Welcome to GymFit', 'Why not upload a profile picture so the world know what you look like?'),
	(2, '0', 'Email Not Verified', 'Check your email account for a verification email form GymFit. You need a verified email address before you can submit a result.'),
	(2, '0', 'Location Not Set', 'You need to specify your location before you can submit a result so you can be ranked geographically.'),
	(2, '0', 'Weight Not Set', 'You need to specify your weight before you can submit a result so you will be ranked accordinly.');

CREATE TABLE user_notification (
	id SERIAL PRIMARY KEY,
	timestamp TIMESTAMP NOT NULL default current_timestamp,
	user_id INT REFERENCES "user" (id) NOT NULL,
	notification_id INT REFERENCES notification (id) NOT NULL,
	acknowledged TIMESTAMP
);

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_get_notifications
(
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
	(
		SELECT			n.id,
								n.title,
								n.description,
								n.user_dismissable AS "isUserDismissable",
								LOWER(nt.value) AS "type"
		FROM				user_notification AS un
		INNER JOIN	notification AS n
		ON					n.id = un.notification_id
		INNER JOIN	notification_type AS nt
		ON					n.type = nt.id
		WHERE				un.user_id = _user_id
		AND					un.acknowledged IS NULL
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_acknowledge_notification
(
	_user_id INT,
	_notification_id INT
)
RETURNS JSON AS
$$
  UPDATE			user_notification
  SET					acknowledged = current_timestamp
  WHERE				user_id = _user_id
  AND					notification_id = _notification_id;

	SELECT to_json(1);
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_registration_add_user
(
	_registration_id INT
)
RETURNS JSON AS
$$
	DECLARE
		_user_id INT;
	BEGIN
		INSERT INTO	"user" (username, hash, first_name, last_name, date_of_birth, email)
		SELECT			username,
								hash,
								first_name,
								last_name,
								date_of_birth,
								email
		FROM				user_registration
		WHERE				id = _registration_id
		RETURNING		id
		INTO				_user_id;

		DELETE FROM	user_registration
		WHERE				id = _registration_id;

		INSERT INTO	user_email_verification (user_id)
		VALUES			(_user_id);

		INSERT INTO	user_notification (user_id, notification_id)
		VALUES			(_user_id, 1),
								(_user_id, 2),
								(_user_id, 3),
								(_user_id, 4);

		RETURN row_to_json(result)
		FROM (
			SELECT					u.id,
											u.username,
											u.email,
											ev.key AS "verificationKey"
			FROM						"user" AS u
			LEFT OUTER JOIN	user_email_verification AS ev ON ev.user_id = u.id
			WHERE						u.id = _user_id
		) AS result;
	END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_associate_with_district
(
	_user_id INT,
	_district_id INT
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO	user_district (user_id, district_id)
		VALUES (_user_id, _district_id);

		UPDATE			user_notification
		SET					acknowledged = current_timestamp
		WHERE				user_id = _user_id
		AND					notification_id = 3;

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_register_set_parameters
(
	_user_id INT,
	_is_male BIT,
	_height SMALLINT,
	_weight INT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE "user" set is_male = _is_male
		WHERE id = _user_id;

		INSERT INTO	user_height (user_id, height)
		VALUES (_user_id, _height);

		INSERT INTO	user_weight (user_id, weight)
		VALUES (_user_id, _weight);

		UPDATE			user_notification
		SET					acknowledged = current_timestamp
		WHERE				user_id = _user_id
		AND					notification_id = 4;

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_confirm_email
(
	_key UUID
)
RETURNS JSON AS
$$
	DECLARE
		_username TEXT;
	BEGIN
		DELETE
		FROM				user_email_verification AS ev
		USING				"user" AS u
		WHERE				ev.user_id = u.id
		AND					ev.key = _key
		RETURNING		u.username
		INTO				_username;

		UPDATE			user_notification
		SET					acknowledged = current_timestamp
		WHERE				user_id = (SELECT id from "user" WHERE username = _username LIMIT 1)
		AND					notification_id = 2;

		RETURN to_json(_username);
	END
$$
LANGUAGE plpgsql;
