CREATE OR REPLACE FUNCTION sp_gymfit_api_auth_password_reset(_userId integer)
  RETURNS json AS
$BODY$
DECLARE
  _key UUID;
BEGIN

  DELETE
  FROM        user_password_reset
  WHERE       user_id = _userId;

  INSERT INTO user_password_reset (user_id)
  SELECT      id
  FROM        "user"
  WHERE       id = _userId
  RETURNING   key
  INTO        _key;

  RETURN row_to_json(result)
  FROM
  (
    SELECT  _key AS "key", email
    FROM    "user"
    WHERE   id = _userId
  ) AS result;
 
END
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

 CREATE OR REPLACE FUNCTION public.sp_gymfit_api_auth_user_exists(_userid integer)
  RETURNS json AS
$BODY$

SELECT to_json(result_to_return)
  FROM
  (
    SELECT          COUNT(1)
    FROM            "user"
    WHERE           id = _userId
  ) AS result_to_return;

$BODY$
  LANGUAGE sql VOLATILE
  COST 100;