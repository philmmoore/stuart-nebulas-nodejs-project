INSERT INTO challenge_type (value, measurement, min_first) VALUES ('How many clients', 'client-count', FALSE);

UPDATE challenge SET type = (SELECT id FROM challenge_type WHERE measurement = 'client-count' LIMIT 1)
WHERE id =
(
	SELECT c.id FROM challenge AS c
	JOIN challenge_category AS cc ON c.category = cc.id
	WHERE cc.value = 'Special'
);