CREATE OR REPLACE FUNCTION sp_gymfit_site_location_get_districts_for_user_region
(
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array(
		SELECT			d.name
		FROM				district AS d
		INNER JOIN	county AS c on c.id = d.county_id
		INNER JOIN	region AS r on r.id = c.region_id
		WHERE				r.id = (
													SELECT			region_id
													FROM 				user_region
													WHERE 			user_id = _user_id
													ORDER BY		created
													DESC LIMIT 	1
												)
	));
$$
LANGUAGE sql;

DROP FUNCTION sp_gymfit_site_user_register_set_parameters(INT, BIT, SMALLINT, INT);

CREATE FUNCTION sp_gymfit_site_user_register_set_parameters
(
	_user_id INT,
	_is_male BIT,
	_height SMALLINT,
	_weight INT,
	_district_name TEXT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE "user" set is_male = _is_male
		WHERE id = _user_id;

		INSERT INTO	user_height (user_id, height)
		VALUES (_user_id, _height);

		INSERT INTO	user_weight (user_id, weight)
		VALUES (_user_id, _weight);

		INSERT INTO	user_district (user_id, district_id)
		VALUES (_user_id, (
			SELECT			d.id
			FROM				district AS d
			INNER JOIN	county AS c on c.id = d.county_id
			INNER JOIN	region AS r on r.id = c.region_id
			WHERE 			LOWER(d.name) = _district_name
			AND					r.id = (
														SELECT			region_id
														FROM 				user_region
														WHERE 			user_id = _user_id
														ORDER BY		created
														DESC LIMIT 	1
													)
		));

		UPDATE			user_notification
		SET					acknowledged = current_timestamp
		WHERE				user_id = _user_id
		AND					notification_id IN (3, 4);

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;

DROP FUNCTION sp_gymfit_site_user_associate_with_district(INT, INT);
DROP FUNCTION sp_gymfit_site_location_get_districts(INT);
DROP FUNCTION sp_gymfit_site_location_get_counties(INT);
