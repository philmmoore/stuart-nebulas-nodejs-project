ALTER TABLE challenge ADD COLUMN hidden BOOL;
ALTER TABLE challenge ADD COLUMN coming_soon BOOL;

UPDATE challenge SET hidden=FALSE, coming_soon=false;

ALTER TABLE challenge ALTER COLUMN hidden SET NOT NULL;
ALTER TABLE challenge ALTER COLUMN coming_soon SET NOT NULL;

CREATE OR REPLACE FUNCTION sp_gymfit_site_get_challenges()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))	FROM (
		SELECT		      c.name,
							      cc.value AS "category",
                    ct.value AS "type",
										c.coming_soon AS "comingSoon"
		FROM			      challenge AS c
    LEFT OUTER JOIN challenge_category AS cc ON c.category = cc.id
    LEFT OUTER JOIN challenge_type AS ct ON c.type = ct.id
		WHERE						c.hidden IS FALSE
		ORDER BY 	      c.name
	) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_challenge_get_challenge
(
	_challenge_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
		(
			SELECT	id,
							name,
							category AS "categoryId",
							type AS "typeId",
							body_copy AS "bodyCopy",
							submission_message AS "submissionMessage",
							(
								array
								(
									SELECT			cg.value
									FROM				challenge_group AS cg
									INNER JOIN	challenge_to_challenge_group AS ccg
									ON 					ccg.challenge_id = c.id
									AND					ccg.challenge_group_id = cg.id
									ORDER BY		cg.id
								)
							) AS groups,
							(
								SELECT COUNT(*) FROM result WHERE challenge_id = c.id
							) AS "totalResults",
							hidden,
							coming_soon AS "comingSoon"
      FROM 		challenge AS c
      WHERE 	c.id = _challenge_id
			LIMIT		1
		) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_challenge_save
(
	_challengeId INT,
	_name TEXT,
	_category INT,
	_type INT,
	_group_ids INT[],
	_body_copy TEXT,
	_submission_message TEXT,
	_hidden BOOL,
	_coming_soon BOOL
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	challenge
		SET			name = _name,
						category = _category,
						type = _type,
						body_copy = _body_copy,
						submission_message = _submission_message,
						hidden = _hidden,
						coming_soon = _coming_soon
		WHERE		id = _challengeId;

		IF FOUND THEN
			DELETE FROM	challenge_to_challenge_group
			WHERE				challenge_id = _challengeId;
		ELSE
			INSERT INTO challenge (name, category, type, body_copy, submission_message, hidden, coming_soon)
			VALUES			(_name, _category, _type, _body_copy, _submission_message, _hidden, _coming_soon)
			RETURNING		id
			INTO				_challengeId;
		END IF;

		INSERT INTO		challenge_to_challenge_group (challenge_id, challenge_group_id)
		SELECT				_challengeId,
									_group_id
		FROM					unnest(_group_ids) AS _group_id;

		RETURN to_json(_challengeId);
	END;
$$
LANGUAGE plpgsql;