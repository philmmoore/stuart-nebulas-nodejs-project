CREATE TABLE challenge_category (
  id SERIAL PRIMARY KEY,
  value TEXT NOT NULL
);

INSERT INTO challenge_category (value)
VALUES
  ('Strength'),
  ('Cardio'),
  ('Ultimate');

CREATE TABLE challenge_type (
  id SERIAL PRIMARY KEY,
  value TEXT NOT NULL
);

INSERT INTO challenge_type (value)
VALUES
  ('Time taken'),
  ('How many'),
  ('How many within a set time'),
  ('Distance covered'),
  ('Distance covered within a set time');

CREATE TABLE challenge_group (
  id SERIAL PRIMARY KEY,
  value TEXT NOT NULL
);

CREATE TABLE challenge (
  id SERIAL PRIMARY KEY,
  country INTEGER REFERENCES country (id) NOT NULL,
  name TEXT NOT NULL,
  category INTEGER REFERENCES challenge_category (id) NOT NULL,
  type INTEGER REFERENCES challenge_type (id) NOT NULL,
  bodyCopy TEXT NOT NULL,
  submissionMessage TEXT NOT NULL
);

CREATE TABLE challenge_to_challenge_group (
  challenge INTEGER REFERENCES challenge (id) NOT NULL,
  challengeGroup INTEGER REFERENCES challenge_group (id) NOT NULL,
  PRIMARY KEY (challenge, challengeGroup)
);
