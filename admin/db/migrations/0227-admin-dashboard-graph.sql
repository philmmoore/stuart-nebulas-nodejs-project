CREATE OR REPLACE FUNCTION sp_gymfit_admin_dashboard_chart()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
		(
			SELECT 	date_trunc('day', date_row.generate_series) AS "date",
							(
								SELECT count(*) FROM "user" WHERE date_trunc('day', created) = date_trunc('day', date_row.generate_series)
							) AS "registrations",
							(
								SELECT count(*) FROM result WHERE date_trunc('day', submitted) = date_trunc('day', date_row.generate_series)
							) AS "resultSubmissions",
							(
								SELECT count(*) FROM result WHERE date_trunc('day', image_validated) = date_trunc('day', date_row.generate_series)
							) AS "imagesUploaded",
							(
								SELECT count(*) FROM result WHERE date_trunc('day', video_validated) = date_trunc('day', date_row.generate_series)
							) AS "videosUploaded",
							(
								SELECT count(DISTINCT user_id) FROM user_audit WHERE date_trunc('day', timestamp) = date_trunc('day', date_row.generate_series)
							) AS "logins",
							(
								SELECT count(*) FROM result_feedback WHERE date_trunc('day', created) = date_trunc('day', date_row.generate_series) AND FLAG = false
							) AS "likes"
				FROM
				(
				    SELECT generate_series
				    (
				        current_timestamp - interval '30 days',
				        current_timestamp,
				        '1 day'
				     )
				) AS date_row
		) AS result_to_return;
$$
LANGUAGE sql;