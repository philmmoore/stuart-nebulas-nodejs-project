CREATE OR REPLACE FUNCTION sp_gymfit_site_register_persist_step_6_details (
  _user_id INT,
  _occupation TEXT,
  _relationship_status_id INT,
  _nationality TEXT,
  _routine_type_id INT,
  _gym_id INT
)
RETURNS JSON AS
$$
BEGIN
	UPDATE   "user"
  SET     occupation = _occupation,
          nationality = _nationality
  WHERE   id = _user_id;

  IF _relationship_status_id != NULL THEN
    INSERT INTO user_relationship_status (user_id, relationship_status_id)
    VALUES (_user_id, _relationship_status_id);
  END IF;

  IF _routine_type_id != NULL THEN
    INSERT INTO user_routine_type (user_id, routine_type_id)
    VALUES (_user_id, _routine_type_id);
  END IF;

  IF _gym_id != NULL THEN
    INSERT INTO user_gym (user_id, gym_id)
    VALUES (_user_id, _gym_id);
  END IF;

  RETURN sp_gymfit_site_user_get(_user_id);
END;
$$
LANGUAGE plpgsql;
