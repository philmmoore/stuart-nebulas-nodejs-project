CREATE OR REPLACE FUNCTION sp_gymfit_site_leaderboard_search
(
	_challenge_id INT,
	_challenge_group_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
	(
		SELECT			u.id,
								u.first_name "firstName",
								u.last_name "lastName",
								r.result
		FROM				result AS r
		INNER JOIN	"user" AS u ON r.user_id = u.id
		WHERE				r.challenge_id = _challenge_id
		AND					r.challenge_group_id = _challenge_group_id
	) AS result_to_return;
$$
LANGUAGE sql;
