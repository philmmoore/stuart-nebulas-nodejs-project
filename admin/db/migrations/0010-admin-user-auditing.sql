ALTER TABLE admin_user DROP last_login;

CREATE TABLE admin_audit (
  timestamp TIMESTAMP PRIMARY KEY default current_timestamp,
  activity TEXT NOT NULL,
  message TEXT
);
