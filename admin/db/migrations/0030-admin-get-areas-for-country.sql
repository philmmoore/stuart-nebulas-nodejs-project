CREATE FUNCTION sp_admin_get_all_areas_for_country
(
	_countryId INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM
		(
			SELECT		id,
								name,
								parent
			FROM			area
			WHERE			country = _countryId
			ORDER BY	name
		) AS result;
$$
LANGUAGE sql;
