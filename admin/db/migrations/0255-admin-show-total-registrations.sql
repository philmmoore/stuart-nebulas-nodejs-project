CREATE OR REPLACE FUNCTION sp_gymfit_admin_get_charity_donation_counts()
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		SELECT
		(
			SELECT	COUNT(*)
			FROM 		"user"
			WHERE 	created BETWEEN '31-JAN-2016' AND '21-MAR-2016'
		) AS "registrations",
		(
			SELECT	COUNT(*)
			FROM 		"user"
		) AS "totalRegistrations",
		(
			SELECT	COUNT(*)
			FROM 		result
			WHERE 	submitted BETWEEN '31-JAN-2016' AND '21-MAR-2016'
			AND			excluded IS NULL
		) AS "results",
    (
			SELECT	COUNT(*)
			FROM 		result
			WHERE		excluded IS NULL
		) AS "totalResults"
	) AS result_to_return
$$
LANGUAGE sql;