--turn on the magic
CREATE EXTENSION IF NOT EXISTS cube;
CREATE EXTENSION IF NOT EXISTS earthdistance;

-- Column: District Lat/Long columns
DO $$ 
    BEGIN
        BEGIN
            ALTER TABLE district ADD COLUMN latitude DECIMAL(9,6);
            ALTER TABLE district ADD COLUMN longitude DECIMAL(9,6);
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column <latitude> already exists in <district>.';
            WHEN duplicate_column THEN RAISE NOTICE 'column <longitude> already exists in <district>.';
        END;
    END;
$$;

-- Column: Region Lat/Long columns
DO $$ 
    BEGIN
        BEGIN
            ALTER TABLE region ADD COLUMN latitude DECIMAL(9,6);
            ALTER TABLE region ADD COLUMN longitude DECIMAL(9,6);
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column <latitude> already exists in <region>.';
            WHEN duplicate_column THEN RAISE NOTICE 'column <longitude> already exists in <region>.';
        END;
    END;
$$;

-- Update district & region lat/lng values
DO $$ 
    BEGIN

        UPDATE region SET latitude = 52.355518, longitude = -1.174320 WHERE name = 'East of England';
        UPDATE region SET latitude = 52.475074, longitude = -1.829833 WHERE name = 'West Midlands';
        UPDATE region SET latitude = 53.719370, longitude = -0.451480 WHERE name = 'Yorkshire and the Humber';
        UPDATE region SET latitude = 52.821632, longitude = -1.147234 WHERE name = 'East Midlands';
        UPDATE region SET latitude = 52.355518, longitude = -1.174320 WHERE name = 'North East England';
        UPDATE region SET latitude = 52.355518, longitude = -1.174320 WHERE name = 'South East England';
        UPDATE region SET latitude = 52.355518, longitude = -1.174320 WHERE name = 'South West England';
        UPDATE region SET latitude = 52.086578, longitude = -4.656266 WHERE name = 'North Wales';
        UPDATE region SET latitude = 52.624772, longitude = -3.695021 WHERE name = 'Mid Wales';
        UPDATE region SET latitude = 52.130661, longitude = -3.783712 WHERE name = 'South Wales';
        UPDATE region SET latitude = 51.868197, longitude = -8.335690 WHERE name = 'Highlands and Islands';
        UPDATE region SET latitude = 56.490671, longitude = -4.202646 WHERE name = 'North Eastern Scotland';
        UPDATE region SET latitude = 56.490671, longitude = -4.202646 WHERE name = 'East Scotland';
        UPDATE region SET latitude = 56.490671, longitude = -4.202646 WHERE name = 'South West Scotland';
        UPDATE region SET latitude = 54.787715, longitude = -6.492315 WHERE name = 'Northern Ireland';
        UPDATE region SET latitude = 54.597285, longitude = -5.930120 WHERE name = 'Belfast';
        UPDATE region SET latitude = 54.787715, longitude = -6.492315 WHERE name = 'South East Northern Ireland';
        UPDATE region SET latitude = 54.787715, longitude = -6.492315 WHERE name = 'South Northern Ireland';
        UPDATE region SET latitude = 54.787715, longitude = -6.492315 WHERE name = 'West Northern Ireland';
        UPDATE region SET latitude = 51.507351, longitude = -0.127758 WHERE name = 'London';
        UPDATE region SET latitude = 52.355518, longitude = -1.174320 WHERE name = 'North West England';

        UPDATE district SET latitude = 53.576865, longitude = -2.428219 WHERE name = 'Bolton';
        UPDATE district SET latitude = 53.593350, longitude = -2.296605 WHERE name = 'Bury';
        UPDATE district SET latitude = 53.480759, longitude = -2.242631 WHERE name = 'Manchester';
        UPDATE district SET latitude = 53.540930, longitude = -2.111366 WHERE name = 'Oldham';
        UPDATE district SET latitude = 53.609714, longitude = -2.156100 WHERE name = 'Rochdale';
        UPDATE district SET latitude = 53.487523, longitude = -2.290126 WHERE name = 'Salford';
        UPDATE district SET latitude = 53.410632, longitude = -2.157533 WHERE name = 'Stockport';
        UPDATE district SET latitude = 53.480583, longitude = -2.080989 WHERE name = 'Tameside';
        UPDATE district SET latitude = 53.421513, longitude = -2.351726 WHERE name = 'Trafford';
        UPDATE district SET latitude = 53.545065, longitude = -2.632507 WHERE name = 'Wigan';
        UPDATE district SET latitude = 54.729410, longitude = -1.881160 WHERE name = 'County Durham';
        UPDATE district SET latitude = 54.523610, longitude = -1.559458 WHERE name = 'Darlington';
        UPDATE district SET latitude = 54.691745, longitude = -1.212926 WHERE name = 'Hartlepool';
        UPDATE district SET latitude = 54.580005, longitude = -1.354258 WHERE name = 'Stockton-on-Tees North';
        UPDATE district SET latitude = 55.208254, longitude = -2.078414 WHERE name = 'Northumberland';
        UPDATE district SET latitude = 55.008000, longitude = -1.546000 WHERE name = 'North Tyneside';
        UPDATE district SET latitude = 54.952680, longitude = -1.603411 WHERE name = 'Gateshead';
        UPDATE district SET latitude = 54.978252, longitude = -1.617780 WHERE name = 'Newcastle Upon Tyne';
        UPDATE district SET latitude = 54.963669, longitude = -1.441863 WHERE name = 'South Tyneside';
        UPDATE district SET latitude = 54.906869, longitude = -1.383801 WHERE name = 'Sunderland';
        UPDATE district SET latitude = 54.597134, longitude = -1.077600 WHERE name = 'Redcar and Cleveland';
        UPDATE district SET latitude = 54.570455, longitude = -1.328982 WHERE name = 'Stockton-on-Tees South';
        UPDATE district SET latitude = 54.574227, longitude = -1.234956 WHERE name = 'Middlesbrough';
        UPDATE district SET latitude = 53.161045, longitude = -2.218593 WHERE name = 'Cheshire East';
        UPDATE district SET latitude = 53.230297, longitude = -2.715112 WHERE name = 'Cheshire West and Chester';
        UPDATE district SET latitude = 53.361270, longitude = -2.733479 WHERE name = 'Halton';
        UPDATE district SET latitude = 53.390044, longitude = -2.596950 WHERE name = 'Warrington';
        UPDATE district SET latitude = 54.647168, longitude = -3.551258 WHERE name = 'Allerdale';
        UPDATE district SET latitude = 54.108967, longitude = -3.218894 WHERE name = 'Barrow-in-Furness';
        UPDATE district SET latitude = 54.892473, longitude = -2.932931 WHERE name = 'Carlisle';
        UPDATE district SET latitude = 52.576631, longitude = -0.289522 WHERE name = 'Copeland';
        UPDATE district SET latitude = 54.728975, longitude = -5.758101 WHERE name = 'Eden';
        UPDATE district SET latitude = 52.451881, longitude = -1.166869 WHERE name = 'South Lakeland';
        UPDATE district SET latitude = 53.789288, longitude = -2.240503 WHERE name = 'Burnley';
        UPDATE district SET latitude = 53.653511, longitude = -2.632596 WHERE name = 'Chorley';
        UPDATE district SET latitude = 52.414694, longitude = -1.459625 WHERE name = 'Fylde';
        UPDATE district SET latitude = 53.768000, longitude = -2.382000 WHERE name = 'Hyndburn';
        UPDATE district SET latitude = 54.046575, longitude = -2.800740 WHERE name = 'Lancaster';
        UPDATE district SET latitude = 53.363000, longitude = -6.186197 WHERE name = 'Pendle';
        UPDATE district SET latitude = 53.763201, longitude = -2.703090 WHERE name = 'Preston';
        UPDATE district SET latitude = 53.873000, longitude = -2.391000 WHERE name = 'Ribble Valley';
        UPDATE district SET latitude = 53.706451, longitude = -2.279366 WHERE name = 'Rossendale';
        UPDATE district SET latitude = 53.717000, longitude = -2.697000 WHERE name = 'South Ribble';
        UPDATE district SET latitude = 53.763225, longitude = -2.704405 WHERE name = 'West Lancashire';
        UPDATE district SET latitude = 59.124292, longitude = -2.971426 WHERE name = 'Wyre';
        UPDATE district SET latitude = 53.454594, longitude = -2.852907 WHERE name = 'Knowsley';
        UPDATE district SET latitude = 53.408371, longitude = -2.991573 WHERE name = 'Liverpool';
        UPDATE district SET latitude = 53.503445, longitude = -2.970359 WHERE name = 'Sefton';
        UPDATE district SET latitude = 53.456307, longitude = -2.737095 WHERE name = 'St Helens';
        UPDATE district SET latitude = 53.333333, longitude = -3.083333 WHERE name = 'Wirral';
        UPDATE district SET latitude = 53.841617, longitude = -0.434411 WHERE name = 'East Riding of Yorkshire';
        UPDATE district SET latitude = 53.745671, longitude = -0.336741 WHERE name = 'Kingston upon Hull';
        UPDATE district SET latitude = 53.566820, longitude = -0.081507 WHERE name = 'North East Lincolnshire';
        UPDATE district SET latitude = 53.605559, longitude = -0.559658 WHERE name = 'North Lincolnshire';
        UPDATE district SET latitude = 53.959965, longitude = -1.087298 WHERE name = 'York';
        UPDATE district SET latitude = 52.619305, longitude = -1.654242 WHERE name = 'Craven';
        UPDATE district SET latitude = 53.875195, longitude = -2.954922 WHERE name = 'Hambleton';
        UPDATE district SET latitude = 53.992120, longitude = -1.541812 WHERE name = 'Harrogate';
        UPDATE district SET latitude = 54.329981, longitude = -2.011858 WHERE name = 'Richmondshire';
        UPDATE district SET latitude = 52.626265, longitude = -8.632758 WHERE name = 'Ryedale';
        UPDATE district SET latitude = 54.283113, longitude = -0.399752 WHERE name = 'Scarborough';
        UPDATE district SET latitude = 53.783524, longitude = -1.067189 WHERE name = 'Selby';
        UPDATE district SET latitude = 53.552630, longitude = -1.479726 WHERE name = 'Barnsley';
        UPDATE district SET latitude = 53.522820, longitude = -1.128462 WHERE name = 'Doncaster';
        UPDATE district SET latitude = 53.432603, longitude = -1.363501 WHERE name = 'Rotherham';
        UPDATE district SET latitude = 53.381129, longitude = -1.470085 WHERE name = 'Sheffield';
        UPDATE district SET latitude = 53.795984, longitude = -1.759398 WHERE name = 'Bradford';
        UPDATE district SET latitude = 53.716157, longitude = -1.858460 WHERE name = 'Calderdale';
        UPDATE district SET latitude = 52.602054, longitude = 1.280326 WHERE name = 'Kirklees';
        UPDATE district SET latitude = 53.800755, longitude = -1.549077 WHERE name = 'Leeds';
        UPDATE district SET latitude = 53.683298, longitude = -1.505924 WHERE name = 'Wakefield';
        UPDATE district SET latitude = 52.922530, longitude = -1.474619 WHERE name = 'Derby';
        UPDATE district SET latitude = 52.636878, longitude = -1.139759 WHERE name = 'Leicester';
        UPDATE district SET latitude = 52.954783, longitude = -1.158109 WHERE name = 'Nottingham';
        UPDATE district SET latitude = 52.658301, longitude = -0.639643 WHERE name = 'Rutland';
        UPDATE district SET latitude = 53.015187, longitude = -1.482307 WHERE name = 'Amber Valley';
        UPDATE district SET latitude = 53.231044, longitude = -1.289721 WHERE name = 'Bolsover';
        UPDATE district SET latitude = 53.235048, longitude = -1.421629 WHERE name = 'Chesterfield';
        UPDATE district SET latitude = 53.150000, longitude = -1.650000 WHERE name = 'Derbyshire Dales';
        UPDATE district SET latitude = 52.900000, longitude = -1.320000 WHERE name = 'Erewash';
        UPDATE district SET latitude = 53.324669, longitude = -2.003017 WHERE name = 'High Peak';
        UPDATE district SET latitude = 53.104678, longitude = -1.562388 WHERE name = 'North East Derbyshire';
        UPDATE district SET latitude = 53.104678, longitude = -1.562388 WHERE name = 'South Derbyshire';
        UPDATE district SET latitude = 52.570776, longitude = -1.165650 WHERE name = 'Blaby';
        UPDATE district SET latitude = 52.740123, longitude = -1.140593 WHERE name = 'Charnwood';
        UPDATE district SET latitude = 52.500000, longitude = -1.000000 WHERE name = 'Harborough';
        UPDATE district SET latitude = 52.630259, longitude = -1.411521 WHERE name = 'Hinckley and Bosworth';
        UPDATE district SET latitude = 52.104856, longitude = 1.330878 WHERE name = 'Melton';
        UPDATE district SET latitude = 52.772571, longitude = -1.205213 WHERE name = 'North West Leicestershire';
        UPDATE district SET latitude = 52.586318, longitude = -1.106354 WHERE name = 'Oadby and Wigston';
        UPDATE district SET latitude = 52.978940, longitude = -0.026577 WHERE name = 'Boston';
        UPDATE district SET latitude = 52.067716, longitude = 0.883572 WHERE name = 'East Lindsey';
        UPDATE district SET latitude = 53.230688, longitude = -0.540579 WHERE name = 'Lincoln';
        UPDATE district SET latitude = 53.088050, longitude = -0.448908 WHERE name = 'North Kesteven';
        UPDATE district SET latitude = 51.806132, longitude = 1.189651 WHERE name = 'South Holland';
        UPDATE district SET latitude = 52.830022, longitude = -0.544082 WHERE name = 'South Kesteven';
        UPDATE district SET latitude = 52.067716, longitude = 0.883572 WHERE name = 'West Lindsey';
        UPDATE district SET latitude = 52.492298, longitude = -0.684233 WHERE name = 'Corby';
        UPDATE district SET latitude = 52.257473, longitude = -1.164947 WHERE name = 'Daventry';
        UPDATE district SET latitude = 52.272994, longitude = -0.875552 WHERE name = 'East Northamptonshire';
        UPDATE district SET latitude = 52.396322, longitude = -0.730249 WHERE name = 'Kettering';
        UPDATE district SET latitude = 52.240477, longitude = -0.902656 WHERE name = 'Northampton';
        UPDATE district SET latitude = 52.272994, longitude = -0.875552 WHERE name = 'South Northamptonshire';
        UPDATE district SET latitude = 52.302419, longitude = -0.693964 WHERE name = 'Wellingborough';
        UPDATE district SET latitude = 52.218676, longitude = 1.234777 WHERE name = 'Ashfield';
        UPDATE district SET latitude = 53.400000, longitude = -0.950000 WHERE name = 'Bassetlaw';
        UPDATE district SET latitude = 52.974507, longitude = -1.217772 WHERE name = 'Broxtowe';
        UPDATE district SET latitude = 52.973455, longitude = -1.080056 WHERE name = 'Gedling';
        UPDATE district SET latitude = 53.147195, longitude = -1.198674 WHERE name = 'Mansfield';
        UPDATE district SET latitude = 53.070039, longitude = -0.806570 WHERE name = 'Newark and Sherwood';
        UPDATE district SET latitude = 54.926324, longitude = -1.382171 WHERE name = 'Rushcliffe';
        UPDATE district SET latitude = 52.076516, longitude = -2.654418 WHERE name = 'Herefordshire';
        UPDATE district SET latitude = 52.706366, longitude = -2.741785 WHERE name = 'Shropshire';
        UPDATE district SET latitude = 53.002668, longitude = -2.179404 WHERE name = 'Stoke-on-Trent';
        UPDATE district SET latitude = 52.740992, longitude = -2.486859 WHERE name = 'Telford and Wrekin';
        UPDATE district SET latitude = 52.718303, longitude = -1.985881 WHERE name = 'Cannock Chase';
        UPDATE district SET latitude = 52.879275, longitude = -2.057187 WHERE name = 'East Staffordshire';
        UPDATE district SET latitude = 52.681602, longitude = -1.831672 WHERE name = 'Lichfield';
        UPDATE district SET latitude = 53.013208, longitude = -2.227300 WHERE name = 'Newcastle-Under-Lyme';
        UPDATE district SET latitude = 52.879275, longitude = -2.057187 WHERE name = 'South Staffordshire';
        UPDATE district SET latitude = 52.806693, longitude = -2.120660 WHERE name = 'Stafford';
        UPDATE district SET latitude = 53.066667, longitude = -1.983333 WHERE name = 'Staffordshire Moorlands';
        UPDATE district SET latitude = 52.633584, longitude = -1.691032 WHERE name = 'Tamworth';
        UPDATE district SET latitude = 52.267135, longitude = -1.467522 WHERE name = 'North Warwickshire';
        UPDATE district SET latitude = 52.520489, longitude = -1.465382 WHERE name = 'Nuneaton and Bedworth';
        UPDATE district SET latitude = 52.370878, longitude = -1.265032 WHERE name = 'Rugby';
        UPDATE district SET latitude = 52.137819, longitude = -1.609123 WHERE name = 'Stratford-on-Avon';
        UPDATE district SET latitude = 52.282316, longitude = -1.584927 WHERE name = 'Warwick';
        UPDATE district SET latitude = 52.486243, longitude = -1.890401 WHERE name = 'Birmingham';
        UPDATE district SET latitude = 52.335589, longitude = -2.061906 WHERE name = 'Bromsgrove';
        UPDATE district SET latitude = 52.103532, longitude = -2.337761 WHERE name = 'Malvern Hills';
        UPDATE district SET latitude = 52.308970, longitude = -1.940936 WHERE name = 'Redditch';
        UPDATE district SET latitude = 52.193636, longitude = -2.221575 WHERE name = 'Worcester';
        UPDATE district SET latitude = 52.152518, longitude = -2.033095 WHERE name = 'Wychavon';
        UPDATE district SET latitude = 52.403805, longitude = -2.253763 WHERE name = 'Wyre Forest';
        UPDATE district SET latitude = 52.406822, longitude = -1.519693 WHERE name = 'Coventry';
        UPDATE district SET latitude = 52.512255, longitude = -2.081112 WHERE name = 'Dudley';
        UPDATE district SET latitude = 52.536167, longitude = -2.010793 WHERE name = 'Sandwell';
        UPDATE district SET latitude = 52.411811, longitude = -1.777610 WHERE name = 'Solihull';
        UPDATE district SET latitude = 52.586214, longitude = -1.982919 WHERE name = 'Walsall';
        UPDATE district SET latitude = 52.586973, longitude = -2.128820 WHERE name = 'Wolverhampton';
        UPDATE district SET latitude = 52.135973, longitude = -0.466655 WHERE name = 'Bedford';
        UPDATE district SET latitude = 52.002974, longitude = -0.465139 WHERE name = 'Central Bedfordshire';
        UPDATE district SET latitude = 51.878671, longitude = -0.420026 WHERE name = 'Luton';
        UPDATE district SET latitude = 52.569498, longitude = -0.240530 WHERE name = 'Peterborough';
        UPDATE district SET latitude = 51.545927, longitude = 0.707712 WHERE name = 'Southend-on-Sea';
        UPDATE district SET latitude = 51.493456, longitude = 0.352920 WHERE name = 'Thurrock';
        UPDATE district SET latitude = 52.205337, longitude = 0.121817 WHERE name = 'Cambridge';
        UPDATE district SET latitude = 52.205297, longitude = 0.121820 WHERE name = 'East Cambridgeshire';
        UPDATE district SET latitude = 52.574546, longitude = 0.048768 WHERE name = 'Fenland';
        UPDATE district SET latitude = 54.783113, longitude = -1.524133 WHERE name = 'Huntingdonshire';
        UPDATE district SET latitude = 52.205297, longitude = 0.121820 WHERE name = 'South Cambridgeshire';
        UPDATE district SET latitude = 51.576084, longitude = 0.488736 WHERE name = 'Basildon';
        UPDATE district SET latitude = 51.880087, longitude = 0.550927 WHERE name = 'Braintree';
        UPDATE district SET latitude = 51.620475, longitude = 0.307175 WHERE name = 'Brentwood';
        UPDATE district SET latitude = 50.231684, longitude = -3.780082 WHERE name = 'Castle Point';
        UPDATE district SET latitude = 51.735587, longitude = 0.468550 WHERE name = 'Chelmsford';
        UPDATE district SET latitude = 51.895927, longitude = 0.891874 WHERE name = 'Colchester';
        UPDATE district SET latitude = 51.657077, longitude = 0.041282 WHERE name = 'Epping Forest';
        UPDATE district SET latitude = 51.767787, longitude = 0.087806 WHERE name = 'Harlow';
        UPDATE district SET latitude = 51.731805, longitude = 0.671448 WHERE name = 'Maldon';
        UPDATE district SET latitude = 51.582071, longitude = 0.706515 WHERE name = 'Rochford';
        UPDATE district SET latitude = 51.874808, longitude = 1.114504 WHERE name = 'Tendring';
        UPDATE district SET latitude = 51.978943, longitude = 0.239124 WHERE name = 'Uttlesford';
        UPDATE district SET latitude = 51.743473, longitude = -0.021151 WHERE name = 'Broxbourne';
        UPDATE district SET latitude = 51.754497, longitude = -0.473550 WHERE name = 'Dacorum';
        UPDATE district SET latitude = 51.809782, longitude = -0.237674 WHERE name = 'East Hertfordshire';
        UPDATE district SET latitude = 51.666667, longitude = -0.266667 WHERE name = 'Hertsmere';
        UPDATE district SET latitude = 51.809782, longitude = -0.237674 WHERE name = 'North Hertfordshire';
        UPDATE district SET latitude = 51.752725, longitude = -0.339436 WHERE name = 'St Albans';
        UPDATE district SET latitude = 51.903761, longitude = -0.196612 WHERE name = 'Stevenage';
        UPDATE district SET latitude = 51.650910, longitude = -0.461900 WHERE name = 'Three Rivers';
        UPDATE district SET latitude = 51.656489, longitude = -0.390320 WHERE name = 'Watford';
        UPDATE district SET latitude = 51.746502, longitude = -0.213369 WHERE name = 'Welwyn Hatfield';
        UPDATE district SET latitude = 52.571616, longitude = 0.835468 WHERE name = 'Breckland';
        UPDATE district SET latitude = 52.739118, longitude = 1.240942 WHERE name = 'Broadland';
        UPDATE district SET latitude = 52.598233, longitude = 1.728047 WHERE name = 'Great Yarmouth';
        UPDATE district SET latitude = 52.751680, longitude = 0.402296 WHERE name = 'Kings Lynn and West Norfolk';
        UPDATE district SET latitude = 52.613969, longitude = 0.886402 WHERE name = 'North Norfolk';
        UPDATE district SET latitude = 52.630886, longitude = 1.297355 WHERE name = 'Norwich';
        UPDATE district SET latitude = 52.613969, longitude = 0.886402 WHERE name = 'South Norfolk';
        UPDATE district SET latitude = 52.037904, longitude = 0.733527 WHERE name = 'Babergh';
        UPDATE district SET latitude = 53.694017, longitude = -1.478014 WHERE name = 'Forest Heath';
        UPDATE district SET latitude = 52.056736, longitude = 1.148220 WHERE name = 'Ipswich';
        UPDATE district SET latitude = 52.168428, longitude = 0.986740 WHERE name = 'Mid Suffolk';
        UPDATE district SET latitude = 52.770637, longitude = 0.866410 WHERE name = 'St Edmundsbury';
        UPDATE district SET latitude = 52.150000, longitude = 1.500000 WHERE name = 'Suffolk Coastal';
        UPDATE district SET latitude = 51.774249, longitude = -0.451736 WHERE name = 'Waveney';
        UPDATE district SET latitude = 51.551706, longitude = -0.158826 WHERE name = 'Camden';
        UPDATE district SET latitude = 51.512344, longitude = -0.090985 WHERE name = 'City of London';
        UPDATE district SET latitude = 53.156314, longitude = -1.575022 WHERE name = 'Hackney';
        UPDATE district SET latitude = 51.499017, longitude = -0.229150 WHERE name = 'Hammersmith and Fulham';
        UPDATE district SET latitude = 51.590611, longitude = -0.110971 WHERE name = 'Haringey';
        UPDATE district SET latitude = 51.546506, longitude = -0.105806 WHERE name = 'Islington';
        UPDATE district SET latitude = 51.499080, longitude = -0.193825 WHERE name = 'Kensington and Chelsea';
        UPDATE district SET latitude = 51.457148, longitude = -0.123068 WHERE name = 'Lambeth';
        UPDATE district SET latitude = 51.441458, longitude = -0.011701 WHERE name = 'Lewisham';
        UPDATE district SET latitude = 51.525516, longitude = 0.035216 WHERE name = 'Newham';
        UPDATE district SET latitude = 51.502781, longitude = -0.087738 WHERE name = 'Southwark';
        UPDATE district SET latitude = 51.520261, longitude = -0.029340 WHERE name = 'Tower Hamlets';
        UPDATE district SET latitude = 51.457072, longitude = -0.181782 WHERE name = 'Wandsworth';
        UPDATE district SET latitude = 51.500175, longitude = -0.133233 WHERE name = 'Westminster';
        UPDATE district SET latitude = 51.546483, longitude = 0.129350 WHERE name = 'Barking and Dagenham';
        UPDATE district SET latitude = 51.656923, longitude = -0.194925 WHERE name = 'Barnet';
        UPDATE district SET latitude = 51.439933, longitude = 0.154327 WHERE name = 'Bexley';
        UPDATE district SET latitude = 51.567281, longitude = -0.271057 WHERE name = 'Brent';
        UPDATE district SET latitude = 51.406025, longitude = 0.013156 WHERE name = 'Bromley';
        UPDATE district SET latitude = 51.376165, longitude = -0.098234 WHERE name = 'Croydon';
        UPDATE district SET latitude = 51.513295, longitude = -0.304331 WHERE name = 'Ealing';
        UPDATE district SET latitude = 51.652299, longitude = -0.080712 WHERE name = 'Enfield';
        UPDATE district SET latitude = 51.482577, longitude = -0.007659 WHERE name = 'Greenwich';
        UPDATE district SET latitude = 51.580559, longitude = -0.341995 WHERE name = 'Harrow';
        UPDATE district SET latitude = 51.577924, longitude = 0.212083 WHERE name = 'Havering';
        UPDATE district SET latitude = 51.535183, longitude = -0.448138 WHERE name = 'Hillingdon';
        UPDATE district SET latitude = 51.460922, longitude = -0.373149 WHERE name = 'Hounslow';
        UPDATE district SET latitude = 51.412330, longitude = -0.300689 WHERE name = 'Kingston upon Thames';
        UPDATE district SET latitude = 50.891854, longitude = -4.095316 WHERE name = 'Merton';
        UPDATE district SET latitude = 51.588610, longitude = 0.082398 WHERE name = 'Redbridge';
        UPDATE district SET latitude = 51.461311, longitude = -0.303742 WHERE name = 'Richmond upon Thames';
        UPDATE district SET latitude = 51.361428, longitude = -0.193961 WHERE name = 'Sutton';
        UPDATE district SET latitude = 51.588638, longitude = -0.011763 WHERE name = 'Waltham Forest';
        UPDATE district SET latitude = 52.040622, longitude = -0.759417 WHERE name = 'Milton Keynes';
        UPDATE district SET latitude = 50.819767, longitude = -1.087977 WHERE name = 'Portsmouth';
        UPDATE district SET latitude = 51.815606, longitude = -0.808400 WHERE name = 'Aylesbury Vale';
        UPDATE district SET latitude = 51.660607, longitude = -0.640948 WHERE name = 'Chiltern';
        UPDATE district SET latitude = 51.813707, longitude = -0.809470 WHERE name = 'South Bucks';
        UPDATE district SET latitude = 51.638511, longitude = -0.807860 WHERE name = 'Wycombe';
        UPDATE district SET latitude = 50.768035, longitude = 0.290472 WHERE name = 'Eastbourne';
        UPDATE district SET latitude = 50.854259, longitude = 0.573453 WHERE name = 'Hastings';
        UPDATE district SET latitude = 50.873872, longitude = 0.008780 WHERE name = 'Lewes';
        UPDATE district SET latitude = 51.009514, longitude = -0.733152 WHERE name = 'Rother';
        UPDATE district SET latitude = 50.999000, longitude = 0.212000 WHERE name = 'Wealden';
        UPDATE district SET latitude = 51.266540, longitude = -1.092396 WHERE name = 'Basingstoke and Deane';
        UPDATE district SET latitude = 51.057695, longitude = -1.308063 WHERE name = 'East Hampshire';
        UPDATE district SET latitude = 50.967182, longitude = -1.374688 WHERE name = 'Eastleigh';
        UPDATE district SET latitude = 50.854846, longitude = -1.186587 WHERE name = 'Fareham';
        UPDATE district SET latitude = 50.794995, longitude = -1.117547 WHERE name = 'Gosport';
        UPDATE district SET latitude = 51.301690, longitude = -0.892945 WHERE name = 'Hart';
        UPDATE district SET latitude = 51.415383, longitude = -0.753649 WHERE name = 'Bracknell Forest';
        UPDATE district SET latitude = 50.822530, longitude = -0.137163 WHERE name = 'Brighton and Hove';
        UPDATE district SET latitude = 50.692718, longitude = -1.316710 WHERE name = 'Isle of Wight';
        UPDATE district SET latitude = 51.404743, longitude = 0.541786 WHERE name = 'Medway';
        UPDATE district SET latitude = 51.454265, longitude = -0.978130 WHERE name = 'Reading';
        UPDATE district SET latitude = 51.510538, longitude = -0.595041 WHERE name = 'Slough';
        UPDATE district SET latitude = 50.909700, longitude = -1.404351 WHERE name = 'Southampton';
        UPDATE district SET latitude = 51.430825, longitude = -1.144493 WHERE name = 'West Berkshire';
        UPDATE district SET latitude = 51.481539, longitude = -0.614126 WHERE name = 'Windsor and Maidenhead';
        UPDATE district SET latitude = 51.410457, longitude = -0.833861 WHERE name = 'Wokingham';
        UPDATE district SET latitude = 50.851832, longitude = -0.984713 WHERE name = 'Havant';
        UPDATE district SET latitude = 50.875875, longitude = -1.632772 WHERE name = 'New Forest';
        UPDATE district SET latitude = 54.699822, longitude = -1.589553 WHERE name = 'Rushmoor';
        UPDATE district SET latitude = 52.108896, longitude = -0.487427 WHERE name = 'Test Valley';
        UPDATE district SET latitude = 51.059771, longitude = -1.310142 WHERE name = 'Winchester';
        UPDATE district SET latitude = 51.146466, longitude = 0.875019 WHERE name = 'Ashford';
        UPDATE district SET latitude = 51.280233, longitude = 1.078909 WHERE name = 'Canterbury';
        UPDATE district SET latitude = 51.446210, longitude = 0.216872 WHERE name = 'Dartford';
        UPDATE district SET latitude = 51.127876, longitude = 1.313403 WHERE name = 'Dover';
        UPDATE district SET latitude = 51.400000, longitude = 0.366667 WHERE name = 'Gravesham';
        UPDATE district SET latitude = 51.270363, longitude = 0.522699 WHERE name = 'Maidstone';
        UPDATE district SET latitude = 51.272410, longitude = 0.190898 WHERE name = 'Sevenoaks';
        UPDATE district SET latitude = 51.257987, longitude = 0.543083 WHERE name = 'Shepway';
        UPDATE district SET latitude = 54.249100, longitude = -1.775052 WHERE name = 'Swale';
        UPDATE district SET latitude = 51.360252, longitude = 1.348466 WHERE name = 'Thanet';
        UPDATE district SET latitude = 51.195043, longitude = 0.275680 WHERE name = 'Tonbridge and Malling';
        UPDATE district SET latitude = 51.132377, longitude = 0.263695 WHERE name = 'Tunbridge Wells';
        UPDATE district SET latitude = 51.888413, longitude = -1.203428 WHERE name = 'Cherwell';
        UPDATE district SET latitude = 51.752021, longitude = -1.257726 WHERE name = 'Oxford';
        UPDATE district SET latitude = 51.761206, longitude = -1.246467 WHERE name = 'South Oxfordshire';
        UPDATE district SET latitude = 51.616667, longitude = -1.516667 WHERE name = 'Vale of White Horse';
        UPDATE district SET latitude = 51.761206, longitude = -1.246467 WHERE name = 'West Oxfordshire';
        UPDATE district SET latitude = 52.307599, longitude = -2.147420 WHERE name = 'Elmbridge';
        UPDATE district SET latitude = 51.336223, longitude = -0.268823 WHERE name = 'Epsom and Ewell';
        UPDATE district SET latitude = 51.236220, longitude = -0.570409 WHERE name = 'Guildford';
        UPDATE district SET latitude = 51.266000, longitude = -0.328000 WHERE name = 'Mole Valley';
        UPDATE district SET latitude = 51.237276, longitude = -0.205883 WHERE name = 'Reigate and Banstead';
        UPDATE district SET latitude = 51.438838, longitude = -0.554452 WHERE name = 'Runnymede';
        UPDATE district SET latitude = 51.420000, longitude = -0.460000 WHERE name = 'Spelthorne';
        UPDATE district SET latitude = 51.296700, longitude = -0.654387 WHERE name = 'Surrey Heath';
        UPDATE district SET latitude = 51.242484, longitude = -0.034156 WHERE name = 'Tandridge';
        UPDATE district SET latitude = 53.382044, longitude = -1.374048 WHERE name = 'Waverley';
        UPDATE district SET latitude = 51.316774, longitude = -0.560035 WHERE name = 'Woking';
        UPDATE district SET latitude = 50.834817, longitude = -0.310126 WHERE name = 'Adur';
        UPDATE district SET latitude = 50.855171, longitude = -0.555119 WHERE name = 'Arun';
        UPDATE district SET latitude = 50.837610, longitude = -0.774936 WHERE name = 'Chichester';
        UPDATE district SET latitude = 51.109140, longitude = -0.187228 WHERE name = 'Crawley';
        UPDATE district SET latitude = 51.062883, longitude = -0.325858 WHERE name = 'Horsham';
        UPDATE district SET latitude = 51.033000, longitude = -0.115000 WHERE name = 'Mid Sussex';
        UPDATE district SET latitude = 50.817870, longitude = -0.372882 WHERE name = 'Worthing';
        UPDATE district SET latitude = 51.325010, longitude = -2.476628 WHERE name = 'Bath and North East Somerset';
        UPDATE district SET latitude = 50.719164, longitude = -1.880769 WHERE name = 'Bournemouth';
        UPDATE district SET latitude = 51.454513, longitude = -2.587910 WHERE name = 'Bristol';
        UPDATE district SET latitude = 50.266047, longitude = -5.052712 WHERE name = 'Cornwall';
        UPDATE district SET latitude = 49.923252, longitude = -6.296573 WHERE name = 'Isles of Scilly';
        UPDATE district SET latitude = 51.387903, longitude = -2.778109 WHERE name = 'North Somerset';
        UPDATE district SET latitude = 50.375456, longitude = -4.142656 WHERE name = 'Plymouth';
        UPDATE district SET latitude = 50.715050, longitude = -1.987248 WHERE name = 'Poole';
        UPDATE district SET latitude = 51.526436, longitude = -2.472849 WHERE name = 'South Gloucestershire';
        UPDATE district SET latitude = 51.555774, longitude = -1.779718 WHERE name = 'Swindon';
        UPDATE district SET latitude = 50.461921, longitude = -3.525315 WHERE name = 'Torbay';
        UPDATE district SET latitude = 51.349200, longitude = -1.992710 WHERE name = 'Wiltshire';
        UPDATE district SET latitude = 50.760283, longitude = -4.255333 WHERE name = 'East Devon';
        UPDATE district SET latitude = 50.718412, longitude = -3.533899 WHERE name = 'Exeter';
        UPDATE district SET latitude = 50.919032, longitude = -3.474622 WHERE name = 'Mid Devon';
        UPDATE district SET latitude = 51.135067, longitude = -4.214368 WHERE name = 'North Devon';
        UPDATE district SET latitude = 50.410224, longitude = -3.800933 WHERE name = 'South Hams';
        UPDATE district SET latitude = 50.546000, longitude = -3.497000 WHERE name = 'Teignbridge';
        UPDATE district SET latitude = 52.597984, longitude = -1.658619 WHERE name = 'Torridge';
        UPDATE district SET latitude = 51.089637, longitude = -3.848000 WHERE name = 'West Devon';
        UPDATE district SET latitude = 50.735777, longitude = -1.778586 WHERE name = 'Christchurch';
        UPDATE district SET latitude = 50.748764, longitude = -2.344479 WHERE name = 'East Dorset';
        UPDATE district SET latitude = 50.748764, longitude = -2.344479 WHERE name = 'North Dorset';
        UPDATE district SET latitude = 50.702096, longitude = -2.096519 WHERE name = 'Purbeck';
        UPDATE district SET latitude = 50.748764, longitude = -2.344479 WHERE name = 'West Dorset';
        UPDATE district SET latitude = 50.614428, longitude = -2.457621 WHERE name = 'Weymouth and Portland';
        UPDATE district SET latitude = 51.899386, longitude = -2.078253 WHERE name = 'Cheltenham';
        UPDATE district SET latitude = 51.794968, longitude = -1.883894 WHERE name = 'Cotswold';
        UPDATE district SET latitude = 51.806856, longitude = -2.552469 WHERE name = 'Forest of Dean';
        UPDATE district SET latitude = 51.864245, longitude = -2.238156 WHERE name = 'Gloucester';
        UPDATE district SET latitude = 51.745734, longitude = -2.217758 WHERE name = 'Stroud';
        UPDATE district SET latitude = 51.992265, longitude = -2.157960 WHERE name = 'Tewkesbury';
        UPDATE district SET latitude = 51.156288, longitude = 0.866924 WHERE name = 'Mendip';
        UPDATE district SET latitude = 51.185102, longitude = -2.914048 WHERE name = 'Sedgemoor';
        UPDATE district SET latitude = 51.105097, longitude = -2.926231 WHERE name = 'South Somerset';
        UPDATE district SET latitude = 51.015344, longitude = -3.106849 WHERE name = 'Taunton Deane';
        UPDATE district SET latitude = 51.105097, longitude = -2.926231 WHERE name = 'West Somerset';
        UPDATE district SET latitude = 53.166866, longitude = -3.141891 WHERE name = 'Flintshire';
        UPDATE district SET latitude = 53.184229, longitude = -3.422498 WHERE name = 'Denbighshire';
        UPDATE district SET latitude = 53.043040, longitude = -2.992494 WHERE name = 'Wrexham';
        UPDATE district SET latitude = 53.282872, longitude = -3.829480 WHERE name = 'Conwy';
        UPDATE district SET latitude = 53.255694, longitude = -4.310578 WHERE name = 'Ynys Mon';
        UPDATE district SET latitude = 52.646425, longitude = -3.326090 WHERE name = 'Powys';
        UPDATE district SET latitude = 51.857231, longitude = -4.311596 WHERE name = 'Carmarthenshire';
        UPDATE district SET latitude = 52.219143, longitude = -3.932126 WHERE name = 'Cardiganshire';
        UPDATE district SET latitude = 51.674078, longitude = -4.908879 WHERE name = 'Pembrokeshire';
        UPDATE district SET latitude = 52.219143, longitude = -3.932126 WHERE name = 'Ceredigion';
        UPDATE district SET latitude = 51.481581, longitude = -3.179090 WHERE name = 'Cardiff';
        UPDATE district SET latitude = 51.409596, longitude = -3.484817 WHERE name = 'Vale of Glamorgan';
        UPDATE district SET latitude = 51.691743, longitude = -3.734673 WHERE name = 'Neath Port Talbot';
        UPDATE district SET latitude = 51.621440, longitude = -3.943646 WHERE name = 'Swansea';
        UPDATE district SET latitude = 51.649021, longitude = -3.428869 WHERE name = 'Rhondda Cynon Taf';
        UPDATE district SET latitude = 51.748730, longitude = -3.381646 WHERE name = 'Merthyr Tydfil';
        UPDATE district SET latitude = 51.504286, longitude = -3.576945 WHERE name = 'Bridgend';
        UPDATE district SET latitude = 51.787578, longitude = -3.204393 WHERE name = 'Blaenau Gwent';
        UPDATE district SET latitude = 51.578829, longitude = -3.218134 WHERE name = 'Caerphilly';
        UPDATE district SET latitude = 51.811610, longitude = -2.716342 WHERE name = 'Monmouthshire';
        UPDATE district SET latitude = 51.584151, longitude = -2.997664 WHERE name = 'Newport';
        UPDATE district SET latitude = 51.700225, longitude = -3.044601 WHERE name = 'Torfaen';
        UPDATE district SET latitude = 56.400621, longitude = -5.480748 WHERE name = 'Argyll and Bute';
        UPDATE district SET latitude = 55.921313, longitude = -4.319328 WHERE name = 'Bearsden and Milngavie';
        UPDATE district SET latitude = 56.107682, longitude = -3.752998 WHERE name = 'Clackmannan';
        UPDATE district SET latitude = 56.200842, longitude = -3.159860 WHERE name = 'Areas of Glenrothes';
        UPDATE district SET latitude = 56.107535, longitude = -3.752941 WHERE name = 'Clackmannanshire';
        UPDATE district SET latitude = 51.646789, longitude = -0.042087 WHERE name = 'Clydesdale';
        UPDATE district SET latitude = 55.991895, longitude = -3.930273 WHERE name = 'Cumbernauld and Kilsyth';
        UPDATE district SET latitude = 55.453552, longitude = -4.265829 WHERE name = 'Cumnock and Doon Valley';
        UPDATE district SET latitude = 55.613158, longitude = -4.674182 WHERE name = 'Cunninghame';
        UPDATE district SET latitude = 56.071741, longitude = -3.452151 WHERE name = 'Dunfermline';
        UPDATE district SET latitude = 55.764352, longitude = -4.176999 WHERE name = 'East Kilbride';
        UPDATE district SET latitude = 54.101704, longitude = -9.159348 WHERE name = 'Eastwood and Strathclyde';
        UPDATE district SET latitude = 55.953252, longitude = -3.188267 WHERE name = 'Edinburgh';
        UPDATE district SET latitude = 56.001878, longitude = -3.783913 WHERE name = 'Falkirk';
        UPDATE district SET latitude = 55.864237, longitude = -4.251806 WHERE name = 'Glasgow';
        UPDATE district SET latitude = 55.680488, longitude = -2.562334 WHERE name = 'Gordon';
        UPDATE district SET latitude = 55.777633, longitude = -4.053679 WHERE name = 'Hamilton and South Lanarkshire';
        UPDATE district SET latitude = 55.931657, longitude = -4.680016 WHERE name = 'Inverclyde District';
        UPDATE district SET latitude = 55.676004, longitude = -4.508740 WHERE name = 'Kilmarnock and Loudoun';
        UPDATE district SET latitude = 55.499050, longitude = -4.611870 WHERE name = 'Kyle and Ayrshire';
        UPDATE district SET latitude = 52.508442, longitude = -7.504818 WHERE name = 'Kyle and Carrick';
        UPDATE district SET latitude = 54.719534, longitude = -6.207250 WHERE name = 'Antrim';
        UPDATE district SET latitude = 54.865293, longitude = -6.280221 WHERE name = 'Ballymena';
        UPDATE district SET latitude = 55.070489, longitude = -6.517371 WHERE name = 'Ballymoney';
        UPDATE district SET latitude = 54.726187, longitude = -5.810121 WHERE name = 'Carrickfergus';
        UPDATE district SET latitude = 55.132580, longitude = -6.664610 WHERE name = 'Coleraine';
        UPDATE district SET latitude = 54.857800, longitude = -5.823622 WHERE name = 'Larne';
        UPDATE district SET latitude = 54.755373, longitude = -6.607980 WHERE name = 'Magherafelt';
        UPDATE district SET latitude = 55.204733, longitude = -6.253174 WHERE name = 'Moyle';
        UPDATE district SET latitude = 54.685279, longitude = -5.964502 WHERE name = 'Newtonabbey';
        UPDATE district SET latitude = 54.597285, longitude = -5.930120 WHERE name = 'Belfast';
        UPDATE district SET latitude = 54.590270, longitude = -5.692787 WHERE name = 'Ards';
        UPDATE district SET latitude = 54.575725, longitude = -5.888274 WHERE name = 'Castlereagh';
        UPDATE district SET latitude = 54.327699, longitude = -5.715767 WHERE name = 'Down';
        UPDATE district SET latitude = 54.516246, longitude = -6.058011 WHERE name = 'Lisburn';
        UPDATE district SET latitude = 54.350280, longitude = -6.652792 WHERE name = 'Armagh';
        UPDATE district SET latitude = 54.348729, longitude = -6.270480 WHERE name = 'Banbridge';
        UPDATE district SET latitude = 54.641816, longitude = -6.744389 WHERE name = 'Cookstown';
        UPDATE district SET latitude = 54.442926, longitude = -6.417565 WHERE name = 'Craigavon';
        UPDATE district SET latitude = 54.508268, longitude = -6.766589 WHERE name = 'Dungannon and South Tyrone';
        UPDATE district SET latitude = 54.174250, longitude = -6.339199 WHERE name = 'Newry and Mourne';
        UPDATE district SET latitude = 54.996612, longitude = -7.308575 WHERE name = 'Derry';
        UPDATE district SET latitude = 54.343829, longitude = -7.631873 WHERE name = 'Fermanagh';
        UPDATE district SET latitude = 55.045456, longitude = -6.933676 WHERE name = 'Limavady';
        UPDATE district SET latitude = 54.597715, longitude = -7.309960 WHERE name = 'Omagh';
        UPDATE district SET latitude = 54.827387, longitude = -7.463310 WHERE name = 'Strabane';
        UPDATE district SET latitude = 53.817505, longitude = -3.035675 WHERE name = 'Blackpool';
        UPDATE district SET latitude = 53.748575, longitude = -2.487529 WHERE name = 'Blackburn';
    END;
$$;

-- API endpoint for finding users near me
DO $$ 
    BEGIN
        DROP FUNCTION IF EXISTS sp_gymfit_api_near_me();
        DROP FUNCTION IF EXISTS sp_gymfit_api_users_near_me();
        CREATE OR REPLACE FUNCTION sp_gymfit_api_users_near_me(lat numeric, lon numeric, radius numeric, user_is_pt boolean, user_is_gym boolean)
         RETURNS json
         LANGUAGE sql
        AS $function$
            SELECT array_to_json(array_agg(result_to_return))
            FROM (
                SELECT 
                    DISTINCT(u.id),
                    u.username,
                    u.first_name as "firstName",
                    u.last_name as "lastName",
                    u.is_male as "isMale",
                    u.is_personal_trainer as "isPersonalTrainer",
                    u.is_gym as "isGym",
                    u.bio,
                    d.name as "district",
                    point(lon, lat) <@> point(d.longitude, d.latitude)::point AS "distance",
                    u.avatar_uploaded AS "avatarUploaded"
                FROM user_district ud
                JOIN "user" u ON u.id = ud.user_id
                JOIN district d ON d.id = ud.district_id
                WHERE (ud.district_id IN (
                        SELECT 
                           d.id
                        FROM district d
                        WHERE (point(lon, lat) <@> point(d.longitude, d.latitude)) < radius -- SEARCH RADIUS IN MILES
                        GROUP by d.id
                    )
                ) AND u.is_personal_trainer = user_is_pt AND u.is_gym = user_is_gym

                ORDER by distance ASC
            ) as "result_to_return"
        $function$;
    END;
$$;