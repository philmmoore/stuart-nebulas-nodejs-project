DROP TABLE advert_to_area;
DROP TABLE area;

INSERT INTO advert_country (advert_id, country_id) VALUES (1, 1);
INSERT INTO advert_region (advert_id, region_id) VALUES (1, 3);
INSERT INTO advert_county (advert_id, county_id) VALUES (1, 53);
INSERT INTO advert_district (advert_id, district_id) VALUES (1, 6);

CREATE OR REPLACE FUNCTION sp_gymfit_site_advert_get_for_location
(
	_country_id INT,
	_region_id INT,
	_county_id INT,
	_district_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))	FROM (
		(
				SELECT					a.id,
												a.link,
												a.small_horizontal_image_extension AS "smallHorizontalImageExtension",
												a.large_vertical_image_extension AS "largeVerticalImageExtension",
												1 AS o
				FROM						advert AS a
				INNER JOIN 			advert_district AS district
				ON							district.advert_id = a.id
				AND							district.district_id = _district_id
		)
		UNION
		(
				SELECT					a.id,
												a.link,
												a.small_horizontal_image_extension AS "smallHorizontalImageExtension",
												a.large_vertical_image_extension AS "largeVerticalImageExtension",
												2 AS o
				FROM						advert AS a
				INNER JOIN 			advert_county AS county
				ON 							county.advert_id = a.id
				AND 						county.county_id = _county_id
		)
		UNION
		(
				SELECT					a.id,
												a.link,
												a.small_horizontal_image_extension AS "smallHorizontalImageExtension",
												a.large_vertical_image_extension AS "largeVerticalImageExtension",
												3 AS o
				FROM						advert AS a
				INNER JOIN 			advert_region AS region
				ON							region.advert_id = a.id
				AND 						region.region_id = _region_id
		)
		UNION
		(
				SELECT					a.id,
												a.link,
												a.small_horizontal_image_extension AS "smallHorizontalImageExtension",
												a.large_vertical_image_extension AS "largeVerticalImageExtension",
												4 AS o
				FROM						advert AS a
				INNER JOIN 			advert_country AS country
				ON							country.advert_id = a.id
				AND							country.country_id = _country_id
		)
		ORDER BY				o
		LIMIT						2
	) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_get_advertisements
(
	_countryId INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))	FROM (
		SELECT 		a.id,
					 		a.name,
					 		a.link,
	          	(
								array
								(
									SELECT			c.name
									FROM				country AS c
									INNER JOIN	advert_country AS ac
		            	ON 					ac.advert_id = a.id
									AND					ac.country_id = c.id
									UNION
									SELECT			r.name
									FROM				region AS r
									INNER JOIN	advert_region AS ar
		            	ON 					ar.advert_id = a.id
									AND					ar.region_id = r.id
									UNION
									SELECT			ct.name
									FROM				county AS ct
									INNER JOIN	advert_county AS act
		            	ON 					act.advert_id = a.id
									AND					act.county_id = ct.id
									UNION
									SELECT			d.name
									FROM				district AS d
									INNER JOIN	advert_district AS ad
		            	ON 					ad.advert_id = a.id
									AND					ad.district_id = d.id
								)
	         		) AS areas
		FROM	 		advert AS a
		WHERE  		country = _countryId
		ORDER BY 	name
	) AS result;
$$
LANGUAGE sql;

DROP FUNCTION sp_gymfit_admin_get_all_areas_for_country(INT);

CREATE OR REPLACE FUNCTION sp_gymfit_admin_get_all_regions_for_country
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM
		(
			SELECT		id,
								name
			FROM			region
			WHERE			country_id = _country_id
			ORDER BY	name
		) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_get_all_counties_for_region
(
	_region_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM
		(
			SELECT		id,
								name
			FROM			county
			WHERE			region_id = _region_id
			ORDER BY	name
		) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_get_all_districts_for_county
(
	_county_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM
		(
			SELECT		id,
								name
			FROM			district
			WHERE			county_id = _county_id
			ORDER BY	name
		) AS result;
$$
LANGUAGE sql;

DROP FUNCTION sp_gymfit_admin_get_advertisement(INT);

CREATE OR REPLACE FUNCTION sp_gymfit_admin_get_advertisement
(
	_advertisement_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
	(
		SELECT	advert.id,
						advert.name,
						advert.link,
						advert.small_horizontal_image_extension "smallHorizontalImageExtension",
						advert.large_vertical_image_extension "largeVerticalImageExtension",
						(
							array
							(
								SELECT	region_id AS "id"
								FROM		advert_region
								WHERE		advert_id = advert.id
							)
						) AS regions,
						(
							array
							(
								SELECT	county_id AS "id"
								FROM		advert_county
								WHERE		advert_id = advert.id
							)
						) AS counties,
						(
							array
							(
								SELECT	district_id AS "id"
								FROM		advert_district
								WHERE		advert_id = advert.id
							)
						) AS districts
		FROM		advert AS advert
		WHERE		advert.id = _advertisement_id
		LIMIT		1
	) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_get_country_selection
(
	_country_id INT,
	_advert_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM (
		SELECT	c.name,
						(
							SELECT	COUNT(*)
							FROM 		advert_country
							WHERE		country_id = c.id
							AND			advert_id = _advert_id
						) AS "hasAdvert"
		FROM		country AS c
		WHERE		c.id = _country_id
		LIMIT		1
	) AS result;
$$
LANGUAGE sql;

DROP FUNCTION sp_gymfit_admin_advert_save(INT, INT, TEXT, TEXT, INT[]);

CREATE FUNCTION sp_gymfit_admin_advert_save
(
	_country_id INT,
	_advert_id INT,
	_name TEXT,
	_link TEXT,
	_country_selected BIT,
	_regions INT[],
	_counties INT[],
	_districts INT[]
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	advert
		SET			name = _name,
						link = _link
		WHERE		id = _advert_id;

		IF FOUND THEN
			DELETE FROM	advert_country
			WHERE				advert_id = _advert_id;

			DELETE FROM	advert_region
			WHERE				advert_id = _advert_id;

			DELETE FROM	advert_county
			WHERE				advert_id = _advert_id;

			DELETE FROM	advert_district
			WHERE				advert_id = _advert_id;
		ELSE
			INSERT INTO advert (country, name, link)
			VALUES			(_country_id, _name, _link)
			RETURNING		id
			INTO				_advert_id;
		END IF;

		IF _country_selected = '1' THEN
			INSERT INTO	advert_country (advert_id, country_id)
			VALUES			(_advert_id, _country_id);
		END IF;

		INSERT INTO		advert_region (advert_id, region_id)
		SELECT				_advert_id,
									region
		FROM					unnest(_regions) AS region;

		INSERT INTO		advert_county (advert_id, county_id)
		SELECT				_advert_id,
									county
		FROM					unnest(_counties) AS county;

		INSERT INTO		advert_district (advert_id, district_id)
		SELECT				_advert_id,
									district
		FROM					unnest(_districts) AS district;

		RETURN to_json(_advert_id);
	END;
$$
LANGUAGE plpgsql;
