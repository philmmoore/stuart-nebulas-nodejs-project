UPDATE challenge SET hidden = TRUE WHERE id IN (SELECT c.id FROM challenge AS c JOIN challenge_category AS cc ON c.category = cc.id WHERE cc.value = 'Special');

DELETE
FROM		social_notification
WHERE		id IN
(
  SELECT 	n.id
  FROM		social_notification AS n
  JOIN		result AS r ON json_extract_path_text(n.notification, 'resultId')::bigint = r.id
  JOIN    challenge AS c ON c.id = r.challenge_id
  WHERE 	c.hidden = TRUE
);

CREATE OR REPLACE FUNCTION sp_gymfit_api_challenge_get_for_category
(
	_category_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
		(
              SELECT  array
                      (
                          SELECT row_to_json(a)
                          FROM (
                                  SELECT			cg.id,
                                              cg.value AS "name"
                                  FROM				challenge_group AS cg
                                  INNER JOIN	challenge_to_challenge_group AS ccg ON ccg.challenge_group_id = cg.id
                                  WHERE				ccg.challenge_id = c.id
                          ) AS a
                      ) AS "groups",
                      c.id,
                      c.name,
                      cc.value AS "category",
                      cc.id AS "categoryId",
                      ct.value AS "type",
                      ct.measurement,
                      c.body_copy AS "bodyCopy",
                      c.submission_message AS "submissionMessage",
                      c.coming_soon AS "comingSoon"
			FROM		        challenge AS c
			LEFT OUTER JOIN challenge_category AS cc ON cc.id = c.category
			LEFT OUTER JOIN	challenge_type AS ct ON ct.id = c.type
			WHERE		        c.category = _category_id
      AND						  c.hidden IS FALSE
		) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_challenge_get()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
		(
			SELECT          array
                      (
                          SELECT row_to_json(a)
                          FROM (
                                  SELECT			cg.id,
                                              cg.value AS "name"
                                  FROM				challenge_group AS cg
                                  INNER JOIN	challenge_to_challenge_group AS ccg ON ccg.challenge_group_id = cg.id
                                  WHERE				ccg.challenge_id = c.id
                          ) AS a
                      ) AS "groups",
                      c.id,
                      c.name,
                      cc.value AS "category",
                      cc.id AS "categoryId",
                      ct.value AS "type",
                      ct.measurement,
                      c.body_copy AS "bodyCopy",
                      c.submission_message AS "submissionMessage",
                      c.coming_soon AS "comingSoon"
			FROM		        challenge AS c
			LEFT OUTER JOIN challenge_category AS cc ON cc.id = c.category
			LEFT OUTER JOIN	challenge_type AS ct ON ct.id = c.type
      WHERE						c.hidden IS FALSE
		) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_user_activity
(
	_user_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_previous_period_start TIMESTAMP,
	_previous_period_end TIMESTAMP
)
RETURNS JSON AS
$$
	DECLARE
		_district_id INT;
		_county_id INT;
		_region_id INT;
		_country_id INT;
		
	BEGIN
		SELECT		district_id
		FROM			user_district
		WHERE			user_id = _user_id
		ORDER BY	created DESC
		LIMIT			1
		INTO			_district_id;
		
		SELECT		county_id
		FROM			district
		WHERE			id = _district_id
		INTO			_county_id;
		
		SELECT		region_id
		FROM			county
		WHERE			id = _county_id
		INTO			_region_id;
		
		SELECT		country_id
		FROM			region
		WHERE			id = _region_id
		INTO			_country_id;

		RETURN array_to_json(array_agg(result_to_return))
		FROM
		(
			SELECT
				DISTINCT
				c.id AS "challengeId",
				c.name AS "challengeName",
				cg.id AS "challengeGroupId",
				cg.value AS "challengeGroupName",
				cc.value AS "category",
				cc.id AS "categoryId",
				ct.measurement AS "measurement",
				
				
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.district_id = _district_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "districtTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.county_id = _county_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "countyTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.region_id = _region_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "regionTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.country_id = _country_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "countryTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "worldTotal",
				
				
				
				
				
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM			result AS r
						WHERE			r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.district_id = _district_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "districtCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM			result AS r
						WHERE			r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.district_id = _district_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "districtPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.county_id = _county_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "countyCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.county_id = _county_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "countyPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.region_id = _region_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "regionCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.region_id = _region_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "regionPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.country_id = _country_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "countryCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.country_id = _country_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "countryPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
				) AS "worldCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = _user_id
			) AS "worldPrevious",
				
				
				
				
				
			(
				SELECT 		CASE WHEN ct.min_first = TRUE THEN
                    MIN(r.result)
									ELSE
										MAX(r.result)
									END
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				AND 			r.user_id = _user_id
			) AS "result",
			
			(
				SELECT 		MAX(r.submitted)
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				AND 			r.user_id = _user_id
			) AS "submitted"
				
				
				
			FROM 						result AS ee
			JOIN 						challenge AS c ON ee.challenge_id = c.id
			JOIN 						challenge_group AS cg ON ee.challenge_group_id = cg.id
			JOIN 						challenge_type AS ct on c.type = ct.id
			JOIN 						challenge_category AS cc ON cc.id = c.category
			WHERE						ee.challenge_id IN (SELECT challenge_id FROM result WHERE user_id = _user_id)
			AND							ee.challenge_group_id IN (SELECT challenge_group_id FROM result WHERE user_id = _user_id)
      AND             c.hidden = FALSE
			AND
			(
				SELECT COUNT(r.result)
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				AND 			r.user_id = _user_id
			) > 0
			ORDER BY				submitted DESC

		) AS result_to_return;
	END
$$
LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION sp_gymfit_api_result_get
(
	_result_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_previous_period_start TIMESTAMP,
	_previous_period_end TIMESTAMP
)
RETURNS JSON AS
$$	
	BEGIN
		RETURN row_to_json(result_to_return)
		FROM
		(
			SELECT
				c.id AS "challengeId",
				c.name AS "challengeName",
				cg.id AS "challengeGroupId",
				cg.value AS "challengeGroupName",
				cc.value AS "category",
				cc.id AS "categoryId",
				ct.measurement AS "measurement",
				
				ee.result,
				ee.submitted,
				ee.image_validated AS "imageValidated",
				ee.video_validated AS "videoValidated",
				
				u.id AS "userId",
				u.username,
				u.first_name AS "firstName",
				u.last_name AS "lastName",
				u.avatar_uploaded AS "avatarUploaded",
				
				
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.district_id = ee.district_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "districtTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.county_id = ee.county_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "countyTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.region_id = ee.region_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "regionTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND							r.country_id = ee.country_id
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "countryTotal",
				
				(SELECT COUNT(*) FROM
					(
						SELECT DISTINCT r.user_id
						FROM 						result AS r
						WHERE 					r.challenge_id = ee.challenge_id
						AND							r.submitted >= _this_period_start
						AND							r.submitted <= _this_period_end
						AND 						r.excluded IS NULL
						AND 						r.challenge_group_id = ee.challenge_group_id
						GROUP BY 				r.user_id
					) AS xy
				) AS "worldTotal",
				
				
				
				
				
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM			result AS r
						WHERE			r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.district_id = ee.district_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "districtCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM			result AS r
						WHERE			r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.district_id = ee.district_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "districtPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.county_id = ee.county_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "countyCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.county_id = ee.county_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "countyPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.region_id = ee.region_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "regionCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.region_id = ee.region_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "regionPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND				r.country_id = ee.country_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "countryCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND				r.country_id = ee.country_id
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "countryPrevious",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _this_period_start
						AND				r.submitted <= _this_period_end
						AND 			r.excluded IS NULL
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
				) AS "worldCurrent",
				(
					SELECT xy.row_number
					FROM
					(
						SELECT 		CASE WHEN ct.min_first = TRUE THEN
                        row_number() over (ORDER BY MIN(r.result))
											ELSE
												row_number() over (ORDER BY MAX(r.result) DESC)
											END AS row_number,
											r.user_id
						FROM 			result AS r
						WHERE 		r.challenge_id = ee.challenge_id
						AND				r.submitted >= _previous_period_start
						AND				r.submitted <= _previous_period_end
						AND 			r.excluded IS NULL
						AND 			r.challenge_group_id = ee.challenge_group_id
						GROUP BY 	r.user_id
					) AS xy
					WHERE xy.user_id = u.id
			) AS "worldPrevious",
				
				
				
				
				
			(
				SELECT 		CASE WHEN ct.min_first = TRUE THEN
                    MIN(r.result)
									ELSE
										MAX(r.result)
									END
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				AND 			r.user_id = u.id
			) AS "result",
			
			(
				SELECT 		MAX(r.submitted)
				FROM 			result AS r
				WHERE 		r.challenge_id = ee.challenge_id
				AND 			r.excluded IS NULL
				AND 			r.challenge_group_id = ee.challenge_group_id
				AND 			r.user_id = u.id
			) AS "submitted"
				
				
				
			FROM 						result AS ee
			JOIN						"user" AS u ON u.id = ee.user_id
			JOIN 						challenge AS c ON ee.challenge_id = c.id
			JOIN 						challenge_group AS cg ON ee.challenge_group_id = cg.id
			JOIN 						challenge_type AS ct on c.type = ct.id
			JOIN 						challenge_category AS cc ON cc.id = c.category
			WHERE						ee.id = _result_id
      AND             c.hidden = FALSE

		) AS result_to_return;
	END
$$
LANGUAGE plpgsql;