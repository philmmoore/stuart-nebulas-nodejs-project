CREATE OR REPLACE FUNCTION public.sp_gymfit_site_user_set_is_gym(_user_id integer, _is_gym boolean)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
    BEGIN

        RETURN sp_gymfit_api_user_set_is_gym(_user_id, _is_gym);

    END;
$function$;

CREATE OR REPLACE FUNCTION public.sp_gymfit_site_user_set_is_pt(_user_id integer, _is_pt boolean)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
    BEGIN

        RETURN sp_gymfit_api_user_set_is_personal_trainer(_user_id, _is_pt);

    END;
$function$;
