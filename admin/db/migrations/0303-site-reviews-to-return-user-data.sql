DROP FUNCTION IF EXISTS sp_gymfit_site_user_reviews(_user_id integer);
DROP FUNCTION IF EXISTS sp_gymfit_site_user_reviews(_user_id integer, _number_to_return integer);
CREATE OR REPLACE FUNCTION sp_gymfit_site_user_reviews(_user_id integer, _number_to_return integer)
 RETURNS json
 LANGUAGE sql
AS $function$
   
  SELECT array_to_json(array_agg(result_to_return))
  FROM (
    SELECT 
      id,
      sp_gymfit_site_user_get(reviewer_user_id) AS "reviewer",
      title AS "title",
      body AS "body",
      rating AS "rating",
      created AS "created",
      last_updated AS "lastUpdated"
    FROM user_reviews
    WHERE user_id = _user_id
    ORDER BY last_updated DESC
    LIMIT _number_to_return
  ) as "result_to_return"

$function$;

CREATE OR REPLACE FUNCTION sp_gymfit_api_user_reviews(_user_id integer, _number_to_return integer)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
   
  BEGIN

    if (_number_to_return > 0) THEN 
      RETURN sp_gymfit_site_user_reviews(_user_id, _number_to_return);
    ELSE 
      RETURN sp_gymfit_site_user_reviews(_user_id, NULL);
    END IF;

  END;

$function$;