CREATE FUNCTION sp_admin_challenge_get_groups()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM
		(
			SELECT	id,
							value
			FROM challenge_group
		) AS result;
$$
LANGUAGE sql;
