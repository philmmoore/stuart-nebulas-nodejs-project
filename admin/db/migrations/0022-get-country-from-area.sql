CREATE OR REPLACE FUNCTION sp_site_get_country_from_area
(
	_name TEXT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)	FROM (
		SELECT id,
				name
		FROM	area
		WHERE	parent IS NULL
		AND		LOWER(REPLACE(name, ' ', '-')) = LOWER(_name)
		LIMIT 1
	) AS result;
$$
LANGUAGE sql;
