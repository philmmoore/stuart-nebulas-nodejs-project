CREATE OR REPLACE FUNCTION sp_gymfit_admin_area_save_region
(
	_country_id INT,
	_region_id INT,
	_name TEXT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	region
		SET			name = _name
		WHERE		id = _region_id;

		IF NOT FOUND THEN
			INSERT INTO region (country_id, name)
			VALUES			(_country_id, _name)
			RETURNING		id
			INTO				_region_id;
		END IF;

		RETURN to_json(_region_id);
	END;
$$
LANGUAGE plpgsql;
