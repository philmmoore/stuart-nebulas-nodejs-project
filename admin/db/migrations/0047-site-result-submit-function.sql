ALTER TABLE "user" ADD CONSTRAINT user_unique_username UNIQUE (username);
ALTER TABLE admin_user ADD CONSTRAINT admin_user_unique_username UNIQUE (username);

INSERT INTO "user" (username, hash, first_name, last_name, date_of_birth)
VALUES ('hello@wearehinge.com', '\x00', 'Hinge', 'Ltd', '16-MAY-1990');

CREATE FUNCTION sp_gymfit_site_result_submit
(
	_user_id INT,
	_challenge_id INT,
	_challenge_group_id INT,
	_result INT
)
RETURNS JSON AS
$$
	DECLARE
		_result_id INT;
	BEGIN
		INSERT INTO	result (user_id, challenge_id, challenge_group_id, result)
		VALUES (
			_user_id,
			_challenge_id,
			_challenge_group_id,
			_result
		)
		RETURNING		id
		INTO				_result_id;

		RETURN to_json(_result_id);
	END
$$
LANGUAGE plpgsql;
