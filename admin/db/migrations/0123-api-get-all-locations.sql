CREATE OR REPLACE FUNCTION sp_gymfit_api_location_get_all_names()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
		(
			SELECT		name,
								id AS "districtID",
								null::int AS "countyId",
								null::int AS "regionId",
								null::int AS "countryId"
			FROM			district
			UNION
			SELECT		name,
								null::int AS "districtId",
								id AS "countyID",
								null::int AS "regionId",
								null::int AS "countryId"
			FROM			county
			UNION
			SELECT		name,
								null::int AS "districtId",
								null::int AS "countyId",
								id AS "regionId",
								null::int AS "countryId"
			FROM			region
			UNION
			SELECT		name,
								null::int AS "districtId",
								null::int AS "countyId",
								null::int AS "regionId",
								id AS "countryId"
			FROM			country
		) AS result_to_return;
$$
LANGUAGE sql;
