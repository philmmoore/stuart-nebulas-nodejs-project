CREATE FUNCTION sp_admin_get_page
(
	_pageId INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
		(
			SELECT	id,
							name,
							body_copy "bodyCopy"
			FROM		page
			WHERE		id = _pageId
			LIMIT		1
		) AS result;
$$
LANGUAGE sql;
