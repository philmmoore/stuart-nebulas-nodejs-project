CREATE OR REPLACE FUNCTION sp_gymfit_admin_home_latest_results()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
	(
		SELECT					COUNT(CASE WHEN f.flag = TRUE THEN 1 END) AS "flagCount",
										COUNT(CASE WHEN f.flag = FALSE THEN 1 END) AS "likeCount",
										r.id AS "resultId",
										r.result,
										r.image_validated AS "imageValidated",
										r.video_validated AS "videoValidated",
										r.submitted,
										u.id AS "userId",
										u.username AS "username",
										c.name AS "challengeName",
										cg.value AS "challengeGroup",
										ct.measurement AS "measurement",
										(SELECT COUNT(*) FROM result WHERE excluded IS NOT NULL AND user_id = u.id) AS "numberOfExcludedResults"
		FROM						result AS r
		LEFT OUTER JOIN	result_feedback AS f ON r.id = f.result_id
		JOIN						"user" AS u on u.id = r.user_id
		JOIN						challenge AS c on c.id = r.challenge_id
		JOIN						challenge_group AS cg on cg.id = r.challenge_group_id
		JOIN						challenge_type AS ct on ct.id = c.type
		WHERE						r.excluded IS NULL
		AND							r.approved IS NULL
		GROUP BY				r.id,
										u.id,
										c.id,
										cg.id,
										ct.id
	 	ORDER BY				r.submitted DESC
		LIMIT						20
	) AS result_to_return;
$$
LANGUAGE sql;