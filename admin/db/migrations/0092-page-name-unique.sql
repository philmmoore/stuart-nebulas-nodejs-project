CREATE UNIQUE INDEX page_enforce_names_unique_Index ON page (LOWER(name));

ALTER TABLE challenge DROP CONSTRAINT challenge_unique_name;
CREATE UNIQUE INDEX challenge_enforce_names_unique_Index ON challenge (LOWER(name));
