CREATE OR REPLACE FUNCTION sp_gymfit_task_runner_avatar_set (
  _user_id INT
)
RETURNS JSON AS
$$
BEGIN
	UPDATE  "user"
  SET     avatar_uploaded = '1'
  WHERE   id = _user_id;

  RETURN to_json(1);
END;
$$
LANGUAGE plpgsql;
