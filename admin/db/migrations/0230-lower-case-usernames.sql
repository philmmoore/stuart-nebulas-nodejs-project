CREATE OR REPLACE FUNCTION sp_gymfit_api_register_user
(
	_username TEXT,
	_hash BYTEA,
	_first_name TEXT,
	_last_name TEXT,
	_date_of_birth DATE,
	_email TEXT
)
RETURNS JSON AS
$$
	DECLARE
		_user_id INT;
	BEGIN
		INSERT INTO	"user" (username, hash, first_name, last_name, date_of_birth, email)
		VALUES (
								LOWER(_username),
								_hash,
								_first_name,
								_last_name,
								_date_of_birth,
								_email
		);

		SELECT			id
		FROM				"user"
		WHERE				username = LOWER(_username)
		INTO				_user_id;
		
		DELETE FROM	user_registration
		WHERE				email IN (SELECT email FROM "user");

		INSERT INTO	user_email_verification (user_id)
		VALUES			(_user_id);

		INSERT INTO	user_notification (user_id, notification_id)
		VALUES			(_user_id, 1),
								(_user_id, 2),
								(_user_id, 3),
								(_user_id, 4);

		RETURN row_to_json(result)
		FROM (
			SELECT					u.id,
											u.email,
											ev.key AS "verificationKey"
			FROM						"user" AS u
			LEFT OUTER JOIN	user_email_verification AS ev ON ev.user_id = u.id
			WHERE						u.id = _user_id
		) AS result;
	END;
$$
LANGUAGE plpgsql;