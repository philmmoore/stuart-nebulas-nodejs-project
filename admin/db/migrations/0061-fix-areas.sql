INSERT INTO	region (country_id, name)
SELECT			1,
						name
FROM				area
WHERE				parent = 137
ORDER BY		id;

INSERT INTO	county (region_id, name)
SELECT			parent,
						name
FROM				area
WHERE				parent < 10
ORDER BY		id;

INSERT INTO	district (county_id, name)
SELECT			53,
						name
FROM				area
WHERE				parent = 62
ORDER BY		id;

ALTER TABLE country ADD name TEXT;
ALTER TABLE country ADD is_available BIT;
UPDATE country SET name = 'Great Britain', is_available = '1';
ALTER TABLE country ALTER COLUMN name SET NOT NULL;

CREATE FUNCTION sp_gymfit_site_location_get_country
(
	_country_name TEXT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM (
		SELECT						id,
											name,
											is_available "isAvailable"
		FROM							country
		WHERE							LOWER(REPLACE(code, ' ', '-')) = LOWER(_country_name)
		LIMIT							1
	) AS result;
$$
LANGUAGE sql;

CREATE FUNCTION sp_gymfit_site_location_get_region
(
	_country_id INT,
	_region_name TEXT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM (
		SELECT						id,
											name
		FROM							region
		WHERE							country_id = _country_id
		AND								LOWER(REPLACE(name, ' ', '-')) = LOWER(_region_name)
		LIMIT							1
	) AS result;
$$
LANGUAGE sql;

CREATE FUNCTION sp_gymfit_site_location_get_county
(
	_region_id INT,
	_county_name TEXT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM (
		SELECT						id,
											name
		FROM							county
		WHERE							region_id = _region_id
		AND								LOWER(REPLACE(name, ' ', '-')) = LOWER(_county_name)
		LIMIT							1
	) AS result;
$$
LANGUAGE sql;

CREATE FUNCTION sp_gymfit_site_location_get_district
(
	_county_id INT,
	_district_name TEXT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM (
		SELECT						id,
											name
		FROM							district
		WHERE							county_id = _county_id
		AND								LOWER(REPLACE(name, ' ', '-')) = LOWER(_district_name)
		LIMIT							1
	) AS result;
$$
LANGUAGE sql;

CREATE FUNCTION sp_gymfit_site_location_get_regions
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM (
		SELECT						name
		FROM							region
		WHERE							country_id = _country_id
		ORDER BY					name
	) AS result;
$$
LANGUAGE sql;

CREATE FUNCTION sp_gymfit_site_location_get_counties
(
	_region_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM (
		SELECT						name
		FROM							county
		WHERE							region_id = _region_id
		ORDER BY					name
	) AS result;
$$
LANGUAGE sql;

CREATE FUNCTION sp_gymfit_site_location_get_districts
(
	_county_id INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM (
		SELECT						name
		FROM							district
		WHERE							county_id = _county_id
		ORDER BY					name
	) AS result;
$$
LANGUAGE sql;
