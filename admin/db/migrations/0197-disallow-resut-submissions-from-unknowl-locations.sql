INSERT INTO 		user_notification (user_id, notification_id)
SELECT 					u.id,
								3
FROM 						"user" AS u
LEFT OUTER JOIN user_region AS ur ON ur.user_id = u.id
LEFT OUTER JOIN user_district AS ud ON ud.user_id = u.id
WHERE 					(SELECT COUNT(id) FROM user_notification WHERE user_id = u.id AND notification_id = 3 AND acknowledged IS NULL) < 1
AND							(ur IS NULL OR ud IS NULL);