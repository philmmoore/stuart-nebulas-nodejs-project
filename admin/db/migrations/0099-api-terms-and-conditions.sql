CREATE OR REPLACE FUNCTION sp_gymfit_api_register_get_terms_and_conditions
(
	_country_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM (
		SELECT	body_copy AS "bodyCopy"
		FROM		page
		WHERE		country = _country_id
		AND			LOWER(REPLACE(name, ' ', '-')) = 'terms-and-conditions'
		LIMIT		1
	) AS result;
$$
LANGUAGE sql;
