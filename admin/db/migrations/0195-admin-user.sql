
CREATE FUNCTION sp_gymfit_admin_admin_user_get_all()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM
		(
			SELECT		id,
								username
			FROM			admin_user
			ORDER BY	username
		) AS result;
$$
LANGUAGE sql;

CREATE FUNCTION sp_gymfit_admin_admin_user_get
(
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
		(
			SELECT	id,
							username
			FROM		admin_user
			WHERE		id = _user_id
			LIMIT		1
		) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_admin_user_persist
(
	_country_id INT,
	_username TEXT,
	_hash BYTEA
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO admin_user
		(
			country,
			username,
			hash
		)
		VALUES (
			_country_id,
			_username,
			_hash
		);

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_admin_user_delete
(
	_user_id INT
)
RETURNS JSON AS
$$
	BEGIN
		DELETE FROM admin_user WHERE id = _user_id;

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;