CREATE OR REPLACE FUNCTION sp_site_get_areas_for_parent_id
(
	_parentId INT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))	FROM (
		SELECT		LOWER(REPLACE(name, ' ', '-')) AS link,
							name
		FROM			area
		WHERE			parent = _parentId
		ORDER BY	name
	) AS result;
$$
LANGUAGE sql;
