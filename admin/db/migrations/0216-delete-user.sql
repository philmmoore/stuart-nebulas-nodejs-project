CREATE OR REPLACE FUNCTION sp_gymfit_admin_user_delete
(
	_user_id INT,
	_reason TEXT
)
RETURNS JSON AS
$$
	BEGIN
		INSERT INTO banned_email
		(
			email,
			reason
		)
		SELECT			email,
								_reason
		FROM				"user"
		WHERE				id = _user_id;
		
		DELETE FROM	social_notification
		WHERE				user_id = _user_id;
		
		DELETE FROM	user_email_verification
		WHERE				user_id = _user_id;
		
		DELETE FROM	user_password_reset
		WHERE				user_id = _user_id;
	
		DELETE FROM	user_district
		WHERE				user_id = _user_id;
		
		DELETE FROM	user_region
		WHERE				user_id = _user_id;

		DELETE FROM user_height
		WHERE				user_id = _user_id;

		DELETE FROM user_weight
		WHERE				user_id = _user_id;

		DELETE FROM result_feedback
		WHERE				user_id = _user_id
		OR					result_id IN (SELECT id FROM result WHERE user_id = _user_id);
		
		DELETE FROM user_nomination
		WHERE				user_id = _user_id
		OR					nominee_user_id = _user_id;

		DELETE FROM result
		WHERE				user_id = _user_id;
		
		DELETE FROM user_routine_type
		WHERE				user_id = _user_id;
		
		DELETE FROM user_audit
		WHERE				user_id = _user_id;
		
		DELETE FROM user_notification
		WHERE				user_id = _user_id;
		
		DELETE FROM user_privacy_preferences
		WHERE				user_id = _user_id;
		
		DELETE FROM user_email_preference
		WHERE				user_id = _user_id;
		
		DELETE FROM	user_follower
		WHERE				user_id = _user_id
		OR					follower_user_id = _user_id;
		
		DELETE FROM	user_client
		WHERE				user_id = _user_id
		OR					client_user_id = _user_id;

		DELETE FROM "user"
		WHERE 			id = _user_id;

		RETURN to_json(1);
	END;
$$
LANGUAGE plpgsql;