CREATE OR REPLACE FUNCTION sp_site_get_page
(
	_countryId INT,
	_name TEXT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM (
		SELECT	name,
						body_copy AS "bodyCopy"
		FROM		page
		WHERE		country = _countryId
		AND			LOWER(REPLACE(name, ' ', '-')) = LOWER(_name)
		LIMIT		1
	) AS result;
$$
LANGUAGE sql;
