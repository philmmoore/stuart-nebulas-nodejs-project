CREATE OR REPLACE FUNCTION sp_gymfit_site_user_get
(
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		SELECT		u.id,
							u.username,
							u.first_name AS "firstName",
							u.last_name AS "lastName",
							u.is_male AS "isMale",
							u.date_of_birth AS "dateOfBirth",
							(
								SELECT row_to_json(result_location)
								FROM
								(
									SELECT
									(
										SELECT row_to_json(result_a)
										FROM
										(
											SELECT	d.id,
												d.name
										) AS result_a
									) AS "district",
									(
										SELECT row_to_json(result_b)
										FROM
										(
											SELECT	c.id,
												c.name
										) AS result_b
									) AS "county",
									(
										SELECT row_to_json(result_c)
										FROM
										(
											SELECT	r.id,
												r.name
										) AS result_c
									) AS "region",
									(
										SELECT row_to_json(result_d)
										FROM
										(
											SELECT	ct.id,
												ct.name,
												ct.code
										) AS result_d
									) AS "country"
								) AS result_location
							) AS "location",
              uh.height,
              uw.weight,
              rt.value AS "routineType"
		FROM		"user" AS u
		LEFT OUTER JOIN	user_district AS ud
		ON							u.id = ud.user_id
		LEFT OUTER JOIN	district AS d
		ON							ud.district_id = d.id
		LEFT OUTER JOIN	county AS c
		ON							d.county_id = c.id
		LEFT OUTER JOIN region AS r
		ON							c.region_id = r.id
		LEFT OUTER JOIN	country AS ct
		ON							r.country_id = ct.id
    LEFT OUTER JOIN user_height AS uh
    ON              u.id = uh.user_id
    LEFT OUTER JOIN user_weight AS uw
    ON              u.id = uw.user_id
    LEFT OUTER JOIN user_routine_type AS ur
    ON              u.id = ur.user_id
    LEFT OUTER JOIN routine_type AS rt
    ON              ur.routine_type_id = rt.id
		WHERE						u.id = _user_id
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_register_persist_step_6_details (
  _user_id INT,
  _occupation TEXT,
  _relationship_status_id INT,
  _nationality TEXT,
  _routine_type_id INT,
  _gym_id INT
)
RETURNS JSON AS
$$
BEGIN
	UPDATE   "user"
  SET     occupation = _occupation,
          nationality = _nationality
  WHERE   id = _user_id;

  IF _relationship_status_id IS NOT NULL THEN
    INSERT INTO user_relationship_status (user_id, relationship_status_id)
    VALUES (_user_id, _relationship_status_id);
  END IF;

  IF _routine_type_id IS NOT NULL THEN
    INSERT INTO user_routine_type (user_id, routine_type_id)
    VALUES (_user_id, _routine_type_id);
  END IF;

  IF _gym_id IS NOT NULL THEN
    INSERT INTO user_gym (user_id, gym_id)
    VALUES (_user_id, _gym_id);
  END IF;

  RETURN sp_gymfit_site_user_get(_user_id);
END;
$$
LANGUAGE plpgsql;
