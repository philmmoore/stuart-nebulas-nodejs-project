CREATE FUNCTION sp_gymfit_admin_advert_attach_image
(
	_advertId INT,
	_type TEXT,
	_extension TEXT
)
RETURNS JSON AS
$$
  BEGIN
    IF _type = 'sm' THEN
      UPDATE  advert
      SET     small_horizontal_image_extension = _extension
      WHERE   id = _advertId;
    ELSE
        UPDATE  advert
        SET     large_vertical_image_extension = _extension
        WHERE   id = _advertId;
    END IF;

  	RETURN to_json(_advertId);
  END
$$
LANGUAGE plpgsql;
