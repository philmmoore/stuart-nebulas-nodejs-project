CREATE OR REPLACE FUNCTION sp_gymfit_site_get_challenge_groups
(
	_challenge_name TEXT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM
	(
		SELECT			cg.id,
								cg.value AS name
		FROM				challenge_group AS cg
		INNER JOIN	challenge_to_challenge_group AS ccg ON cg.id = ccg.challenge_group_id
		INNER JOIN	challenge AS c ON c.id = ccg.challenge_id
		WHERE				LOWER(REPLACE(c.name, ' ', '-')) = LOWER(_challenge_name)
	) AS result;
$$
LANGUAGE sql;
