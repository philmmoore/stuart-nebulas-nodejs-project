CREATE OR REPLACE FUNCTION sp_gymfit_api_user_set_first_name
(
  _user_id INT,
  _first_name TEXT
)
RETURNS JSON AS
$$
  UPDATE "user"
  SET   first_name = _first_name
  WHERE id = _user_id;
  
  SELECT  to_json(
    (
      SELECT  COUNT(id) = 1
      FROM    "user"
      WHERE   id = _user_id
    )
  );
  
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_user_set_last_name
(
  _user_id INT,
  _last_name TEXT
)
RETURNS JSON AS
$$
  UPDATE "user"
  SET   last_name = _last_name
  WHERE id = _user_id;
  
  SELECT  to_json(
    (
      SELECT  COUNT(id) = 1
      FROM    "user"
      WHERE   id = _user_id
    )
  );
  
$$
LANGUAGE sql;