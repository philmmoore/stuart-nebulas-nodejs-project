CREATE OR REPLACE FUNCTION sp_gymfit_admin_result_get
(
	_result_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
		(
			SELECT					CASE WHEN COUNT(f) < 1 THEN 0
											ELSE
												ROUND(100.0 * COUNT(CASE WHEN f.flag = TRUE THEN 1 END) / COUNT(f.flag), 2)
											END AS "flagPercent",
											COUNT(CASE WHEN f.flag = TRUE THEN 1 END) AS "flagCount",
											COUNT(CASE WHEN f.flag = FALSE THEN 1 END) AS "likeCount",
											r.id AS "resultId",
											r.result,
											r.submitted,
											r.image_validated AS "imageValidated",
											r.video_validated AS "videoValidated",
											u.id AS "userId",
											u.username AS "username",
											u.email,
											c.name AS "challengeName",
											cg.value AS "challengeGroup",
											ct.measurement AS "measurement",
											(SELECT COUNT(*) FROM result WHERE excluded IS NOT NULL AND user_id = u.id) AS "numberOfExcludedResults"
			FROM						result AS r
			LEFT OUTER JOIN	result_feedback AS f ON r.id = f.result_id
			JOIN						"user" AS u on u.id = r.user_id
			JOIN						challenge AS c on c.id = r.challenge_id
			JOIN						challenge_group AS cg on cg.id = r.challenge_group_id
			JOIN						challenge_type AS ct on ct.id = c.type
			WHERE						r.id = _result_id
			GROUP BY				r.id,
											u.id,
											c.id,
											cg.id,
											ct.id
		) AS result_to_return;
$$
LANGUAGE sql;