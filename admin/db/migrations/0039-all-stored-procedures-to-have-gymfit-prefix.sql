ALTER FUNCTION sp_admin_user_login(TEXT, BYTEA) RENAME TO sp_gymfit_admin_user_login;
ALTER FUNCTION sp_admin_get_advertisements(INT) RENAME TO sp_gymfit_admin_get_advertisements;
ALTER FUNCTION sp_admin_get_advertisement(INT) RENAME TO sp_gymfit_admin_get_advertisement;
ALTER FUNCTION sp_admin_get_all_areas_for_country(INT) RENAME TO sp_gymfit_admin_get_all_areas_for_country;
ALTER FUNCTION sp_admin_get_all_pages_for_country(INT) RENAME TO sp_gymfit_admin_get_all_pages_for_country;
ALTER FUNCTION sp_admin_save_page(INT, INT, TEXT, TEXT) RENAME TO sp_gymfit_admin_save_page;
ALTER FUNCTION sp_admin_save_challenge(INT, TEXT, INT, INT, TEXT, TEXT) RENAME TO sp_gymfit_admin_save_challenge;
ALTER FUNCTION sp_admin_advert_save(INT, INT, TEXT, TEXT) RENAME TO sp_gymfit_admin_advert_save;
ALTER FUNCTION sp_admin_challenge_get_groups() RENAME TO sp_gymfit_admin_challenge_get_groups;
ALTER FUNCTION sp_admin_challenge_get_challenge(INT) RENAME TO sp_gymfit_admin_challenge_get_challenge;
ALTER FUNCTION sp_admin_challenge_get_challenges() RENAME TO sp_gymfit_admin_challenge_get_challenges;
ALTER FUNCTION sp_admin_get_page(INT) RENAME TO sp_gymfit_admin_get_page;
ALTER FUNCTION sp_site_get_adverts_for_location(INT, INT, INT, INT) RENAME TO sp_gymfit_site_get_adverts_for_location;
ALTER FUNCTION sp_site_get_country_from_area(TEXT) RENAME TO sp_gymfit_site_get_country_from_area;
ALTER FUNCTION sp_site_get_child_area_from_parent_area(INT, TEXT) RENAME TO sp_gymfit_site_get_child_area_from_parent_area;
ALTER FUNCTION sp_site_get_challenge(TEXT) RENAME TO sp_gymfit_site_get_challenge;
ALTER FUNCTION sp_site_get_areas_for_parent_id(INT) RENAME TO sp_gymfit_site_get_areas_for_parent_id;
ALTER FUNCTION sp_site_get_challenges() RENAME TO sp_gymfit_site_get_challenges;
ALTER FUNCTION sp_site_get_page(INT, TEXT) RENAME TO sp_gymfit_site_get_page;
