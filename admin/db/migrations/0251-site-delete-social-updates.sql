CREATE OR REPLACE FUNCTION sp_gymfit_site_social_update_delete
(
  _user_id INT,
  _id UUID
)
RETURNS JSON AS
$$
  DECLARE
    _response JSON;
    
  BEGIN
    UPDATE  result
    SET     excluded = current_timestamp,
            exclude_reason = 'user removed'
    WHERE   id = (SELECT result_id FROM social_update_to_result WHERE update_id = _id)
    AND     user_id = _user_id;

    UPDATE  social_update
    SET     rejected = current_timestamp,
            rejected_reason = 'user removed'
    WHERE   id = _id
    AND     user_id = _user_id;

  	SELECT row_to_json(result_to_return)
  	FROM
  	(
      SELECT		      array
                      (
                        SELECT			id
                        FROM				social_update_video
                        WHERE				update_id = u.id
                      ) AS "videos",
                      array
                      (
                        SELECT			id
                        FROM				social_update_image
                        WHERE				update_id = u.id
                      ) AS "images"
      FROM		        social_update AS u
      JOIN						"user" AS us ON us.id = u.user_id
      WHERE           u.id = _id
      AND             us.id = _user_id
  	) AS result_to_return
    INTO _response;
    
    DELETE FROM social_update_image
    WHERE       id = (SELECT id FROM social_update WHERE id = _id AND user_id = _user_id);
    
    DELETE FROM social_update_video
    WHERE       id = (SELECT id FROM social_update WHERE id = _id AND user_id = _user_id);
    
    RETURN _response;
  END
$$
LANGUAGE plpgsql;