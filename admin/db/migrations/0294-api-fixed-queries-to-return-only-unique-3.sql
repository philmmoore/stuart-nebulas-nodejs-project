CREATE OR REPLACE FUNCTION sp_gymfit_admin_user_featured_get()
 RETURNS json
 LANGUAGE sql
AS $function$
  SELECT array_to_json(array_agg(result_to_return))
  FROM
  (
    SELECT              DISTINCT ON (fu.user_id)
                      u.id,
                    u.username,
                    u.first_name AS "firstName",
                    u.last_name AS "lastName",
                    u.is_personal_trainer AS "isPersonalTrainer",
                    u.is_gym AS "isGym",
                    u.avatar_uploaded AS "avatarUploaded",
                    u.bio,
                     (          
                        SELECT row_to_json(result_location)
                        FROM
                        (
                            SELECT
                            r.name AS "region",
                            d.name AS "district"
                        ) AS result_location
                     ) AS "location"
    FROM            featured_users fu
    LEFT JOIN "user" u ON u.id = fu.user_id
    LEFT JOIN user_district ud ON ud.user_id = u.id
    LEFT JOIN user_region ur ON ur.user_id = u.id
    LEFT JOIN region r ON r.id = ur.region_id
    LEFT JOIN district d ON d.id = ud.district_id
    GROUP BY fu.user_id, u.id,r.name, d.name

  ) AS result_to_return;

$function$;