CREATE OR REPLACE FUNCTION sp_gymfit_site_user_get_privacy_preferences
(
	_user_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		SELECT					gymfit_email_opt_in AS "gymFitEmailOptIn",
										partners_email_opt_in AS "partnersEmailOptIn",
										gymfit_sms_opt_in AS "gymFitSMSOptIn",
										partners_sms_opt_in AS "partnersSMSOptIn",
										gymfit_online_opt_in AS "gymFitOnlineOptIn",
										partners_online_opt_in AS "partnersOnlineOptIn"
		FROM						user_privacy_preferences
		WHERE						user_id = _user_id
		ORDER BY				updated DESC
		LIMIT						1
	) AS result_to_return;
$$
LANGUAGE sql;