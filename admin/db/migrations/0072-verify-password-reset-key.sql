CREATE FUNCTION sp_gymfit_site_user_is_password_reset_key_valid
(
	_key UUID
)
RETURNS JSON AS
$$
SELECT array_to_json(array_agg(result_to_return))
FROM (
	SELECT			user_id
	FROM				user_password_reset
	WHERE				key = _key
) AS result_to_return;
$$
LANGUAGE sql;

CREATE FUNCTION sp_gymfit_site_user_reset_password_validate_email_key
(
	_email TEXT,
	_key UUID
)
RETURNS JSON AS
$$
SELECT array_to_json(array_agg(result_to_return))
FROM (
	SELECT					u.id
	FROM						user_password_reset AS pr
	LEFT OUTER JOIN "user" AS u ON u.id = pr.user_id
	WHERE						u.email = _email
	AND							pr.key = _key
) AS result_to_return;
$$
LANGUAGE sql;
