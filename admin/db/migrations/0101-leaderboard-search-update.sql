DROP FUNCTION IF EXISTS sp_gymfit_site_leaderboard_search(INT, INT, TIMESTAMP, TIMESTAMP, TIMESTAMP, TIMESTAMP, INT, INT);

TRUNCATE TABLE result RESTART IDENTITY CASCADE;

ALTER TABLE result ADD country_id INT REFERENCES country (id) NOT NULL;
ALTER TABLE result ADD region_id INT REFERENCES region (id) NOT NULL;
ALTER TABLE result ADD county_id INT REFERENCES county (id) NOT NULL;
ALTER TABLE result ADD district_id INT REFERENCES district (id) NOT NULL;

DROP FUNCTION sp_gymfit_site_result_submit(INT, INT, INT, INT);

CREATE FUNCTION sp_gymfit_site_result_submit
(
	_user_id INT,
	_challenge_id INT,
	_challenge_group_id INT,
	_result INT,
	_country_id INT,
	_region_id INT,
	_county_id INT,
	_district_id INT
)
RETURNS JSON AS
$$
	DECLARE
		_result_id INT;
	BEGIN
		INSERT INTO	result (
												user_id,
												challenge_id,
												challenge_group_id,
												result,
												country_id,
												region_id,
												county_id,
												district_id)
		VALUES (
			_user_id,
			_challenge_id,
			_challenge_group_id,
			_result,
			_country_id,
			_region_id,
			_county_id,
			_district_id
		)
		RETURNING		id
		INTO				_result_id;

		RETURN to_json(_result_id);
	END
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_leaderboard_search
(
	_challenge_id INT,
	_challenge_group_id INT,
	_district_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_last_period_start TIMESTAMP,
	_last_period_end TIMESTAMP,
	_results_per_page INT,
	_page INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		SELECT
		(
			SELECT array_to_json(array_agg(results))
			FROM
			(
				SELECT			row_number() over (ORDER BY MAX(r.result) DESC) AS "rank",
										u.id AS "userId",
										u.username,
                    u.first_name "firstName",
                    u.last_name "lastName",
										(SELECT MAX(r.result)) AS "result"
				from				result AS r
				INNER JOIN	"user" AS u
				ON					r.user_id = u.id
				WHERE				r.submitted >= _this_period_start
				AND					r.submitted <= _this_period_end
				AND					r.challenge_id = _challenge_id
				AND					r.challenge_group_id = _challenge_group_id
				AND					r.district_id = _district_id
				GROUP BY		u.id
				ORDER BY		rank
				OFFSET			(_page - 1) * _results_per_page
				LIMIT				_results_per_page
			) AS results
		) AS "results",
		(
			SELECT				ceiling(COUNT(DISTINCT r.user_id)::DECIMAL / _results_per_page)
			FROM					result AS r
			WHERE					r.submitted >= _this_period_start
			AND						r.submitted <= _this_period_end
			AND						r.challenge_id = _challenge_id
			AND						r.challenge_group_id = _challenge_group_id
		) AS "numberOfPages",
		(
			SELECT				ct.measurement
			FROM					challenge_type AS ct
			LEFT OUTER JOIN challenge AS c ON c.type = ct.id
			WHERE					c.id = _challenge_id
		) AS "measurement"
	) AS result_to_return;
$$
LANGUAGE sql;
