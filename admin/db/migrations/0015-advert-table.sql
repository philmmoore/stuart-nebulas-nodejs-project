CREATE TABLE advert (
  id SERIAL PRIMARY KEY,
  country INTEGER REFERENCES country (id) NOT NULL,
  name TEXT NOT NULL,
  link TEXT
);

CREATE TABLE advert_to_area (
  advert_id INTEGER REFERENCES advert (id) NOT NULL,
  area_id INTEGER REFERENCES area (id) NOT NULL,
  PRIMARY KEY (advert_id, area_id)
);

INSERT INTO advert (country, name, link)
VALUES
  (1, 'Test', 'http://google.com');

INSERT INTO advert_to_area
SELECT  1, id
FROM    area
WHERE   id = 62
OR      parent = 62;
