CREATE TABLE social_update
(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  created TIMESTAMP NOT NULL DEFAULT current_timestamp,
  user_id INT REFERENCES "user" (id) NOT NULL,
  body TEXT
);

CREATE TABLE social_update_tag
(
  id BIGSERIAL PRIMARY KEY,
  value TEXT NOT NULL
);

CREATE TABLE social_update_to_social_update_tag
(
  update_id UUID REFERENCES social_update (id),
  tag_id INT REFERENCES social_update_tag (id),
  PRIMARY KEY (update_id, tag_id)
);

CREATE TABLE social_update_to_result
(
  update_id UUID REFERENCES social_update (id),
  result_id BIGINT REFERENCES result (id),
  PRIMARY KEY (update_id, result_id)
);

CREATE TABLE social_update_video
(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  update_id UUID REFERENCES social_update (id),
  created TIMESTAMP NOT NULL DEFAULT current_timestamp,
  processed TIMESTAMP
);

CREATE TABLE social_update_image
(
  id UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  update_id UUID REFERENCES social_update (id),
  created TIMESTAMP NOT NULL DEFAULT current_timestamp,
  processed TIMESTAMP
);

CREATE TABLE social_update_like
(
  update_id UUID REFERENCES social_update (id),
  user_id INT REFERENCES "user",
  created TIMESTAMP NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (update_id, user_id)
);

CREATE TABLE social_update_flag
(
  update_id UUID REFERENCES social_update (id),
  user_id INT REFERENCES "user",
  created TIMESTAMP NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (update_id, user_id)
);

CREATE OR REPLACE FUNCTION sp_gymfit_api_social_update_new
(
  _user_id INT,
  _body TEXT,
  _tags TEXT
)
RETURNS JSON AS
$$
  DECLARE
    _id UUID;
  
  BEGIN
    INSERT INTO social_update (user_id, body)
    VALUES      (_user_id, _body)
    RETURNING   id
    INTO        _id;
    
    INSERT INTO social_update_tag (value)
    SELECT      trim(both FROM unnest)
    FROM        unnest(regexp_split_to_array(_tags, ','))
    WHERE       char_length(trim(both FROM unnest)) > 1
    AND         NOT EXISTS (SELECT value FROM social_update_tag WHERE LOWER(value) = LOWER(trim(both FROM unnest)));
    
    INSERT INTO social_update_to_social_update_tag (update_id, tag_id)
    SELECT      _id,
                id
    FROM        social_update_tag
    WHERE       LOWER(value) IN (SELECT LOWER(trim(both FROM unnest)) FROM unnest(regexp_split_to_array(_tags, ',')));
    
    RETURN row_to_json(result)
    FROM
    (
      SELECT _id AS "id"
    ) AS result;
  END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_social_update_attach_video
(
  _user_id INT,
  _update_id UUID
)
RETURNS JSON AS
$$
  DECLARE
    _id UUID;
  
  BEGIN
    INSERT INTO   social_update_video (update_id)
    SELECT      	id
    FROM          social_update
    WHERE         id = _update_id
    AND           user_id = _user_id
    RETURNING     id
    INTO          _id;
    
    RETURN row_to_json(result)
    FROM
    (
      SELECT _id AS "filename"
    ) AS result;
  END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_social_update_attach_image
(
  _user_id INT,
  _update_id UUID
)
RETURNS JSON AS
$$
  DECLARE
    _id UUID;
  
  BEGIN
    INSERT INTO   social_update_image (update_id)
    SELECT      	id
    FROM          social_update
    WHERE         id = _update_id
    AND           user_id = _user_id
    RETURNING     id
    INTO          _id;
    
    RETURN row_to_json(result)
    FROM
    (
      SELECT _id AS "filename"
    ) AS result;
  END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_social_update_like
(
  _user_id INT,
  _update_id UUID
)
RETURNS JSON AS
$$
  DECLARE
    _id UUID;
  
  BEGIN
    INSERT INTO social_update_like (update_id, user_id)
    SELECT      _update_id,
                _user_id
    WHERE       NOT EXISTS (SELECT * FROM social_update_like WHERE update_id = _update_id AND user_id = _user_id);
    
    DELETE FROM social_update_flag
    WHERE       update_id = _update_id
    AND         user_id = _user_id;
    
    RETURN row_to_json(result)
    FROM
    (
      SELECT  (SELECT COUNT(*) FROM social_update_like WHERE update_id = _update_id) AS "likes",
              (SELECT COUNT(*) FROM social_update_flag WHERE update_id = _update_id) AS "flags"
    ) AS result;
  END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_social_update_unlike
(
  _user_id INT,
  _update_id UUID
)
RETURNS JSON AS
$$
  DECLARE
    _id UUID;
  
  BEGIN
    DELETE FROM social_update_like
    WHERE       update_id = _update_id
    AND         user_id = _user_id;
    
    RETURN row_to_json(result)
    FROM
    (
      SELECT  (SELECT COUNT(*) FROM social_update_like WHERE update_id = _update_id) AS "likes",
              (SELECT COUNT(*) FROM social_update_flag WHERE update_id = _update_id) AS "flags"
    ) AS result;
  END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_social_update_flag
(
  _user_id INT,
  _update_id UUID
)
RETURNS JSON AS
$$
  DECLARE
    _id UUID;
  
  BEGIN
    INSERT INTO social_update_flag (update_id, user_id)
    SELECT      _update_id,
                _user_id
    WHERE       NOT EXISTS (SELECT * FROM social_update_flag WHERE update_id = _update_id AND user_id = _user_id);
    
    DELETE FROM social_update_like
    WHERE       update_id = _update_id
    AND         user_id = _user_id;
    
    RETURN row_to_json(result)
    FROM
    (
      SELECT  (SELECT COUNT(*) FROM social_update_like WHERE update_id = _update_id) AS "likes",
              (SELECT COUNT(*) FROM social_update_flag WHERE update_id = _update_id) AS "flags"
    ) AS result;
  END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_social_update_unflag
(
  _user_id INT,
  _update_id UUID
)
RETURNS JSON AS
$$
  DECLARE
    _id UUID;
  
  BEGIN
    DELETE FROM social_update_flag
    WHERE       update_id = _update_id
    AND         user_id = _user_id;
    
    RETURN row_to_json(result)
    FROM
    (
      SELECT  (SELECT COUNT(*) FROM social_update_like WHERE update_id = _update_id) AS "likes",
              (SELECT COUNT(*) FROM social_update_flag WHERE update_id = _update_id) AS "flags"
    ) AS result;
  END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_social_get_global_updates
(
  _from_timestamp TIMESTAMP,
  _number_of_updates INT
)
RETURNS JSON AS
$$
  SELECT array_to_json(array_agg(result_to_return))
  FROM
  (
    SELECT        ud.id,
                  ud.created,
                  ud.user_id AS "userId",
                  ud.body,
                  u.username,
                  u.first_name AS "firstName",
                  u.last_name AS "lastName",
                  u.avatar_uploaded AS "avatarUploaded",
                  (SELECT COUNT(*) FROM social_update_like WHERE update_id = ud.id) AS "likes",
                  (SELECT COUNT(*) FROM social_update_flag WHERE update_id = ud.id) AS "flags",
                  (
                      SELECT row_to_json(a) AS "result"
                      FROM
                      (
                              SELECT      r.id,
                                          r.result,
                                          r.submitted,
                                          c.id AS "challengeId",
                                          c.name AS "challengeName",
                                          cg.id AS "challengeGroupId",
                                          cg.value AS "challengeGroupName",
                                          cc.value AS "category",
                                          cc.id AS "categoryId",
                                          ct.measurement AS "measurement",
                                          r.image_validated AS "imageValidated",
                                          r.video_validated AS "videoValidated"
                              FROM    result AS r
                              JOIN    social_update_to_result AS ur ON ur.result_id = r.id
                              JOIN    challenge AS c ON r.challenge_id = c.id
                              JOIN    challenge_group AS cg ON r.challenge_group_id = cg.id
                              JOIN    challenge_type AS ct on c.type = ct.id
                              JOIN    challenge_category AS cc ON cc.id = c.category
                              WHERE   ur.update_id = ud.id
                              AND     r.excluded IS NULL
                      ) AS a
                  ),
                  array
                  (
                    SELECT			id
                    FROM				social_update_video
                    WHERE				update_id = ud.id
                    AND         processed IS NOT NULL
                  ) AS "videos",
                  array
                  (
                    SELECT			id
                    FROM				social_update_image
                    WHERE				update_id = ud.id
                    AND         processed IS NOT NULL
                  ) AS "images",
                  array
                  (
                    SELECT      t.value
                    FROM				social_update_to_social_update_tag AS ut
                    JOIN        social_update_tag AS t ON ut.tag_id = t.id
                    WHERE				ut.update_id = ud.id
                  ) AS "tags"
    FROM          social_update AS ud
    JOIN          "user" AS u ON ud.user_id = u.id
    WHERE         ud.created < _from_timestamp
    AND           u.blocked IS NULL
    ORDER BY      ud.created DESC
    LIMIT         _number_of_updates
  ) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_social_get_user_updates
(
  _user_id INT,
  _from_timestamp TIMESTAMP,
  _number_of_updates INT
)
RETURNS JSON AS
$$
  SELECT array_to_json(array_agg(result_to_return))
  FROM
  (
    SELECT        ud.id,
                  ud.created,
                  ud.user_id AS "userId",
                  ud.body,
                  u.username,
                  u.first_name AS "firstName",
                  u.last_name AS "lastName",
                  u.avatar_uploaded AS "avatarUploaded",
                  (SELECT COUNT(*) FROM social_update_like WHERE update_id = ud.id) AS "likes",
                  (SELECT COUNT(*) FROM social_update_flag WHERE update_id = ud.id) AS "flags",
                  (
                      SELECT row_to_json(a) AS "result"
                      FROM
                      (
                              SELECT      r.id,
                                          r.result,
                                          r.submitted,
                                          c.id AS "challengeId",
                                          c.name AS "challengeName",
                                          cg.id AS "challengeGroupId",
                                          cg.value AS "challengeGroupName",
                                          cc.value AS "category",
                                          cc.id AS "categoryId",
                                          ct.measurement AS "measurement",
                                          r.image_validated AS "imageValidated",
                                          r.video_validated AS "videoValidated"
                              FROM    result AS r
                              JOIN    social_update_to_result AS ur ON ur.result_id = r.id
                              JOIN    challenge AS c ON r.challenge_id = c.id
                              JOIN    challenge_group AS cg ON r.challenge_group_id = cg.id
                              JOIN    challenge_type AS ct on c.type = ct.id
                              JOIN    challenge_category AS cc ON cc.id = c.category
                              WHERE   ur.update_id = ud.id
                              AND     r.excluded IS NULL
                      ) AS a
                  ),
                  array
                  (
                    SELECT			id
                    FROM				social_update_video
                    WHERE				update_id = ud.id
                    AND         processed IS NOT NULL
                  ) AS "videos",
                  array
                  (
                    SELECT			id
                    FROM				social_update_image
                    WHERE				update_id = ud.id
                    AND         processed IS NOT NULL
                  ) AS "images",
                  array
                  (
                    SELECT      t.value
                    FROM				social_update_to_social_update_tag AS ut
                    JOIN        social_update_tag AS t ON ut.tag_id = t.id
                    WHERE				ut.update_id = ud.id
                  ) AS "tags"
    FROM          social_update AS ud
    JOIN          "user" AS u ON ud.user_id = u.id
    WHERE         ud.created < _from_timestamp
    AND           u.blocked IS NULL
    AND
    (
      ud.user_id = _user_id
      OR						ud.user_id IN
                    (
                      SELECT client_user_id FROM user_client WHERE user_id = _user_id
                      UNION
                      SELECT user_id FROM user_follower WHERE follower_user_id = _user_id
                    )
    )
    ORDER BY      ud.created DESC
    LIMIT         _number_of_updates
  ) AS result_to_return;
$$
LANGUAGE sql;

INSERT INTO social_update (user_id, body, created)
SELECT      r.user_id,
            r.id,
            r.submitted
FROM        result AS r
WHERE       r.excluded IS NULL;

INSERT INTO   social_update_to_result (update_id, result_id)
SELECT        id,
              body::bigint
FROM          social_update;

UPDATE        social_update
SET           body = NULL;

INSERT INTO social_update_like (update_id, user_id, created)
SELECT      su.id,
            f.user_id,
            f.created
FROM        social_update AS su
JOIN        social_update_to_result AS sur ON su.id = sur.update_id
JOIN        result_feedback AS f ON sur.result_id = f.result_id
WHERE       f.flag = FALSE;

INSERT INTO social_update_flag (update_id, user_id, created)
SELECT      su.id,
            f.user_id,
            f.created
FROM        social_update AS su
JOIN        social_update_to_result AS sur ON su.id = sur.update_id
JOIN        result_feedback AS f ON sur.result_id = f.result_id
WHERE       f.flag = TRUE;

DROP FUNCTION sp_gymfit_shared_result_notification(INT, BIGINT);

CREATE OR REPLACE FUNCTION sp_gymfit_shared_result_notification
(
  _user_id INT,
  _result_id BIGINT
)
RETURNS VOID AS
$$
  DECLARE
    _social_update_id UUID;
    
  BEGIN
    INSERT INTO social_update (user_id)
    VALUES      (_user_id)
    RETURNING   id INTO _social_update_id;
    
    INSERT INTO social_update_to_result (update_id, result_id)
    VALUES      (_social_update_id, _result_id);
    
    INSERT INTO social_notification (user_id, notification)
    VALUES (
      _user_id,
      (
        SELECT row_to_json(social_notification)
        FROM
        (
          SELECT					'result'::TEXT AS "type",
                          r.id AS "resultId",
                          r.result,
                          u.id AS "userId",
                          u.username,
                          u.first_name AS "firstName",
                          u.last_name AS "lastName",
                          u.avatar_uploaded AS "avatarUploaded",
                          c.id AS "challengeId",
                          c.name AS "challengeName",
                          cg.id AS "challengeGroupId",
                          cg.value AS "challengeGroupName",
                          cc.value AS "challengeCategory",
                          cc.id AS "challengeCategoryId",
                          ct.measurement AS "measurement"
          FROM						result AS r
          JOIN						"user" AS u ON u.id = r.user_id
          JOIN						challenge AS c on c.id = r.challenge_id
          JOIN 						challenge_group AS cg ON r.challenge_group_id = cg.id
          JOIN 						challenge_type AS ct on c.type = ct.id
          JOIN 						challenge_category AS cc ON cc.id = c.category
          WHERE						r.id = _result_id
        ) AS social_notification
      )
    );
  END;
$$
LANGUAGE plpgsql;