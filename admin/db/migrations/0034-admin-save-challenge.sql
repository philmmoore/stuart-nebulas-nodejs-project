CREATE FUNCTION sp_admin_save_challenge
(
	_challengeId INT,
	_name TEXT,
	_category INT,
	_type INT,
	_body_copy TEXT,
	_submission_message TEXT
)
RETURNS JSON AS
$$
	BEGIN
		UPDATE	challenge
		SET			name = _name,
						category = _category,
						type = _type,
						body_copy = _body_copy,
						submission_message = _submission_message
		WHERE		id = _challengeId;

		IF NOT FOUND THEN
			INSERT INTO page (name, category, type, body_copy, submission_message)
			VALUES			(_name, _category, _type, _body_copy, _submission_message)
			RETURNING		id
			INTO				_challengeId;
		END IF;

		RETURN to_json(_challengeId);
	END;
$$
LANGUAGE plpgsql;
