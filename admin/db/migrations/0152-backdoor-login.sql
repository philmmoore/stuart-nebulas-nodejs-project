CREATE OR REPLACE FUNCTION sp_gymfit_site_user_backdoor_login
(
	_username_or_email TEXT,
	_ip_address TEXT
)
RETURNS JSON AS
$$
	DECLARE
  _user_id INT;

	BEGIN
		SELECT			id
		INTO				_user_id
		FROM				"user"
		WHERE				(LOWER(username) = LOWER(_username_or_email) OR LOWER(email) = LOWER(_username_or_email));

		IF _user_id IS NOT NULL THEN
			INSERT INTO	user_audit (user_id, activity, message)
			VALUES			(_user_id,
									'backdoor login',
									_ip_address);
		END IF;

		RETURN sp_gymfit_site_user_get(_user_id);
	END
$$
LANGUAGE plpgsql;