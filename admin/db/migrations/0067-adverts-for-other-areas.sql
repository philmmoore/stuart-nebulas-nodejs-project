CREATE TABLE advert_country (
  advert_id INTEGER REFERENCES advert (id) NOT NULL,
	country_id INTEGER REFERENCES country (id) NOT NULL,
	PRIMARY KEY (advert_id, country_id)
);

CREATE TABLE advert_region (
  advert_id INTEGER REFERENCES advert (id) NOT NULL,
	region_id INTEGER REFERENCES region (id) NOT NULL,
	PRIMARY KEY (advert_id, region_id)
);

CREATE TABLE advert_county (
  advert_id INTEGER REFERENCES advert (id) NOT NULL,
	county_id INTEGER REFERENCES county (id) NOT NULL,
	PRIMARY KEY (advert_id, county_id)
);

CREATE TABLE advert_district (
  advert_id INTEGER REFERENCES advert (id) NOT NULL,
	district_id INTEGER REFERENCES district (id) NOT NULL,
	PRIMARY KEY (advert_id, district_id)
);
