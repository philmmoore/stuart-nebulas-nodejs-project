CREATE OR REPLACE FUNCTION sp_gymfit_site_user_get_details
(
	_username_or_email TEXT,
	_hash BYTEA
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM (
		SELECT	id,
						username
		FROM		"user"
		WHERE		hash = _hash
		AND			(LOWER(username) = LOWER(_username_or_email) OR LOWER(email) = LOWER(_username_or_email))
		LIMIT		1
	) AS result;
$$
LANGUAGE sql;
