CREATE OR REPLACE FUNCTION sp_gymfit_site_leaderboard_user_position
(
	_challenge_id INT,
	_challenge_group_id INT,
	_district_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_last_period_start TIMESTAMP,
	_last_period_end TIMESTAMP,
	_is_male BIT,
	_age_min SMALLINT,
	_age_max SMALLINT,
	_weight_min INT,
	_weight_max INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		SELECT array(
			SELECT			u.id
			from				result AS r
			INNER JOIN	"user" AS u
			ON					r.user_id = u.id
			WHERE				r.submitted >= _this_period_start
			AND					r.submitted <= _this_period_end
			AND					r.challenge_id = _challenge_id
			AND					r.challenge_group_id = _challenge_group_id
			AND					r.district_id = _district_id
			AND					(r.age >= _age_min OR _age_min IS NULL)
			AND					(r.age <= _age_max OR _age_max IS NULL)
			AND					(r.weight >= _weight_min OR _weight_min IS NULL)
			AND					(r.weight <= _weight_max OR _weight_max IS NULL)
			AND					(u.is_male = _is_male OR _is_male IS NULL)
			GROUP BY		u.id
			ORDER BY		row_number() over (ORDER BY MAX(r.result) DESC)
		) AS rankings
	) AS result_to_return
$$
LANGUAGE sql;
