CREATE OR REPLACE FUNCTION sp_gymfit_admin_challenge_get_challenges()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result))
	FROM
		(
			SELECT		      c.id,
      								c.name,
      								c.category AS categoryId,
                      cg.value AS category,
      								c.type AS typeId,
                      ct.value AS type,
      								c.body_copy AS "bodyCopy",
      								c.submission_message AS "submissionMessage",
      								(
      									array
      									(
      										SELECT			cg.value
      										FROM				challenge_group AS cg
      										INNER JOIN	challenge_to_challenge_group AS ccg
      										ON 					ccg.challenge_id = c.id
      										AND					ccg.challenge_group_id = cg.id
      										ORDER BY		cg.id
      									)
      								) AS groups,
											(
												SELECT COUNT(*) FROM result WHERE challenge_id = c.id
											) AS "totalResults"
      FROM 			      challenge AS c
      LEFT OUTER JOIN challenge_category AS cg on cg.id = c.category
      LEFT OUTER JOIN challenge_type AS ct on ct.id = c.type
			ORDER BY	      c.name
		) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_challenge_get_challenge
(
	_challenge_id INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
		(
			SELECT	id,
							name,
							category AS "categoryId",
							type AS "typeId",
							body_copy AS "bodyCopy",
							submission_message AS "submissionMessage",
							(
								array
								(
									SELECT			cg.value
									FROM				challenge_group AS cg
									INNER JOIN	challenge_to_challenge_group AS ccg
									ON 					ccg.challenge_id = c.id
									AND					ccg.challenge_group_id = cg.id
									ORDER BY		cg.id
								)
							) AS groups,
							(
								SELECT COUNT(*) FROM result WHERE challenge_id = c.id
							) AS "totalResults"
      FROM 		challenge AS c
      WHERE 	c.id = _challenge_id
			LIMIT		1
		) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_admin_challenge_delete
(
	_challenge_id INT
)
RETURNS JSON AS
$$
	BEGIN
		DELETE FROM result_feedback WHERE result_id IN (SELECT id FROM result where challenge_id = _challenge_id);
		DELETE FROM result WHERE challenge_id = _challenge_id;
		DELETE FROM challenge_to_challenge_group WHERE challenge_id = _challenge_id;
		DELETE FROM challenge WHERE id = _challenge_id;

		RETURN to_json(1);
	END
$$
LANGUAGE plpgsql;