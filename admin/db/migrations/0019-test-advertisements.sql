UPDATE advert SET small_horizontal_image_extension='png', large_vertical_image_extension='png';

INSERT INTO advert (country, name, link, small_horizontal_image_extension, large_vertical_image_extension)
VALUES
  (1, 'Test Advert 2', 'http://bbc.co.uk', 'png', 'png');
