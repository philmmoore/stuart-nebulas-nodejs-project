TRUNCATE TABLE result RESTART IDENTITY CASCADE;

ALTER TABLE result ADD age SMALLINT NOT NULL;
ALTER TABLE result ADD weight INT NOT NULL;

DROP FUNCTION IF EXISTS sp_gymfit_site_leaderboard_search(INT, INT, INT, TIMESTAMP, TIMESTAMP, TIMESTAMP, TIMESTAMP, INT, INT);

CREATE OR REPLACE FUNCTION sp_gymfit_site_leaderboard_search
(
	_challenge_id INT,
	_challenge_group_id INT,
	_district_id INT,
	_this_period_start TIMESTAMP,
	_this_period_end TIMESTAMP,
	_last_period_start TIMESTAMP,
	_last_period_end TIMESTAMP,
	_results_per_page INT,
	_page INT,
	_is_male BIT,
	_age_min SMALLINT,
	_age_max SMALLINT,
	_weight_min INT,
	_weight_max INT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result_to_return)
	FROM
	(
		SELECT
		(
			SELECT array_to_json(array_agg(results))
			FROM
			(
				SELECT			row_number() over (ORDER BY MAX(r.result) DESC) AS "rank",
										u.id AS "userId",
										u.username,
                    u.first_name "firstName",
                    u.last_name "lastName",
										(SELECT MAX(r.result)) AS "result"
				from				result AS r
				INNER JOIN	"user" AS u
				ON					r.user_id = u.id
				WHERE				r.submitted >= _this_period_start
				AND					r.submitted <= _this_period_end
				AND					r.challenge_id = _challenge_id
				AND					r.challenge_group_id = _challenge_group_id
				AND					r.district_id = _district_id
				AND					(r.age >= _age_min OR _age_min IS NULL)
				AND					(r.age <= _age_max OR _age_max IS NULL)
				AND					(r.weight >= _weight_min OR _weight_min IS NULL)
				AND					(r.weight <= _weight_max OR _weight_max IS NULL)
				AND					(u.is_male = _is_male OR _is_male IS NULL)
				GROUP BY		u.id
				ORDER BY		rank
				OFFSET			(_page - 1) * _results_per_page
				LIMIT				_results_per_page
			) AS results
		) AS "results",
		(
			SELECT				ceiling(COUNT(DISTINCT r.user_id)::DECIMAL / _results_per_page)
			FROM					result AS r
			INNER JOIN		"user" AS u
			ON						r.user_id = u.id
			WHERE					r.submitted >= _this_period_start
			AND						r.submitted <= _this_period_end
			AND						r.challenge_id = _challenge_id
			AND						r.challenge_group_id = _challenge_group_id
			AND						r.district_id = _district_id
			AND						(r.age >= _age_min OR _age_min IS NULL)
			AND						(r.age <= _age_max OR _age_max IS NULL)
			AND						(r.weight >= _weight_min OR _weight_min IS NULL)
			AND						(r.weight <= _weight_max OR _weight_max IS NULL)
			AND						(u.is_male = _is_male OR _is_male IS NULL)
		) AS "numberOfPages",
		(
			SELECT				ct.measurement
			FROM					challenge_type AS ct
			LEFT OUTER JOIN challenge AS c ON c.type = ct.id
			WHERE					c.id = _challenge_id
		) AS "measurement"
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_weight_filter_get_range
(
	_weight_filter_name TEXT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
		(
			SELECT		min,
								max
			FROM			weight_group
			WHERE			LOWER(REPLACE(name, ' ', '')) = _weight_filter_name
		) AS result;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_site_age_filter_get_range
(
	_age_filter_name TEXT
)
RETURNS JSON AS
$$
	SELECT row_to_json(result)
	FROM
		(
			SELECT		min,
								max
			FROM			age_group
			WHERE			LOWER(REPLACE(name, ' ', '')) = _age_filter_name
		) AS result;
$$
LANGUAGE sql;
