CREATE OR REPLACE FUNCTION sp_gymfit_api_location_get_all_names()
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM
		(
			SELECT		name,
								id 						AS "districtId",
								null::int 		AS "countyId",
								null::int 		AS "regionId",
								null::int 		AS "countryId",
								null::char(2) AS "countryCode"
			FROM			district
			UNION
			SELECT		name,
								null::int 		AS "districtId",
								id 						AS "countyId",
								null::int 		AS "regionId",
								null::int 		AS "countryId",
								null::char(2) AS "countryCode"
			FROM			county
			UNION
			SELECT		name,
								null::int 		AS "districtId",
								null::int 		AS "countyId",
								id 						AS "regionId",
								null::int 		AS "countryId",
								null::char(2)	AS "countryCode"
			FROM			region
			UNION
			SELECT		name,
								null::int 		AS "districtId",
								null::int		 	AS "countyId",
								null::int 		AS "regionId",
								id 						AS "countryId",
								code 					AS "countryCode"
			FROM			country
		) AS result_to_return;
$$
LANGUAGE sql;
