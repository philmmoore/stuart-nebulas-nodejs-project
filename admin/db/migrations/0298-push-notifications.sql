DROP TABLE IF EXISTS user_devices_for_push_notification;
CREATE TABLE IF NOT EXISTS user_devices_for_push_notification (
  id SERIAL PRIMARY KEY,  
  user_id INTEGER REFERENCES "user" (id) NOT NULL,
  device_id TEXT NOT NULL,
  device_token TEXT NOT NULL,
  endpoint TEXT NOT NULL,
  created_at TIMESTAMP NOT NULL default current_timestamp
);

CREATE OR REPLACE FUNCTION sp_gymfit_api_user_register_device_for_push_notification(_user_id integer, _device_id text, _device_token text, _endpoint text)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
  BEGIN

    -- Deletes the current device from all associated users, user might be logging out and into another account on the same device
    -- and we need to prevent duplicate notifications to users
    DELETE FROM user_devices_for_push_notification WHERE device_id = _device_id;

    -- Insert the device, token and endpoint for the current user
    INSERT INTO user_devices_for_push_notification (
      user_id, 
      device_id, 
      device_token, 
      endpoint
    ) VALUES (
      _user_id, 
      _device_id, 
      _device_token, 
      _endpoint
    );

    -- Return a list of all devices
    RETURN to_json(1);

  END;
$function$;


DROP FUNCTION IF EXISTS sp_gymfit_api_user_get_devices_for_push_notification();
CREATE OR REPLACE FUNCTION sp_gymfit_api_user_get_devices_for_push_notification(_user_id integer)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
  BEGIN

    RETURN array_to_json(array_agg(result))
    FROM (
      SELECT 
        d.device_id AS "deviceId",
        d.device_token AS "deviceToken",
        d.endpoint AS "endpoint"
      FROM 
        user_devices_for_push_notification d
      WHERE 
        d.user_id = _user_id
    ) as result;

  END;
$function$;

CREATE OR REPLACE FUNCTION sp_gymfit_task_runner_user_get_devices_for_push_notification(_user_id integer)
 RETURNS json
 LANGUAGE plpgsql
AS $function$
  BEGIN

    RETURN sp_gymfit_api_user_get_devices_for_push_notification(_user_id);

  END;
$function$

