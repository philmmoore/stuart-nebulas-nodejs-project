CREATE TABLE user_audit (
  timestamp TIMESTAMP PRIMARY KEY default current_timestamp,
  activity TEXT NOT NULL,
  message TEXT
);
