CREATE FUNCTION sp_gymfit_api_register_is_email_registered
(
	_email TEXT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM (
		SELECT		TRUE as result
		FROM			"user"
		WHERE			email = LOWER(_email)
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE FUNCTION sp_gymfit_api_register_is_username_registered
(
	_username TEXT
)
RETURNS JSON AS
$$
	SELECT array_to_json(array_agg(result_to_return))
	FROM (
		SELECT		TRUE as result
		FROM			"user"
		WHERE			username = LOWER(_username)
	) AS result_to_return;
$$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION sp_gymfit_api_register_user
(
	_username TEXT,
	_hash BYTEA,
	_first_name TEXT,
	_last_name TEXT,
	_date_of_birth DATE,
	_email TEXT
)
RETURNS JSON AS
$$
	DECLARE
		_user_id INT;
	BEGIN
		INSERT INTO	"user" (username, hash, first_name, last_name, date_of_birth, email)
		VALUES (
								_username,
								_hash,
								_first_name,
								_last_name,
								_date_of_birth,
								_email
		);

		SELECT			id
		FROM				"user"
		WHERE				username = _username
		INTO				_user_id;

		INSERT INTO	user_email_verification (user_id)
		VALUES			(_user_id);

		INSERT INTO	user_notification (user_id, notification_id)
		VALUES			(_user_id, 1),
								(_user_id, 2),
								(_user_id, 3),
								(_user_id, 4);

		RETURN row_to_json(result)
		FROM (
			SELECT					id
			FROM						"user" AS u
			WHERE						u.id = _user_id
		) AS result;
	END;
$$
LANGUAGE plpgsql;
