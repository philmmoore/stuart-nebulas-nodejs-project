var pg = require('pg');

var PostgresDB = function PostgresDB(connectionString, logger) {
  this.connectionString = connectionString;
  this.logger = logger;
};

PostgresDB.prototype.connect = function connect(success) {
  var logger = this.logger;
  logger.log('Connecting to Postgres...');

  pg.connect(this.connectionString, function(err, client, done) {
    if (err) {
      throw err;
    }

    logger.log('Connected to Postgres.');

    success(client);
  });
};

module.exports = PostgresDB;