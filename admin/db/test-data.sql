TRUNCATE TABLE region RESTART IDENTITY CASCADE;

INSERT INTO region VALUES (2, 1, 'London');
INSERT INTO region VALUES (4, 1, 'East of England');
INSERT INTO region VALUES (5, 1, 'West Midlands');
INSERT INTO region VALUES (7, 1, 'Yorkshire and the Humber');
INSERT INTO region VALUES (8, 1, 'East Midlands');
INSERT INTO region VALUES (9, 1, 'North East England');
INSERT INTO region VALUES (3, 1, 'North West England');
INSERT INTO region VALUES (1, 1, 'South East England');
INSERT INTO region VALUES (6, 1, 'South West England');
INSERT INTO region VALUES (10, 1, 'North Wales');
INSERT INTO region VALUES (11, 1, 'Mid Wales');
INSERT INTO region VALUES (12, 1, 'South Wales');
INSERT INTO region VALUES (13, 1, 'Highlands and Islands');
INSERT INTO region VALUES (14, 1, 'North Eastern Scotland');
INSERT INTO region VALUES (15, 1, 'East Scotland');
INSERT INTO region VALUES (16, 1, 'South West Scotland');
INSERT INTO region VALUES (17, 1, 'Northern Ireland');
INSERT INTO region VALUES (18, 1, 'Belfast');
INSERT INTO region VALUES (19, 1, 'South East Northern Ireland');
INSERT INTO region VALUES (20, 1, 'South Northern Ireland');
INSERT INTO region VALUES (21, 1, 'West Northern Ireland');

INSERT INTO county VALUES (2, 1, 'Buckinghamshire');
INSERT INTO county VALUES (3, 1, 'Milton Keynes');
INSERT INTO county VALUES (4, 1, 'East Sussex');
INSERT INTO county VALUES (6, 1, 'Hampshire');
INSERT INTO county VALUES (7, 1, 'Southampton');
INSERT INTO county VALUES (8, 1, 'Portsmouth');
INSERT INTO county VALUES (9, 1, 'Isle of Wight');
INSERT INTO county VALUES (10, 1, 'Kent');
INSERT INTO county VALUES (11, 1, 'Medway');
INSERT INTO county VALUES (12, 1, 'Oxfordshire');
INSERT INTO county VALUES (13, 1, 'Surrey');
INSERT INTO county VALUES (14, 1, 'West Sussex');
INSERT INTO county VALUES (52, 3, 'Cumbria');
INSERT INTO county VALUES (53, 3, 'Greater Manchester');
INSERT INTO county VALUES (54, 3, 'Lancashire');
INSERT INTO county VALUES (57, 3, 'Merseyside');
INSERT INTO county VALUES (58, 4, 'Thurrock');
INSERT INTO county VALUES (59, 4, 'Southend-on-Sea');
INSERT INTO county VALUES (60, 4, 'Essex');
INSERT INTO county VALUES (61, 4, 'Hertfordshire');
INSERT INTO county VALUES (62, 4, 'Luton');
INSERT INTO county VALUES (63, 4, 'Bedford');
INSERT INTO county VALUES (64, 4, 'Central Bedfordshire');
INSERT INTO county VALUES (65, 4, 'Cambridgeshire');
INSERT INTO county VALUES (66, 4, 'Peterborough');
INSERT INTO county VALUES (67, 4, 'Norfolk');
INSERT INTO county VALUES (68, 4, 'Suffolk');
INSERT INTO county VALUES (69, 5, 'Herefordshire');
INSERT INTO county VALUES (70, 5, 'Shropshire');
INSERT INTO county VALUES (71, 5, 'Telford and Wrekin');
INSERT INTO county VALUES (72, 5, 'Staffordshire');
INSERT INTO county VALUES (73, 5, 'Stoke-on-Trent');
INSERT INTO county VALUES (74, 5, 'Warwickshire');
INSERT INTO county VALUES (75, 5, 'West Midlands');
INSERT INTO county VALUES (76, 5, 'Worchestershire');
INSERT INTO county VALUES (77, 6, 'Bath and North East Somerset');
INSERT INTO county VALUES (78, 6, 'North Somerset');
INSERT INTO county VALUES (79, 6, 'Somerset');
INSERT INTO county VALUES (80, 6, 'Bristol');
INSERT INTO county VALUES (81, 6, 'South Gloucestershire');
INSERT INTO county VALUES (82, 6, 'Gloucestershire');
INSERT INTO county VALUES (83, 6, 'Swindon');
INSERT INTO county VALUES (84, 6, 'Wiltshire');
INSERT INTO county VALUES (85, 6, 'Dorset');
INSERT INTO county VALUES (86, 6, 'Poole');
INSERT INTO county VALUES (87, 6, 'Bournemouth');
INSERT INTO county VALUES (88, 6, 'Devon');
INSERT INTO county VALUES (89, 6, 'Torbay');
INSERT INTO county VALUES (90, 6, 'Plymouth');
INSERT INTO county VALUES (91, 6, 'Isles of Scilly');
INSERT INTO county VALUES (92, 6, 'Cornwall');
INSERT INTO county VALUES (93, 7, 'South Yorkshire');
INSERT INTO county VALUES (94, 7, 'West Yorkshire');
INSERT INTO county VALUES (95, 7, 'North Yorkshire');
INSERT INTO county VALUES (96, 7, 'York');
INSERT INTO county VALUES (97, 7, 'East Riding of Yorkshire');
INSERT INTO county VALUES (98, 7, 'Kingston upon Hull');
INSERT INTO county VALUES (99, 7, 'North Lincolnshire');
INSERT INTO county VALUES (100, 7, 'North East Lincolnshire');
INSERT INTO county VALUES (101, 8, 'Derbyshire');
INSERT INTO county VALUES (102, 8, 'Derby');
INSERT INTO county VALUES (103, 8, 'Nottinghamshire');
INSERT INTO county VALUES (104, 8, 'Nottingham');
INSERT INTO county VALUES (105, 8, 'Lincolnshire');
INSERT INTO county VALUES (106, 8, 'Leicestershire');
INSERT INTO county VALUES (107, 8, 'Leicester');
INSERT INTO county VALUES (108, 8, 'Rutland');
INSERT INTO county VALUES (109, 8, 'Northamptonshire');
INSERT INTO county VALUES (110, 9, 'Northumberland');
INSERT INTO county VALUES (111, 9, 'Tyne and Wear');
INSERT INTO county VALUES (112, 9, 'Durham');
INSERT INTO county VALUES (116, 9, 'North Yorkshire');
INSERT INTO county VALUES (48, 3, 'Cheshire');
INSERT INTO county VALUES (15, 2, 'Inner London');
INSERT INTO county VALUES (118, 2, 'Outer London');
INSERT INTO county VALUES (5, 1, 'Brighton and Hove');
INSERT INTO county VALUES (1, 1, 'Bracknell Forest');
INSERT INTO county VALUES (119, 1, 'Reading');
INSERT INTO county VALUES (120, 1, 'Slough');
INSERT INTO county VALUES (121, 1, 'West Berkshire');
INSERT INTO county VALUES (122, 1, 'Windsor and Maidenhead');
INSERT INTO county VALUES (123, 1, 'Wokingham');
INSERT INTO county VALUES (124, 10, 'Clwyd');
INSERT INTO county VALUES (125, 10, 'Gwynedd');
INSERT INTO county VALUES (126, 11, 'Powys');
INSERT INTO county VALUES (127, 12, 'Dyfed');
INSERT INTO county VALUES (128, 12, 'South Glamorgan');
INSERT INTO county VALUES (129, 12, 'West Glamorgan');
INSERT INTO county VALUES (130, 12, 'Mid Glamorgan');
INSERT INTO county VALUES (131, 12, 'Gwent');
INSERT INTO county VALUES (132, 13, 'Argyll and Bute');
INSERT INTO county VALUES (133, 14, 'Bearsden and Milngavie');
INSERT INTO county VALUES (134, 15, 'Clackmannan');
INSERT INTO county VALUES (156, 16, 'Clackmannanshire');
INSERT INTO county VALUES (157, 16, 'Clydesdale');
INSERT INTO county VALUES (158, 16, 'Cumbernauld and Kilsyth');
INSERT INTO county VALUES (159, 16, 'Cumnock and Doon Valley');
INSERT INTO county VALUES (160, 16, 'Cunninghame');
INSERT INTO county VALUES (161, 16, 'Dunfermline');
INSERT INTO county VALUES (162, 16, 'East Kilbride');
INSERT INTO county VALUES (163, 16, 'Eastwood and Strathclyde');
INSERT INTO county VALUES (164, 16, 'Edinburgh');
INSERT INTO county VALUES (165, 16, 'Falkirk');
INSERT INTO county VALUES (166, 16, 'Glasgow');
INSERT INTO county VALUES (167, 16, 'Areas of Glenrothes');
INSERT INTO county VALUES (168, 16, 'Gordon');
INSERT INTO county VALUES (169, 16, 'Hamilton and South Lanarkshire');
INSERT INTO county VALUES (170, 16, 'Inverclyde District');
INSERT INTO county VALUES (171, 16, 'Kilmarnock and Loudoun');
INSERT INTO county VALUES (172, 16, 'Kyle and Carrick');
INSERT INTO county VALUES (173, 16, 'Kyle and Ayrshire');
INSERT INTO county VALUES (174, 17, 'Antrim');
INSERT INTO county VALUES (175, 17, 'Ballymena');
INSERT INTO county VALUES (176, 17, 'Ballymoney');
INSERT INTO county VALUES (177, 17, 'Carrickfergus');
INSERT INTO county VALUES (178, 17, 'Coleraine');
INSERT INTO county VALUES (179, 17, 'Larne');
INSERT INTO county VALUES (180, 17, 'Magherafelt');
INSERT INTO county VALUES (181, 17, 'Moyle');
INSERT INTO county VALUES (182, 17, 'Newtonabbey');
INSERT INTO county VALUES (183, 18, 'Belfast');
INSERT INTO county VALUES (184, 19, 'Ards');
INSERT INTO county VALUES (185, 19, 'Castlereagh');
INSERT INTO county VALUES (186, 19, 'Down');
INSERT INTO county VALUES (187, 19, 'Lisburn');
INSERT INTO county VALUES (188, 20, 'Armagh');
INSERT INTO county VALUES (189, 20, 'Banbridge');
INSERT INTO county VALUES (190, 20, 'Cookstown');
INSERT INTO county VALUES (191, 20, 'Craigavon');
INSERT INTO county VALUES (192, 20, 'Dungannon and South Tyrone');
INSERT INTO county VALUES (193, 20, 'Newry and Mourne');
INSERT INTO county VALUES (194, 21, 'Derry');
INSERT INTO county VALUES (195, 21, 'Fermanagh');
INSERT INTO county VALUES (196, 21, 'Limavady');
INSERT INTO county VALUES (197, 21, 'Omagh');
INSERT INTO county VALUES (198, 21, 'Strabane');

INSERT INTO district VALUES (1, 53, 'Bolton');
INSERT INTO district VALUES (2, 53, 'Bury');
INSERT INTO district VALUES (3, 53, 'Manchester');
INSERT INTO district VALUES (4, 53, 'Oldham');
INSERT INTO district VALUES (5, 53, 'Rochdale');
INSERT INTO district VALUES (6, 53, 'Salford');
INSERT INTO district VALUES (7, 53, 'Stockport');
INSERT INTO district VALUES (8, 53, 'Tameside');
INSERT INTO district VALUES (9, 53, 'Trafford');
INSERT INTO district VALUES (10, 53, 'Wigan');
INSERT INTO district VALUES (11, 112, 'County Durham');
INSERT INTO district VALUES (12, 112, 'Darlington');
INSERT INTO district VALUES (13, 112, 'Hartlepool');
INSERT INTO district VALUES (14, 112, 'Stockton-on-Tees North');
INSERT INTO district VALUES (15, 110, 'Northumberland');
INSERT INTO district VALUES (16, 111, 'North Tyneside');
INSERT INTO district VALUES (17, 111, 'Gateshead');
INSERT INTO district VALUES (18, 111, 'Newcastle Upon Tyne');
INSERT INTO district VALUES (19, 111, 'South Tyneside');
INSERT INTO district VALUES (20, 111, 'Sunderland');
INSERT INTO district VALUES (21, 116, 'Redcar and Cleveland');
INSERT INTO district VALUES (22, 116, 'Stockton-on-Tees South');
INSERT INTO district VALUES (23, 116, 'Middlesbrough');
INSERT INTO district VALUES (24, 48, 'Cheshire East');
INSERT INTO district VALUES (25, 48, 'Cheshire West and Chester');
INSERT INTO district VALUES (26, 48, 'Halton');
INSERT INTO district VALUES (27, 48, 'Warrington');
INSERT INTO district VALUES (28, 52, 'Allerdale');
INSERT INTO district VALUES (29, 52, 'Barrow-in-Furness');
INSERT INTO district VALUES (30, 52, 'Carlisle');
INSERT INTO district VALUES (31, 52, 'Copeland');
INSERT INTO district VALUES (32, 52, 'Eden');
INSERT INTO district VALUES (33, 52, 'South Lakeland');
INSERT INTO district VALUES (34, 54, 'Burnley');
INSERT INTO district VALUES (35, 54, 'Chorley');
INSERT INTO district VALUES (36, 54, 'Fylde');
INSERT INTO district VALUES (37, 54, 'Hyndburn');
INSERT INTO district VALUES (38, 54, 'Lancaster');
INSERT INTO district VALUES (39, 54, 'Pendle');
INSERT INTO district VALUES (40, 54, 'Preston');
INSERT INTO district VALUES (41, 54, 'Ribble Valley');
INSERT INTO district VALUES (42, 54, 'Rossendale');
INSERT INTO district VALUES (43, 54, 'South Ribble');
INSERT INTO district VALUES (44, 54, 'West Lancashire');
INSERT INTO district VALUES (45, 54, 'Wyre');
INSERT INTO district VALUES (46, 57, 'Knowsley');
INSERT INTO district VALUES (47, 57, 'Liverpool');
INSERT INTO district VALUES (48, 57, 'Sefton');
INSERT INTO district VALUES (49, 57, 'St Helens');
INSERT INTO district VALUES (50, 57, 'Wirral');
INSERT INTO district VALUES (51, 97, 'East Riding of Yorkshire');
INSERT INTO district VALUES (52, 98, 'Kingston upon Hull');
INSERT INTO district VALUES (53, 100, 'North East Lincolnshire');
INSERT INTO district VALUES (54, 99, 'North Lincolnshire');
INSERT INTO district VALUES (55, 96, 'York');
INSERT INTO district VALUES (56, 95, 'Craven');
INSERT INTO district VALUES (57, 95, 'Hambleton');
INSERT INTO district VALUES (58, 95, 'Harrogate');
INSERT INTO district VALUES (59, 95, 'Richmondshire');
INSERT INTO district VALUES (60, 95, 'Ryedale');
INSERT INTO district VALUES (61, 95, 'Scarborough');
INSERT INTO district VALUES (62, 95, 'Selby');
INSERT INTO district VALUES (63, 93, 'Barnsley');
INSERT INTO district VALUES (64, 93, 'Doncaster');
INSERT INTO district VALUES (65, 93, 'Rotherham');
INSERT INTO district VALUES (66, 93, 'Sheffield');
INSERT INTO district VALUES (67, 94, 'Bradford');
INSERT INTO district VALUES (68, 94, 'Calderdale');
INSERT INTO district VALUES (69, 94, 'Kirklees');
INSERT INTO district VALUES (70, 94, 'Leeds');
INSERT INTO district VALUES (71, 94, 'Wakefield');
INSERT INTO district VALUES (72, 102, 'Derby');
INSERT INTO district VALUES (73, 107, 'Leicester');
INSERT INTO district VALUES (74, 104, 'Nottingham');
INSERT INTO district VALUES (75, 108, 'Rutland');
INSERT INTO district VALUES (76, 101, 'Amber Valley');
INSERT INTO district VALUES (77, 101, 'Bolsover');
INSERT INTO district VALUES (78, 101, 'Chesterfield');
INSERT INTO district VALUES (79, 101, 'Derbyshire Dales');
INSERT INTO district VALUES (80, 101, 'Erewash');
INSERT INTO district VALUES (81, 101, 'High Peak');
INSERT INTO district VALUES (82, 101, 'North East Derbyshire');
INSERT INTO district VALUES (83, 101, 'South Derbyshire');
INSERT INTO district VALUES (84, 106, 'Blaby');
INSERT INTO district VALUES (85, 106, 'Charnwood');
INSERT INTO district VALUES (86, 106, 'Harborough');
INSERT INTO district VALUES (87, 106, 'Hinckley and Bosworth');
INSERT INTO district VALUES (88, 106, 'Melton');
INSERT INTO district VALUES (89, 106, 'North West Leicestershire');
INSERT INTO district VALUES (90, 106, 'Oadby and Wigston');
INSERT INTO district VALUES (91, 105, 'Boston');
INSERT INTO district VALUES (92, 105, 'East Lindsey');
INSERT INTO district VALUES (93, 105, 'Lincoln');
INSERT INTO district VALUES (94, 105, 'North Kesteven');
INSERT INTO district VALUES (95, 105, 'South Holland');
INSERT INTO district VALUES (96, 105, 'South Kesteven');
INSERT INTO district VALUES (97, 105, 'West Lindsey');
INSERT INTO district VALUES (98, 109, 'Corby');
INSERT INTO district VALUES (101, 109, 'Kettering');
INSERT INTO district VALUES (102, 109, 'Northampton');
INSERT INTO district VALUES (103, 109, 'South Northamptonshire');
INSERT INTO district VALUES (104, 109, 'Wellingborough');
INSERT INTO district VALUES (99, 109, 'Daventry');
INSERT INTO district VALUES (100, 109, 'East Northamptonshire');
INSERT INTO district VALUES (105, 103, 'Ashfield');
INSERT INTO district VALUES (106, 103, 'Bassetlaw');
INSERT INTO district VALUES (107, 103, 'Broxtowe');
INSERT INTO district VALUES (108, 103, 'Gedling');
INSERT INTO district VALUES (109, 103, 'Mansfield');
INSERT INTO district VALUES (110, 103, 'Newark and Sherwood');
INSERT INTO district VALUES (111, 103, 'Rushcliffe');
INSERT INTO district VALUES (112, 69, 'Herefordshire');
INSERT INTO district VALUES (113, 70, 'Shropshire');
INSERT INTO district VALUES (114, 73, 'Stoke-on-Trent');
INSERT INTO district VALUES (115, 71, 'Telford and Wrekin');
INSERT INTO district VALUES (116, 72, 'Cannock Chase');
INSERT INTO district VALUES (117, 72, 'East Staffordshire');
INSERT INTO district VALUES (118, 72, 'Lichfield');
INSERT INTO district VALUES (119, 72, 'Newcastle-Under-Lyme');
INSERT INTO district VALUES (120, 72, 'South Staffordshire');
INSERT INTO district VALUES (121, 72, 'Stafford');
INSERT INTO district VALUES (122, 72, 'Staffordshire Moorlands');
INSERT INTO district VALUES (123, 72, 'Tamworth');
INSERT INTO district VALUES (124, 74, 'North Warwickshire');
INSERT INTO district VALUES (125, 74, 'Nuneaton and Bedworth');
INSERT INTO district VALUES (126, 74, 'Rugby');
INSERT INTO district VALUES (127, 74, 'Stratford-on-Avon');
INSERT INTO district VALUES (128, 74, 'Warwick');
INSERT INTO district VALUES (130, 76, 'Bromsgrove');
INSERT INTO district VALUES (131, 76, 'Malvern Hills');
INSERT INTO district VALUES (132, 76, 'Redditch');
INSERT INTO district VALUES (133, 76, 'Worcester');
INSERT INTO district VALUES (134, 76, 'Wychavon');
INSERT INTO district VALUES (135, 76, 'Wyre Forest');
INSERT INTO district VALUES (129, 75, 'Birmingham');
INSERT INTO district VALUES (136, 75, 'Coventry');
INSERT INTO district VALUES (137, 75, 'Dudley');
INSERT INTO district VALUES (138, 75, 'Sandwell');
INSERT INTO district VALUES (139, 75, 'Solihull');
INSERT INTO district VALUES (140, 75, 'Walsall');
INSERT INTO district VALUES (141, 75, 'Wolverhampton');
INSERT INTO district VALUES (142, 63, 'Bedford');
INSERT INTO district VALUES (143, 64, 'Central Bedfordshire');
INSERT INTO district VALUES (144, 62, 'Luton');
INSERT INTO district VALUES (145, 66, 'Peterborough');
INSERT INTO district VALUES (146, 59, 'Southend-on-Sea');
INSERT INTO district VALUES (147, 58, 'Thurrock');
INSERT INTO district VALUES (148, 65, 'Cambridge');
INSERT INTO district VALUES (149, 65, 'East Cambridgeshire');
INSERT INTO district VALUES (150, 65, 'Fenland');
INSERT INTO district VALUES (151, 65, 'Huntingdonshire');
INSERT INTO district VALUES (152, 65, 'South Cambridgeshire');
INSERT INTO district VALUES (153, 60, 'Basildon');
INSERT INTO district VALUES (154, 60, 'Braintree');
INSERT INTO district VALUES (155, 60, 'Brentwood');
INSERT INTO district VALUES (156, 60, 'Castle Point');
INSERT INTO district VALUES (157, 60, 'Chelmsford');
INSERT INTO district VALUES (158, 60, 'Colchester');
INSERT INTO district VALUES (159, 60, 'Epping Forest');
INSERT INTO district VALUES (160, 60, 'Harlow');
INSERT INTO district VALUES (161, 60, 'Maldon');
INSERT INTO district VALUES (162, 60, 'Rochford');
INSERT INTO district VALUES (163, 60, 'Tendring');
INSERT INTO district VALUES (164, 60, 'Uttlesford');
INSERT INTO district VALUES (165, 61, 'Broxbourne');
INSERT INTO district VALUES (166, 61, 'Dacorum');
INSERT INTO district VALUES (167, 61, 'East Hertfordshire');
INSERT INTO district VALUES (168, 61, 'Hertsmere');
INSERT INTO district VALUES (169, 61, 'North Hertfordshire');
INSERT INTO district VALUES (170, 61, 'St Albans');
INSERT INTO district VALUES (171, 61, 'Stevenage');
INSERT INTO district VALUES (172, 61, 'Three Rivers');
INSERT INTO district VALUES (173, 61, 'Watford');
INSERT INTO district VALUES (174, 61, 'Welwyn Hatfield');
INSERT INTO district VALUES (175, 67, 'Breckland');
INSERT INTO district VALUES (176, 67, 'Broadland');
INSERT INTO district VALUES (177, 67, 'Great Yarmouth');
INSERT INTO district VALUES (178, 67, 'Kings Lynn and West Norfolk');
INSERT INTO district VALUES (179, 67, 'North Norfolk');
INSERT INTO district VALUES (180, 67, 'Norwich');
INSERT INTO district VALUES (181, 67, 'South Norfolk');
INSERT INTO district VALUES (182, 68, 'Babergh');
INSERT INTO district VALUES (183, 68, 'Forest Heath');
INSERT INTO district VALUES (184, 68, 'Ipswich');
INSERT INTO district VALUES (185, 68, 'Mid Suffolk');
INSERT INTO district VALUES (186, 68, 'St Edmundsbury');
INSERT INTO district VALUES (187, 68, 'Suffolk Coastal');
INSERT INTO district VALUES (188, 68, 'Waveney');
INSERT INTO district VALUES (189, 15, 'Camden');
INSERT INTO district VALUES (190, 15, 'City of London');
INSERT INTO district VALUES (191, 15, 'Hackney');
INSERT INTO district VALUES (192, 15, 'Hammersmith and Fulham');
INSERT INTO district VALUES (193, 15, 'Haringey');
INSERT INTO district VALUES (194, 15, 'Islington');
INSERT INTO district VALUES (195, 15, 'Kensington and Chelsea');
INSERT INTO district VALUES (196, 15, 'Lambeth');
INSERT INTO district VALUES (197, 15, 'Lewisham');
INSERT INTO district VALUES (198, 15, 'Newham');
INSERT INTO district VALUES (199, 15, 'Southwark');
INSERT INTO district VALUES (200, 15, 'Tower Hamlets');
INSERT INTO district VALUES (201, 15, 'Wandsworth');
INSERT INTO district VALUES (202, 15, 'Westminster');
INSERT INTO district VALUES (203, 118, 'Barking and Dagenham');
INSERT INTO district VALUES (204, 118, 'Barnet');
INSERT INTO district VALUES (205, 118, 'Bexley');
INSERT INTO district VALUES (206, 118, 'Brent');
INSERT INTO district VALUES (207, 118, 'Bromley');
INSERT INTO district VALUES (208, 118, 'Croydon');
INSERT INTO district VALUES (209, 118, 'Ealing');
INSERT INTO district VALUES (210, 118, 'Enfield');
INSERT INTO district VALUES (211, 118, 'Greenwich');
INSERT INTO district VALUES (212, 118, 'Harrow');
INSERT INTO district VALUES (213, 118, 'Havering');
INSERT INTO district VALUES (214, 118, 'Hillingdon');
INSERT INTO district VALUES (215, 118, 'Hounslow');
INSERT INTO district VALUES (216, 118, 'Kingston upon Thames');
INSERT INTO district VALUES (217, 118, 'Merton');
INSERT INTO district VALUES (218, 118, 'Redbridge');
INSERT INTO district VALUES (219, 118, 'Richmond upon Thames');
INSERT INTO district VALUES (220, 118, 'Sutton');
INSERT INTO district VALUES (221, 118, 'Waltham Forest');
INSERT INTO district VALUES (222, 3, 'Milton Keynes');
INSERT INTO district VALUES (223, 8, 'Portsmouth');
INSERT INTO district VALUES (224, 2, 'Aylesbury Vale');
INSERT INTO district VALUES (225, 2, 'Chiltern');
INSERT INTO district VALUES (226, 2, 'South Bucks');
INSERT INTO district VALUES (227, 2, 'Wycombe');
INSERT INTO district VALUES (228, 4, 'Eastbourne');
INSERT INTO district VALUES (229, 4, 'Hastings');
INSERT INTO district VALUES (230, 4, 'Lewes');
INSERT INTO district VALUES (231, 4, 'Rother');
INSERT INTO district VALUES (232, 4, 'Wealden');
INSERT INTO district VALUES (233, 6, 'Basingstoke and Deane');
INSERT INTO district VALUES (234, 6, 'East Hampshire');
INSERT INTO district VALUES (235, 6, 'Eastleigh');
INSERT INTO district VALUES (236, 6, 'Fareham');
INSERT INTO district VALUES (237, 6, 'Gosport');
INSERT INTO district VALUES (238, 6, 'Hart');
INSERT INTO district VALUES (331, 1, 'Bracknell Forest');
INSERT INTO district VALUES (332, 5, 'Brighton and Hove');
INSERT INTO district VALUES (333, 9, 'Isle of Wight');
INSERT INTO district VALUES (334, 11, 'Medway');
INSERT INTO district VALUES (336, 119, 'Reading');
INSERT INTO district VALUES (337, 120, 'Slough');
INSERT INTO district VALUES (338, 7, 'Southampton');
INSERT INTO district VALUES (339, 121, 'West Berkshire');
INSERT INTO district VALUES (340, 122, 'Windsor and Maidenhead');
INSERT INTO district VALUES (341, 123, 'Wokingham');
INSERT INTO district VALUES (342, 6, 'Havant');
INSERT INTO district VALUES (343, 6, 'New Forest');
INSERT INTO district VALUES (344, 6, 'Rushmoor');
INSERT INTO district VALUES (345, 6, 'Test Valley');
INSERT INTO district VALUES (346, 6, 'Winchester');
INSERT INTO district VALUES (347, 10, 'Ashford');
INSERT INTO district VALUES (348, 10, 'Canterbury');
INSERT INTO district VALUES (349, 10, 'Dartford');
INSERT INTO district VALUES (351, 10, 'Dover');
INSERT INTO district VALUES (352, 10, 'Gravesham');
INSERT INTO district VALUES (353, 10, 'Maidstone');
INSERT INTO district VALUES (354, 10, 'Sevenoaks');
INSERT INTO district VALUES (355, 10, 'Shepway');
INSERT INTO district VALUES (356, 10, 'Swale');
INSERT INTO district VALUES (357, 10, 'Thanet');
INSERT INTO district VALUES (358, 10, 'Tonbridge and Malling');
INSERT INTO district VALUES (359, 10, 'Tunbridge Wells');
INSERT INTO district VALUES (360, 12, 'Cherwell');
INSERT INTO district VALUES (361, 12, 'Oxford');
INSERT INTO district VALUES (362, 12, 'South Oxfordshire');
INSERT INTO district VALUES (363, 12, 'Vale of White Horse');
INSERT INTO district VALUES (364, 12, 'West Oxfordshire');
INSERT INTO district VALUES (365, 13, 'Elmbridge');
INSERT INTO district VALUES (366, 13, 'Epsom and Ewell');
INSERT INTO district VALUES (367, 13, 'Guildford');
INSERT INTO district VALUES (368, 13, 'Mole Valley');
INSERT INTO district VALUES (369, 13, 'Reigate and Banstead');
INSERT INTO district VALUES (370, 13, 'Runnymede');
INSERT INTO district VALUES (371, 13, 'Spelthorne');
INSERT INTO district VALUES (372, 13, 'Surrey Heath');
INSERT INTO district VALUES (373, 13, 'Tandridge');
INSERT INTO district VALUES (374, 13, 'Waverley');
INSERT INTO district VALUES (375, 13, 'Woking');
INSERT INTO district VALUES (376, 14, 'Adur');
INSERT INTO district VALUES (377, 14, 'Arun');
INSERT INTO district VALUES (378, 14, 'Chichester');
INSERT INTO district VALUES (379, 14, 'Crawley');
INSERT INTO district VALUES (380, 14, 'Horsham');
INSERT INTO district VALUES (381, 14, 'Mid Sussex');
INSERT INTO district VALUES (382, 14, 'Worthing');
INSERT INTO district VALUES (383, 77, 'Bath and North East Somerset');
INSERT INTO district VALUES (384, 87, 'Bournemouth');
INSERT INTO district VALUES (385, 80, 'Bristol');
INSERT INTO district VALUES (386, 92, 'Cornwall');
INSERT INTO district VALUES (387, 91, 'Isles of Scilly');
INSERT INTO district VALUES (388, 78, 'North Somerset');
INSERT INTO district VALUES (389, 90, 'Plymouth');
INSERT INTO district VALUES (390, 86, 'Poole');
INSERT INTO district VALUES (391, 81, 'South Gloucestershire');
INSERT INTO district VALUES (392, 83, 'Swindon');
INSERT INTO district VALUES (393, 89, 'Torbay');
INSERT INTO district VALUES (394, 84, 'Wiltshire');
INSERT INTO district VALUES (395, 88, 'East Devon');
INSERT INTO district VALUES (396, 88, 'Exeter');
INSERT INTO district VALUES (397, 88, 'Mid Devon');
INSERT INTO district VALUES (398, 88, 'North Devon');
INSERT INTO district VALUES (399, 88, 'South Hams');
INSERT INTO district VALUES (400, 88, 'Teignbridge');
INSERT INTO district VALUES (401, 88, 'Torridge');
INSERT INTO district VALUES (402, 88, 'West Devon');
INSERT INTO district VALUES (403, 85, 'Christchurch');
INSERT INTO district VALUES (404, 85, 'East Dorset');
INSERT INTO district VALUES (405, 85, 'North Dorset');
INSERT INTO district VALUES (406, 85, 'Purbeck');
INSERT INTO district VALUES (407, 85, 'West Dorset');
INSERT INTO district VALUES (408, 85, 'Weymouth and Portland');
INSERT INTO district VALUES (409, 82, 'Cheltenham');
INSERT INTO district VALUES (410, 82, 'Cotswold');
INSERT INTO district VALUES (411, 82, 'Forest of Dean');
INSERT INTO district VALUES (412, 82, 'Gloucester');
INSERT INTO district VALUES (413, 82, 'Stroud');
INSERT INTO district VALUES (414, 82, 'Tewkesbury');
INSERT INTO district VALUES (415, 79, 'Mendip');
INSERT INTO district VALUES (416, 79, 'Sedgemoor');
INSERT INTO district VALUES (417, 79, 'South Somerset');
INSERT INTO district VALUES (418, 79, 'Taunton Deane');
INSERT INTO district VALUES (419, 79, 'West Somerset');
INSERT INTO district VALUES (420, 124, 'Flintshire');
INSERT INTO district VALUES (421, 124, 'Denbighshire');
INSERT INTO district VALUES (422, 124, 'Wrexham');
INSERT INTO district VALUES (423, 125, 'Conwy');
INSERT INTO district VALUES (424, 125, 'Ynys Mon');
INSERT INTO district VALUES (425, 126, 'Powys');
INSERT INTO district VALUES (426, 127, 'Carmarthenshire');
INSERT INTO district VALUES (427, 127, 'Cardiganshire');
INSERT INTO district VALUES (428, 127, 'Pembrokeshire');
INSERT INTO district VALUES (429, 127, 'Ceredigion');
INSERT INTO district VALUES (430, 128, 'Cardiff');
INSERT INTO district VALUES (431, 128, 'Vale of Glamorgan');
INSERT INTO district VALUES (432, 129, 'Neath Port Talbot');
INSERT INTO district VALUES (433, 129, 'Swansea');
INSERT INTO district VALUES (434, 130, 'Rhondda Cynon Taf');
INSERT INTO district VALUES (435, 130, 'Merthyr Tydfil');
INSERT INTO district VALUES (436, 130, 'Bridgend');
INSERT INTO district VALUES (437, 131, 'Blaenau Gwent');
INSERT INTO district VALUES (438, 131, 'Caerphilly');
INSERT INTO district VALUES (439, 131, 'Monmouthshire');
INSERT INTO district VALUES (440, 131, 'Newport');
INSERT INTO district VALUES (441, 131, 'Torfaen');
INSERT INTO district VALUES (442, 132, 'Argyll and Bute');
INSERT INTO district VALUES (443, 133, 'Bearsden and Milngavie');
INSERT INTO district VALUES (444, 134, 'Clackmannan');
INSERT INTO district VALUES (445, 167, 'Areas of Glenrothes');
INSERT INTO district VALUES (446, 156, 'Clackmannanshire');
INSERT INTO district VALUES (447, 157, 'Clydesdale');
INSERT INTO district VALUES (448, 158, 'Cumbernauld and Kilsyth');
INSERT INTO district VALUES (449, 159, 'Cumnock and Doon Valley');
INSERT INTO district VALUES (450, 160, 'Cunninghame');
INSERT INTO district VALUES (451, 161, 'Dunfermline');
INSERT INTO district VALUES (452, 162, 'East Kilbride');
INSERT INTO district VALUES (453, 163, 'Eastwood and Strathclyde');
INSERT INTO district VALUES (454, 164, 'Edinburgh');
INSERT INTO district VALUES (455, 165, 'Falkirk');
INSERT INTO district VALUES (456, 166, 'Glasgow');
INSERT INTO district VALUES (457, 168, 'Gordon');
INSERT INTO district VALUES (458, 169, 'Hamilton and South Lanarkshire');
INSERT INTO district VALUES (459, 170, 'Inverclyde District');
INSERT INTO district VALUES (460, 171, 'Kilmarnock and Loudoun');
INSERT INTO district VALUES (461, 173, 'Kyle and Ayrshire');
INSERT INTO district VALUES (462, 172, 'Kyle and Carrick');
INSERT INTO district VALUES (463, 174, 'Antrim');
INSERT INTO district VALUES (464, 175, 'Ballymena');
INSERT INTO district VALUES (465, 176, 'Ballymoney');
INSERT INTO district VALUES (466, 177, 'Carrickfergus');
INSERT INTO district VALUES (467, 178, 'Coleraine');
INSERT INTO district VALUES (468, 179, 'Larne');
INSERT INTO district VALUES (469, 180, 'Magherafelt');
INSERT INTO district VALUES (470, 181, 'Moyle');
INSERT INTO district VALUES (471, 182, 'Newtonabbey');
INSERT INTO district VALUES (472, 183, 'Belfast');
INSERT INTO district VALUES (473, 184, 'Ards');
INSERT INTO district VALUES (474, 185, 'Castlereagh');
INSERT INTO district VALUES (475, 186, 'Down');
INSERT INTO district VALUES (476, 187, 'Lisburn');
INSERT INTO district VALUES (477, 188, 'Armagh');
INSERT INTO district VALUES (478, 189, 'Banbridge');
INSERT INTO district VALUES (479, 190, 'Cookstown');
INSERT INTO district VALUES (480, 191, 'Craigavon');
INSERT INTO district VALUES (481, 192, 'Dungannon and South Tyrone');
INSERT INTO district VALUES (482, 193, 'Newry and Mourne');
INSERT INTO district VALUES (483, 194, 'Derry');
INSERT INTO district VALUES (484, 195, 'Fermanagh');
INSERT INTO district VALUES (485, 196, 'Limavady');
INSERT INTO district VALUES (486, 197, 'Omagh');
INSERT INTO district VALUES (487, 198, 'Strabane');

SELECT setval('region_id_seq', (SELECT max(id) FROM region));
SELECT setval('county_id_seq', (SELECT max(id) FROM county));
SELECT setval('district_id_seq', (SELECT max(id) FROM district));

DO $$
  -- Password is dev
  DECLARE dev BYTEA = '\x4bc7ad6fbcb9983f22f8d3e7a07e3cbaa7e01d50cb6e8220dea0a0f9285612b4e98104000140d46ce5e2cf963e313abd0d41edaa3684c9057ff713f76c073275ad607354b2d9e1b79338a5477892fd381391940c86f98fc9050784b84e55379bcca8b69a85497d30f11706d763d5c3762c6d930e0667301f4892a0c7847150edb731d747de6aed3eeaa3efaf480f8abfd2b59270881c3de4747019b640dfb67d2b4bc7df04dd3394de0e69cae3927ba1f0c86d849481c02c0b18ed085805724bcbc3962ed1f00fbe6b14b64fda8025a4ebe894e971f2d87ab9377356ae2c3bba8188d0926ee782deac37435cc5c2fbacc602f467aaa95f6c3f6bb7d7696bccc5';
BEGIN
  INSERT INTO "user" (username, hash, first_name, last_name, date_of_birth, email, is_male)
  VALUES  ('roscoe.babar', dev, 'Roscoe', 'Babar', '09-JAN-1986', 'test1@example.com', '0'),
          ('emeka.milan', dev, 'Emeka', 'Milan', '19-JAN-1986', 'test2@example.com', '1'),
          ('dimas.quincy', dev, 'Dimas', 'Quincy', '29-JAN-1986', 'test3@example.com', '0'),
          ('anuj.dubhan', dev, 'Anuj', 'Dubhán', '08-JAN-1986', 'test4@example.com', NULL),
          ('avitus.olujimi', dev, 'Avitus', 'Olujimi', '09-JAN-1986', 'test5@example.com', '1'),
          ('seppo.sydney', dev, 'Seppo', 'Sydney', '09-JAN-1986', 'test6@example.com', '0'),
          ('hammond.culhwch', dev, 'Hammond', 'Culhwch', '09-JAN-1986', 'test7@example.com', NULL),
          ('arthur.brad', dev, 'Arthur', 'Brad', '19-JAN-1986', 'test8@example.com', '1'),
          ('jochim.epiktetos', dev, 'Jochim', 'Epiktetos', '09-JAN-1986', 'test9@example.com', '0'),
          ('zopyros.jehiel', dev, 'Zopyros', 'Jehiel', '09-JAN-1986', 'test10@example.com', NULL),
          ('priam.dirk', dev, 'Priam', 'Dirk', '09-JAN-1986', 'test11@example.com', '1'),
          ('norris.brigham', dev, 'Norris', 'Brigham', '09-JAN-1986', 'test12@example.com', '0'),
          ('nat.herbert', dev, 'Nat', 'Herbert', '20-JAN-1986', 'test13@example.com', '0'),
          ('elwyn.ricky', dev, 'Elwyn', 'Ricky', '22-JAN-1986', 'test14@example.com', '1'),
          ('brennan.upton', dev, 'Brennan', 'Upton', '09-JAN-1986', 'test15@example.com', '0'),
          ('konnor.wilton', dev, 'Konnor', 'Wilton', '06-JAN-1986', 'test16@example.com', '0'),
          ('hartley.cade', dev, 'Hartley', 'Cade', '09-JAN-1986', 'test17@example.com', '1'),
          ('corey.brody', dev, 'Corey', 'Brody', '05-JAN-1986', 'test18@example.com', '0'),
          ('quincy.phoenix', dev, 'Quincy', 'Phoenix', '01-JAN-1986', 'test19@example.com', '0'),
          ('harding.kayden', dev, 'Harding', 'Kayden', '09-JAN-1986', 'test20@example.com', '1'),
          ('lucky.fulk', dev, 'Lucky', 'Fulk', '09-JAN-1986', 'test21@example.com', '0'),
          ('lowell.ricky', dev, 'Lowell', 'Ricky', '04-JAN-1986', 'test22@example.com', '0'),
          ('joyce.jimmy', dev, 'Joyce', 'Jimmy', '09-JAN-1986', 'test23@example.com', '1'),
          ('berny.hal', dev, 'Berny', 'Hal', '09-JAN-1986', 'test24@example.com', '0'),
          ('jayson.deforest', dev, 'Jayson', 'Deforest', '09-JAN-1986', 'test25@example.com', '1');
END $$;

INSERT INTO result (
  user_id,
  challenge_id,
  challenge_group_id,
  result,
  country_id,
  region_id,
  county_id,
  district_id,
  age,
  weight)
VALUES  (1, 6, 12, 213, 1, 3, 53, 6, 29, 52100),
        (2, 6, 12, 105, 1, 3, 53, 6, 29, 52100),
        (3, 6, 12, 213, 1, 3, 53, 6, 29, 52100),
        (4, 6, 12, 22, 1, 3, 53, 6, 29, 52100),
        (5, 6, 12, 21, 1, 3, 53, 6, 29, 52100),
        (6, 6, 12, 4, 1, 3, 53, 6, 29, 52100),
        (7, 6, 12, 23, 1, 3, 53, 6, 29, 52100),
        (8, 6, 12, 105, 1, 3, 53, 6, 29, 52100),
        (9, 6, 12, 23, 1, 3, 53, 6, 29, 52100),
        (10, 6, 12, 120, 1, 3, 53, 6, 29, 52100),
        (11, 6, 12, 129, 1, 3, 53, 6, 29, 52100),
        (12, 6, 12, 34, 1, 3, 53, 6, 29, 52100),
        (13, 6, 12, 1, 1, 3, 53, 6, 29, 52100),
        (14, 6, 12, 13, 1, 3, 53, 6, 29, 52100),
        (15, 6, 12, 23, 1, 3, 53, 6, 29, 52100),
        (16, 6, 12, 23, 1, 3, 53, 6, 29, 52100),
        (17, 6, 12, 135, 1, 3, 53, 6, 29, 52100),
        (18, 6, 12, 23, 1, 3, 53, 6, 29, 52100),
        (19, 6, 12, 22, 1, 3, 53, 6, 29, 52100),
        (20, 6, 12, 23, 1, 3, 53, 6, 29, 52100),
        (21, 6, 12, 191, 1, 3, 53, 6, 29, 52100),
        (22, 6, 12, 111, 1, 3, 53, 6, 29, 52100),
        (23, 6, 12, 23, 1, 3, 53, 6, 29, 52100),
        (24, 6, 12, 100, 1, 3, 53, 6, 29, 52100),
        (25, 6, 12, 12, 1, 3, 53, 6, 29, 52100),
        (1, 6, 12, 23, 1, 3, 53, 6, 29, 52100),
        (2, 6, 12, 435, 1, 3, 53, 6, 29, 52100),
        (3, 6, 12, 234, 1, 3, 53, 6, 29, 52100),
        (4, 6, 12, 126, 1, 3, 53, 6, 29, 52100),
        (5, 6, 12, 29, 1, 3, 53, 6, 29, 52100),
        (6, 6, 12, 39, 1, 3, 53, 6, 29, 52100),
        (7, 6, 12, 45, 1, 3, 53, 6, 29, 52100),
        (8, 6, 12, 67, 1, 3, 53, 6, 29, 52100),
        (9, 6, 12, 457, 1, 3, 53, 6, 29, 52100),
        (10, 6, 12, 345, 1, 3, 53, 6, 52, 52100),
        (11, 6, 12, 240, 1, 3, 53, 6, 50, 52100),
        (12, 6, 12, 28, 1, 3, 53, 6, 59, 52100),
        (13, 6, 12, 239, 1, 3, 53, 6, 29, 52100),
        (14, 6, 12, 293, 1, 3, 53, 6, 29, 52100),
        (15, 6, 12, 192, 1, 3, 53, 6, 29, 52100),
        (16, 6, 12, 101, 1, 3, 53, 6, 29, 52100),
        (17, 6, 12, 24, 1, 3, 53, 6, 29, 52100),
        (18, 6, 12, 1, 1, 3, 53, 6, 29, 52100),
        (19, 6, 12, 234, 1, 3, 53, 6, 29, 52100),
        (20, 6, 12, 39, 1, 3, 53, 6, 29, 52100),
        (21, 6, 12, 29, 1, 3, 53, 6, 29, 52100),
        (22, 6, 12, 239, 1, 3, 53, 6, 29, 72100),
        (23, 6, 12, 192, 1, 3, 53, 6, 29, 72100),
        (24, 6, 12, 221, 1, 3, 53, 6, 29, 72100),
        (25, 6, 12, 392, 1, 3, 53, 6, 29, 72100);
