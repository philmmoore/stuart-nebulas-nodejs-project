# Task Runner

After an image is placed in the image inbox bucket, the S3 bucket will trigger the SQS notification to invoke the image processor. This happens for both evidence images and social images.

After the image has been processed and placed in the output bucket, that S3 bucket will trigger the SQS notification to invoke the evidence processed notifier task.

# Requirements
* ImageMagick - http://www.imagemagick.org/, for OS X use `brew install imagemagick`