var Promise = require('bluebird');

module.exports = function EmailService(config) {
  var FROM_EMAIL_ADDRESS = 'noreply@gymfit.com';
  
  var AWS = require('aws-sdk');
  AWS.config.update(config.queueConfig);
  var ses = new AWS.SES();
  
  var _sendEmail = function _sendEmail(toAddress, subject, textBody, htmlBody) {
    var params = {
      Destination: {
        ToAddresses: [ config.developmentEmailAddress || toAddress ]
      },
      Message: {
        Subject: {
          Data: subject
        },
        Body: {
          Text: {
            Data: textBody
          },
          Html: {
            Data: htmlBody
          }
        }
      },
      Source: FROM_EMAIL_ADDRESS
    };

    return new Promise(function(resolve, reject) {
      ses.sendEmail(params, function(err, data) {
        if (err) {
          return reject(err);
        }

        return resolve();
      });
    });
  };

  return {
    sendEmail: _sendEmail
  };
};
