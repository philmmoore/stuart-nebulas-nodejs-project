var Promise = require('bluebird');

module.exports = function S3StorageService(config, logger) {
  var AWS = require('aws-sdk');
  AWS.config.update(config.s3Config);
  var s3 = new AWS.S3();

  var _get = function _get(bucketName, filename) {
    return new Promise(function(resolve, reject) {
      var params = {
        Bucket: config.awsResourcePrefix + bucketName,
        Key: filename
      };

      s3.getObject(params, function(err, data) {
        if (err) {
          return reject(err);
        }

        return resolve(data);
      });
    });
  };

  var _save = function _save(bucketName, file) {
    return new Promise(function(resolve, reject) {
      var params = {
        Bucket: config.awsResourcePrefix + bucketName,
        Key: file.name,
        Body: file.data,
        ContentType: file.mimeType
      };

      s3.upload(params, function(err, data) {
        if (err) {
          return reject(err);
        }

        return resolve();
      });
    });
  };

  return {
    get: _get,
    save: _save
  };
};
