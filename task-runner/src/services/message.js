module.exports = function Message(message, queueName, messageAcknowledger) {
  var _acknowledge = function _acknowledge() {
    return messageAcknowledger(queueName, message.ReceiptHandle);
  };

  return {
    body: JSON.parse(message.Body),
    acknowledge: _acknowledge
  };
};
