var Promise = require('bluebird');
var Message = require('./message');

module.exports = function MessageQueueService(config) {
  var AWS = require('aws-sdk');
  AWS.config.update(config.queueConfig);
  var sqs = new AWS.SQS();

  var _acknowledgeMessage = function _acknowledgeMessage(queueName, receipt) {
    return new Promise(function(resolve, reject) {
      var params = {
        QueueUrl: config.queueUrlPrefix + queueName,
        ReceiptHandle: receipt
      };

      sqs.deleteMessage(params, function(err, data) {
        if (err) {
          return reject(err);
        }

        return resolve();
      });
    });
  };

  var _receiveMessage = function _receiveMessage(queueName) {
    return new Promise(function(resolve, reject) {
      var params = {
        QueueUrl: config.queueUrlPrefix + queueName,
        WaitTimeSeconds: 10
      };

      sqs.receiveMessage(params, function(err, data) {
        if (err) {
          return reject(err);
        }

        if (!data.Messages) {
          return reject('No Messages');
        }

        var message = data.Messages[0];
        return resolve(new Message(message, queueName, _acknowledgeMessage));
      });
    });
  };

  return {
    receiveMessage: _receiveMessage
  };
};
