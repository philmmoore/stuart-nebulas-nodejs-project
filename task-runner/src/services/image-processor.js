var Promise = require('bluebird');
var gm = require('gm').subClass({
    imageMagick: true
});

module.exports = function ImageProcessor() {
  var _process = function _process(image, size) {
    return new Promise(function(resolve, reject) {
      gm(image)
      .autoOrient()
      .noProfile()
      .resize(size)
      .quality(50)
      .toBuffer('png', function (err, buffer) {
        if (err) {
          return reject(err);
        }

        return resolve(buffer);
      });
    });
  };
  
  var _processNoResize = function _processNoResize(image) {
    return new Promise(function(resolve, reject) {
      gm(image)
      .autoOrient()
      .noProfile()
      .quality(50)
      .toBuffer('png', function (err, buffer) {
        if (err) {
          return reject(err);
        }

        return resolve(buffer);
      });
    });
  };

  var _processSquare = function _processSquare(image, size) {
    return new Promise(function(resolve, reject) {
      gm(image)
      .autoOrient()
      .noProfile()
      .resize(size, size, '!')
      .quality(50)
      .toBuffer('png', function (err, buffer) {
        if (err) {
          return reject(err);
        }

        return resolve(buffer);
      });
    });
  };

  return {
    process: _process,
    processNoResize: _processNoResize,
    processSquare: _processSquare
  };
};
