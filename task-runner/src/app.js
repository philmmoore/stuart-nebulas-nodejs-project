var Config = require('./config');

var logger = console;
var config = new Config();
logger.log('Environment: %s.', config.isProduction ? 'production' : 'development');

var postgresDB = require('./db/postgres-db')(config, logger);
var emailService = require('./services/email')(config);
var storageService = require('./services/s3-storage')(config, logger);
var imageProcessor = require('./services/image-processor')(config, logger);

postgresDB.connect().then(function connectedToPostgresDB(db) {
  var evidenceRepository = require('./repositories/evidence')(db);
  var userRepository = require('./repositories/user')(db);
  var emailRepository = require('./repositories/email')(db);
  var registrationRepository = require('./repositories/registration')(db);
  var socialRepository = require('./repositories/social')(db);

  require('./tasks/email-confirmation-emailer')(config, logger, emailService, emailRepository);
  require('./tasks/password-changed-emailer')(config, logger, emailService, emailRepository);
  require('./tasks/recover-username-emailer')(config, logger, emailService, emailRepository);
  require('./tasks/password-reset-emailer')(config, logger, emailService, emailRepository);
  require('./tasks/result-rejected-emailer')(config, logger, emailService, emailRepository);
  require('./tasks/social-update-rejected-emailer')(config, logger, emailService, emailRepository);
  require('./tasks/social-update-flagged-emailer')(config, logger, emailService);
  require('./tasks/user-blocked-emailer')(config, logger, emailService, emailRepository);
  require('./tasks/user-deleted-emailer')(config, logger, emailService, emailRepository);
  require('./tasks/user-unblocked-emailer')(config, logger, emailService, emailRepository);
  require('./tasks/bulk-emailer')(config, logger, emailService, emailRepository);
  require('./tasks/user-avatar-removed-emailer')(config, logger, emailService, emailRepository);
  require('./tasks/evidence-photo-processor')(config, logger, storageService, imageProcessor);
  require('./tasks/social-update-photo-processor')(config, logger, storageService, imageProcessor);
  require('./tasks/avatar-processor')(config, logger, storageService, imageProcessor, userRepository);
  require('./tasks/evidence-processed-notifier')(config, logger, evidenceRepository);
  require('./tasks/social-attachment-processed-notifier')(config, logger, socialRepository);
  require('./tasks/abandoned-registration-reminder')(registrationRepository, logger, emailService, emailRepository);
  require('./tasks/send-push-notification')(config, logger, userRepository);

});
