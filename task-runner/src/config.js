var Config = function config() {
  var redisURL = process.env.REDIS_URL || 'redis://localhost:6379';
  var postgresURL = process.env.DATABASE_URL || 'postgres://localhost:5432';
  var postgresPoolSize = process.env.DATABASE_POOL_SIZE || 4;
  var isProduction = process.env.NODE_ENV === 'production';
  var imageUploadAWSAccessKeyID = process.env.IMAGE_UPLOAD_AWS_ACCESS_KEY_ID || 'AKIAJI4SBWWMMM34BUDQ';
  var imageUploadAWSSecretAccessKey = process.env.IMAGE_UPLOAD_AWS_SECRET_ACCESS_KEY || '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';
  var awsRegion = process.env.AWS_REGION_NAME || 'eu-west-1';
  var awsResourcePrefix = process.env.AWS_RESOURCE_PREFIX || 'gymfit-staging-';
  var siteUrl = process.env.SITE_URL || 'http://localhost:4000/';
  var adminUrl = process.env.ADMIN_URL || 'http://localhost:3000/';
  var developmentEmailAddress = isProduction ? null : 'phil@wearehinge.com';
  var moderatorEmailAddress = process.env.MODERATOR_EMAIL_ADDRESS;

  if (isProduction) {
    postgresURL += '?ssl=true';
  }

  return {
    redisURL: redisURL,
    postgresURL: postgresURL,
    postgresPoolSize: postgresPoolSize,
    isProduction: isProduction,
    s3Config: {
      accessKeyId: imageUploadAWSAccessKeyID,
      secretAccessKey: imageUploadAWSSecretAccessKey,
      region: awsRegion
    },
    queueConfig: {
      accessKeyId: imageUploadAWSAccessKeyID,
      secretAccessKey: imageUploadAWSSecretAccessKey,
      region: awsRegion
    },
    snsConfig: {
      accessKeyId: imageUploadAWSAccessKeyID,
      secretAccessKey: imageUploadAWSSecretAccessKey,
      region: awsRegion
    },
    queueUrlPrefix: 'https://sqs.eu-west-1.amazonaws.com/164535666447/' + awsResourcePrefix,
    awsResourcePrefix: awsResourcePrefix,
    siteUrl: siteUrl,
    adminUrl: adminUrl,
    developmentEmailAddress: developmentEmailAddress,
    moderatorEmailAddress: moderatorEmailAddress
  };
};

module.exports = Config;