var Promise = require('bluebird');
var MessageQueueService = require('../services/message-queue');

module.exports = function AvatarProcessor(config, logger, storageService, imageProcessor, repository) {
  var _messageQueueService = new MessageQueueService(config);

  var _processImage = function _processImage(file, size) {
    return imageProcessor.processSquare(file.Body, size).then(function(processedImage) {
      file = {
        mimeType: 'image/png',
        name: file.name + '-' + size + 'x' + size + '.png',
        data: processedImage
      };

      return storageService.save('avatars', file);
    });
  };

  var _run = function _run() {
    return _messageQueueService.receiveMessage('avatar-uploaded').then(function(message) {
      var userId = parseInt(message.body.Records[0].s3.object.key).toString();

      return storageService.get('avatars-to-process', userId).then(function(file) {
        file.name = userId;

        return Promise.all([
          _processImage(file, 50),  // nav
          _processImage(file, 100), // nav - Retina
          _processImage(file, 40),  // leaderboard
          _processImage(file, 80),  // leaderboard - Retina
          _processImage(file, 120), // profile picture
          _processImage(file, 240), // profile picture - Retina
          _processImage(file, 170)  // iOS app
        ]);
      }).then(function() {
        return repository.setAvatar(userId);
      }).then(function() {
        return message.acknowledge();
      });
    }).catch(function(err) {
      if (err !== 'No Messages') {
        logger.log(err);
      }
    }).finally(function() {
      return _run();
    });
  };

  return _run();
};
