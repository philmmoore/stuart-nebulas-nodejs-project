var MessageQueueService = require('../services/message-queue');

module.exports = function SocialAttachmentProcessedProcessor(config, logger, repository) {
  var _messageQueueService = new MessageQueueService(config);
  
  var _isValidUUID = function _isValidUUID(uuid) {
    return /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i.test(uuid);
  };

  var _run = function _run() {
    return _messageQueueService.receiveMessage('social-attachment-processed').then(function(message) {
      logger.log('received social-attachment-processed message.');
      
      var s3 = message.body.Records[0].s3;
      var filename = s3.object.key.split('.');
      var key = filename[0];
      var fileExtension = filename[1].toLowerCase();
      
      if (s3.configurationId === 'image' && s3.object.key.indexOf('-sm.png') !== -1) {
        logger.log('Ignoring social-attachment-processed message: ' + s3.object.key);
        return message.acknowledge();
      }
      
      if (s3.configurationId === 'image') {
        return repository.setImageProcessed(key).then(function() {
          return message.acknowledge();
        });
      }
      
      if (s3.configurationId === 'video' && fileExtension !== 'mp4' || !_isValidUUID(key)) {
        logger.log('Ignoring social-attachment-processed message: ' + s3.object.key);
        return message.acknowledge();
      }
      
      return repository.setVideoProcessed(key).then(function() {
        return message.acknowledge();
      });
    }).catch(function(err) {
      if (err !== 'No Messages') {
        logger.log(err);
      }
    }).finally(function() {
      return _run();
    });
  };

  return _run();
};