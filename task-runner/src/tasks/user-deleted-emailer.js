var MessageQueueService = require('../services/message-queue');
var moment = require('moment');

module.exports = function UserDeletedEmailer(config, logger, emailService, repository) {
  var COUNTRY_ID = 1;
  
  var _messageQueueService = new MessageQueueService(config);
  
  var _getBody = function _getBody(body, message) {
    return body.replace(/{{REASON}}/g, message.reason);
  };
  
  var _getHtmlBody = function _getHtmlBody(templateData, message) {
    var htmlBody = _getBody(templateData.userDeletedHtmlBody, message);
    
    return templateData.standardTemplate.replace(/{{SUBJECT}}/g, templateData.userDeletedSubject)
                                        .replace(/{{BODY}}/g, htmlBody)
                                        .replace(/{{FOOTER_TEXT}}/g, '');
  };
  
  var _run = function _run() {
    return _messageQueueService.receiveMessage('user-deleted-email').then(function(message) {
      return [message, repository.getStandardTemplate(COUNTRY_ID)];
    }).spread(function(message, templateData) {
      var textBody = _getBody(templateData.userDeletedPlainBody, message.body);
      var htmlBody = _getHtmlBody(templateData, message.body);
      
      return emailService.sendEmail(message.body.email, templateData.userDeletedSubject, textBody, htmlBody).catch(function(err) {
        logger.log('could not send user deleted email to: ' + message.body.email + ' (ignoring)', err);
        return message.acknowledge();
      }).then(function() {
        logger.log('sent user deleted email to: ', message.body.email);
        return message.acknowledge();
      });
    }).catch(function(err) {
      if (err !== 'No Messages') {
        logger.log('could not send user deleted email', err);
      }
    }).finally(function() {
      return _run();
    });
  };

  return _run();
};
