var MessageQueueService = require('../services/message-queue');

module.exports = function EvidencePhotoProcessor(config, logger, repository) {
  var _messageQueueService = new MessageQueueService(config);

  var _run = function _run() {
    return _messageQueueService.receiveMessage('evidence-processed').then(function(message) {
      logger.log('received evidence-processed message.');
      
      var s3 = message.body.Records[0].s3;
      var isImage = s3.configurationId === 'image';
      
      if (isImage && s3.object.key.indexOf('-sm.png') !== -1) {
        logger.log('Ignoring evidence-processed message: ' + s3.object.key);
        return message.acknowledge();
      }
      
      var resultId = parseInt(s3.object.key.split('.')[0]);
      var fileExtension = s3.object.key.split('.')[1].toLowerCase();
      
      if (s3.configurationId === 'video' && fileExtension !== 'mp4' || isNaN(resultId)) {
        logger.log('Ignoring evidence-processed message: ' + s3.object.key);
        return message.acknowledge();
      }

      return repository.setResultEvidence(resultId, isImage).then(function() {
        return message.acknowledge();
      });
    }).catch(function(err) {
      if (err !== 'No Messages') {
        logger.log(err);
      }
    }).finally(function() {
      return _run();
    });
  };

  return _run();
};
