var MessageQueueService = require('../services/message-queue');

module.exports = function SocialUpdatePhotoProcessor(config, logger, storageService, imageProcessor) {
  var _messageQueueService = new MessageQueueService(config);

  var _run = function _run() {
    return _messageQueueService.receiveMessage('social-photo-to-process').then(function(message) {
      var filename = message.body.Records[0].s3.object.key;
      
      return storageService.get('social-photos-to-process', filename).then(function(file) {
        return [
          imageProcessor.processNoResize(file.Body),
          imageProcessor.process(file.Body, 300)
        ];
      }).spread(function(processedImage, thumbnailImage) {
        file = {
          mimeType: 'image/png',
          name: filename + '.png',
          data: processedImage
        };
        
        thumbnailFile = {
          mimeType: 'image/png',
          name: filename + '-sm.png',
          data: thumbnailImage
        };
        
        return [
          storageService.save('social-photos', file),
          storageService.save('social-photos', thumbnailFile)
        ];
      }).then(function() {
        return message.acknowledge();
      });
    }).catch(function(err) {
      if (err !== 'No Messages') {
        logger.log(err);
      }
    }).finally(function() {
      return _run();
    });
  };

  return _run();
};