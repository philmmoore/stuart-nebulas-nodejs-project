var MessageQueueService = require('../services/message-queue');

module.exports = function EmailConfirmationEmailer(config, logger, emailService, repository) {
  var COUNTRY_ID = 1;
  
  var _messageQueueService = new MessageQueueService(config);
  
  var _run = function _run() {
    return _messageQueueService.receiveMessage('change-password-email').then(function(message) {
      return [message, repository.getStandardTemplate(COUNTRY_ID)];
    }).spread(function(message, templateData) {
      var htmlBody = templateData.standardTemplate.replace(/{{SUBJECT}}/g, templateData.userPasswordChangedSubject)
                                          .replace(/{{BODY}}/g, templateData.userPasswordChangedHtmlBody)
                                          .replace(/{{FOOTER_TEXT}}/g, '');
      
      return emailService.sendEmail(message.body.email, templateData.userPasswordChangedSubject, templateData.userPasswordChangedPlainBody, htmlBody).catch(function(err) {
        logger.log('could not send password changed email to: ' + message.body.email + ' (ignoring)', err);
        return message.acknowledge();
      }).then(function() {
        logger.log('sent password changed email to: ', message.body.email);
        return message.acknowledge();
      });
    }).catch(function(err) {
      if (err !== 'No Messages') {
        logger.log('could not send password changed email', err);
      }
    }).finally(function() {
      return _run();
    });
  };

  return _run();
};
