var MessageQueueService = require('../services/message-queue');

module.exports = function PasswordResetEmailer(config, logger, emailService, repository) {
  var COUNTRY_ID = 1;
  
  var _messageQueueService = new MessageQueueService(config);
  
  var _getHtmlBody = function _getHtmlBody(templateData) {
    
    return templateData.standardTemplate.replace(/{{SUBJECT}}/g, templateData.stdSubject)
                                        .replace(/{{BODY}}/g, templateData.stdBody)
                                        .replace(/{{FOOTER_TEXT}}/g, '');
  };

  var _run = function _run() {
    return _messageQueueService.receiveMessage('recover-username').then(function(message) {
      return [message, repository.getStandardTemplate(COUNTRY_ID)];
    }).spread(function(message, templateData) {
      
      templateData.stdSubject = "GymFit Username Recovery";
      templateData.stdBody = "You have requested help logging into your GymFit account. Your username is shown below: <div class=\"box\">" + message.body.username + "</div>";

      var textBody = "Your username is: " + message.body.username;
      var htmlBody = _getHtmlBody(templateData);

      return emailService.sendEmail(message.body.email, templateData.stdSubject, textBody, htmlBody).catch(function(err) {
        logger.log('could not send recover username email to: ' + message.body.email + ' (ignoring)', err);
        return message.acknowledge();
      }).then(function() {
        logger.log('sent recover username email to: ', message.body.email);
        return message.acknowledge();
      });
    }).catch(function(err) {
      if (err !== 'No Messages') {
        logger.log('could not send recover username email', err);
      }
    }).finally(function() {
      return _run();
    });
  };

  return _run();
};
