var MessageQueueService = require('../services/message-queue');

module.exports = function SocialUpdateFlaggedEmailer(config, logger, emailService) {
  var _messageQueueService = new MessageQueueService(config);
  
  var _run = function _run() {
    return _messageQueueService.receiveMessage('social-update-flagged-email').then(function(message) {
      if (!config.moderatorEmailAddress) {
        logger.log('Ignoring social update flagged email message');
        return message.acknowledge();
      }
      
      var link = config.adminUrl + 'social-update/' + message.body.updateId + '/';
      var textBody = 'Social Update: ' + link + '.';
      var htmlBody = 'Social Update: <a href="' + link + '">' + link + '</a>.';
      
      return emailService.sendEmail(config.moderatorEmailAddress, 'Social Update Flagged', textBody, htmlBody).catch(function(err) {
        logger.log('could not send social update flagged email to: ' + config.moderatorEmailAddress + ' (ignoring)', err);
        return message.acknowledge();
      }).then(function() {
        logger.log('sent social update flag email to: ', config.moderatorEmailAddress);
        return message.acknowledge();
      });
    }).catch(function(err) {
      if (err !== 'No Messages') {
        logger.log('could not send social update flag email', err);
      }
    }).finally(function() {
      return _run();
    });
  };

  return _run();
};