var Promise = require('bluebird');
var MessageQueueService = require('../services/message-queue');

module.exports = function SendPushNotification(config, logger, userRepository) {

  var _messageQueueService = new MessageQueueService(config);

  var _AWS = require('aws-sdk');
  _AWS.config.update(config.snsConfig);
  var _SNS = new _AWS.SNS();

  var _sendPushNotification = function _sendPushNotification(device, apnsPayload){

      _SNS.publish({
        TargetArn: device.endpoint,
        MessageStructure: 'json',
        Message: JSON.stringify({
          "APNS_SANDBOX": apnsPayload,
          "APNS": apnsPayload
        })
      },
      function(err,data){
          if (!err){
            logger.log('sent push notification: ', data.MessageId);
          } else {
            logger.log('could not send push notification: ', err);
          }
      });

  };

  var _run = function _run() {
    return _messageQueueService.receiveMessage('send-push-notification').then(function(message) {
      return [message];
    }).spread(function(notification) {
        // Returns all devices for the message userId 
        return [userRepository.getDevicesForPushNotification(notification.body.userId), notification];
    }).spread(function(devices, notification){

      // Here I thought we could register a topic, subscribe the devices, publish to the topic and then delete the topic.
      // SNS only allows for 100,000 topics to be registered so in the event that the app were to have 100,000 concurrent users
      // all performing a push notification (not unlikely with enough users) push notifications would not get sent
      // 
      // Instead I think simply looping over the user returned devices and publishing to each Arn is favourable. Something
      // that can be looked at in future in terms of deliverability and performance.
      // 
      if (devices && devices.length > 0){

        // Construct the notification from the data that was sent from the push notification message payload
        var apnsPayload = {
          aps: {
            alert: notification.body.content,
            badge: notification.body.badge
          } 
        };

        // Add optional parameters here
        if (notification.body.title){
          apnsPayload.aps.title = notification.body.title;
        }
        
        if (notification.body.sound){
          apnsPayload.aps.sound = 'default';
        }

        // Stringify the JSON object for passing to SNS
        apnsPayload = JSON.stringify(apnsPayload);

        // Loop through each device and push the notification
        for (var deviceIndex in devices){
          var device = devices[deviceIndex];
          _sendPushNotification(device, apnsPayload);
        }
        
      } else {
        logger.log('could not send push notification: ', 'no devices registered');
      }

      return notification.acknowledge();

    }).catch(function(err) {
      if (err !== 'No Messages') {
        logger.log('could not send push notification: ', err);
      }
    }).finally(function() {
      return _run();
    });
  };

  return _run();

};
