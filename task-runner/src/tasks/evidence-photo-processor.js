var MessageQueueService = require('../services/message-queue');

module.exports = function EvidencePhotoProcessor(config, logger, storageService, imageProcessor) {
  var _messageQueueService = new MessageQueueService(config);

  var _run = function _run() {
    return _messageQueueService.receiveMessage('evidence-photo').then(function(message) {
      var resultId = parseInt(message.body.Records[0].s3.object.key).toString();

      return storageService.get('evidence-photos-to-process', resultId).then(function(file) {
        return [
          imageProcessor.processNoResize(file.Body),
          imageProcessor.process(file.Body, 300)
        ];
      }).spread(function(processedImage, thumbnailImage) {
        file = {
          mimeType: 'image/png',
          name: resultId + '.png',
          data: processedImage
        };
        
        thumbnailFile = {
          mimeType: 'image/png',
          name: resultId + '-sm.png',
          data: thumbnailImage
        };

        return [
          storageService.save('evidence-photos', file),
          storageService.save('evidence-photos', thumbnailFile)
        ];
      }).then(function() {
        return message.acknowledge();
      });
    }).catch(function(err) {
      if (err !== 'No Messages') {
        logger.log(err);
      }
    }).finally(function() {
      return _run();
    });
  };

  return _run();
};
