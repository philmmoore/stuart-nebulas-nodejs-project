var MessageQueueService = require('../services/message-queue');
var moment = require('moment');

module.exports = function EmailConfirmationEmailer(config, logger, emailService, repository) {
  var COUNTRY_ID = 1;
  
  var _messageQueueService = new MessageQueueService(config);
  
  var _getBody = function _getBody(body, message) {
    return body.replace(/{{CHALLENGE_NAME}}/g, message.challengeName)
               .replace(/{{CHALLENGE_GROUP}}/g, message.challengeGroup)
               .replace(/{{RESULT_VALUE}}/g, message.result)
               .replace(/{{RESULT_DATE}}/g, moment(message.submitted).format('LLL'))
               .replace(/{{REJECT_REASON}}/g, message.reason);
  };
  
  var _getHtmlBody = function _getHtmlBody(templateData, message) {
    var htmlBody = _getBody(templateData.resultRejectedHtmlBody, message);
    
    return templateData.standardTemplate.replace(/{{SUBJECT}}/g, templateData.resultRejectedSubject)
                                        .replace(/{{BODY}}/g, htmlBody)
                                        .replace(/{{FOOTER_TEXT}}/g, '');
  };
  
  var _run = function _run() {
    return _messageQueueService.receiveMessage('result-rejected-email').then(function(message) {
      return [message, repository.getStandardTemplate(COUNTRY_ID)];
    }).spread(function(message, templateData) {
      var textBody = _getBody(templateData.resultRejectedPlainBody, message.body);
      var htmlBody = _getHtmlBody(templateData, message.body);
      
      return emailService.sendEmail(message.body.email, templateData.resultRejectedSubject, textBody, htmlBody).catch(function(err) {
        logger.log('could not send result rejection email to: ' + message.body.email + ' (ignoring)', err);
        return message.acknowledge();
      }).then(function() {
        logger.log('sent result rejection email to: ', message.body.email);
        return message.acknowledge();
      });
    }).catch(function(err) {
      if (err !== 'No Messages') {
        logger.log('could not send result rejection email', err);
      }
    }).finally(function() {
      return _run();
    });
  };

  return _run();
};
