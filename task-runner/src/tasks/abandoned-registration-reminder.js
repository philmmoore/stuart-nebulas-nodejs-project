var Promise = require('bluebird');

module.exports = function AbandonedRegistrationReminder(repository, logger, emailService, emailRepository) {
  var DAY_DURATION = 1000 * 60 * 60 * 24;
  var COUNTRY_ID = 1;
  
  var _getHtmlBody = function _getHtmlBody(templateData) {
    var htmlBody = templateData.registerReminderHtmlBody;
    
    return templateData.standardTemplate.replace(/{{SUBJECT}}/g, templateData.emailConfirmationSubject)
                                        .replace(/{{BODY}}/g, htmlBody)
                                        .replace(/{{FOOTER_TEXT}}/g, '');
  };
  
  var _emailReminder = function _emailReminder(email, templateData) {
    var textBody = templateData.registerReminderPlainBody;
    var htmlBody = _getHtmlBody(templateData);
    
    return emailService.sendEmail(email, templateData.registerReminderSubject, textBody, htmlBody).catch(function(err) {
      logger.log('could not send registration reminder email to: ' + email + ' (ignoring)', err);      
      return Promise.resolve();
    });
  };
  
  var _run = function _run() {
    return emailRepository.getStandardTemplate(COUNTRY_ID).then(function(templateData) {
      return repository.getAbandonedRegistrationEmails().then(function(abandonedRegistrations) {
        if (!abandonedRegistrations) {
          abandonedRegistrations = [];
        }
        
        return Promise.each(abandonedRegistrations, function(abandonedRegistration) {
          var email = abandonedRegistration.email.trim();
          
          return _emailReminder(email, templateData).then(function() {
            return repository.markAbandonedRegistrationAsReminded(email).then(function() {
              logger.log('sent registration reminder email to: ', email);
            });
          });
        });
      });
    }).then(function() {
      return Promise.delay(DAY_DURATION);
    }).then(function() {
      return _run();
    });
  };

  return _run();
};
