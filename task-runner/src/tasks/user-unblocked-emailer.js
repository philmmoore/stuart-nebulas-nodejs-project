var MessageQueueService = require('../services/message-queue');
var moment = require('moment');

module.exports = function UserUnblockedEmailer(config, logger, emailService, repository) {
  var COUNTRY_ID = 1;
  
  var _messageQueueService = new MessageQueueService(config);
  
  var _getHtmlBody = function _getHtmlBody(templateData, message) {
    var htmlBody = templateData.userUnblockedHtmlBody;
    
    return templateData.standardTemplate.replace(/{{SUBJECT}}/g, templateData.userUnblockedSubject)
                                        .replace(/{{BODY}}/g, htmlBody)
                                        .replace(/{{FOOTER_TEXT}}/g, '');
  };
  
  var _run = function _run() {
    return _messageQueueService.receiveMessage('user-unblocked-email').then(function(message) {
      return [message, repository.getStandardTemplate(COUNTRY_ID)];
    }).spread(function(message, templateData) {
      var textBody = templateData.userUnblockedPlainBody;
      var htmlBody = _getHtmlBody(templateData, message.body);
      
      return emailService.sendEmail(message.body.email, templateData.userUnblockedSubject, textBody, htmlBody).catch(function(err) {
        logger.log('could not send user unblocked email to: ' + message.body.email + ' (ignoring)', err);
        return message.acknowledge();
      }).then(function() {
        logger.log('sent user unblocked email to: ', message.body.email);
        return message.acknowledge();
      });
    }).catch(function(err) {
      if (err !== 'No Messages') {
        logger.log('could not send user unblocked email: ', err);
      }
    }).finally(function() {
      return _run();
    });
  };

  return _run();
};
