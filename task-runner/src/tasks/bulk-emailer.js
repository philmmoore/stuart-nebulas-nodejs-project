var MessageQueueService = require('../services/message-queue');

module.exports = function BulkEmailer(config, logger, emailService, repository) {
  var _messageQueueService = new MessageQueueService(config);
  
  var _run = function _run() {
    return _messageQueueService.receiveMessage('bulk-email').then(function(message) {
      return emailService.sendEmail(message.body.email, message.body.subject, message.body.plainBody, message.body.htmlBody).then(function() {
        logger.log('sent bulk email (' + message.body.name + ') to: ', message.body.email);
        return message.acknowledge();
      }).catch(function(err) {
        logger.log('could not send bulk email to: ' + message.body.email + ' (ignoring)', err);        
        return message.acknowledge();
      });
    }).catch(function(err) {
      if (err !== 'No Messages') {
        logger.log('could not send bulk email', err);
      }
    }).finally(function() {
      return _run();
    });
  };

  return _run();
};