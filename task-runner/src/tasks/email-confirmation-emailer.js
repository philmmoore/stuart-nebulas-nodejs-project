var MessageQueueService = require('../services/message-queue');

module.exports = function EmailConfirmationEmailer(config, logger, emailService, repository) {
  var COUNTRY_ID = 1;
  
  var _messageQueueService = new MessageQueueService(config);
  
  var _getHtmlBody = function _getHtmlBody(templateData, url) {
    var urlLink = '<a href="' + url + '">' + url + '</a>';
    var htmlBody = templateData.emailConfirmationHtmlBody.replace(/{{EMAIL_CONFIRMATION_LINK}}/, urlLink);
    
    return templateData.standardTemplate.replace(/{{SUBJECT}}/g, templateData.emailConfirmationSubject)
                                        .replace(/{{BODY}}/g, htmlBody)
                                        .replace(/{{FOOTER_TEXT}}/g, '');
  };
  
  var _run = function _run() {
    return _messageQueueService.receiveMessage('confirm-email').then(function(message) {
      return [message, repository.getStandardTemplate(COUNTRY_ID)];
    }).spread(function(message, templateData) {
      var url = config.siteUrl + 'confirm-email/' + message.body.key + '/';
      var textBody = templateData.emailConfirmationPlainBody.replace(/{{EMAIL_CONFIRMATION_LINK}}/, url);
      var htmlBody = _getHtmlBody(templateData, url);
      
      return emailService.sendEmail(message.body.email, templateData.emailConfirmationSubject, textBody, htmlBody).catch(function(err) {
        logger.log('could not send email confirmation email to: ' + message.body.email + ' (ignoring)', err);
        return message.acknowledge();
      }).then(function() {
        logger.log('sent email confirmation email to: ', message.body.email);
        return message.acknowledge();
      });
    }).catch(function(err) {
      if (err !== 'No Messages') {
        logger.log('could not send email confirmation email', err);
      }
    }).finally(function() {
      return _run();
    });
  };

  return _run();
};
