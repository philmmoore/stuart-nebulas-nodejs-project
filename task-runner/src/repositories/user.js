module.exports = function UserRepository(db) {

  db.registerParameters('avatar_set', [ 'int' ]);
  db.registerParameters('user_get_devices_for_push_notification', [ 'int' ]);
  
  var _setAvatar = function _setAvatar(userId) {
    return db.queryJson('avatar_set', [ userId ]);
  };

  var _getDevicesForPushNotification = function _getDevicesForPushNotification(userId) {
    return db.queryJson('user_get_devices_for_push_notification', [ userId ]);
  };

  return {
    setAvatar: _setAvatar,
    getDevicesForPushNotification: _getDevicesForPushNotification
  };
};