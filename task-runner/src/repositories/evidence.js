module.exports = function EvidenceRepository(db) {
  db.registerParameters('evidence_set', [ 'bigint', 'bit' ]);
  
  var _setResultEvidence = function _setResultEvidence(resultId, isImage) {
    return db.queryJson('evidence_set', [ resultId, isImage ? 1 :0 ]);
  };

  return {
    setResultEvidence: _setResultEvidence
  };
};