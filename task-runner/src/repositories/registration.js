module.exports = function RegistrationRepository(db) {
  db.registerParameters('mark_abandoned_registration_reminded', [ 'text' ]);
  
  var _getAbandonedRegistrationEmails = function _getAbandonedRegistrationEmails() {
    return db.queryJson('get_abandoned_registration_emails');
  };
  
  var _markAbandonedRegistrationAsReminded = function _markAbandonedRegistrationAsReminded(email) {
    return db.queryJson('mark_abandoned_registration_reminded', [ email] );
  };

  return {
    getAbandonedRegistrationEmails: _getAbandonedRegistrationEmails,
    markAbandonedRegistrationAsReminded: _markAbandonedRegistrationAsReminded
  };
};