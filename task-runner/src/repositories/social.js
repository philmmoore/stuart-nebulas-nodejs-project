module.exports = function SocialRepository(db) {
  db.registerParameters('social_update_image_processed', [ 'uuid' ]);
  db.registerParameters('social_update_video_processed', [ 'uuid' ]);
  
  var _setImageProcessed = function _setImageProcessed(key) {
    return db.queryJson('social_update_image_processed', [ key ]);
  };
  
  var _setVideoProcessed = function _setVideoProcessed(key) {
    return db.queryJson('social_update_video_processed', [ key ]);
  };

  return {
    setImageProcessed: _setImageProcessed,
    setVideoProcessed: _setVideoProcessed
  };
};