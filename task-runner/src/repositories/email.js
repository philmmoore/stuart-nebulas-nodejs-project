module.exports = function EmailRepository(db) {
  db.registerParameters('email_template_get', [ 'int' ]);
  
  var _getStandardTemplate = function _getStandardTemplate(countryId) {
    return db.queryJson('email_template_get', [ countryId ]);
  };

  return {
    getStandardTemplate: _getStandardTemplate
  };
};